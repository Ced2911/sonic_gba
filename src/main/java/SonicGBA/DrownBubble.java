package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import Lib.MyRandom;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: EnemyObject */
class DrownBubble extends EnemyObject {
    private static final int BUBBLE_UP_SPEED = 64;
    private static Animation BubbleAnimation = null;
    private static final int COLLISION_HEIGHT = 512;
    private static final int COLLISION_WIDTH = 512;
    private AnimationDrawer bubbledrawer;
    private int waterLevel;

    protected DrownBubble(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        if (BubbleAnimation == null) {
            BubbleAnimation = new Animation("/animation/bubble_up");
        }
        this.bubbledrawer = BubbleAnimation.getDrawer(MyRandom.nextInt(0, 10) > 5 ? 1 : 2, true, 0);
        this.waterLevel = StageManager.getWaterLevel() << 6;
        this.posX = x;
        this.posY = y;
    }

    public void logic() {
        if (!this.dead) {
            if (!isInCamera() || this.posY - 256 < this.waterLevel) {
                this.dead = true;
            }
            this.posY -= 64;
            refreshCollisionRect(this.posX, this.posY);
        }
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 256, y - 256, 512, 512);
    }

    public int getPaintLayer() {
        return 1;
    }

    public void draw(MFGraphics g) {
        if (!this.dead) {
            drawInMap(g, this.bubbledrawer);
        }
    }

    public void doWhileCollision(PlayerObject object, int direction) {
    }

    public void doWhileBeAttack(PlayerObject object, int direction, int animationID) {
    }

    public static void releaseAllResource() {
        Animation.closeAnimation(BubbleAnimation);
        BubbleAnimation = null;
    }

    public void close() {
        this.bubbledrawer = null;
    }
}
