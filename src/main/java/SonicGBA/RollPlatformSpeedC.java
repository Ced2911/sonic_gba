package SonicGBA;

import Lib.MyAPI;
import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;

/* compiled from: GimmickObject */
public class RollPlatformSpeedC extends GimmickObject {
    public static final int COLLISION_HEIGHT = 960;
    public static final int COLLISION_OFFSET_Y = 256;
    public static final int COLLISION_WIDTH = 3072;
    public static final int DEGREE_VELOCITY = 180;
    public static final int DRAW_OFFSET_Y = 768;
    private static final int RING_RANGE = 16;
    public static int degree;
    private int centerX;
    private int centerY;
    private boolean direction;
    private int offsetY2;
    private int radius;
    private int ring1PosX;
    private int ring1PosY;
    private int ring2PosX;
    private int ring2PosY;

    protected RollPlatformSpeedC(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        if (this.mWidth > this.mHeight) {
            this.radius = this.mWidth;
            this.direction = false;
        } else {
            this.radius = this.mHeight;
            this.direction = true;
        }
        this.centerX = this.posX;
        this.centerY = this.posY;
        if (rolllinkImage == null) {
            rolllinkImage = MFImage.createImage("/gimmick/roll_ring.png");
        }
        if (platformImage == null) {
            try {
                if (StageManager.getCurrentZoneId() != 6) {
                    platformImage = MFImage.createImage("/gimmick/platform" + StageManager.getCurrentZoneId() + ".png");
                } else {
                    platformImage = MFImage.createImage("/gimmick/platform" + StageManager.getCurrentZoneId() + (StageManager.getStageID() - 9) + ".png");
                }
            } catch (Exception e) {
                e.printStackTrace();
                try {
                    platformImage = MFImage.createImage("/gimmick/platform0.png");
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        }
    }

    public static void staticLogic() {
        degree += DEGREE_VELOCITY;
        degree %= 23040;
    }

    public void logic() {
        if (StageManager.getCurrentZoneId() >= 3 && StageManager.getCurrentZoneId() <= 6) {
            this.offsetY2 = GimmickObject.PLATFORM_OFFSET_Y;
        }
        int thisDegree = degree + ((this.iLeft * 23040) / 16);
        int preX = this.posX;
        int preY = this.posY;
        thisDegree = (thisDegree + 23040) % 23040;
        if (!this.direction) {
            thisDegree = ((-thisDegree) + 23040) % 23040;
        }
        this.posX = ((this.centerX * 100) + (this.radius * MyAPI.dCos(thisDegree >> 6))) / 100;
        this.posY = ((this.centerY * 100) + (this.radius * MyAPI.dSin(thisDegree >> 6))) / 100;
        this.ring1PosX = ((this.centerX * 100) + ((this.radius / 3) * MyAPI.dCos(thisDegree >> 6))) / 100;
        this.ring1PosY = ((this.centerY * 100) + ((this.radius / 3) * MyAPI.dSin(thisDegree >> 6))) / 100;
        this.ring2PosX = ((this.centerX * 100) + (((this.radius * 2) / 3) * MyAPI.dCos(thisDegree >> 6))) / 100;
        this.ring2PosY = ((this.centerY * 100) + (((this.radius * 2) / 3) * MyAPI.dSin(thisDegree >> 6))) / 100;
        checkWithPlayer(preX, preY, this.posX, this.posY);
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (!player.isFootOnObject(this)) {
            switch (direction) {
                case 1:
                    object.beStop(this.collisionRect.y0, 1, this);
                    this.used = true;
                    return;
                case 4:
                    if (object.getMoveDistance().f14y > 0 && object.getCollisionRect().y1 < this.collisionRect.y1) {
                        object.beStop(this.collisionRect.y0, 1, this);
                        this.used = true;
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    public void draw(MFGraphics g) {
        if (StageManager.getCurrentZoneId() == 6 || StageManager.getCurrentZoneId() == 4) {
            drawInMap(g, rolllinkImage, 0, 0, 16, 16, 0, this.centerX, this.centerY, 3);
            drawInMap(g, rolllinkImage, 16, 0, 16, 16, 0, this.ring1PosX, this.ring1PosY, 3);
            drawInMap(g, rolllinkImage, 16, 0, 16, 16, 0, this.ring2PosX, this.ring2PosY, 3);
        }
        drawInMap(g, platformImage, this.posX, this.posY, 3);
        drawCollisionRect(g);
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 1536, ((y + 256) - 768) + this.offsetY2, 3072, 960);
    }

    public int getPaintLayer() {
        return 0;
    }
}
