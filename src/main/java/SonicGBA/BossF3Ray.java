package SonicGBA;

import Lib.MyAPI;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: BulletObject */
class BossF3Ray extends BulletObject {
    private static final int ATTACK_PLUS_HEIGHT = 512;
    private static final int COLLISION_HEIGHT = 1024;
    private static final int COLLISION_WIDTH = 18432;
    private int directFlag;
    private int fadevalue = 200;
    private int frame_cn = 0;

    protected BossF3Ray(int x, int y, int direct) {
        super(x, y, 0, 0, false);
        this.directFlag = direct;
        this.posX = x;
        this.posY = y;
    }

    public void bulletLogic() {
        this.frame_cn++;
        refreshCollisionRect(this.posX, this.posY);
    }

    public boolean chkDestroy() {
        return this.frame_cn >= 8;
    }

    public void draw(MFGraphics g) {
        int cameraX = MapManager.getCamera().f13x;
        int cameraY = MapManager.getCamera().f14y;
        if (this.directFlag == 0) {
            MyAPI.drawFadeRange(g, this.fadevalue, ((this.posX - COLLISION_WIDTH) >> 6) - cameraX, ((this.posY - 512) >> 6) - cameraY, this.frame_cn);
        } else {
            MyAPI.drawFadeRange(g, this.fadevalue, (this.posX >> 6) - cameraX, ((this.posY - 512) >> 6) - cameraY, this.frame_cn);
        }
        drawCollisionRect(g);
    }

    public void refreshCollisionRect(int x, int y) {
        if (this.directFlag == 0) {
            this.collisionRect.setRect(x - COLLISION_WIDTH, y - 1024, COLLISION_WIDTH, 1536);
        } else {
            this.collisionRect.setRect(x, y - 1024, COLLISION_WIDTH, 1536);
        }
    }
}
