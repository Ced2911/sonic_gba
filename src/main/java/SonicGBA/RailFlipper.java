package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: GimmickObject */
class RailFlipper extends GimmickObject {
    private static Animation railFlipperAnimation = null;
    private AnimationDrawer drawer;

    protected RailFlipper(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        if (railFlipperAnimation == null) {
            railFlipperAnimation = new Animation("/animation/rail_flipper");
        }
        this.drawer = railFlipperAnimation.getDrawer(0, false, 0);
    }

    public void draw(MFGraphics g) {
        drawInMap(g, this.drawer);
        if (this.drawer.getActionId() == 1 && this.drawer.checkEnd()) {
            this.drawer.setActionId(0);
        }
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(this.posX, this.posY - 512, 512, 512);
    }

    public void doWhileRail(PlayerObject object, int direction) {
        if (this.firstTouch) {
            player.setRailFlip();
            this.drawer.setActionId(1);
        }
    }

    public void close() {
        this.drawer = null;
    }

    public static void releaseAllResource() {
        Animation.closeAnimation(railFlipperAnimation);
        railFlipperAnimation = null;
    }
}
