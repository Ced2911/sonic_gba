package SonicGBA;

import GameEngine.Def;
import Lib.MyAPI;
import com.sega.mobile.framework.device.MFGamePad;
import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;

/* compiled from: BackGroundManager */
class BackManagerStage61 extends BackGroundManager {
    private static final int[][] IMAGE_DIVIDE_PARAM = null;
    private static final int STATE_EVE = 0;
    private static final int STATE_PASSING = 1;
    private static final int STATE_STAR = 2;
    private int bgY;
    private int cloudY;
    private MFImage image0;
    private MFImage image1;
    private MFImage image2;
    private int passingY;
    private int starY1;
    private int starY2;
    public int state;

    /*
    static {
        r0 = new int[5][];
        int[] iArr = new int[]{120, 284, 284, iArr};
        int[] iArr2 = new int[4];
        iArr2[2] = 284;
        iArr2[3] = 120;
        r0[1] = iArr2;
        iArr2 = new int[]{256, 284, 124, iArr2};
        iArr2 = new int[]{380, 284, 128, iArr2};
        iArr2 = new int[4];
        iArr2[2] = 284;
        iArr2[3] = 256;
        r0[4] = iArr2;
        IMAGE_DIVIDE_PARAM = r0;
    }
    */

    public BackManagerStage61() {
        try {
            this.image0 = MFImage.createImage("/map/stage6_bg_0.png");
            this.image1 = MFImage.createImage("/map/stage6_bg_1.png");
            this.image2 = MFImage.createImage("/map/stage6_bg_2.png");
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.state = 0;
        this.starY1 = 0;
        this.starY2 = 32768;
    }

    public void draw(MFGraphics g) {
        int cameraX = MapManager.getCamera().f13x;
        int i;
        switch (this.state) {
            case 0:
                if (cameraX <= 1058) {
                    MyAPI.drawImage(g, this.image0, (-cameraX) / 51, 0, 0);
                    return;
                }
                for (i = 0; i < ((SCREEN_HEIGHT + 255) / 256) + 1; i++) {
                    MyAPI.drawImage(g, this.image1, IMAGE_DIVIDE_PARAM[0][0], IMAGE_DIVIDE_PARAM[0][1], IMAGE_DIVIDE_PARAM[0][2], IMAGE_DIVIDE_PARAM[0][3], 0, 0, ((this.bgY - MFGamePad.KEY_NUM_8) + (i * MFGamePad.KEY_NUM_8)) >> 6, 0);
                }
                if (!GameObject.IsGamePause) {
                    this.bgY += 480;
                    this.bgY %= MFGamePad.KEY_NUM_8;
                    return;
                }
                return;
            case 1:
                g.setColor(MapManager.END_COLOR);
                MyAPI.fillRect(g, 0, (this.passingY >> 6) + 224, SCREEN_WIDTH, 8);
                drawStarWorld(g, this.passingY >> 6, true);
                MyAPI.setClip(g, 0, (this.passingY >> 6) + 104, SCREEN_WIDTH, 128);
                for (i = 0; i < 2; i++) {
                    MyAPI.drawImage(g, this.image2, IMAGE_DIVIDE_PARAM[3][0], IMAGE_DIVIDE_PARAM[3][1], IMAGE_DIVIDE_PARAM[3][2], IMAGE_DIVIDE_PARAM[3][3], 0, 0, (this.passingY >> 6) + (((this.cloudY + (32768 * (i - 1))) / 256) + 104), 0);
                }
                MyAPI.setClip(g, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
                MyAPI.drawImage(g, this.image1, IMAGE_DIVIDE_PARAM[1][0], IMAGE_DIVIDE_PARAM[1][1], IMAGE_DIVIDE_PARAM[1][2], IMAGE_DIVIDE_PARAM[1][3], 0, 0, ((this.passingY >> 6) + 224) + 8, 0);
                for (i = 0; i < ((SCREEN_HEIGHT + 255) / 256) + 1; i++) {
                    MyAPI.drawImage(g, this.image1, IMAGE_DIVIDE_PARAM[0][0], IMAGE_DIVIDE_PARAM[0][1], IMAGE_DIVIDE_PARAM[0][2], IMAGE_DIVIDE_PARAM[0][3], 0, 0, (i * 256) + ((((this.passingY >> 6) + 224) + 8) + 120), 0);
                }
                if (!GameObject.IsGamePause) {
                    if (this.passingY < -960) {
                        this.passingY += 480;
                    } else {
                        this.passingY = MyAPI.calNextPosition((double) this.passingY, 0.0d, 1, 2);
                    }
                }
                if (this.passingY == 0) {
                    this.state = 2;
                    return;
                }
                return;
            case 2:
                drawStarWorld(g, 0, false);
                return;
            default:
                return;
        }
    }

    public void close() {
        this.image0 = null;
        this.image1 = null;
        this.image2 = null;
    }

    public void nextState() {
        this.state = 1;
        this.passingY = -(38912 - this.bgY);
    }

    private void drawStarWorld(MFGraphics g, int yOffset, boolean passing) {
        int i;
        g.setColor(528448);
        MyAPI.fillRect(g, 0, yOffset + 0, SCREEN_WIDTH, 100);
        for (i = 0; i < 2; i++) {
            MyAPI.drawImage(g, this.image2, IMAGE_DIVIDE_PARAM[4][0], IMAGE_DIVIDE_PARAM[4][1], IMAGE_DIVIDE_PARAM[4][2], IMAGE_DIVIDE_PARAM[4][3], 0, Def.TOUCH_HELP_LEFT_X, ((this.starY2 + (65536 * (i - 1))) / 256) + yOffset, 0);
            MyAPI.drawImage(g, this.image2, IMAGE_DIVIDE_PARAM[4][0], IMAGE_DIVIDE_PARAM[4][1], IMAGE_DIVIDE_PARAM[4][2], IMAGE_DIVIDE_PARAM[4][3], 0, 128, ((this.starY2 + (65536 * (i - 1))) / 256) + yOffset, 0);
        }
        for (i = 0; i < 2; i++) {
            MyAPI.drawImage(g, this.image2, IMAGE_DIVIDE_PARAM[4][0], IMAGE_DIVIDE_PARAM[4][1], IMAGE_DIVIDE_PARAM[4][2], IMAGE_DIVIDE_PARAM[4][3], 0, 0, ((this.starY1 + (65536 * (i - 1))) / 256) + yOffset, 0);
        }
        MyAPI.drawImage(g, this.image2, IMAGE_DIVIDE_PARAM[2][0], IMAGE_DIVIDE_PARAM[2][1], IMAGE_DIVIDE_PARAM[2][2], IMAGE_DIVIDE_PARAM[2][3], 0, 0, yOffset + 100, 0);
        if (!passing) {
            MyAPI.setClip(g, 0, yOffset + 104, SCREEN_WIDTH, SCREEN_HEIGHT - 104);
            for (i = 0; i < (((SCREEN_HEIGHT - 104) + 127) / 128) + 1; i++) {
                MyAPI.drawImage(g, this.image2, IMAGE_DIVIDE_PARAM[3][0], IMAGE_DIVIDE_PARAM[3][1], IMAGE_DIVIDE_PARAM[3][2], IMAGE_DIVIDE_PARAM[3][3], 0, 0, (((this.cloudY + (32768 * (i - 1))) / 256) + 104) + yOffset, 0);
            }
            MyAPI.setClip(g, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
        }
        if (!GameObject.IsGamePause) {
            this.cloudY -= 30;
            this.cloudY += 32768;
            this.cloudY %= 32768;
            this.starY1 += Def.TOUCH_CHARACTER_SELECT_LEFT_ARROW_OFFSET_X;
            this.starY1 += 65536;
            this.starY1 %= 65536;
            this.starY2 -= 15;
            this.starY2 += 65536;
            this.starY2 %= 65536;
        }
    }
}
