package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: GimmickObject */
class TutorialPoint extends GimmickObject {
    private static final int COLLISION_WIDTH = 2048;
    private static Animation tutorialAnimation;
    private AnimationDrawer drawer;

    protected TutorialPoint(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
    }

    public void draw(MFGraphics g) {
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 1024, y - 1024, 2048, 2048);
    }

    public int getPaintLayer() {
        return 0;
    }

    public void close() {
        this.drawer = null;
    }

    public static void releaseAllResource() {
        Animation.closeAnimation(tutorialAnimation);
        tutorialAnimation = null;
    }
}
