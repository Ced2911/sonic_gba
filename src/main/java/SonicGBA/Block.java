package SonicGBA;

import GameEngine.Def;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: GimmickObject */
class Block extends GimmickObject {
    private static final int Y_OFFSET = 1024;
    private int collisionHeight;
    private int collisionWidth;
    private int direct;
    private int height;
    private boolean isActive;
    private int width;

    protected Block(int x, int y, int width, int height, int direct) {
        super(99, x, y, 0, 0, width, height);
        this.height = 0;
        this.posX = x;
        this.posY = y + 1024;
        this.width = (width * 8) << 6;
        this.collisionWidth = this.width;
        this.height = (height * 4) << 6;
        this.direct = direct;
        this.isActive = false;
    }

    protected Block(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        this.height = 0;
        this.direct = this.iLeft;
        this.isActive = false;
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (direction == 1 || direction == 0) {
            object.beStop(0, direction, this);
        }
        if (direction != 2 && direction != 3) {
            return;
        }
        if (object.getFootPositionY() >= this.collisionRect.y0 + ((this.mHeight * 3) / 4)) {
            object.beStop(0, direction, this);
        } else if (object.getVelX() > 0 && direction == 3) {
            object.setFootPositionY(this.collisionRect.y0 - 64);
        } else if (object.getVelX() < 0 && direction == 2) {
            object.setFootPositionY(this.collisionRect.y0 - 64);
        }
    }

    public void logic() {
        if (this.collisionRect.collisionChk(player.getCollisionRect()) && player.collisionState == (byte) 2) {
            player.moveOnObject(player.footPointX + (this.direct == 0 ? Def.TOUCH_HELP_LEFT_X : 128), player.footPointY);
            this.isActive = true;
            player.isOnBlock = true;
        }
    }

    public void doWhileNoCollision() {
    }

    public void draw(MFGraphics g) {
        drawCollisionRect(g);
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x, y, this.mWidth, this.mHeight);
    }
}
