package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: GimmickObject */
class TorchFire extends GimmickObject {
    private static AnimationDrawer drawer;
    private static AnimationDrawer drawer2;

    protected TorchFire(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        if (drawer == null) {
            if (StageManager.getCurrentZoneId() != 6) {
                drawer = new Animation("/animation/torch_fire").getDrawer(0, true, 0);
            } else {
                drawer = new Animation("/animation/light").getDrawer(0, true, 0);
                drawer2 = new Animation("/animation/light").getDrawer(1, true, 0);
                drawer2.setPause(true);
            }
            drawer.setPause(true);
        }
        this.posX += 256;
        this.posY += 1024;
    }

    public static void staticLogic() {
        drawer.moveOn();
        if (StageManager.getCurrentZoneId() == 6) {
            drawer2.moveOn();
        }
    }

    public void draw(MFGraphics g) {
        if (StageManager.getCurrentZoneId() != 6) {
            drawInMap(g, drawer);
        } else if (this.iLeft == 0) {
            drawInMap(g, drawer, this.posX + GimmickObject.PLATFORM_OFFSET_Y, this.posY - 1280);
        } else {
            drawInMap(g, drawer2, this.posX + GimmickObject.PLATFORM_OFFSET_Y, this.posY - 1280);
        }
    }

    public int getPaintLayer() {
        return 0;
    }

    public static void releaseAllResource() {
        Animation.closeAnimationDrawer(drawer);
        drawer = null;
    }
}
