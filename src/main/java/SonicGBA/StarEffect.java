package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import Lib.Coordinate;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: PlayerSuperSonic */
class StarEffect implements SonicDef {
    private static final int MAX_VELOCITY = -720;
    private static final int MOVE_POWER = -360;
    private AnimationDrawer drawer;
    private int velX = 0;
    private int f25x;
    private int f26y;

    public StarEffect(Animation animation, int type, int x, int y) {
        this.drawer = animation.getDrawer(type, false, 0);
        this.f25x = x;
        this.f26y = y;
    }

    public boolean draw(MFGraphics g) {
        if (!GameObject.IsGamePause) {
            this.velX += MOVE_POWER;
            if (this.velX < MAX_VELOCITY) {
                this.velX = MAX_VELOCITY;
            }
            this.f25x += this.velX;
        }
        Coordinate camera = MapManager.getCamera();
        this.drawer.draw(g, (this.f25x >> 6) - camera.f13x, (this.f26y >> 6) - camera.f14y);
        return this.drawer.checkEnd();
    }

    public void close() {
        this.drawer = null;
    }
}
