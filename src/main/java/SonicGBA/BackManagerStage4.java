package SonicGBA;

import Common.WaveInvertEffect;
import Lib.MyAPI;
import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;

/* compiled from: BackGroundManager */
class BackManagerStage4 extends BackGroundManager {
    private static int IMAGE_HEIGHT = 512;
    private static int IMAGE_WIDTH = 300;
    private MFImage image;
    private int speedx = 0;
    private int speedy = 0;

    public BackManagerStage4(int subid) {
        try {
            this.image = MFImage.createImage("/map/stage4_bg.png");
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (subid == 0) {
            this.speedx = 637;
            this.speedy = 9;
            return;
        }
        this.speedx = 535;
        this.speedy = 7;
    }

    public void close() {
        this.image = null;
    }

    public void draw(MFGraphics g) {
        int i;
        int cameraX = MapManager.getCamera().f13x;
        int cameraY = MapManager.getCamera().f14y;
        int j;
        int waterLevel = StageManager.getWaterLevel();
        for (i = 0; i < MapManager.CAMERA_HEIGHT; i += IMAGE_HEIGHT) {
            for (j = 0; j < MapManager.CAMERA_WIDTH; j += IMAGE_WIDTH) {
                WaveInvertEffect.drawImage(g, this.image, (0 - (cameraX / this.speedx)) + j, 0 - (cameraY / this.speedy), 0, 0, 300, 40, 2);
            }
        }
        g.setClip(0, 0, SCREEN_WIDTH, waterLevel - cameraY);
        for (i = 0; i < MapManager.CAMERA_HEIGHT; i += IMAGE_HEIGHT) {
            for (j = 0; j < MapManager.CAMERA_WIDTH; j += IMAGE_WIDTH) {
                MyAPI.drawImage(g, this.image, (0 - (cameraX / this.speedx)) + j, 0 - (cameraY / this.speedy), 0);
            }
        }
        g.setClip(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    }
}
