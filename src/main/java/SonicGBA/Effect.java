package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import com.sega.mobile.framework.device.MFGraphics;
import java.lang.reflect.Array;

public class Effect {
    public static final int EFFECT_LAYER_NUM = 2;
    public static final int EFFECT_LAYER_PLAYER = 1;
    public static final int EFFECT_LAYER_TOP = 0;
    public static final int EFFECT_NUM = 10;
    public static Effect[][] effectArray = ((Effect[][]) Array.newInstance(Effect.class, new int[]{2, 10}));
    private AnimationDrawer drawer = null;
    private int mPosX;
    private int mPosY;

    static {
        for (int l = 0; l < 2; l++) {
            for (int i = 0; i < 10; i++) {
                effectArray[l][i] = new Effect(null, 0, 0, 0, 0);
            }
        }
    }

    public static void showEffect(Animation animation, int id, int x, int y, int trans, int layer) {
        if (layer >= 0 && layer < 2) {
            for (int i = 0; i < 10; i++) {
                if (effectArray[layer][i].drawer == null) {
                    effectArray[layer][i].setDrawer(animation, id, x, y, trans);
                    return;
                }
            }
        }
    }

    public static void showEffect(Animation animation, int id, int x, int y, int trans) {
        showEffect(animation, id, x, y, trans, 0);
    }

    public static void showEffectPlayer(Animation animation, int id, int x, int y, int trans) {
        showEffect(animation, id, x, y, trans, 1);
    }

    public static void draw(MFGraphics g, int layer) {
        if (layer >= 0 && layer < 2) {
            for (int i = 0; i < 10; i++) {
                effectArray[layer][i].mDraw(g);
            }
        }
    }

    private Effect(Animation animation, int id, int x, int y, int trans) {
        if (animation != null) {
            this.drawer = animation.getDrawer(id, false, trans);
            this.mPosX = x;
            this.mPosY = y;
        }
    }

    private void setDrawer(Animation animation, int id, int x, int y, int trans) {
        if (animation != null) {
            this.drawer = animation.getDrawer(id, false, trans);
            this.mPosX = x;
            this.mPosY = y;
        }
    }

    private boolean mDraw(MFGraphics g) {
        if (this.drawer == null) {
            return true;
        }
        this.drawer.draw(g, this.mPosX - MapManager.getCamera().f13x, this.mPosY - MapManager.getCamera().f14y);
        if (!this.drawer.checkEnd()) {
            return false;
        }
        this.drawer = null;
        return true;
    }
}
