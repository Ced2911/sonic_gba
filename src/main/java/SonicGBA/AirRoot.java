package SonicGBA;

import Lib.MyAPI;
import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;

/* compiled from: GimmickObject */
class AirRoot extends GimmickObject {
    private static final int COLLISION_HEIGHT = 320;
    private static final int COLLISION_WIDTH = 1792;
    private static final int CORNER_LEFT_BOTTOM = 2;
    private static final int CORNER_LEFT_TOP = 0;
    private static final int CORNER_RIGHT_BOTTOM = 3;
    private static final int CORNER_RIGHT_TOP = 1;
    private static final int IMAGE_HEIGHT = 1792;
    private static final int IMAGE_WIDTH = 1792;
    private static MFImage image;
    private int imageHeight = 0;
    private int imageWidth;
    private int type;

    protected AirRoot(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        if (image == null) {
            image = MFImage.createImage("/gimmick/airroot.png");
        }
        if (image != null) {
            this.imageWidth = MyAPI.zoomIn(image.getWidth());
            this.imageHeight = MyAPI.zoomIn(image.getHeight());
        }
        this.type = left;
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (!player.piping) {
            if (object.getBodyPositionY() < this.collisionRect.y0) {
                object.setFootPositionY(this.collisionRect.y0 - 64);
                direction = 1;
            }
            if (direction == 1) {
                object.beStop(this.collisionRect.x0, direction, this);
            }
        }
    }

    public int getPaintLayer() {
        return 0;
    }

    public void draw(MFGraphics g) {
        int trans = 0;
        switch (this.type) {
            case 0:
                trans = 0;
                break;
            case 1:
                trans = 5;
                break;
            case 2:
                trans = 6;
                break;
            case 3:
                trans = 3;
                break;
        }
        drawInMap(g, image, 0, 0, this.imageWidth, this.imageHeight, trans, this.posX - 896, this.posY - 896, 0);
        drawCollisionRect(g);
    }

    public void refreshCollisionRect(int x, int y) {
        if (this.type == 0 || this.type == 1) {
            this.collisionRect.setRect(x - 896, (y - 896) + 192, 1792, COLLISION_HEIGHT);
        } else if (this.type == 2 || this.type == 3) {
            this.collisionRect.setRect(x - 896, (y - 896) + 1472, 1792, COLLISION_HEIGHT);
        }
    }

    public static void releaseAllResource() {
        image = null;
    }
}
