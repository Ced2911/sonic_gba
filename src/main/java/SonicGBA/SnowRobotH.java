package SonicGBA;

import Lib.Animation;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: EnemyObject */
class SnowRobotH extends EnemyObject {
    private static final int ALERT_HEIGHT = 100;
    private static final int ALERT_WIDTH = 2;
    private static final int COLLISION_HEIGHT = 1600;
    private static final int COLLISION_WIDTH = 1152;
    private static final int LAUNCH_SPEED = -1280;
    private static final int SPEED = 128;
    private static final int STATE_ATTACK = 1;
    private static final int STATE_PATROL = 0;
    private static final int WAIT_MAX = 20;
    private boolean IsFire = false;
    private int alert_state;
    private boolean dir = false;
    private int endPosX;
    private int startPosX;
    private int state;
    private int wait_cn;

    protected SnowRobotH(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        if (snowrobotAnimation == null) {
            snowrobotAnimation = new Animation("/animation/yukimal");
        }
        this.drawer = snowrobotAnimation.getDrawer(0, true, 0);
        this.startPosX = this.posX;
        this.endPosX = this.posX + this.mWidth;
        this.posY = getGroundY(this.posX, this.posY);
        this.dir = false;
        this.state = 0;
        this.wait_cn = 0;
        this.IsFire = false;
    }

    public static void releaseAllResource() {
        Animation.closeAnimation(snowrobotAnimation);
        snowrobotAnimation = null;
    }

    public void logic() {
        if (!this.dead) {
            this.alert_state = checkPlayerInEnemyAlertRange(this.posX >> 6, this.posY >> 6, 2, 100);
            int preX = this.posX;
            int preY = this.posY;
            switch (this.state) {
                case 0:
                    if (this.dir) {
                        this.posX -= 128;
                        if (this.posX <= this.startPosX) {
                            this.dir = false;
                            this.posX = this.startPosX;
                        }
                    } else {
                        this.posX += 128;
                        if (this.posX >= this.endPosX) {
                            this.dir = true;
                            this.posX = this.endPosX;
                        }
                    }
                    if (!(this.alert_state != 0 || this.posX == this.startPosX || this.posX == this.endPosX)) {
                        this.state = 1;
                        this.drawer.setActionId(1);
                        this.drawer.setLoop(false);
                        this.IsFire = true;
                    }
                    checkWithPlayer(preX, preY, this.posX, this.posY);
                    return;
                case 1:
                    if (this.wait_cn < 20) {
                        this.wait_cn++;
                        if (this.drawer.checkEnd() && this.IsFire) {
                            this.IsFire = false;
                            BulletObject.addBullet(17, this.posX, this.posY - COLLISION_HEIGHT, 0, LAUNCH_SPEED);
                        }
                    } else {
                        this.wait_cn = 0;
                        this.drawer.setActionId(0);
                        this.drawer.setTrans(0);
                        this.drawer.setLoop(true);
                        this.state = 0;
                    }
                    checkWithPlayer(preX, preY, this.posX, this.posY);
                    return;
                default:
                    return;
            }
        }
    }

    public void draw(MFGraphics g) {
        if (!this.dead) {
            drawInMap(g, this.drawer);
        }
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 576, y - COLLISION_HEIGHT, 1152, COLLISION_HEIGHT);
    }
}
