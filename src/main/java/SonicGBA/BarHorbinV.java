package SonicGBA;

import Lib.MyAPI;
import State.StringIndex;
import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;

/* compiled from: GimmickObject */
public class BarHorbinV extends GimmickObject {
    public static final int COLLISION_HEIGHT = 704;
    public static final int COLLISION_OFFSET = 512;
    public static final int COLLISION_WIDTH = 2560;
    public static final int HOBIN_POWER = 1152;
    public static MFImage barImage;
    private int functionDirection;
    public HobinCal hobinCal;

    protected BarHorbinV(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        if (barImage == null) {
            try {
                barImage = MFImage.createImage("/gimmick/bar.png");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        this.functionDirection = 2;
        if (this.iLeft == 0) {
            this.functionDirection = 3;
        }
        this.hobinCal = new HobinCal();
    }

    public void draw(MFGraphics g) {
        if (this.iLeft == 0) {
            drawInMap(g, barImage, 0, 0, MyAPI.zoomIn(barImage.getWidth()), MyAPI.zoomIn(barImage.getHeight()), 6, this.posX + this.hobinCal.getPosOffsetX(), this.posY + this.hobinCal.getPosOffsetY(), 3);
        } else {
            drawInMap(g, barImage, 0, 0, MyAPI.zoomIn(barImage.getWidth()), MyAPI.zoomIn(barImage.getHeight()), 5, this.posX + this.hobinCal.getPosOffsetX(), this.posY + this.hobinCal.getPosOffsetY(), 3);
        }
        this.hobinCal.logic();
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 512, y - 1280, COLLISION_HEIGHT, COLLISION_WIDTH);
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (object == player) {
            player.beStop(0, direction, this);
            soundInstance.playSe(24);
            switch (direction) {
                case 0:
                    player.bePop(HOBIN_POWER, direction);
                    player.bePop(HOBIN_POWER, this.functionDirection);
                    if (this.functionDirection == 2) {
                        this.hobinCal.startHobin(0, 225, 10);
                        return;
                    } else {
                        this.hobinCal.startHobin(0, -45, 10);
                        return;
                    }
                case 1:
                    player.bePop(HOBIN_POWER, direction);
                    player.bePop(HOBIN_POWER, this.functionDirection);
                    if (this.functionDirection == 2) {
                        this.hobinCal.startHobin(0, StringIndex.FONT_COLON_RED, 10);
                        return;
                    } else {
                        this.hobinCal.startHobin(0, 45, 10);
                        return;
                    }
                case 2:
                case 3:
                    if (this.iLeft != 0 && direction == 3) {
                        return;
                    }
                    if (this.iLeft != 0 || direction != 2) {
                        if (player.getCheckPositionY() <= this.collisionRect.getCenterY()) {
                            player.bePop(HOBIN_POWER, 1);
                            if (this.functionDirection == 2) {
                                this.hobinCal.startHobin(0, StringIndex.FONT_COLON_RED, 10);
                            } else {
                                this.hobinCal.startHobin(0, 45, 10);
                            }
                        } else {
                            player.bePop(HOBIN_POWER, 0);
                            if (this.functionDirection == 2) {
                                this.hobinCal.startHobin(0, 225, 10);
                            } else {
                                this.hobinCal.startHobin(0, -45, 10);
                            }
                        }
                        player.bePop(HOBIN_POWER, direction);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    public static void releaseAllResource() {
        barImage = null;
    }

    public int getPaintLayer() {
        return 0;
    }
}
