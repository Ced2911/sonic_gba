package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import State.GameState;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: GimmickObject */
class SpSpring extends Spring {
    private static final int COLLISION_HEIGHT = 2048;
    private static final int COLLISION_WIDTH = 1920;
    private static final int ENTERED_SP_SPRING_POWER = 2096;
    private static final int SP_SPRING_POWER = 2800;
    private static final int VELOCITY_MINUS = 7;
    public static Animation springAnimation = null;
    private AnimationDrawer drawer;
    private boolean spEntered;

    public SpSpring(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        if (springAnimation == null) {
            springAnimation = new Animation("/animation/sp_bane");
        }
        this.springPower = SP_SPRING_POWER;
        this.drawer = springAnimation.getDrawer(0, false, 0);
        this.spEntered = GameState.isBackFromSpStage;
    }

    public void draw(MFGraphics g) {
        drawInMap(g, this.drawer, this.posX, this.posY);
        if (this.drawer.checkEnd()) {
            this.drawer.setActionId(0);
        }
        drawCollisionRect(g);
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (!this.used) {
            object.beStop(this.collisionRect.x0, direction, this);
            if (object == player) {
                switch (direction) {
                    case 1:
                        if (this.spEntered) {
                            player.beSpring(ENTERED_SP_SPRING_POWER, direction);
                            player.setAnimationId(14);
                            soundInstance.playSe(37);
                        } else {
                            player.beSpSpring(this.springPower, direction);
                            StageManager.saveSpecialStagePoint(this.posX - COLLISION_WIDTH, this.posY);
                        }
                        this.drawer.setActionId(1);
                        return;
                    default:
                        return;
                }
            }
        }
    }

    public void doWhileBeAttack(PlayerObject object, int direction, int animationID) {
        if (PlayerObject.getCharacterID() == 3 && animationID != 6 && animationID != 7 && direction == 4) {
            if (this.spEntered) {
                player.beSpring(2724, 1);
                player.setAnimationId(14);
                soundInstance.playSe(37);
            } else {
                player.beSpSpring(this.springPower, direction);
                StageManager.saveSpecialStagePoint(this.posX - COLLISION_WIDTH, this.posY);
            }
            this.drawer.setActionId(1);
        }
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 960, y - 2048, COLLISION_WIDTH, 2048);
    }

    public static void releaseAllResource() {
        Animation.closeAnimation(springAnimation);
        springAnimation = null;
    }

    public void close() {
        this.drawer = null;
    }
}
