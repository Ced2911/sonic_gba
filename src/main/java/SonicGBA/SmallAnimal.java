package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import Lib.MyRandom;
import com.sega.engine.action.ACCollision;
import com.sega.engine.action.ACObject;
import com.sega.mobile.framework.device.MFGraphics;
import java.util.Vector;

public class SmallAnimal extends GameObject implements MapBehavior {
    private static final int[][] ANIMAL_ID;
    private static final int ANIMAL_INIT_VEL_Y = -1000;
    private static final int ANIMAL_NUM = 12;
    private static final int[] ANIMAL_TYPE_INFO;
    private static final int COLLISION_HEIGHT = 1024;
    private static final int COLLISION_WIDTH = 1024;
    public static final int FLY_VELOCITY_X = -1000;
    public static final int FLY_VELOCITY_Y = -300;
    private static final int[][] STAGE_TO_ANIMAL_ID;
    private static final int[][] STAGE_TO_CAGE_ANIMAL_ID;
    public static final int TYPE_FLY = 2;
    public static final int TYPE_MOVE = 1;
    public static final int TYPE_STAY = 0;
    private static Animation animalAnimation;
    private static Vector animalVec = new Vector();
    private static int animationIndex;
    private static boolean changeLayer;
    private static int initalVelY = -1000;
    protected AnimationDrawer drawer;
    protected MapObject mObj;
    private int sourceLayer;
    private int startPosX;
    private int startPosY;
    protected int type;


    static {
        int[][] var0 = new int[][]{{1, 3, 4, 6, 11}, null, null};
        int[] var1 = new int[]{0, 2, 7, 8, 10};
        var0[1] = var1;
        var0[2] = new int[]{5, 9};
        ANIMAL_ID = var0;
        int[] var2 = new int[15];
        var2[0] = 1;
        var2[2] = 1;
        var2[5] = 2;
        var2[7] = 1;
        var2[8] = 1;
        var2[9] = 2;
        var2[10] = 1;
        var2[12] = 1;
        var2[13] = 1;
        ANIMAL_TYPE_INFO = var2;
        int[][] var3 = new int[][]{{6, 1, 10}, {4, 8, 9}, {3, 2, 5}, {6, 14, 13}, null, null};
        int[] var4 = new int[]{11, 3, 0};
        var3[4] = var4;
        var3[5] = new int[]{4, 7, 12};
        STAGE_TO_ANIMAL_ID = var3;
        int[][] var5 = new int[5][];
        int[] var6 = new int[]{0, 10};
        var5[0] = var6;
        int[] var7 = new int[]{0, 8};
        var5[1] = var7;
        var5[2] = new int[]{2, 7};
        var5[3] = new int[]{12, 13};
        var5[4] = new int[]{8, 10};
        STAGE_TO_CAGE_ANIMAL_ID = var5;
        animalVec = new Vector();
        initalVelY = -1000;
    }

    public static void setInitVelY(int velY) {
        initalVelY = velY;
    }

    public static void addAnimal(int x, int y, int layer) {
        addAnimal(x, y, layer, false);
    }

    public static void addAnimal(int x, int y, int layer, boolean isTransLayer) {
        int i;
        int stageId = StageManager.getCurrentZoneId() - 1;
        changeLayer = isTransLayer;
        int i2 = STAGE_TO_ANIMAL_ID[stageId][animationIndex];
        if (changeLayer) {
            i = (-layer) + 1;
        } else {
            i = layer;
        }
        addAnimalByID(i2, x, y, i);
        animationIndex++;
        animationIndex %= STAGE_TO_ANIMAL_ID[stageId].length;
        initalVelY = ANIMAL_INIT_VEL_Y;
    }

    public static void addPatrolAnimal(int x, int y, int layer, int left, int right) {
        addPatrolAnimal(MyRandom.nextInt(1, 2), x, y, layer, left, right);
    }

    public static void addAnimal(int type, int x, int y, int layer) {
        if (animalAnimation == null) {
            animalAnimation = new Animation("/animation/animal");
        }
        int[] animalIdArray = ANIMAL_ID[type];
        SmallAnimal obj = new SmallAnimal(type, animalIdArray[MyRandom.nextInt(animalIdArray.length)], x, y, layer);
        obj.refreshCollisionRect(x, y);
        animalVec.addElement(obj);
    }

    public static void addAnimalByID(int animalID, int x, int y, int layer) {
        if (animalAnimation == null) {
            animalAnimation = new Animation("/animation/animal");
        }
        SmallAnimal obj = new SmallAnimal(ANIMAL_TYPE_INFO[animalID], animalID, x, y, layer);
        obj.refreshCollisionRect(x, y);
        animalVec.addElement(obj);
    }

    public static void addPatrolAnimal(int type, int x, int y, int layer, int left, int right) {
        int[] animalIdArray;
        if (animalAnimation == null) {
            animalAnimation = new Animation("/animation/animal");
        }
        switch (type) {
            case TYPE_FLY:
                animalIdArray = ANIMAL_ID[type];
                break;
            default:
                animalIdArray = STAGE_TO_CAGE_ANIMAL_ID[StageManager.getCurrentZoneId() - 1];
                break;
        }
        SmallAnimal obj = new PatrolAnimal(type, animalIdArray[MyRandom.nextInt(animalIdArray.length)], x, y, layer, left, right);
        obj.refreshCollisionRect(x, y);
        animalVec.addElement(obj);
    }

    public static void animalLogic() {
        int i = 0;
        while (i < animalVec.size()) {
            SmallAnimal tmpAnimal = (SmallAnimal) animalVec.elementAt(i);
            tmpAnimal.logic();
            if (tmpAnimal.checkDestroy()) {
                tmpAnimal.close();
                animalVec.removeElementAt(i);
                i--;
            }
            i++;
        }
    }

    public static void animalDraw(MFGraphics g) {
        for (int i = 0; i < animalVec.size(); i++) {
            ((SmallAnimal) animalVec.elementAt(i)).draw(g);
        }
    }

    public static void animalClose() {
        for (int i = 0; i < animalVec.size(); i++) {
            ((SmallAnimal) animalVec.elementAt(i)).close();
        }
        animalVec.removeAllElements();
    }

    public static void animalInit() {
        animalClose();
        if (animalAnimation == null) {
            animalAnimation = new Animation("/animation/animal");
        }
        animationIndex = 0;
    }

    public static void releaseAllResource() {
        Animation.closeAnimation(animalAnimation);
        animalAnimation = null;
    }

    public SmallAnimal(int type, int animalId, int x, int y, int layer) {
        this.posX = x;
        this.posY = y;
        this.startPosX = x;
        this.startPosY = y;
        this.sourceLayer = layer;
        this.type = type;
        this.drawer = animalAnimation.getDrawer(animalId, true, 0);
        if (type != TYPE_FLY) {
            this.mObj = new MapObject(x, y, 0, initalVelY, this, layer);
            this.mObj.setBehavior(this);
        }
    }

    public void doWhileCollision(PlayerObject object, int direction) {
    }

    public void draw(MFGraphics g) {
        drawInMap(g, this.drawer);
    }

    public void logic() {
        if (this.type == TYPE_FLY) {
            this.posX += FLY_VELOCITY_X;
            this.posY += FLY_VELOCITY_Y;
        } else if (this.mObj != null) {
            this.mObj.logic();
            this.posX = this.mObj.getPosX();
            this.posY = this.mObj.getPosY();
            if (changeLayer && this.posY > this.startPosY && this.posY < this.startPosY + (COLLISION_HEIGHT/2)) {
                this.sourceLayer = (-this.sourceLayer) + 1;
                this.mObj.setLayer(this.sourceLayer);
                changeLayer = false;
            }
        }
        refreshCollisionRect(this.posX, this.posY);
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - (COLLISION_WIDTH/2), y - COLLISION_HEIGHT, COLLISION_WIDTH, COLLISION_HEIGHT);
    }

    public boolean checkDestroy() {
        return !isInCamera();
    }

    public void doWhileTouchGround(int vx, int vy) {
        if (this.type == TYPE_STAY) {
            this.mObj.doStop();
        } else if (this.type == TYPE_MOVE) {
            this.mObj.doJump(FLY_VELOCITY_X, ANIMAL_INIT_VEL_Y);
        }
    }

    public void close() {
        this.mObj = null;
        this.drawer = null;
    }

    public boolean hasSideCollision() {
        return false;
    }

    public boolean hasTopCollision() {
        return false;
    }

    public void doBeforeCollisionCheck() {
    }

    public void doWhileCollision(ACObject arg0, ACCollision arg1, int arg2, int arg3, int arg4, int arg5, int arg6) {
    }

    public boolean hasDownCollision() {
        return true;
    }

    public void doWhileToucRoof(int vx, int vy) {
    }

    public int getGravity() {
        return GRAVITY;
    }
}
