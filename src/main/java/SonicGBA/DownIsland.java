package SonicGBA;

import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;

/* compiled from: GimmickObject */
class DownIsland extends GimmickObject {
    private static int COLLISION_HEIGHT = BarHorbinV.COLLISION_WIDTH;
    private static int COLLISION_WIDTH = BarHorbinV.COLLISION_WIDTH;
    private static final int VELOCITY = 400;
    private static MFImage image;
    private int posYOriginal = this.posY;

    protected DownIsland(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        if (image != null) {
            return;
        }
        if (StageManager.getCurrentZoneId() != 4) {
            image = MFImage.createImage("/gimmick/down_island.png");
        } else {
            image = MFImage.createImage("/gimmick/drip_island_4.png");
        }
    }

    public void logic() {
        int preY = this.posY;
        if (player.isFootOnObject(this)) {
            if (player instanceof PlayerKnuckles) {
                ((PlayerKnuckles) player).setFloating(false);
            }
            this.posY += 400;
            checkWithMap(this.posX, preY, this.posX, this.posY);
            if (this.posY + (COLLISION_HEIGHT >> 1) >= getGroundY(this.posX, this.posY)) {
                this.posY = getGroundY(this.posX, this.posY) - (COLLISION_HEIGHT >> 1);
            }
        } else {
            this.posY -= 400;
            if (this.posY <= this.posYOriginal) {
                this.posY = this.posYOriginal;
            }
        }
        checkWithPlayer(this.posX, preY, this.posX, this.posY);
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - (COLLISION_WIDTH >> 1), y - (COLLISION_HEIGHT >> 1), COLLISION_WIDTH, COLLISION_HEIGHT);
    }

    public int getPaintLayer() {
        return 3;
    }

    public void draw(MFGraphics g) {
        drawInMap(g, image, 3);
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        switch (direction) {
            case 1:
                object.beStop(this.collisionRect.y0, 1, this);
                this.used = true;
                return;
            case 2:
            case 3:
                if (((object.faceDirection && direction == 3) || (!object.faceDirection && direction == 2)) && object.getCollisionRect().y1 < this.collisionRect.y1) {
                    object.beStop(this.collisionRect.y0, 1, this);
                    this.used = true;
                    return;
                }
                return;
            default:
                return;
        }
    }

    public static void releaseAllResource() {
        image = null;
    }
}
