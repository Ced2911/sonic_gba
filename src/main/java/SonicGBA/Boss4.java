package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import Lib.MyRandom;
import Lib.SoundSystem;
import com.sega.mobile.define.MDPhone;
import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;
import java.lang.reflect.Array;

/* compiled from: EnemyObject */
class Boss4 extends BossObject {
    private static final int ATTACK_FIRE = 3;
    private static final int ATTACK_INIT = 0;
    private static final int ATTACK_MOVE = 1;
    private static final int ATTACK_TIME_MAX = 32;
    private static final int ATTACK_WAIT = 2;
    private static final int BOSS_LAUGH_MAX = 16;
    private static final int BOSS_SHOW_END_POSX = 556544;
    private static final int FACE_HURT = 2;
    private static final int FACE_NORMAL = 0;
    private static final int FACE_SMILE = 1;
    private static final int FLY_TOP_OFFSETY = 9600;
    private static final int ICE_INTERVAL = 1280;
    private static final int MACHINE_ATTACK = 2;
    private static final int MACHINE_ATTACK_HURT = 3;
    private static final int MACHINE_BASE_HALF_HEIGHT = 1280;
    private static final int MACHINE_MOVE = 0;
    private static final int MACHINE_MOVE_HURT = 1;
    private static final int MACHINE_WAIT = 4;
    private static final int MACHINE_WAIT_HURT = 5;
    private static final int MOVE_INTERVAL = 2048;
    private static final int MOVE_SPEED = 256;
    private static final int SHOW_BOSS_END = 2;
    private static final int SHOW_BOSS_ENTER = 0;
    private static final int SHOW_BOSS_LAUGH = 1;
    private static final int SIDE_DOWN1 = 1758;
    private static final int SIDE_DOWN2 = 1808;
    private static final int SIDE_LEFT = 8496;
    private static final int SIDE_MIDDLE = 553984;
    private static final int SIDE_RIGHT = 8816;
    private static final int SIDE_UP = 1616;
    private static final int STATE_BROKEN = 3;
    private static final int STATE_ENTER_SHOW = 1;
    private static final int STATE_ESCAPE = 4;
    private static final int STATE_INIT = 0;
    private static final int STATE_PRO = 2;
    private static final int StartPosX = 548992;
    private static final int TOUCH_BOTTOM_CNT_MAX = 20;
    private static final int WAIT_TIME_MAX = 8;
    private static final int WATER_LEVEL_DROP_SPEED = 2;
    private static Animation boatAni = null;
    private static final int cnt_max = 30;
    private static Animation escapefaceAni = null;
    private static Animation faceAni = null;
    private static final int limitLeftIceX = 544384;
    private static final int limitLeftX = 545792;
    private static final int limitRightX = 562176;
    private static Animation machineAni;
    private static MFImage machineBase;
    private static Animation machinePartsAni;
    private int COLLISION_HEIGHT = 3648;
    private int COLLISION_WIDTH = BarHorbinV.COLLISION_WIDTH;
    private int WaitCnt;
    private int attack_cn;
    private int attack_distance;
    private int attack_step;
    private AnimationDrawer boatdrawer;
    private BossBroken bossbroken;
    private boolean direct = false;
    private int escape_v = 512;
    private AnimationDrawer escapefacedrawer;
    private AnimationDrawer faceDrawer;
    private int face_cnt;
    private int face_state;
    private int fly_end;
    private int fly_top;
    private int fly_top_range = 3840;
    public boolean isNoneIce = false;
    private int laugh_cn;
    private AnimationDrawer machineDrawer;
    private AnimationDrawer machinePartsdrawer;
    private int machine_cnt;
    private int machine_state;
    private int move_distance;
    private int move_velX;
    private int[][] parts_pos;
    private int[][] parts_v;
    private int posStartX;
    private int pro_machine_state;
    private int pro_machine_state2;
    private int show_step;
    private int state;
    private int touch_bottom_cnt;
    private int wait_cn;
    private int wait_cnt;
    private int wait_cnt_max = 10;
    private int water_level;

    protected Boss4(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        this.posX -= this.iLeft * 8;
        this.posY -= this.iTop * 8;
        this.posY += 1024;
        if (machineAni == null) {
            machineAni = new Animation("/animation/boss4_machine");
        }
        this.machineDrawer = machineAni.getDrawer(0, true, 0);
        if (faceAni == null) {
            faceAni = new Animation("/animation/boss4_face");
        }
        this.faceDrawer = faceAni.getDrawer(0, true, 0);
        machineBase = MFImage.createImage("/animation/boss4_base.png");
        if (machinePartsAni == null) {
            machinePartsAni = new Animation("/animation/boss4_parts");
        }
        this.machinePartsdrawer = machinePartsAni.getDrawer(0, true, 0);
        this.parts_pos = (int[][]) Array.newInstance(Integer.TYPE, new int[]{3, 2});
        this.parts_v = (int[][]) Array.newInstance(Integer.TYPE, new int[]{3, 2});
        if (boatAni == null) {
            boatAni = new Animation("/animation/pod_boat");
        }
        this.boatdrawer = boatAni.getDrawer(0, true, 0);
        if (escapefaceAni == null) {
            escapefaceAni = new Animation("/animation/pod_face");
        }
        this.escapefacedrawer = escapefaceAni.getDrawer(4, true, 0);
        this.state = 0;
        this.wait_cn = 0;
        this.attack_cn = 0;
        this.isNoneIce = false;
        setBossHP();
    }

    public static void releaseAllResource() {
        Animation.closeAnimation(machineAni);
        Animation.closeAnimation(faceAni);
        machineAni = null;
        faceAni = null;
    }

    public void close() {
        this.machineDrawer = null;
        this.faceDrawer = null;
        this.bossbroken = null;
    }

    private int facePosY() {
        switch (this.machineDrawer.getCurrentFrame()) {
            case 0:
            case 2:
                return 2752;
            case 1:
            case 3:
            case 4:
                return 2688;
            case 5:
                return 2816;
            default:
                return 2688;
        }
    }

    private void setAniState(AnimationDrawer aniDrawer, int state) {
        if (aniDrawer == this.faceDrawer) {
            this.face_state = state;
        }
        aniDrawer.setActionId(state);
        aniDrawer.setLoop(true);
    }

    private void setAniState(int machine_state, int face_state) {
        this.machineDrawer.setActionId(machine_state);
        this.machineDrawer.setLoop(true);
        this.faceDrawer.setActionId(face_state);
        this.faceDrawer.setLoop(true);
        this.face_state = face_state;
    }

    private void setMoveConf() {
        if (this.posX > SIDE_MIDDLE) {
            if (this.direct) {
                this.move_velX = GimmickObject.PLATFORM_OFFSET_Y;
            } else {
                this.move_velX = 256;
            }
        } else if (this.direct) {
            this.move_velX = 256;
        } else {
            this.move_velX = GimmickObject.PLATFORM_OFFSET_Y;
        }
        int point = MyRandom.nextInt(0, 100);
        if (point < 5) {
            this.move_distance = 1;
        } else if (point < 20) {
            this.move_distance = 2;
        } else if (point < 35) {
            this.move_distance = 3;
        } else if (point < 55) {
            this.move_distance = 4;
        } else if (point < 75) {
            this.move_distance = 5;
        } else if (point < 90) {
            this.move_distance = 6;
        } else {
            this.move_distance = 7;
        }
        setAniState(this.machineDrawer, 0);
        if (this.move_velX > 0) {
            this.machineDrawer.setTrans(2);
        } else {
            this.machineDrawer.setTrans(0);
        }
        this.posStartX = this.posX;
        this.attack_step = 1;
        this.wait_cn = 0;
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (this.dead || this.state != 2 || object != player) {
            return;
        }
        if (!(player instanceof PlayerTails) || (player.getCharacterAnimationID() != 12 && player.getCharacterAnimationID() != 13)) {
            if (player.isAttackingEnemy()) {
                if (this.HP > 0 && this.face_state != 2) {
                    this.HP--;
                    player.doBossAttackPose(this, direction);
                    setAniState(this.faceDrawer, 2);
                    this.pro_machine_state = this.machine_state;
                    this.machine_state = this.pro_machine_state + 1;
                    setAniState(this.machineDrawer, this.machine_state);
                    if (this.HP == 0) {
                        this.state = 3;
                        this.isNoneIce = true;
                        this.fly_top = this.posY + FLY_TOP_OFFSETY;
                        this.fly_end = 564224;
                        for (int i = 0; i < 3; i++) {
                            this.parts_pos[i][0] = this.posX;
                            this.parts_pos[i][1] = this.posY;
                            this.parts_v[i][0] = MyRandom.nextInt(0, 10) > 5 ? -640 : MDPhone.SCREEN_HEIGHT;
                            this.parts_v[i][1] = MyRandom.nextInt(0, 10) > 5 ? -320 : -512;
                        }
                        setAniState(this.faceDrawer, 1);
                        this.bossbroken = new BossBroken(25, this.posX >> 6, this.posY >> 6, 0, 0, 0, 0);
                        GameObject.addGameObject(this.bossbroken, this.posX >> 6, this.posY >> 6);
                        SoundSystem.getInstance().playSe(35, false);
                        return;
                    }
                    SoundSystem.getInstance().playSe(34, false);
                }
            } else if (this.machine_state != 1 && this.machine_state != 3 && this.machine_state != 5 && player.canBeHurt()) {
                player.beHurt();
                setAniState(this.faceDrawer, 1);
            }
        }
    }

    public void doWhileBeAttack(PlayerObject object, int direction, int animationID) {
        if (this.state == 2 && this.HP > 0) {
            if ((!(player instanceof PlayerTails) || (!(player.getCharacterAnimationID() == 12 || player.getCharacterAnimationID() == 13) || player.getVelY() <= 0)) && this.face_state != 2) {
                this.HP--;
                player.doBossAttackPose(this, direction);
                setAniState(this.faceDrawer, 2);
                this.pro_machine_state = this.machine_state;
                this.machine_state = this.pro_machine_state + 1;
                setAniState(this.machineDrawer, this.machine_state);
                if (this.HP == 0) {
                    this.state = 3;
                    this.isNoneIce = true;
                    this.fly_top = this.posY + FLY_TOP_OFFSETY;
                    this.fly_end = 564224;
                    for (int i = 0; i < 3; i++) {
                        this.parts_pos[i][0] = this.posX;
                        this.parts_pos[i][1] = this.posY;
                        this.parts_v[i][0] = MyRandom.nextInt(0, 10) > 5 ? -640 : MDPhone.SCREEN_HEIGHT;
                        this.parts_v[i][1] = MyRandom.nextInt(0, 10) > 5 ? -320 : -512;
                    }
                    setAniState(this.faceDrawer, 1);
                    this.bossbroken = new BossBroken(25, this.posX >> 6, this.posY >> 6, 0, 0, 0, 0);
                    GameObject.addGameObject(this.bossbroken, this.posX >> 6, this.posY >> 6);
                    SoundSystem.getInstance().playSe(35, false);
                    return;
                }
                SoundSystem.getInstance().playSe(34, false);
            }
        }
    }

    public void logic() {
        if (!this.dead) {
            int preX = this.posX;
            int preY = this.posY;
            if (this.state > 0) {
                isBossEnter = true;
            }
            switch (this.state) {
                case 0:
                    if (player.getFootPositionX() >= StartPosX) {
                        this.state = 1;
                        MapManager.setCameraLeftLimit(SIDE_LEFT);
                        MapManager.setCameraRightLimit(SIDE_RIGHT);
                        MapManager.setCameraUpLimit(SIDE_UP);
                        MapManager.setCameraDownLimit(SIDE_DOWN1);
                        if (!this.IsPlayBossBattleBGM) {
                            bossFighting = true;
                            bossID = 25;
                            SoundSystem.getInstance().playBgm(22, true);
                            this.IsPlayBossBattleBGM = true;
                        }
                        this.show_step = 0;
                        setAniState(0, 0);
                        player.setMeetingBoss(false);
                        this.water_level = StageManager.getWaterLevel();
                        if (player instanceof PlayerKnuckles) {
                            player.dripDownUnderWater();
                        }
                        player.isAttackBoss4 = true;
                        break;
                    }
                    break;
                case 1:
                    if (player instanceof PlayerKnuckles) {
                        player.dripDownUnderWater();
                    }
                    switch (this.show_step) {
                        case 0:
                            if (this.posX <= BOSS_SHOW_END_POSX) {
                                this.show_step = 1;
                                setAniState(4, 1);
                                this.laugh_cn = 0;
                                break;
                            }
                            this.posX -= 256;
                            break;
                        case 1:
                            if (this.laugh_cn >= 16) {
                                this.show_step = 2;
                                setAniState(this.faceDrawer, 0);
                                break;
                            }
                            this.laugh_cn++;
                            break;
                        case 2:
                            this.state = 2;
                            this.attack_step = 0;
                            this.wait_cn = 0;
                            MapManager.setCameraDownLimit(SIDE_DOWN2);
                            break;
                        default:
                            break;
                    }
                case 2:
                    if (MapManager.actualDownCameraLimit == MapManager.proposeDownCameraLimit) {
                        player.setMeetingBoss(true);
                    }
                    if (this.face_state != 0) {
                        if (this.face_cnt < 30) {
                            this.face_cnt++;
                        } else {
                            setAniState(this.faceDrawer, 0);
                            this.face_cnt = 0;
                        }
                    }
                    if (!(this.machine_state == 0 || this.machine_state == 2 || this.machine_state == 4)) {
                        this.pro_machine_state2 = this.machine_state;
                        if (this.machine_cnt < 30) {
                            this.machine_cnt++;
                        } else {
                            this.machine_state = this.pro_machine_state2 - 1;
                            setAniState(this.machineDrawer, this.machine_state);
                            this.machine_cnt = 0;
                        }
                    }
                    switch (this.attack_step) {
                        case 0:
                            if (this.wait_cn >= 8) {
                                this.direct = false;
                                if (MyRandom.nextInt(0, 100) <= 95) {
                                    this.direct = true;
                                } else {
                                    this.direct = false;
                                }
                                setMoveConf();
                                break;
                            }
                            this.wait_cn++;
                            break;
                        case 1:
                            if ((this.posStartX <= SIDE_MIDDLE || !this.direct || this.posX < this.posStartX - (this.move_distance * 2048)) && ((this.posStartX <= SIDE_MIDDLE || this.direct || this.posX > this.posStartX + (this.move_distance * 2048)) && ((this.posStartX > SIDE_MIDDLE || !this.direct || this.posX > this.posStartX + (this.move_distance * 2048)) && (this.posStartX > SIDE_MIDDLE || this.direct || this.posX < this.posStartX - (this.move_distance * 2048))))) {
                                if (this.posStartX > SIDE_MIDDLE) {
                                    if (this.direct) {
                                        this.posX = this.posStartX - (this.move_distance * 2048);
                                    } else {
                                        this.posX = this.posStartX + (this.move_distance * 2048);
                                    }
                                } else if (this.direct) {
                                    this.posX = this.posStartX + (this.move_distance * 2048);
                                } else {
                                    this.posX = this.posStartX - (this.move_distance * 2048);
                                }
                                this.attack_step = 2;
                                setAniState(this.machineDrawer, 4);
                            } else if (this.posX > 560128) {
                                this.posX = 560128;
                                this.attack_step = 2;
                                setAniState(this.machineDrawer, 4);
                            } else if (this.posX < limitLeftX) {
                                this.posX = limitLeftX;
                                this.attack_step = 2;
                                setAniState(this.machineDrawer, 4);
                            } else {
                                this.posX += this.move_velX;
                            }
                            this.wait_cn = 0;
                            break;
                        case 2:
                            if (this.move_distance == 1) {
                                if (this.wait_cn >= 16) {
                                    this.direct = false;
                                    if (MyRandom.nextInt(0, 100) >= 50) {
                                        this.direct = true;
                                    } else {
                                        this.direct = false;
                                    }
                                    setMoveConf();
                                    break;
                                }
                                this.wait_cn++;
                                break;
                            } else if (this.wait_cn >= 8) {
                                this.attack_step = 3;
                                setAniState(this.machineDrawer, 2);
                                this.attack_cn = 0;
                                this.wait_cn = 0;
                                MapManager.setShake(16);
                                break;
                            } else {
                                this.wait_cn++;
                                break;
                            }
                        case 3:
                            if (this.attack_cn >= 32) {
                                this.attack_step = 0;
                                setAniState(this.machineDrawer, 4);
                                this.attack_cn = 0;
                                this.wait_cn = 0;
                                break;
                            }
                            this.attack_cn++;
                            if (this.HP <= 4) {
                                if (this.HP <= 2) {
                                    switch (this.attack_cn) {
                                        case 6:
                                        case 13:
                                        case 22:
                                        case 29:
                                            GameObject.addGameObject(new Boss4Ice(limitLeftIceX + (MyRandom.nextInt(16) * 1280), 103424, MyRandom.nextInt(92, 148), 116352, this));
                                            SoundSystem.getInstance().playSe(36);
                                            break;
                                        default:
                                            break;
                                    }
                                }
                                switch (this.attack_cn) {
                                    case 6:
                                    case 16:
                                    case 25:
                                        GameObject.addGameObject(new Boss4Ice(limitLeftIceX + (MyRandom.nextInt(16) * 1280), 103424, MyRandom.nextInt(92, 148), 116352, this));
                                        SoundSystem.getInstance().playSe(36);
                                        break;
                                    default:
                                        break;
                                }
                            }
                            switch (this.attack_cn) {
                                case 6:
                                case 22:
                                    GameObject.addGameObject(new Boss4Ice(limitLeftIceX + (MyRandom.nextInt(16) * 1280), 103424, MyRandom.nextInt(92, 148), 116352, this));
                                    SoundSystem.getInstance().playSe(36);
                                    break;
                                default:
                                    break;
                            }
                        default:
                            break;
                    }
                case 3:
                    this.water_level += 2;
                    StageManager.setWaterLevel(this.water_level);
                    for (int i = 0; i < 3; i++) {
                        int[] iArr = this.parts_pos[i];
                        iArr[0] = iArr[0] + this.parts_v[i][0];
                        iArr = this.parts_v[i];
                        iArr[1] = iArr[1] + GRAVITY;
                        iArr = this.parts_pos[i];
                        iArr[1] = iArr[1] + this.parts_v[i][1];
                    }
                    if (this.posY >= getGroundY(this.posX, this.posY) - 1280) {
                        this.posY = getGroundY(this.posX, this.posY) - 1280;
                        this.touch_bottom_cnt++;
                        if (this.touch_bottom_cnt > 20) {
                            this.state = 4;
                            bossFighting = false;
                            SoundSystem.getInstance().playBgm(StageManager.getBgmId(), true);
                        }
                    } else {
                        this.posY = this.water_level << 6;
                    }
                    this.bossbroken.logicBoom(this.posX, this.posY);
                    break;
                case 4:
                    this.wait_cnt++;
                    if (this.wait_cnt >= this.wait_cnt_max && this.posY >= this.fly_top - this.fly_top_range) {
                        this.posY -= this.escape_v;
                    }
                    if (this.posY <= this.fly_top - this.fly_top_range && this.WaitCnt == 0) {
                        this.posY = this.fly_top - this.fly_top_range;
                        this.escapefacedrawer.setActionId(0);
                        this.boatdrawer.setActionId(1);
                        this.boatdrawer.setLoop(false);
                        this.WaitCnt = 1;
                    }
                    if (this.WaitCnt == 1 && this.boatdrawer.checkEnd()) {
                        this.escapefacedrawer.setActionId(0);
                        this.escapefacedrawer.setTrans(2);
                        this.escapefacedrawer.setLoop(true);
                        this.boatdrawer.setActionId(1);
                        this.boatdrawer.setTrans(2);
                        this.boatdrawer.setLoop(false);
                        this.WaitCnt = 2;
                    }
                    if (this.WaitCnt == 2 && this.boatdrawer.checkEnd()) {
                        this.boatdrawer.setActionId(0);
                        this.boatdrawer.setTrans(2);
                        this.boatdrawer.setLoop(true);
                        this.WaitCnt = 3;
                    }
                    if (this.WaitCnt == 3 || this.WaitCnt == 4) {
                        this.posX += this.escape_v;
                    }
                    if (this.posX - this.fly_end > this.fly_top_range && this.WaitCnt == 3) {
                        GameObject.addGameObject(new Cage((MapManager.getCamera().f13x + (MapManager.CAMERA_WIDTH >> 1)) << 6, (MapManager.getCamera().f14y + 40) << 6));
                        this.WaitCnt = 4;
                        break;
                    }
            }
            checkWithPlayer(preX, preY, this.posX, this.posY);
        }
    }

    public void draw(MFGraphics g) {
        if (!this.dead) {
            if (this.state != 3 && this.state != 4) {
                drawInMap(g, this.machineDrawer);
                if (this.face_state != 0) {
                    drawInMap(g, this.faceDrawer, this.posX, this.posY + facePosY());
                }
            } else if (this.state == 3) {
                if (this.bossbroken != null) {
                    this.bossbroken.draw(g);
                }
                this.machinePartsdrawer.setActionId(0);
                drawInMap(g, this.machinePartsdrawer, this.parts_pos[0][0], this.parts_pos[0][1]);
                this.machinePartsdrawer.setActionId(1);
                drawInMap(g, this.machinePartsdrawer, this.parts_pos[1][0], this.parts_pos[1][1]);
                drawInMap(g, this.machinePartsdrawer, this.parts_pos[2][0], this.parts_pos[2][1]);
                drawInMap(g, machineBase, this.posX, this.posY, 3);
                drawInMap(g, this.faceDrawer, this.posX, this.posY);
            } else if (this.state == 4) {
                drawInMap(g, this.boatdrawer, this.posX, this.posY - 960);
                drawInMap(g, this.escapefacedrawer, this.posX, this.posY - 2624);
            }
            drawCollisionRect(g);
        }
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - (this.COLLISION_WIDTH >> 1), y, this.COLLISION_WIDTH, this.COLLISION_HEIGHT);
    }
}
