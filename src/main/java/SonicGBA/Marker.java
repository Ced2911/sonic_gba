package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import State.GameState;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: GimmickObject */
class Marker extends GimmickObject {
    private static final int MARKER_HEIGHT = 3200;
    private static final int MARKER_HEIGHT_61 = 16384;
    private static final int MARKER_HEIGHT_61_2 = 2560;
    private static final int MARKER_WIDTH = 640;
    private static final int MARKER_WIDTH_61 = 9216;
    private static Animation markerAnimation;
    private int MarkID;
    private AnimationDrawer drawer;
    private boolean isStage61;

    public Marker(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        boolean z;
        this.MarkID = 0;
        this.used = false;
        if (markerAnimation == null) {
            markerAnimation = new Animation("/animation/se_marker");
        }
        this.drawer = markerAnimation.getDrawer(0, true, 0);
        this.MarkID = left;
        if (StageManager.getStageID() == 10) {
            z = true;
        } else {
            z = false;
        }
        this.isStage61 = z;
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (object instanceof PlayerKnuckles) {
            ((PlayerKnuckles) object).dripDownUnderWater();
        }
        if (!this.used) {
            PlayerObject playerObject = player;
            if (PlayerObject.currentMarkId < this.iLeft) {
                this.used = true;
                this.drawer.setActionId(1);
                this.drawer.setLoop(false);
                if (stageModeState == 0) {
                    StageManager.saveCheckPoint(this.posX, this.posY);
                }
                if (!this.isStage61) {
                    PlayerObject.currentMarkId = this.iLeft;
                    if (!GameState.PreGame) {
                        soundInstance.playSe(42);
                    }
                } else if (player.faceDegree == 0) {
                    PlayerObject.currentMarkId = this.iLeft;
                    RocketSeparateEffect.getInstance().init(this.iLeft - 1);
                } else {
                    this.used = false;
                }
            }
        }
    }

    public void draw(MFGraphics g) {
        if (StageManager.getStageID() != 10) {
            drawInMap(g, this.drawer, this.posX, this.posY);
            if (this.drawer.checkEnd()) {
                this.drawer.setActionId(2);
                this.drawer.setLoop(true);
            }
            drawCollisionRect(g);
        }
    }

    public void logic() {
        if (!this.used) {
            PlayerObject playerObject = player;
            if (PlayerObject.currentMarkId >= this.iLeft) {
                this.used = true;
                this.drawer.setActionId(2);
                this.drawer.setLoop(true);
                playerObject = player;
                if (PlayerObject.currentMarkId == this.iLeft && StageManager.getStageID() == 10) {
                    RocketSeparateEffect.getInstance().functionSecond(this.iLeft - 1);
                }
            }
        }
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 320, y - MARKER_HEIGHT, 640, MARKER_HEIGHT);
        if (StageManager.getStageID() == 10) {
            switch (this.iLeft - 1) {
                case 0:
                    this.collisionRect.setRect(x - 320, y - 16384, 640, 16384);
                    return;
                default:
                    this.collisionRect.setRect(x - 4608, y - MARKER_HEIGHT, MARKER_WIDTH_61, 2560);
                    return;
            }
        }
    }

    public void close() {
        this.drawer = null;
    }

    public int getPaintLayer() {
        return 0;
    }

    public static void releaseAllResource() {
        Animation.closeAnimation(markerAnimation);
        markerAnimation = null;
    }
}
