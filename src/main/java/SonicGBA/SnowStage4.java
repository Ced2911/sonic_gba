package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import Lib.MyAPI;
import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;

/* compiled from: BackGroundManager */
class SnowStage4 extends BackGroundManager {
    private static int IMAGE_HEIGHT = 256;
    private static int IMAGE_WIDTH = 256;
    private static int SPEED_X = -1;
    private static int SPEED_Y = 4;
    private static int posX = 0;
    private static int posY = 0;
    private AnimationDrawer snowDrawer;
    private MFImage waterImage;
    private AnimationDrawer waterSurface;

    public SnowStage4() {
        try {
            this.snowDrawer = new Animation("/map/snow").getDrawer(0, true, 0);
            this.snowDrawer.setPause(true);
            this.waterImage = MFImage.createImage("/water/water_filter.png");
            this.waterSurface = new Animation("/water/stage6_water_surface").getDrawer(0, true, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void close() {
        this.snowDrawer = null;
        this.waterImage = null;
    }

    public void draw(MFGraphics g) {
        if (!GameObject.IsGamePause) {
            posX += SPEED_X;
            posX %= IMAGE_WIDTH;
            posY += SPEED_Y;
            posY %= IMAGE_HEIGHT;
        }
        drawWater(g);
    }

    public void drawWater(MFGraphics g) {
        int cameraX = MapManager.getCamera().f13x;
        int cameraY = MapManager.getCamera().f14y;
        int waterLevel = StageManager.getWaterLevel();
        int x;
        int waterStartY = waterLevel;
        if (waterLevel > 0 && cameraY > waterLevel) {
            waterStartY = cameraY;
        }
        for (int y = waterStartY; y < SCREEN_HEIGHT + cameraY; y += 96) {
            for (x = 0; x < SCREEN_WIDTH; x += 96) {
                MyAPI.drawImage(g, this.waterImage, x, y - cameraY, 20);
            }
        }
        if (waterLevel > cameraY - 2 && waterLevel < (SCREEN_HEIGHT + cameraY) + 2) {
            for (x = 0; x < SCREEN_WIDTH; x++) {
                this.waterSurface.draw(g, x, waterLevel - cameraY);
            }
        }
        if (cameraY < waterLevel) {
            g.setClip(0, 0, SCREEN_WIDTH, waterLevel - cameraY);
            for (int i = (cameraY / IMAGE_HEIGHT) - 1; i < (cameraY / IMAGE_HEIGHT) + 3; i++) {
                for (int j = cameraX / IMAGE_WIDTH; j < (cameraX / IMAGE_WIDTH) + 4; j++) {
                    this.snowDrawer.draw(g, (posX + (IMAGE_WIDTH * j)) - cameraX, (posY + (IMAGE_HEIGHT * i)) - cameraY);
                }
            }
            if (!GameObject.IsGamePause) {
                this.snowDrawer.moveOn();
            }
            g.setClip(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
        }
    }
}
