package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import Lib.SoundSystem;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: GimmickObject */
class Balloon extends GimmickObject {
    private static final int COLLISION_HEIGHT = 1600;
    private static final int COLLISION_WIDTH = 1408;
    private static final int POP_POWER = 1600;
    private static Animation balloonAnimation;
    private AnimationDrawer drawer;

    protected Balloon(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        if (balloonAnimation == null) {
            balloonAnimation = new Animation("/animation/balloon");
        }
        this.drawer = balloonAnimation.getDrawer((Math.abs(this.iLeft) % 3) * 2, true, 0);
    }

    public void draw(MFGraphics g) {
        drawInMap(g, this.drawer);
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (!this.used) {
            player.bePop(1600, 1);
            this.used = true;
            this.drawer.setActionId(((Math.abs(this.iLeft) % 3) * 2) + 1);
            this.drawer.setLoop(false);
            SoundSystem.getInstance().playSe(57);
        }
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - BarHorbinV.COLLISION_HEIGHT, y - 1600, COLLISION_WIDTH, 1600);
    }

    public void close() {
        this.drawer = null;
    }

    public static void releaseAllResource() {
        Animation.closeAnimation(balloonAnimation);
        balloonAnimation = null;
    }

    public void doInitWhileInCamera() {
        this.used = false;
        this.drawer.setActionId((Math.abs(this.iLeft) % 3) * 2);
        this.drawer.setLoop(true);
    }

    public int getPaintLayer() {
        return 0;
    }
}
