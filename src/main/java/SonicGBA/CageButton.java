package SonicGBA;

import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;

/* compiled from: GimmickObject */
class CageButton extends GimmickObject {
    private static final int COLLISION_HEIGHT = 896;
    private static final int COLLISION_WIDTH = 2176;
    private static final int PUSH_OFFSET_Y = 320;
    private static MFImage cageButtonImage = null;
    private int posOriginalY;

    protected CageButton(int x, int y) {
        super(0, x, y, 0, 0, 0, 0);
        if (cageButtonImage == null) {
            try {
                cageButtonImage = MFImage.createImage("/gimmick/cage_button.png");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void logic() {
        if (this.used) {
            checkWithPlayer(this.posX, this.posY, this.posX, this.posOriginalY + PUSH_OFFSET_Y);
            this.posY = this.posOriginalY + PUSH_OFFSET_Y;
            return;
        }
        checkWithPlayer(this.posX, this.posY, this.posX, this.posY);
    }

    public void drawButton(MFGraphics g) {
        drawInMap(g, cageButtonImage, 33);
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 1088, y - COLLISION_HEIGHT, 2176, COLLISION_HEIGHT);
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if ((object instanceof PlayerKnuckles) && object.myAnimationID >= 19 && object.myAnimationID <= 22) {
            object.beStop(0, direction, this);
        } else if (!this.used) {
            if (direction != 0) {
                object.beStop(0, direction, this);
            }
            if (direction == 1 && !this.used) {
                this.used = true;
                isUnlockCage = true;
                this.posOriginalY = this.posY;
                object.setCelebrate();
            }
        }
    }

    public void doWhileBeAttack(PlayerObject object, int direction, int animationID) {
        if ((object instanceof PlayerAmy) && object.myAnimationID != 7 && !this.used) {
            boolean z;
            this.used = true;
            isUnlockCage = true;
            this.posOriginalY = this.posY;
            object.setCelebrate();
            if (object.faceDirection) {
                z = false;
            } else {
                z = true;
            }
            object.faceDirection = z;
        }
    }

    public void close() {
    }

    public static void releaseAllResource() {
        cageButtonImage = null;
    }
}
