package SonicGBA;

import Lib.Line;
import Lib.MyAPI;
import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;

/* compiled from: GimmickObject */
class CornerHobin extends GimmickObject {
    private static final int COLLISION_HEIGHT = 2816;
    private static final int COLLISION_WIDTH = 2816;
    private static final int POP_POWER = 1152;
    private static MFImage barCornerImage;
    private Line checkLine;
    private int funcDirectionH;
    private int funcDirectionV;
    private HobinCal hobinCal;
    private int hobinDegree;
    private int trans;

    protected CornerHobin(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        if (barCornerImage == null) {
            try {
                barCornerImage = MFImage.createImage("/gimmick/barCorner.png");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        this.hobinDegree = -45;
        this.trans = 0;
        this.funcDirectionH = 2;
        if (this.iLeft == 0) {
            this.funcDirectionH = 3;
            this.trans |= 2;
            this.hobinDegree = RollPlatformSpeedC.DEGREE_VELOCITY - this.hobinDegree;
        }
        this.funcDirectionV = 1;
        if (this.iTop == 0) {
            this.funcDirectionV = 0;
            this.trans |= 1;
            this.hobinDegree = -this.hobinDegree;
        }
        this.hobinCal = new HobinCal();
    }

    public void draw(MFGraphics g) {
        drawInMap(g, barCornerImage, 0, 0, MyAPI.zoomIn(barCornerImage.getWidth()), MyAPI.zoomIn(barCornerImage.getHeight()), this.trans, this.posX + this.hobinCal.getPosOffsetX(), this.posY + this.hobinCal.getPosOffsetY(), (this.funcDirectionH == 2 ? 4 : 8) | (this.funcDirectionV == 1 ? 32 : 16));
        this.hobinCal.logic();
    }

    public void refreshCollisionRect(int x, int y) {
        int i;
        CollisionRect collisionRect = this.collisionRect;
        int i2 = x - (this.funcDirectionH == 3 ? 2816 : 0);
        if (this.funcDirectionV == 1) {
            i = 2816;
        } else {
            i = 0;
        }
        collisionRect.setRect(i2, y - i, 2816, 2816);
        if (this.checkLine == null) {
            this.checkLine = new Line();
        }
        if ((this.funcDirectionH == 2 && this.funcDirectionV == 1) || (this.funcDirectionH == 3 && this.funcDirectionV == 0)) {
            this.checkLine.setProperty(this.collisionRect.x0, this.collisionRect.y0, this.collisionRect.x1, this.collisionRect.y1);
        } else {
            this.checkLine.setProperty(this.collisionRect.x0, this.collisionRect.y1, this.collisionRect.x1, this.collisionRect.y0);
        }
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (direction == 4 && object == player) {
            boolean touched = false;
            if (this.funcDirectionV == 1) {
                if (this.checkLine.getY(player.getFootPositionX()) < player.getFootPositionY()) {
                    player.setFootPositionY(this.checkLine.getY(player.getFootPositionX()));
                    touched = true;
                }
            } else if (this.checkLine.getY(player.getFootPositionX()) > player.getHeadPositionY()) {
                player.setHeadPositionY(this.checkLine.getY(player.getFootPositionX()));
                touched = true;
            }
            if (touched) {
                player.bePop(1152, this.funcDirectionV);
                player.bePop(1152, this.funcDirectionH);
                player.dashRolling = false;
                this.hobinCal.startHobin(0, this.hobinDegree + RollPlatformSpeedC.DEGREE_VELOCITY, 10);
                soundInstance.playSe(24);
            }
        }
    }

    public void close() {
        this.checkLine = null;
    }

    public static void releaseAllResource() {
        barCornerImage = null;
    }

    public int getPaintLayer() {
        return 0;
    }
}
