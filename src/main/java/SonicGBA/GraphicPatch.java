package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import Lib.MyAPI;
import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;

/* compiled from: GimmickObject */
class GraphicPatch extends GimmickObject {
    private static final int HOLE_PATCH_HEIGHT = 48;
    private static final int HOLE_PATCH_OFFSET_X = -32;
    private static final int HOLE_PATCH_OFFSET_Y = -48;
    private static final int HOLE_PATCH_WIDTH = 56;
    private static final int MAP_PATCH_HEIGHT = 12288;
    private static final int MAP_PATCH_OFFSET_X = -6144;
    private static final int MAP_PATCH_OFFSET_Y = -2048;
    private static final int MAP_PATCH_WIDTH = 12288;
    private static final int RAIL_SPACE = 1024;
    private static Animation holePatchAnimation;
    private static Animation mapPatchAnimation;
    private static MFImage poalImage;
    private static AnimationDrawer railPatchDrawer;
    private static MFImage ropeImage;
    private int direction;
    private AnimationDrawer drawer;
    private int moveDistance;

    protected GraphicPatch(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        switch (this.iLeft) {
            case 0:
                if (WaterFall.waterFallDrawer3 == null) {
                    WaterFall.waterFallDrawer3 = new Animation("/animation/sand_03").getDrawer(0, true, 0);
                    WaterFall.waterFallDrawer3.setPause(true);
                }
                this.drawer = WaterFall.waterFallDrawer3;
                return;
            case 1:
                if (TransPoint.caveAnimation == null) {
                    TransPoint.caveAnimation = new Animation("/animation/cave");
                }
                this.drawer = TransPoint.caveAnimation.getDrawer(1, true, 0);
                return;
            case 2:
                if (ropeImage == null) {
                    ropeImage = MFImage.createImage("/gimmick/rope_patch.png");
                    return;
                }
                return;
            case 3:
                if (mapPatchAnimation == null) {
                    mapPatchAnimation = new Animation(MapManager.image, "/animation/patch_st" + StageManager.getCurrentZoneId());
                }
                this.drawer = mapPatchAnimation.getDrawer(this.iTop, false, 0);
                this.mWidth = 12288;
                this.mHeight = 12288;
                return;
            case 4:
                if (holePatchAnimation == null) {
                    holePatchAnimation = new Animation("/animation/hole_patch");
                }
                this.drawer = holePatchAnimation.getDrawer(this.iTop, false, 0);
                this.posX -= 32;
                this.posY += this.iTop == 0 ? -48 : -512;
                this.mWidth = 56;
                this.mHeight = 48;
                return;
            case 5:
                if (poalImage == null) {
                    poalImage = MFImage.createImage("/gimmick/poal_patch.png");
                    return;
                }
                return;
            case 6:
                if (railPatchDrawer == null) {
                    railPatchDrawer = new Animation("/animation/rail_patch_2").getDrawer();
                }
                this.direction = width;
                System.out.println("6 height:" + height);
                this.moveDistance = height;
                return;
            default:
                return;
        }
    }

    public void draw(MFGraphics g) {
        int i;
        switch (this.iLeft) {
            case 0:
                MyAPI.setClip(g, (this.collisionRect.x0 >> 6) - camera.f13x, (this.collisionRect.y0 >> 6) - camera.f14y, this.collisionRect.getWidth() >> 6, this.collisionRect.getHeight() >> 6);
                for (i = this.collisionRect.y0; i < this.collisionRect.y1; i += 6144) {
                    drawInMap(g, this.drawer, this.posX, i);
                }
                MyAPI.setClip(g, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
                return;
            case 1:
                drawInMap(g, this.drawer);
                return;
            case 2:
                i = this.collisionRect.y0;
                int j = 0;
                while (i < this.collisionRect.y1) {
                    if (this.iTop == 0) {
                        drawInMap(g, ropeImage, this.posX + j, i, 20);
                    } else {
                        drawInMap(g, ropeImage, 0, 0, MyAPI.zoomIn(ropeImage.getWidth(), true), MyAPI.zoomIn(ropeImage.getHeight(), true), 2, (this.posX - j) + this.mWidth, i, 24);
                    }
                    i += 512;
                    j += 1024;
                }
                return;
            case 3:
                drawInMap(g, this.drawer, this.posX, this.posY);
                return;
            case 4:
                drawInMap(g, this.drawer, this.posX - -32, this.posY - (this.iTop == 0 ? -48 : 0));
                return;
            case 5:
                MyAPI.setClip(g, (this.collisionRect.x0 >> 6) - camera.f13x, (this.collisionRect.y0 >> 6) - camera.f14y, 512, this.collisionRect.getHeight() >> 6);
                for (int j2 = this.posY; j2 < this.posY + this.collisionRect.getHeight(); j2 += 1024) {
                    drawInMap(g, poalImage, this.posX + 128, j2, 17);
                }
                MyAPI.setClip(g, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
                return;
            case 6:
                int endX = this.posX + (RollIsland.DIRECTION[this.direction][0] * ((this.moveDistance / 1024) * 1024));
                int endY = this.posY + (RollIsland.DIRECTION[this.direction][1] * ((this.moveDistance / 1024) * 1024));
                int startX = this.posX;
                int startY = this.posY;
                if (this.posX > endX) {
                    startX = endX;
                    startY = endY;
                    endX = this.posX;
                    endY = this.posY;
                } else if (this.posY > endY && RollIsland.DIRECTION[this.direction][1] * RollIsland.DIRECTION[this.direction][0] != -1) {
                    startX = endX;
                    startY = endY;
                    endX = this.posX;
                    endY = this.posY;
                }
                if (startX == endX) {
                    MyAPI.setClip(g, 0, (startY >> 6) - camera.f14y, SCREEN_WIDTH, Math.abs(endY - startY) >> 6);
                } else if (startY == endY) {
                    MyAPI.setClip(g, (startX >> 6) - camera.f13x, 0, Math.abs(endX - startX) >> 6, SCREEN_HEIGHT);
                } else {
                    MyAPI.setClip(g, ((startX >> 6) - camera.f13x) - 2, ((Math.min(startY, endY) >> 6) - camera.f14y) - 2, (Math.abs(endX - startX) >> 6) + 4, (Math.abs(endY - startY) >> 6) + 4);
                }
                for (i = 0; i < this.moveDistance; i += 1024) {
                    int x;
                    int y;
                    if (RollIsland.DIRECTION[this.direction][1] * RollIsland.DIRECTION[this.direction][0] == -1) {
                        x = startX + (RollIsland.DIRECTION[this.direction][0] * i);
                        y = startY + (RollIsland.DIRECTION[this.direction][1] * i);
                    } else {
                        x = startX + (RollIsland.DIRECTION[this.direction][0] != 0 ? i : 0);
                        y = startY + (RollIsland.DIRECTION[this.direction][1] != 0 ? i : 0);
                    }
                    switch (this.direction) {
                        case 0:
                        case 4:
                            railPatchDrawer.setActionId(1);
                            break;
                        case 1:
                        case 5:
                            railPatchDrawer.setActionId(7);
                            break;
                        case 2:
                        case 6:
                            railPatchDrawer.setActionId(0);
                            break;
                        case 3:
                        case 7:
                            railPatchDrawer.setActionId(6);
                            break;
                        default:
                            break;
                    }
                    drawInMap(g, railPatchDrawer, x, y);
                }
                MyAPI.setClip(g, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
                switch (this.direction) {
                    case 0:
                    case 4:
                        railPatchDrawer.setActionId(5);
                        drawInMap(g, railPatchDrawer, startX, startY);
                        railPatchDrawer.setActionId(4);
                        drawInMap(g, railPatchDrawer, endX, endY);
                        return;
                    case 1:
                    case 5:
                        railPatchDrawer.setActionId(10);
                        drawInMap(g, railPatchDrawer, startX, startY);
                        railPatchDrawer.setActionId(11);
                        drawInMap(g, railPatchDrawer, endX, endY);
                        return;
                    case 2:
                    case 6:
                        railPatchDrawer.setActionId(3);
                        drawInMap(g, railPatchDrawer, startX, startY);
                        railPatchDrawer.setActionId(2);
                        drawInMap(g, railPatchDrawer, endX, endY);
                        return;
                    case 3:
                    case 7:
                        railPatchDrawer.setActionId(8);
                        drawInMap(g, railPatchDrawer, startX, startY);
                        railPatchDrawer.setActionId(9);
                        drawInMap(g, railPatchDrawer, endX, endY);
                        return;
                    default:
                        return;
                }
            default:
                return;
        }
    }

    public int getPaintLayer() {
        switch (this.iLeft) {
            case 1:
            case 6:
                return 0;
            case 3:
                if (this.iTop == 1) {
                    return 2;
                }
                break;
        }
        return super.getPaintLayer();
    }

    public void refreshCollisionRect(int x, int y) {
        switch (this.iLeft) {
            case 3:
                this.collisionRect.setRect(x + MAP_PATCH_OFFSET_X, y + MAP_PATCH_OFFSET_Y, this.mWidth, this.mHeight);
                return;
            case 6:
                int endX = this.posX + (RollIsland.DIRECTION[this.direction][0] * this.moveDistance);
                int endY = this.posY + (RollIsland.DIRECTION[this.direction][1] * this.moveDistance);
                int startX = this.posX;
                int startY = this.posY;
                this.collisionRect.setRect(Math.min(startX, endX), Math.min(startY, endY), Math.abs(startX - endX), Math.abs(startY - endY));
                return;
            default:
                this.collisionRect.setRect(x, y, this.mWidth, this.mHeight);
                return;
        }
    }

    public void close() {
        this.drawer = null;
    }

    public static void releaseAllResource() {
        Animation.closeAnimation(mapPatchAnimation);
        Animation.closeAnimation(holePatchAnimation);
        Animation.closeAnimationDrawer(railPatchDrawer);
        ropeImage = null;
        mapPatchAnimation = null;
        holePatchAnimation = null;
        poalImage = null;
        railPatchDrawer = null;
    }
}
