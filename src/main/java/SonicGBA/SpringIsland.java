package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: GimmickObject */
class SpringIsland extends GimmickObject {
    private static final int COLLISION_HEIGHT = 2688;
    private static final int COLLISION_WIDTH = 1920;
    private static final int COLLSION_OFFSET_Y = 1856;
    private static int SPRING_POWER = Spring.SPRING_POWER[0];
    private static Animation animation;
    private int debugCollisionHeight = 0;
    private AnimationDrawer drawer;
    private int initPos;
    private boolean isH;
    private MoveCalculator moveCal;
    private int offset_distance;

    protected SpringIsland(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        boolean moveDirection;
        if (animation == null) {
            if (StageManager.getCurrentZoneId() == 4 || StageManager.getCurrentZoneId() == 6) {
                animation = new Animation("/animation/spring_island" + StageManager.getCurrentZoneId());
            } else {
                animation = new Animation("/animation/spring_island");
            }
        }
        this.drawer = animation.getDrawer(0, false, 0);
        if (this.mWidth >= this.mHeight) {
            this.isH = true;
            if (this.iLeft == 0) {
                moveDirection = false;
            } else {
                moveDirection = true;
            }
        } else {
            this.isH = false;
            if (this.iTop == 0) {
                moveDirection = false;
            } else {
                moveDirection = true;
            }
        }
        this.initPos = this.isH ? this.posX : this.posY;
        if (this.posX == 69632 && this.posY == 35328) {
            this.debugCollisionHeight = 3200;
        }
        this.moveCal = new MoveCalculator(this.isH ? this.posX : this.posY, this.isH ? this.mWidth : this.mHeight, moveDirection);
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 960, (y + COLLSION_OFFSET_Y) - COLLISION_HEIGHT, COLLISION_WIDTH, this.debugCollisionHeight == 0 ? COLLISION_HEIGHT : this.debugCollisionHeight);
    }

    public void draw(MFGraphics g) {
        SPRING_POWER = player.isInWater ? Spring.SPRING_INWATER_POWER[0] : Spring.SPRING_POWER[0];
        drawInMap(g, this.drawer);
        if (this.drawer.checkEnd() && this.drawer.getActionId() == 1) {
            this.drawer.setActionId(0);
        }
    }

    public void doWhileCollision(PlayerObject obj, int direction) {
        if (direction == 2) {
            obj.beStop(0, direction, this);
        } else if (direction == 3) {
            obj.beStop(0, direction, this);
        } else if (direction == 4) {
            if (obj.animationID == 9) {
                return;
            }
            if (obj.velX < 0) {
                obj.beStop(0, 2, this);
            } else if (obj.velX > 0) {
                obj.beStop(0, 3, this);
            }
        } else if (direction == 1) {
            obj.beSpring(SPRING_POWER, direction);
            this.drawer.setActionId(1);
            soundInstance.playSe(37);
        } else if (direction == 0) {
            obj.beStop(0, direction, this);
        }
    }

    public void doWhileBeAttack(PlayerObject obj, int direction, int animationID) {
        if (PlayerObject.getCharacterID() == 3) {
            if (obj.myAnimationID >= 6 && obj.myAnimationID <= 7) {
                return;
            }
            if (direction == 4 || direction == 1) {
                obj.beSpring((SPRING_POWER * 13) / 10, 1);
                ((PlayerAmy) obj).skipBeStop = true;
                this.drawer.setActionId(1);
                soundInstance.playSe(37);
            }
        }
    }

    public void logic() {
        int preX = this.posX;
        int preY = this.posY;
        if (this.isH) {
            if (this.iLeft == 0) {
                this.posX = this.moveCal.getPosition();
            } else {
                this.offset_distance = this.initPos - this.moveCal.getPosition();
                this.posX = this.initPos + this.offset_distance;
            }
        } else if (this.iTop == 0) {
            this.posY = this.moveCal.getPosition();
        } else {
            this.offset_distance = this.initPos - this.moveCal.getPosition();
            this.posY = this.initPos + this.offset_distance;
        }
        checkWithPlayer(preX, preY, this.posX, this.posY);
    }

    public void close() {
        this.drawer = null;
    }

    public static void releaseAllResource() {
        Animation.closeAnimation(animation);
        animation = null;
    }
}
