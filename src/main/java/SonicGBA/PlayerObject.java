package SonicGBA;

import Common.NumberDrawer;
import GameEngine.Def;
import GameEngine.Key;
import Lib.Animation;
import Lib.AnimationDrawer;
import Lib.Coordinate;
import Lib.Direction;
import Lib.Line;
import Lib.MyAPI;
import Lib.MyRandom;
import Lib.SoundSystem;
import Lib.crlFP32;
import Special.SSDef;
import State.GameState;
import State.State;
import State.StringIndex;
import com.sega.engine.action.ACBlock;
import com.sega.engine.action.ACCollision;
import com.sega.engine.action.ACObject;
import com.sega.engine.action.ACUtilities;
import com.sega.engine.action.ACWorldCalUser;
import com.sega.engine.action.ACWorldCollisionCalculator;
import com.sega.mobile.define.MDPhone;
import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;
import java.lang.reflect.Array;
import java.util.Vector;

public abstract class PlayerObject extends MoveObject implements Focusable, ACWorldCalUser {
    private static final int f23A = 3072;
    private static final String ANIMATION_PATH = "/animation";
    public static final int ANI_ATTACK_1 = 18;
    public static final int ANI_ATTACK_2 = 19;
    public static final int ANI_ATTACK_3 = 20;
    public static final int ANI_BANK_1 = 32;
    public static final int ANI_BANK_2 = 33;
    public static final int ANI_BANK_3 = 34;
    public static final int ANI_BAR_ROLL_1 = 22;
    public static final int ANI_BAR_ROLL_2 = 23;
    private static final int ANI_BIG_ZERO = 67;
    public static final int ANI_BRAKE = 17;
    public static final int ANI_BREATHE = 49;
    public static final int ANI_CAUGHT = 52;
    public static final int ANI_CELEBRATE_1 = 35;
    public static final int ANI_CELEBRATE_2 = 36;
    public static final int ANI_CELEBRATE_3 = 37;
    public static final int ANI_CLIFF_1 = 47;
    public static final int ANI_CLIFF_2 = 48;
    public static final int ANI_DEAD = 41;
    public static final int ANI_DEAD_PRE = 45;
    public static final int ANI_FALLING = 10;
    public static final int ANI_HURT = 12;
    public static final int ANI_HURT_PRE = 44;
    public static final int ANI_JUMP = 4;
    public static final int ANI_JUMP_ROLL = 15;
    public static final int ANI_JUMP_RUSH = 16;
    public static final int ANI_LOOK_UP_1 = 38;
    public static final int ANI_LOOK_UP_2 = 39;
    public static final int ANI_LOOK_UP_OVER = 40;
    public static final int ANI_NONE = -1;
    public static final int ANI_POAL_PULL = 13;
    public static final int ANI_POAL_PULL_2 = 31;
    public static final int ANI_POP_JUMP_DOWN_SLOW = 43;
    public static final int ANI_POP_JUMP_UP = 14;
    public static final int ANI_POP_JUMP_UP_SLOW = 42;
    public static final int ANI_PULL = 24;
    public static final int ANI_PULL_BAR_MOVE = 28;
    public static final int ANI_PULL_BAR_STAY = 27;
    public static final int ANI_PUSH_WALL = 8;
    public static final int ANI_RAIL_ROLL = 21;
    public static final int ANI_ROPE_ROLL_1 = 25;
    public static final int ANI_ROPE_ROLL_2 = 26;
    public static final int ANI_ROTATE_JUMP = 9;
    public static final int ANI_RUN_1 = 1;
    public static final int ANI_RUN_2 = 2;
    public static final int ANI_RUN_3 = 3;
    public static final int ANI_SLIP = 11;
    private static final int ANI_SMALL_ZERO = 27;
    private static final int ANI_SMALL_ZERO_Y = 37;
    public static final int ANI_SPIN_LV1 = 6;
    public static final int ANI_SPIN_LV2 = 7;
    public static final int ANI_SQUAT = 5;
    public static final int ANI_SQUAT_PROCESS = 46;
    public static final int ANI_STAND = 0;
    public static final int ANI_VS_FAKE_KNUCKLE = 53;
    public static final int ANI_WAITING_1 = 50;
    public static final int ANI_WAITING_2 = 51;
    public static final int ANI_WIND_JUMP = 29;
    public static final int ANI_YELL = 30;
    private static final int ASPIRATE_INTERVAL = 3;
    public static final int ATTACK_POP_POWER = (GRAVITY + 774);
    private static final int ATTRACT_EFFECT_HEIGHT = 9600;
    private static final int ATTRACT_EFFECT_WIDTH = 9600;
    private static final int BACKGROUND_WIDTH = 80;
    public static final int BALL_HEIGHT_OFFSET = 1024;
    public static final int BANKING_MIN_SPEED = 500;
    public static int BANK_BRAKE_SPEED_LIMIT = 1100;
    private static final int BAR_COLOR = 2;
    private static final int BG_NUM = (((SCREEN_WIDTH + 80) - 1) / 80);
    public static final int BIG_NUM = 2;
    private static final int BODY_OFFSET = 768;
    private static final int BREATHE_IMAGE_HEIGHT = 16;
    private static final int BREATHE_IMAGE_WIDTH = 16;
    private static final int BREATHE_TIME_COUNT = 21000;
    private static final int BREATHE_TO_DIE_PER_COUNT = 1760;
    private static final int B_1 = 5760;
    private static final int B_2 = 11264;
    private static final int f24C = 3072;
    private static final int CAMERA_MAX_DISTANCE = 20;
    public static final boolean CAN_BE_SQUEEZE = true;
    private static final int CENTER_X = 660480;
    private static final int CENTER_Y = 63488;
    public static final int CHARACTER_AMY = 3;
    public static final int CHARACTER_KNUCKLES = 2;
    public static final int CHARACTER_SONIC = 0;
    public static final int CHARACTER_TAILS = 1;
    public static final int[] CHARACTER_LIST  = new int[] {CHARACTER_SONIC, CHARACTER_TAILS, CHARACTER_KNUCKLES, CHARACTER_AMY};
    public static final byte COLLISION_STATE_IN_SAND = (byte) 3;
    public static final byte COLLISION_STATE_JUMP = (byte) 1;
    public static final byte COLLISION_STATE_NONE = (byte) 4;
    public static final byte COLLISION_STATE_NUM = (byte) 4;
    public static final byte COLLISION_STATE_ON_OBJECT = (byte) 2;
    public static final byte COLLISION_STATE_WALK = (byte) 0;
    private static final int COUNT_INDEX = 1;
    private static final boolean DEBUG_WUDI = false;
    private static final int[] DEGREE_DIVIDE = new int[]{44, 75, 105, Def.TOUCH_HELP_HEIGHT, 224, 255, 285, 316, MDPhone.SCREEN_WIDTH};
    public static final int DETECT_HEIGHT = 2048;
    private static final int DIE_DRIP_STATE_JUMP_V0 = -800;
    private static final int DO_POAL_MOTION_SPEED = 600;
    private static final boolean[] EFFECT_LOOP = new boolean[]{true, true};
    protected static final int EFFECT_NONE = -1;
    protected static final int EFFECT_SAND_1 = 0;
    protected static final int EFFECT_SAND_2 = 1;
    private static final int ENLARGE_NUM = 1920;
    private static final int FADE_FILL_HEIGHT = 40;
    private static final int FADE_FILL_WIDTH = 40;
    public static int FAKE_GRAVITY_ON_BALL = 224;
    public static int FAKE_GRAVITY_ON_WALK = 72;
    public static final int FALL_IN_SAND_SLIP_LEFT = 2;
    public static final int FALL_IN_SAND_SLIP_NONE = 0;
    public static final int FALL_IN_SAND_SLIP_RIGHT = 1;
    private static final int FOCUS_MAX_OFFSET = ((MapManager.CAMERA_HEIGHT >> 1) - 16);
    private static final int FOCUS_MOVE_SPEED = 15;
    protected static final int FOCUS_MOVING_DOWN = 2;
    private static final int FOCUS_MOVING_NONE = 0;
    protected static final int FOCUS_MOVING_UP = 1;
    private static final int FONT_NUM = 7;
    private static final int FOOT_OFFSET = 256;
    private static final int[] FOOT_OFFSET_X = new int[]{-256, 256};
    public static final int HEIGHT = 1536;
    private static final int HINER_JUMP_LIMIT = 1024;
    private static final int HINER_JUMP_MAX = 4352;
    private static final int HINER_JUMP_X_ADD = 1024;
    private static final int HINER_JUMP_Y = 2048;
    public static final int HUGE_POWER_SPEED = 1900;
    public static final int HURT_COUNT = 48;
    public static int HURT_POWER_X = PlayerSonic.BACK_JUMP_SPEED_X;
    public static int HURT_POWER_Y = -992;
    private static final int ICE_SLIP_FLUSH_OFFSET_Y = 512;
    private static final int INVINCIBLE_COUNT = 320;
    public static final boolean IN_BLOCK_CHECK = false;
    private static final float IN_WATER_WALK_SPEED_SCALE1 = 5.0f;
    private static final float IN_WATER_WALK_SPEED_SCALE2 = 9.0f;
    private static final int ITEM_INDEX = 0;
    public static final int ITEM_INVINCIBLE = 3;
    public static final int ITEM_LIFE = 0;
    public static final int ITEM_RING_10 = 7;
    public static final int ITEM_RING_5 = 6;
    public static final int ITEM_RING_RANDOM = 5;
    public static final int ITEM_SHIELD = 1;
    public static final int ITEM_SHIELD_2 = 2;
    public static final int ITEM_SPEED = 4;
    public static boolean IsDisplayRaceModeNewRecord = false;
    public static boolean IsStarttoCnt = false;
    private static final int JUMP_EFFECT_HEIGHT = 1920;
    private static final int JUMP_EFFECT_OFFSET_Y = 256;
    private static final int JUMP_EFFECT_WIDTH = 1920;
    public static int JUMP_INWATER_START_VELOCITY = (-1304 - GRAVITY);
    public static int JUMP_PROTECT = ((-GRAVITY) - GRAVITY);
    public static int JUMP_REVERSE_POWER = 32;
    public static int JUMP_RUSH_SPEED_PLUS = 480;
    public static int JUMP_START_VELOCITY = (-1208 - GRAVITY);
    private static final int LEFT_FOOT_OFFSET_X = -256;
    private static final int LEFT_WALK_COLLISION_CHECK_OFFSET_X = -512;
    private static final int LEFT_WALK_COLLISION_CHECK_OFFSET_Y = -512;
    public static final int LIFE_NUM_RESET = 2;
    private static final int LOOK_COUNT = 32;
    private static final int MAX_ITEM = 5;
    private static final int MAX_ITEM_SHOW_NUM = 4;
    public static int MAX_VELOCITY = 1280;
    public static final int MIN_ATTACK_JUMP = -900;
    private static final int MOON_STAR_DES_X_1 = (SCREEN_WIDTH - 22);
    private static final int MOON_STAR_DES_Y_1 = 26;
    private static final int MOON_STAR_FRAMES_1 = 207;
    private static final int MOON_STAR_FRAMES_2 = 120;
    private static final int MOON_STAR_ORI_X_1 = SCREEN_WIDTH;
    private static final int MOON_STAR_ORI_Y_1 = 18;
    public static int MOVE_POWER = 28;
    public static int MOVE_POWER_IN_AIR = 92;
    public static int MOVE_POWER_REVERSE = 336;
    public static int MOVE_POWER_REVERSE_BALL = 96;
    public static final boolean NEED_RESET_DEDREE = false;
    private static final int[] NUM_ANI_ID = new int[]{27, 37, ANI_BIG_ZERO, StringIndex.FONT_COLON_BIG, 37};
    public static final int NUM_CENTER = 0;
    public static final int NUM_DISTANCE_BIG = 72;
    public static final int NUM_LEFT = 1;
    private static final int NUM_PIC_HEIGHT = 13;
    private static final int NUM_PIC_WIDTH = 7;
    public static final int NUM_RIGHT = 2;
    private static final int[] NUM_SPACE_ANIMATION = new int[]{8, 8, 16, 8, 8};
    private static final int[] NUM_SPACE = NUM_SPACE_ANIMATION;
    private static final int NUM_DISTANCE = (NUM_SPACE[0] * 8 > 60 ? NUM_SPACE[0] * 7 : 60);
    private static final int[] NUM_SPACE_FONT = new int[]{FONT_WIDTH_NUM, FONT_WIDTH_NUM, FONT_WIDTH_NUM, FONT_WIDTH_NUM, FONT_WIDTH_NUM};
    private static final int[] NUM_SPACE_IMAGE = new int[]{NUM_PIC_WIDTH + 1, NUM_PIC_WIDTH + 1, NUM_PIC_WIDTH + 1, NUM_PIC_WIDTH + 1, NUM_PIC_WIDTH + 1};
    public static final int NumberSideX = (81 - MyAPI.zoomIn(0));
    public static int PAUSE_FRAME_WIDTH = (FONT_WIDTH * 7) + 4;
    public static final int PAUSE_FRAME_HEIGHT = ((MENU_SPACE * 5) + 20);
    public static final int PAUSE_FRAME_OFFSET_X = ((-PAUSE_FRAME_WIDTH) >> 1);
    public static final int PAUSE_FRAME_OFFSET_Y = ((-PAUSE_FRAME_HEIGHT) >> 1);
    private static int[] PAUSE_MENU_NORMAL_ITEM = null;
    private static final int[] PAUSE_MENU_NORMAL_NOSHOP = new int[]{12, 81, 5, 6};
    private static final int[] PAUSE_MENU_NORMAL_SHOP = new int[]{12, 81, 52, 5, 6};
    private static final int[] PAUSE_MENU_RACE_ITEM = new int[]{12, 82, 11, 81, 5, 6};
    private static final int PIPE_SET_POWER = 2880;
    protected static final String PLAYER_ANIMATION_PATH = "/animation/player";
    private static final int RAIL_FLIPPER_V0 = -3380;
    private static final int RAIL_OUT_SPEED_VY0 = -1200;
    private static final int[] RANDOM_RING_NUM = new int[]{1, 5, 5, 5, 5, 10, 20, 30, 40};
    public static final int RED_NUM = 3;
    private static final int RIGHT_FOOT_OFFSET_X = 256;
    private static final int RIGHT_WALK_COLLISION_CHECK_OFFSET_X = 512;
    private static final int RIGHT_WALK_COLLISION_CHECK_OFFSET_Y = -512;
    protected static final int ROTATE_MODE_NEGATIVE = 2;
    protected static final int ROTATE_MODE_NEVER_MIND = 0;
    protected static final int ROTATE_MODE_POSITIVE = 1;
    public static int RUN_BRAKE_SPEED_LIMIT = 480;
    public static int RingBonus = 0;
    public static final int SHOOT_POWER = -1800;
    private static final int SIDE_COLLISION_NUM = -2;
    private static final int SIDE_FOOT_FROM_CENTER = 256;
    private static final int SMALL_JUMP_COUNT = 4;
    public static final int SMALL_NUM = 0;
    public static final int SMALL_NUM_Y = 1;
    public static final int SONIC_ATTACK_LEVEL_1_V0 = 488;
    public static final int SONIC_ATTACK_LEVEL_2_V0 = 672;
    public static final int SONIC_ATTACK_LEVEL_3_V0 = 1200;
    private static final int SONIC_DRAW_HEIGHT = 1920;
    public static int SPEED_FLOAT_DEVICE = 40;
    public static int SPEED_LIMIT_LEVEL_1 = BANKING_MIN_SPEED;
    public static int SPEED_LIMIT_LEVEL_2 = 1120;
    public static int SPIN_INWATER_START_SPEED_1 = 2160;
    public static int SPIN_INWATER_START_SPEED_2 = 3600;
    private static final int SPIN_KEY_COUNT = 20;
    private static final int SPIN_LV2_COUNT = 12;
    private static final int SPIN_LV2_COUNT_CONF = 36;
    public static int SPIN_START_SPEED_1 = 1440;
    public static int SPIN_START_SPEED_2 = 2400;
    private static final int STAGE_PASS_STR_SPACE = 182;
    private static final int STAGE_PASS_STR_SPACE_FONT = (MyAPI.zoomIn(MFGraphics.stringWidth(14, "索尼克完成行动1")) + 20);
    protected static final byte STATE_PIPE_IN = (byte) 0;
    protected static final byte STATE_PIPE_OVER = (byte) 2;
    protected static final byte STATE_PIPING = (byte) 1;
    private static final int SUPER_SONIC_CHANGING_CENTER_Y = 25280;
    private static final int SUPER_SONIC_STAND_POS_X = 235136;
    private static final int TERMINAL_COUNT = 10;
    public static final int TERMINAL_NO_MOVE = 1;
    public static final int TERMINAL_RUN_TO_RIGHT = 0;
    public static final int TERMINAL_RUN_TO_RIGHT_2 = 2;
    public static final int TERMINAL_SUPER_SONIC = 3;
    protected static final byte TER_STATE_BRAKE = (byte) 1;
    protected static final byte TER_STATE_CHANGE_1 = (byte) 4;
    protected static final byte TER_STATE_CHANGE_2 = (byte) 5;
    protected static final byte TER_STATE_GO_AWAY = (byte) 6;
    protected static final byte TER_STATE_LOOK_MOON = (byte) 2;
    protected static final byte TER_STATE_LOOK_MOON_WAIT = (byte) 3;
    protected static final byte TER_STATE_RUN = (byte) 0;
    protected static final byte TER_STATE_SHINING_2 = (byte) 7;
    protected static final int[] TRANS = new int[]{0, 5, 3, 6};

    public static int TimeBonus = 0;
    private static final int WALK_COLLISION_CHECK_OFFSET_X = 0;
    private static final int WALK_COLLISION_CHECK_OFFSET_Y = 0;
    private static final int WHITE_BACKGROUND_ID = 118;
    protected static final int WIDTH = 1024;
    public static final int YELLOW_NUM = 4;
    private static AnimationDrawer bariaDrawer;
    private static MFImage breatheCountImage;
    private static int characterID = 0;
    private static int clipendw;
    private static int cliph;
    private static int clipspeed = 5;
    private static int clipstartw;
    private static int clipx;
    private static int clipy;
    private static ACBlock collisionBlockGround = CollisionMap.getInstance().getNewCollisionBlock();
    private static ACBlock collisionBlockGroundTmp = CollisionMap.getInstance().getNewCollisionBlock();
    private static ACBlock collisionBlockSky = CollisionMap.getInstance().getNewCollisionBlock();
    public static int currentMarkId;
    private static int[] currentPauseMenuItem;
    public static int cursor;
    public static int cursorIndex;
    public static int cursorMax = 5;
    private static int fadeAlpha = 40;
    private static int fadeFromValue;
    private static int[] fadeRGB = new int[1600];
    private static int fadeToValue;
    private static AnimationDrawer fastRunDrawer;
    private static AnimationDrawer gBariaDrawer;
    private static AnimationDrawer getLifeDrawer;
    private static AnimationDrawer headDrawer;
    private static Animation invincibleAnimation;
    private static int invincibleCount;
    private static AnimationDrawer invincibleDrawer;
    public static boolean isDeadLineEffect;
    public static boolean isNeedPlayWaterSE = false;
    public static boolean isOnlyBarOut = false;
    private static boolean isStartStageEndFlag = false;
    public static boolean isTerminal;
    public static boolean isbarOut = false;
    private static int itemOffsetX;
    private static int[][] itemVec = ((int[][]) Array.newInstance(Integer.TYPE, new int[]{5, 2}));
    public static int lastTimeCount;
    private static int lifeDrawerX = 0;
    public static int lifeNum = 2;
    private static AnimationDrawer moonStarDrawer;
    private static int movespeedx = 96;
    private static int movespeedy = 28;
    private static int newRecordCount;
    private static AnimationDrawer numDrawer;
    public static MFImage numImage;
    private static int offsetx;
    private static int offsety;
    public static int onlyBarOutCnt = 0;
    public static int onlyBarOutCntMax = 80;
    public static int overTime;
    private static int passStageActionID = 0;
    private static int preFadeAlpha;
    private static int preLifeNum;
    private static int preScoreNum;
    private static int preTimeCount = 0;
    public static int raceScoreNum;
    protected static int ringNum;
    private static int ringRandomNum;
    protected static int ringTmpNum;
    private static int score1 = 49700;
    private static int score2 = 12700;
    public static int scoreNum;
    private static int shieldType;
    public static int slidingFrame;
    protected static int speedCount;
    private static int stageEndFrameCnt;
    private static int stagePassResultOutOffsetX;
    protected static byte terminalState;
    protected static int terminalType;
    public static int timeCount;
    protected static boolean timeStopped = false;
    private static int totalPlusscore;
    private static AnimationDrawer uiDrawer;
    public static int uiOffsetX = 0;
    private static MFImage uiRingImage;
    private static MFImage uiSonicHeadImage;
    private static AnimationDrawer waterSprayDrawer;
    public boolean IsStandOnItems;
    private CollisionRect aaaAttackRect;
    protected int animationID;
    private int attackAnimationID;
    private int attackCount;
    private int attackLevel;
    public Vector attackRectVec;
    public CollisionRect attractRect;
    public boolean bankwalking;
    public boolean beAttackByHari;
    public int bePushedFootX;
    protected int breatheCount;
    protected int breatheFrame;
    private int breatheNumCount;
    private int breatheNumY;
    public boolean canAttackByHari;
    public boolean changeRectHeight;
    protected int checkPositionX;
    protected int checkPositionY;
    private boolean checkedObject;
    public boolean collisionChkBreak;
    private int collisionLayer;
    public byte collisionState;
    public boolean controlObjectLogic;
    private long count;
    protected boolean dashRolling;
    private int deadPosX;
    private int deadPosY;
    protected int degreeForDraw;
    protected int degreeRotateMode;
    public int degreeStable;
    protected boolean doJumpForwardly;
    protected AnimationDrawer drawer;
    private int drownCnt;
    private boolean ducting;
    private int ductingCount;
    protected Animation dustEffectAnimation;
    private AnimationDrawer effectDrawer;
    protected int effectID;
    private boolean enteringSP;
    public boolean extraAttackFlag;
    protected int faceDegree;
    public boolean faceDirection;
    protected boolean fading;
    public int fallTime;
    public int fallinSandSlipState;
    public boolean finishDeadStuff;
    protected int focusMovingState;
    private int focusOffsetY;
    public boolean footObjectLogic;
    int footOffsetX;
    public GameObject footOnObject;
    public int footPointX;
    public int footPointY;
    private int frame;
    private int frameCnt;
    private boolean freeMoveDebug;
    public int hurtCount;
    public boolean hurtNoControl;
    public boolean ignoreFirstTouch;
    public boolean isAfterSpinDash;
    public boolean isAntiGravity;
    public boolean isAttackBoss4;
    public boolean isAttacking;
    public boolean isCelebrate;
    public boolean isCrashFallingSand;
    public boolean isCrashPipe;
    public boolean isDead;
    public boolean isDirectioninSkyChange;
    public boolean isInGravityCircle;
    public boolean isInSnow;
    protected boolean isInWater;
    public boolean isOnBlock;
    public boolean isOnlyJump;
    public boolean isPowerShoot;
    public boolean isResetWaitAni;
    public boolean isSharked;
    public int isSidePushed;
    public boolean isStopByObject;
    private boolean isTouchSandSlip;
    public boolean isUpPipeIn;
    private CollisionRect jumpAttackRect;
    private int justLeaveCount;
    private int justLeaveDegree;
    public boolean justLeaveLand;
    public boolean leavingBar;
    public boolean leftStopped;
    private int lookCount;
    protected int maxVelocity;
    private int moonStarFrame1;
    private int moonStarFrame2;
    public int moveLimit;
    private int movePower;
    private int movePowerInAir;
    private int movePowerReserseBall;
    private int movePowerReserseBallInSand;
    private int movePowerReverse;
    private int movePowerReverseInSand;
    public int movedSpeedX;
    public int movedSpeedY;
    protected int myAnimationID;
    private int nextVelX;
    private int nextVelY;
    private boolean noKeyFlag;
    public boolean noMoving;
    private int noMovingPosition;
    public boolean noVelMinus;
    public boolean onBank;
    private boolean onGround;
    private boolean onObjectContinue;
    private boolean orgGravity;
    public boolean outOfControl;
    public GameObject outOfControlObject;
    private int pipeDesX;
    private int pipeDesY;
    protected byte pipeState;
    public boolean piping;
    private int preBreatheNumCount;
    public CollisionRect preCollisionRect;
    private int preFocusX;
    private int preFocusY;
    public boolean prefaceDirection;
    private int preposY;
    private boolean pushOnce;
    private boolean railFlipping;
    protected Line railLine;
    public boolean railOut;
    public boolean railing;
    public boolean rightStopped;
    private int sBlockX;
    private int sBlockY;
    private int sXPosition;
    private int sYPosition;
    private int sandFrame;
    private boolean sandStanding;
    private boolean setNoMoving;
    public boolean showWaterFlush;
    public boolean slideSoundStart;
    private boolean slipFlag;
    public boolean slipping;
    private int smallJumpCount;
    public boolean speedLock;
    private int spinCount;
    private int spinDownWaitCount;
    private int spinKeyCount;
    private boolean squeezeFlag;
    public int terminalCount;
    public int terminalOffset;
    private boolean transing;
    private boolean visible;
    private int waitingCount;
    private int waitingLevel;
    private AnimationDrawer waterFallDrawer;
    private boolean waterFalling;
    private AnimationDrawer waterFlushDrawer;
    private boolean waterSprayFlag;
    private int waterSprayX;
    protected ACWorldCollisionCalculator worldCal;
    private boolean xFirst;

    public abstract void closeImpl();

    public static boolean characterSelectLogic() {
        if (Key.press(Key.gSelect)) {
            return true;
        }
        if (Key.press(Key.gLeft)) {
            characterID--;
            characterID += CHARACTER_LIST.length;
            characterID %= CHARACTER_LIST.length;
        } else if (Key.press(Key.gRight)) {
            characterID++;
            characterID += CHARACTER_LIST.length;
            characterID %= CHARACTER_LIST.length;
        }
        return false;
    }

    public static void setCharacter(int ID) {
        characterID = ID;
    }

    public static int getCharacterID() {
        return characterID;
    }

    public static PlayerObject getPlayer() {
        PlayerObject re;
        switch (characterID) {
            case CHARACTER_SONIC:
                if (StageManager.getCurrentZoneId() == 8) {
                    re = new PlayerSuperSonic();
                } else {
                    re = new PlayerSonic();
                }
                break;
            case CHARACTER_TAILS:
                re = new PlayerTails();
                break;
            case CHARACTER_KNUCKLES:
                re = new PlayerKnuckles();
                break;
            case CHARACTER_AMY:
                re = new PlayerAmy();
                break;
            default:
                re = new PlayerSonic();
                break;
        }
        terminalState = (byte) 0;
        terminalType = 0;
        return re;
    }

    public void setMeetingBoss(boolean state) {
        boolean z;
        if (state) {
            z = false;
        } else {
            z = true;
        }
        this.setNoMoving = z;
        this.noMovingPosition = this.footPointX;
        this.worldCal.stopMoveX();
        this.collisionChkBreak = true;
    }

    public boolean changeRectUpCheck() {
        for (int i = 2048 / this.worldInstance.getTileHeight(); i >= 0; i--) {
            if (this.worldInstance.getWorldY(this.collisionRect.x0 + 512, this.collisionRect.y0 - (this.worldInstance.getTileHeight() * i), this.currentLayer, 0) != -1000) {
                return true;
            }
        }
        return false;
    }

    public boolean changeRectDownCheck() {
        for (int i = 2048 / this.worldInstance.getTileHeight(); i >= 0; i--) {
            if (this.worldInstance.getWorldY(this.collisionRect.x0 + 512, this.collisionRect.y0 + (this.worldInstance.getTileHeight() * i), this.currentLayer, 2) != -1000) {
                return true;
            }
        }
        return false;
    }

    public boolean needChangeRect() {
        return this.animationID == 4 && this.collisionState == (byte) 1 && ((!this.isAntiGravity && changeRectUpCheck()) || (this.isAntiGravity && changeRectDownCheck()));
    }

    public int getObjHeight() {
        if (needChangeRect()) {
            return this.collisionRect.getHeight();
        }
        return 1536;
    }

    public static void setNewParam(int[] newParam) {
        MOVE_POWER = newParam[0];
        MOVE_POWER_IN_AIR = MOVE_POWER << 1;
        MOVE_POWER_REVERSE = newParam[1];
        MAX_VELOCITY = newParam[2];
        MOVE_POWER_REVERSE_BALL = newParam[3];
        SPIN_START_SPEED_1 = newParam[4];
        SPIN_START_SPEED_2 = newParam[5];
        JUMP_START_VELOCITY = newParam[6];
        HURT_POWER_X = newParam[7];
        HURT_POWER_Y = newParam[8];
        JUMP_RUSH_SPEED_PLUS = newParam[10];
        JUMP_REVERSE_POWER = newParam[11];
        FAKE_GRAVITY_ON_WALK = newParam[12];
        FAKE_GRAVITY_ON_BALL = newParam[13];
    }

    public PlayerObject() {
        this.degreeStable = 0;
        this.faceDegree = 0;
        this.faceDirection = true;
        this.prefaceDirection = true;
        this.extraAttackFlag = false;
        this.footPointX = 0;
        this.onGround = false;
        this.spinCount = 0;
        this.movePower = MOVE_POWER;
        this.movePowerInAir = MOVE_POWER_IN_AIR;
        this.movePowerReverse = MOVE_POWER_REVERSE;
        this.movePowerReserseBall = MOVE_POWER_REVERSE_BALL;
        this.movePowerReverseInSand = MOVE_POWER_REVERSE << 1;
        this.movePowerReserseBallInSand = MOVE_POWER_REVERSE << 1;
        this.maxVelocity = MAX_VELOCITY;
        this.effectID = -1;
        this.collisionLayer = 0;
        this.dashRolling = false;
        this.hurtCount = 0;
        this.hurtNoControl = false;
        this.visible = true;
        this.outOfControl = false;
        this.controlObjectLogic = false;
        this.leavingBar = false;
        this.footObjectLogic = false;
        this.outOfControlObject = null;
        this.attackRectVec = new Vector();
        this.jumpAttackRect = new CollisionRect();
        this.attractRect = new CollisionRect();
        this.aaaAttackRect = new CollisionRect();
        this.fallinSandSlipState = 0;
        this.isAttacking = false;
        this.canAttackByHari = false;
        this.beAttackByHari = false;
        this.setNoMoving = false;
        this.leftStopped = false;
        this.rightStopped = false;
        this.focusMovingState = 0;
        this.lookCount = 32;
        this.footOffsetX = 0;
        this.justLeaveLand = false;
        this.justLeaveCount = 2;
        this.IsStandOnItems = false;
        this.degreeRotateMode = 0;
        this.slipping = false;
        this.doJumpForwardly = false;
        this.preCollisionRect = new CollisionRect();
        this.ignoreFirstTouch = false;
        this.waterFallDrawer = null;
        this.waterFlushDrawer = null;
        this.railFlipping = false;
        this.isPowerShoot = false;
        this.isDead = false;
        this.isSharked = false;
        this.finishDeadStuff = false;
        this.deadPosX = 0;
        this.deadPosY = 0;
        this.noKeyFlag = false;
        this.bankwalking = false;
        this.transing = false;
        this.ducting = false;
        this.ductingCount = 0;
        this.pushOnce = false;
        this.squeezeFlag = true;
        this.orgGravity = false;
        this.footPointX = 512;
        this.footPointY = 0;
        MapManager.setFocusObj(this);
        MapManager.focusQuickLocation();
        this.dustEffectAnimation = new Animation("/animation/effect_dust");
        this.effectDrawer = this.dustEffectAnimation.getDrawer();
        this.animationID = 1;
        this.collisionState = (byte) 1;
        this.currentLayer = 1;
        if (bariaDrawer == null) {
            bariaDrawer = new Animation("/animation/baria").getDrawer(0, true, 0);
        }
        if (gBariaDrawer == null) {
            gBariaDrawer = new Animation("/animation/g_baria").getDrawer(0, true, 0);
        }
        if (invincibleAnimation == null) {
            invincibleAnimation = new Animation("/animation/muteki");
        }
        if (invincibleDrawer == null) {
            invincibleDrawer = invincibleAnimation.getDrawer(0, true, 0);
        }
        if (breatheCountImage == null) {
            breatheCountImage = MFImage.createImage("/animation/player/breathe_count.png");
        }
        if (waterSprayDrawer == null && StageManager.getCurrentZoneId() == 4) {
            waterSprayDrawer = new Animation("/animation/stage6_water_spray").getDrawer();
        }
        if (moonStarDrawer == null && StageManager.isGoingToExtraStage()) {
            moonStarDrawer = new Animation("/animation/moon_star").getDrawer();
        }
        this.width = 1024;
        this.height = 1536;
        this.worldCal = new ACWorldCollisionCalculator(this, this);
        initUIResource();
    }

    private void initUIResource() {
    }

    public void _logic() {
        int i;
        for (int i2 = 0; i2 < 5; i2++) {
            if (itemVec[i2][0] >= 0) {
                if (itemVec[i2][1] > 0) {
                    int[] iArr = itemVec[i2];
                    iArr[1] = iArr[1] - 1;
                }
                if (itemVec[i2][1] == 0) {
                    getItem(itemVec[i2][0]);
                    itemVec[i2][0] = -1;
                }
            }
        }
        if (this.isAntiGravity) {
            i = RollPlatformSpeedC.DEGREE_VELOCITY;
        } else {
            i = 0;
        }
        this.degreeStable = i;
        this.leftStopped = false;
        this.rightStopped = false;
        if (this.enteringSP && (this.posY >> 6) < camera.f14y) {
            GameState.enterSpStage(ringNum, currentMarkId, timeCount);
            this.enteringSP = false;
        }
        if (this.hurtCount > 0) {
            this.hurtCount--;
        }
        if (invincibleCount > 0) {
            invincibleCount--;
            if (invincibleCount == 0) {
                i = SoundSystem.getInstance().getPlayingBGMIndex();
                SoundSystem.getInstance();
                if (i == 44) {
                    SoundSystem.getInstance().stopBgm(false);
                    if (!isTerminal) {
                        SoundSystem.getInstance().playBgm(StageManager.getBgmId());
                    }
                }
                i = SoundSystem.getInstance().getPlayingBGMIndex();
                SoundSystem.getInstance();
                if (i == 43) {
                    SoundSystem.getInstance().playNextBgm(StageManager.getBgmId());
                }
            }
        }
        this.preFocusX = getNewPointX(this.footPointX, 0, -768, this.faceDegree) >> 6;
        this.preFocusY = getNewPointY(this.footPointY, 0, -768, this.faceDegree) >> 6;
        if (this.setNoMoving) {
            if (this.collisionState == (byte) 0) {
                this.footPointX = this.noMovingPosition;
                setVelX(0);
                setVelY(0);
                this.animationID = 0;
                return;
            } else if (this.collisionState == (byte) 1) {
                this.footPointX = this.noMovingPosition;
                this.velX = 0;
                setNoKey();
            }
        }
        if (this.collisionState == (byte) 0) {
            this.deadPosX = this.footPointX;
            this.deadPosY = this.footPointY;
        }
        if (characterID == 1) {
            if (!(this.myAnimationID == 12 || this.myAnimationID == 48 || this.myAnimationID == 49)) {
                if (soundInstance.getPlayingLoopSeIndex() == 15) {
                    soundInstance.stopLoopSe();
                }
                resetFlyCount();
            }
            if (this.collisionState == (byte) 0) {
                if (soundInstance.getPlayingLoopSeIndex() == 15) {
                    soundInstance.stopLoopSe();
                }
                resetFlyCount();
            }
        }
        if (this.isDead) {
            if (this.isInWater && this.breatheNumCount >= 6) {
                this.drownCnt++;
                if (this.drownCnt % 2 == 0) {
                    GameObject.addGameObject(new DrownBubble(41, this.footPointX, this.footPointY - 1536, 0, 0, 0, 0));
                }
            }
            boolean deadOver = false;
            if (this.isAntiGravity) {
                if (this.footPointY < (MapManager.getCamera().f14y << 6) - 3072) {
                    this.footPointY = (MapManager.getCamera().f14y << 6) - 3072;
                    deadOver = true;
                }
            } else if (this.velY > 0 && this.footPointY > ((MapManager.getCamera().f14y + MapManager.CAMERA_HEIGHT) << 6) + 3072) {
                this.footPointY = ((MapManager.getCamera().f14y + MapManager.CAMERA_HEIGHT) << 6) + 3072;
                deadOver = true;
            }
            if (deadOver && !this.finishDeadStuff) {
                if (stageModeState == 1) {
                    StageManager.setStageRestart();
                } else if (!(timeCount == overTime && GlobalResource.timeIsLimit())) {
                    if (lifeNum > 0) {
                        lifeNum--;
                        StageManager.setStageRestart();
                    } else {
                        StageManager.setStageGameover();
                    }
                }
                this.finishDeadStuff = true;
                return;
            }
            return;
        }
        this.focusMovingState = 0;
        this.controlObjectLogic = false;
        if (!this.outOfControl) {
            int waterLevel = StageManager.getWaterLevel();
            if (waterLevel > 0) {
                if (characterID == 2) {
                    ((PlayerKnuckles) player).setPreWaterFlag(this.isInWater);
                }
                if (!this.isInWater) {
                    this.breatheCount = 0;
                    this.breatheNumCount = -1;
                    this.preBreatheNumCount = -1;
                    if (getNewPointY(this.posY, 0, (-this.collisionRect.getHeight()) >> 1, this.faceDegree) - 256 >= (waterLevel << 6)) {
                        this.isInWater = true;
                        if (isNeedPlayWaterSE) {
                            SoundSystem.getInstance().playSe(58);
                        }
                        this.waterSprayFlag = true;
                        this.waterSprayX = this.posX;
                        waterSprayDrawer.restart();
                    }
                } else if (!IsGamePause) {
                    PlayerObject playerObject;
                    this.breatheCount += 63;
                    this.breatheNumCount = -1;
                    if (characterID == 2 && this.collisionState == (byte) 4) {
                        if (getNewPointY(this.posY, 0, -this.collisionRect.getHeight(), this.faceDegree) + 256 < (waterLevel << 6)) {
                            this.breatheCount = 0;
                            i = SoundSystem.getInstance().getPlayingBGMIndex();
                            SoundSystem.getInstance();
                            if (i == 21) {
                                SoundSystem.getInstance().stopBgm(false);
                                playerObject = player;
                                if (IsInvincibility()) {
                                    SoundSystem.getInstance().playBgm(44);
                                } else if (this.isAttackBoss4) {
                                    SoundSystem.getInstance().playBgm(22);
                                } else {
                                    SoundSystem.getInstance().playBgm(StageManager.getBgmId());
                                }
                            }
                        }
                    }
                    if (this.breatheCount > BREATHE_TIME_COUNT) {
                        this.breatheNumCount = (this.breatheCount - BREATHE_TIME_COUNT) / BREATHE_TO_DIE_PER_COUNT;
                        if (this.breatheCount == 0) {
                            i = SoundSystem.getInstance().getPlayingBGMIndex();
                            SoundSystem.getInstance();
                            if (i != 21) {
                                if (this.isAttackBoss4) {
                                    soundInstance.playBgm(21);
                                } else {
                                    soundInstance.playBgm(21);
                                }
                                if (this.breatheNumCount < 6 && canBeHurt()) {
                                    setDie(true);
                                    return;
                                } else if (this.breatheNumCount != this.preBreatheNumCount) {
                                    this.breatheNumY = ((this.posY >> 6) - camera.f14y) - 30;
                                }
                            }
                        }
                        i = SoundSystem.getInstance().getPlayingBGMIndex();
                        SoundSystem.getInstance();
                        if (i != 21) {
                            long startTime = (long) (((this.breatheCount - BREATHE_TIME_COUNT) * 10000) / 10560);
                            if (this.isAttackBoss4) {
                                soundInstance.playBgmFromTime(startTime, 21);
                            } else {
                                soundInstance.playBgmFromTime(startTime, 21);
                            }
                        }
                        if (this.breatheNumCount < 6) {
                        }
                        if (this.breatheNumCount != this.preBreatheNumCount) {
                            this.breatheNumY = ((this.posY >> 6) - camera.f14y) - 30;
                        }
                    }
                    this.preBreatheNumCount = this.breatheNumCount;
                    int bodyCenterY = getNewPointY(this.posY, 0, (-this.collisionRect.getHeight()) >> 1, this.faceDegree);
                    if (characterID == 3) {
                        bodyCenterY = getNewPointY(this.posY, 0, (((-this.collisionRect.getHeight()) * 3) / 4) - 128, this.faceDegree);
                    }
                    if (bodyCenterY + 256 <= (waterLevel << 6)) {
                        this.isInWater = false;
                        if (this.breatheNumCount >= 0 && SoundSystem.getInstance().getPlayingBGMIndex() == 21) {
                            SoundSystem.getInstance().stopBgm(false);
                            playerObject = player;
                            if (IsInvincibility()) {
                                SoundSystem.getInstance().playBgm(44);
                            } else if (this.isAttackBoss4) {
                                SoundSystem.getInstance().playBgm(22);
                            } else {
                                SoundSystem.getInstance().playBgm(StageManager.getBgmId());
                            }
                        }
                        if (isNeedPlayWaterSE) {
                            SoundSystem.getInstance().playSe(58);
                        }
                        this.waterSprayFlag = true;
                        this.waterSprayX = this.posX;
                        waterSprayDrawer.restart();
                    }
                    this.breatheFrame++;
                    this.breatheFrame %= 51;
                    if (this.breatheFrame == MyRandom.nextInt(1, 8) * 6) {
                        GameObject.addGameObject(new AspirateBubble(40, player.getFootPositionX() + (this.faceDirection ? PlayerSonic.BACK_JUMP_SPEED_X : -384), player.getFootPositionY() - 1536, 0, 0, 0, 0));
                    }
                }
            }
            if (speedCount > 0) {
                speedCount--;
                this.movePower = MOVE_POWER << 1;
                this.movePowerInAir = MOVE_POWER_IN_AIR << 1;
                this.movePowerReverse = MOVE_POWER_REVERSE << 1;
                this.movePowerReserseBall = MOVE_POWER_REVERSE_BALL << 1;
                this.maxVelocity = MAX_VELOCITY << 1;
                if (!(speedCount != 0 || SoundSystem.getInstance().getPlayingBGMIndex() == 42 || SoundSystem.getInstance().getPlayingBGMIndex() == 41 || SoundSystem.getInstance().getPlayingBGMIndex() == 26)) {
                    SoundSystem.getInstance().setSoundSpeed(1.0f);
                    if (SoundSystem.getInstance().getPlayingBGMIndex() != 43) {
                        SoundSystem.getInstance().restartBgm();
                    }
                }
            } else {
                this.movePower = MOVE_POWER;
                this.movePowerInAir = MOVE_POWER_IN_AIR;
                this.movePowerReverse = MOVE_POWER_REVERSE;
                this.movePowerReserseBall = MOVE_POWER_REVERSE_BALL;
                this.maxVelocity = MAX_VELOCITY;
            }
            if (this.isAntiGravity) {
                if (!this.isDead && this.footPointY > (MapManager.getPixelHeight() << 6)) {
                    this.footPointY = (MapManager.getCamera().f14y + MapManager.CAMERA_HEIGHT) << 6;
                    if (getVelY() < 0) {
                        setVelY(0);
                    }
                }
            } else if (!this.isDead && this.footPointY > (MapManager.getPixelHeight() << 6)) {
                this.footPointY = ((MapManager.getCamera().f14y + MapManager.CAMERA_HEIGHT) << 6) + 3072;
                setDie(false, -1600);
            }
            this.ignoreFirstTouch = false;
            if (this.dashRolling) {
                dashRollingLogic();
                if (this.dashRolling) {
                    collisionChk();
                    return;
                }
            } else if (this.effectID == 0 || this.effectID == 1) {
                this.effectID = -1;
            }
            if (this.railing) {
                setNoKey();
                if (this.railLine == null) {
                    this.velY += getGravity();
                    checkWithObject(this.footPointX, this.footPointY, this.footPointX + this.velX, this.footPointY + this.velY);
                } else {
                    int preFootPointX = this.footPointX;
                    int preFootPointY = this.footPointY;
                    int velocityChange = this.railLine.sin(getGravity());
                    if (!this.railLine.directRatio()) {
                        velocityChange = -velocityChange;
                    }
                    if (velocityChange != 0) {
                        Direction direction = this.railLine.getOneDirection();
                        this.totalVelocity += velocityChange;
                        checkWithObject(this.footPointX, this.footPointY, this.footPointX + direction.getValueX(this.railLine.cos(this.totalVelocity)), this.footPointY + direction.getValueY(this.railLine.sin(this.totalVelocity)));
                    } else {
                        checkWithObject(this.footPointX, this.footPointY, this.footPointX + ((this.totalVelocity < 0 ? -1 : 1) * this.railLine.cos(this.totalVelocity)), this.footPointY + ((this.totalVelocity < 0 ? -1 : 1) * this.railLine.sin(this.totalVelocity)));
                    }
                    if (!(this.railOut || this.railLine == null)) {
                        this.velX = this.footPointX - preFootPointX;
                        this.velY = this.footPointY - preFootPointY;
                    }
                }
                if (this.railOut && this.velY == getGravity() + RAIL_OUT_SPEED_VY0) {
                    if (characterID == 3) {
                        soundInstance.playSe(25);
                    } else {
                        soundInstance.playSe(37);
                    }
                }
                if (this.railOut && this.velY > 0) {
                    this.railOut = false;
                    this.railing = false;
                    this.collisionState = (byte) 1;
                }
            } else if (this.piping) {
                int preX = this.footPointX;
                int preY = this.footPointY;
                switch (this.pipeState) {
                    case (byte) 0:
                        if (this.footPointX < this.pipeDesX) {
                            this.footPointX += 250;
                            if (this.footPointX >= this.pipeDesX) {
                                this.footPointX = this.pipeDesX;
                            }
                        } else if (this.footPointX > this.pipeDesX) {
                            this.footPointX -= 250;
                            if (this.footPointX <= this.pipeDesX) {
                                this.footPointX = this.pipeDesX;
                            }
                        }
                        if (this.footPointY < this.pipeDesY) {
                            this.footPointY += 250;
                            if (this.footPointY >= this.pipeDesY) {
                                this.footPointY = this.pipeDesY;
                            }
                        } else if (this.footPointY > this.pipeDesY) {
                            this.footPointY -= 250;
                            if (this.footPointY <= this.pipeDesY) {
                                this.footPointY = this.pipeDesY;
                            }
                        }
                        if (this.footPointX == this.pipeDesX && this.footPointY == this.pipeDesY) {
                            this.pipeState = (byte) 1;
                            this.velX = this.nextVelX;
                            this.velY = this.nextVelY;
                            break;
                        }
                    case (byte) 1:
                        this.footPointX += this.velX;
                        this.footPointY += this.velY;
                        break;
                    case (byte) 2:
                        this.footPointX += this.velX;
                        this.footPointY += this.velY;
                        if (this.velX != 0) {
                            if (this.velX > 0 && this.footPointX > this.pipeDesX) {
                                this.footPointX = this.pipeDesX;
                            } else if (this.velX < 0 && this.footPointX < this.pipeDesX) {
                                this.footPointX = this.pipeDesX;
                            }
                        }
                        if (this.velY != 0) {
                            if (this.velY > 0 && this.footPointY > this.pipeDesY) {
                                this.footPointY = this.pipeDesY;
                            } else if (this.velY < 0 && this.footPointY < this.pipeDesY) {
                                this.footPointY = this.pipeDesY;
                            }
                        }
                        if ((this.velX == 0 || this.footPointX == this.pipeDesX || this.nextVelX != 0) && (this.velY == 0 || this.footPointY == this.pipeDesY || this.nextVelY != 0)) {
                            this.velX = this.nextVelX;
                            this.velY = this.nextVelY;
                            this.pipeState = (byte) 1;
                            break;
                        }
                        break;
                }
                checkWithObject(preX, preY, this.footPointX, this.footPointY);
                this.animationID = 4;
            } else {
                bankLogic();
                if (!this.onBank) {
                    if (isTerminal) {
                        if (this.terminalCount > 0) {
                            this.terminalCount--;
                        }
                        if (this.animationID == 4) {
                            this.totalVelocity -= MOVE_POWER_REVERSE_BALL;
                            if (this.totalVelocity < 0) {
                                this.totalVelocity = 0;
                            }
                        } else if (this.totalVelocity > MAX_VELOCITY) {
                            this.totalVelocity -= MOVE_POWER_REVERSE_BALL;
                            if (this.totalVelocity <= MAX_VELOCITY) {
                                this.totalVelocity = MAX_VELOCITY;
                            }
                        }
                        this.noKeyFlag = true;
                    }
                    if (this.isCelebrate) {
                        if (this.faceDirection) {
                            if (this.collisionState == (byte) 0) {
                                setVelX(0);
                            }
                        } else if (this.collisionState == (byte) 0) {
                            setVelX(0);
                        }
                        this.noKeyFlag = true;
                    }
                    if (StageManager.getStageID() != 11) {
                        if (!isFirstTouchedWind && this.animationID == 29) {
                            soundInstance.playSe(68);
                            isFirstTouchedWind = true;
                            this.frameCnt = 0;
                        }
                        if (isFirstTouchedWind) {
                            if (this.animationID == 29) {
                                this.frameCnt++;
                                if (this.frameCnt > 4 && !IsGamePause) {
                                    soundInstance.playLoopSe(69);
                                }
                            } else {
                                if (soundInstance.getPlayingLoopSeIndex() == 69) {
                                    soundInstance.stopLoopSe();
                                }
                                isFirstTouchedWind = false;
                            }
                        }
                    }
                    if (StageManager.getCurrentZoneId() == 5) {
                        if (!isFirstTouchedSandSlip && this.animationID == 30) {
                            isFirstTouchedSandSlip = true;
                            this.frameCnt = 0;
                        }
                        if (isFirstTouchedSandSlip) {
                            if (this.animationID == 30 && this.collisionState == (byte) 0) {
                                this.frameCnt++;
                                if (this.frameCnt > 2 && !IsGamePause) {
                                    soundInstance.playLoopSe(71);
                                }
                            } else {
                                if (soundInstance.getPlayingLoopSeIndex() == 71) {
                                    soundInstance.stopLoopSe();
                                }
                                isFirstTouchedSandSlip = false;
                            }
                        }
                    }
                    if (this.ducting) {
                        this.ductingCount++;
                        this.noKeyFlag = true;
                        this.animationID = 4;
                        this.attackAnimationID = this.animationID;
                        this.attackCount = 0;
                        this.attackLevel = 0;
                    }
                    if (this.noKeyFlag) {
                        Key.setKeyFunction(false);
                    }
                    if (!(!this.hurtNoControl || this.animationID == 12 || this.animationID == 44)) {
                        this.hurtNoControl = false;
                    }
                    switch (this.collisionState) {
                        case (byte) 0:
                            inputLogicWalk();
                            break;
                        case (byte) 1:
                            inputLogicJump();
                            if (this.transing) {
                                this.velX = 0;
                                this.velY = 0;
                                if (MapManager.isCameraStop()) {
                                    this.transing = false;
                                    break;
                                }
                            }
                            break;
                        case (byte) 2:
                            inputLogicOnObject();
                            break;
                        case (byte) 3:
                            inputLogicSand();
                            break;
                        default:
                            extraInputLogic();
                            break;
                    }
                    if (this.noKeyFlag) {
                        Key.setKeyFunction(true);
                        this.noKeyFlag = false;
                    }
                    if (this.slipFlag) {
                        if (this.collisionState == (byte) 0) {
                            this.animationID = 30;
                        }
                        this.slipFlag = false;
                    }
                    calPreCollisionRect();
                    collisionChk();
                    if (this.animationID == 17) {
                        Effect.showEffect(this.dustEffectAnimation, 2, this.posX >> 6, this.posY >> 6, 0);
                    }
                    switch (this.collisionState) {
                        case (byte) 0:
                            fallChk();
                            this.degreeForDraw = this.faceDegree;
                            if (noRotateDraw()) {
                                this.degreeForDraw = this.degreeStable;
                            }
                            if (isTerminal) {
                                MapManager.setCameraDownLimit((this.posY >> 6) + 24);
                            }
                            if (!isTerminal || this.terminalCount != 0 || this.totalVelocity < MAX_VELOCITY) {
                                switch (terminalType) {
                                    case 3:
                                        terminalLogic();
                                        break;
                                    default:
                                        break;
                                }
                            }
                            switch (terminalType) {
                                case 0:
                                    if (this.animationID == 35 && this.drawer.checkEnd()) {
                                        this.animationID = 36;
                                    }
                                    if (!(this.animationID == 4 || this.animationID == 35 || this.animationID == 36)) {
                                        this.animationID = 35;
                                        break;
                                    }
                                case 2:
                                    if (StageManager.getCurrentZoneId() != 6) {
                                        if (this.fading) {
                                            if (fadeChangeOver()) {
                                                StageManager.setStagePass();
                                                break;
                                            }
                                        }
                                        setFadeColor(MapManager.END_COLOR);
                                        fadeInit(0, 255);
                                        this.fading = true;
                                        break;
                                    }
                                    StageManager.setStagePass();
                                    break;
                                case 3:
                                    terminalLogic();
                                    break;
                            }
                            if (this.isCelebrate) {
                                this.animationID = 37;
                                break;
                            }
                            break;
                        case (byte) 1:
                            if (noRotateDraw()) {
                                this.degreeForDraw = this.degreeStable;
                            }
                            terminalLogic();
                            if (isTerminal && this.terminalCount == 0 && terminalType == 1) {
                                StageManager.setStagePass();
                                break;
                            }
                        case (byte) 2:
                        case (byte) 3:
                            this.degreeForDraw = this.faceDegree;
                            break;
                        case (byte) 4:
                            terminalLogic();
                            break;
                    }
                    if (this.footPointX - 512 < (MapManager.actualLeftCameraLimit << 6)) {
                        this.footPointX = (MapManager.actualLeftCameraLimit << 6) + 512;
                        if (getVelX() < 0) {
                            setVelX(0);
                        }
                    }
                    if (MapManager.actualRightCameraLimit != MapManager.getPixelWidth() && this.footPointX + 512 > (MapManager.actualRightCameraLimit << 6)) {
                        this.footPointX = (MapManager.actualRightCameraLimit << 6) - 512;
                        if (getVelX() > 0) {
                            setVelX(0);
                        }
                    }
                    if (EnemyObject.isBossEnter) {
                        if ((this.footPointY - 1536) + 1024 < (MapManager.actualUpCameraLimit << 6)) {
                            this.footPointY = ((MapManager.actualUpCameraLimit << 6) + 1536) - 1024;
                            if (getVelY() < 0) {
                                setVelY(0);
                            }
                        }
                    } else if (this.footPointY - 1536 < (MapManager.actualUpCameraLimit << 6)) {
                        this.footPointY = (MapManager.actualUpCameraLimit << 6) + 1536;
                        if (getVelY() < 0) {
                            setVelY(0);
                        }
                    }
                    if (isDeadLineEffect && !this.isDead && this.footPointY > (MapManager.actualDownCameraLimit << 6)) {
                        this.footPointY = ((MapManager.getCamera().f14y + MapManager.CAMERA_HEIGHT) << 6) + 3072;
                        setDie(false, -1600);
                    }
                    if (this.leftStopped && this.rightStopped) {
                        setDie(false);
                    }
                }
            }
        } else if (this.outOfControlObject != null) {
            this.outOfControlObject.logic();
            this.controlObjectLogic = true;
        }
    }


    public void logic() {
        for(int var1 = 0; var1 < 5; ++var1) {
            if(itemVec[var1][0] >= 0) {
                if(itemVec[var1][1] > 0) {
                    int[] var2 = itemVec[var1];
                    --var2[1];
                }

                if(itemVec[var1][1] == 0) {
                    this.getItem(itemVec[var1][0]);
                    itemVec[var1][0] = -1;
                }
            }
        }

        short var3;
        if(this.isAntiGravity) {
            var3 = 180;
        } else {
            var3 = 0;
        }

        this.degreeStable = var3;
        this.leftStopped = false;
        this.rightStopped = false;
        if(this.enteringSP && this.posY >> 6 < camera.f14y) {
            GameState.enterSpStage(ringNum, currentMarkId, timeCount);
            this.enteringSP = false;
        }

        if(this.hurtCount > 0) {
            --this.hurtCount;
        }

        if(invincibleCount > 0) {
            --invincibleCount;
            if(invincibleCount == 0) {
                int var37 = SoundSystem.getInstance().getPlayingBGMIndex();
                SoundSystem.getInstance();
                if(var37 == 44) {
                    SoundSystem.getInstance().stopBgm(false);
                    if(!isTerminal) {
                        SoundSystem.getInstance().playBgm(StageManager.getBgmId());
                    }
                }

                int var39 = SoundSystem.getInstance().getPlayingBGMIndex();
                SoundSystem.getInstance();
                if(var39 == 43) {
                    SoundSystem.getInstance().playNextBgm(StageManager.getBgmId());
                }
            }
        }

        this.preFocusX = this.getNewPointX(this.footPointX, 0, -768, this.faceDegree) >> 6;
        this.preFocusY = this.getNewPointY(this.footPointY, 0, -768, this.faceDegree) >> 6;
        if(this.setNoMoving) {
            if(this.collisionState == 0) {
                this.footPointX = this.noMovingPosition;
                this.setVelX(0);
                this.setVelY(0);
                this.animationID = 0;
                return;
            }

            if(this.collisionState == 1) {
                this.footPointX = this.noMovingPosition;
                this.velX = 0;
                this.setNoKey();
            }
        }

        if(this.collisionState == 0) {
            this.deadPosX = this.footPointX;
            this.deadPosY = this.footPointY;
        }

        if(characterID == 1) {
            if(this.myAnimationID != 12 && this.myAnimationID != 48 && this.myAnimationID != 49) {
                if(soundInstance.getPlayingLoopSeIndex() == 15) {
                    soundInstance.stopLoopSe();
                }

                this.resetFlyCount();
            }

            if(this.collisionState == 0) {
                if(soundInstance.getPlayingLoopSeIndex() == 15) {
                    soundInstance.stopLoopSe();
                }

                this.resetFlyCount();
            }
        }

        if(this.isDead) {
            if(this.isInWater && this.breatheNumCount >= 6) {
                ++this.drownCnt;
                if(this.drownCnt % 2 == 0) {
                    addGameObject(new DrownBubble(41, this.footPointX, this.footPointY - 1536, 0, 0, 0, 0));
                }
            }

            boolean var33;
            if(!this.isAntiGravity) {
                int var34 = this.velY;
                var33 = false;
                if(var34 > 0) {
                    int var35 = this.footPointY;
                    int var36 = 3072 + (MapManager.getCamera().f14y + MapManager.CAMERA_HEIGHT << 6);
                    var33 = false;
                    if(var35 > var36) {
                        this.footPointY = 3072 + (MapManager.getCamera().f14y + MapManager.CAMERA_HEIGHT << 6);
                        var33 = true;
                    }
                }
            } else {
                int var31 = this.footPointY;
                int var32 = (MapManager.getCamera().f14y << 6) - 3072;
                var33 = false;
                if(var31 < var32) {
                    this.footPointY = (MapManager.getCamera().f14y << 6) - 3072;
                    var33 = true;
                }
            }

            if(var33 && !this.finishDeadStuff) {
                if(stageModeState == 1) {
                    StageManager.setStageRestart();
                } else if(timeCount != overTime || !GlobalResource.timeIsLimit()) {
                    if(lifeNum > 0) {
                        --lifeNum;
                        StageManager.setStageRestart();
                    } else {
                        StageManager.setStageGameover();
                    }
                }

                this.finishDeadStuff = true;
                return;
            }
        } else {
            this.focusMovingState = 0;
            this.controlObjectLogic = false;
            if(this.outOfControl) {
                if(this.outOfControlObject != null) {
                    this.outOfControlObject.logic();
                    this.controlObjectLogic = true;
                    return;
                }
            } else {
                int var4 = StageManager.getWaterLevel();
                if(var4 > 0) {
                    if(characterID == 2) {
                        ((PlayerKnuckles)player).setPreWaterFlag(this.isInWater);
                    }

                    if(this.isInWater) {
                        if(!IsGamePause) {
                            this.breatheCount += 63;
                            this.breatheNumCount = -1;
                            PlayerObject var10000;
                            if(characterID == 2 && this.collisionState == 4 && 256 + this.getNewPointY(this.posY, 0, -this.collisionRect.getHeight(), this.faceDegree) < var4 << 6) {
                                this.breatheCount = 0;
                                int var28 = SoundSystem.getInstance().getPlayingBGMIndex();
                                SoundSystem.getInstance();
                                if(var28 == 21) {
                                    SoundSystem.getInstance().stopBgm(false);
                                    var10000 = player;
                                    if(IsInvincibility()) {
                                        SoundSystem.getInstance().playBgm(44);
                                    } else if(this.isAttackBoss4) {
                                        SoundSystem.getInstance().playBgm(22);
                                    } else {
                                        SoundSystem.getInstance().playBgm(StageManager.getBgmId());
                                    }
                                }
                            }

                            if(this.breatheCount > 21000) {
                                label624: {
                                    this.breatheNumCount = (this.breatheCount - 21000) / 1760;
                                    if(this.breatheCount == 0) {
                                        int var26 = SoundSystem.getInstance().getPlayingBGMIndex();
                                        SoundSystem.getInstance();
                                        if(var26 != 21) {
                                            if(this.isAttackBoss4) {
                                                soundInstance.playBgm(21);
                                            } else {
                                                soundInstance.playBgm(21);
                                            }
                                            break label624;
                                        }
                                    }

                                    int var22 = SoundSystem.getInstance().getPlayingBGMIndex();
                                    SoundSystem.getInstance();
                                    if(var22 != 21) {
                                        long var24 = (long)(10000 * (this.breatheCount - 21000) / 10560);
                                        if(this.isAttackBoss4) {
                                            soundInstance.playBgmFromTime(var24, 21);
                                        } else {
                                            soundInstance.playBgmFromTime(var24, 21);
                                        }
                                    }
                                }

                                if(this.breatheNumCount >= 6 && this.canBeHurt()) {
                                    this.setDie(true);
                                    return;
                                }

                                if(this.breatheNumCount != this.preBreatheNumCount) {
                                    this.breatheNumY = (this.posY >> 6) - camera.f14y - 30;
                                }
                            }

                            this.preBreatheNumCount = this.breatheNumCount;
                            int var18 = this.getNewPointY(this.posY, 0, -this.collisionRect.getHeight() >> 1, this.faceDegree);
                            if(characterID == 3) {
                                var18 = this.getNewPointY(this.posY, 0, 3 * -this.collisionRect.getHeight() / 4 - 128, this.faceDegree);
                            }

                            if(var18 + 256 <= var4 << 6) {
                                this.isInWater = false;
                                if(this.breatheNumCount >= 0 && SoundSystem.getInstance().getPlayingBGMIndex() == 21) {
                                    SoundSystem.getInstance().stopBgm(false);
                                    var10000 = player;
                                    if(IsInvincibility()) {
                                        SoundSystem.getInstance().playBgm(44);
                                    } else if(this.isAttackBoss4) {
                                        SoundSystem.getInstance().playBgm(22);
                                    } else {
                                        SoundSystem.getInstance().playBgm(StageManager.getBgmId());
                                    }
                                }

                                if(isNeedPlayWaterSE) {
                                    SoundSystem.getInstance().playSe(58);
                                }

                                this.waterSprayFlag = true;
                                this.waterSprayX = this.posX;
                                waterSprayDrawer.restart();
                            }

                            ++this.breatheFrame;
                            this.breatheFrame %= 51;
                            if(this.breatheFrame == 6 * MyRandom.nextInt(1, 8)) {
                                int var19 = player.getFootPositionX();
                                short var20;
                                if(this.faceDirection) {
                                    var20 = 384;
                                } else {
                                    var20 = -384;
                                }

                                addGameObject(new AspirateBubble(40, var19 + var20, player.getFootPositionY() - 1536, 0, 0, 0, 0));
                            }
                        }
                    } else {
                        this.breatheCount = 0;
                        this.breatheNumCount = -1;
                        this.preBreatheNumCount = -1;
                        if(this.getNewPointY(this.posY, 0, -this.collisionRect.getHeight() >> 1, this.faceDegree) - 256 >= var4 << 6) {
                            this.isInWater = true;
                            if(isNeedPlayWaterSE) {
                                SoundSystem.getInstance().playSe(58);
                            }

                            this.waterSprayFlag = true;
                            this.waterSprayX = this.posX;
                            waterSprayDrawer.restart();
                        }
                    }
                }

                if(speedCount > 0) {
                    --speedCount;
                    this.movePower = MOVE_POWER << 1;
                    this.movePowerInAir = MOVE_POWER_IN_AIR << 1;
                    this.movePowerReverse = MOVE_POWER_REVERSE << 1;
                    this.movePowerReserseBall = MOVE_POWER_REVERSE_BALL << 1;
                    this.maxVelocity = MAX_VELOCITY << 1;
                    if(speedCount == 0 && SoundSystem.getInstance().getPlayingBGMIndex() != 42 && SoundSystem.getInstance().getPlayingBGMIndex() != 41 && SoundSystem.getInstance().getPlayingBGMIndex() != 26) {
                        SoundSystem.getInstance().setSoundSpeed(1.0F);
                        if(SoundSystem.getInstance().getPlayingBGMIndex() != 43) {
                            SoundSystem.getInstance().restartBgm();
                        }
                    }
                } else {
                    this.movePower = MOVE_POWER;
                    this.movePowerInAir = MOVE_POWER_IN_AIR;
                    this.movePowerReverse = MOVE_POWER_REVERSE;
                    this.movePowerReserseBall = MOVE_POWER_REVERSE_BALL;
                    this.maxVelocity = MAX_VELOCITY;
                }

                if(this.isAntiGravity) {
                    if(!this.isDead && this.footPointY > MapManager.getPixelHeight() << 6) {
                        this.footPointY = MapManager.getCamera().f14y + MapManager.CAMERA_HEIGHT << 6;
                        if(this.getVelY() < 0) {
                            this.setVelY(0);
                        }
                    }
                } else if(!this.isDead && this.footPointY > MapManager.getPixelHeight() << 6) {
                    this.footPointY = 3072 + (MapManager.getCamera().f14y + MapManager.CAMERA_HEIGHT << 6);
                    this.setDie(false, -1600);
                }

                this.ignoreFirstTouch = false;
                if(this.dashRolling) {
                    this.dashRollingLogic();
                    if(this.dashRolling) {
                        this.collisionChk();
                        return;
                    }
                } else if(this.effectID == 0 || this.effectID == 1) {
                    this.effectID = -1;
                }

                if(this.railing) {
                    this.setNoKey();
                    if(this.railLine == null) {
                        this.velY += this.getGravity();
                        this.checkWithObject(this.footPointX, this.footPointY, this.footPointX + this.velX, this.footPointY + this.velY);
                    } else {
                        int var7 = this.footPointX;
                        int var8 = this.footPointY;
                        int var9 = this.railLine.sin(this.getGravity());
                        if(!this.railLine.directRatio()) {
                            var9 = -var9;
                        }

                        if(var9 != 0) {
                            Direction var17 = this.railLine.getOneDirection();
                            this.totalVelocity += var9;
                            this.checkWithObject(this.footPointX, this.footPointY, this.footPointX + var17.getValueX(this.railLine.cos(this.totalVelocity)), this.footPointY + var17.getValueY(this.railLine.sin(this.totalVelocity)));
                        } else {
                            int var10 = this.footPointX;
                            int var11 = this.footPointY;
                            int var12 = this.footPointX;
                            byte var13;
                            if(this.totalVelocity < 0) {
                                var13 = -1;
                            } else {
                                var13 = 1;
                            }

                            int var14 = var12 + var13 * this.railLine.cos(this.totalVelocity);
                            int var15 = this.footPointY;
                            byte var16;
                            if(this.totalVelocity < 0) {
                                var16 = -1;
                            } else {
                                var16 = 1;
                            }

                            this.checkWithObject(var10, var11, var14, var15 + var16 * this.railLine.sin(this.totalVelocity));
                        }

                        if(!this.railOut && this.railLine != null) {
                            this.velX = this.footPointX - var7;
                            this.velY = this.footPointY - var8;
                        }
                    }

                    if(this.railOut && this.velY == -1200 + this.getGravity()) {
                        if(characterID == 3) {
                            soundInstance.playSe(25);
                        } else {
                            soundInstance.playSe(37);
                        }
                    }

                    if(this.railOut && this.velY > 0) {
                        this.railOut = false;
                        this.railing = false;
                        this.collisionState = 1;
                        return;
                    }
                } else {
                    if(this.piping) {
                        int var5 = this.footPointX;
                        int var6 = this.footPointY;
                        switch(this.pipeState) {
                            case 0:
                                if(this.footPointX < this.pipeDesX) {
                                    this.footPointX += 250;
                                    if(this.footPointX >= this.pipeDesX) {
                                        this.footPointX = this.pipeDesX;
                                    }
                                } else if(this.footPointX > this.pipeDesX) {
                                    this.footPointX -= 250;
                                    if(this.footPointX <= this.pipeDesX) {
                                        this.footPointX = this.pipeDesX;
                                    }
                                }

                                if(this.footPointY < this.pipeDesY) {
                                    this.footPointY += 250;
                                    if(this.footPointY >= this.pipeDesY) {
                                        this.footPointY = this.pipeDesY;
                                    }
                                } else if(this.footPointY > this.pipeDesY) {
                                    this.footPointY -= 250;
                                    if(this.footPointY <= this.pipeDesY) {
                                        this.footPointY = this.pipeDesY;
                                    }
                                }

                                if(this.footPointX == this.pipeDesX && this.footPointY == this.pipeDesY) {
                                    this.pipeState = 1;
                                    this.velX = this.nextVelX;
                                    this.velY = this.nextVelY;
                                }
                                break;
                            case 1:
                                this.footPointX += this.velX;
                                this.footPointY += this.velY;
                                break;
                            case 2:
                                this.footPointX += this.velX;
                                this.footPointY += this.velY;
                                if(this.velX != 0) {
                                    if(this.velX > 0 && this.footPointX > this.pipeDesX) {
                                        this.footPointX = this.pipeDesX;
                                    } else if(this.velX < 0 && this.footPointX < this.pipeDesX) {
                                        this.footPointX = this.pipeDesX;
                                    }
                                }

                                if(this.velY != 0) {
                                    if(this.velY > 0 && this.footPointY > this.pipeDesY) {
                                        this.footPointY = this.pipeDesY;
                                    } else if(this.velY < 0 && this.footPointY < this.pipeDesY) {
                                        this.footPointY = this.pipeDesY;
                                    }
                                }

                                if((this.velX == 0 || this.footPointX == this.pipeDesX || this.nextVelX != 0) && (this.velY == 0 || this.footPointY == this.pipeDesY || this.nextVelY != 0)) {
                                    this.velX = this.nextVelX;
                                    this.velY = this.nextVelY;
                                    this.pipeState = 1;
                                }
                        }

                        this.checkWithObject(var5, var6, this.footPointX, this.footPointY);
                        this.animationID = 4;
                        return;
                    }

                    this.bankLogic();
                    if(!this.onBank) {
                        if(isTerminal) {
                            if(this.terminalCount > 0) {
                                --this.terminalCount;
                            }

                            if(this.animationID == 4) {
                                this.totalVelocity -= MOVE_POWER_REVERSE_BALL;
                                if(this.totalVelocity < 0) {
                                    this.totalVelocity = 0;
                                }
                            } else if(this.totalVelocity > MAX_VELOCITY) {
                                this.totalVelocity -= MOVE_POWER_REVERSE_BALL;
                                if(this.totalVelocity <= MAX_VELOCITY) {
                                    this.totalVelocity = MAX_VELOCITY;
                                }
                            }

                            this.noKeyFlag = true;
                        }

                        if(this.isCelebrate) {
                            if(this.faceDirection) {
                                if(this.collisionState == 0) {
                                    this.setVelX(0);
                                }
                            } else if(this.collisionState == 0) {
                                this.setVelX(0);
                            }

                            this.noKeyFlag = true;
                        }

                        if(StageManager.getStageID() != 11) {
                            if(!isFirstTouchedWind && this.animationID == 29) {
                                soundInstance.playSe(68);
                                isFirstTouchedWind = true;
                                this.frameCnt = 0;
                            }

                            if(isFirstTouchedWind) {
                                if(this.animationID == 29) {
                                    ++this.frameCnt;
                                    if(this.frameCnt > 4 && !IsGamePause) {
                                        soundInstance.playLoopSe(69);
                                    }
                                } else {
                                    if(soundInstance.getPlayingLoopSeIndex() == 69) {
                                        soundInstance.stopLoopSe();
                                    }

                                    isFirstTouchedWind = false;
                                }
                            }
                        }

                        if(StageManager.getCurrentZoneId() == 5) {
                            if(!isFirstTouchedSandSlip && this.animationID == 30) {
                                isFirstTouchedSandSlip = true;
                                this.frameCnt = 0;
                            }

                            if(isFirstTouchedSandSlip) {
                                if(this.animationID == 30 && this.collisionState == 0) {
                                    ++this.frameCnt;
                                    if(this.frameCnt > 2 && !IsGamePause) {
                                        soundInstance.playLoopSe(71);
                                    }
                                } else {
                                    if(soundInstance.getPlayingLoopSeIndex() == 71) {
                                        soundInstance.stopLoopSe();
                                    }

                                    isFirstTouchedSandSlip = false;
                                }
                            }
                        }

                        if(this.ducting) {
                            ++this.ductingCount;
                            this.noKeyFlag = true;
                            this.animationID = 4;
                            this.attackAnimationID = this.animationID;
                            this.attackCount = 0;
                            this.attackLevel = 0;
                        }

                        if(this.noKeyFlag) {
                            Key.setKeyFunction(false);
                        }

                        if(this.hurtNoControl && this.animationID != 12 && this.animationID != 44) {
                            this.hurtNoControl = false;
                        }

                        switch(this.collisionState) {
                            case 0:
                                this.inputLogicWalk();
                                break;
                            case 1:
                                this.inputLogicJump();
                                if(this.transing) {
                                    this.velX = 0;
                                    this.velY = 0;
                                    if(MapManager.isCameraStop()) {
                                        this.transing = false;
                                    }
                                }
                                break;
                            case 2:
                                this.inputLogicOnObject();
                                break;
                            case 3:
                                this.inputLogicSand();
                                break;
                            default:
                                this.extraInputLogic();
                        }

                        if(this.noKeyFlag) {
                            Key.setKeyFunction(true);
                            this.noKeyFlag = false;
                        }

                        if(this.slipFlag) {
                            if(this.collisionState == 0) {
                                this.animationID = 30;
                            }

                            this.slipFlag = false;
                        }

                        this.calPreCollisionRect();
                        this.collisionChk();
                        if(this.animationID == 17) {
                            Effect.showEffect(this.dustEffectAnimation, 2, this.posX >> 6, this.posY >> 6, 0);
                        }

                        switch(this.collisionState) {
                            case 0:
                                this.fallChk();
                                this.degreeForDraw = this.faceDegree;
                                if(this.noRotateDraw()) {
                                    this.degreeForDraw = this.degreeStable;
                                }

                                if(isTerminal) {
                                    MapManager.setCameraDownLimit(24 + (this.posY >> 6));
                                }

                                if(isTerminal && this.terminalCount == 0 && this.totalVelocity >= MAX_VELOCITY) {
                                    switch(terminalType) {
                                        case 0:
                                            if(this.animationID == 35 && this.drawer.checkEnd()) {
                                                this.animationID = 36;
                                            }

                                            if(this.animationID != 4 && this.animationID != 35 && this.animationID != 36) {
                                                this.animationID = 35;
                                            }
                                        case 1:
                                        default:
                                            break;
                                        case 2:
                                            if(StageManager.getCurrentZoneId() == 6) {
                                                StageManager.setStagePass();
                                            } else if(!this.fading) {
                                                setFadeColor(16777215);
                                                fadeInit(0, 255);
                                                this.fading = true;
                                            } else if(fadeChangeOver()) {
                                                StageManager.setStagePass();
                                            }
                                            break;
                                        case 3:
                                            this.terminalLogic();
                                    }
                                } else {
                                    switch(terminalType) {
                                        case 3:
                                            this.terminalLogic();
                                    }
                                }

                                if(this.isCelebrate) {
                                    this.animationID = 37;
                                }
                                break;
                            case 1:
                                if(this.noRotateDraw()) {
                                    this.degreeForDraw = this.degreeStable;
                                }

                                this.terminalLogic();
                                if(isTerminal && this.terminalCount == 0 && terminalType == 1) {
                                    StageManager.setStagePass();
                                }
                                break;
                            case 2:
                            case 3:
                                this.degreeForDraw = this.faceDegree;
                                break;
                            case 4:
                                this.terminalLogic();
                        }

                        if(this.footPointX - 512 < MapManager.actualLeftCameraLimit << 6) {
                            this.footPointX = 512 + (MapManager.actualLeftCameraLimit << 6);
                            if(this.getVelX() < 0) {
                                this.setVelX(0);
                            }
                        }

                        if(MapManager.actualRightCameraLimit != MapManager.getPixelWidth() && 512 + this.footPointX > MapManager.actualRightCameraLimit << 6) {
                            this.footPointX = (MapManager.actualRightCameraLimit << 6) - 512;
                            if(this.getVelX() > 0) {
                                this.setVelX(0);
                            }
                        }

                        if(EnemyObject.isBossEnter) {
                            if(1024 + (this.footPointY - 1536) < MapManager.actualUpCameraLimit << 6) {
                                this.footPointY = 1536 + (MapManager.actualUpCameraLimit << 6) - 1024;
                                if(this.getVelY() < 0) {
                                    this.setVelY(0);
                                }
                            }
                        } else if(this.footPointY - 1536 < MapManager.actualUpCameraLimit << 6) {
                            this.footPointY = 1536 + (MapManager.actualUpCameraLimit << 6);
                            if(this.getVelY() < 0) {
                                this.setVelY(0);
                            }
                        }

                        if(isDeadLineEffect && !this.isDead && this.footPointY > MapManager.actualDownCameraLimit << 6) {
                            this.footPointY = 3072 + (MapManager.getCamera().f14y + MapManager.CAMERA_HEIGHT << 6);
                            this.setDie(false, -1600);
                        }

                        if(this.leftStopped && this.rightStopped) {
                            this.setDie(false);
                            return;
                        }
                    }
                }
            }
        }

    }



    public void terminalLogic() {
        if (terminalType == 3) {
            switch (terminalState) {
                case (byte) 0:
                    if (this.posX > SUPER_SONIC_STAND_POS_X) {
                        terminalState = (byte) 1;
                        return;
                    }
                    return;
                case (byte) 1:
                    if (this.totalVelocity == 0 && this.animationID == 0) {
                        terminalState = (byte) 2;
                        this.terminalCount = 10;
                        return;
                    }
                    return;
                case (byte) 2:
                    if (this.terminalCount == 0) {
                        StageManager.setOnlyScoreCal();
                        StageManager.setStagePass();
                        terminalState = (byte) 3;
                        return;
                    }
                    return;
                case (byte) 3:
                    if (this.terminalCount == 0 && StageManager.isScoreBarOut()) {
                        terminalState = (byte) 4;
                        this.collisionState = (byte) 4;
                        this.velY = this.isInWater ? JUMP_INWATER_START_VELOCITY : JUMP_START_VELOCITY;
                        this.velX = 0;
                        this.worldCal.actionState = (byte) 1;
                        MapManager.setCameraUpLimit(MapManager.getCamera().f14y);
                        return;
                    }
                    return;
                case (byte) 4:
                    this.velY += getGravity();
                    this.collisionState = (byte) 4;
                    if (this.posY <= SUPER_SONIC_CHANGING_CENTER_Y) {
                        this.velY = -100;
                        terminalState = (byte) 5;
                        this.terminalCount = 60;
                        return;
                    }
                    return;
                case (byte) 5:
                    this.collisionState = (byte) 4;
                    if (this.posY <= SUPER_SONIC_CHANGING_CENTER_Y) {
                        this.velY += 30;
                    } else {
                        this.velY -= 30;
                    }
                    if (this.terminalCount == 0) {
                        this.velY = 0;
                        this.velX = 0;
                        MapManager.setCameraRightLimit(MapManager.getPixelWidth());
                        MapManager.setFocusObj(null);
                        this.terminalCount = 30;
                        terminalState = (byte) 6;
                        this.posY -= 2112;
                        this.footPointY = this.posY;
                        return;
                    }
                    return;
                case (byte) 6:
                    this.collisionState = (byte) 4;
                    this.terminalOffset += 1600;
                    if (this.terminalCount == 0) {
                        this.terminalCount = 100;
                        terminalState = (byte) 7;
                        return;
                    }
                    return;
                case (byte) 7:
                    if (this.terminalCount == 0) {
                        StageManager.setStraightlyPass();
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    public void drawCharacter(MFGraphics g) {
    }

    public void draw2(MFGraphics g) {
        boolean z = drawAtFront() && this.visible;
        draw(g, z);
        drawCollisionRect(g);
        if (this.waterSprayFlag && StageManager.getCurrentZoneId() == 4 && waterSprayDrawer != null) {
            waterSprayDrawer.draw(g, 0, (this.waterSprayX >> 6) - camera.f13x, StageManager.getWaterLevel() - camera.f14y, false, 0);
            if (waterSprayDrawer.checkEnd()) {
                this.waterSprayFlag = false;
                waterSprayDrawer.restart();
            }
        }
        if (!IsGamePause) {
            if (this.isDead) {
                this.velY += (this.isAntiGravity ? -1 : 1) * getGravity();
                this.footPointX += this.velX;
                this.footPointY += this.velY;
            }
            if (this.isInWater && this.breatheNumCount >= 0 && this.breatheNumCount < 6) {
                int i;
                MFImage mFImage = breatheCountImage;
                int i2 = this.breatheNumCount * 16;
                int i3 = (this.posX >> 6) - camera.f13x;
                if (this.breatheNumY > 16) {
                    i = this.breatheNumY;
                } else {
                    i = 16;
                }
                MyAPI.drawRegion(g, mFImage, i2, 0, 16, 16, 0, i3, i, 33);
                this.breatheNumY--;
            }
        }
        if (this.fading) {
            drawFadeBase(g, 12);
        }
        if (terminalType == 3) {
            if (terminalState < (byte) 2 || terminalState >= (byte) 6) {
                this.moonStarFrame1 = 0;
            } else {
                moonStarDrawer.draw(g, 0, (((MOON_STAR_DES_X_1 - MOON_STAR_ORI_X_1) * this.moonStarFrame1) / MOON_STAR_FRAMES_1) + MOON_STAR_ORI_X_1, ((this.moonStarFrame1 * 8) / MOON_STAR_FRAMES_1) + 18, true, 0);
                this.moonStarFrame1++;
            }
            if (terminalState == (byte) 7) {
                moonStarDrawer.draw(g, 1, (((MOON_STAR_DES_X_1 - MOON_STAR_ORI_X_1) * this.moonStarFrame2) / MOON_STAR_FRAMES_2) + MOON_STAR_ORI_X_1, ((this.moonStarFrame2 * 8) / MOON_STAR_FRAMES_2) + 18, true, 0);
                this.moonStarFrame2++;
                return;
            }
            this.moonStarFrame2 = 0;
        }
    }

    public boolean drawAtFront() {
        return this.slipping || this.isDead;
    }

    public void collisionChk() {
        if (!this.noMoving) {
            switch (this.collisionState) {
                case (byte) 0:
                    calDivideVelocity(this.faceDegree);
                    break;
            }
            this.posZ = this.currentLayer;
            this.worldCal.footDegree = this.faceDegree;
            this.posX = this.footPointX;
            this.posY = this.footPointY;
            if (this.collisionState == (byte) 2) {
                collisionLogicOnObject();
            } else if (this.isInWater) {
                this.worldCal.actionLogic(this.velX >> 1, this.velY >> 1, (int) ((((float) this.totalVelocity) * IN_WATER_WALK_SPEED_SCALE1) / IN_WATER_WALK_SPEED_SCALE2));
            } else if (this.movedSpeedX != 0) {
                this.worldCal.actionLogic(this.movedSpeedX, this.velY);
            } else {
                this.worldCal.actionLogic(this.velX, this.velY, this.totalVelocity);
            }
            this.footPointX = this.posX;
            this.footPointY = this.posY;
            this.faceDegree = this.worldCal.footDegree;
        }
    }

    public void setFaceDegree(int degree) {
        this.worldCal.footDegree = degree;
        this.faceDegree = degree;
    }

    public void draw(MFGraphics g) {
        boolean z = !drawAtFront() && this.visible;
        draw(g, z);
    }

    public void draw(MFGraphics g, boolean visible) {
        if (visible) {
            switch (this.collisionState) {
                case (byte) 0:
                    if (noRotateDraw()) {
                        this.degreeForDraw = this.degreeStable;
                        break;
                    }
                    break;
            }
            if (this.isInWater) {
                this.drawer.setSpeed(1, 2);
            } else {
                this.drawer.setSpeed(1, 1);
            }
            if (this.animationID == 1) {
                if (this.isInSnow) {
                    this.drawer.setSpeed(1, 2);
                } else {
                    this.drawer.setSpeed(1, 1);
                }
            }
            drawCharacter(g);
            if (characterID == 3) {
                if (this.animationID == 4 && !IsGamePause) {
                    if (!this.ducting) {
                        soundInstance.playLoopSe(25);
                    } else if (this.ductingCount % 2 == 0) {
                        soundInstance.stopLoopSe();
                        soundInstance.playLoopSe(25);
                    }
                }
                if ((this.animationID != 4 || IsGamePause) && soundInstance.getPlayingLoopSeIndex() == 25) {
                    soundInstance.stopLoopSe();
                }
            }
            if (this.effectID > -1) {
                this.effectDrawer.draw(g, this.effectID, (this.footPointX >> 6) - camera.f13x, (this.footPointY >> 6) - camera.f14y, EFFECT_LOOP[this.effectID], getTrans());
                if (this.effectDrawer.checkEnd()) {
                    this.effectDrawer.restart();
                    this.effectID = -1;
                }
            }
            waterFallDraw(g, camera);
            waterFlushDraw(g);
            if (this.drawer.checkEnd()) {
                switch (this.animationID) {
                    case 9:
                        if (this.isInGravityCircle) {
                            this.animationID = 9;
                            this.drawer.restart();
                            return;
                        }
                        this.animationID = 10;
                        return;
                    case 15:
                        this.animationID = 16;
                        return;
                    case 22:
                        this.animationID = 23;
                        return;
                    case 23:
                        this.animationID = 22;
                        return;
                    case 25:
                        this.animationID = 26;
                        return;
                    case 26:
                        this.animationID = 25;
                        return;
                    case 31:
                        this.animationID = 1;
                        return;
                    case ANI_CELEBRATE_1:
                    case ANI_CELEBRATE_3:
                        StageManager.setStagePass();
                        return;
                    case 42:
                        this.animationID = 43;
                        return;
                    case 43:
                        this.animationID = 10;
                        return;
                    case 44:
                        this.animationID = 12;
                        return;
                    case 45:
                        this.animationID = 41;
                        return;
                    case 46:
                        if (Key.repeat(Key.gDown)) {
                            this.animationID = 5;
                            return;
                        } else {
                            this.animationID = 0;
                            return;
                        }
                    case 49:
                        this.animationID = 1;
                        return;
                    case 53:
                        this.animationID = 0;
                        return;
                    default:
                        return;
                }
            }
        }
    }

    public void drawSheild1(MFGraphics g) {
        if (!drawAtFront()) {
            drawSheildPrivate(g);
        }
    }

    public void drawSheild2(MFGraphics g) {
        if (drawAtFront()) {
            drawSheildPrivate(g);
        }
    }

    private void drawSheildPrivate(MFGraphics g) {
        int offset_x;
        int offset_y;
        int drawDegree = this.faceDegree;
        int offset = (-(getCollisionRectHeight() + PlayerSonic.BACK_JUMP_SPEED_X)) >> 1;
        if (characterID == 2 && this.myAnimationID >= 19 && this.myAnimationID <= 22) {
            offset = -384;
        } else if (this.animationID == 11 && getAnimationOffset() == 1) {
            drawDegree = 0;
            offset = -1408;
        } else if (this.animationID == 4 || this.animationID == 5 || this.animationID == 46 || this.animationID == 6 || this.animationID == 7 || this.animationID == 18 || this.animationID == 19 || this.animationID == 20) {
            offset = -640;
        } else if (this.animationID == 25 || this.animationID == 26) {
            offset = 0;
        }
        if (characterID == 0 && this.myAnimationID == 25) {
            offset_x = Def.TOUCH_HELP_LEFT_X;
            offset_y = 0;
        } else if (characterID == 3 && this.myAnimationID == 38) {
            offset_x = Def.TOUCH_HELP_LEFT_X;
            offset_y = 128;
        } else if (characterID == 3 && this.myAnimationID == 37) {
            offset_x = Def.TOUCH_HELP_LEFT_X;
            offset_y = 256;
        } else if (characterID != 2 || this.myAnimationID < 29 || this.myAnimationID > 32) {
            offset_x = 0;
            offset_y = 0;
        } else if (player.isAntiGravity) {
            if (this.faceDirection) {
                offset_x = 256;
                offset_y = -256;
            } else {
                offset_x = -256;
                offset_y = -256;
            }
        } else if (this.faceDirection) {
            offset_x = -256;
            offset_y = 256;
        } else {
            offset_x = 256;
            offset_y = 256;
        }
        int bodyCenterX = getNewPointX(this.footPointX, 0, offset, drawDegree);
        int bodyCenterY = getNewPointY(this.footPointY, 0, offset, drawDegree);
        if (invincibleCount > 0) {
            if (invincibleDrawer != null) {
                drawInMap(g, invincibleDrawer, bodyCenterX + offset_x, bodyCenterY + offset_y);
            }
            if (systemClock % 2 == 0) {
                Effect.showEffect(invincibleAnimation, 1, (bodyCenterX >> 6) + MyRandom.nextInt(-3, 3), (bodyCenterY >> 6) + MyRandom.nextInt(-3, 3), 0);
            }
        } else if (shieldType <= 0) {
        } else {
            if (shieldType == 1) {
                drawInMap(g, bariaDrawer, bodyCenterX + offset_x, bodyCenterY + offset_y);
            } else if (isAttracting()) {
                drawInMap(g, gBariaDrawer, bodyCenterX + offset_x, bodyCenterY + offset_y);
            }
        }
    }

    protected int getAnimationOffset() {
        return getAnimationOffset(this.faceDegree);
    }

    protected int getAnimationOffset(int degree) {
        for (int resault = 0; resault < DEGREE_DIVIDE.length; resault++) {
            if (degree < DEGREE_DIVIDE[resault]) {
                return resault % 2;
            }
        }
        return 0;
    }

    protected int getTransId(int degree) {
        int resault = 0;
        while (resault < DEGREE_DIVIDE.length) {
            if (degree < DEGREE_DIVIDE[resault]) {
                resault %= 8;
                break;
            }
            resault++;
        }
        return ((resault + 1) / 2) % 4;
    }

    protected int getTrans(int degree) {
        int re = TRANS[getTransId(degree)];
        int offset = getAnimationOffset(degree);
        if (this.faceDirection) {
            return re;
        }
        if (offset != 0) {
            switch (re) {
                case 0:
                    re = 4;
                    break;
                case 3:
                    re = 7;
                    break;
                case 5:
                    re = 2;
                    break;
                case 6:
                    re = 1;
                    break;
                default:
                    break;
            }
        }
        switch (re) {
            case 0:
            case 3:
            case 5:
            case 6:
                re ^= 2;
                break;
        }
        return re;
    }

    protected int getTrans() {
        return getTrans(this.faceDegree);
    }

    public int getFocusX() {
        return getNewPointX(this.footPointX, 0, -768, this.faceDegree) >> 6;
    }

    public int getFocusY() {
        if (FOCUS_MAX_OFFSET > 10) {
            if (this.focusMovingState == 0) {
                this.lookCount = 32;
            }
            if (this.lookCount == 0) {
                switch (this.focusMovingState) {
                    case 1:
                        if (this.focusOffsetY < FOCUS_MAX_OFFSET) {
                            this.focusOffsetY += 15;
                            if (this.focusOffsetY > FOCUS_MAX_OFFSET) {
                                this.focusOffsetY = FOCUS_MAX_OFFSET;
                                break;
                            }
                        }
                        break;
                    case 2:
                        if (this.focusOffsetY > (-FOCUS_MAX_OFFSET)) {
                            this.focusOffsetY -= 15;
                            if (this.focusOffsetY < (-FOCUS_MAX_OFFSET)) {
                                this.focusOffsetY = -FOCUS_MAX_OFFSET;
                                break;
                            }
                        }
                        break;
                }
            }
            this.lookCount--;
            if (this.focusOffsetY > 0) {
                this.focusOffsetY -= 15;
                if (this.focusOffsetY < 0) {
                    this.focusOffsetY = 0;
                }
            }
            if (this.focusOffsetY < 0) {
                this.focusOffsetY += 15;
                if (this.focusOffsetY > 0) {
                    this.focusOffsetY = 0;
                }
            }
        }
        return (getNewPointY(this.footPointY, 0, -768, this.faceDegree) >> 6) + ((this.isAntiGravity ? 1 : -1) * this.focusOffsetY);
    }

    public void collisionLogicOnObject() {
        this.onObjectContinue = false;
        this.checkedObject = false;
        this.footObjectLogic = false;
        this.worldCal.actionState = (byte) 1;
        if (this.isInWater) {
            this.worldCal.actionLogic(this.velX >> 1, this.velY);
        } else {
            this.worldCal.actionLogic(this.velX, this.velY);
        }
        if (this.worldCal.actionState == (byte) 0) {
            this.onObjectContinue = false;
        } else if (!(this.checkedObject || this.footOnObject == null || !this.footOnObject.onObjectChk(this))) {
            this.footOnObject.doWhileCollisionWrap(this);
            this.onObjectContinue = true;
        }
        if (!this.onObjectContinue) {
            this.footOnObject = null;
            calTotalVelocity();
            if (this.collisionState == (byte) 2) {
                this.collisionState = (byte) 1;
                this.worldCal.actionState = (byte) 1;
            }
        } else if (this.collisionState == (byte) 2 && !this.piping) {
            this.velY = 0;
        }
    }

    public void calDivideVelocity() {
        calDivideVelocity(this.faceDegree);
    }

    public void calDivideVelocity(int degree) {
        this.velX = (this.totalVelocity * MyAPI.dCos(degree)) / 100;
        this.velY = (this.totalVelocity * MyAPI.dSin(degree)) / 100;
    }

    public void calTotalVelocity() {
        calTotalVelocity(this.faceDegree);
    }

    public void calTotalVelocity(int degree) {
        this.totalVelocity = ((this.velX * MyAPI.dCos(degree)) + (this.velY * MyAPI.dSin(degree))) / 100;
    }

    protected int getNewPointX(int oriX, int xOffset, int yOffset, int degree) {
        return (((MyAPI.dCos(degree) * xOffset) / 100) + oriX) - ((MyAPI.dSin(degree) * yOffset) / 100);
    }

    protected int getNewPointY(int oriY, int xOffset, int yOffset, int degree) {
        return (((MyAPI.dSin(degree) * xOffset) / 100) + oriY) + ((MyAPI.dCos(degree) * yOffset) / 100);
    }

    private boolean faceDirectionChk() {
        if (this.totalVelocity > 0) {
            return true;
        }
        if (this.totalVelocity < 0) {
            return false;
        }
        if (Key.press(Key.gLeft) || Key.repeat(Key.gLeft)) {
            return false;
        }
        if (Key.press(Key.gRight) || Key.repeat(Key.gRight)) {
            return true;
        }
        return true;
    }

    private void faceSlopeChk() {
        int slopeVelocity = (MyAPI.dSin(this.faceDegree) * (getGravity() * (this.isAntiGravity ? -1 : 1))) / 100;
    }

    private void decelerate() {
        int preTotalVelocity = this.totalVelocity;
        int resistance = getRetPower();
        if (this.totalVelocity > 0) {
            this.totalVelocity -= resistance;
            if (this.totalVelocity < 0) {
                this.totalVelocity = 0;
            }
        } else if (this.totalVelocity < 0) {
            this.totalVelocity += resistance;
            if (this.totalVelocity > 0) {
                this.totalVelocity = 0;
            }
        }
        if (this.totalVelocity * preTotalVelocity <= 0 && this.animationID == 4) {
            this.animationID = 0;
        }
    }

    private void inputLogicWalk() {
        int preTotalVelocity;
        this.leavingBar = false;
        this.doJumpForwardly = false;
        this.degreeRotateMode = 0;
        if (this.slipFlag || this.totalVelocity != 0) {
            int fakeGravity = getSlopeGravity() * (this.isAntiGravity ? -1 : 1);
            if (this.slipFlag) {
                fakeGravity *= 3;
            }
            int velChange = (MyAPI.dSin(this.faceDegree) * fakeGravity) / 100;
            preTotalVelocity = this.totalVelocity;
            if (this.slipFlag && Math.abs(velChange) < 100) {
                velChange = velChange < 0 ? -100 : 100;
            }
            if (this.animationID == 4) {
                if (this.totalVelocity >= 0) {
                    if (velChange < 0) {
                        velChange >>= 2;
                    }
                } else if (velChange > 0) {
                    velChange >>= 2;
                }
            }
            this.totalVelocity += velChange;
            if (this.totalVelocity * preTotalVelocity <= 0 && this.animationID == 4) {
                this.animationID = 0;
                this.faceDirection = preTotalVelocity > 0;
            }
        }
        if (!(this.attackLevel != 0 || Key.repeat(Key.gDown) || this.animationID == -1 || this.animationID == 30)) {
            int reversePower;
            if ((!this.isAntiGravity && Key.repeat(Key.gLeft)) || ((this.isAntiGravity && Key.repeat(Key.gRight)) || doBrake())) {
                if (this.animationID == 5) {
                    this.animationID = 0;
                }
                if (!((this.animationID == 4 && this.collisionState == (byte) 0) || doBrake())) {
                    this.faceDirection = false;
                }
                if (this.fallTime == 0) {
                    if (this.totalVelocity > 0 || doBrake()) {
                        if (this.animationID == 4) {
                            reversePower = this.movePowerReserseBall;
                        } else {
                            reversePower = this.movePowerReverse;
                        }
                        this.totalVelocity -= reversePower;
                        if (this.totalVelocity < 0) {
                            if (this.onBank) {
                                this.totalVelocity = 0;
                                this.onBank = false;
                                this.bankwalking = false;
                            } else {
                                this.totalVelocity = (0 - reversePower) >> 2;
                            }
                        }
                        if (!(Math.abs(this.totalVelocity) <= BANK_BRAKE_SPEED_LIMIT || this.animationID == 4 || this.animationID == 17)) {
                            soundInstance.playSe(10);
                            if (this.onBank) {
                                this.onBank = false;
                                this.bankwalking = false;
                            }
                        }
                    } else if (this.animationID != 4) {
                        this.totalVelocity -= this.movePower;
                        if (this.totalVelocity < (-this.maxVelocity)) {
                            this.totalVelocity += this.movePower;
                            if (this.totalVelocity > (-this.maxVelocity)) {
                                this.totalVelocity = -this.maxVelocity;
                            }
                        }
                    }
                }
            } else if ((!this.isAntiGravity && Key.repeat(Key.gRight)) || ((this.isAntiGravity && Key.repeat(Key.gLeft)) || isTerminalRunRight())) {
                if (this.animationID == 5) {
                    this.animationID = 0;
                }
                if (!(this.animationID == 4 && this.collisionState == (byte) 0)) {
                    this.faceDirection = true;
                }
                if (this.fallTime == 0) {
                    if (this.totalVelocity < 0 || doBrake()) {
                        if (this.animationID == 4) {
                            reversePower = this.movePowerReserseBall;
                        } else {
                            reversePower = this.movePowerReverse;
                        }
                        this.totalVelocity += reversePower;
                        if (this.totalVelocity > -1) {
                            if (this.onBank) {
                                this.totalVelocity = 0;
                                this.onBank = false;
                                this.bankwalking = false;
                            } else {
                                this.totalVelocity = reversePower >> 2;
                            }
                        }
                        if (!(Math.abs(this.totalVelocity) <= BANK_BRAKE_SPEED_LIMIT || this.animationID == 4 || this.animationID == 17)) {
                            soundInstance.playSe(10);
                            if (this.onBank) {
                                this.onBank = false;
                                this.bankwalking = false;
                            }
                        }
                    } else if (this.animationID != 4) {
                        this.totalVelocity += this.movePower;
                        if (this.totalVelocity > this.maxVelocity) {
                            this.totalVelocity -= this.movePower;
                            if (this.totalVelocity < this.maxVelocity) {
                                this.totalVelocity = this.maxVelocity;
                            }
                        }
                    }
                }
            }
        }
        if (this.animationID != -1) {
            if (Math.abs(this.totalVelocity) <= 0) {
                if (!(this.animationID == 38 || this.animationID == 39 || this.animationID == 40 || this.animationID == 5 || this.collisionState == (byte) 1)) {
                    this.animationID = 0;
                    this.bankwalking = false;
                    checkCliffAnimation();
                }
            } else if (!(this.animationID == 4 || this.animationID == 35 || this.animationID == 36 || this.animationID == 5 || this.animationID == 31)) {
                if (Math.abs(this.totalVelocity) < SPEED_LIMIT_LEVEL_1) {
                    this.animationID = 1;
                } else if (Math.abs(this.totalVelocity) < SPEED_LIMIT_LEVEL_2) {
                    this.animationID = 2;
                } else if (!this.slipping) {
                    this.animationID = 3;
                }
            }
        }
        waitingChk();
        int slopeVelocity = (MyAPI.dSin(this.faceDegree) * (getGravity() * (this.isAntiGravity ? -1 : 1))) / 100;
        faceSlopeChk();
        if (this.animationID != -1 && this.attackLevel == 0 && this.animationID != 4 && Math.abs(this.totalVelocity) > Math.abs(slopeVelocity) && this.fallTime == 0) {
            if (!(Key.repeat(Key.gLeft) && Key.repeat(Key.gRight)) && ((((!this.isAntiGravity && Key.repeat(Key.gLeft)) || (this.isAntiGravity && Key.repeat(Key.gRight))) && this.totalVelocity > RUN_BRAKE_SPEED_LIMIT) || (((!this.isAntiGravity && Key.repeat(Key.gRight)) || (this.isAntiGravity && Key.repeat(Key.gLeft))) && this.totalVelocity < (-RUN_BRAKE_SPEED_LIMIT)))) {
                boolean z;
                this.animationID = 17;
                soundInstance.playSe(10);
                if (this.totalVelocity > 0) {
                    z = true;
                } else {
                    z = false;
                }
                this.faceDirection = z;
            } else if (this.totalVelocity != 0 && doBrake()) {
                this.animationID = 17;
                soundInstance.playSe(10);
                this.faceDirection = this.totalVelocity > 0;
            }
        }
        if (this.ducting && Math.abs(this.totalVelocity) < MDPhone.SCREEN_HEIGHT) {
            if (this.totalVelocity > 0 && this.pushOnce) {
                this.totalVelocity += MDPhone.SCREEN_HEIGHT;
                this.pushOnce = false;
            }
            if (this.totalVelocity < 0 && this.pushOnce) {
                this.totalVelocity -= 640;
                this.pushOnce = false;
            }
        }
        if (!spinLogic()) {
            if (canDoJump() && Key.press(Key.gUp | 16777216)) {
                if ((characterID != 3 || PlayerAmy.isCanJump) && !(characterID == 3 && (getCharacterAnimationID() == 18 || getCharacterAnimationID() == 19))) {
                    doJump();
                }
            } else if (Key.repeat(Key.gUp | 33554432)) {
                if (this.animationID == 38 && this.drawer.checkEnd()) {
                    this.animationID = 39;
                }
                if (!(this.animationID == 38 || this.animationID == 39 || (this.animationID != 0 && this.animationID != 50 && this.animationID != 51))) {
                    this.animationID = 38;
                }
                if (this.animationID == 39) {
                    this.focusMovingState = 1;
                }
            } else {
                if (this.animationID == 40 && this.drawer.checkEnd()) {
                    this.animationID = 0;
                }
                if (this.animationID == 38 || this.animationID == 39) {
                    this.animationID = 40;
                }
            }
        }
        extraLogicWalk();
        int newPointX;
        if (((!this.isAntiGravity && this.faceDegree >= 90 && this.faceDegree <= 270) || (this.isAntiGravity && (this.faceDegree <= 90 || this.faceDegree >= 270))) && ((Math.abs((FAKE_GRAVITY_ON_WALK * MyAPI.dCos(this.faceDegree)) / 100) >= (this.totalVelocity * this.totalVelocity) / 4864 && !this.ducting) || this.animationID == 17)) {
            calDivideVelocity();
            int bodyCenterX = getNewPointX(this.posX, 0, (-this.collisionRect.getHeight()) >> 1, this.faceDegree);
            int bodyCenterY = getNewPointY(this.posY, 0, (-this.collisionRect.getHeight()) >> 1, this.faceDegree);
            newPointX = getNewPointX(bodyCenterX, 0, this.collisionRect.getHeight() >> 1, this.faceDegree);
            this.footPointX = newPointX;
            this.posX = newPointX;
            newPointX = getNewPointY(bodyCenterY, 0, this.collisionRect.getHeight() >> 1, this.faceDegree);
            this.footPointY = newPointX;
            this.posY = newPointX;
            this.collisionState = (byte) 1;
            this.worldCal.actionState = (byte) 1;
        } else if (!this.ducting) {
            if (needRetPower() && this.collisionState == (byte) 0) {
                preTotalVelocity = this.totalVelocity;
                int resistance = getRetPower();
                if (this.totalVelocity > 0) {
                    this.totalVelocity -= resistance;
                    if (this.totalVelocity < 0) {
                        this.totalVelocity = 0;
                    }
                } else if (this.totalVelocity < 0) {
                    this.totalVelocity += resistance;
                    if (this.totalVelocity > 0) {
                        this.totalVelocity = 0;
                    }
                }
                if (this.totalVelocity * preTotalVelocity <= 0 && this.animationID == 4) {
                    this.animationID = 0;
                    this.faceDirection = preTotalVelocity > 0;
                }
            }
            System.out.println("");
            if (this.collisionState == (byte) 1) {
                int i;
                newPointX = this.velY;
                if (this.isAntiGravity) {
                    i = -1;
                } else {
                    i = 1;
                }
                this.velY = newPointX + (i * getGravity());
            }
        }
    }

    private void inputLogicOnObject() {
        int i;
        this.leavingBar = false;
        this.doJumpForwardly = false;
        this.degreeRotateMode = 0;
        int tmpPower = this.movePower;
        int tmpMaxVel = this.maxVelocity;
        if (this.animationID != 5) {
            int reversePower;
            if (((Key.repeat(Key.gLeft) && (this.animationID == 0 || this.animationID == 47 || this.animationID == 48 || this.animationID == 1 || this.animationID == 2 || this.animationID == 3)) || (this.isCelebrate && !this.faceDirection)) && !isOnSlip0()) {
                if (this.animationID == 5) {
                    this.animationID = 0;
                }
                this.faceDirection = this.isAntiGravity;
                if (this.velX > 0) {
                    if (this.animationID == 4) {
                        reversePower = this.movePowerReserseBall;
                    } else {
                        reversePower = this.movePowerReverse;
                    }
                    this.velX -= reversePower;
                    if (this.velX < 0) {
                        this.velX = (0 - reversePower) >> 2;
                    } else {
                        this.faceDirection = true;
                    }
                } else if (this.animationID != 4) {
                    this.velX -= tmpPower;
                    if (this.velX < (-tmpMaxVel)) {
                        this.velX += tmpPower;
                        if (this.velX > (-tmpMaxVel)) {
                            this.velX = -tmpMaxVel;
                        }
                    }
                }
            } else if ((Key.repeat(Key.gRight) && (this.animationID == 0 || this.animationID == 47 || this.animationID == 48 || this.animationID == 1 || this.animationID == 2 || this.animationID == 3)) || (this.isCelebrate && this.faceDirection)) {
                if (this.animationID == 5) {
                    this.animationID = 0;
                }
                this.faceDirection = !this.isAntiGravity;
                if (this.velX < 0) {
                    if (this.animationID == 4) {
                        reversePower = this.movePowerReserseBall;
                    } else {
                        reversePower = this.movePowerReverse;
                    }
                    this.velX += reversePower;
                    if (this.velX > -1) {
                        this.velX = reversePower >> 2;
                    } else {
                        this.faceDirection = false;
                    }
                } else if (this.animationID != 4) {
                    this.velX += tmpPower;
                    if (this.velX > tmpMaxVel) {
                        this.velX -= tmpPower;
                        if (this.velX < tmpMaxVel) {
                            this.velX = tmpMaxVel;
                        }
                    }
                }
            }
        }
        if (this.animationID != -1) {
            if (Math.abs(this.velX) <= 0) {
                if (!(this.animationID == 38 || this.animationID == 39 || this.animationID == 40 || this.animationID == 5)) {
                    this.animationID = 0;
                    checkCliffAnimation();
                }
            } else if (this.animationID != 4) {
                if (Math.abs(this.velX) < SPEED_LIMIT_LEVEL_1) {
                    this.animationID = 1;
                } else if (Math.abs(this.velX) < SPEED_LIMIT_LEVEL_2) {
                    this.animationID = 2;
                } else {
                    this.animationID = 3;
                }
            }
        }
        extraLogicOnObject();
        this.attackAnimationID = this.animationID;
        if (!spinLogic()) {
            if (canDoJump() && !this.dashRolling && Key.press(Key.gUp | 16777216)) {
                if (characterID != 3 || PlayerAmy.isCanJump) {
                    doJump();
                }
            } else if (Key.repeat(Key.gUp | 33554432)) {
                if (this.animationID == 38 && this.drawer.checkEnd()) {
                    this.animationID = 39;
                }
                if (!(this.animationID == 38 || this.animationID == 39 || this.animationID != 0)) {
                    this.animationID = 38;
                }
                if (this.animationID == 39) {
                    this.focusMovingState = 1;
                }
            } else {
                if (this.animationID == 40 && this.drawer.checkEnd()) {
                    this.animationID = 0;
                }
                if (this.animationID == 38 || this.animationID == 39) {
                    this.animationID = 40;
                }
            }
        }
        if (needRetPower() && this.collisionState == (byte) 2) {
            int resistance = getRetPower();
            if (this.velX > 0) {
                this.velX -= resistance;
                if (this.velX < 0) {
                    this.velX = 0;
                }
            } else if (this.velX < 0) {
                this.velX += resistance;
                if (this.velX > 0) {
                    this.velX = 0;
                }
            }
        }
        int i2 = this.velY;
        if (this.isAntiGravity) {
            i = -1;
        } else {
            i = 1;
        }
        this.velY = i2 + (i * getGravity());
        waitingChk();
    }

    private void inputLogicJump() {
        int newPointX;
        int i;
        if (this.faceDegree != this.degreeStable) {
            int bodyCenterX = getNewPointX(this.posX, 0, (-this.collisionRect.getHeight()) >> 1, this.faceDegree);
            int bodyCenterY = getNewPointY(this.posY, 0, (-this.collisionRect.getHeight()) >> 1, this.faceDegree);
            this.faceDegree = this.degreeStable;
            newPointX = getNewPointX(bodyCenterX, 0, this.collisionRect.getHeight() >> 1, this.faceDegree);
            this.footPointX = newPointX;
            this.posX = newPointX;
            newPointX = getNewPointY(bodyCenterY, 0, this.collisionRect.getHeight() >> 1, this.faceDegree);
            this.footPointY = newPointX;
            this.posY = newPointX;
        }
        if (this.degreeForDraw != this.faceDegree) {
            int degreeDiff = this.faceDegree - this.degreeForDraw;
            int degreeDes = this.faceDegree;
            switch (this.degreeRotateMode) {
                case 0:
                    if (Math.abs(degreeDiff) > RollPlatformSpeedC.DEGREE_VELOCITY) {
                        if (degreeDes > this.degreeForDraw) {
                            degreeDes -= 360;
                        } else {
                            degreeDes += MDPhone.SCREEN_WIDTH;
                        }
                    }
                    this.degreeForDraw = MyAPI.calNextPosition((double) this.degreeForDraw, (double) degreeDes, 1, 3);
                    break;
                case 1:
                    this.degreeForDraw += 24;
                    break;
                case 2:
                    this.degreeForDraw -= 24;
                    break;
            }
            while (this.degreeForDraw < 0) {
                this.degreeForDraw += MDPhone.SCREEN_WIDTH;
            }
            this.degreeForDraw %= MDPhone.SCREEN_WIDTH;
        }
        if (this.animationID == 8) {
            doWalkPoseInAir();
        }
        if (!(this.hurtNoControl || this.animationID == 30 || (characterID == 3 && this.myAnimationID >= 5 && this.myAnimationID <= 7))) {
            if ((Key.repeat(Key.gLeft) || (this.isCelebrate && !this.faceDirection)) && !this.ducting) {
                if (this.velX > (-this.maxVelocity)) {
                    this.velX -= this.movePowerInAir;
                    if (this.velX < (-this.maxVelocity)) {
                        this.velX = -this.maxVelocity;
                    }
                }
                if (this.degreeRotateMode == 0) {
                    boolean z;
                    if (this.isAntiGravity) {
                        z = true;
                    } else {
                        z = false;
                    }
                    this.faceDirection = z;
                }
            } else if ((Key.repeat(Key.gRight) || isTerminal || (this.isCelebrate && this.faceDirection)) && !this.ducting) {
                if (this.velX < this.maxVelocity) {
                    this.velX += this.movePowerInAir;
                    if (this.velX > this.maxVelocity) {
                        this.velX = this.maxVelocity;
                    }
                }
                if (this.degreeRotateMode == 0) {
                    this.faceDirection = !this.isAntiGravity;
                }
            }
        }
        if (!this.isOnlyJump) {
            extraLogicJump();
        }
        if (this.velY >= -768 - getGravity()) {
            int velX2 = this.velX << 5;
            int resistance = (velX2 * 3) / JUMP_REVERSE_POWER;
            if (velX2 > 0) {
                velX2 -= resistance;
                if (velX2 < 0) {
                    velX2 = 0;
                }
            } else if (velX2 < 0) {
                velX2 -= resistance;
                if (velX2 > 0) {
                    velX2 = 0;
                }
            }
            this.velX = velX2 >> 5;
        }
        if (this.smallJumpCount > 0) {
            this.smallJumpCount--;
            if (!(this.noVelMinus || Key.repeat(Key.gUp | 16777216))) {
                newPointX = this.velY;
                if (this.isAntiGravity) {
                    i = -1;
                } else {
                    i = 1;
                }
                this.velY = newPointX + (i * (getGravity() >> 1));
                newPointX = this.velY;
                if (this.isAntiGravity) {
                    i = -1;
                } else {
                    i = 1;
                }
                this.velY = newPointX + (i * (getGravity() >> 2));
            }
        }
        newPointX = this.velY;
        if (this.isAntiGravity) {
            i = -1;
        } else {
            i = 1;
        }
        this.velY = newPointX + (i * getGravity());
        if (this.animationID != 14) {
            return;
        }
        if ((this.velY > -200 && !this.isAntiGravity) || (this.velY < 200 && this.isAntiGravity)) {
            this.animationID = 42;
        }
    }

    private void inputLogicSand() {
        this.leavingBar = false;
        this.doJumpForwardly = false;
        this.degreeRotateMode = 0;
        if (this.velY > 0 && !this.sandStanding) {
            this.sandStanding = true;
        }
        this.sandFrame++;
        if (this.velX == 0) {
            this.sandFrame = 0;
        } else if (this.sandFrame == 1) {
            soundInstance.playSe(70);
        } else if (this.sandFrame > 2) {
            soundInstance.playSequenceSe(71);
        }
        if (this.sandStanding) {
            int reversePower;
            int tmpPower = this.movePower >> 1;
            int tmpMaxVel = this.maxVelocity >> 1;
            if (Key.repeat(Key.gLeft)) {
                this.faceDirection = false;
                if (this.velX > 0) {
                    if (this.animationID == 4) {
                        reversePower = this.movePowerReserseBallInSand;
                    } else {
                        reversePower = this.movePowerReverseInSand;
                    }
                    this.velX -= reversePower;
                    if (this.velX < 0) {
                        this.velX = (0 - reversePower) >> 2;
                    } else {
                        this.faceDirection = true;
                    }
                } else if (this.animationID != 4) {
                    this.velX -= tmpPower;
                    if (this.velX < (-tmpMaxVel)) {
                        this.velX += tmpPower;
                        if (this.velX > (-tmpMaxVel)) {
                            this.velX = -tmpMaxVel;
                        }
                    }
                }
            } else if (Key.repeat(Key.gRight)) {
                this.faceDirection = true;
                if (this.velX < 0) {
                    if (this.animationID == 4) {
                        reversePower = this.movePowerReserseBallInSand;
                    } else {
                        reversePower = this.movePowerReverseInSand;
                    }
                    this.velX += reversePower;
                    if (this.velX > -1) {
                        this.velX = reversePower >> 2;
                    } else {
                        this.faceDirection = false;
                    }
                } else if (this.animationID != 4) {
                    this.velX += tmpPower;
                    if (this.velX > tmpMaxVel) {
                        this.velX -= tmpPower;
                        if (this.velX < tmpMaxVel) {
                            this.velX = tmpMaxVel;
                        }
                    }
                }
            } else {
                this.velX = 0;
            }
            if (Math.abs(this.velX) <= 64) {
                if (!(((this instanceof PlayerAmy) && getCharacterAnimationID() == 13 && getVelY() < 0) || this.animationID == 38 || this.animationID == 39 || this.animationID == 40)) {
                    this.animationID = 0;
                }
            } else if (characterID != 1 || ((PlayerTails) player).flyCount <= 0) {
                if ((!(this instanceof PlayerAmy) || (getCharacterAnimationID() != 4 && (getCharacterAnimationID() != 5 || this.drawer.getCurrentFrame() >= 2))) && !((this instanceof PlayerAmy) && getCharacterAnimationID() == 13 && getVelY() < 0)) {
                    if (Math.abs(this.velX) < SPEED_LIMIT_LEVEL_1) {
                        this.animationID = 1;
                    } else if (Math.abs(this.velX) < SPEED_LIMIT_LEVEL_2) {
                        this.animationID = 2;
                    } else {
                        this.animationID = 3;
                    }
                }
            } else if (!(this.myAnimationID == 12 || this.myAnimationID == 48 || this.myAnimationID == 49)) {
                ((PlayerTails) player).flyCount = 0;
            }
            if (!((this instanceof PlayerAmy) && getCharacterAnimationID() == 13 && getVelY() < 0)) {
                this.velY = 100;
            }
            if (characterID == 0) {
                int sandDash = SPEED_LIMIT_LEVEL_1 >> 2;
                if (Key.press(Key.gSelect)) {
                    soundInstance.playSe(4);
                    if (this.faceDirection) {
                        if (this.velX < 0) {
                            if (this.animationID == 4) {
                                reversePower = this.movePowerReserseBallInSand;
                            } else {
                                reversePower = this.movePowerReverseInSand;
                            }
                            this.velX += reversePower;
                            if (this.velX > -1) {
                                this.velX = reversePower >> 2;
                            } else {
                                this.faceDirection = false;
                            }
                        } else if (this.animationID != 4) {
                            this.velX += sandDash;
                            if (this.velX > tmpMaxVel) {
                                this.velX -= sandDash;
                                if (this.velX < tmpMaxVel) {
                                    this.velX = tmpMaxVel;
                                }
                            }
                        }
                    } else if (this.velX > 0) {
                        if (this.animationID == 4) {
                            reversePower = this.movePowerReserseBallInSand;
                        } else {
                            reversePower = this.movePowerReverseInSand;
                        }
                        this.velX -= reversePower;
                        if (this.velX < 0) {
                            this.velX = (0 - reversePower) >> 2;
                        } else {
                            this.faceDirection = true;
                        }
                    } else if (this.animationID != 4) {
                        this.velX -= sandDash;
                        if (this.velX < (-tmpMaxVel)) {
                            this.velX += sandDash;
                            if (this.velX > (-tmpMaxVel)) {
                                this.velX = -tmpMaxVel;
                            }
                        }
                    }
                }
            }
            if (!spinLogic()) {
                if (!(Key.repeat(Key.gLeft) || Key.repeat(Key.gRight) || isTerminalRunRight() || this.animationID == -1)) {
                    if (Key.repeat(Key.gDown)) {
                        if (Math.abs(this.velX) > 64) {
                            this.velX = 0;
                        } else if (this.animationID != 5) {
                            this.animationID = 46;
                        }
                    } else if (this.animationID == 5) {
                        this.animationID = 46;
                    }
                }
                if (this.animationID != 5 && Key.press(Key.gUp | 16777216)) {
                    if ((this instanceof PlayerTails) && ((PlayerTails) player).flyCount > 0) {
                        ((PlayerTails) player).flyCount = 0;
                    }
                    doJump();
                    this.velY -= getGravity();
                    this.sandStanding = false;
                }
            }
            if (!Key.repeat(Key.gLeft | Key.gRight) && this.sandStanding) {
                int resistance;
                if (this.animationID != 4) {
                    resistance = tmpPower;
                } else {
                    resistance = tmpPower >> 1;
                }
                if (this.velX > 0) {
                    this.velX -= resistance;
                    if (this.velX < 0) {
                        this.velX = 0;
                    }
                } else if (this.velX < 0) {
                    this.velX += resistance;
                    if (this.velX > 0) {
                        this.velX = 0;
                    }
                }
            }
        } else {
            inputLogicJump();
        }
        this.collisionState = (byte) 1;
    }

    private int faceDegreeChk() {
        return this.faceDegree;
    }

    private int jumpDirectionX() {
        return (this.faceDegree <= 90 || this.faceDegree >= 270) ? -1 : 1;
    }

    public void slipJumpOut() {
    }

    public void resetFlyCount() {
    }

    public void doJump() {
        if (this.collisionState == (byte) 0) {
            calDivideVelocity();
        }
        this.collisionState = (byte) 1;
        this.worldCal.actionState = (byte) 1;
        this.velY += ((this.isInWater ? JUMP_INWATER_START_VELOCITY : JUMP_START_VELOCITY) * MyAPI.dCos(faceDegreeChk())) / 100;
        this.velX += ((this.isInWater ? JUMP_INWATER_START_VELOCITY : JUMP_START_VELOCITY) * (-MyAPI.dSin(faceDegreeChk()))) / 100;
        if (this.faceDegree >= 0 && this.faceDegree <= 90) {
            if (this.isAntiGravity) {
                this.velY = Math.max(this.velY, -JUMP_PROTECT);
            } else {
                this.velY = Math.min(this.velY, JUMP_PROTECT);
            }
        }
        this.animationID = 4;
        soundInstance.playSe(11);
        this.smallJumpCount = 4;
        this.onBank = false;
        this.attackAnimationID = 0;
        this.attackCount = 0;
        this.attackLevel = 0;
        this.noVelMinus = false;
        this.doJumpForwardly = true;
        slipJumpOut();
        if (StageManager.getWaterLevel() > 0 && characterID == 2) {
            ((PlayerKnuckles) player).floatchk();
        }
    }

    public void doJump(int v0) {
        if (this.collisionState == (byte) 0) {
            calDivideVelocity();
        }
        this.collisionState = (byte) 1;
        this.worldCal.actionState = (byte) 1;
        this.velY += (MyAPI.dCos(faceDegreeChk()) * v0) / 100;
        this.velX += ((-MyAPI.dSin(faceDegreeChk())) * v0) / 100;
        if (this.isAntiGravity) {
            this.velY = Math.max(this.velY, -JUMP_PROTECT);
        } else {
            this.velY = Math.min(this.velY, JUMP_PROTECT);
        }
        this.animationID = 4;
        soundInstance.playSe(11);
        this.smallJumpCount = 4;
        this.onBank = false;
        this.attackAnimationID = 0;
        this.attackCount = 0;
        this.attackLevel = 0;
        this.noVelMinus = false;
        this.doJumpForwardly = true;
    }

    public void doJumpV() {
        if (this.collisionState == (byte) 0) {
            calDivideVelocity();
        }
        this.collisionState = (byte) 1;
        this.worldCal.actionState = (byte) 1;
        setVelX(0);
        setVelY(this.isInWater ? JUMP_INWATER_START_VELOCITY : JUMP_START_VELOCITY);
        this.animationID = 4;
        soundInstance.playSe(11);
        this.smallJumpCount = 4;
        this.onBank = false;
        this.attackAnimationID = 0;
        this.attackCount = 0;
        this.attackLevel = 0;
        this.noVelMinus = false;
        this.doJumpForwardly = true;
        slipJumpOut();
    }

    public void doJumpV(int v0) {
        this.collisionState = (byte) 1;
        this.worldCal.actionState = (byte) 1;
        setVelY(v0);
        this.animationID = 4;
        soundInstance.playSe(11);
        this.smallJumpCount = 4;
        this.onBank = false;
        this.attackAnimationID = 0;
        this.attackCount = 0;
        this.attackLevel = 0;
        this.noVelMinus = false;
        this.doJumpForwardly = true;
    }

    public void setFurikoOutVelX(int degree) {
        this.velX = ((-JUMP_PROTECT) * MyAPI.dCos(degree)) / 100;
    }

    public int getCheckPositionX() {
        return (this.collisionRect.x0 + this.collisionRect.x1) >> 1;
    }

    public int getCheckPositionY() {
        return (this.collisionRect.y0 + this.collisionRect.y1) >> 1;
    }

    public int getFootPositionX() {
        return this.footPointX;
    }

    public int getFootPositionY() {
        return this.footPointY;
    }

    public int getHeadPositionY() {
        return getNewPointY(this.footPointY, 0, -1536, this.faceDegree);
    }

    public void setHeadPositionY(int y) {
        this.footPointY = getNewPointY(y, 0, 1536, this.faceDegree);
    }

    public void doWhileCollision(PlayerObject object, int direction) {
    }

    public void setCollisionLayer(int layer) {
        if (layer >= 0 && layer <= 1) {
            this.currentLayer = layer;
        }
    }

    private void land() {
        calTotalVelocity();
        int playingLoopSeIndex = soundInstance.getPlayingLoopSeIndex();
        SoundSystem soundSystem = soundInstance;
        if (playingLoopSeIndex == 18) {
            soundInstance.stopLoopSe();
        }
        if (this.animationID != 45) {
            if (Math.abs(this.totalVelocity) == 0) {
                this.animationID = 0;
            } else if (Math.abs(this.totalVelocity) < SPEED_LIMIT_LEVEL_1) {
                this.animationID = 1;
            } else if (Math.abs(this.totalVelocity) < SPEED_LIMIT_LEVEL_2) {
                this.animationID = 2;
            } else if (!this.slipping) {
                this.animationID = 3;
            }
        }
        if (this.ducting) {
            if (this.totalVelocity > 0 && this.totalVelocity < MDPhone.SCREEN_HEIGHT && this.pushOnce) {
                this.totalVelocity += MDPhone.SCREEN_HEIGHT;
                this.pushOnce = false;
            }
            if (this.totalVelocity < 0 && this.totalVelocity > -640 && this.pushOnce) {
                this.totalVelocity -= MDPhone.SCREEN_HEIGHT;
                this.pushOnce = false;
            }
        }
    }

    public void collisionCheckWithGameObject(int footX, int footY) {
        this.collisionChkBreak = false;
        refreshCollisionRectWrap();
        if (isAttracting()) {
            this.attractRect.setRect(footX - 4800, (footY - 4800) - 768, 9600, 9600);
        }
        GameObject.collisionChkWithAllGameObject(this);
        calPreCollisionRect();
    }

    public int getCurrentHeight() {
        return getCollisionRectHeight();
    }

    public void calPreCollisionRect() {
        int RECT_HEIGHT = getCollisionRectHeight();
        this.checkPositionX = getNewPointX(this.footPointX, 0, (-RECT_HEIGHT) >> 1, this.faceDegree);
        this.checkPositionY = getNewPointY(this.footPointY, 0, (-RECT_HEIGHT) >> 1, this.faceDegree);
        this.preCollisionRect.setTwoPosition(this.checkPositionX - 512, this.checkPositionY - (RECT_HEIGHT >> 1), this.checkPositionX + 512, this.checkPositionY + (RECT_HEIGHT >> 1));
    }

    public void collisionCheckWithGameObject() {
        collisionCheckWithGameObject(this.footPointX, this.footPointY);
    }

    public void moveOnObject(int newX, int newY) {
        moveOnObject(newX, newY, false);
    }

    public void moveOnObject(int newX, int newY, boolean fountain) {
        int moveDistanceX = newX - this.footPointX;
        int moveDistanceY = newY - this.footPointY;
        this.posZ = this.currentLayer;
        this.worldCal.footDegree = this.faceDegree;
        this.posX = this.footPointX;
        this.posY = this.footPointY;
        int preVelX = this.velX;
        int preVelY = this.velY;
        this.worldCal.actionLogic(moveDistanceX, moveDistanceY);
        if (getAnimationId() != 12 && getAnimationId() != 44) {
            this.footPointX = this.posX;
            this.footPointY = this.posY;
            this.velX = preVelX;
            this.velY = preVelY;
            this.faceDegree = this.worldCal.footDegree;
        }
    }

    public void prepareForCollision() {
        refreshCollisionRectWrap();
    }

    public void setSlideAni() {
    }

    public void beSlide0(GameObject object) {
        if (this.collisionState == (byte) 0) {
            calDivideVelocity();
        }
        this.degreeRotateMode = 0;
        if (!this.hurtNoControl || this.collisionState != (byte) 1 || ((this.velY >= 0 || this.isAntiGravity) && (this.velY <= 0 || !this.isAntiGravity))) {
            if (this.collisionState != (byte) 2) {
                calTotalVelocity();
            }
            setSlideAni();
            if (this.isAntiGravity) {
                this.footPointY = object.getCollisionRect().y1;
            } else {
                this.footPointY = object.getCollisionRect().y0;
            }
            if (isFootOnObject(object)) {
                this.checkedObject = true;
            }
            setVelY(0);
            this.worldCal.stopMoveY();
            if (!(this.collisionState == (byte) 2 && isFootOnObject(object))) {
                this.footOnObject = object;
                this.collisionState = (byte) 2;
                this.collisionChkBreak = true;
            }
        } else if (this.isAntiGravity) {
            this.footPointY = object.getCollisionRect().y1;
        } else {
            this.footPointY = object.getCollisionRect().y0;
        }
        this.onObjectContinue = true;
        this.posX = this.footPointX;
        this.posY = this.footPointY;
        setPosition(this.posX, this.posY);
    }

    public void beStop(int newPosition, int direction, GameObject object, boolean isDirectionDown) {
        if (this.isAntiGravity) {
            if (direction == 1) {
                direction = 0;
            } else if (direction == 0) {
                direction = 1;
            }
        }
        switch (direction) {
            case 0:
                if (!this.isAntiGravity && this.velY < 0) {
                    setVelY(0);
                    this.worldCal.stopMoveY();
                }
                if (this.isAntiGravity && this.velY > 0) {
                    setVelY(0);
                    this.worldCal.stopMoveY();
                }
                if (this.isAntiGravity) {
                    this.footPointY = object.getCollisionRect().y0 - this.collisionRect.getHeight();
                } else {
                    this.footPointY = object.getCollisionRect().y1 + this.collisionRect.getHeight();
                }
                if ((this.collisionState == (byte) 0 || this.collisionState == (byte) 2) && this.faceDegree == 0 && !(object instanceof ItemObject)) {
                    setDie(false);
                    break;
                }
            case 1:
                if (this.collisionState == (byte) 0) {
                    calDivideVelocity();
                }
                this.degreeRotateMode = 0;
                int prey = this.footPointY;
                if (!this.hurtNoControl || this.collisionState != (byte) 1 || ((this.velY >= 0 || this.isAntiGravity) && (this.velY <= 0 || !this.isAntiGravity))) {
                    if (!(this.collisionState == (byte) 2 || (object instanceof Spring))) {
                        land();
                    }
                    if (this.isAntiGravity) {
                        this.footPointY = object.getCollisionRect().y1;
                    } else {
                        this.footPointY = object.getCollisionRect().y0;
                    }
                    if (isFootOnObject(object)) {
                        this.checkedObject = true;
                    }
                    setVelY(0);
                    this.worldCal.stopMoveY();
                    if (!(this.collisionState == (byte) 2 && isFootOnObject(object))) {
                        this.footOnObject = object;
                        this.collisionState = (byte) 2;
                        this.collisionChkBreak = true;
                    }
                    if (!(this.isSidePushed == 4 && isDirectionDown)) {
                        if (this.isSidePushed == 3) {
                            this.footPointX = this.bePushedFootX;
                            System.out.println("~~RIGHT footPointX:" + this.footPointX);
                            if (getVelX() > 0) {
                                setVelX(0);
                                this.worldCal.stopMoveX();
                            }
                        } else if (this.isSidePushed == 2) {
                            this.footPointX = this.bePushedFootX;
                            System.out.println("~~LEFT footPointX:" + this.footPointX);
                            if (getVelX() < 0) {
                                setVelX(0);
                                this.worldCal.stopMoveX();
                            }
                        }
                    }
                } else if (this.isAntiGravity) {
                    this.footPointY = object.getCollisionRect().y1;
                } else {
                    this.footPointY = object.getCollisionRect().y0;
                }
                this.movedSpeedY = this.footPointY - prey;
                this.onObjectContinue = true;
                break;
            case 2:
            case 3:
                int prex;
                int curx;
                if (direction == 3) {
                    prex = this.footPointX;
                    this.footPointX = (object.getCollisionRect().x0 - (this.collisionRect.getWidth() >> 1)) + 1;
                    this.footPointX = getNewPointX(this.footPointX, 0, getCurrentHeight() >> 1, this.faceDegree);
                    curx = this.footPointX;
                    this.bePushedFootX = this.footPointX - 512;
                    this.movedSpeedX = curx - prex;
                    if (!(object instanceof DekaPlatform)) {
                        this.movedSpeedX = 0;
                    }
                    if (Key.repeat(Key.gRight) && ((this.collisionState == (byte) 0 || this.collisionState == (byte) 2 || this.collisionState == (byte) 3) && !this.isAttacking)) {
                        this.animationID = 8;
                    }
                    if (getVelX() > 0) {
                        setVelX(0);
                        this.worldCal.stopMoveX();
                    }
                    this.rightStopped = true;
                } else {
                    prex = this.footPointX;
                    this.footPointX = (object.getCollisionRect().x1 + (this.collisionRect.getWidth() >> 1)) - 1;
                    this.footPointX = getNewPointX(this.footPointX, 0, getCurrentHeight() >> 1, this.faceDegree);
                    curx = this.footPointX;
                    this.bePushedFootX = this.footPointX;
                    this.movedSpeedX = curx - prex;
                    if (!(object instanceof DekaPlatform)) {
                        this.movedSpeedX = 0;
                    }
                    if (Key.repeat(Key.gLeft) && ((this.collisionState == (byte) 0 || this.collisionState == (byte) 2 || this.collisionState == (byte) 3) && !this.isAttacking)) {
                        this.animationID = 8;
                    }
                    if (getVelX() < 0) {
                        setVelX(0);
                        this.worldCal.stopMoveX();
                    }
                    this.leftStopped = true;
                }
                if (this.collisionState == (byte) 0 && this.animationID == 4 && (object instanceof Hari)) {
                    this.animationID = 0;
                }
                switch (this.collisionState) {
                    case (byte) 1:
                        this.xFirst = false;
                        break;
                }
                if (!(object instanceof GimmickObject)) {
                    this.isStopByObject = false;
                    break;
                } else {
                    this.isStopByObject = true;
                    break;
                }
        }
        this.posX = this.footPointX;
        this.posY = this.footPointY;
    }

    public void beStopbyDoor(int newPosition, int direction, GameObject object) {
        if (this.isAntiGravity) {
            if (direction == 1) {
                direction = 0;
            } else if (direction == 0) {
                direction = 1;
            }
        }
        switch (direction) {
            case 0:
                if (!this.isAntiGravity && this.velY < 0) {
                    setVelY(0);
                    this.worldCal.stopMoveY();
                }
                if (this.isAntiGravity && this.velY > 0) {
                    setVelY(0);
                    this.worldCal.stopMoveY();
                }
                if (this.isAntiGravity) {
                    this.footPointY = object.getCollisionRect().y0 - this.collisionRect.getHeight();
                } else {
                    this.footPointY = object.getCollisionRect().y1 + this.collisionRect.getHeight();
                }
                if ((this.collisionState == (byte) 0 || this.collisionState == (byte) 2) && this.faceDegree == 0) {
                    setDie(false);
                    break;
                }
            case 1:
                if (this.collisionState == (byte) 0) {
                    calDivideVelocity();
                }
                this.degreeRotateMode = 0;
                if (!this.hurtNoControl || this.collisionState != (byte) 1 || ((this.velY >= 0 || this.isAntiGravity) && (this.velY <= 0 || !this.isAntiGravity))) {
                    if (!(this.collisionState == (byte) 2 || (object instanceof Spring))) {
                        land();
                    }
                    if (this.isAntiGravity) {
                        this.footPointY = object.getCollisionRect().y1;
                    } else {
                        this.footPointY = object.getCollisionRect().y0;
                    }
                    if (isFootOnObject(object)) {
                        this.checkedObject = true;
                    }
                    if (!(this.collisionState == (byte) 2 && isFootOnObject(object))) {
                        this.footOnObject = object;
                        this.collisionState = (byte) 2;
                        this.collisionChkBreak = true;
                    }
                } else if (this.isAntiGravity) {
                    this.footPointY = object.getCollisionRect().y1;
                } else {
                    this.footPointY = object.getCollisionRect().y0;
                }
                this.onObjectContinue = true;
                break;
            case 2:
            case 3:
                int prex;
                if (direction == 3) {
                    boolean z;
                    prex = this.footPointX;
                    this.footPointX = (object.getCollisionRect().x0 - (this.collisionRect.getWidth() >> 1)) + 1;
                    this.footPointX = getNewPointX(this.footPointX, 0, getCurrentHeight() >> 1, this.faceDegree);
                    this.movedSpeedX = this.footPointX - prex;
                    if (!(object instanceof DekaPlatform)) {
                        this.movedSpeedX = 0;
                    }
                    if (Key.repeat(Key.gRight) && ((this.collisionState == (byte) 0 || this.collisionState == (byte) 2 || this.collisionState == (byte) 3) && !this.isAttacking)) {
                        this.animationID = 8;
                    }
                    if (getVelX() > 0) {
                        setVelX(0);
                        this.worldCal.stopMoveX();
                    }
                    this.rightStopped = true;
                    if (this.isAntiGravity) {
                        z = false;
                    } else {
                        z = true;
                    }
                    this.faceDirection = z;
                } else {
                    prex = this.footPointX;
                    this.footPointX = (object.getCollisionRect().x1 + (this.collisionRect.getWidth() >> 1)) - 1;
                    this.footPointX = getNewPointX(this.footPointX, 0, getCurrentHeight() >> 1, this.faceDegree);
                    this.movedSpeedX = this.footPointX - prex;
                    if (!(object instanceof DekaPlatform)) {
                        this.movedSpeedX = 0;
                    }
                    if (Key.repeat(Key.gLeft) && ((this.collisionState == (byte) 0 || this.collisionState == (byte) 2 || this.collisionState == (byte) 3) && !this.isAttacking)) {
                        this.animationID = 8;
                    }
                    if (getVelX() < 0) {
                        setVelX(0);
                        this.worldCal.stopMoveX();
                    }
                    this.leftStopped = true;
                    this.faceDirection = this.isAntiGravity;
                }
                if (this.collisionState == (byte) 0 && this.animationID == 4 && (object instanceof Hari)) {
                    this.animationID = 0;
                }
                switch (this.collisionState) {
                    case (byte) 1:
                        this.xFirst = false;
                        break;
                }
                if (!(object instanceof GimmickObject)) {
                    this.isStopByObject = false;
                    break;
                } else {
                    this.isStopByObject = true;
                    break;
                }
        }
        this.posX = this.footPointX;
        this.posY = this.footPointY;
    }

    public void beStop(int newPosition, int direction, GameObject object) {
        if (this.isAntiGravity) {
            if (direction == 1) {
                direction = 0;
            } else if (direction == 0) {
                direction = 1;
            }
        }
        switch (direction) {
            case 0:
                if (!this.isAntiGravity && this.velY < 0) {
                    setVelY(0);
                    this.worldCal.stopMoveY();
                }
                if (this.isAntiGravity && this.velY > 0) {
                    setVelY(0);
                    this.worldCal.stopMoveY();
                }
                if (this.isAntiGravity) {
                    this.footPointY = object.getCollisionRect().y0 - this.collisionRect.getHeight();
                } else {
                    this.footPointY = object.getCollisionRect().y1 + this.collisionRect.getHeight();
                }
                if ((this.collisionState == (byte) 0 || this.collisionState == (byte) 2) && this.faceDegree == 0 && !(object instanceof Spring) && !(object instanceof ItemObject)) {
                    setDie(false);
                    break;
                }
            case 1:
                if (this.collisionState == (byte) 0) {
                    calDivideVelocity();
                }
                this.degreeRotateMode = 0;
                if (!this.hurtNoControl || this.collisionState != (byte) 1 || ((this.velY >= 0 || this.isAntiGravity) && (this.velY <= 0 || !this.isAntiGravity))) {
                    if (!(this.collisionState == (byte) 2 || (object instanceof Spring))) {
                        land();
                    }
                    if (this.isAntiGravity) {
                        this.footPointY = object.getCollisionRect().y1;
                    } else {
                        this.footPointY = object.getCollisionRect().y0;
                    }
                    if (isFootOnObject(object)) {
                        this.checkedObject = true;
                    }
                    setVelY(0);
                    this.worldCal.stopMoveY();
                    if (!(this.collisionState == (byte) 2 && isFootOnObject(object))) {
                        this.footOnObject = object;
                        this.collisionState = (byte) 2;
                        this.collisionChkBreak = true;
                    }
                } else if (this.isAntiGravity) {
                    this.footPointY = object.getCollisionRect().y1;
                } else {
                    this.footPointY = object.getCollisionRect().y0;
                }
                this.onObjectContinue = true;
                break;
            case 2:
            case 3:
                int prex;
                if (direction == 3) {
                    prex = this.footPointX;
                    this.footPointX = (object.getCollisionRect().x0 - (this.collisionRect.getWidth() >> 1)) + 1;
                    this.footPointX = getNewPointX(this.footPointX, 0, getCurrentHeight() >> 1, this.faceDegree);
                    this.movedSpeedX = this.footPointX - prex;
                    if (!(object instanceof DekaPlatform)) {
                        this.movedSpeedX = 0;
                    }
                    if (Key.repeat(Key.gRight) && ((this.animationID == 0 || this.animationID == 47 || this.animationID == 48 || this.animationID == 1 || this.animationID == 2 || this.animationID == 3) && !((object instanceof Hari) && object.objId == 3 && canBeHurt()))) {
                        this.animationID = 8;
                    }
                    if (!((object instanceof Hari) && object.objId == 3 && canBeHurt()) && getVelX() > 0) {
                        setVelX(0);
                        this.worldCal.stopMoveX();
                    }
                    this.rightStopped = true;
                } else {
                    prex = this.footPointX;
                    this.footPointX = (object.getCollisionRect().x1 + (this.collisionRect.getWidth() >> 1)) - 1;
                    this.footPointX = getNewPointX(this.footPointX, 0, getCurrentHeight() >> 1, this.faceDegree);
                    this.movedSpeedX = this.footPointX - prex;
                    if (!(object instanceof DekaPlatform)) {
                        this.movedSpeedX = 0;
                    }
                    if (Key.repeat(Key.gLeft) && ((this.animationID == 0 || this.animationID == 47 || this.animationID == 48 || this.animationID == 1 || this.animationID == 2 || this.animationID == 3) && !((object instanceof Hari) && object.objId == 4 && canBeHurt()))) {
                        this.animationID = 8;
                    }
                    if (!((object instanceof Hari) && object.objId == 4 && canBeHurt()) && getVelX() < 0) {
                        setVelX(0);
                        this.worldCal.stopMoveX();
                    }
                    this.leftStopped = true;
                }
                if (this.collisionState == (byte) 0 && this.animationID == 4 && (object instanceof Hari)) {
                    this.animationID = 0;
                }
                switch (this.collisionState) {
                    case (byte) 1:
                        this.xFirst = false;
                        break;
                }
                if (!(object instanceof GimmickObject)) {
                    this.isStopByObject = false;
                    break;
                } else {
                    this.isStopByObject = true;
                    break;
                }
        }
        this.posX = this.footPointX;
        this.posY = this.footPointY;
    }

    public boolean isAttackingEnemy() {
        if ((this instanceof PlayerAmy) && getCharacterAnimationID() == 39) {
            return false;
        }
        if ((this instanceof PlayerAmy) && (getCharacterAnimationID() == 18 || getCharacterAnimationID() == 19 || getCharacterAnimationID() == 20 || getCharacterAnimationID() == 21 || getCharacterAnimationID() == 22 || getCharacterAnimationID() == 7)) {
            return true;
        }
        if ((this instanceof PlayerSonic) && (getCharacterAnimationID() == 13 || getCharacterAnimationID() == 14 || getCharacterAnimationID() == 15 || getCharacterAnimationID() == 4)) {
            return true;
        }
        if ((this instanceof PlayerTails) && getCharacterAnimationID() == 11) {
            return true;
        }
        if ((this instanceof PlayerKnuckles) && (getCharacterAnimationID() == 11 || getCharacterAnimationID() == 12 || getCharacterAnimationID() == 13 || getCharacterAnimationID() == 19 || getCharacterAnimationID() == 20 || getCharacterAnimationID() == 21 || getCharacterAnimationID() == 22)) {
            return true;
        }
        return this.animationID == 18 || this.animationID == 19 || this.animationID == 20 || this.animationID == 4 || this.animationID == 6 || this.animationID == 7 || invincibleCount > 0;
    }

    public boolean isAttackingItem(boolean pFirstTouch) {
        if (this.ignoreFirstTouch || pFirstTouch) {
            return isAttackingItem();
        }
        return false;
    }

    public boolean isAttackingItem() {
        if ((this instanceof PlayerAmy) && (getCharacterAnimationID() == 21 || getCharacterAnimationID() == 22)) {
            player.setVelY(player.getVelY() - 325);
            return true;
        } else if ((this instanceof PlayerAmy) && getCharacterAnimationID() == 39) {
            return false;
        } else {
            if ((this instanceof PlayerAmy) && getCharacterAnimationID() == 17) {
                return false;
            }
            return this.animationID == 18 || this.animationID == 19 || this.animationID == 20 || this.animationID == 4;
        }
    }

    public int getVelX() {
        if (this.collisionState == (byte) 0) {
            return (this.totalVelocity * MyAPI.dCos(this.faceDegree)) / 100;
        }
        return this.velX;
    }

    public int getVelY() {
        if (this.collisionState == (byte) 0) {
            return (this.totalVelocity * MyAPI.dSin(this.faceDegree)) / 100;
        }
        return this.velY;
    }

    public void setVelX(int mVelX) {
        if (this.collisionState == (byte) 0) {
            int tmpVelX = (this.totalVelocity * MyAPI.dCos(this.faceDegree)) / 100;
            tmpVelX = mVelX;
            this.totalVelocity = ((MyAPI.dCos(this.faceDegree) * tmpVelX) + (MyAPI.dSin(this.faceDegree) * ((this.totalVelocity * MyAPI.dSin(this.faceDegree)) / 100))) / 100;
            return;
        }
        super.setVelX(mVelX);
    }

    public void setVelY(int mVelY) {
        if (this.collisionState == (byte) 0) {
            int dSin = (this.totalVelocity * MyAPI.dSin(this.faceDegree)) / 100;
            this.totalVelocity = ((MyAPI.dCos(this.faceDegree) * ((this.totalVelocity * MyAPI.dCos(this.faceDegree)) / 100)) + (MyAPI.dSin(this.faceDegree) * mVelY)) / 100;
            return;
        }
        super.setVelY(mVelY);
    }

    public void setVelXPercent(int percentage) {
        if (this.collisionState == (byte) 0) {
            int tmpVelX = (this.totalVelocity * MyAPI.dCos(this.faceDegree)) / 100;
            tmpVelX = (this.totalVelocity * percentage) / 100;
            this.totalVelocity = ((MyAPI.dCos(this.faceDegree) * tmpVelX) + (MyAPI.dSin(this.faceDegree) * ((this.totalVelocity * MyAPI.dSin(this.faceDegree)) / 100))) / 100;
            return;
        }
        super.setVelX((this.totalVelocity * percentage) / 100);
    }

    public void setVelYPercent(int percentage) {
        if (this.collisionState == (byte) 0) {
            int tmpVelY = (this.totalVelocity * MyAPI.dSin(this.faceDegree)) / 100;
            this.totalVelocity = ((MyAPI.dCos(this.faceDegree) * ((this.totalVelocity * MyAPI.dCos(this.faceDegree)) / 100)) + (MyAPI.dSin(this.faceDegree) * ((this.totalVelocity * percentage) / 100))) / 100;
            return;
        }
        super.setVelY((this.totalVelocity * percentage) / 100);
    }

    public void beSpring(int springPower, int direction) {
        if (this.collisionState == (byte) 0) {
            calDivideVelocity();
        }
        if (this.isInWater) {
            springPower = (springPower * 185) / 100;
        }
        switch (direction) {
            case 0:
                this.velY = springPower;
                this.worldCal.stopMoveY();
                break;
            case 1:
                this.velY = -springPower;
                this.worldCal.stopMoveY();
                break;
            case 2:
                this.velX = springPower;
                this.worldCal.stopMoveX();
                break;
            case 3:
                this.velX = -springPower;
                this.worldCal.stopMoveX();
                break;
        }
        if (this.collisionState == (byte) 0) {
            calTotalVelocity();
        }
        if ((!this.isAntiGravity && direction == 1) || (this.isAntiGravity && direction == 0)) {
            int i = this.degreeStable;
            this.faceDegree = i;
            this.degreeForDraw = i;
            this.animationID = 9;
            this.collisionState = (byte) 1;
            this.worldCal.actionState = (byte) 1;
            this.collisionChkBreak = true;
            this.drawer.restart();
        }
        if (player instanceof PlayerTails) {
            ((PlayerTails) player).resetFlyCount();
        }
    }

    public void bePop(int springPower, int direction) {
        beSpring(springPower, direction);
        if ((!this.isAntiGravity && direction == 1) || (this.isAntiGravity && direction == 0)) {
            this.animationID = 14;
            this.collisionState = (byte) 1;
            this.worldCal.actionState = (byte) 1;
        }
    }

    public void beHurt() {
        if (player.canBeHurt()) {
            doHurt();
            int bodyCenterX = getNewPointX(this.footPointX, 0, -768, this.faceDegree);
            int bodyCenterY = getNewPointY(this.footPointY, 0, -768, this.faceDegree);
            this.faceDegree = this.degreeStable;
            this.footPointX = getNewPointX(bodyCenterX, 0, 768, this.faceDegree);
            this.footPointY = getNewPointY(bodyCenterY, 0, 768, this.faceDegree);
            if (shieldType != 0) {
                shieldType = 0;
                if (!this.beAttackByHari) {
                    soundInstance.playSe(14);
                }
                if (this.beAttackByHari) {
                    this.beAttackByHari = false;
                }
            } else if (ringNum + ringTmpNum > 0) {
                RingObject.hurtRingExplosion(ringNum + ringTmpNum, getBodyPositionX(), getBodyPositionY(), this.currentLayer, this.isAntiGravity);
                ringNum = 0;
                ringTmpNum = 0;
            } else if (ringNum == 0 && ringTmpNum == 0) {
                setDie(false);
            }
        }
    }

    public void beHurtNoRingLose() {
        if (player.canBeHurt()) {
            doHurt();
            int bodyCenterX = getNewPointX(this.footPointX, 0, -768, this.faceDegree);
            int bodyCenterY = getNewPointY(this.footPointY, 0, -768, this.faceDegree);
            this.faceDegree = this.degreeStable;
            this.footPointX = getNewPointX(bodyCenterX, 0, 768, this.faceDegree);
            this.footPointY = getNewPointY(bodyCenterY, 0, 768, this.faceDegree);
            if (shieldType != 0) {
                shieldType = 0;
            }
        }
    }

    public void beHurtByCage() {
        if (this.hurtCount == 0) {
            doHurt();
            this.velX = (this.velX * 3) / 2;
            this.velY = (this.velY * 3) / 2;
        }
    }

    public void doHurt() {
        int i;
        this.animationID = 44;
        if (this.collisionState == (byte) 2) {
            this.footPointY -= 128;
            prepareForCollision();
        }
        if (this.outOfControl && this.outOfControlObject != null && this.outOfControlObject.releaseWhileBeHurt()) {
            this.outOfControl = false;
            this.outOfControlObject = null;
        }
        this.hurtCount = 48;
        if (this.velX == 0) {
            this.velX = (this.faceDirection ? -1 : 1) * HURT_POWER_X;
        } else if (this.velX > 0) {
            this.velX = -HURT_POWER_X;
        } else {
            this.velX = HURT_POWER_X;
        }
        if (this.isAntiGravity) {
            this.velX = -this.velX;
        }
        if (this.isAntiGravity) {
            i = -1;
        } else {
            i = 1;
        }
        this.velY = i * HURT_POWER_Y;
        this.collisionState = (byte) 1;
        this.worldCal.actionState = (byte) 1;
        this.collisionChkBreak = true;
        this.worldCal.stopMove();
        this.onObjectContinue = false;
        this.footOnObject = null;
        this.hurtNoControl = true;
        this.attackAnimationID = 0;
        this.attackCount = 0;
        this.attackLevel = 0;
        this.dashRolling = false;
        MyAPI.vibrate();
        this.degreeRotateMode = 0;
    }

    public boolean canBeHurt() {
        if (this.hurtCount > 0 || invincibleCount > 0 || this.isDead) {
            return false;
        }
        return true;
    }

    public boolean isFootOnObject(GameObject object) {
        if (this.outOfControl) {
            return false;
        }
        if (this.collisionState != (byte) 2) {
            return false;
        }
        return this.footOnObject == object;
    }

    public boolean isFootObjectAndLogic(GameObject object) {
        return this.footObjectLogic && this.footOnObject == object && this.collisionState == (byte) 2;
    }

    public void setFootPositionX(int x) {
        this.footPointX = x;
        this.posX = x;
    }

    public void setFootPositionY(int y) {
        this.footPointY = y;
        this.posY = y;
    }

    public void setBodyPositionX(int x) {
        setFootPositionX(x);
    }

    public void setBodyPositionY(int y) {
        setFootPositionY(y + 768);
    }

    public int getBodyPositionX() {
        return getFootPositionX();
    }

    public int getBodyPositionY() {
        return getFootPositionY() + (this.isAntiGravity ? 768 : -768);
    }

    private int spinLv2Calc() {
        return (((this.isInWater ? SPIN_INWATER_START_SPEED_2 : SPIN_START_SPEED_2) * (SONIC_ATTACK_LEVEL_3_V0 - (this.spinDownWaitCount * 36))) / 12) / 100;
    }

    public void dashRollingLogic() {
        int i;
        this.animationID = 6;
        if (this.spinCount > 9) {
            this.animationID = 7;
        } else {
            if (Key.press(16777216 | Key.gUp)) {
                this.spinDownWaitCount = 0;
                this.spinCount = 12;
                this.animationID = 7;
                this.spinKeyCount = 20;
                this.drawer.restart();
                if (characterID != 3) {
                    soundInstance.playSe(4);
                }
            } else if (Key.repeat((2097152 | Key.B_7) | Key.B_9) && this.spinKeyCount == 0) {
                this.spinCount = 12;
                this.animationID = 7;
                this.spinKeyCount = 20;
                this.drawer.restart();
                if (characterID != 3) {
                    soundInstance.playSe(4);
                }
            }
            if (this.spinCount == 0 && this.spinKeyCount > 0) {
                this.spinKeyCount--;
            }
        }
        if (this.spinCount > 0) {
            if (this.spinDownWaitCount < 12) {
                this.spinDownWaitCount++;
            } else {
                this.spinDownWaitCount = 12;
            }
        }
        if (this.spinCount > 0) {
            this.spinCount--;
            this.effectID = 1;
        } else {
            this.effectID = 0;
        }
        switch (this.collisionState) {
            case (byte) 0:
                this.totalVelocity = 0;
                break;
            default:
                this.velX = 0;
                break;
        }
        if (!Key.repeat(((Key.gDown | Key.B_7) | Key.B_9) | 2097152)) {
            this.effectID = -1;
            switch (this.collisionState) {
                case (byte) 0:
                    this.totalVelocity = SPIN_START_SPEED_1;
                    if (this.isInWater) {
                        this.totalVelocity = SPIN_INWATER_START_SPEED_1;
                    }
                    if (this.spinCount > 0) {
                        this.totalVelocity = spinLv2Calc();
                        SoundSystem.getInstance().playSe(5);
                    } else {
                        SoundSystem.getInstance().playSe(5);
                    }
                    if (!this.faceDirection) {
                        this.totalVelocity = -this.totalVelocity;
                        break;
                    }
                    break;
                default:
                    this.velX = SPIN_START_SPEED_1;
                    if (this.isInWater) {
                        this.totalVelocity = SPIN_INWATER_START_SPEED_1;
                    }
                    if (this.spinCount > 0) {
                        this.velX = spinLv2Calc();
                        SoundSystem.getInstance().playSe(5);
                    } else {
                        SoundSystem.getInstance().playSe(5);
                    }
                    if (!this.faceDirection) {
                        if (this.isAntiGravity) {
                            i = 1;
                        } else {
                            i = -1;
                        }
                        this.velX = i * this.velX;
                        break;
                    }
                    this.velX = (this.isAntiGravity ? -1 : 1) * this.velX;
                    break;
            }
            this.spinCount = 0;
            this.animationID = 4;
            this.dashRolling = false;
            this.ignoreFirstTouch = true;
            this.isAfterSpinDash = true;
        }
        switch (this.collisionState) {
            case (byte) 0:
                return;
            case (byte) 3:
                this.velY = 100;
                return;
            default:
                int i2;
                i = this.velY;
                if (this.isAntiGravity) {
                    i2 = -1;
                } else {
                    i2 = 1;
                }
                this.velY = i + (i2 * getGravity());
                return;
        }
    }

    public void beWaterFall() {
        this.waterFalling = true;
        this.velY += GRAVITY / 10;
    }

    public boolean getWaterFallState() {
        return this.waterFalling;
    }

    public void initWaterFall() {
        if (this.waterFallDrawer == null) {
            MFImage image = null;
            if (StageManager.getCurrentZoneId() == 5) {
                image = MFImage.createImage("/animation/water_fall_5.png");
            }
            if (image == null) {
                this.waterFallDrawer = new Animation("/animation/water_fall").getDrawer(0, true, 0);
            } else {
                this.waterFallDrawer = new Animation(image, "/animation/water_fall").getDrawer(0, true, 0);
            }
        }
    }

    private void waterFallDraw(MFGraphics g, Coordinate camera) {
        if (this.waterFalling) {
            int offset_y = (characterID == 2 && this.myAnimationID == 22) ? INVINCIBLE_COUNT : -320;
            drawInMap(g, this.waterFallDrawer, (this.collisionRect.x0 + this.collisionRect.x1) >> 1, this.collisionRect.y0 + offset_y);
            this.waterFalling = false;
        }
    }

    public void initWaterFlush() {
        if (this.waterFlushDrawer == null) {
            MFImage image = null;
            if (StageManager.getCurrentZoneId() == 5) {
                image = MFImage.createImage("/animation/water_flush_5.png");
            }
            if (image == null) {
                this.waterFlushDrawer = new Animation("/animation/water_flush").getDrawer(0, true, 0);
            } else {
                this.waterFlushDrawer = new Animation(image, "/animation/water_flush").getDrawer(0, true, 0);
            }
        }
    }

    private void waterFlushDraw(MFGraphics g) {
        if (this.showWaterFlush) {
            int i;
            initWaterFlush();
            AnimationDrawer animationDrawer = this.waterFlushDrawer;
            int i2 = this.footPointX;
            if (StageManager.getCurrentZoneId() == 4 || StageManager.getCurrentZoneId() == 5) {
                i = this.collisionRect.y1 - 512;
            } else {
                i = this.collisionRect.y1;
            }
            drawInMap(g, animationDrawer, i2, i);
            this.showWaterFlush = false;
        }
    }

    public boolean beAccelerate(int power, boolean IsX, GameObject sender) {
        if (this.collisionState == (byte) 0) {
            this.totalVelocity = power;
            this.faceDirection = this.totalVelocity > 0;
            return true;
        } else if (this.collisionState != (byte) 2 || (sender instanceof Accelerate)) {
            return false;
        } else {
            if (IsX) {
                this.velX = power;
            } else {
                this.velY = power;
            }
            return true;
        }
    }

    public boolean isOnGound() {
        return this.collisionState == (byte) 0;
    }

    public boolean doPoalMotion(int x, int y, boolean isLeft) {
        if (this.collisionState == (byte) 0) {
            this.collisionState = (byte) 1;
        }
        if (this.collisionState != (byte) 1) {
            return false;
        }
        this.animationID = 13;
        this.faceDirection = !isLeft;
        this.footPointX = x;
        this.footPointY = y + 2048;
        this.velX = 0;
        this.velY = 0;
        return true;
    }

    public boolean doPoalMotion2(int x, int y, boolean direction) {
        if (this.collisionState != (byte) 0 || ((!this.faceDirection || !direction || this.totalVelocity < DO_POAL_MOTION_SPEED) && (this.faceDirection || direction || this.totalVelocity > -600))) {
            return false;
        }
        int i;
        this.animationID = 31;
        this.faceDirection = direction;
        this.footPointX = ((this.faceDirection ? -1 : 1) * 1024) + x;
        setNoKey();
        if (this.faceDirection) {
            i = -1;
        } else {
            i = 1;
        }
        this.totalVelocity = i * 300;
        this.worldCal.stopMoveX();
        return true;
    }

    public void doPullMotion(int x, int y) {
        this.animationID = 24;
        this.footPointX = x;
        this.footPointY = y + 2048;
        this.velX = 0;
        this.velY = 0;
        if (this.faceDirection) {
            this.footPointX -= 256;
        } else {
            this.footPointX += 256;
        }
    }

    public void doPullBarMotion(int y) {
        this.animationID = 27;
        this.footPointY = y + 1792;
        this.velX = 0;
        this.velY = 0;
    }

    public void doWalkPoseInAir() {
        if (this.collisionState != (byte) 1) {
            return;
        }
        if (Math.abs(this.velX) < SPEED_LIMIT_LEVEL_1) {
            this.animationID = 1;
        } else if (Math.abs(this.velX) < SPEED_LIMIT_LEVEL_2) {
            this.animationID = 2;
        } else {
            this.animationID = 3;
        }
    }

    public void doDripInAir() {
        if (this.collisionState == (byte) 1) {
            if (this.animationID == 4) {
                this.animationID = 4;
            } else if (Math.abs(this.velX) < SPEED_LIMIT_LEVEL_1) {
                this.animationID = 1;
            } else if (Math.abs(this.velX) < SPEED_LIMIT_LEVEL_2) {
                this.animationID = 2;
            } else {
                this.animationID = 3;
            }
        }
        this.bankwalking = false;
    }

    public void setAnimationId(int id) {
        this.animationID = id;
    }

    public void restartAniDrawer() {
        this.drawer.restart();
    }

    public int getAnimationId() {
        return this.animationID;
    }

    public void refreshCollisionRectWrap() {
        int RECT_HEIGHT = getCollisionRectHeight();
        int RECT_WIDTH = getCollisionRectWidth();
        int switchDegree = this.faceDegree;
        int yOffset = 0;
        if (this.animationID == 11) {
            if (getAnimationOffset() == 1) {
                yOffset = -960;
            } else {
                yOffset = -320;
            }
            switchDegree = 0;
        }
        this.checkPositionX = getNewPointX(this.footPointX, 0, (-RECT_HEIGHT) >> 1, switchDegree) + 0;
        this.checkPositionY = getNewPointY(this.footPointY, 0, (-RECT_HEIGHT) >> 1, switchDegree) + yOffset;
        this.collisionRect.setTwoPosition(this.checkPositionX - (RECT_WIDTH >> 1), this.checkPositionY - (RECT_HEIGHT >> 1), this.checkPositionX + (RECT_WIDTH >> 1), this.checkPositionY + (RECT_HEIGHT >> 1));
    }

    public int getCollisionRectWidth() {
        if (this.animationID == 21) {
            return 1536;
        }
        return 1024;
    }

    public int getCollisionRectHeight() {
        if (this.animationID == 4 || this.animationID == 5 || this.animationID == 46 || this.animationID == 6 || this.animationID == 7 || this.animationID == 18 || this.animationID == 19 || this.animationID == 20) {
            return BarHorbinV.HOBIN_POWER;
        }
        return 1536;
    }

    public void refreshCollisionRect(int x, int y) {
    }

    public void fallChk() {
        if (this.fallTime > 0) {
            this.fallTime--;
            if (this.animationID == 0) {
                this.animationID = 1;
                return;
            }
            return;
        }
        if (this.isAntiGravity || this.faceDegree < 45 || this.faceDegree > 315) {
            if (!this.isAntiGravity) {
                return;
            }
            if (this.faceDegree > StringIndex.FONT_COLON_RED && this.faceDegree < 225) {
                return;
            }
        }
        if (Math.abs(this.totalVelocity) < 474) {
            if (this.totalVelocity == 0) {
                calDivideVelocity();
                this.velY += getGravity();
                calTotalVelocity();
            }
            this.fallTime = 7;
        }
    }

    public void railIn(int x, int y) {
        this.railLine = null;
        this.velY = 0;
        this.velX = 0;
        this.worldCal.stopMoveX();
        setFootPositionX(x);
        this.collisionChkBreak = true;
        this.railing = true;
        this.railOut = false;
        this.animationID = 21;
        setNoKey();
        if (characterID == 3) {
            soundInstance.playSe(25);
        } else {
            soundInstance.playSe(37);
        }
    }

    public void railOut(int x, int y) {
        if (this.railing) {
            this.railOut = true;
            this.railLine = null;
            this.velY = RAIL_OUT_SPEED_VY0;
            this.velX = 0;
            setVelX(0);
            setFootPositionX(x);
            setFootPositionY(y);
            this.collisionChkBreak = true;
            this.animationID = 4;
        }
    }

    public void pipeIn(int x, int y, int vx, int vy) {
        this.piping = true;
        this.pipeState = (byte) 0;
        this.pipeDesX = x;
        this.pipeDesY = y + 768;
        this.velX = 250;
        this.velY = 250;
        this.nextVelX = (vx << 6) / 1;
        this.nextVelY = (vy << 6) / 1;
        this.collisionChkBreak = true;
    }

    public void pipeSet(int x, int y, int vx, int vy) {
        int degree = 0;
        int sourceSpeed = 0;
        if (this.piping) {
            this.pipeDesX = x;
            this.pipeDesY = y + 768;
            degree = crlFP32.actTanDegree(vy, vx);
            sourceSpeed = crlFP32.sqrt((vy * vy) + (vx * vx)) >> 6;
            this.nextVelX = vx;
            this.nextVelY = vy;
            this.pipeState = (byte) 2;
            if (this.velX > 0 && this.footPointX > this.pipeDesX) {
                this.footPointX = this.pipeDesX;
            }
            if (this.velX < 0 && this.footPointX < this.pipeDesX) {
                this.footPointX = this.pipeDesX;
            }
            if (this.velY > 0 && this.footPointY > this.pipeDesY) {
                this.footPointY = this.pipeDesY;
            }
            if (this.velY < 0 && this.footPointY < this.pipeDesY) {
                this.footPointY = this.pipeDesY;
            }
            this.collisionChkBreak = true;
            return;
        }
        this.footPointX = x;
        this.footPointY = y;
        degree = crlFP32.actTanDegree(vy, vx);
        sourceSpeed = crlFP32.sqrt((vy * vy) + (vx * vx)) >> 6;
        this.nextVelX = vx;
        this.nextVelY = vy;
        this.velX = vx;
        this.velY = vy;
        this.pipeState = (byte) 1;
        this.piping = true;
        this.collisionChkBreak = true;
        this.worldCal.stopMove();
    }

    public void pipeOut() {
        if (this.piping) {
            this.piping = false;
            this.collisionState = (byte) 1;
            this.worldCal.actionState = (byte) 1;
        }
    }

    public void setFall(int x, int y, int left, int top) {
        if (this instanceof PlayerTails) {
            ((PlayerTails) this).stopFly();
        }
        this.railing = true;
        setFootPositionX(x);
        this.velX = 0;
        this.velY = 0;
        this.railLine = null;
        this.collisionChkBreak = true;
    }

    public void setFallOver() {
        this.railing = false;
    }

    public void setRailFlip() {
        this.velX = 0;
        this.velY = RAIL_FLIPPER_V0;
        this.railLine = null;
        this.collisionChkBreak = true;
        this.railFlipping = true;
        SoundSystem.getInstance().playSe(54);
    }

    public boolean setRailLine(Line line, int startX, int startY, int railDivX, int railDivY, int railDevX, int railDevY, GameObject obj) {
        if (!obj.getCollisionRect().collisionChk(this.footPointX, this.footPointY)) {
            return false;
        }
        if (!this.railing || this.velY < 0) {
            return false;
        }
        if (this.railLine == null) {
            this.totalVelocity = 0;
        }
        this.railLine = line;
        calDivideVelocity();
        this.posX = startX;
        this.posY = startY;
        if (Math.abs(railDivY) <= 1) {
            this.velX = (railDivX * 1920) / railDevX;
            this.velY = 0;
            if (this.railFlipping) {
                this.railFlipping = false;
                setFootPositionY(this.railLine.getY(this.footPointX) + 768);
            } else {
                setFootPositionY((this.railLine.getY(this.footPointX) - 512) + 768);
            }
        } else {
            this.velX = (railDivX * 1920) / railDevX;
            this.velY = (railDivY * 1920) / railDevY;
            setFootPositionY(this.railLine.getY(this.footPointX) + 768);
        }
        calTotalVelocity();
        System.out.println("~~1velX:" + this.velX + "|velY:" + this.velY);
        this.collisionChkBreak = true;
        return true;
    }

    public void checkWithObject(int preX, int preY, int currentX, int currentY) {
        int moveDistanceX = currentX - preX;
        int moveDistanceY = currentY - preY;
        if (moveDistanceX == 0 && moveDistanceY == 0) {
            this.footPointX = currentX;
            this.footPointY = currentY;
            return;
        }
        int moveDistance;
        if (Math.abs(moveDistanceX) >= Math.abs(moveDistanceY)) {
            moveDistance = Math.abs(moveDistanceX);
        } else {
            moveDistance = Math.abs(moveDistanceY);
        }
        int preCheckX = preX;
        int preCheckY = preY;
        int i = 0;
        while (i <= moveDistance && i < moveDistance) {
            i += 512;
            if (i >= moveDistance) {
                i = moveDistance;
            }
            int tmpCurrentX = preX + ((moveDistanceX * i) / moveDistance);
            int tmpCurrentY = preY + ((moveDistanceY * i) / moveDistance);
            player.moveDistance.f13x = (tmpCurrentX >> 6) - (preCheckX >> 6);
            player.moveDistance.f14y = (tmpCurrentY >> 6) - (preCheckY >> 6);
            this.footPointX = tmpCurrentX;
            this.footPointY = tmpCurrentY;
            collisionCheckWithGameObject(tmpCurrentX, tmpCurrentY);
            if (!this.collisionChkBreak) {
                preCheckX = tmpCurrentX;
                preCheckY = tmpCurrentY;
            } else {
                return;
            }
        }
    }

    public void cancelFootObject(GameObject object) {
        if (this.collisionState == (byte) 2 && isFootOnObject(object)) {
            player.collisionState = (byte) 1;
            player.footOnObject = null;
            this.onObjectContinue = false;
        }
    }

    public void cancelFootObject() {
        if (this.collisionState == (byte) 2) {
            player.footOnObject = null;
            this.onObjectContinue = false;
        }
    }

    public void doItemAttackPose(GameObject object, int direction) {
        if (!this.extraAttackFlag) {
            int i;
            int maxPower = this.isPowerShoot ? SHOOT_POWER : MIN_ATTACK_JUMP;
            if (this.isAntiGravity) {
                i = 1;
            } else {
                i = -1;
            }
            int newVelY = i * getVelY();
            if (newVelY > 0) {
                newVelY = -newVelY;
            } else if (newVelY > maxPower) {
                newVelY = maxPower;
            }
            if (this.doJumpForwardly) {
                newVelY = maxPower;
            }
            if (characterID != 2 || this.myAnimationID < 19 || this.myAnimationID > 22) {
                setVelY((this.isAntiGravity ? -1 : 1) * newVelY);
            }
            if (characterID != 3) {
                switch (direction) {
                    case 1:
                        cancelFootObject(this);
                        this.collisionState = (byte) 1;
                        this.animationID = 4;
                        break;
                }
            }
            if (this.isPowerShoot) {
                this.isPowerShoot = false;
            }
        }
    }

    public void doAttackPose(GameObject object, int direction) {
        if (!this.extraAttackFlag) {
            int newVelY = (this.isAntiGravity ? 1 : -1) * getVelY();
            if (newVelY > 0) {
                newVelY = -newVelY;
            } else if (newVelY > MIN_ATTACK_JUMP) {
                newVelY = MIN_ATTACK_JUMP;
            }
            if (this.doJumpForwardly) {
                newVelY = MIN_ATTACK_JUMP;
            }
            if (characterID != 3) {
                setVelY((this.isAntiGravity ? -1 : 1) * newVelY);
            } else if (!IsInvincibility() || this.myAnimationID < 14 || this.myAnimationID > 17) {
                int i;
                if (this.isAntiGravity) {
                    i = -1;
                } else {
                    i = 1;
                }
                setVelY(i * newVelY);
            }
            if (characterID != 3) {
                switch (direction) {
                    case 1:
                        cancelFootObject(this);
                        this.collisionState = (byte) 1;
                        return;
                    default:
                        return;
                }
            }
        }
    }

    public void doBossAttackPose(GameObject object, int direction) {
        if (this.collisionState == (byte) 1) {
            if (characterID != 3) {
                setVelX(-this.velX);
            }
            if ((-this.velY) < (-ATTACK_POP_POWER)) {
                setVelY(-ATTACK_POP_POWER);
            } else if (characterID != 2) {
                setVelY(-this.velY);
            } else if (getCharacterAnimationID() == 19 || getCharacterAnimationID() == 20 || getCharacterAnimationID() == 21 || getCharacterAnimationID() == 22) {
                setVelY((-this.velY) - 325);
            } else {
                setVelY(-this.velY);
            }
        }
    }

    public boolean inRailState() {
        return this.railing || this.railOut;
    }

    public void changeVisible(boolean mVisible) {
        this.visible = mVisible;
    }

    public void setOutOfControl(GameObject object) {
        this.outOfControl = true;
        this.outOfControlObject = object;
        this.piping = false;
    }

    public void setOutOfControlInPipe(GameObject object) {
        this.outOfControl = true;
        this.outOfControlObject = object;
    }

    public void releaseOutOfControl() {
        this.outOfControl = false;
        this.outOfControlObject = null;
    }

    public boolean isControlObject(GameObject object) {
        return this.controlObjectLogic && object == this.outOfControlObject;
    }

    public void setDieInit(boolean isDrowning, int v0) {
        this.velX = 0;
        if (!isDrowning || this.breatheNumCount < 6) {
            this.velY = v0;
        } else {
            this.velY = 0;
        }
        if (this.isAntiGravity) {
            this.velY = -this.velY;
        }
        int i = this.degreeStable;
        this.faceDegree = i;
        this.degreeForDraw = i;
        this.collisionState = (byte) 1;
        MapManager.setFocusObj(null);
        this.isDead = true;
        this.finishDeadStuff = false;
        this.animationID = 45;
        this.drawer.restart();
        timeStopped = true;
        this.worldCal.stopMove();
        this.collisionChkBreak = true;
        this.hurtCount = 0;
        this.dashRolling = false;
        if (this.effectID == 0 || this.effectID == 1) {
            this.effectID = -1;
        }
        this.drownCnt = 0;
        if (stageModeState == 1 && StageManager.getStageID() == 10) {
            RocketSeparateEffect.clearInstance();
        }
        GameState.isThroughGame = true;
        shieldType = 0;
        invincibleCount = 0;
        speedCount = 0;
        if (this.currentLayer == 0) {
            this.currentLayer = 1;
        } else if (this.currentLayer == 1) {
            this.currentLayer = 0;
        }
        resetFlyCount();
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setDie(boolean r3, int r4) {
        setDieInit(r3, r4);
        int i = SoundSystem.getInstance().getPlayingBGMIndex();
        SoundSystem.getInstance();
        if (i == SoundSystem.BGM_ASPHYXY)
        {
            SoundSystem.getInstance().stopBgm(false);
        } else
        {
            SoundSystem.getInstance().getPlayingBGMIndex();
        }
        if (!r3) {
            soundInstance.playSe(14);
        } else {
            soundInstance.playSe(60);
        }
        //throw new UnsupportedOperationException("Method not decompiled: SonicGBA.PlayerObject.setDie(boolean, int):void");
        System.out.print("throw new UnsupportedOperationException(\"Method not decompiled: SonicGBA.PlayerObject.setDie(boolean, int):void\");");
    }

    public void setDieWithoutSE() {
        setDieInit(false, DIE_DRIP_STATE_JUMP_V0);
    }

    public void setDie(boolean isDrowning) {
        setDie(isDrowning, DIE_DRIP_STATE_JUMP_V0);
    }

    public void setNoKey() {
        this.noKeyFlag = true;
    }

    public void setCollisionState(byte state) {
        if (this.collisionState == (byte) 0) {
            calDivideVelocity();
        }
        switch (state) {
            case (byte) 1:
                this.faceDegree = this.degreeStable;
                this.worldCal.actionState = (byte) 1;
                break;
        }
        this.collisionState = state;
    }

    public void setSlip() {
        if (this.collisionState == (byte) 0) {
            this.slipFlag = true;
            this.showWaterFlush = true;
            this.animationID = 30;
            setNoKey();
        }
    }

    public boolean beUnseenPop() {
        if (this.collisionState != (byte) 0 || Math.abs(getVelX()) <= 1024) {
            return false;
        }
        beSpring(getGravity() + 2048, 1);
        int nextVelX = 2048;
        if (2048 > HINER_JUMP_MAX) {
            nextVelX = HINER_JUMP_MAX;
        }
        if (getVelX() > 0) {
            beSpring(nextVelX, 2);
        } else {
            beSpring(nextVelX, 3);
        }
        SoundSystem.getInstance().playSequenceSe(37);
        return true;
    }

    public void setBank() {
        this.onBank = !this.onBank;
        if (this.onBank && this.collisionState == (byte) 0) {
            calDivideVelocity();
        }
    }

    public void bankLogic() {
        if (this.onBank) {
            this.faceDegree = 0;
            inputLogicWalk();
            if (this.onBank) {
                calDivideVelocity();
                this.velY = 0;
                int preX = player.getFootPositionX();
                int preY = player.getFootPositionY();
                this.footPointX += this.velX;
                int yLimit = CENTER_Y - (((MyAPI.dCos((((this.footPointX - CENTER_X) * B_1) / B_2) >> 6) * 3072) / 100) + 3072);
                decelerate();
                int velX = player.getVelX();
                if (Math.abs(velX) > BANKING_MIN_SPEED) {
                    player.setFootPositionY(Math.max(yLimit, player.getFootPositionY() - ((Math.abs(velX) * 400) / BANKING_MIN_SPEED)));
                } else {
                    player.setFootPositionY(Math.min(CENTER_Y, player.getFootPositionY() + 200));
                    if (this.footPointY >= CENTER_Y) {
                        this.onBank = false;
                        this.collisionState = (byte) 1;
                        this.worldCal.actionState = (byte) 1;
                        this.bankwalking = false;
                    }
                }
                if (this.animationID != 4) {
                    if (Math.abs(velX) <= BANKING_MIN_SPEED) {
                        this.onBank = false;
                        this.collisionState = (byte) 1;
                        this.worldCal.actionState = (byte) 1;
                        doDripInAir();
                    } else if (this.footPointY < 61184) {
                        this.animationID = 34;
                    } else if (this.footPointY < 61952) {
                        this.animationID = 33;
                    } else if (this.footPointY < 62720) {
                        this.animationID = 32;
                    }
                }
                checkWithObject(preX, preY, this.footPointX, this.footPointY);
            }
        }
    }

    public void setTerminal(int type) {
        this.terminalOffset = 0;
        terminalType = type;
        this.terminalCount = 10;
        isTerminal = true;
        timeStopped = true;
        switch (terminalType) {
            case 0:
            case 2:
                if (this.collisionState == (byte) 0) {
                    if (this.animationID == 4) {
                        land();
                    }
                    if (this.totalVelocity > MAX_VELOCITY) {
                        this.totalVelocity = MAX_VELOCITY;
                        return;
                    }
                    return;
                }
                return;
            case 1:
                changeVisible(false);
                this.noMoving = true;
                return;
            case 3:
                terminalState = (byte) 0;
                return;
            default:
                return;
        }
    }

    public void setTerminalSingle(int type) {
        terminalType = type;
        this.terminalCount = 10;
        isTerminal = true;
        timeStopped = true;
    }

    public boolean isTerminalRunRight() {
        return isTerminal && (terminalType == 0 || terminalType == 2 || (terminalType == 3 && terminalState == (byte) 0 && this.posX < SUPER_SONIC_STAND_POS_X));
    }

    public boolean doBrake() {
        return isTerminal && terminalType == 3 && terminalState == (byte) 1 && this.posX > SUPER_SONIC_STAND_POS_X && this.totalVelocity > 0;
    }

    public void beTrans(int desX, int desY) {
        this.animationID = 4;
        this.collisionState = (byte) 1;
        this.transing = true;
        setBodyPositionX(desX);
        setBodyPositionY(desY);
        MapManager.setCameraMoving();
        calPreCollisionRect();
    }

    public void setCelebrate() {
        timeStopped = true;
        this.isCelebrate = true;
        MapManager.setCameraLeftLimit(MapManager.getCamera().f13x);
        MapManager.setCameraRightLimit(MapManager.getCamera().f13x + MapManager.CAMERA_WIDTH);
        if (this.faceDirection) {
            this.moveLimit = this.posX + 3840;
        } else {
            this.moveLimit = this.posX - 3840;
        }
    }

    public void getPreItem(int itemId) {
        for (int i = 0; i < 5; i++) {
            if (itemVec[i][0] == -1) {
                itemVec[i][0] = itemId;
                itemVec[i][1] = 20;
                return;
            }
        }
    }

    public void getItem(int itemId) {
        switch (itemId) {
            case 0:
                addLife();
                playerLifeUpBGM();
                return;
            case 1:
                shieldType = 1;
                soundInstance.playSe(41);
                return;
            case 2:
                shieldType = 2;
                soundInstance.playSe(41);
                return;
            case 3:
                invincibleCount = INVINCIBLE_COUNT;
                SoundSystem.getInstance().stopBgm(false);
                SoundSystem.getInstance().playBgm(44);
                return;
            case 4:
                speedCount = INVINCIBLE_COUNT;
                SoundSystem.getInstance().setSoundSpeed(2.0f);
                if (SoundSystem.getInstance().getPlayingBGMIndex() != 43) {
                    SoundSystem.getInstance().restartBgm();
                    return;
                }
                return;
            case 5:
                if (this.hurtCount == 0) {
                    getRing(ringRandomNum);
                    return;
                }
                return;
            case 6:
                if (this.hurtCount == 0) {
                    getRing(5);
                    return;
                }
                return;
            case 7:
                if (this.hurtCount == 0) {
                    getRing(10);
                    return;
                }
                return;
            default:
                return;
        }
    }

    public static void getTmpRing(int itemId) {
        switch (itemId) {
            case 5:
                ringTmpNum = RANDOM_RING_NUM[MyRandom.nextInt(RANDOM_RING_NUM.length)];
                ringRandomNum = ringTmpNum;
                return;
            case 6:
                ringTmpNum = 5;
                return;
            case 7:
                ringTmpNum = 10;
                return;
            default:
                return;
        }
    }

    public static void getRing(int num) {
        int preRingNum = ringNum;
        ringNum += num;
        if (stageModeState != 1 && StageManager.getCurrentZoneId() != 8) {
            if (preRingNum / 100 != ringNum / 100) {
                addLife();
                playerLifeUpBGM();
            }
            if (ringTmpNum != 0) {
                ringTmpNum = 0;
            }
        }
    }

    public boolean isAttracting() {
        return shieldType == 2;
    }

    public void getEnemyScore() {
        scoreNum += 100;
        raceScoreNum += 100;
    }

    public void getBossScore() {
        scoreNum += 1000;
        raceScoreNum += 1000;
    }

    public void getBallHobinScore() {
        scoreNum += 10;
        raceScoreNum += 10;
    }

    public void ductIn() {
        this.ducting = true;
        this.pushOnce = true;
        this.ductingCount = 0;
    }

    public void ductOut() {
        this.ducting = false;
        this.pushOnce = false;
        this.ductingCount = 0;
    }

    public void setSqueezeEnable(boolean enable) {
        this.squeezeFlag = enable;
    }

    protected boolean isHeadCollision() {
        boolean collision = false;
        int headBlockY = this.worldInstance.getWorldY(this.footPointX, this.footPointY - 1536, 1, 2);
        int headBlockY2 = this.worldInstance.getWorldY(this.footPointX + 1024, this.footPointY - 1536, 1, 2);
        if (headBlockY >= 0) {
            collision = true;
        }
        if (headBlockY2 >= 0) {
            return true;
        }
        return collision;
    }

    public static void addLife() {
        lifeNum++;
    }

    public static void minusLife() {
        lifeNum--;
    }

    public static int getLife() {
        return lifeNum;
    }

    public static void setLife(int num) {
        lifeNum = num;
    }

    public static void setScore(int num) {
        scoreNum = num;
    }

    public static int getScore() {
        return scoreNum;
    }

    public static void resetGameParam() {
        scoreNum = 0;
        lifeNum = 2;
    }

    public void resetPlayer() {
        this.footPointX = this.deadPosX;
        this.footPointY = this.deadPosY;
        this.worldCal.stopMove();
        StageManager.resetStageGameover();
        this.velX = 0;
        this.velY = 0;
        setVelX(this.velX);
        setVelY(this.velY);
        this.totalVelocity = 0;
        this.collisionState = (byte) 0;
        MapManager.setFocusObj(this);
        MapManager.focusQuickLocation();
        this.isDead = false;
        this.animationID = 0;
        timeStopped = false;
        invincibleCount = SSDef.PLAYER_MOVE_HEIGHT;
        preScoreNum = scoreNum;
        preLifeNum = lifeNum;
        timeCount = 0;
        lastTimeCount = timeCount;
    }

    public static void doInitInNewStage() {
        currentMarkId = 0;
    }

    public static void initStageParam() {
        ringNum = 0;
        invincibleCount = 0;
        speedCount = 0;
        SoundSystem.getInstance().setSoundSpeed(1.0f);
        shieldType = 0;
        timeCount = 0;
        lastTimeCount = timeCount;
        timeStopped = false;
        raceScoreNum = 0;
        preScoreNum = scoreNum;
        preLifeNum = lifeNum;
        for (int i = 0; i < 5; i++) {
            itemVec[i][0] = -1;
        }
        setOverCount(SonicDef.OVER_TIME);
    }

    public static void initSpParam(int param_ringNum, int checkPointID, int param_timeCount) {
        if (player != null) {
            PlayerObject playerObject = player;
            currentMarkId = checkPointID;
        }
        ringNum = param_ringNum;
        timeCount = param_timeCount;
        lastTimeCount = timeCount;
    }

    public static void doPauseLeaveGame() {
        scoreNum = preScoreNum;
        lifeNum = preLifeNum;
    }

    public void headInit() {
        if (GameState.guiAnimation == null) {
            GameState.guiAnimation = new Animation("/animation/gui");
        }
        headDrawer = GameState.guiAnimation.getDrawer(characterID, false, 0);
        this.isAttackBoss4 = false;
    }

    public static void drawGameUI(MFGraphics g) {
        if (!isTerminal || terminalType != 3 || terminalState <= (byte) 3) {
            GameState.guiAniDrawer.draw(g, 5, uiOffsetX + 0, 0, false, 0);
            int i = ringNum;
            int i2 = uiOffsetX + 12;
            int i3 = (ringNum == 0 && (timeCount / SSDef.PLAYER_MOVE_HEIGHT) % 2 == 0) ? 3 : 0;
            drawNum(g, i, i2, 15, 0, i3);
            drawNum(g, stageModeState == 1 ? raceScoreNum : scoreNum, NumberSideX + uiOffsetX, 8, 2, 0);
            timeDraw(g, NumberSideX + uiOffsetX, 22);
            if (stageModeState != 1) {
                if (StageManager.getCurrentZoneId() == 8) {
                    if (player.isDead) {
                        headDrawer.setActionId(0);
                    } else {
                        headDrawer.setActionId(4);
                    }
                }
                headDrawer.draw(g, SCREEN_WIDTH, 0);
                drawNum(g, lifeNum >= 9 ? 9 : lifeNum, SCREEN_WIDTH - 9, 4, 12, 0);
            }
        }
    }

    public static void drawNum(MFGraphics g, int num, int x, int y, int anchor, int type) {
        int divideNum = 10;
        int blockNum = 1;
        int i = 0;
        while (num / divideNum != 0) {
            blockNum++;
            divideNum *= 10;
            i++;
        }
        divideNum /= 10;
        int localanchor = 0;
        switch (anchor) {
            case 0:
                localanchor = 34;
                break;
            case 1:
                localanchor = 33;
                break;
            case 2:
                localanchor = 36;
                break;
        }
        int localtype = 0;
        switch (type) {
            case 0:
                localtype = 0;
                break;
            case 1:
                localtype = 1;
                break;
            case 2:
                localtype = 3;
                break;
            case 3:
                localtype = 2;
                break;
            case 4:
                localtype = 1;
                break;
        }
        NumberDrawer.drawNum(g, localtype, num, x, y, localanchor);
    }

    public static void drawNum(MFGraphics g, int num, int x, int y, int anchor, int type, int blockNum) {
        int i;
        if (numDrawer == null) {
            numDrawer = GlobalResource.statusAnimation.getDrawer(0, false, 0);
        }
        int divideNum = 1;
        for (i = 1; i < blockNum; i++) {
            divideNum *= 10;
        }
        int leftPosition = 0;
        switch (anchor) {
            case 0:
                leftPosition = x - ((NUM_SPACE[type] * (blockNum - 1)) >> 1);
                break;
            case 1:
                leftPosition = x;
                break;
            case 2:
                leftPosition = x - (NUM_SPACE[type] * (blockNum - 1));
                break;
        }
        for (i = 0; i < blockNum; i++) {
            int tmpNum = Math.abs(num / divideNum) % 10;
            divideNum /= 10;
            if (type == 3 && tmpNum == 0) {
                numDrawer.setActionId(26);
            } else {
                numDrawer.setActionId(NUM_ANI_ID[type] + tmpNum);
            }
            numDrawer.draw(g, (NUM_SPACE[type] * i) + leftPosition, y);
        }
    }

    public static void timeLogic() {
        if (!timeStopped) {
            if (overTime > timeCount) {
                timeCount += 60;
                if (timeCount > overTime) {
                    timeCount = overTime;
                }
                if (GlobalResource.timeIsLimit()) {
                    if (overTime - timeCount <= BREATHE_TIME_COUNT) {
                        if (timeCount / 1000 != preTimeCount) {
                            SoundSystem.getInstance().playSe(30);
                        }
                        preTimeCount = timeCount / 1000;
                    }
                    if (timeCount == overTime && player != null) {
                        if (stageModeState == 1) {
                            player.setDie(false);
                            StageManager.setStageTimeover();
                            StageManager.checkPointTime = 0;
                        } else if (lifeNum > 0) {
                            player.setDie(false);
                            StageManager.setStageTimeover();
                            StageManager.checkPointTime = 0;
                            minusLife();
                        } else {
                            player.setDie(false);
                            StageManager.setStageGameover();
                        }
                    }
                } else if (stageModeState == 1) {
                    if (overTime - timeCount <= BREATHE_TIME_COUNT) {
                        if (timeCount / 1000 != preTimeCount) {
                            SoundSystem.getInstance().playSe(30);
                        }
                        preTimeCount = timeCount / 1000;
                    }
                    if (timeCount == overTime && player != null) {
                        if (stageModeState == 1) {
                            player.setDie(false);
                            StageManager.setStageTimeover();
                            StageManager.checkPointTime = 0;
                        } else if (lifeNum > 0) {
                            player.setDie(false);
                            StageManager.setStageTimeover();
                            StageManager.checkPointTime = 0;
                            minusLife();
                        } else {
                            player.setDie(false);
                            StageManager.setStageGameover();
                        }
                    }
                }
            } else if (overTime < timeCount) {
                timeCount -= 60;
                if (timeCount < overTime) {
                    timeCount = overTime;
                }
                if (GlobalResource.timeIsLimit()) {
                    if (timeCount <= BREATHE_TIME_COUNT) {
                        if (timeCount / 1000 != preTimeCount) {
                            SoundSystem.getInstance().playSe(30);
                        }
                        preTimeCount = timeCount / 1000;
                    }
                    if (timeCount == overTime && player != null) {
                        if (stageModeState == 1) {
                            player.setDie(false);
                            StageManager.setStageTimeover();
                            StageManager.checkPointTime = 0;
                        } else if (lifeNum > 0) {
                            player.setDie(false);
                            StageManager.setStageTimeover();
                            StageManager.checkPointTime = 0;
                            minusLife();
                        } else {
                            player.setDie(false);
                            StageManager.setStageGameover();
                        }
                    }
                } else if (stageModeState == 1) {
                    if (timeCount <= BREATHE_TIME_COUNT) {
                        if (timeCount / 1000 != preTimeCount) {
                            SoundSystem.getInstance().playSe(30);
                        }
                        preTimeCount = timeCount / 1000;
                    }
                    if (timeCount == overTime && player != null) {
                        if (stageModeState == 1) {
                            player.setDie(false);
                            StageManager.setStageTimeover();
                            StageManager.checkPointTime = 0;
                        } else if (lifeNum > 0) {
                            player.setDie(false);
                            StageManager.setStageTimeover();
                            StageManager.checkPointTime = 0;
                            minusLife();
                        } else {
                            player.setDie(false);
                            StageManager.setStageGameover();
                        }
                    }
                }
            }
        }
    }

    public static void setTimeCount(int min, int sec, int msec) {
        timeCount = (((min * 60) * 1000) + (sec * 1000)) + msec;
        lastTimeCount = timeCount;
    }

    public static void setTimeCount(int count) {
        timeCount = count;
        lastTimeCount = timeCount;
    }

    public static void setOverCount(int min, int sec, int msec) {
        overTime = (((min * 60) * 1000) + (sec * 1000)) + msec;
    }

    public static void setOverCount(int count) {
        overTime = count;
    }

    public static int getTimeCount() {
        return timeCount;
    }

    public static void timeDraw(MFGraphics g, int x, int y) {
        int min = timeCount / 60000;
        int sec = (timeCount % 60000) / 1000;
        int msec = ((timeCount % 60000) % 1000) / 10;
        int numType = 0;
        if ((GlobalResource.timeIsLimit() || stageModeState == 1) && (((overTime > timeCount && timeCount > 540000) || (overTime < timeCount && timeCount < 60000)) && (timeCount / SSDef.PLAYER_MOVE_HEIGHT) % 2 == 0)) {
            numType = 3;
        }
        if (msec < 10) {
            drawNum(g, 0, x - NUM_SPACE[numType], y, 2, numType);
        }
        drawNum(g, msec, x, y, 2, numType);
        NumberDrawer.drawColon(g, numType == 3 ? 2 : 0, (x - (NUM_SPACE[numType] * 2)) - (NUM_SPACE[numType] >> 1), y, 34);
        if (sec < 10) {
            drawNum(g, 0, x - (NUM_SPACE[numType] * 4), y, 2, numType);
        }
        drawNum(g, sec, x - (NUM_SPACE[numType] * 3), y, 2, numType);
        NumberDrawer.drawColon(g, numType == 3 ? 2 : 0, (x - (NUM_SPACE[numType] * 5)) - (NUM_SPACE[numType] >> 1), y, 34);
        drawNum(g, min, x - (NUM_SPACE[numType] * 6), y, 2, numType);
    }

    public static void drawRecordTime(MFGraphics g, int timeCount, int x, int y, int numType, int anchor) {
        int min = timeCount / 60000;
        int sec = (timeCount % 60000) / 1000;
        timeCount = ((timeCount % 60000) % 1000) / 10;
        switch (anchor) {
            case 0:
                x += (NUM_SPACE[numType] * 7) >> 1;
                break;
            case 1:
                x += NUM_SPACE[numType] * 7;
                break;
        }
        if (timeCount < 10) {
            drawNum(g, 0, x - NUM_SPACE[numType], y, 2, numType);
        }
        drawNum(g, timeCount, x, y, 2, numType);
        NumberDrawer.drawColon(g, 3, (x - (NUM_SPACE[numType] * 2)) - (NUM_SPACE[numType] >> 1), y, 34);
        if (sec < 10) {
            drawNum(g, 0, x - (NUM_SPACE[numType] * 4), y, 2, numType);
        }
        drawNum(g, sec, x - (NUM_SPACE[numType] * 3), y, 2, numType);
        NumberDrawer.drawColon(g, 3, (x - (NUM_SPACE[numType] * 5)) - (NUM_SPACE[numType] >> 1), y, 34);
        drawNum(g, min, x - (NUM_SPACE[numType] * 6), y, 2, numType);
    }

    public static void drawRecordTimeTotalYellow(MFGraphics g, int timeCount, int x, int y, int numType, int anchor) {
        int min = timeCount / 60000;
        int sec = (timeCount % 60000) / 1000;
        timeCount = ((timeCount % 60000) % 1000) / 10;
        switch (anchor) {
            case 0:
                x += (NUM_SPACE[numType] * 7) >> 1;
                break;
            case 1:
                x += NUM_SPACE[numType] * 7;
                break;
        }
        if (timeCount < 10) {
            drawNum(g, 0, x - NUM_SPACE[numType], y, 2, numType);
        }
        drawNum(g, timeCount, x, y, 2, numType);
        NumberDrawer.drawColon(g, 0, (x - (NUM_SPACE[numType] * 2)) - (NUM_SPACE[numType] >> 1), y, 34);
        if (sec < 10) {
            drawNum(g, 0, x - (NUM_SPACE[numType] * 4), y, 2, numType);
        }
        drawNum(g, sec, x - (NUM_SPACE[numType] * 3), y, 2, numType);
        NumberDrawer.drawColon(g, 0, (x - (NUM_SPACE[numType] * 5)) - (NUM_SPACE[numType] >> 1), y, 34);
        drawNum(g, min, x - (NUM_SPACE[numType] * 6), y, 2, numType);
    }

    public static void drawRecordTimeLeft(MFGraphics g, int timeCount, int x, int y) {
        drawRecordTimeTotalYellow(g, timeCount, x, y, 0, 1);
        MyAPI.setBmfColor(0);
    }

    private static void drawStaticAni(MFGraphics g, int aniId, int x, int y) {
        numDrawer.setActionId(aniId);
        numDrawer.draw(g, x, y);
    }

    private static void drawStagePassInfoScroll(MFGraphics g, int y, int speed, int space) {
        State.drawBar(g, 2, y);
        itemOffsetX -= speed;
        itemOffsetX %= space;
        int x1 = itemOffsetX - 294;
        while (x1 < SCREEN_WIDTH * 2) {
            GameState.stageInfoAniDrawer.draw(g, getCharacterID() + 29, x1, (y - 10) + 2, false, 0);
            GameState.stageInfoAniDrawer.draw(g, 33, x1, (y - 10) + 2, false, 0);
            MFGraphics mFGraphics = g;
            GameState.stageInfoAniDrawer.draw(mFGraphics, passStageActionID, x1, (y - 10) + 2, false, 0);
            x1 += space;
        }
    }

    private static void drawStagePassInfoScroll(MFGraphics g, int offset_x, int y, int speed, int space) {
        if (isbarOut) {
            State.drawBar(g, 2, offset_x, y);
            State.drawBar(g, 2, SCREEN_WIDTH + offset_x, y);
            State.drawBar(g, 2, (SCREEN_WIDTH * 2) + offset_x, y);
        } else {
            State.drawBar(g, 2, y);
        }
        if (offset_x == 0) {
            itemOffsetX -= speed;
            itemOffsetX %= space;
        }
        int x1 = itemOffsetX - 294;
        while (x1 < SCREEN_WIDTH * 2) {
            MFGraphics mFGraphics = g;
            GameState.stageInfoAniDrawer.draw(mFGraphics, getCharacterID() + 29, x1 + offset_x, (y - 10) + 2, false, 0);
            mFGraphics = g;
            GameState.stageInfoAniDrawer.draw(mFGraphics, 33, x1 + offset_x, (y - 10) + 2, false, 0);
            mFGraphics = g;
            GameState.stageInfoAniDrawer.draw(mFGraphics, passStageActionID, x1 + offset_x, (y - 10) + 2, false, 0);
            x1 += space;
        }
    }

    public static void initMovingBar() {
        offsetx = SCREEN_WIDTH;
        offsety = (SCREEN_HEIGHT >> 1) + 48;
        if (StageManager.getStageID() >= 12) {
            passStageActionID = (StageManager.getStageID() - 12) + 36;
        } else if (StageManager.getStageID() % 2 == 0) {
            passStageActionID = 34;
        } else if (StageManager.getStageID() % 2 == 1) {
            passStageActionID = 35;
        }
    }

    private static void drawMovingbar(MFGraphics g, int space) {
        State.drawBar(g, 2, offsetx - 80, offsety);
        State.drawBar(g, 2, (offsetx - 80) + SCREEN_WIDTH, offsety);
        int drawNum = (((SCREEN_WIDTH + space) - 1) / space) + 2;
        for (int i = 0; i < drawNum; i++) {
            int x2 = offsetx + (i * space);
            GameState.stageInfoAniDrawer.draw(g, getCharacterID() + 29, x2, (offsety - 10) + 2, false, 0);
            GameState.stageInfoAniDrawer.draw(g, 33, x2, (offsety - 10) + 2, false, 0);
            GameState.stageInfoAniDrawer.draw(g, passStageActionID, x2, (offsety - 10) + 2, false, 0);
        }
    }

    public static void stagePassLogic() {
        switch (stageModeState) {
        }
    }

    private static boolean isRaceModeNewRecord() {
        return timeCount < StageManager.getTimeModeScore(characterID);
    }

    public static boolean isHadRaceRecord() {
        return StageManager.getTimeModeScore(characterID) < SonicDef.OVER_TIME;
    }

    public static boolean movingBar() {
        if (offsetx <= 0) {
            offsetx = 0;
        } else {
            offsetx -= movespeedx;
            if (offsetx == SCREEN_WIDTH - movespeedx) {
                if (stageModeState == 1) {
                    if (isRaceModeNewRecord()) {
                        SoundSystem.getInstance().playBgm(41, false);
                    } else {
                        SoundSystem.getInstance().playBgm(42, false);
                    }
                } else if (StageManager.getStageID() == 12) {
                    SoundSystem.getInstance().playBgm(28, false);
                } else if (StageManager.getStageID() == 13) {
                    SoundSystem.getInstance().playBgm(29, false);
                } else {
                    if (StageManager.getStageID() % 2 == 0) {
                        SoundSystem.getInstance().playBgm(26, false);
                    }
                    if (StageManager.getStageID() % 2 == 1) {
                        SoundSystem.getInstance().playBgm(27, false);
                    }
                }
            }
        }
        if (offsetx != 0) {
            return false;
        }
        if (offsety <= (SCREEN_HEIGHT >> 1) - 36) {
            offsety = (SCREEN_HEIGHT >> 1) - 36;
            return true;
        }
        offsety -= movespeedy;
        return false;
    }

    public static void clipMoveInit(int startx, int starty, int startw, int endw, int height) {
        clipx = startx;
        clipy = starty;
        clipstartw = startw;
        clipendw = endw;
        cliph = height;
    }

    public static boolean clipMoveLogic() {
        if (clipstartw < clipendw) {
            clipstartw += clipspeed;
            return false;
        }
        clipstartw = clipendw;
        return true;
    }

    public static void clipMoveShadow(MFGraphics g) {
        MyAPI.setClip(g, clipx, 0, clipstartw, SCREEN_HEIGHT);
    }

    public static void calculateScore() {
        if (StageManager.getStageID() == 10) {
            System.out.println("timeCount=" + timeCount);
            if (timeCount > 192000) {
                score1 = 1000;
            } else if (timeCount > 192000 || timeCount <= 132000) {
                score1 = 0;
            } else {
                score1 = BANKING_MIN_SPEED;
            }
            score2 = ringNum * 100;
            return;
        }
        if (timeCount < 50000) {
            score1 = 50000;
        } else if (timeCount >= 50000 && timeCount < 60000) {
            score1 = 10000;
        } else if (timeCount >= 60000 && timeCount < 90000) {
            score1 = 5000;
        } else if (timeCount >= 90000 && timeCount < 120000) {
            score1 = 4000;
        } else if (timeCount >= 120000 && timeCount < 180000) {
            score1 = 3000;
        } else if (timeCount >= 180000 && timeCount < 240000) {
            score1 = 2000;
        } else if (timeCount >= 240000 && timeCount < 300000) {
            score1 = 1000;
        } else if (timeCount < 300000 || timeCount >= 360000) {
            score1 = 0;
        } else {
            score1 = BANKING_MIN_SPEED;
        }
        score2 = ringNum * 100;
    }

    public static void stagePassDraw(MFGraphics g) {
        if (!StageManager.isOnlyStagePass) {
            switch (stageModeState) {
                case 0:
                    if (movingBar()) {
                        drawStagePassInfoScroll(g, stagePassResultOutOffsetX, (SCREEN_HEIGHT >> 1) - 36, 8, 256);
                        if (!clipMoveLogic()) {
                            clipMoveShadow(g);
                            GameState.guiAniDrawer.draw(g, 6, (SCREEN_WIDTH >> 1) - 70, (SCREEN_HEIGHT >> 1) - 6, false, 0);
                            GameState.guiAniDrawer.draw(g, 7, (SCREEN_WIDTH >> 1) - 70, ((SCREEN_HEIGHT >> 1) + MENU_SPACE) - 6, false, 0);
                            drawNum(g, score1, (SCREEN_WIDTH >> 1) + NUM_DISTANCE, SCREEN_HEIGHT >> 1, 2, 0);
                            drawNum(g, score2, (SCREEN_WIDTH >> 1) + NUM_DISTANCE, (SCREEN_HEIGHT >> 1) + MENU_SPACE, 2, 0);
                            MyAPI.setClip(g, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
                            totalPlusscore = (score1 + score2) + scoreNum;
                        }
                    } else {
                        drawMovingbar(g, STAGE_PASS_STR_SPACE);
                        stagePassResultOutOffsetX = 0;
                        isStartStageEndFlag = false;
                        stageEndFrameCnt = 0;
                        isOnlyBarOut = false;
                    }
                    if (clipMoveLogic()) {
                        GameState.guiAniDrawer.draw(g, 6, stagePassResultOutOffsetX + ((SCREEN_WIDTH >> 1) - 70), (SCREEN_HEIGHT >> 1) - 6, false, 0);
                        GameState.guiAniDrawer.draw(g, 7, stagePassResultOutOffsetX + ((SCREEN_WIDTH >> 1) - 70), ((SCREEN_HEIGHT >> 1) + MENU_SPACE) - 6, false, 0);
                        if (stageModeState == 1) {
                            raceScoreNum = MyAPI.calNextPosition((double) raceScoreNum, (double) totalPlusscore, 1, 5);
                        } else {
                            scoreNum = MyAPI.calNextPosition((double) scoreNum, (double) totalPlusscore, 1, 5);
                        }
                        score1 = MyAPI.calNextPosition((double) score1, 0.0d, 1, 5);
                        score2 = MyAPI.calNextPosition((double) score2, 0.0d, 1, 5);
                        drawNum(g, score1, ((SCREEN_WIDTH >> 1) + NUM_DISTANCE) + stagePassResultOutOffsetX, SCREEN_HEIGHT >> 1, 2, 0);
                        drawNum(g, score2, ((SCREEN_WIDTH >> 1) + NUM_DISTANCE) + stagePassResultOutOffsetX, (SCREEN_HEIGHT >> 1) + MENU_SPACE, 2, 0);
                        if (scoreNum == totalPlusscore) {
                            IsStarttoCnt = true;
                            if (StageManager.isOnlyScoreCal) {
                                isOnlyBarOut = true;
                            } else {
                                isStartStageEndFlag = true;
                            }
                        } else {
                            SoundSystem.getInstance().playSe(31);
                        }
                    }
                    if (isStartStageEndFlag) {
                        stageEndFrameCnt++;
                        if (stageEndFrameCnt == 2) {
                            SoundSystem.getInstance().playSe(32);
                        }
                    }
                    if (isOnlyBarOut) {
                        onlyBarOutCnt++;
                        if (onlyBarOutCnt == 2) {
                            SoundSystem.getInstance().playSe(32);
                        }
                        if (onlyBarOutCnt > onlyBarOutCntMax) {
                            stagePassResultOutOffsetX -= 96;
                        }
                        if (stagePassResultOutOffsetX < -1000) {
                            StageManager.isScoreBarOutOfScreen = true;
                            return;
                        }
                        return;
                    }
                    return;
                case 1:
                    if (movingBar()) {
                        drawStagePassInfoScroll(g, (SCREEN_HEIGHT >> 1) - 36, 8, 256);
                        if (clipMoveLogic()) {
                            IsStarttoCnt = true;
                        }
                        clipMoveShadow(g);
                        GameState.guiAniDrawer.draw(g, 8, (SCREEN_WIDTH >> 1) - 80, SCREEN_HEIGHT >> 1, false, 0);
                        drawRecordTime(g, timeCount, (SCREEN_WIDTH >> 1) + 72, (SCREEN_HEIGHT >> 1) + 7, 2, 2);
                        MyAPI.setClip(g, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
                        if (isRaceModeNewRecord() && IsStarttoCnt && !StageManager.isSaveTimeModeScore) {
                            IsDisplayRaceModeNewRecord = true;
                        }
                        if (IsDisplayRaceModeNewRecord) {
                            GameState.guiAniDrawer.draw(g, 9, SCREEN_WIDTH >> 1, (SCREEN_HEIGHT >> 1) + 33, false, 0);
                        }
                        //if (StageManager.isSaveTimeModeScore == null && IsStarttoCnt) {
                        if (StageManager.isSaveTimeModeScore == false && IsStarttoCnt) {
                            StageManager.setTimeModeScore(characterID, timeCount);
                            StageManager.isSaveTimeModeScore = true;
                            return;
                        }
                        return;
                    }
                    drawMovingbar(g, STAGE_PASS_STR_SPACE);
                    return;
                default:
                    return;
            }
        }
    }

    public static void gamepauseInit() {
        cursor = 0;
        cursorIndex = 0;
        Key.touchkeygameboardClose();
    }

    public static void gamepauseDraw(MFGraphics g) {
        PAUSE_MENU_NORMAL_ITEM = PAUSE_MENU_NORMAL_NOSHOP;
        State.fillMenuRect(g, (SCREEN_WIDTH >> 1) + PAUSE_FRAME_OFFSET_X, (SCREEN_HEIGHT >> 1) + PAUSE_FRAME_OFFSET_Y, PAUSE_FRAME_WIDTH, PAUSE_FRAME_HEIGHT);
        State.drawMenuFontById(g, 80, SCREEN_WIDTH >> 1, (((SCREEN_HEIGHT >> 1) + PAUSE_FRAME_OFFSET_Y) + (MENU_SPACE >> 1)) + 10);
        if (stageModeState == 0) {
            currentPauseMenuItem = PAUSE_MENU_NORMAL_ITEM;
        } else {
            currentPauseMenuItem = PAUSE_MENU_RACE_ITEM;
        }
        if (currentPauseMenuItem.length <= 4) {
            cursorIndex = 0;
        } else if (cursorIndex > cursor) {
            cursorIndex = cursor;
        } else if ((cursorIndex + 4) - 1 < cursor) {
            cursorIndex = (cursor - 4) + 1;
        }
        State.drawMenuFontById(g, 119, SCREEN_WIDTH >> 1, (((((SCREEN_HEIGHT >> 1) + PAUSE_FRAME_OFFSET_Y) + 10) + (MENU_SPACE >> 1)) + MENU_SPACE) + (MENU_SPACE * (cursor - cursorIndex)));
        State.drawMenuFontById(g, StringIndex.STR_RIGHT_ARROW, ((SCREEN_WIDTH >> 1) - 56) - 0, (((((SCREEN_HEIGHT >> 1) + PAUSE_FRAME_OFFSET_Y) + 10) + (MENU_SPACE >> 1)) + MENU_SPACE) + (MENU_SPACE * (cursor - cursorIndex)));
        for (int i = cursorIndex; i < cursorIndex + 4; i++) {
            State.drawMenuFontById(g, currentPauseMenuItem[i], SCREEN_WIDTH >> 1, (((((SCREEN_HEIGHT >> 1) + PAUSE_FRAME_OFFSET_Y) + 10) + (MENU_SPACE >> 1)) + MENU_SPACE) + (MENU_SPACE * (i - cursorIndex)));
        }
        if (currentPauseMenuItem.length > 4) {
            if (cursorIndex == 0) {
                State.drawMenuFontById(g, 96, SCREEN_WIDTH >> 1, ((SCREEN_HEIGHT >> 1) - PAUSE_FRAME_OFFSET_Y) + (MENU_SPACE >> 1));
                GameState.IsSingleUp = false;
                GameState.IsSingleDown = true;
            } else if (cursorIndex == currentPauseMenuItem.length - 4) {
                State.drawMenuFontById(g, 95, SCREEN_WIDTH >> 1, ((SCREEN_HEIGHT >> 1) - PAUSE_FRAME_OFFSET_Y) + (MENU_SPACE >> 1));
                GameState.IsSingleUp = true;
                GameState.IsSingleDown = false;
            } else {
                State.drawMenuFontById(g, 95, (SCREEN_WIDTH >> 1) - 23, ((SCREEN_HEIGHT >> 1) - PAUSE_FRAME_OFFSET_Y) + (MENU_SPACE >> 1));
                State.drawMenuFontById(g, 96, (SCREEN_WIDTH >> 1) + 22, ((SCREEN_HEIGHT >> 1) - PAUSE_FRAME_OFFSET_Y) + (MENU_SPACE >> 1));
                GameState.IsSingleUp = false;
                GameState.IsSingleDown = false;
            }
        }
        State.drawSoftKey(g, true, true);
    }

    public void close() {
        Animation.closeAnimationDrawer(this.waterFallDrawer);
        this.waterFallDrawer = null;
        Animation.closeAnimationDrawer(this.waterFlushDrawer);
        this.waterFlushDrawer = null;
        Animation.closeAnimationDrawer(this.drawer);
        this.drawer = null;
        Animation.closeAnimationDrawer(this.effectDrawer);
        this.effectDrawer = null;
        Animation.closeAnimation(this.dustEffectAnimation);
        this.dustEffectAnimation = null;
        closeImpl();
    }

    public static void doWhileQuitGame() {
        bariaDrawer = null;
        gBariaDrawer = null;
        invincibleAnimation = null;
        invincibleDrawer = null;
    }

    public static boolean IsInvincibility() {
        if (invincibleCount > 0) {
            return true;
        }
        return false;
    }

    public static boolean IsUnderSheild() {
        if (shieldType == 2) {
            return true;
        }
        return false;
    }

    public static boolean IsSpeedUp() {
        if (speedCount > 0) {
            return true;
        }
        return false;
    }

    public void setAntiGravity() {
        boolean z;
        int i;
        if (this.isAntiGravity) {
            z = false;
        } else {
            z = true;
        }
        this.isAntiGravity = z;
        this.worldCal.actionState = (byte) 1;
        this.collisionState = (byte) 1;
        if (this.faceDirection) {
            z = false;
        } else {
            z = true;
        }
        this.faceDirection = z;
        int bodyCenterX = getNewPointX(this.posX, 0, (-this.collisionRect.getHeight()) >> 1, this.faceDegree);
        int bodyCenterY = getNewPointY(this.posY, 0, (-this.collisionRect.getHeight()) >> 1, this.faceDegree);
        if (this.isAntiGravity) {
            i = RollPlatformSpeedC.DEGREE_VELOCITY;
        } else {
            i = 0;
        }
        this.faceDegree = i;
        i = getNewPointX(bodyCenterX, 0, this.collisionRect.getHeight() >> 1, this.faceDegree);
        this.footPointX = i;
        this.posX = i;
        i = getNewPointY(bodyCenterY, 0, this.collisionRect.getHeight() >> 1, this.faceDegree);
        this.footPointY = i;
        this.posY = i;
    }

    public void setAntiGravity(boolean GraFlag) {
        this.orgGravity = this.isAntiGravity;
        this.isAntiGravity = GraFlag;
        if (this.orgGravity != this.isAntiGravity) {
            int i;
            this.worldCal.actionState = (byte) 1;
            this.collisionState = (byte) 1;
            this.faceDirection = !this.faceDirection;
            if (this.isAntiGravity) {
                i = RollPlatformSpeedC.DEGREE_VELOCITY;
            } else {
                i = 0;
            }
            this.faceDegree = i;
        }
    }

    public void doWhileTouchWorld(int direction, int degree) {
        if (this.worldCal.getActionState() == (byte) 1) {
            switch (direction) {
                case 0:
                    if (this.collisionState == (byte) 2 && this.movedSpeedY < 0) {
                        setDie(false);
                        break;
                    }
                case 1:
                    if (this.isAntiGravity) {
                        this.leftStopped = true;
                    } else {
                        this.rightStopped = true;
                    }
                    if (this.leftStopped && this.rightStopped) {
                        setDie(false);
                        return;
                    }
                case 3:
                    if (this.isAntiGravity) {
                        this.rightStopped = true;
                    } else {
                        this.leftStopped = true;
                    }
                    if (this.leftStopped && this.rightStopped) {
                        setDie(false);
                        return;
                    }
            }
        }
        if (this.worldCal.getActionState() == (byte) 0 || this.collisionState == (byte) 2) {
            switch (direction) {
                case 0:
                    if (this.collisionState == (byte) 2 && this.movedSpeedY < 0) {
                        setDie(false);
                        return;
                    }
                    return;
                case 1:
                    if (!this.speedLock) {
                        this.totalVelocity = 0;
                    }
                    if (this.isAntiGravity) {
                        this.leftStopped = true;
                    } else {
                        this.rightStopped = true;
                    }
                    if (this.leftStopped && this.rightStopped) {
                        setDie(false);
                        return;
                    } else if ((Key.repeat(Key.gRight) && !this.isAntiGravity) || (Key.repeat(Key.gLeft) && this.isAntiGravity)) {
                        if (this.animationID == 0 || this.animationID == 47 || this.animationID == 48 || this.animationID == 1 || this.animationID == 2 || this.animationID == 3) {
                            this.animationID = 8;
                            return;
                        }
                        return;
                    } else {
                        return;
                    }
                case 3:
                    if (!this.speedLock) {
                        this.totalVelocity = 0;
                    }
                    if (this.isAntiGravity) {
                        this.rightStopped = true;
                    } else {
                        this.leftStopped = true;
                    }
                    if (this.leftStopped && this.rightStopped) {
                        setDie(false);
                        return;
                    } else if ((Key.repeat(Key.gLeft) && !this.isAntiGravity) || (Key.repeat(Key.gRight) && this.isAntiGravity)) {
                        if (this.animationID == 0 || this.animationID == 47 || this.animationID == 48 || this.animationID == 1 || this.animationID == 2 || this.animationID == 3) {
                            this.animationID = 8;
                            return;
                        }
                        return;
                    } else {
                        return;
                    }
                default:
                    return;
            }
        }
    }

    public int getBodyDegree() {
        return this.worldCal.footDegree;
    }

    public int getBodyOffset() {
        return 768;
    }

    public int getFootOffset() {
        return 256;
    }

    public int getFootX() {
        return this.posX;
    }

    public int getFootY() {
        return this.posY;
    }

    public int getPressToGround() {
        return GRAVITY << 1;
    }

    public void didAfterEveryMove(int arg0, int arg1) {
        player.moveDistance.f13x = arg0;
        player.moveDistance.f14y = arg1;
        this.footPointX = this.posX;
        this.footPointY = this.posY;
        collisionCheckWithGameObject();
        this.posZ = this.currentLayer;
    }

    public void doBeforeCollisionCheck() {
    }

    public void doWhileCollision(ACObject arg0, ACCollision arg1, int arg2, int arg3, int arg4, int arg5, int arg6) {
    }

    public void doWhileLeaveGround() {
        calDivideVelocity();
        this.collisionState = (byte) 1;
        if (isTerminal && terminalState >= (byte) 4) {
            this.collisionState = (byte) 4;
        }
    }

    public void doWhileLand(int degree) {
        this.faceDegree = degree;
        land();
        if (this.footOnObject != null) {
            this.worldCal.stopMove();
            this.footOnObject = null;
        }
        this.collisionState = (byte) 0;
        this.isSidePushed = 4;
        System.out.println("~~velx:" + (this.velX >> 6));
    }

    public int getMinDegreeToLeaveGround() {
        return 45;
    }

    public void stopMove() {
        this.worldCal.stopMove();
    }

    public ACWorldCollisionCalculator getCal() {
        return this.worldCal;
    }

    public int getDegreeDiff(int degree1, int degree2) {
        int re = Math.abs(degree1 - degree2);
        if (re > RollPlatformSpeedC.DEGREE_VELOCITY) {
            re = MDPhone.SCREEN_WIDTH - re;
        }
        if (re > 90) {
            return RollPlatformSpeedC.DEGREE_VELOCITY - re;
        }
        return re;
    }

    protected void extraLogicJump() {
    }

    protected void extraLogicWalk() {
    }

    protected void extraLogicOnObject() {
    }

    protected void extraInputLogic() {
    }

    private void checkCliffAnimation() {
        int footLeftX = ACUtilities.getRelativePointX(this.posX, -256, 0, this.faceDegree);
        int footLeftY = ACUtilities.getRelativePointY(this.posY, -256, this.worldInstance.getTileHeight(), this.faceDegree);
        int footCenterX = ACUtilities.getRelativePointX(this.posX, 0, 0, this.faceDegree);
        int footCenterY = ACUtilities.getRelativePointY(this.posY, 0, this.worldInstance.getTileHeight(), this.faceDegree);
        int footRightX = ACUtilities.getRelativePointX(this.posX, 256, 0, this.faceDegree);
        int footRightY = ACUtilities.getRelativePointY(this.posY, 256, this.worldInstance.getTileHeight(), this.faceDegree);
        switch (this.collisionState) {
            case (byte) 0:
                if (this.worldInstance.getWorldY(footCenterX, footCenterY, this.currentLayer, this.worldCal.getDirectionByDegree(this.faceDegree)) != -1000) {
                    return;
                }
                if (this.worldInstance.getWorldY(footLeftX, footLeftY, this.currentLayer, this.worldCal.getDirectionByDegree(this.faceDegree)) != -1000) {
                    if (this.faceDirection) {
                        this.animationID = 47;
                        return;
                    } else {
                        this.animationID = 48;
                        return;
                    }
                } else if (this.worldInstance.getWorldY(footRightX, footRightY, this.currentLayer, this.worldCal.getDirectionByDegree(this.faceDegree)) == -1000) {
                    return;
                } else {
                    if (this.faceDirection) {
                        this.animationID = 48;
                        return;
                    } else {
                        this.animationID = 47;
                        return;
                    }
                }
            case (byte) 2:
                if (this.footOnObject == null) {
                    return;
                }
                if (footCenterX < this.footOnObject.collisionRect.x0) {
                    if (this.faceDirection) {
                        this.animationID = 48;
                        return;
                    } else {
                        this.animationID = 47;
                        return;
                    }
                } else if (footCenterX <= this.footOnObject.collisionRect.x1) {
                    return;
                } else {
                    if (this.faceDirection) {
                        this.animationID = 47;
                        return;
                    } else {
                        this.animationID = 48;
                        return;
                    }
                }
            default:
                return;
        }
    }

    public void setCliffAnimation() {
        if (this.faceDirection) {
            this.animationID = 48;
        } else {
            this.animationID = 47;
        }
        this.drawer.restart();
    }

    protected boolean spinLogic() {
        if (!(Key.repeat(Key.gLeft) || Key.repeat(Key.gRight) || isTerminal || this.animationID == -1 || this.animationID == 47 || this.animationID == 48)) {
            if (Key.repeat(Key.gDown)) {
                if (Math.abs(getVelX()) > 64 || getDegreeDiff(this.faceDegree, this.degreeStable) > 45) {
                    if (!(this.animationID == 4 || characterID == 3 || this.isCrashFallingSand)) {
                        soundInstance.playSe(4);
                    }
                    this.animationID = 4;
                } else {
                    if (this.animationID != 5) {
                        this.animationID = 46;
                    }
                    if (this.collisionState == (byte) 3) {
                        if (this instanceof PlayerAmy) {
                            this.dashRolling = true;
                            this.spinDownWaitCount = 0;
                            if (characterID != 3) {
                                soundInstance.playSe(4);
                            }
                        }
                    } else if (Key.press(16777216 | Key.gUp)) {
                        this.dashRolling = true;
                        this.spinDownWaitCount = 0;
                        if (characterID != 3) {
                            soundInstance.playSe(4);
                        }
                    }
                    if (!this.dashRolling) {
                        this.focusMovingState = 2;
                    }
                }
            } else if (this.animationID == 5) {
                this.animationID = 46;
            }
        }
        if (this.animationID == 0 && getDegreeDiff(this.faceDegree, this.degreeStable) <= 45) {
            if (Key.press(2097152)) {
                this.dashRolling = true;
                if (characterID != 3) {
                    soundInstance.playSe(4);
                }
                this.spinCount = 12;
                this.spinKeyCount = 20;
            } else if (Key.press(Key.B_7)) {
                this.faceDirection = false;
                this.dashRolling = true;
                this.spinKeyCount = 20;
                if (characterID != 3) {
                    soundInstance.playSe(4);
                }
                this.spinCount = 12;
            } else if (Key.press(Key.B_9)) {
                this.faceDirection = true;
                this.dashRolling = true;
                this.spinKeyCount = 20;
                if (characterID != 3) {
                    soundInstance.playSe(4);
                }
                this.spinCount = 12;
            }
        }
        return this.dashRolling;
    }

    protected boolean spinLogic2() {
        if (!(Key.repeat(Key.gLeft) || Key.repeat(Key.gRight) || isTerminal || this.animationID == -1 || this.animationID == 47 || this.animationID == 48)) {
            if (Key.repeat(Key.gDown)) {
                if (getDegreeDiff(this.faceDegree, this.degreeStable) <= 45 && this.animationID != 5) {
                    this.animationID = 46;
                }
            } else if (this.animationID == 5) {
                this.animationID = 46;
            }
        }
        return this.dashRolling;
    }

    public void dashRollingLogicCheck() {
        if (this.dashRolling) {
            dashRollingLogic();
        } else if (this.effectID == 0 || this.effectID == 1) {
            this.effectID = -1;
        }
    }

    public int getCharacterAnimationID() {
        return this.myAnimationID;
    }

    public void setCharacterAnimationID(int aniID) {
        this.myAnimationID = aniID;
    }

    public int getGravity() {
        if (this.isInWater) {
            return (GRAVITY * 3) / 5;
        }
        return GRAVITY;
    }

    public boolean doBreatheBubble() {
        if (this.collisionState != (byte) 1) {
            return false;
        }
        resetBreatheCount();
        this.animationID = 49;
        if (characterID == 1) {
            ((PlayerTails) player).flyCount = 0;
        }
        this.velX = 0;
        this.velY = 0;
        return true;
    }

    public void resetBreatheCount() {
        this.breatheCount = 0;
        this.breatheNumCount = -1;
        this.preBreatheNumCount = -1;
    }

    public void checkBreatheReset() {
        if (getNewPointY(this.posY, 0, -this.collisionRect.getHeight(), this.faceDegree) + 256 < (StageManager.getWaterLevel() << 6)) {
            resetBreatheCount();
        }
    }

    public void waitingChk() {
        if (Key.repeat(((((Key.gSelect | Key.gLeft) | Key.gRight) | Key.gDown) | Key.gUp) | 16777216) || !(this.animationID == 0 || this.animationID == 50 || this.animationID == 51)) {
            this.waitingCount = 0;
            this.waitingLevel = 0;
            this.isResetWaitAni = true;
            return;
        }
        this.waitingCount++;
        if (this.waitingCount > 96) {
            if (this.waitingLevel == 0) {
                this.animationID = 50;
            }
            if ((this.drawer.checkEnd() && this.waitingLevel == 0) || this.waitingLevel == 1) {
                this.waitingLevel = 1;
                this.animationID = 51;
            }
        }
    }

    public void drawDrawerByDegree(MFGraphics g, AnimationDrawer drawer, int aniID, int x, int y, boolean loop, int degree, boolean mirror) {
        g.saveCanvas();
        g.translateCanvas(x, y);
        g.rotateCanvas((float) degree);
        drawer.draw(g, aniID, 0, 0, loop, !mirror ? 0 : 2);
        g.restoreCanvas();
    }

    public void loseRing(int rNum) {
        RingObject.hurtRingExplosion(rNum, getBodyPositionX(), getBodyPositionY(), this.currentLayer, this.isAntiGravity);
    }

    public static int getRingNum() {
        return ringNum;
    }

    public static void setRingNum(int rNum) {
        ringNum = rNum;
    }

    public void beSpSpring(int springPower, int direction) {
        if (this.collisionState == (byte) 0) {
            calDivideVelocity();
        }
        this.velY = -springPower;
        this.worldCal.stopMoveY();
        if (this.collisionState == (byte) 0) {
            calTotalVelocity();
        }
        int i = this.degreeStable;
        this.faceDegree = i;
        this.degreeForDraw = i;
        this.animationID = 9;
        this.collisionState = (byte) 1;
        this.worldCal.actionState = (byte) 1;
        this.collisionChkBreak = true;
        this.drawer.restart();
        MapManager.setFocusObj(null);
        setMeetingBoss(false);
        this.animationID = 14;
        this.enteringSP = true;
        soundInstance.playSe(37);
    }

    public void setStagePassRunOutofScreen() {
        MapManager.setFocusObj(null);
        this.animationID = 3;
    }

    public boolean stagePassRunOutofScreenLogic() {
        if ((StageManager.isOnlyScoreCal || this.footPointX + 512 <= (((camera.f13x + SCREEN_WIDTH) + 800) << 6)) && (!isStartStageEndFlag || stageEndFrameCnt <= 80)) {
            return false;
        }
        stagePassResultOutOffsetX -= 96;
        if (stagePassResultOutOffsetX < -1000) {
            return true;
        }
        return false;
    }

    public boolean needRetPower() {
        return !(Key.repeat(Key.gLeft | Key.gRight) || isTerminalRunRight() || this.isCelebrate) || this.animationID == 4 || this.slipFlag;
    }

    public int getRetPower() {
        if (this.animationID != 4) {
            return this.movePower;
        }
        return this.movePower >> 1;
    }

    public int getSlopeGravity() {
        if (this.animationID != 4) {
            return FAKE_GRAVITY_ON_WALK;
        }
        return FAKE_GRAVITY_ON_BALL;
    }

    public boolean noRotateDraw() {
        return this.animationID == 0 || this.animationID == 5 || this.animationID == 46 || this.animationID == 50 || this.animationID == 51 || this.animationID == 6 || this.animationID == 7 || this.animationID == 30 || this.animationID == 8;
    }

    public boolean canDoJump() {
        return this.animationID != 5;
    }

    private void aspirating() {
        int i = this.breatheCount % 3;
    }

    public static void setFadeColor(int color) {
        for (int i = 0; i < fadeRGB.length; i++) {
            fadeRGB[i] = color;
        }
    }

    public static void fadeInit(int from, int to) {
        fadeFromValue = from;
        fadeToValue = to;
        fadeAlpha = fadeFromValue;
        preFadeAlpha = -1;
    }

    public static void drawFadeBase(MFGraphics g, int vel2) {
        fadeAlpha = MyAPI.calNextPosition((double) fadeAlpha, (double) fadeToValue, 1, vel2, 3.0d);
        if (fadeAlpha != 0) {
            int w;
            int h;
            if (preFadeAlpha != fadeAlpha) {
                for (w = 0; w < 40; w++) {
                    for (h = 0; h < 40; h++) {
                        fadeRGB[(h * 40) + w] = ((fadeAlpha << 24) & -16777216) | (fadeRGB[(h * 40) + w] & MapManager.END_COLOR);
                    }
                }
                preFadeAlpha = fadeAlpha;
            }
            for (w = 0; w < MyAPI.zoomOut(SCREEN_WIDTH); w += 40) {
                for (h = 0; h < MyAPI.zoomOut(SCREEN_HEIGHT); h += 40) {
                    g.drawRGB(fadeRGB, 0, 40, w, h, 40, 40, true);
                }
            }
        }
    }

    public static boolean fadeChangeOver() {
        return fadeAlpha == fadeToValue;
    }

    private static void playerLifeUpBGM() {
        SoundSystem.getInstance().stopBgm(false);
        if (invincibleCount > 0) {
            SoundSystem.getInstance().playBgmSequence(43, 44);
        } else {
            SoundSystem.getInstance().playBgmSequence(43, StageManager.getBgmId());
        }
    }

    public boolean isBodyCenterOutOfWater() {
        return getNewPointY(this.posY, 0, -this.collisionRect.getHeight(), this.faceDegree) < (StageManager.getWaterLevel() << 6);
    }

    public void dripDownUnderWater() {
    }

    public void resetPlayerDegree() {
        int i = this.degreeStable;
        this.faceDegree = i;
        this.degreeForDraw = i;
    }

    public boolean isOnSlip0() {
        return false;
    }

    public void setSlip0() {
    }

    public void lookUpCheck() {
        if (Key.repeat(Key.gUp | 33554432)) {
            if (this.animationID == 38 && this.drawer.checkEnd()) {
                this.animationID = 39;
            }
            if (!(this.animationID == 38 || this.animationID == 39 || this.animationID != 0)) {
                this.animationID = 38;
            }
            if (this.animationID == 39) {
                this.focusMovingState = 1;
                return;
            }
            return;
        }
        if (this.animationID == 40 && this.drawer.checkEnd()) {
            this.animationID = 0;
        }
        if (this.animationID == 38 || this.animationID == 39) {
            this.animationID = 40;
        }
    }
}
