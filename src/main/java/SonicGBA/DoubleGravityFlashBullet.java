package SonicGBA;

import Lib.Animation;
import com.sega.mobile.define.MDPhone;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: BulletObject */
class DoubleGravityFlashBullet extends BulletObject {
    private int COLLISION_HEIGHT = 768;
    private int COLLISION_WIDTH = 768;

    protected DoubleGravityFlashBullet(int x, int y, int velX, int velY, int type) {
        super(x, y, velX, velY, true);
        switch (type) {
            case 0:
                this.COLLISION_WIDTH = 768;
                this.COLLISION_HEIGHT = 768;
                doublegravityflashbulletAnimation = null;
                doublegravityflashbulletAnimation = new Animation("/animation/dgfa_bullet");
                break;
            case 1:
                this.COLLISION_WIDTH = MDPhone.SCREEN_HEIGHT;
                this.COLLISION_HEIGHT = MDPhone.SCREEN_HEIGHT;
                doublegravityflashbulletAnimation = null;
                doublegravityflashbulletAnimation = new Animation("/animation/dgfn_bullet");
                break;
        }
        this.drawer = doublegravityflashbulletAnimation.getDrawer(0, true, 0);
    }

    public void bulletLogic() {
        int preX = this.posX;
        int preY = this.posY;
        this.posX += this.velX;
        this.velY += GRAVITY;
        this.posY += this.velY;
        checkWithPlayer(preX, preY, this.posX, this.posY);
    }

    public void draw(MFGraphics g) {
        drawInMap(g, this.drawer);
        this.collisionRect.draw(g, camera);
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - (this.COLLISION_WIDTH >> 1), y - (this.COLLISION_HEIGHT >> 1), this.COLLISION_WIDTH, this.COLLISION_HEIGHT);
    }

    public boolean chkDestroy() {
        return !isInCamera() || isFarAwayCamera();
    }
}
