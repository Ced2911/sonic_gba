package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import Lib.MyRandom;
import Lib.SoundSystem;
import com.sega.mobile.framework.device.MFGraphics;
import java.util.Vector;

public class RocketSeparateEffect implements SonicDef {
    private static final int ANICOUNT_TIME = 5;
    private static final String PARTS_ANIMATION_PATH = "/animation/parts";
    private static final int PARTS_INFO_TYPE = 0;
    private static final int PARTS_INFO_VX = 3;
    private static final int PARTS_INFO_VY = 4;
    private static final int PARTS_INFO_X = 1;
    private static final int PARTS_INFO_Y = 2;
    private static final int PARTS_NUM = 6;
    private static final int SHAKING_COUNT = 80;
    private static final int STATE_INIT = 1;
    private static final int STATE_MAP_BROKE_1 = 6;
    private static final int STATE_MAP_BROKE_2 = 7;
    private static final int STATE_MAP_BROKE_3 = 8;
    private static final int STATE_MAP_BROKE_4 = 9;
    private static final int STATE_NONE = 0;
    private static final int STATE_OVER = 3;
    private static final int STATE_SHAKING = 2;
    private static final int STATE_WAITING = 4;
    private static final int STATE_WAITING_CAMERA = 5;
    private static RocketSeparateEffect instance;
    private int brokeOffset;
    private int brokeVel;
    private int count;
    private int effectID;
    private int markY;
    private AnimationDrawer partsDrawer = new Animation(PARTS_ANIMATION_PATH).getDrawer();
    private Vector partsInfoVec = new Vector();
    private int state = 0;

    public static void clearInstance() {
        instance = null;
    }

    public static RocketSeparateEffect getInstance() {
        if (instance == null) {
            instance = new RocketSeparateEffect();
        }
        return instance;
    }

    private RocketSeparateEffect() {
    }

    public void init(int effectID) {
        this.effectID = effectID;
        switch (effectID) {
            case 0:
                MapManager.setCameraLeftLimit(1056);
                break;
            case 1:
                this.markY = 696;
                break;
            case 2:
                this.markY = 408;
                break;
        }
        GameObject.player.setMeetingBoss(false);
        this.state = 1;
    }

    public void functionSecond(int effectID) {
        this.effectID = effectID;
        if (PlayerObject.stageModeState == 0) {
            PlayerObject.setTimeCount(5, 0, 0);
            PlayerObject.setOverCount(0, 0, 0);
        }
        switch (effectID) {
            case 2:
                BackGroundManager.next();
                return;
            default:
                return;
        }
    }

    public void logic() {
        if (!GameObject.IsGamePause) {
            this.count++;
            switch (this.state) {
                case 1:
                    this.count = 0;
                    this.state = 4;
                    if (this.effectID != 0) {
                        PlayerObject.timeStopped = true;
                        return;
                    }
                    return;
                case 2:
                    MapManager.setShake(20, MyRandom.nextInt(5, 10));
                    if (this.count == 80) {
                        this.state = 3;
                    }
                    if (this.count % 4 == 0) {
                        SoundSystem.getInstance().playSe(35);
                        return;
                    }
                    return;
                case 3:
                    PlayerObject.timeStopped = false;
                    GameObject.player.setMeetingBoss(true);
                    StageManager.saveCheckPointCamera(MapManager.proposeUpCameraLimit, MapManager.proposeDownCameraLimit, MapManager.proposeLeftCameraLimit, MapManager.proposeRightCameraLimit);
                    MapManager.actualUpCameraLimit = MapManager.proposeUpCameraLimit;
                    this.state = 0;
                    if (PlayerObject.stageModeState == 0) {
                        PlayerObject.setTimeCount(5, 0, 0);
                        PlayerObject.setOverCount(0, 0, 0);
                        return;
                    }
                    return;
                case 4:
                    if (GameObject.player.isOnGound() && this.count > 10) {
                        switch (this.effectID) {
                            case 0:
                                this.state = 2;
                                break;
                            case 1:
                            case 2:
                                MapManager.setCameraUpLimit((this.markY - 2) * 8);
                                this.state = 5;
                                break;
                        }
                        this.count = 0;
                        return;
                    }
                    return;
                case 5:
                    if (MapManager.proposeUpCameraLimit == MapManager.actualUpCameraLimit) {
                        this.state = 9;
                        this.brokeOffset = 4;
                        this.brokeVel = 2;
                        createParts(2);
                        MapManager.setMapBrokeParam(this.markY, this.brokeOffset);
                        this.count = 0;
                        if (this.effectID == 2) {
                            BackGroundManager.next();
                            return;
                        }
                        return;
                    }
                    return;
                case 6:
                    if (this.count % 4 == 0) {
                        SoundSystem.getInstance().playSe(35);
                    }
                    this.brokeOffset += this.brokeVel;
                    if (this.brokeOffset > this.brokeVel * 10) {
                        this.state = 7;
                    }
                    MapManager.setMapBrokeParam(this.markY, this.brokeOffset);
                    return;
                case 7:
                    if (this.count % 4 == 0) {
                        SoundSystem.getInstance().playSe(35);
                    }
                    this.brokeVel++;
                    this.brokeOffset += this.brokeVel;
                    createParts(2);
                    MapManager.setShakeX(MyRandom.nextInt(5, 10));
                    MapManager.setShake(20, MyRandom.nextInt(5, 10));
                    MapManager.setMapBrokeParam(this.markY, this.brokeOffset);
                    if (this.brokeOffset > SCREEN_HEIGHT) {
                        MapManager.releaseCameraUpLimit();
                        MapManager.setCameraDownLimit(this.markY * 8);
                        PlayerObject.isDeadLineEffect = true;
                        this.state = 8;
                        return;
                    }
                    return;
                case 8:
                    if (this.count % 4 == 0) {
                        SoundSystem.getInstance().playSe(35);
                    }
                    createParts(2);
                    MapManager.setShakeX(MyRandom.nextInt(5, 10));
                    MapManager.setShake(20, MyRandom.nextInt(5, 10));
                    if (MapManager.proposeDownCameraLimit == MapManager.actualDownCameraLimit) {
                        this.state = 3;
                        return;
                    }
                    return;
                case 9:
                    if (this.count == 2) {
                        SoundSystem.getInstance().playSequenceSe(45);
                    }
                    if (this.count > 8) {
                        this.state = 6;
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    public void draw(MFGraphics g) {
        partsDraw(g);
    }

    public void createParts(int partsNum) {
        for (int i = 0; i < partsNum; i++) {
            int[] info = new int[5];
            info[0] = MyRandom.nextInt(6);
            info[1] = MyRandom.nextInt(SCREEN_WIDTH);
            info[2] = MyRandom.nextInt(-20, -10);
            info[3] = MyRandom.nextInt(-5, 5);
            this.partsInfoVec.addElement(info);
        }
    }

    public void partsDraw(MFGraphics g) {
        int i = 0;
        while (i < this.partsInfoVec.size()) {
            int[] info = (int[]) this.partsInfoVec.elementAt(i);
            if (!GameObject.IsGamePause) {
                info[4] = info[4] + 2;
                info[1] = info[1] + info[3];
                info[2] = info[2] + info[4];
                if (info[2] > SCREEN_HEIGHT + 20) {
                    this.partsInfoVec.removeElementAt(i);
                    i--;
                    i++;
                }
            }
            this.partsDrawer.draw(g, info[0], info[1], info[2], false, 0);
            i++;
        }
    }

    public void close() {
        this.state = 0;
    }
}
