package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: EnemyObject */
class Boss2Spring extends EnemyObject {
    private static final int SPRING_DAMPING = 2;
    private static final int SPRING_FLYING = 0;
    private static final int SPRING_WAITING = 1;
    private static Animation springAni;
    private int COLLISION_HEIGHT = 2240;
    private int COLLISION_WIDTH = 2048;
    private boolean IsAttack = false;
    private boolean IsBossBroken;
    private boolean IsEnd;
    private boolean IsHurt;
    private AnimationDrawer springdrawer;
    private int state;
    private int velocity;

    public static void releaseAllResource() {
        Animation.closeAnimation(springAni);
        springAni = null;
    }

    protected Boss2Spring(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        this.posX -= this.iLeft * 8;
        this.posY -= this.iTop * 8;
        refreshCollisionRect(this.posX >> 6, this.posY >> 6);
        if (springAni == null) {
            springAni = new Animation("/animation/boss2_spring");
        }
        this.springdrawer = springAni.getDrawer(0, true, 0);
        this.IsEnd = false;
        this.IsBossBroken = false;
        this.IsAttack = false;
    }

    public void setAttackable(boolean state) {
        this.IsAttack = state;
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (!this.dead && object == player && direction != 1 && !this.IsHurt && this.IsAttack && !this.IsBossBroken) {
            player.beHurt();
        }
    }

    public void doWhileBeAttack(PlayerObject object, int direction, int animationID) {
    }

    public void logic() {
    }

    public void setSpringAni(int state, boolean isloop) {
        changeAniState(this.springdrawer, state, isloop);
        this.IsEnd = false;
    }

    public void changeAniState(AnimationDrawer AniDrawer, int state, boolean isloop) {
        if (this.velocity > 0) {
            AniDrawer.setActionId(state);
            AniDrawer.setTrans(2);
            AniDrawer.setLoop(isloop);
            return;
        }
        AniDrawer.setActionId(state);
        AniDrawer.setTrans(0);
        AniDrawer.setLoop(isloop);
    }

    public boolean getEndState() {
        return this.IsEnd;
    }

    public void setBossBrokenState(boolean isBossBroekn) {
        this.IsBossBroken = isBossBroekn;
    }

    public void logic(int posx, int posy, int state, int vel) {
        this.posX = posx;
        this.posY = posy;
        if (!this.dead) {
            this.COLLISION_HEIGHT = getSpringHeight();
            int preX = this.posX;
            int preY = this.posY;
            switch (state) {
                case 2:
                    if (this.springdrawer.checkEnd()) {
                        this.IsEnd = true;
                        break;
                    }
                    break;
            }
            checkWithPlayer(preX, preY, this.posX, this.posY);
        }
    }

    public void getIsHurt(boolean bosshurt) {
        this.IsHurt = bosshurt;
    }

    public void draw(MFGraphics g) {
        if (!this.dead) {
            drawInMap(g, this.springdrawer);
            drawCollisionRect(g);
        }
    }

    public int getSpringHeight() {
        if (springAni != null) {
            return this.springdrawer.getCurrentFrameHeight() << 6;
        }
        return 0;
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - (this.COLLISION_WIDTH >> 1), y - this.COLLISION_HEIGHT, this.COLLISION_WIDTH, this.COLLISION_HEIGHT);
    }

    public void close() {
        this.springdrawer = null;
    }
}
