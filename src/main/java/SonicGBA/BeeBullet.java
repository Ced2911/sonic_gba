package SonicGBA;

import Lib.Animation;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: BulletObject */
class BeeBullet extends BulletObject {
    private static final int COLLISION_HEIGHT = 512;
    private static final int COLLISION_WIDTH = 512;

    protected BeeBullet(int x, int y, int velX, int velY) {
        super(x, y, velX, velY, true);
        if (beebulletAnimation == null) {
            beebulletAnimation = new Animation("/animation/bee_bullet");
        }
        this.drawer = beebulletAnimation.getDrawer(0, true, 0);
    }

    public void bulletLogic() {
        checkWithPlayer(this.posX, this.posY, this.posX + this.velX, this.posY + this.velY);
        this.posX += this.velX;
        this.posY += this.velY;
    }

    public void draw(MFGraphics g) {
        drawInMap(g, this.drawer);
        this.collisionRect.draw(g, camera);
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 256, y - 256, 512, 512);
    }

    public boolean chkDestroy() {
        return !isInCamera() || isFarAwayCamera();
    }
}
