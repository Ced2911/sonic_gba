package SonicGBA;

import Lib.MyAPI;
import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;

/* compiled from: GimmickObject */
class Ship extends GimmickObject {
    private static final int COLLISION_HEIGHT = 192;
    private static final int COLLISION_OFFSET_Y = 768;
    private static final int COLLISION_WIDTH = 4224;
    private static MFImage shipImage;
    private ShipSystem system;

    protected Ship(int id, int x, int y, ShipSystem system) {
        super(id, x, y, 0, 0, 0, 0);
        this.system = system;
        if (shipImage == null) {
            try {
                shipImage = MFImage.createImage("/gimmick/ship.png");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void logic() {
        this.system.getNewShipPosition(this);
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 2112, y + 768, COLLISION_WIDTH, 192);
    }

    public void draw(MFGraphics g) {
        drawInMap(g, shipImage, this.posX, (this.posY + 768) + 192, 40);
        drawInMap(g, shipImage, 0, 0, MyAPI.zoomIn(shipImage.getWidth()), MyAPI.zoomIn(shipImage.getHeight()), 2, this.posX, (this.posY + 768) + 192, 36);
        drawCollisionRect(g);
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (!player.isFootOnObject(this)) {
            switch (direction) {
                case 1:
                    object.beStop(this.collisionRect.y0, direction, this);
                    return;
                default:
                    return;
            }
        }
    }

    public void close() {
        this.system = null;
    }

    public static void releaseAllResource() {
        shipImage = null;
    }
}
