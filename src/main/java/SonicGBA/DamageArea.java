package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: GimmickObject */
class DamageArea extends GimmickObject {
    private static AnimationDrawer magmaDrawer;
    private AnimationDrawer drawer;
    private boolean isActived;

    protected DamageArea(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        if (StageManager.getCurrentZoneId() == 2) {
            if (magmaDrawer == null) {
                magmaDrawer = new Animation("/animation/magma_bg").getDrawer(0, true, 0);
                magmaDrawer.setPause(true);
            }
            this.drawer = magmaDrawer;
        }
        this.isActived = false;
    }

    public void draw(MFGraphics g) {
        if (this.drawer != null) {
            drawInMap(g, magmaDrawer, this.posX, this.posY - 1024);
            if (this.mWidth > 6144) {
                drawInMap(g, magmaDrawer, this.posX + 6144, this.posY - 1024);
            }
        }
    }

    public void doWhileCollision(PlayerObject player, int direction) {
        if (this.iLeft == 0) {
            player.beHurt();
            return;
        }
        isDamageSandActive = true;
        this.isActived = true;
        player.setDie(false);
    }

    public void doWhileNoCollision() {
        if (this.isActived) {
            isDamageSandActive = false;
            this.isActived = false;
        }
    }

    public void close() {
        this.drawer = null;
    }

    public static void staticLogic() {
        if (magmaDrawer != null) {
            magmaDrawer.moveOn();
        }
    }

    public static void releaseAllResource() {
        Animation.closeAnimationDrawer(magmaDrawer);
        magmaDrawer = null;
    }
}
