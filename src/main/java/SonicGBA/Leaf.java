package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import Lib.SoundSystem;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: GimmickObject */
class Leaf extends GimmickObject {
    private static final int COLLISION_HEIGHT = 4608;
    private static final int COLLISION_WIDTH = 1024;
    private static Animation leafAnimation;
    private boolean isStart;
    private AnimationDrawer leafdrawer;

    protected Leaf(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        if (leafAnimation == null) {
            leafAnimation = new Animation("/animation/bush");
        }
        this.leafdrawer = leafAnimation.getDrawer(0, false, 0);
        this.isStart = false;
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (this.collisionRect.collisionChk(player.getCheckPositionX(), player.getCheckPositionY()) && !this.isStart) {
            this.isStart = true;
            SoundSystem.getInstance().playSe(49);
        }
    }

    public void doWhileNoCollision() {
        this.isStart = false;
        this.leafdrawer.restart();
    }

    public void draw(MFGraphics g) {
        if (this.isStart) {
            drawInMap(g, this.leafdrawer, this.posX, this.posY + 1024);
        }
        drawCollisionRect(g);
    }

    public int getPaintLayer() {
        return 2;
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 512, y, 1024, COLLISION_HEIGHT);
    }
}
