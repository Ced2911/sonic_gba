package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import Lib.MyRandom;
import Lib.SoundSystem;
import com.sega.mobile.define.MDPhone;
import com.sega.mobile.framework.device.MFGamePad;
import com.sega.mobile.framework.device.MFGraphics;
import java.lang.reflect.Array;

/* compiled from: EnemyObject */
class Boss5 extends BossObject {
    private static final int CAGE_DRIP_Y = 1020;
    private static final int EGG_POS_X = 609792;
    private static final int EGG_POS_Y = 69120;
    private static final int EGG_SIDE_LEFT = 9360;
    private static final int EGG_SIDE_RIGHT = (SCREEN_WIDTH + EGG_SIDE_LEFT);
    private static final int F_BALL_A = 3;
    private static final int F_BALL_A_HURT = 8;
    private static final int F_BALL_B = 4;
    private static final int F_BALL_B_HURT = 9;
    private static final int F_DEFENCE_BACK = 15;
    private static final int F_DEFENCE_READY = 13;
    private static final int F_DEFENCING = 14;
    private static final int F_DRIP_AIR = 11;
    private static final int F_DRIP_LAND = 12;
    private static final int F_DRIP_READY = 10;
    private static final int F_FIGHT = 18;
    private static final int F_FLY_A = 16;
    private static final int F_FLY_B = 17;
    private static final int F_HURT_AIR = 6;
    private static final int F_HURT_KNOCK = 5;
    private static final int F_HURT_LAND = 7;
    private static final int F_READY = 0;
    private static final int F_WAIT = 1;
    private static final int F_WAIT_HURT = 2;
    private static final int M_BALL_A = 21;
    private static final int M_BALL_A_HURT = 26;
    private static final int M_BALL_B = 22;
    private static final int M_BALL_B_HURT = 27;
    private static final int M_DEFENCE_BACK = 33;
    private static final int M_DEFENCE_READY = 31;
    private static final int M_DEFENCING = 32;
    private static final int M_DRIP_AIR = 29;
    private static final int M_DRIP_LAND = 30;
    private static final int M_DRIP_READY = 28;
    private static final int M_FLY_A = 34;
    private static final int M_FLY_B = 35;
    private static final int M_HURT_AIR = 24;
    private static final int M_HURT_KNOCK = 23;
    private static final int M_HURT_LAND = 25;
    private static final int M_KO_AIR = 39;
    private static final int M_KO_LAND = 40;
    private static final int M_MISSILE_LAUNCH = 37;
    private static final int M_MISSILE_READY = 36;
    private static final int M_MISSILE_READY_HURT = 38;
    private static final int M_PARTS_BODY = 42;
    private static final int M_PARTS_HAND_L = 44;
    private static final int M_PARTS_HAND_R = 43;
    private static final int M_PARTS_HEAD = 41;
    private static final int M_PARTS_LEG_L = 46;
    private static final int M_PARTS_LEG_R = 45;
    private static final int M_WAIT = 19;
    private static final int M_WAIT_HURT = 20;
    private static final int SIDE_DOWN_MIDDLE = 1128;
    private static final int SIDE_RIGHT = 9496;
    private static final int SIDE_LEFT = (SIDE_RIGHT - SCREEN_WIDTH);
    private static final int BEFORE_MEET_LINE = ((SIDE_LEFT - 232) << 6);
    private static final int MEET_SONIC_LINE = ((SIDE_LEFT + 24) << 6);
    private static final int MIDDLE_SCREEN = (((SIDE_LEFT + SIDE_RIGHT) >> 1) << 6);
    private static final int START_POS_X = 605696;
    private static final int STATE_ESCAPE = 40;
    private static final int STATE_FRESH_ATTACK_TRANS = 3;
    private static final int STATE_FRESH_BALL_HORIZON_ATTACK_FIGHT = 6;
    private static final int STATE_FRESH_BALL_HORIZON_ATTACK_READY = 5;
    private static final int STATE_FRESH_BALL_UP = 7;
    private static final int STATE_FRESH_DEFENCE_BACK = 18;
    private static final int STATE_FRESH_DEFENCE_READY = 16;
    private static final int STATE_FRESH_DEFENCING = 17;
    private static final int STATE_FRESH_FIGHT = 12;
    private static final int STATE_FRESH_FLY = 8;
    private static final int STATE_FRESH_FLY_DRIPPING = 10;
    private static final int STATE_FRESH_FLY_DRIP_LAND = 11;
    private static final int STATE_FRESH_FLY_DRIP_READY = 9;
    private static final int STATE_FRESH_HURT_AIR = 14;
    private static final int STATE_FRESH_HURT_KNOCK = 13;
    private static final int STATE_FRESH_HURT_LAND = 15;
    private static final int STATE_FRESH_READY = 4;
    private static final int STATE_FRESH_WAIT_0 = 0;
    private static final int STATE_FRESH_WAIT_1 = 1;
    private static final int STATE_FRESH_WAKE = 2;
    private static final int STATE_MACHINE_ATTACK_TRANS = 20;
    private static final int STATE_MACHINE_BALL_HORIZON_ATTACK_FIGHT = 22;
    private static final int STATE_MACHINE_BALL_HORIZON_ATTACK_READY = 21;
    private static final int STATE_MACHINE_BALL_UP = 23;
    private static final int STATE_MACHINE_BROKEN = 38;
    private static final int STATE_MACHINE_DEFENCE_BACK = 35;
    private static final int STATE_MACHINE_DEFENCE_READY = 33;
    private static final int STATE_MACHINE_DEFENCING = 34;
    private static final int STATE_MACHINE_FLY = 24;
    private static final int STATE_MACHINE_FLY_DRIPPING = 26;
    private static final int STATE_MACHINE_FLY_DRIP_LAND = 27;
    private static final int STATE_MACHINE_FLY_DRIP_READY = 25;
    private static final int STATE_MACHINE_HURT_AIR = 31;
    private static final int STATE_MACHINE_HURT_KNOCK = 30;
    private static final int STATE_MACHINE_HURT_LAND = 32;
    private static final int STATE_MACHINE_KO_AIR = 36;
    private static final int STATE_MACHINE_KO_LAND = 37;
    private static final int STATE_MACHINE_MISSILE_ATTACK = 29;
    private static final int STATE_MACHINE_MISSILE_READY = 28;
    private static final int STATE_MACHINE_PIECES = 39;
    private static final int STATE_MACHINE_READY = 19;
    private static Animation boatAni = null;
    private static int damageframe = 0;
    private static final int defence_cnt_max = 15;
    private static Animation escapefaceAni = null;
    private static final int first_jump_cnt_max = 3;
    private static Animation knucklesAni = null;
    private static final int ready_cnt_max = 24;
    private static final int talk_cnt_max = 9;
    private int ALERT_RANGE = 11520;
    private int AttackStartDirection;
    private int BOSS5_WIDTH = 1536;
    private int COLLISION_HEIGHT = Boss6Block.COLLISION2_HEIGHT;
    private int COLLISION_WIDTH = 1024;
    private boolean IsConner = false;
    private boolean IsHurt = false;
    private boolean IsPlayerRunaway = false;
    private int KOWaitCnt;
    private int KOWaitCntMax = 10;
    private int[] Vix = new int[]{750, SmallAnimal.FLY_VELOCITY_Y, -150, 150, 450, -750, 600, 300, -450};
    private int[] Viy = new int[]{-1200, -1050, PlayerObject.MIN_ATTACK_JUMP, -750, -600};
    private int WaitCnt;
    private int alert_state;
    private AnimationDrawer boatdrawer;
    private int boomX;
    private int boomY;
    private int boom_offset = MDPhone.SCREEN_HEIGHT;
    private AnimationDrawer boomdrawer;
    private BossBroken bossbroken;
    private int defence_cnt = 0;
    private int enemyDirct;
    private int escape_cnt = 0;
    private int escape_cnt_max = 60;
    private int escape_v = 512;
    private AnimationDrawer escapefacedrawer;
    private int fight_alert_range = 2048;
    private int first_jump_cnt = 0;
    private int fly_attack_site;
    private int fly_drip_offset = 2304;
    private int fly_end;
    private int fly_move_x_speed1 = -772;
    private int fly_move_y_speed1 = 140;
    private int fly_range = MFGamePad.KEY_NUM_6;
    private int fly_top;
    private int fly_top_offset = 5632;
    private int fly_up_speed1 = BarHorbinV.COLLISION_HEIGHT;
    private Boss5FlyDefence flydefence;
    private int horizonAttackReady_cnt;
    private int horizonAttackReady_cnt_max = 16;
    private int horizon_move_speed = 1080;
    private AnimationDrawer knuckdrawer;
    private int limitLeftX;
    private int limitRightX;
    private int missile_alert_range = 5120;
    private int missile_alert_state;
    private int pieces_drip_cnt;
    private int[][] pos;
    private int prestate;
    private int randomAttackState;
    private int ready_cnt = 0;
    private int state;
    private int talk_cnt = 0;
    private int velX;
    private int velY;
    private int velocity = -768;

    public static void releaseAllResource() {
        Animation.closeAnimation(knucklesAni);
        Animation.closeAnimation(BoomAni);
        Animation.closeAnimation(boatAni);
        Animation.closeAnimation(escapefaceAni);
        knucklesAni = null;
        BoomAni = null;
        boatAni = null;
        escapefaceAni = null;
    }

    protected Boss5(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        this.posX -= this.iLeft * 8;
        this.posY -= this.iTop * 8;
        this.posY -= 960;
        this.posX = START_POS_X;
        this.posY = getGroundY(this.posX, this.posY);
        this.limitRightX = 606720;
        this.limitLeftX = (SIDE_LEFT + 16) << 6;
        this.fly_top = this.posY - this.fly_top_offset;
        refreshCollisionRect(this.posX >> 6, this.posY >> 6);
        if (knucklesAni == null) {
            knucklesAni = new Animation("/animation/boss5");
        }
        this.knuckdrawer = knucklesAni.getDrawer(0, true, 0);
        if (BoomAni == null) {
            BoomAni = new Animation("/animation/boom");
        }
        this.boomdrawer = BoomAni.getDrawer(0, true, 0);
        if (boatAni == null) {
            boatAni = new Animation("/animation/pod_boat");
        }
        this.boatdrawer = boatAni.getDrawer(0, true, 0);
        if (escapefaceAni == null) {
            escapefaceAni = new Animation("/animation/pod_face");
        }
        this.escapefacedrawer = escapefaceAni.getDrawer(2, false, 0);
        this.flydefence = new Boss5FlyDefence(33, x, y, 0, 0, 0, 0);
        GameObject.addGameObject(this.flydefence, x, y);
        setBossHP();
    }

    private int halfLife() {
        return (GlobalResource.isEasyMode() && stageModeState == 0) ? 3 : 4;
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (this.dead || object != player) {
            return;
        }
        if (player.isAttackingEnemy()) {
            switch (this.state) {
                case 3:
                case 13:
                case 14:
                case 15:
                case 20:
                case 30:
                case 31:
                case 32:
                case 36:
                case 37:
                case 38:
                case 39:
                case 40:
                    return;
                case 5:
                case 6:
                case 7:
                case 8:
                case 21:
                case 22:
                case 23:
                case 24:
                    player.beHurt();
                    return;
                case 16:
                case 17:
                case 18:
                case 33:
                case 34:
                case 35:
                    PlayerHurtBall(object, direction);
                    return;
                default:
                    this.enemyDirct = direction;
                    HurtLogic();
                    return;
            }
        }
        switch (this.state) {
            case 4:
            case 9:
            case 10:
            case 11:
            case 13:
            case 14:
            case 15:
            case 19:
            case 20:
            case 25:
            case 26:
            case 27:
            case 30:
            case 31:
            case 32:
            case 36:
            case 37:
            case 38:
            case 39:
            case 40:
                return;
            default:
                player.beHurt();
                return;
        }
    }

    public void doWhileBeAttack(PlayerObject object, int direction, int animationID) {
        switch (this.state) {
            case 3:
            case 13:
            case 14:
            case 15:
            case 20:
            case 30:
            case 31:
            case 32:
            case 36:
            case 37:
            case 38:
            case 39:
            case 40:
                return;
            case 16:
            case 17:
            case 18:
            case 33:
            case 34:
            case 35:
                PlayerHurtBall(object, direction);
                return;
            default:
                this.enemyDirct = direction;
                HurtLogic();
                return;
        }
    }

    private int halfLifeNum() {
        return (GlobalResource.isEasyMode() && stageModeState == 0) ? 3 : 4;
    }

    public void HurtLogic() {
        if (!this.IsHurt) {
            player.doAttackPose(this, this.enemyDirct);
            this.HP--;
            this.velY = -Math.abs(this.velocity);
            if (player.getVelX() > 0) {
                this.velX = Math.abs(this.velocity) >> 1;
            } else {
                this.velX = (-Math.abs(this.velocity)) >> 1;
            }
            hurt_side_cotrol();
            this.IsHurt = true;
            if (this.HP >= halfLifeNum()) {
                changeAniState(this.knuckdrawer, 5, false);
                this.state = 13;
                if (this.HP == halfLifeNum()) {
                    isBossHalf = true;
                    SoundSystem.getInstance().playBgm(24, true);
                } else {
                    isBossHalf = false;
                    SoundSystem.getInstance().playSe(34, false);
                }
            } else if (this.HP == 0) {
                changeAniState(this.knuckdrawer, 39, true);
                this.state = 36;
                SoundSystem.getInstance().playSe(35, false);
            } else {
                changeAniState(this.knuckdrawer, 23, false);
                this.state = 30;
                SoundSystem.getInstance().playSe(34, false);
            }
            this.COLLISION_WIDTH = 64;
            this.COLLISION_HEIGHT = 64;
        }
    }

    public void PlayerHurtBall(PlayerObject object, int direction) {
        int playerVelX = Math.abs(player.getVelX());
        int preAnimationId = player.getAnimationId();
        switch (direction) {
            case 0:
                object.beStop(this.collisionRect.y1, direction, this);
                break;
            case 1:
            case 4:
                object.beStop(this.collisionRect.y0, direction, this);
                break;
            case 2:
                object.beStop(this.collisionRect.x1, direction, this);
                player.setVelX(playerVelX);
                break;
            case 3:
                object.beStop(this.collisionRect.x0, direction, this);
                player.setVelX(-playerVelX);
                break;
        }
        player.setAnimationId(preAnimationId);
        if (this.defence_cnt < 15) {
            this.defence_cnt++;
        } else {
            this.IsPlayerRunaway = true;
        }
        PlayerObject playerObject = player;
        int characterID = PlayerObject.getCharacterID();
        PlayerObject playerObject2 = player;
        if (characterID == 3) {
            soundInstance.playSe(24);
        }
    }

    public boolean IsNeedforDefence(int state) {
        return state == 0 && player.isAttackingEnemy() && player.isOnGound();
    }

    public void logic() {
        if (!this.dead) {
            int preX = this.posX;
            int preY = this.posY;
            this.boomX = preX;
            this.boomY = preY;
            if ((this.boomY + 1024) + this.velY >= getGroundY(this.boomX, this.boomY)) {
                this.boomY = getGroundY(this.boomX, this.boomY) - 1024;
            }
            if (this.state > 0) {
                isBossEnter = true;
            }
            int tmpstate;
            int i;
            switch (this.state) {
                case 0:
                    if (player.getFootPositionX() >= BEFORE_MEET_LINE) {
                        this.state = 1;
                        MapManager.setCameraUpLimit(SIDE_DOWN_MIDDLE - ((MapManager.CAMERA_HEIGHT * 3) / 4));
                        MapManager.setCameraDownLimit(((MapManager.CAMERA_HEIGHT * 1) / 4) + SIDE_DOWN_MIDDLE);
                        break;
                    }
                    break;
                case 1:
                    if (player.getFootPositionX() >= MEET_SONIC_LINE) {
                        player.setMeetingBoss(false);
                        MapManager.setCameraLeftLimit(SIDE_LEFT);
                        MapManager.setCameraRightLimit(SIDE_RIGHT);
                    } else {
                        MapManager.setCameraLeftLimit(MapManager.getCamera().f13x);
                    }
                    if (this.posX < (((MapManager.getCamera().f13x + MapManager.CAMERA_WIDTH) - 30) << 6)) {
                        changeAniState(this.knuckdrawer, 0, false);
                        this.COLLISION_WIDTH = 1024;
                        this.COLLISION_HEIGHT = Boss6Block.COLLISION2_HEIGHT;
                        this.state = 2;
                        bossFighting = true;
                        bossID = 26;
                        SoundSystem.getInstance().playBgm(23);
                        break;
                    }
                    break;
                case 2:
                    if (this.knuckdrawer.checkEnd()) {
                        changeAniState(this.knuckdrawer, 1, true);
                        this.COLLISION_WIDTH = 1024;
                        this.COLLISION_HEIGHT = Boss6Block.COLLISION2_HEIGHT;
                        this.talk_cnt = 1;
                    }
                    if (this.talk_cnt >= 1) {
                        this.talk_cnt++;
                        if (this.talk_cnt != 9) {
                            if (this.talk_cnt >= 19 && player.getAnimationId() == 0) {
                                this.state = 4;
                                player.releaseOutOfControl();
                                break;
                            }
                        }
                        player.setMeetingBoss(true);
                        player.setOutOfControl(this);
                        PlayerObject playerObject = player;
                        PlayerObject playerObject2 = player;
                        playerObject.setAnimationId(53);
                        break;
                    }
                    break;
                case 3:
                    this.state = randomSetState();
                    break;
                case 4:
                    this.alert_state = checkPlayerInEnemyAlertRange(this.posX >> 6, this.posY >> 6, this.fight_alert_range * 2);
                    if (!IsNeedforDefence(this.alert_state)) {
                        if (this.ready_cnt >= 24) {
                            this.ready_cnt = 0;
                            this.state = 3;
                            this.prestate = 4;
                            break;
                        }
                        this.ready_cnt++;
                        break;
                    }
                    changeAniState(this.knuckdrawer, 13, false);
                    this.COLLISION_WIDTH = 1024;
                    this.COLLISION_HEIGHT = 1024;
                    this.state = 16;
                    soundInstance.playSe(24);
                    this.IsPlayerRunaway = false;
                    this.defence_cnt = 0;
                    break;
                case 5:
                    if (this.horizonAttackReady_cnt >= this.horizonAttackReady_cnt_max) {
                        if (this.IsHurt) {
                            tmpstate = 8;
                        } else {
                            tmpstate = 3;
                        }
                        changeAniState(this.knuckdrawer, tmpstate, true);
                        this.COLLISION_WIDTH = 1536;
                        this.COLLISION_HEIGHT = 1536;
                        this.horizonAttackReady_cnt = 0;
                        this.state = 6;
                        soundInstance.playSe(5);
                        break;
                    }
                    this.horizonAttackReady_cnt++;
                    break;
                case 6:
                    this.IsHurt = false;
                    if (this.AttackStartDirection > 0) {
                        if (this.posX <= this.limitLeftX) {
                            this.posX = this.limitLeftX;
                            this.IsConner = true;
                        } else {
                            this.posX += this.horizon_move_speed;
                        }
                    } else if (this.posX >= this.limitRightX) {
                        this.posX = this.limitRightX;
                        this.IsConner = true;
                    } else {
                        this.posX += this.horizon_move_speed;
                    }
                    if (this.IsConner) {
                        this.horizonAttackReady_cnt = 0;
                        this.IsConner = false;
                        this.state = 3;
                        this.prestate = 6;
                        break;
                    }
                    break;
                case 7:
                    if (this.posY > this.fly_top) {
                        this.posY -= this.fly_up_speed1;
                        break;
                    }
                    this.posY = this.fly_top;
                    changeAniState(this.knuckdrawer, 16, true);
                    this.AttackStartDirection = this.posX - player.getFootPositionX();
                    this.COLLISION_WIDTH = 960;
                    this.COLLISION_HEIGHT = 1472;
                    this.state = 8;
                    break;
                case 8:
                    this.IsHurt = false;
                    if (this.AttackStartDirection <= 0) {
                        if (player.getFootPositionX() + this.fly_drip_offset < this.limitRightX) {
                            this.fly_attack_site = player.getFootPositionX() + this.fly_drip_offset;
                        } else {
                            this.fly_attack_site = this.limitRightX;
                        }
                        if (this.posX + this.fly_move_x_speed1 < this.fly_attack_site) {
                            this.posX += this.fly_move_x_speed1;
                            this.posY += this.fly_move_y_speed1;
                            break;
                        }
                        this.posX = this.fly_attack_site;
                        changeAniStateNoTrans(this.knuckdrawer, 10, false);
                        this.COLLISION_WIDTH = 1408;
                        this.COLLISION_HEIGHT = 2432;
                        this.state = 9;
                        break;
                    }
                    if (player.getFootPositionX() - this.fly_drip_offset > this.limitLeftX) {
                        this.fly_attack_site = player.getFootPositionX() - this.fly_drip_offset;
                    } else {
                        this.fly_attack_site = this.limitLeftX;
                    }
                    if (this.posX + this.fly_move_x_speed1 > this.fly_attack_site) {
                        this.posX += this.fly_move_x_speed1;
                        this.posY += this.fly_move_y_speed1;
                        break;
                    }
                    this.posX = this.fly_attack_site;
                    changeAniStateNoTrans(this.knuckdrawer, 10, false);
                    this.COLLISION_WIDTH = 1920;
                    this.COLLISION_HEIGHT = 2432;
                    this.state = 9;
                    break;
                case 9:
                    if (this.knuckdrawer.checkEnd()) {
                        changeAniStateNoTrans(this.knuckdrawer, 11, true);
                        this.COLLISION_WIDTH = 1536;
                        this.COLLISION_HEIGHT = 2432;
                        this.state = 10;
                        this.velY = 0;
                        break;
                    }
                    break;
                case 10:
                    if (this.posY + this.velY < getGroundY(this.posX, this.posY)) {
                        this.velY += GRAVITY;
                        this.posY += this.velY;
                        break;
                    }
                    this.posY = getGroundY(this.posX, this.posY);
                    changeAniStateNoTrans(this.knuckdrawer, 12, false);
                    this.COLLISION_WIDTH = 1408;
                    this.COLLISION_HEIGHT = 1920;
                    this.state = 11;
                    break;
                case 11:
                    if (this.knuckdrawer.checkEnd()) {
                        this.state = 3;
                        this.prestate = 11;
                        break;
                    }
                    break;
                case 12:
                    if (!this.knuckdrawer.checkEnd()) {
                        if (this.knuckdrawer.getCurrentFrame() == 3 || this.knuckdrawer.getCurrentFrame() == 4 || this.knuckdrawer.getCurrentFrame() == 5 || this.knuckdrawer.getCurrentFrame() == 6 || this.knuckdrawer.getCurrentFrame() == 11 || this.knuckdrawer.getCurrentFrame() == 12) {
                            this.COLLISION_WIDTH = 3584;
                            this.COLLISION_HEIGHT = Boss6Block.COLLISION_WIDTH;
                        } else {
                            this.COLLISION_WIDTH = 1536;
                            this.COLLISION_HEIGHT = 1920;
                        }
                        if (this.knuckdrawer.getCurrentFrame() == 1 || this.knuckdrawer.getCurrentFrame() == 5) {
                            soundInstance.playSe(19);
                            break;
                        }
                    }
                    this.state = 3;
                    this.prestate = 12;
                    break;
                case 13:
                    this.flydefence.setHurtState(false);
                    if (!this.knuckdrawer.checkEnd()) {
                        hurt_air_control();
                        if (isBossHalf) {
                            damageframe++;
                            damageframe %= 11;
                            if (damageframe % 2 == 0) {
                                SoundSystem.getInstance().playSe(35);
                                break;
                            }
                        }
                    }
                    changeAniStateNoTrans(this.knuckdrawer, 6, true);
                    this.state = 14;
                    break;
                case 14:
                    this.flydefence.setHurtState(false);
                    if (this.posY + this.velY < getGroundY(this.posX, this.posY)) {
                        hurt_air_control();
                        if (isBossHalf) {
                            damageframe++;
                            damageframe %= 11;
                            if (damageframe % 2 == 0) {
                                SoundSystem.getInstance().playSe(35);
                                break;
                            }
                        }
                    }
                    this.posY = getGroundY(this.posX, this.posY);
                    changeAniStateNoTrans(this.knuckdrawer, 7, false);
                    this.state = 15;
                    break;
                case 15:
                    this.flydefence.setHurtState(false);
                    if (this.knuckdrawer.checkEnd()) {
                        if (this.HP != halfLifeNum()) {
                            this.state = 3;
                            this.prestate = 15;
                            break;
                        }
                        this.state = 20;
                        this.prestate = 32;
                        break;
                    }
                    break;
                case 16:
                    if (this.knuckdrawer.checkEnd()) {
                        changeAniState(this.knuckdrawer, 14, true);
                        this.COLLISION_WIDTH = 1024;
                        this.COLLISION_HEIGHT = 1024;
                        this.state = 17;
                        break;
                    }
                    break;
                case 17:
                    if (!(!this.IsPlayerRunaway && player.isAttackingEnemy() && player.isOnGound())) {
                        changeAniState(this.knuckdrawer, 15, false);
                        this.COLLISION_WIDTH = 1024;
                        this.COLLISION_HEIGHT = 1024;
                        this.state = 18;
                        break;
                    }
                case 18:
                    if (this.knuckdrawer.checkEnd()) {
                        this.state = 3;
                        this.prestate = 18;
                        this.IsPlayerRunaway = false;
                        break;
                    }
                    break;
                case 19:
                    this.alert_state = checkPlayerInEnemyAlertRange(this.posX >> 6, this.posY >> 6, this.fight_alert_range * 2);
                    if (!IsNeedforDefence(this.alert_state)) {
                        if (this.ready_cnt >= 24) {
                            this.ready_cnt = 0;
                            this.state = 20;
                            this.prestate = 19;
                            break;
                        }
                        this.ready_cnt++;
                        break;
                    }
                    changeAniState(this.knuckdrawer, 31, false);
                    this.COLLISION_WIDTH = 1024;
                    this.COLLISION_HEIGHT = 1024;
                    this.state = 33;
                    this.IsPlayerRunaway = false;
                    this.defence_cnt = 0;
                    break;
                case 20:
                    this.state = randomSetState();
                    break;
                case 21:
                    if (this.horizonAttackReady_cnt >= this.horizonAttackReady_cnt_max) {
                        if (this.IsHurt) {
                            tmpstate = 26;
                        } else {
                            tmpstate = 21;
                        }
                        changeAniState(this.knuckdrawer, tmpstate, true);
                        this.COLLISION_WIDTH = 1536;
                        this.COLLISION_HEIGHT = 1536;
                        this.horizonAttackReady_cnt = 0;
                        this.state = 22;
                        soundInstance.playSe(5);
                        break;
                    }
                    this.horizonAttackReady_cnt++;
                    break;
                case 22:
                    this.IsHurt = false;
                    if (this.AttackStartDirection > 0) {
                        if (this.posX <= this.limitLeftX) {
                            this.posX = this.limitLeftX;
                            this.IsConner = true;
                        } else {
                            this.posX += this.horizon_move_speed;
                        }
                    } else if (this.posX >= this.limitRightX) {
                        this.posX = this.limitRightX;
                        this.IsConner = true;
                    } else {
                        this.posX += this.horizon_move_speed;
                    }
                    if (this.IsConner) {
                        this.horizonAttackReady_cnt = 0;
                        this.IsConner = false;
                        this.state = 20;
                        this.prestate = 22;
                        break;
                    }
                    break;
                case 23:
                    if (this.posY > this.fly_top) {
                        this.posY -= Math.abs(this.velocity);
                        break;
                    }
                    this.posY = this.fly_top;
                    changeAniState(this.knuckdrawer, 34, true);
                    this.AttackStartDirection = this.posX - player.getFootPositionX();
                    this.COLLISION_WIDTH = 960;
                    this.COLLISION_HEIGHT = 1472;
                    this.state = 24;
                    break;
                case 24:
                    this.IsHurt = false;
                    if (this.AttackStartDirection <= 0) {
                        if (player.getFootPositionX() + this.fly_drip_offset < this.limitRightX) {
                            this.fly_attack_site = player.getFootPositionX() + this.fly_drip_offset;
                        } else {
                            this.fly_attack_site = this.limitRightX;
                        }
                        if (this.posX + this.fly_move_x_speed1 < this.fly_attack_site) {
                            this.posX += this.fly_move_x_speed1;
                            this.posY += this.fly_move_y_speed1;
                            break;
                        }
                        this.posX = this.fly_attack_site;
                        changeAniStateNoTrans(this.knuckdrawer, 28, false);
                        this.COLLISION_WIDTH = 1408;
                        this.COLLISION_HEIGHT = 2432;
                        this.state = 25;
                        break;
                    }
                    if (player.getFootPositionX() - this.fly_drip_offset > this.limitLeftX) {
                        this.fly_attack_site = player.getFootPositionX() - this.fly_drip_offset;
                    } else {
                        this.fly_attack_site = this.limitLeftX;
                    }
                    if (this.posX + this.fly_move_x_speed1 > this.fly_attack_site) {
                        this.posX += this.fly_move_x_speed1;
                        this.posY += this.fly_move_y_speed1;
                        break;
                    }
                    this.posX = this.fly_attack_site;
                    changeAniStateNoTrans(this.knuckdrawer, 28, false);
                    this.COLLISION_WIDTH = 1920;
                    this.COLLISION_HEIGHT = 2432;
                    this.state = 25;
                    break;
                case 25:
                    if (this.knuckdrawer.checkEnd()) {
                        changeAniStateNoTrans(this.knuckdrawer, 29, true);
                        this.COLLISION_WIDTH = 1536;
                        this.COLLISION_HEIGHT = 2432;
                        this.state = 26;
                        this.velY = 0;
                        break;
                    }
                    break;
                case 26:
                    if (this.posY + this.velY < getGroundY(this.posX, this.posY)) {
                        this.velY += GRAVITY;
                        this.posY += this.velY;
                        break;
                    }
                    this.posY = getGroundY(this.posX, this.posY);
                    changeAniStateNoTrans(this.knuckdrawer, 30, false);
                    this.COLLISION_WIDTH = 1408;
                    this.COLLISION_HEIGHT = 1920;
                    this.state = 27;
                    break;
                case 27:
                    if (this.knuckdrawer.checkEnd()) {
                        this.state = 20;
                        this.prestate = 27;
                        break;
                    }
                    break;
                case 28:
                    if (this.knuckdrawer.checkEnd()) {
                        changeAniState(this.knuckdrawer, 37, false);
                        this.COLLISION_WIDTH = 1408;
                        this.COLLISION_HEIGHT = Boss6Block.COLLISION_WIDTH;
                        this.state = 29;
                        if (this.posX - player.getFootPositionX() > 0) {
                            BulletObject.addBullet(16, this.posX - 1280, this.posY - 1280, -320, 0);
                        } else {
                            BulletObject.addBullet(16, this.posX + 1280, this.posY - 1280, 320, 0);
                        }
                        MapManager.setShake(10);
                        break;
                    }
                    break;
                case 29:
                    this.IsHurt = false;
                    if (this.knuckdrawer.checkEnd()) {
                        this.state = 20;
                        this.prestate = 29;
                        break;
                    }
                    break;
                case 30:
                    this.flydefence.setHurtState(false);
                    if (!this.knuckdrawer.checkEnd()) {
                        hurt_air_control();
                        break;
                    }
                    changeAniStateNoTrans(this.knuckdrawer, 24, true);
                    this.state = 31;
                    break;
                case 31:
                    this.flydefence.setHurtState(false);
                    if (this.posY + this.velY < getGroundY(this.posX, this.posY)) {
                        hurt_air_control();
                        break;
                    }
                    this.posY = getGroundY(this.posX, this.posY);
                    changeAniStateNoTrans(this.knuckdrawer, 25, false);
                    this.state = 32;
                    break;
                case 32:
                    this.flydefence.setHurtState(false);
                    if (this.knuckdrawer.checkEnd()) {
                        this.state = 20;
                        this.prestate = 32;
                        break;
                    }
                    break;
                case 33:
                    if (this.knuckdrawer.checkEnd()) {
                        changeAniState(this.knuckdrawer, 32, true);
                        this.COLLISION_WIDTH = 1024;
                        this.COLLISION_HEIGHT = 1024;
                        this.state = 34;
                        break;
                    }
                    break;
                case 34:
                    if (!(!this.IsPlayerRunaway && player.isAttackingEnemy() && player.isOnGound())) {
                        changeAniState(this.knuckdrawer, 33, false);
                        this.COLLISION_WIDTH = 1024;
                        this.COLLISION_HEIGHT = 1024;
                        this.state = 35;
                        break;
                    }
                case 35:
                    if (this.knuckdrawer.checkEnd()) {
                        changeAniState(this.knuckdrawer, 19, false);
                        this.COLLISION_WIDTH = 1024;
                        this.COLLISION_HEIGHT = Boss6Block.COLLISION2_HEIGHT;
                        this.state = 20;
                        this.prestate = 35;
                        this.IsPlayerRunaway = false;
                        break;
                    }
                    break;
                case 36:
                    if (this.posY + this.velY < getGroundY(this.posX, this.posY)) {
                        hurt_air_control();
                        break;
                    }
                    this.posY = getGroundY(this.posX, this.posY);
                    changeAniState(this.knuckdrawer, 40, true);
                    this.state = 37;
                    break;
                case 37:
                    if (this.KOWaitCnt >= this.KOWaitCntMax) {
                        this.state = 38;
                        this.bossbroken = new BossBroken(26, this.posX >> 6, this.posY >> 6, 0, 0, 0, 0);
                        GameObject.addGameObject(this.bossbroken, this.posX >> 6, this.posY >> 6);
                        this.bossbroken.setTotalCntMax(6);
                        this.bossbroken.setJumpTime(9);
                        break;
                    }
                    this.KOWaitCnt++;
                    break;
                case 38:
                    this.bossbroken.logicBoom(this.posX, this.posY);
                    if (this.bossbroken.getEndState()) {
                        this.state = 39;
                        this.pos = (int[][]) Array.newInstance(Integer.TYPE, new int[]{6, 4});
                        for (i = 0; i < this.pos.length; i++) {
                            this.pos[i][0] = this.posX;
                            this.pos[i][1] = this.posY - this.boom_offset;
                            this.pos[i][2] = this.Vix[MyRandom.nextInt(this.Vix.length)];
                            this.pos[i][3] = this.Viy[MyRandom.nextInt(this.Viy.length)];
                        }
                        break;
                    }
                    break;
                case 39:
                    for (i = 0; i < this.pos.length; i++) {
                        int[] iArr = this.pos[i];
                        iArr[0] = iArr[0] + this.pos[i][2];
                        iArr = this.pos[i];
                        iArr[3] = iArr[3] + (GRAVITY >> 1);
                        iArr = this.pos[i];
                        iArr[1] = iArr[1] + this.pos[i][3];
                        if (this.pos[i][1] >= this.posY) {
                            this.pieces_drip_cnt++;
                        }
                    }
                    if (this.pieces_drip_cnt >= this.pos.length) {
                        this.posX = EGG_POS_X;
                        this.posY = EGG_POS_Y;
                        this.fly_end = 607744;
                        this.escapefacedrawer.setActionId(0);
                        this.escapefacedrawer.setLoop(true);
                        this.WaitCnt = 0;
                        this.state = 40;
                        MapManager.setCameraLeftLimit(EGG_SIDE_LEFT);
                        MapManager.setCameraRightLimit(EGG_SIDE_RIGHT);
                        bossFighting = false;
                        player.getBossScore();
                        SoundSystem.getInstance().playBgm(StageManager.getBgmId(), true);
                        break;
                    }
                    break;
                case 40:
                    if (this.posX <= (((MapManager.getCamera().f13x + MapManager.CAMERA_WIDTH) - 30) << 6) && this.WaitCnt == 0) {
                        this.escapefacedrawer.setActionId(2);
                        this.escapefacedrawer.setLoop(false);
                        this.WaitCnt = 1;
                        int side_left = MapManager.getCamera().f13x;
                        int side_right = side_left + MapManager.CAMERA_WIDTH;
                        MapManager.setCameraLeftLimit(side_left);
                        MapManager.setCameraRightLimit(side_right);
                    }
                    if (this.escapefacedrawer.checkEnd() && this.WaitCnt == 1) {
                        this.escapefacedrawer.setActionId(0);
                        this.boatdrawer.setActionId(1);
                        this.boatdrawer.setLoop(false);
                        this.WaitCnt = 2;
                    }
                    if (this.WaitCnt == 2 && this.boatdrawer.checkEnd()) {
                        this.escapefacedrawer.setActionId(0);
                        this.escapefacedrawer.setTrans(2);
                        this.escapefacedrawer.setLoop(true);
                        this.boatdrawer.setActionId(1);
                        this.boatdrawer.setTrans(2);
                        this.boatdrawer.setLoop(false);
                        this.WaitCnt = 3;
                    }
                    if (this.WaitCnt == 3 && this.boatdrawer.checkEnd()) {
                        this.boatdrawer.setActionId(0);
                        this.boatdrawer.setTrans(2);
                        this.boatdrawer.setLoop(true);
                        this.WaitCnt = 4;
                    }
                    if (this.WaitCnt == 4 || this.WaitCnt == 5) {
                        this.posX += this.escape_v;
                    }
                    if (this.posX - this.fly_end > this.fly_range && this.WaitCnt == 4) {
                        this.WaitCnt = 5;
                    }
                    if (this.WaitCnt == 5) {
                        if (this.escape_cnt < this.escape_cnt_max) {
                            this.escape_cnt++;
                        } else {
                            this.WaitCnt = 6;
                        }
                    }
                    if (this.WaitCnt == 6) {
                        GameObject.addGameObject(new Cage((MapManager.getCamera().f13x + (MapManager.CAMERA_WIDTH >> 1)) << 6, 65280));
                        this.WaitCnt = 7;
                    }
                    checkWithPlayer(preX, preY, this.posX, this.posY);
                    break;
            }
            this.flydefence.logic(this.posX, this.posY, this.AttackStartDirection);
            if (this.state == 8 || this.state == 24) {
                this.flydefence.setCollAvailable(true);
                if (this.flydefence.getHurtState()) {
                    HurtLogic();
                    this.flydefence.setHurtState(false);
                }
            } else {
                this.flydefence.setCollAvailable(false);
            }
            refreshCollisionRect(this.posX >> 6, this.posY >> 6);
            checkWithPlayer(preX, preY, this.posX, this.posY);
        }
    }

    private boolean CanFreshFight() {
        if (Math.abs(this.posX - player.getFootPositionX()) > this.fight_alert_range || !player.isOnGound()) {
            return false;
        }
        return true;
    }

    public int randomSetState() {
        int result = 0;
        this.missile_alert_state = checkPlayerInEnemyAlertRange(this.posX >> 6, this.posY >> 6, this.missile_alert_range);
        if (this.first_jump_cnt >= 3) {
            int random;
            switch (this.prestate) {
                case 4:
                    if (!CanFreshFight()) {
                        random = MyRandom.nextInt(0, 100);
                        if (random < 0 || random >= 45) {
                            if (random < 45 || random >= 55) {
                                if (random >= 55) {
                                    result = 5;
                                    soundInstance.playSe(4);
                                    break;
                                }
                            }
                            result = 4;
                            break;
                        }
                        result = 7;
                        soundInstance.playSe(11);
                        break;
                    }
                    result = 12;
                    break;
                case 6:
                    if (!CanFreshFight()) {
                        result = 4;
                        break;
                    }
                    result = 12;
                    break;
                case 11:
                case 12:
                    if (!CanFreshFight()) {
                        random = MyRandom.nextInt(0, 15);
                        if (random < 0 || random >= 5) {
                            if (random < 5 || random >= 10) {
                                if (random >= 10) {
                                    result = 5;
                                    soundInstance.playSe(4);
                                    break;
                                }
                            }
                            result = 4;
                            break;
                        }
                        result = 7;
                        soundInstance.playSe(11);
                        break;
                    }
                    result = 12;
                    break;
                case 15:
                    if (!CanFreshFight()) {
                        random = MyRandom.nextInt(0, 100);
                        if (random < 0 || random >= 50) {
                            if (random >= 50) {
                                result = 5;
                                soundInstance.playSe(4);
                                break;
                            }
                        }
                        result = 7;
                        soundInstance.playSe(11);
                        break;
                    }
                    result = 12;
                    break;
                case 18:
                    if (!CanFreshFight()) {
                        random = MyRandom.nextInt(0, 100);
                        if (random < 0 || random >= 25) {
                            if (random < 25 || random >= 30) {
                                if (random >= 30) {
                                    result = 5;
                                    soundInstance.playSe(4);
                                    break;
                                }
                            }
                            result = 4;
                            break;
                        }
                        result = 7;
                        soundInstance.playSe(11);
                        break;
                    }
                    result = 12;
                    break;
                case 19:
                    if (this.missile_alert_state != 0) {
                        random = MyRandom.nextInt(0, 100);
                        if (random < 0 || random >= 12) {
                            if (random < 12 || random >= 25) {
                                if (random >= 25) {
                                    result = 28;
                                    break;
                                }
                            }
                            result = 21;
                            soundInstance.playSe(4);
                            break;
                        }
                        result = 23;
                        soundInstance.playSe(11);
                        break;
                    }
                    random = MyRandom.nextInt(0, 100);
                    if (random < 0 || random >= 45) {
                        if (random < 45 || random >= 55) {
                            if (random >= 55) {
                                result = 21;
                                soundInstance.playSe(4);
                                break;
                            }
                        }
                        result = 19;
                        break;
                    }
                    result = 23;
                    soundInstance.playSe(11);
                    break;
                case 22:
                    result = 19;
                    break;
                case 27:
                    if (this.missile_alert_state != 0) {
                        random = MyRandom.nextInt(0, 100);
                        if (random < 0 || random >= 15) {
                            if (random < 15 || random >= 17) {
                                if (random < 17 || random >= 25) {
                                    if (random >= 25) {
                                        result = 28;
                                        break;
                                    }
                                }
                                result = 21;
                                soundInstance.playSe(4);
                                break;
                            }
                            result = 19;
                            break;
                        }
                        result = 23;
                        soundInstance.playSe(11);
                        break;
                    }
                    random = MyRandom.nextInt(0, 15);
                    if (random < 0 || random >= 5) {
                        if (random < 5 || random >= 10) {
                            if (random >= 10) {
                                result = 21;
                                soundInstance.playSe(4);
                                break;
                            }
                        }
                        result = 19;
                        break;
                    }
                    result = 23;
                    soundInstance.playSe(11);
                    break;
                case 29:
                    if (this.missile_alert_state != 0) {
                        random = MyRandom.nextInt(0, 100);
                        if (random < 0 || random >= 10) {
                            if (random < 10 || random >= 20) {
                                if (random >= 20) {
                                    result = 28;
                                    break;
                                }
                            }
                            result = 21;
                            soundInstance.playSe(4);
                            break;
                        }
                        result = 23;
                        soundInstance.playSe(11);
                        break;
                    }
                    random = MyRandom.nextInt(0, 100);
                    if (random < 0 || random >= 45) {
                        if (random < 45 || random >= 55) {
                            if (random >= 55) {
                                result = 21;
                                soundInstance.playSe(4);
                                break;
                            }
                        }
                        result = 19;
                        break;
                    }
                    result = 23;
                    soundInstance.playSe(11);
                    break;
                case 32:
                    if (this.missile_alert_state != 0) {
                        random = MyRandom.nextInt(0, 100);
                        if (random < 0 || random >= 25) {
                            if (random < 25 || random >= 50) {
                                if (random >= 50) {
                                    result = 28;
                                    break;
                                }
                            }
                            result = 21;
                            soundInstance.playSe(4);
                            break;
                        }
                        result = 23;
                        soundInstance.playSe(11);
                        break;
                    }
                    random = MyRandom.nextInt(0, 100);
                    if (random < 0 || random >= 50) {
                        if (random >= 50) {
                            result = 21;
                            soundInstance.playSe(4);
                            break;
                        }
                    }
                    result = 23;
                    soundInstance.playSe(11);
                    break;
                case 35:
                    if (this.missile_alert_state != 0) {
                        random = MyRandom.nextInt(0, 100);
                        if (random < 0 || random >= 12) {
                            if (random < 12 || random >= 25) {
                                if (random >= 25) {
                                    result = 28;
                                    break;
                                }
                            }
                            result = 21;
                            soundInstance.playSe(4);
                            break;
                        }
                        result = 23;
                        soundInstance.playSe(11);
                        break;
                    }
                    random = MyRandom.nextInt(0, 100);
                    if (random < 0 || random >= 25) {
                        if (random < 25 || random >= 30) {
                            if (random >= 30) {
                                result = 21;
                                soundInstance.playSe(4);
                                break;
                            }
                        }
                        result = 19;
                        break;
                    }
                    result = 23;
                    soundInstance.playSe(11);
                    break;
                default:
                    break;
            }
        }
        this.first_jump_cnt++;
        if (CanFreshFight()) {
            result = 12;
        } else {
            result = 7;
            soundInstance.playSe(11);
        }
        int tmpstate;
        switch (result) {
            case 4:
                if (this.IsHurt) {
                    tmpstate = 2;
                } else {
                    tmpstate = 1;
                }
                changeAniState(this.knuckdrawer, tmpstate, true);
                this.COLLISION_WIDTH = 1024;
                this.COLLISION_HEIGHT = Boss6Block.COLLISION2_HEIGHT;
                break;
            case 5:
                this.AttackStartDirection = this.posX - MIDDLE_SCREEN;
                if (MIDDLE_SCREEN > this.posX) {
                    if (this.horizon_move_speed < 0) {
                        this.horizon_move_speed = -this.horizon_move_speed;
                    }
                } else if (this.horizon_move_speed > 0) {
                    this.horizon_move_speed = -this.horizon_move_speed;
                }
                if (this.IsHurt) {
                    tmpstate = 9;
                } else {
                    tmpstate = 4;
                }
                changeAniState(this.knuckdrawer, tmpstate, true, this.AttackStartDirection);
                this.COLLISION_WIDTH = 1536;
                this.COLLISION_HEIGHT = 1536;
                break;
            case 7:
                if (this.IsHurt) {
                    tmpstate = 8;
                } else {
                    tmpstate = 3;
                }
                changeAniState(this.knuckdrawer, tmpstate, true);
                this.COLLISION_WIDTH = 1536;
                this.COLLISION_HEIGHT = 1536;
                this.flydefence.setHurtState(false);
                break;
            case 12:
                changeAniState(this.knuckdrawer, 18, false);
                this.COLLISION_WIDTH = 1536;
                this.COLLISION_HEIGHT = 1920;
                break;
            case 19:
                if (this.IsHurt) {
                    tmpstate = 20;
                } else {
                    tmpstate = 19;
                }
                changeAniState(this.knuckdrawer, tmpstate, true);
                this.COLLISION_WIDTH = 1024;
                this.COLLISION_HEIGHT = Boss6Block.COLLISION2_HEIGHT;
                break;
            case 21:
                this.AttackStartDirection = this.posX - MIDDLE_SCREEN;
                if (MIDDLE_SCREEN > this.posX) {
                    if (this.horizon_move_speed < 0) {
                        this.horizon_move_speed = -this.horizon_move_speed;
                    }
                } else if (this.horizon_move_speed > 0) {
                    this.horizon_move_speed = -this.horizon_move_speed;
                }
                if (this.IsHurt) {
                    tmpstate = 27;
                } else {
                    tmpstate = 22;
                }
                changeAniState(this.knuckdrawer, tmpstate, true, this.AttackStartDirection);
                this.COLLISION_WIDTH = 1536;
                this.COLLISION_HEIGHT = 1536;
                break;
            case 23:
                if (this.IsHurt) {
                    tmpstate = 26;
                } else {
                    tmpstate = 21;
                }
                changeAniState(this.knuckdrawer, tmpstate, true);
                this.COLLISION_WIDTH = 1536;
                this.COLLISION_HEIGHT = 1536;
                this.flydefence.setHurtState(false);
                break;
            case 28:
                if (this.IsHurt) {
                    tmpstate = 38;
                } else {
                    tmpstate = 36;
                }
                changeAniState(this.knuckdrawer, tmpstate, false);
                this.COLLISION_WIDTH = 1024;
                this.COLLISION_HEIGHT = Boss6Block.COLLISION2_HEIGHT;
                break;
        }
        return result;
    }

    private void hurt_side_cotrol() {
        if ((this.posX + this.velX) + this.BOSS5_WIDTH >= ((MapManager.getCamera().f13x + MapManager.CAMERA_WIDTH) << 6)) {
            this.posX += 0;
        } else if ((this.posX + this.velX) - this.BOSS5_WIDTH <= (MapManager.getCamera().f13x << 6)) {
            this.posX += 0;
        } else {
            this.posX += this.velX;
        }
    }

    private void hurt_air_control() {
        hurt_side_cotrol();
        this.velY += GRAVITY;
        this.posY += this.velY;
    }

    public void draw(MFGraphics g) {
        if (!this.dead) {
            if (this.state < 39) {
                drawInMap(g, this.knuckdrawer, this.posX, this.posY);
            }
            if ((this.HP == halfLifeNum() && this.state >= 13 && this.state <= 15) || (this.HP == 0 && this.state >= 36 && this.state <= 37)) {
                drawInMap(g, this.boomdrawer, this.boomX, this.boomY);
            }
            if (this.bossbroken != null) {
                this.bossbroken.draw(g);
            }
            if (this.state == 39) {
                for (int i = 0; i < this.pos.length; i++) {
                    this.knuckdrawer.setActionId(i + 41);
                    drawInMap(g, this.knuckdrawer, this.pos[i][0], this.pos[i][1]);
                }
            }
            if (this.state == 40) {
                drawInMap(g, this.boatdrawer, this.posX, this.posY);
                drawInMap(g, this.escapefacedrawer, this.posX, this.posY - Boss6Block.COLLISION2_HEIGHT);
            }
            this.flydefence.draw(g);
            drawCollisionRect(g);
        }
    }

    public void changeAniState(AnimationDrawer AniDrawer, int state, boolean isloop) {
        if (player.getFootPositionX() > this.posX) {
            AniDrawer.setActionId(state);
            AniDrawer.setTrans(2);
            AniDrawer.setLoop(isloop);
            if (this.velocity < 0) {
                this.velocity = -this.velocity;
            }
            if (this.fly_move_x_speed1 < 0) {
                this.fly_move_x_speed1 = -this.fly_move_x_speed1;
                return;
            }
            return;
        }
        AniDrawer.setActionId(state);
        AniDrawer.setTrans(0);
        AniDrawer.setLoop(isloop);
        if (this.velocity > 0) {
            this.velocity = -this.velocity;
        }
        if (this.fly_move_x_speed1 > 0) {
            this.fly_move_x_speed1 = -this.fly_move_x_speed1;
        }
    }

    public void changeAniState(AnimationDrawer AniDrawer, int state, boolean isloop, int direct) {
        if (direct < 0) {
            AniDrawer.setActionId(state);
            AniDrawer.setTrans(2);
            AniDrawer.setLoop(isloop);
            if (this.velocity < 0) {
                this.velocity = -this.velocity;
            }
            if (this.fly_move_x_speed1 < 0) {
                this.fly_move_x_speed1 = -this.fly_move_x_speed1;
                return;
            }
            return;
        }
        AniDrawer.setActionId(state);
        AniDrawer.setTrans(0);
        AniDrawer.setLoop(isloop);
        if (this.velocity > 0) {
            this.velocity = -this.velocity;
        }
        if (this.fly_move_x_speed1 > 0) {
            this.fly_move_x_speed1 = -this.fly_move_x_speed1;
        }
    }

    public void changeAniStateNoTrans(AnimationDrawer AniDrawer, int state, boolean isloop) {
        AniDrawer.setActionId(state);
        AniDrawer.setLoop(isloop);
        if (player.getFootPositionX() > this.posX) {
            if (this.velocity < 0) {
                this.velocity = -this.velocity;
            }
            if (this.fly_move_x_speed1 < 0) {
                this.fly_move_x_speed1 = -this.fly_move_x_speed1;
                return;
            }
            return;
        }
        if (this.velocity > 0) {
            this.velocity = -this.velocity;
        }
        if (this.fly_move_x_speed1 > 0) {
            this.fly_move_x_speed1 = -this.fly_move_x_speed1;
        }
    }

    public void refreshCollisionRect(int x, int y) {
        if (this.state != 8 && this.state != 24) {
            this.collisionRect.setRect(x - (this.COLLISION_WIDTH >> 1), y - this.COLLISION_HEIGHT, this.COLLISION_WIDTH, this.COLLISION_HEIGHT);
        } else if (this.AttackStartDirection > 0) {
            this.collisionRect.setRect(x - 1408, y - this.COLLISION_HEIGHT, this.COLLISION_WIDTH, this.COLLISION_HEIGHT);
        } else {
            this.collisionRect.setRect(x + 448, y - this.COLLISION_HEIGHT, this.COLLISION_WIDTH, this.COLLISION_HEIGHT);
        }
    }

    public void close() {
        this.knuckdrawer = null;
        this.boomdrawer = null;
        this.boatdrawer = null;
        this.escapefacedrawer = null;
        this.bossbroken = null;
        this.flydefence = null;
    }
}
