package SonicGBA;

import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: GimmickObject */
class ShimaSting extends GimmickObject {
    private static final int ATTACK_HEIGHT = 576;
    private static final int COLLISION_WIDTH = 3072;
    private static final int OFFSET_HEIGHT = 1024;
    private static final int STATE_BOTTOM = 2;
    private static final int STATE_NONE = 0;
    private static final int STATE_TOP = 1;
    private int attackState = 0;

    protected ShimaSting(int x, int y) {
        super(0, x, y, 0, 0, 0, 0);
        this.posX = x;
        this.posY = y;
        this.attackState = 0;
    }

    public void logic(int x, int y, int frame) {
        int preX = this.posX;
        int preY = this.posY;
        this.posX = x;
        this.posY = y;
        if (frame == 0) {
            this.attackState = 1;
        } else if (frame == 4) {
            this.attackState = 2;
        } else {
            this.attackState = 0;
        }
        refreshCollisionRect(this.posX, this.posY);
        checkWithPlayer(preX, preY, this.posX, this.posY);
    }

    public void draw(MFGraphics g) {
        drawCollisionRect(g);
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (object == player && this.attackState != 0) {
            object.beHurt();
        }
    }

    public void refreshCollisionRect(int x, int y) {
        if (this.attackState != 0) {
            this.collisionRect.setRect(x - 1536, (this.attackState == 1 ? -1600 : 1024) + y, 3072, ATTACK_HEIGHT);
        } else {
            this.collisionRect.setRect(x, y, 1, 1);
        }
    }
}
