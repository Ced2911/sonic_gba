package SonicGBA;

import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: EnemyObject */
class Boss3Shadow extends EnemyObject {
    private static final int COLLISION_HEIGHT = 3200;
    private static final int COLLISION_WIDTH = 3200;
    private static final int FACE_HURT = 2;
    private static final int FACE_NORMAL = 0;
    private static final int FACE_SMILE = 1;
    public boolean IsInPipeCollision;
    public boolean IsOver;
    private int face_state;

    protected Boss3Shadow(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        this.IsInPipeCollision = false;
        this.IsInPipeCollision = false;
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (!this.IsOver && !this.IsInPipeCollision && object == player) {
            player.beHurt();
            this.face_state = 1;
        }
    }

    public void doWhileBeAttack(PlayerObject object, int direction, int animationID) {
    }

    public void logic() {
    }

    public void logic(int posx, int posy) {
        this.posX = posx;
        this.posY = posy;
        int preX = this.posX;
        int preY = this.posY;
        refreshCollisionRect(this.posX >> 6, this.posY >> 6);
        checkWithPlayer(preX, preY, this.posX, this.posY);
    }

    public int getShadowHurt() {
        return this.face_state;
    }

    public void setShadowHurt(int state) {
        this.face_state = state;
    }

    public void draw(MFGraphics g) {
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 1600, y - 1600, 3200, 3200);
    }
}
