package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: EnemyObject */
class Boom extends EnemyObject {
    private static final int COLLISION_HEIGHT = 64;
    private static final int COLLISION_WIDTH = 64;
    private AnimationDrawer boomdrawer;

    public static void releaseAllResource() {
        Animation.closeAnimation(BoomAni);
        BoomAni = null;
    }

    protected Boom(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        if (BoomAni == null) {
            BoomAni = new Animation("/animation/boom");
        }
        this.boomdrawer = BoomAni.getDrawer(0, false, 0);
        this.posX = x;
        this.posY = y;
    }

    public void doWhileCollision(PlayerObject object, int direction) {
    }

    public void doWhileBeAttack(PlayerObject object, int direction, int animationID) {
    }

    public void logic() {
        refreshCollisionRect(this.posX, this.posY);
        if (this.boomdrawer.checkEnd()) {
            this.dead = true;
        }
    }

    public void draw(MFGraphics g) {
        if (!this.dead) {
            drawInMap(g, this.boomdrawer, this.posX, this.posY);
        }
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 32, y - 32, 64, 64);
    }

    public void close() {
        this.boomdrawer = null;
    }
}
