package SonicGBA;

import Lib.Animation;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: BulletObject */
class MonkeyBullet extends BulletObject {
    private static final int COLLISION_BOOM_HEIGHT = 1600;
    private static final int COLLISION_BOOM_WIDTH = 1600;
    private static final int COLLISION_HEIGHT = 832;
    private static final int COLLISION_WIDTH = 640;
    private static final int STATE_BOOM = 1;
    private static final int STATE_DROP = 0;
    private static final int boom_cnt_max = 10;
    private static final int drop_cnt_max = 2;
    private int boom_cnt;
    private int drop_cnt;
    private boolean isboom;
    private boolean isbooming;
    private int state;

    protected MonkeyBullet(int x, int y, int velX, int velY) {
        super(x, y, velX, velY, true);
        if (monkeybulletAnimation == null) {
            monkeybulletAnimation = new Animation("/animation/monkey_bullet");
        }
        this.drawer = monkeybulletAnimation.getDrawer(0, true, 0);
        this.isboom = false;
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (object == player && player.canBeHurt()) {
            player.beHurt();
            this.state = 1;
            this.boom_cnt = 10;
        }
    }

    public void bulletLogic() {
        int preX = this.posX;
        int preY = this.posY;
        switch (this.state) {
            case 0:
                this.isbooming = false;
                this.drawer.setActionId(0);
                this.drawer.setLoop(true);
                this.boom_cnt = 0;
                this.posX += this.velX;
                this.velY += GRAVITY;
                this.posY += this.velY;
                if (this.posY + 416 >= getGroundY(this.posX, this.posY)) {
                    this.posY = getGroundY(this.posX, this.posY) - 416;
                    switch (this.drop_cnt) {
                        case 0:
                            this.velY = -450;
                            this.drop_cnt = 1;
                            break;
                        case 1:
                            this.velY = SmallAnimal.FLY_VELOCITY_Y;
                            this.drop_cnt = 2;
                            break;
                        case 2:
                            this.state = 1;
                            break;
                    }
                }
                checkWithPlayer(preX, preY, this.posX, this.posY);
                return;
            case 1:
                if (this.boom_cnt < 10) {
                    this.boom_cnt++;
                } else {
                    this.drawer.setActionId(1);
                    this.drawer.setLoop(false);
                    this.isbooming = true;
                }
                if (this.drawer.checkEnd() && this.isbooming) {
                    this.isboom = true;
                }
                checkWithPlayer(preX, preY, this.posX, this.posY);
                return;
            default:
                return;
        }
    }

    public boolean chkDestroy() {
        if (this.isbooming) {
            return this.isboom;
        }
        return super.chkDestroy();
    }

    public void draw(MFGraphics g) {
        if (!this.isboom) {
            drawInMap(g, this.drawer);
        }
        this.collisionRect.draw(g, camera);
    }

    public void refreshCollisionRect(int x, int y) {
        if (this.isbooming) {
            this.collisionRect.setRect(x - 800, y - 800, 1600, 1600);
        } else {
            this.collisionRect.setRect(x - 320, y - 416, 640, COLLISION_HEIGHT);
        }
    }
}
