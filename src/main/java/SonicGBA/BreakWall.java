package SonicGBA;

import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;
import java.lang.reflect.Array;

/* compiled from: GimmickObject */
class BreakWall extends GimmickObject {
    private static final int BREAK_NUM_HEIGHT = 9;
    private static final int BREAK_NUM_WIDTH = 4;
    private static final int BREAK_WIDTH = 448;
    private static final int COLLISION_HEIGHT = 4096;
    private static final int COLLISION_WIDTH = 2048;
    private static final int DRAW_BREAK_WIDTH = 7;
    private static MFImage image;
    private int breakCount;
    private int breakLimitLine;
    private boolean breakOver = false;
    private int[][][] breakPosition;
    private boolean breaking = false;
    private boolean positiveDirection;

    protected BreakWall(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        this.posX -= this.iLeft == 0 ? 0 : 2048;
        this.breakPosition = (int[][][]) Array.newInstance(Integer.TYPE, new int[]{4, 9, 3});
        for (int w = 0; w < 4; w++) {
            for (int h = 0; h < 9; h++) {
                this.breakPosition[w][h][0] = this.posX + (w * BREAK_WIDTH);
                this.breakPosition[w][h][1] = this.posY + (h * BREAK_WIDTH);
                this.breakPosition[w][h][2] = -GRAVITY;
            }
        }
        this.breakLimitLine = (this.posY + 4096) + 2048;
        if (image == null) {
            try {
                image = MFImage.createImage("/gimmick/breakWall_" + StageManager.getCurrentZoneId() + ".png");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (object.isAttackingEnemy()) {
            doWhileBeAttack(object, direction, 0);
        } else if (!this.breaking) {
            object.beStop(0, direction, this);
        }
    }

    public void doWhileBeAttack(PlayerObject object, int direction, int animationID) {
        this.breaking = false;
        if (null != null) {
            this.breakCount = 0;
        }
        this.breaking = true;
        this.breakOver = false;
        if (direction == 3) {
            this.positiveDirection = true;
        }
        if (direction == 2) {
            this.positiveDirection = false;
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void logic() {
        /*
        r9 = this;
        r8 = 2;
        r7 = 1;
        r6 = 0;
        r3 = r9.breaking;
        if (r3 == 0) goto L_0x002a;
    L_0x0007:
        r3 = r9.breakOver;
        if (r3 != 0) goto L_0x002a;
    L_0x000b:
        r0 = 0;
        r9.breakOver = r7;
        r3 = IsGamePause;
        if (r3 != 0) goto L_0x001e;
    L_0x0012:
        r3 = r9.positiveDirection;
        if (r3 == 0) goto L_0x002b;
    L_0x0016:
        r2 = r6;
    L_0x0017:
        r3 = r9.positiveDirection;
        if (r3 == 0) goto L_0x009a;
    L_0x001b:
        r3 = 4;
        if (r2 < r3) goto L_0x002e;
    L_0x001e:
        r3 = r9.breakCount;
        r4 = 36;
        if (r3 >= r4) goto L_0x002a;
    L_0x0024:
        r3 = r9.breakOver;
        if (r3 == 0) goto L_0x002a;
    L_0x0028:
        r9.breakOver = r6;
    L_0x002a:
        return;
    L_0x002b:
        r3 = 3;
        r2 = r3;
        goto L_0x0017;
    L_0x002e:
        r1 = 8;
    L_0x0030:
        if (r1 >= 0) goto L_0x0039;
    L_0x0032:
        r3 = r9.positiveDirection;
        if (r3 == 0) goto L_0x0098;
    L_0x0036:
        r3 = r7;
    L_0x0037:
        r2 = r2 + r3;
        goto L_0x0017;
    L_0x0039:
        r3 = r9.breakPosition;
        r3 = r3[r2];
        r3 = r3[r1];
        r4 = r3[r6];
        r5 = r9.positiveDirection;
        if (r5 == 0) goto L_0x0092;
    L_0x0045:
        r5 = 50;
    L_0x0047:
        r4 = r4 + r5;
        r3[r6] = r4;
        r3 = r9.breakPosition;
        r3 = r3[r2];
        r3 = r3[r1];
        r4 = r3[r8];
        r5 = GRAVITY;
        r4 = r4 + r5;
        r3[r8] = r4;
        r3 = r9.breakPosition;
        r3 = r3[r2];
        r3 = r3[r1];
        r4 = r3[r7];
        r5 = r9.breakPosition;
        r5 = r5[r2];
        r5 = r5[r1];
        r5 = r5[r8];
        r4 = r4 + r5;
        r3[r7] = r4;
        r3 = r9.breakPosition;
        r3 = r3[r2];
        r3 = r3[r1];
        r3 = r3[r7];
        r4 = r9.breakLimitLine;
        if (r3 >= r4) goto L_0x0078;
    L_0x0076:
        r9.breakOver = r6;
    L_0x0078:
        r0 = r0 + 1;
        r3 = r9.breakCount;
        if (r3 != 0) goto L_0x0087;
    L_0x007e:
        r3 = Lib.SoundSystem.getInstance();
        r4 = 45;
        r3.playSe(r4);
    L_0x0087:
        r3 = r9.breakCount;
        if (r0 < r3) goto L_0x0095;
    L_0x008b:
        r3 = r9.breakCount;
        r3 = r3 + 1;
        r9.breakCount = r3;
        goto L_0x001e;
    L_0x0092:
        r5 = -50;
        goto L_0x0047;
    L_0x0095:
        r1 = r1 + -1;
        goto L_0x0030;
    L_0x0098:
        r3 = -1;
        goto L_0x0037;
    L_0x009a:
        if (r2 >= 0) goto L_0x002e;
    L_0x009c:
        goto L_0x001e;
        */
        throw new UnsupportedOperationException("Method not decompiled: SonicGBA.BreakWall.logic():void");
    }

    public void draw(MFGraphics g) {
        drawCollisionRect(g);
        if (!this.breakOver) {
            if (this.breaking) {
                for (int w = 0; w < 4; w++) {
                    for (int h = 0; h < 9; h++) {
                        drawInMap(g, image, w * 7, h * 7, 7, 7, 0, this.breakPosition[w][h][0], this.breakPosition[w][h][1], 20);
                    }
                }
                return;
            }
            drawInMap(g, image, 20);
        }
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x, y, 2048, 4096);
    }

    public void close() {
        this.breakPosition = null;
    }

    public static void releaseAllResource() {
        image = null;
    }

    public void doInitWhileInCamera() {
        this.breaking = false;
        this.breakOver = false;
        this.breakCount = 0;
        for (int w = 0; w < 4; w++) {
            for (int h = 0; h < 9; h++) {
                this.breakPosition[w][h][0] = this.posX + (w * BREAK_WIDTH);
                this.breakPosition[w][h][1] = this.posY + (h * BREAK_WIDTH);
                this.breakPosition[w][h][2] = -GRAVITY;
            }
        }
    }
}
