package SonicGBA;

import Lib.Animation;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: BulletObject */
class BatBullet extends BulletObject {
    private static final int COLLISION_HEIGHT = 1536;
    private static final int COLLISION_WIDTH = 1536;
    private boolean isboom;

    protected BatBullet(int x, int y, int velX, int velY) {
        super(x, y, velX, velY, false);
        if (batbulletAnimation == null) {
            batbulletAnimation = new Animation("/animation/bat_bullet");
        }
        this.drawer = batbulletAnimation.getDrawer(0, true, 0);
        this.isboom = false;
    }

    public void bulletLogic() {
        int preX = this.posX;
        int preY = this.posY;
        this.velY += GRAVITY;
        this.posY += this.velY;
        if (this.posY >= getGroundY(this.posX, this.posY) - 768) {
            this.posY = getGroundY(this.posX, this.posY) - 768;
            this.drawer.setActionId(1);
            this.drawer.setLoop(false);
            this.isboom = true;
            this.velY = 0;
        } else if (IsHitted()) {
            this.drawer.setActionId(1);
            this.drawer.setLoop(false);
            this.isboom = true;
            this.velY = 20;
        }
        checkWithPlayer(preX, preY, this.posX, this.posY);
    }

    public boolean chkDestroy() {
        return super.chkDestroy() || this.drawer.checkEnd();
    }

    public void draw(MFGraphics g) {
        if (!this.drawer.checkEnd()) {
            drawInMap(g, this.drawer);
        }
        this.collisionRect.draw(g, camera);
    }

    public void refreshCollisionRect(int x, int y) {
        if (!this.isboom) {
            this.collisionRect.setRect(x - 768, y - 768, 1536, 1536);
        }
    }
}
