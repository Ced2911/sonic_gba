package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import Special.SSDef;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: EnemyObject */
class Bonus100pts extends EnemyObject {
    private static Animation BonusAnimation = null;
    private static final int COLLISION_HEIGHT = 64;
    private static final int COLLISION_WIDTH = 64;
    private AnimationDrawer bonusdrawer;
    private int frame;
    private int movement = SSDef.PLAYER_MOVE_HEIGHT;

    protected Bonus100pts(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        if (BonusAnimation == null) {
            BonusAnimation = new Animation("/animation/100pts");
        }
        this.bonusdrawer = BonusAnimation.getDrawer(0, false, 0);
        this.frame = 0;
        this.posX = x;
        this.posY = y;
    }

    public void logic() {
        if (!this.dead) {
            if (this.frame > 24) {
                this.dead = true;
            }
            this.frame++;
            if (this.frame < 16) {
                this.posY -= this.movement;
            }
            refreshCollisionRect(this.posX, this.posY);
        }
    }

    public void draw(MFGraphics g) {
        if (!this.dead) {
            drawInMap(g, this.bonusdrawer, this.posX, this.posY);
        }
    }

    public void doWhileCollision(PlayerObject object, int direction) {
    }

    public void doWhileBeAttack(PlayerObject object, int direction, int animationID) {
    }

    public void close() {
        this.bonusdrawer = null;
    }

    public static void releaseAllResource() {
        Animation.closeAnimation(BonusAnimation);
        BonusAnimation = null;
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 32, y - 32, 64, 64);
    }
}
