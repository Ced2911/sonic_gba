package SonicGBA;

import Lib.MyAPI;
import com.sega.mobile.define.MDPhone;

/* compiled from: GimmickObject */
class MoveCalculator {
    private static final int DEGREE_VELOCITY = 5;
    private static final int HALF_MOVE_TIME = 17;
    private static final int MOVE_TIME = 34;
    private static int degree;
    public static boolean direction;
    private static boolean isSide = false;
    private static int moveCount;
    private static int moveCount2;
    private int centerPosition;
    private int position;
    private int radius;
    private boolean ratio;

    public static void staticLogic() {
        if (direction) {
            isSide = false;
            moveCount2++;
            if (moveCount2 > 34) {
                moveCount2 = 34;
                direction = false;
                isSide = true;
            }
        } else {
            isSide = false;
            moveCount2--;
            if (moveCount2 < 0) {
                moveCount2 = 0;
                direction = true;
                isSide = true;
            }
        }
        moveCount = moveCount2 - 17;
        degree += 5;
        degree %= MDPhone.SCREEN_WIDTH;
    }

    public boolean getSide() {
        return degree == 90 || degree == 270;
    }

    public MoveCalculator(int centerPosition, int radius, boolean ratio) {
        this.centerPosition = centerPosition;
        this.radius = radius;
        this.ratio = ratio;
        this.position = centerPosition;
        isSide = false;
    }

    public void logic() {
    }

    public int getPosition() {
        return this.centerPosition + ((MyAPI.dSin(degree) * this.radius) / 100);
    }
}
