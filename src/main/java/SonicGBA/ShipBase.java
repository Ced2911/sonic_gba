package SonicGBA;

import com.sega.mobile.define.MDPhone;
import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;

/* compiled from: GimmickObject */
class ShipBase extends PlatformObject {
    private static final int COLLISION_HEIGHT = 1280;
    private static final int COLLISION_WIDTH = 1280;
    private static MFImage shipBaseImage;

    protected ShipBase(int x, int y) {
        super(x, y);
        if (shipBaseImage == null) {
            try {
                shipBaseImage = MFImage.createImage("/gimmick/ship_base.png");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        refreshCollisionRect(this.posX, this.posY);
    }

    public void draw(MFGraphics g) {
        if (shipBaseImage != null) {
            drawInMap(g, shipBaseImage, 3);
            drawCollisionRect(g);
        }
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - MDPhone.SCREEN_HEIGHT, y - MDPhone.SCREEN_HEIGHT, 1280, 1280);
    }

    public void close() {
    }

    public static void releaseAllResource() {
        shipBaseImage = null;
    }
}
