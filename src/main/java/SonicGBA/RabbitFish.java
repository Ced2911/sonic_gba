package SonicGBA;

import Lib.Animation;
import com.sega.mobile.framework.device.MFGamePad;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: EnemyObject */
class RabbitFish extends EnemyObject {
    private static final int ALERT_RANGE = 80;
    private static final int COLLISION_HEIGHT = 1536;
    private static final int COLLISION_WIDTH = 1536;
    private static final int STATE_DEFEND = 1;
    private static final int STATE_MOVE = 0;
    private static Animation fishAnimation;
    private int alert_state;
    private int defend_cnt;
    private int defend_frame;
    private int limitLeftX;
    private int limitRightX;
    private int move_cnt;
    private int starty;
    private int state;
    private int velocity;

    public static void releaseAllResource() {
        Animation.closeAnimation(fishAnimation);
        fishAnimation = null;
    }

    protected RabbitFish(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y - 8, left, top, width, height);
        this.velocity = 140;
        this.move_cnt = 0;
        this.defend_cnt = 0;
        this.defend_frame = 16;
        this.mWidth = MFGamePad.KEY_NUM_6;
        this.limitLeftX = this.posX;
        this.limitRightX = this.posX + this.mWidth;
        this.posX += this.mWidth >> 1;
        this.starty = this.posY;
        if (fishAnimation == null) {
            fishAnimation = new Animation("/animation/rabbit_fish");
        }
        this.drawer = fishAnimation.getDrawer(0, true, 0);
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (this.dead || object != player) {
            return;
        }
        if (this.state != 0) {
            player.beHurt();
        } else if ((player instanceof PlayerSonic) && (player.getCharacterAnimationID() == 4 || player.getCharacterAnimationID() == 5 || player.getCharacterAnimationID() == 6)) {
            player.doAttackPose(this, direction);
            beAttack();
        } else if ((player instanceof PlayerKnuckles) && (player.getCharacterAnimationID() == 4 || player.getCharacterAnimationID() == 5 || player.getCharacterAnimationID() == 6)) {
            player.doAttackPose(this, direction);
            beAttack();
        } else if ((player instanceof PlayerTails) && (player.getCharacterAnimationID() == 5 || player.getCharacterAnimationID() == 6 || player.getCharacterAnimationID() == 4)) {
            player.doAttackPose(this, direction);
            beAttack();
        } else {
            player.beHurt();
        }
    }

    public void doWhileBeAttack(PlayerObject object, int direction, int animationID) {
        if (this.dead || object != player || this.state != 0) {
            return;
        }
        if (player.isAttackingEnemy()) {
            player.doAttackPose(this, direction);
            beAttack();
            return;
        }
        player.beHurt();
    }

    public boolean IsFacetoPlayer() {
        if ((this.posX <= player.getFootPositionX() || this.velocity >= 0) && (this.posX >= player.getFootPositionX() || this.velocity <= 0)) {
            return false;
        }
        return true;
    }

    public void logic() {
        if (!this.dead) {
            int preX = this.posX;
            int preY = this.posY;
            switch (this.state) {
                case 0:
                    this.alert_state = checkPlayerInEnemyAlertRange(this.posX >> 6, this.posY >> 6, 80, 80);
                    if (this.velocity > 0) {
                        this.posX += this.velocity;
                        this.move_cnt += Math.abs(this.velocity);
                        this.drawer.setActionId(0);
                        this.drawer.setTrans(2);
                        if (this.posX >= this.limitRightX) {
                            this.posX = this.limitRightX;
                            this.velocity = -this.velocity;
                        }
                    } else {
                        this.posX += this.velocity;
                        this.move_cnt += Math.abs(this.velocity);
                        this.drawer.setActionId(0);
                        this.drawer.setTrans(0);
                        if (this.posX <= this.limitLeftX) {
                            this.posX = this.limitLeftX;
                            this.velocity = -this.velocity;
                        }
                    }
                    if (this.alert_state == 0 && IsFacetoPlayer() && this.move_cnt >= 5120) {
                        this.state = 1;
                        this.drawer.setActionId(1);
                        this.defend_cnt = 0;
                        this.move_cnt = 0;
                        if (this.velocity > 0) {
                            this.drawer.setTrans(2);
                        } else {
                            this.drawer.setTrans(0);
                        }
                    }
                    checkWithPlayer(preX, preY, this.posX, this.posY);
                    return;
                case 1:
                    if (this.defend_cnt < this.defend_frame) {
                        this.defend_cnt++;
                    } else {
                        this.state = 0;
                        this.drawer.setActionId(0);
                        if (this.velocity > 0) {
                            this.drawer.setTrans(2);
                        } else {
                            this.drawer.setTrans(0);
                        }
                    }
                    checkWithPlayer(preX, preY, this.posX, this.posY);
                    return;
                default:
                    return;
            }
        }
    }

    public void draw(MFGraphics g) {
        if (!this.dead) {
            drawInMap(g, this.drawer);
            drawCollisionRect(g);
        }
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 768, y - 768, 1536, 1536);
    }
}
