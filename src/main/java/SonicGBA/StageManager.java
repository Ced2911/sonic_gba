package SonicGBA;

import GameEngine.Key;
import Lib.MyAPI;
import Lib.Record;
import Lib.SoundSystem;
import State.GameState;
import State.SpecialStageState;
import State.State;
import State.StringIndex;
import State.TitleState;
import com.sega.mobile.define.MDPhone;
import com.sega.mobile.framework.device.MFGraphics;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;

public class StageManager implements SonicDef {
    private static final int CHARACTER_NUM = 4;
    private static final int HIGH_SCORE_NUM = 5;
    private static final int HIGH_SCORE_OFFSET_X = 0;
    private static final int HIGH_SCORE_Y = (FONT_H_HALF + 44);
    private static final int HIGH_SCORE_Y_TMP = ((SCREEN_HEIGHT - (MENU_SPACE * 5)) >> 1);
    public static boolean IsCalculateScore = false;
    private static final int LOAD_ANIMAL = 10;
    private static final int LOAD_BACKGROUND = 4;
    private static final int LOAD_COLLISION = 3;
    private static final int LOAD_ENEMY = 8;
    private static final int LOAD_GAME_INIT = 13;
    private static final int LOAD_GAME_LOGIC = 11;
    private static final int LOAD_GIMMICK = 6;
    private static final int LOAD_ITEM = 9;
    private static final int LOAD_MAP = 2;
    private static final int LOAD_OBJ_INIT = 5;
    private static final int LOAD_RELEASE_MEMORY = 0;
    private static final int LOAD_RELEASE_MEMORY_2 = 1;
    private static final int LOAD_RING = 7;
    private static final int LOAD_SE = 12;
    private static final int MOVING_SPACE = 2;
    public static final int[] MUSIC_ID_HIGH = new int[]{6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 20};
    public static final int[] MUSIC_ID = MUSIC_ID_HIGH;
    public static final int[] MUSIC_ID_LOW = new int[]{6, 7, 8, 9, 10, 11, 12, 13};
    public static final int[][] PLAYER_START = new int[][]{new int[]{96, 448}, new int[]{96, 690}, new int[]{96, StringIndex.STR_RESET_RECORD}, new int[]{96, 1600}, new int[]{96, 160}, new int[]{96, MDPhone.SCREEN_HEIGHT}, new int[]{100, 200}, new int[]{80, 868}, new int[]{96, 832}, new int[]{96, 928}, new int[]{106, 6988}, new int[]{100, 1732}, new int[]{100, 771}, new int[]{56, RollPlatformSpeedA.DEGREE_VELOCITY}};
    private static final String[] RANK_STR_FOR_EN = new String[]{"1st", "2nd", "3rd", "4th", "5th"};
    private static final int RECORD_NUM = 3;
    private static final int STAGE_4_1_WATER_LEVEL = 1548;
    private static final int STAGE_4_2_WATER_LEVEL = 1667;
    private static final int STAGE_GAMEOVER_FRAME = 10;
    public static final String[] STAGE_NAME_HIGH = new String[]{"1_1", "1_2", "2_1", "2_2", "3_1", "3_2", "4_1", "4_2", "5_1", "5_2", "6_1", "6_2", "final", "EX"};
    public static final String[] STAGE_NAME = STAGE_NAME_HIGH;
    public static final int[] STAGE_NAME_ID = new int[]{86, 87, 88, 89, 89, 89, 89, 89};
    public static final String[] STAGE_NAME_LOW_FOUR = new String[]{"1_1", "1_2", "2_1", "2_2"};
    public static final String[] STAGE_NAME_LOW_SIX = new String[]{"1_1", "1_2", "2_1", "2_2", "5_1", "5_2"};
    private static final String[] STAGE_NAME_LOW = STAGE_NAME_LOW_SIX;
    public static final int STAGE_NUM = STAGE_NAME.length;
    private static final int STAGE_PASS_FRAME = 0;
    private static final int STAGE_RESTART_FRAME = 10;
    private static final int STAGE_TIMEOVER_FRAME = 0;
    public static final int[] ZOME_ID;
    public static int characterFromGame = -1;
    public static int checkCameraDownX;
    public static boolean checkCameraEnable;
    public static int checkCameraLeftX;
    public static int checkCameraRightX;
    public static int checkCameraUpX;
    public static boolean checkPointEnable;
    public static int checkPointTime;
    public static int checkPointX;
    public static int checkPointY;
    private static int drawNewScore = -1;
    private static int[] highScore = new int[5];
    public static boolean isContinueGame;
    public static boolean isNextGameStageDirectedly = false;
    public static boolean isOnlyScoreCal = false;
    public static boolean isOnlyStagePass = false;
    private static boolean isRacing = false;
    public static boolean isSaveTimeModeScore = false;
    public static boolean isScoreBarOutOfScreen = false;
    private static int loadStep = 0;
    private static int movingCount = 0;
    private static int movingRow = 0;
    private static int[] normalStageIDArray = new int[4];
    private static int normalStageId;
    private static int[] openedStageIDArray = new int[4];
    private static int openedStageId = 0;
    private static int[] preStageIDArray = new int[4];
    private static int preStageId = -1;
    private static int[] rankingOffsetX = new int[5];
    public static int specialStagePointX;
    public static int specialStagePointY;
    private static int stageGameoverCount;
    private static boolean stageGameoverFlag;
    private static int[] stageIDArray = new int[4];
    public static int stageIDFromGame = -1;
    private static int stageId = 0;
    private static int stagePassCount;
    private static boolean stagePassFlag;
    private static int stageRestartCount;
    private static boolean stageRestartFlag;
    private static int stageTimeoverCount;
    private static boolean stageTimeoverFlag;
    private static int startStageID;
    private static int[] startStageIDArray = new int[4];
    private static int[] timeModeScore = new int[((STAGE_NUM * 3) * 4)];
    private static int waterLevel = STAGE_4_1_WATER_LEVEL;

    static {
        int[] iArr = new int[14];
        iArr[2] = 1;
        iArr[3] = 1;
        iArr[4] = 2;
        iArr[5] = 2;
        iArr[6] = 3;
        iArr[7] = 3;
        iArr[8] = 4;
        iArr[9] = 4;
        iArr[10] = 5;
        iArr[11] = 5;
        iArr[12] = 6;
        iArr[13] = 7;
        ZOME_ID = iArr;
    }

    public static boolean loadStageStep() {
        boolean nextStep = true;
        switch (loadStep) {
            case LOAD_RELEASE_MEMORY:
                SoundSystem.getInstance().stopBgm(true);
                MapManager.closeMap();
                CollisionMap.getInstance().closeMap();
                Key.touchkeygameboardClose();
                RocketSeparateEffect.getInstance().close();
                Key.touchGamePauseClose();
                State.isDrawTouchPad = false;
                PlayerObject.isNeedPlayWaterSE = false;
                SoundSystem.getInstance().setSoundSpeed(1.0f);
                break;
            case LOAD_RELEASE_MEMORY_2:
                boolean z;
                if (preStageIDArray[PlayerObject.getCharacterID()] == stageIDArray[PlayerObject.getCharacterID()]) {
                    z = true;
                } else {
                    z = false;
                }
                nextStep = GameObject.closeObjectStep(z);
                break;
            case LOAD_MAP:
                PlayerObject.initStageParam();
                if (getCurrentZoneId() == 4) {
                    if (stageIDArray[PlayerObject.getCharacterID()] % 2 == 0) {
                        setWaterLevel(STAGE_4_1_WATER_LEVEL);
                    } else {
                        setWaterLevel(STAGE_4_2_WATER_LEVEL);
                    }
                }
                nextStep = MapManager.loadMapStep(stageIDArray[PlayerObject.getCharacterID()], STAGE_NAME[stageIDArray[PlayerObject.getCharacterID()]]);
                break;
            case LOAD_COLLISION:
                nextStep = CollisionMap.getInstance().loadCollisionInfoStep(STAGE_NAME[stageIDArray[PlayerObject.getCharacterID()]]);
                break;
            case LOAD_BACKGROUND:
                BackGroundManager.init(stageIDArray[PlayerObject.getCharacterID()]);
                break;
            case LOAD_OBJ_INIT:
                GameObject.initObject(MapManager.getPixelWidth(), MapManager.getPixelHeight(), preStageIDArray[PlayerObject.getCharacterID()] == stageIDArray[PlayerObject.getCharacterID()]);
                preStageIDArray[PlayerObject.getCharacterID()] = stageIDArray[PlayerObject.getCharacterID()];
                if (!GameState.isBackFromSpStage) {
                    if (!stageRestartFlag) {
                        checkPointEnable = false;
                        checkCameraEnable = false;
                    }
                    if (stageRestartFlag && checkPointEnable) {
                        GameObject.setPlayerPosition(checkPointX, checkPointY);
                        PlayerObject.timeCount = checkPointTime;
                    } else {
                        GameObject.setPlayerPosition(PLAYER_START[stageIDArray[PlayerObject.getCharacterID()]][0], PLAYER_START[stageIDArray[PlayerObject.getCharacterID()]][1]);
                        PlayerObject.doInitInNewStage();
                    }
                    if (checkCameraEnable && stageRestartFlag && PlayerObject.stageModeState != 1) {
                        MapManager.setCameraUpLimit(checkCameraUpX);
                        MapManager.setCameraDownLimit(checkCameraDownX);
                        MapManager.setCameraLeftLimit(checkCameraLeftX);
                        MapManager.setCameraRightLimit(checkCameraRightX);
                        MapManager.calCameraImmidiately();
                        break;
                    }
                }
                GameObject.setPlayerPosition(specialStagePointX, specialStagePointY);
                break;
            case LOAD_GIMMICK:
                nextStep = GameObject.loadObjectStep("/map/" + STAGE_NAME[stageIDArray[PlayerObject.getCharacterID()]] + ".gi", 0);
                break;
            case LOAD_RING:
                nextStep = GameObject.loadObjectStep("/map/" + STAGE_NAME[stageIDArray[PlayerObject.getCharacterID()]] + ".ri", 1);
                break;
            case LOAD_ENEMY:
                nextStep = GameObject.loadObjectStep("/map/" + STAGE_NAME[stageIDArray[PlayerObject.getCharacterID()]] + ".en", 2);
                if (getCurrentZoneId() == 8) {
                    EnemyObject enemy = EnemyObject.getNewInstance(36, 0, 0, 0, 0, 0, 0);
                    if (!(enemy == null || EnemyObject.IsBoss)) {
                        GameObject.addGameObject(enemy);
                    }
                    nextStep = true;
                    break;
                }
                break;
            case LOAD_ITEM:
                nextStep = GameObject.loadObjectStep("/map/" + STAGE_NAME[stageIDArray[PlayerObject.getCharacterID()]] + ".it", 3);
                Key.clear();
                stagePassFlag = false;
                stageRestartFlag = false;
                stageGameoverFlag = false;
                stageTimeoverFlag = false;
                break;
            case LOAD_ANIMAL:
                SmallAnimal.animalInit();
                MapManager.focusQuickLocation();
                break;
            case LOAD_GAME_LOGIC:
                nextStep = true;
                Key.clear();
                GameObject.logicObjects();
                break;
            case LOAD_SE:
                GameObject.isDamageSandActive = false;
                SoundSystem.getInstance().preLoadAllSe();
                break;
            case LOAD_GAME_INIT:
                SoundSystem.getInstance().playBgm(getBgmId(), true);
                loadStep = 0;
                if (State.loadingType == 2) {
                    isNextGameStageDirectedly = true;
                } else {
                    isNextGameStageDirectedly = false;
                }
                return true;
        }
        if (nextStep) {
            loadStep++;
        }
        return false;
    }

    public static int getBgmId() {
        return MUSIC_ID[stageIDArray[PlayerObject.getCharacterID()]];
    }

    public static void draw(MFGraphics g) {
        g.setColor(MapManager.END_COLOR);
        MyAPI.fillRect(g, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
        for (int i = 0; i < STAGE_NAME.length; i++) {
            g.setColor(0);
            if (i == stageId) {
                g.setColor(16711680);
            }
            MyAPI.drawString(g, "stage" + STAGE_NAME[i], SCREEN_WIDTH >> 1, (i * 20) + 20, 17);
        }
    }

    public static int getCurrentZoneId() {
        if (stageIDArray[PlayerObject.getCharacterID()] >= ZOME_ID.length) {
            return ZOME_ID[ZOME_ID.length - 1] + 1;
        }
        return ZOME_ID[stageIDArray[PlayerObject.getCharacterID()]] + 1;
    }

    public static void setStagePass() {
        if (!stagePassFlag) {
            stagePassFlag = true;
            stagePassCount = STAGE_PASS_FRAME;
        }
    }

    public static void setStageRestart() {
        if (!stageRestartFlag) {
            stageRestartFlag = true;
            stageRestartCount = STAGE_RESTART_FRAME;
        }
    }

    public static void setStageGameover() {
        if (!stageGameoverFlag) {
            stageGameoverFlag = true;
            stageGameoverCount = STAGE_GAMEOVER_FRAME;
        }
    }

    public static void resetStageGameover() {
        stageGameoverFlag = false;
    }

    public static void setStageTimeover() {
        if (!stageTimeoverFlag) {
            stageTimeoverFlag = true;
            stageTimeoverCount = STAGE_TIMEOVER_FRAME;
        }
    }

    public static void stageLogic() {
        if (stagePassFlag && stagePassCount > 0) {
            stagePassCount--;
        }
        if (stageRestartFlag && stageRestartCount > 0) {
            stageRestartCount--;
        }
        if (stageGameoverFlag && stageGameoverCount > 0) {
            stageGameoverCount--;
        }
        if (stageTimeoverFlag && stageTimeoverCount > 0) {
            stageTimeoverCount--;
        }
    }

    public static boolean isStagePassTimePause() {
        return stagePassFlag;
    }

    public static boolean isStagePass() {
        return stagePassFlag && stagePassCount == 0;
    }

    public static boolean isStageRestart() {
        return stageRestartFlag && stageRestartCount == 0;
    }

    public static boolean isStageGameover() {
        return stageGameoverFlag && stageGameoverCount == 0;
    }

    public static boolean isStageTimeover() {
        return stageTimeoverFlag && stageTimeoverCount == 0;
    }

    public static void setStageID(int id) {
        stageIDArray[PlayerObject.getCharacterID()] = id;
    }

    public static void setStartStageID(int id) {
        startStageIDArray[PlayerObject.getCharacterID()] = id;
    }

    public static int getStartStageID() {
        return startStageIDArray[PlayerObject.getCharacterID()];
    }

    public static int getStageID() {
        return stageIDArray[PlayerObject.getCharacterID()];
    }

    public static void addStageID() {
        int[] iArr = stageIDArray;
        int characterID = PlayerObject.getCharacterID();
        iArr[characterID] = iArr[characterID] + 1;
        if (openedStageIDArray[PlayerObject.getCharacterID()] < stageIDArray[PlayerObject.getCharacterID()]) {
            openedStageIDArray[PlayerObject.getCharacterID()] = stageIDArray[PlayerObject.getCharacterID()];
        }
    }

    public static boolean IsStageEnd() {
        if (stageIDArray[PlayerObject.getCharacterID()] + 1 == STAGE_NAME.length) {
            return true;
        }
        return false;
    }

    public static int getTimeModeScore(int characterid) {
        return getTimeModeScore(characterid, stageIDArray[characterid]);
    }

    public static void setTimeModeScore(int characterid, int score) {
        if (score <= timeModeScore[((STAGE_NUM * 3) * characterid) + (stageIDArray[characterid] * 3)]) {
            timeModeScore[(((STAGE_NUM * 3) * characterid) + (stageIDArray[characterid] * 3)) + 2] = timeModeScore[(((STAGE_NUM * 3) * characterid) + (stageIDArray[characterid] * 3)) + 1];
            timeModeScore[(((STAGE_NUM * 3) * characterid) + (stageIDArray[characterid] * 3)) + 1] = timeModeScore[((STAGE_NUM * 3) * characterid) + (stageIDArray[characterid] * 3)];
            timeModeScore[((STAGE_NUM * 3) * characterid) + (stageIDArray[characterid] * 3)] = score;
        } else if (score <= timeModeScore[(((STAGE_NUM * 3) * characterid) + (stageIDArray[characterid] * 3)) + 1]) {
            timeModeScore[(((STAGE_NUM * 3) * characterid) + (stageIDArray[characterid] * 3)) + 2] = timeModeScore[(((STAGE_NUM * 3) * characterid) + (stageIDArray[characterid] * 3)) + 1];
            timeModeScore[(((STAGE_NUM * 3) * characterid) + (stageIDArray[characterid] * 3)) + 1] = score;
        } else if (score <= timeModeScore[(((STAGE_NUM * 3) * characterid) + (stageIDArray[characterid] * 3)) + 2]) {
            timeModeScore[(((STAGE_NUM * 3) * characterid) + (stageIDArray[characterid] * 3)) + 2] = score;
        }
    }

    public static int getTimeModeScore(int characterid, int id) {
        return timeModeScore[((STAGE_NUM * 3) * characterid) + (id * 3)];
    }

    public static int getTimeModeScore(int characterid, int id, int index) {
        return timeModeScore[(((STAGE_NUM * 3) * characterid) + (id * 3)) + index];
    }

    public static int[] getTimeModeScore() {
        return timeModeScore;
    }

    public static void setTimeModeScore(int[] tmpTimeModeScore) {
        for (int i = 0; i < timeModeScore.length; i++) {
            timeModeScore[i] = tmpTimeModeScore[i];
        }
    }

    public static void loadHighScoreRecord() {
        int i;
        ByteArrayInputStream bs = Record.loadRecordStream(Record.HIGHSCORE_RECORD);
        // HACK !
        if (true) {
            return;
        }
        try {
            DataInputStream ds = new DataInputStream(bs);
            for (i = 0; i < timeModeScore.length; i++) {
                timeModeScore[i] = ds.readInt();
            }
            if (bs != null) {
                try {
                    bs.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            for (i = 0; i < timeModeScore.length; i++) {
                timeModeScore[i] = SonicDef.OVER_TIME;
            }
            saveHighScoreRecord();
            if (bs != null) {
                try {
                    bs.close();
                } catch (Exception e3) {
                    e3.printStackTrace();
                }
            }
        } catch (Throwable th) {
            if (bs != null) {
                try {
                    bs.close();
                } catch (IOException e4) {
                    e4.printStackTrace();
                }
            }
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void saveHighScoreRecord() {
        /*
        r0 = new java.io.ByteArrayOutputStream;
        r0.<init>();
        r1 = new java.io.DataOutputStream;
        r1.<init>(r0);
        r3 = 0;
    L_0x000b:
        r4 = timeModeScore;	 Catch:{ Exception -> 0x0023, all -> 0x002d }
        r4 = r4.length;	 Catch:{ Exception -> 0x0023, all -> 0x002d }
        if (r3 < r4) goto L_0x0019;
    L_0x0010:
        r4 = "SONIC_HIGHSCORE_RECORD";
        Lib.Record.saveRecordStream(r4, r0);	 Catch:{ Exception -> 0x0023, all -> 0x002d }
        r1.close();	 Catch:{ IOException -> 0x0037 }
    L_0x0018:
        return;
    L_0x0019:
        r4 = timeModeScore;	 Catch:{ Exception -> 0x0023, all -> 0x002d }
        r4 = r4[r3];	 Catch:{ Exception -> 0x0023, all -> 0x002d }
        r1.writeInt(r4);	 Catch:{ Exception -> 0x0023, all -> 0x002d }
        r3 = r3 + 1;
        goto L_0x000b;
    L_0x0023:
        r4 = move-exception;
        r1.close();	 Catch:{ IOException -> 0x0028 }
        goto L_0x0018;
    L_0x0028:
        r2 = move-exception;
        r2.printStackTrace();
        goto L_0x0018;
    L_0x002d:
        r4 = move-exception;
        r1.close();	 Catch:{ IOException -> 0x0032 }
    L_0x0031:
        throw r4;
    L_0x0032:
        r2 = move-exception;
        r2.printStackTrace();
        goto L_0x0031;
    L_0x0037:
        r2 = move-exception;
        r2.printStackTrace();
        goto L_0x0018;
        */
        //throw new UnsupportedOperationException("Method not decompiled: SonicGBA.StageManager.saveHighScoreRecord():void");
        System.out.println("Method not decompiled: SonicGBA.StageManager.saveHighScoreRecord():void");
    }

    public static void loadStageRecord() {
        int i;
        // HACK !
        if (true) {
            return;
        }
        ByteArrayInputStream bs = Record.loadRecordStream(Record.STAGE_RECORD);
        try {
            DataInputStream ds = new DataInputStream(bs);
            stageId = ds.readByte();
            for (i = 0; i < 4; i++) {
                stageIDArray[i] = ds.readByte();
            }
            openedStageId = ds.readByte();
            if (openedStageId >= STAGE_NUM) {
                openedStageId = STAGE_NUM - 1;
            }
            int characterID = PlayerObject.getCharacterID();
            for (i = 0; i < 4; i++) {
                openedStageIDArray[i] = ds.readByte();
                if (openedStageIDArray[i] >= STAGE_NUM) {
                    openedStageIDArray[i] = STAGE_NUM - 1;
                }
            }
            for (i = 0; i < timeModeScore.length; i++) {
                ds.readInt();
            }
            normalStageId = ds.readByte();
            if (normalStageId != stageId) {
                stageId = normalStageId;
            }
            for (i = 0; i < 4; i++) {
                normalStageIDArray[i] = ds.readByte();
                if (normalStageIDArray[i] != stageIDArray[i]) {
                    stageIDArray[i] = normalStageIDArray[i];
                }
            }
            startStageID = ds.readByte();
            for (i = 0; i < 4; i++) {
                startStageIDArray[i] = ds.readByte();
            }
            characterFromGame = ds.readByte();
            stageIDFromGame = ds.readByte();
            PlayerObject.setLife(ds.readByte());
            PlayerObject.setScore(ds.readInt());
            if (bs != null) {
                try {
                    bs.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            stageId = 0;
            for (i = 0; i < 4; i++) {
                stageIDArray[i] = 0;
            }
            normalStageId = 0;
            for (i = 0; i < 4; i++) {
                normalStageIDArray[i] = 0;
            }
            PlayerObject.setScore(0);
            PlayerObject.setLife(2);
            openedStageId = 0;
            for (i = 0; i < 4; i++) {
                openedStageIDArray[i] = 0;
            }
            PlayerObject.resetGameParam();
            for (i = 0; i < timeModeScore.length; i++) {
                timeModeScore[i] = SonicDef.OVER_TIME;
            }
            startStageID = 0;
            for (i = 0; i < 4; i++) {
                startStageIDArray[i] = 0;
            }
            characterFromGame = -1;
            stageIDFromGame = -1;
            PlayerObject.setScore(0);
            PlayerObject.setLife(2);
            saveStageRecord();
            if (bs != null) {
                try {
                    bs.close();
                } catch (Exception e3) {
                    e3.printStackTrace();
                }
            }
        } catch (Throwable th) {
            if (bs != null) {
                try {
                    bs.close();
                } catch (IOException e4) {
                    e4.printStackTrace();
                }
            }
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void saveStageRecord() {
        /*
        r8 = 1;
        r7 = 4;
        r0 = new java.io.ByteArrayOutputStream;
        r0.<init>();
        r2 = new java.io.DataOutputStream;
        r2.<init>(r0);
        r5 = stageId;	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        r2.writeByte(r5);	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        r4 = 0;
    L_0x0012:
        if (r4 < r7) goto L_0x00b1;
    L_0x0014:
        r5 = openedStageId;	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        r6 = stageId;	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        if (r5 >= r6) goto L_0x001e;
    L_0x001a:
        r5 = stageId;	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        openedStageId = r5;	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
    L_0x001e:
        r5 = openedStageId;	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        r6 = STAGE_NUM;	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        if (r5 < r6) goto L_0x0029;
    L_0x0024:
        r5 = STAGE_NUM;	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        r5 = r5 - r8;
        openedStageId = r5;	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
    L_0x0029:
        r5 = openedStageId;	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        r2.writeByte(r5);	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        r1 = SonicGBA.PlayerObject.getCharacterID();	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        r5 = openedStageIDArray;	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        r5 = r5[r1];	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        r6 = stageIDArray;	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        r6 = r6[r1];	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        if (r5 >= r6) goto L_0x0044;
    L_0x003c:
        r5 = openedStageIDArray;	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        r6 = stageIDArray;	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        r6 = r6[r1];	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        r5[r1] = r6;	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
    L_0x0044:
        r5 = openedStageIDArray;	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        r5 = r5[r1];	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        r6 = STAGE_NUM;	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        if (r5 < r6) goto L_0x0053;
    L_0x004c:
        r5 = openedStageIDArray;	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        r6 = STAGE_NUM;	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        r6 = r6 - r8;
        r5[r1] = r6;	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
    L_0x0053:
        r4 = 0;
    L_0x0054:
        if (r4 < r7) goto L_0x00bc;
    L_0x0056:
        r4 = 0;
    L_0x0057:
        r5 = timeModeScore;	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        r5 = r5.length;	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        if (r4 < r5) goto L_0x00c6;
    L_0x005c:
        r5 = isRacing;	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        if (r5 != 0) goto L_0x006a;
    L_0x0060:
        r5 = normalStageId;	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        r6 = stageId;	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        if (r5 == r6) goto L_0x006a;
    L_0x0066:
        r5 = stageId;	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        normalStageId = r5;	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
    L_0x006a:
        r5 = isRacing;	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        if (r5 != 0) goto L_0x0080;
    L_0x006e:
        r5 = normalStageIDArray;	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        r5 = r5[r1];	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        r6 = stageIDArray;	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        r6 = r6[r1];	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        if (r5 == r6) goto L_0x0080;
    L_0x0078:
        r5 = normalStageIDArray;	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        r6 = stageIDArray;	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        r6 = r6[r1];	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        r5[r1] = r6;	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
    L_0x0080:
        r5 = normalStageId;	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        r2.writeByte(r5);	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        r4 = 0;
    L_0x0086:
        if (r4 < r7) goto L_0x00d0;
    L_0x0088:
        r5 = startStageID;	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        r2.writeByte(r5);	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        r4 = 0;
    L_0x008e:
        if (r4 < r7) goto L_0x00da;
    L_0x0090:
        r5 = characterFromGame;	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        r2.writeByte(r5);	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        r5 = stageIDFromGame;	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        r2.writeByte(r5);	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        r5 = SonicGBA.PlayerObject.getLife();	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        r2.writeByte(r5);	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        r5 = SonicGBA.PlayerObject.getScore();	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        r2.writeInt(r5);	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        r5 = "SONIC_STAGE_RECORD";
        Lib.Record.saveRecordStream(r5, r0);	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        r2.close();	 Catch:{ IOException -> 0x00f8 }
    L_0x00b0:
        return;
    L_0x00b1:
        r5 = stageIDArray;	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        r5 = r5[r4];	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        r2.writeByte(r5);	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        r4 = r4 + 1;
        goto L_0x0012;
    L_0x00bc:
        r5 = openedStageIDArray;	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        r5 = r5[r4];	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        r2.writeByte(r5);	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        r4 = r4 + 1;
        goto L_0x0054;
    L_0x00c6:
        r5 = timeModeScore;	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        r5 = r5[r4];	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        r2.writeInt(r5);	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        r4 = r4 + 1;
        goto L_0x0057;
    L_0x00d0:
        r5 = normalStageIDArray;	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        r5 = r5[r4];	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        r2.writeByte(r5);	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        r4 = r4 + 1;
        goto L_0x0086;
    L_0x00da:
        r5 = startStageIDArray;	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        r5 = r5[r4];	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        r2.writeByte(r5);	 Catch:{ Exception -> 0x00e4, all -> 0x00ee }
        r4 = r4 + 1;
        goto L_0x008e;
    L_0x00e4:
        r5 = move-exception;
        r2.close();	 Catch:{ IOException -> 0x00e9 }
        goto L_0x00b0;
    L_0x00e9:
        r3 = move-exception;
        r3.printStackTrace();
        goto L_0x00b0;
    L_0x00ee:
        r5 = move-exception;
        r2.close();	 Catch:{ IOException -> 0x00f3 }
    L_0x00f2:
        throw r5;
    L_0x00f3:
        r3 = move-exception;
        r3.printStackTrace();
        goto L_0x00f2;
    L_0x00f8:
        r3 = move-exception;
        r3.printStackTrace();
        goto L_0x00b0;
        */
        //throw new UnsupportedOperationException("Method not decompiled: SonicGBA.StageManager.saveStageRecord():void");

        System.out.println("Method not decompiled: SonicGBA.StageManager.saveStageRecord():void");
    }

    public static void addNewNormalScore(int newScore) {
        boolean isNewScore = false;
        int tmpScore = 0;
        for (int i = 0; i < 5; i++) {
            if (isNewScore) {
                int tmpScore2 = highScore[i];
                highScore[i] = tmpScore;
                tmpScore = tmpScore2;
            } else if (newScore > highScore[i]) {
                isNewScore = true;
                tmpScore = highScore[i];
                highScore[i] = newScore;
                drawNewScore = i;
            }
        }
    }

    public static void normalHighScoreInit() {
        movingRow = 0;
        movingCount = 0;
        for (int i = 0; i < rankingOffsetX.length; i++) {
            rankingOffsetX[i] = SCREEN_WIDTH;
        }
    }

    public static void drawNormalHighScore(MFGraphics g) {
        int i;
        for (i = 0; i < 5; i++) {
            if (drawNewScore == i && (System.currentTimeMillis() / 300) % 2 == 0) {
                State.drawMenuFontById(g, 49, (((SCREEN_WIDTH >> 1) - 45) + rankingOffsetX[i]) + 0, HIGH_SCORE_Y + (MENU_SPACE * i));
                State.drawMenuFontById(g, i + 38, (((SCREEN_WIDTH >> 1) - 45) + rankingOffsetX[i]) + 0, HIGH_SCORE_Y + (MENU_SPACE * i));
                PlayerObject.drawNum(g, highScore[i], ((((SCREEN_WIDTH >> 1) + 20) + 48) + rankingOffsetX[i]) + 0, (MENU_SPACE * i) + HIGH_SCORE_Y, 2, 4);
            } else {
                State.drawMenuFontById(g, 48, (((SCREEN_WIDTH >> 1) - 45) + rankingOffsetX[i]) + 0, HIGH_SCORE_Y + (MENU_SPACE * i));
                State.drawMenuFontById(g, i + 28, (((SCREEN_WIDTH >> 1) - 45) + rankingOffsetX[i]) + 0, HIGH_SCORE_Y + (MENU_SPACE * i));
                PlayerObject.drawNum(g, highScore[i], ((((SCREEN_WIDTH >> 1) + 20) + 48) + rankingOffsetX[i]) + 0, (MENU_SPACE * i) + HIGH_SCORE_Y, 2, 0);
            }
        }
        if (movingRow < rankingOffsetX.length && movingCount % 2 == 0) {
            movingRow++;
        }
        movingCount++;
        for (i = 0; i < movingRow; i++) {
            rankingOffsetX[i] = MyAPI.calNextPosition((double) rankingOffsetX[i], 0.0d, 1, 3);
        }
    }

    public static void drawHighScoreEnd() {
        drawNewScore = -1;
    }

    public static int getOpenedStageId() {
        if (TitleState.preStageSelectState == 24) {
            return getMaxStageID();
        }
        int stageid = openedStageIDArray[PlayerObject.getCharacterID()];
        if (PlayerObject.getCharacterID() == 0) {
            if (GameObject.stageModeState == 0) {
                if (stageid >= STAGE_NUM) {
                    stageid = STAGE_NUM;
                }
            } else if (GameObject.stageModeState == 1 && stageid >= STAGE_NUM - 3) {
                stageid = STAGE_NUM - 3;
            }
        } else if (GameObject.stageModeState == 0) {
            if (stageid >= STAGE_NUM - 2) {
                stageid = STAGE_NUM - 2;
            }
        } else if (GameObject.stageModeState == 1 && stageid >= STAGE_NUM - 3) {
            stageid = STAGE_NUM - 3;
        }
        return stageid;
    }

    public static int getMaxStageID() {
        if (PlayerObject.getCharacterID() == 0) {
            if (GameObject.stageModeState == 0) {
                if (openedStageIDArray[PlayerObject.getCharacterID()] >= STAGE_NUM - 1) {
                    return STAGE_NUM - 1;
                }
                return STAGE_NUM - 2;
            } else if (GameObject.stageModeState == 1) {
                return STAGE_NUM - 3;
            }
        } else if (GameObject.stageModeState == 0) {
            return STAGE_NUM - 2;
        } else {
            if (GameObject.stageModeState == 1) {
                return STAGE_NUM - 3;
            }
        }
        return 0;
    }

    public static void resetOpenedStageIdforTry(int id) {
        openedStageIDArray[PlayerObject.getCharacterID()] = id;
        stageIDArray[PlayerObject.getCharacterID()] = 0;
        normalStageIDArray[PlayerObject.getCharacterID()] = 0;
        saveStageRecord();
    }

    public static boolean hasContinueGame() {
        return stageIDArray[PlayerObject.getCharacterID()] == 0;
    }

    public static void saveCheckPoint(int x, int y) {
        checkPointX = x >> 6;
        checkPointY = y >> 6;
        checkPointEnable = true;
        checkPointTime = PlayerObject.timeCount;
    }

    public static void saveSpecialStagePoint(int x, int y) {
        specialStagePointX = x >> 6;
        specialStagePointY = y >> 6;
    }

    public static void saveCheckPointCamera(int cameraUpX, int cameraDownX, int cameraLeftX, int cameraRightX) {
        checkCameraUpX = cameraUpX;
        checkCameraDownX = cameraDownX;
        checkCameraLeftX = cameraLeftX;
        checkCameraRightX = cameraRightX;
        checkCameraEnable = true;
    }

    public static void resetStageId() {
        if (openedStageIDArray[PlayerObject.getCharacterID()] < stageIDArray[PlayerObject.getCharacterID()]) {
            openedStageIDArray[PlayerObject.getCharacterID()] = stageIDArray[PlayerObject.getCharacterID()];
        }
        stageIDArray[PlayerObject.getCharacterID()] = 0;
        normalStageIDArray[PlayerObject.getCharacterID()] = 0;
        saveStageRecord();
    }

    public static void resetStageIdforTry() {
        openedStageIDArray[PlayerObject.getCharacterID()] = 0;
        stageIDArray[PlayerObject.getCharacterID()] = 0;
        normalStageIDArray[PlayerObject.getCharacterID()] = 0;
        saveStageRecord();
    }

    public static void resetStageIdforContinueEnd() {
        characterFromGame = -1;
        stageIDFromGame = -1;
        PlayerObject.setScore(0);
        PlayerObject.setLife(2);
        saveStageRecord();
    }

    public static void resetGameRecord() {
        int i;
        stageId = 0;
        for (i = 0; i < CHARACTER_NUM; i++) {
            stageIDArray[i] = 0;
        }
        normalStageId = 0;
        for (i = 0; i < CHARACTER_NUM; i++) {
            normalStageIDArray[i] = 0;
        }
        PlayerObject.setScore(0);
        PlayerObject.setLife(2);
        openedStageId = 0;
        for (i = 0; i < CHARACTER_NUM; i++) {
            openedStageIDArray[i] = 0;
        }
        PlayerObject.resetGameParam();
        for (i = 0; i < timeModeScore.length; i++) {
            timeModeScore[i] = SonicDef.OVER_TIME;
        }
        startStageID = 0;
        for (i = 0; i < CHARACTER_NUM; i++) {
            startStageIDArray[i] = 0;
        }
        characterFromGame = -1;
        stageIDFromGame = -1;
        PlayerObject.setScore(0);
        PlayerObject.setLife(2);
        SpecialStageState.emptyEmeraldArray();
        GlobalResource.initSystemConfig();
        saveStageRecord();
    }

    public static void doWhileEnterRace() {
        if (!isRacing) {
            normalStageIDArray[PlayerObject.getCharacterID()] = stageIDArray[PlayerObject.getCharacterID()];
            isRacing = true;
        }
    }

    public static void doWhileLeaveRace() {
        if (isRacing) {
            stageIDArray[PlayerObject.getCharacterID()] = normalStageIDArray[PlayerObject.getCharacterID()];
            isRacing = false;
        }
    }

    public static int getStageNameID(int stageID) {
        return STAGE_NAME_ID[stageID / 2];
    }

    public static void setWaterLevel(int level) {
        waterLevel = level;
    }

    public static int getWaterLevel() {
        if (getCurrentZoneId() == 4) {
            return waterLevel;
        }
        return -1;
    }

    public static boolean isGoingToExtraStage() {
        return PlayerObject.getCharacterID() == 0 && !SpecialStageState.emeraldMissed() && getStartStageID() != 12 && getStageID() == 12;
    }

    public static void stagePassInit() {
        isOnlyScoreCal = false;
        isOnlyStagePass = false;
        isScoreBarOutOfScreen = false;
        PlayerObject.isbarOut = false;
    }

    public static void setOnlyScoreCal() {
        isOnlyScoreCal = true;
        isOnlyStagePass = false;
        PlayerObject.isbarOut = true;
    }

    public static void setStraightlyPass() {
        isOnlyStagePass = true;
    }

    public static boolean isScoreBarOut() {
        return isScoreBarOutOfScreen;
    }
}
