package SonicGBA;

import Lib.MyAPI;
import Lib.SoundSystem;
import com.sega.engine.action.ACCollision;
import com.sega.engine.action.ACObject;
import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;

public class ItemObject extends GameObject {
    private static final int COLLISION_HEIGHT = 1728;
    private static final int COLLISION_WIDTH = 2048;
    private static final int MOVE_COUNT = 20;
    private static int gridWidth;
    public static MFImage itemBoxImage;
    public static MFImage itemContentImage;
    public static MFImage itemHeadImage;
    private boolean isActive;
    private MapObject mapObj;
    private int moveCount;
    private int objId;
    private int originalY;
    private boolean poping = false;
    private int posYoffset;
    private boolean used;

    public static ItemObject getNewInstance(int id, int x, int y) {
        x <<= 6;
        y <<= 6;
        if (id == 0 && stageModeState == 1) {
            return null;
        }
        ItemObject reElement = new ItemObject(id, x, y);
        reElement.refreshCollisionRect(reElement.posX, reElement.posY);
        return reElement;
    }

    public static void closeItem() {
        itemBoxImage = null;
        itemContentImage = null;
    }

    private ItemObject(int id, int x, int y) {
        this.objId = id;
        this.mWidth = COLLISION_WIDTH;
        this.mHeight = COLLISION_HEIGHT;
        this.posX = x;
        this.posYoffset = 0;
        this.posY = y + 256;
        this.originalY = y + 256;
        if (itemBoxImage == null) {
            try {
                itemBoxImage = MFImage.createImage("/item/item_box.png");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (itemContentImage == null) {
            try {
                itemContentImage = MFImage.createImage("/item/item_content.png");
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            gridWidth = MyAPI.zoomIn(itemContentImage.getWidth(), true) >> 3;
        }
        if (itemHeadImage == null) {
            itemHeadImage = MFImage.createImage("/item/item_head.png");
        }
        this.used = false;
        this.isActive = false;
    }

    public void doWhileBeAttack(PlayerObject object, int direction, int animationID) {
        if (!this.used && !object.piping) {
            doFunction(object, direction, animationID != 12);
            Effect.showEffect(destroyEffectAnimation, 0, this.posX >> 6, (this.posY - (this.mHeight >> 1)) >> 6, 0);
            if ((player instanceof PlayerTails) && player.myAnimationID == 12 && direction == 1) {
                player.velY = -600;
            }
        }
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (!this.used && !object.piping) {
            if (direction != 4) {
                switch (direction) {
                    case 0:
                        if (player.isAntiGravity) {
                            if (object == player && object.isAttackingItem() && this.firstTouch && player.isAntiGravity && (!(player instanceof PlayerKnuckles) || player.animationID == 4)) {
                                doFunction(object, direction);
                                break;
                            }
                        }
                        this.poping = true;
                        this.velY = -512;
                        this.mapObj.setPosition(this.posX, player.getCollisionRect().y0, 0, this.velY, this);
                        break;
                    case 1:
                        if (object == player && object.isAttackingItem() && this.firstTouch && !player.isAntiGravity) {
                            doFunction(object, direction);
                        }
                        if (object == player && !player.isAntiGravity) {
                            this.isActive = true;
                            player.IsStandOnItems = true;
                            break;
                        }
                    case 2:
                    case 3:
                        if (object == player && object.isAttackingItem() && object.getVelX() != 0) {
                            doFunction(object, direction);
                            break;
                        }
                }
                if (!(this.used || ((player instanceof PlayerKnuckles) && direction == 0 && !player.isAntiGravity))) {
                    object.beStop(0, direction, this);
                }
            }
            if (this.used) {
                Effect.showEffect(destroyEffectAnimation, 0, this.posX >> 6, (this.posY - (this.mHeight >> 1)) >> 6, 0);
            }
        }
    }

    public void doWhileNoCollision() {
        if (this.isActive) {
            player.IsStandOnItems = false;
            this.isActive = false;
        }
    }

    public void draw(MFGraphics g) {
        if (!this.used || this.moveCount >= 0) {
            if (this.objId != 0) {
                drawInMap(g, itemContentImage, this.objId * gridWidth, 0, gridWidth, gridWidth, 0, this.posX, (this.posY - (COLLISION_HEIGHT/2)) + (this.poping ? this.posYoffset : 0), 3);
            } else if (PlayerObject.getCharacterID() == 0) {
                /*
                r2 = itemContentImage;
                r5 = gridWidth;
                r6 = gridWidth;
                r8 = this.posX;
                r0 = this.posY - 896;
                if (this.poping) {
                    r1 = this.posYoffset;
                } else {
                    r1 = 0;
                }
                drawInMap(g, r2, 0, 0, r5, r6, 0, r8, r0 + r1, 3);
                */
                int r0 = this.posY - (COLLISION_HEIGHT/2);
                int r1;
                if (this.poping) {
                    r1 = this.posYoffset;
                } else {
                    r1 = 0;
                }
                drawInMap(g, itemContentImage, 0, 0, gridWidth, gridWidth, 0, this.posX, r0 + r1, 3);
            } else {
                /*
                r2 = itemHeadImage;
                int characterID = (PlayerObject.getCharacterID() - 1) * gridWidth;
                r5 = gridWidth;
                r6 = gridWidth;
                r8 = this.posX;
                r0 = this.posY - 896;
                if (this.poping) {
                    r1 = this.posYoffset;
                } else {
                    r1 = 0;
                }
                drawInMap(g, r2, characterID, 0, r5, r6, 0, r8, r0 + r1, 3);
                */
                int r0 = this.posY - (COLLISION_HEIGHT/2);
                int r1;
                if (this.poping) {
                    r1 = this.posYoffset;
                } else {
                    r1 = 0;
                }
                drawInMap(g, itemHeadImage, (PlayerObject.getCharacterID() - 1) * gridWidth, 0, gridWidth, gridWidth, 0, this.posX, r0 + r1, 3);
            }
        }
        if (!this.used) {
            drawInMap(g, itemBoxImage, 33);
        }
        drawCollisionRect(g);
    }

    public void logic() {
        if (this.used) {
            if (this.moveCount > 0) {
                this.moveCount--;
                if (this.moveCount == 19 && this.objId >= 5 && this.objId <= 7) {
                    PlayerObject.getTmpRing(this.objId);
                }
                this.posY -= 200;
                this.posYoffset -= 200;
            }
            if (this.moveCount == 0) {
                this.moveCount = -1;
                if (this.objId >= 5 && this.objId <= 7) {
                    soundInstance.playSe(12);
                }
            }
        }
        if (this.poping) {
            if (this.mapObj == null) {
                this.mapObj = new MapObject(this.posX + COLLISION_WIDTH, this.posY, 0, 0, this, 1);
                this.mapObj.setPosition(this.posX, this.posY, 0, this.velY, this);
                this.mapObj.setCrashCount(1);
            }
            this.mapObj.logic2();
            checkWithPlayer(this.posX, this.posY, this.mapObj.getPosX(), this.mapObj.getPosY());
            this.posX = this.mapObj.getPosX();
            this.posY = this.mapObj.getPosY();
            refreshCollisionRect(this.posX, this.posY);
        }
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - (this.mWidth >> 1), y - this.mHeight, this.mWidth, this.mHeight);
    }

    public void close() {
    }

    public void doBeforeCollisionCheck() {
    }

    public void doWhileCollision(ACObject arg0, ACCollision arg1, int arg2, int arg3, int arg4, int arg5, int arg6) {
    }

    private void doFunction(PlayerObject object, int direction) {
        doFunction(object, direction, true);
    }

    private void doFunction(PlayerObject object, int direction, boolean attackPose) {
        if (attackPose) {
            object.doItemAttackPose(this, direction);
        }
        this.used = true;
        this.moveCount = MOVE_COUNT;
        player.getPreItem(this.objId);
        SoundSystem.getInstance().playSe(29);
    }

    public int getPaintLayer() {
        return 0;
    }
}
