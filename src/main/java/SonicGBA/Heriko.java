package SonicGBA;

import Lib.Animation;
import com.sega.mobile.define.MDPhone;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: EnemyObject */
class Heriko extends EnemyObject {
    private static final int ALERT_HEIGHT = 128;
    private static final int ALERT_WIDTH = 64;
    private static final int BULLET_SPEED = 512;
    private static final int COLLISION_HEIGHT = 1856;
    private static final int COLLISION_WIDTH = 1280;
    private static final int DISTANCE = 2560;
    private static final int HEIGHT_OFFSET = 4;
    private static final int SPEED = 128;
    private static final int STATE_ATTACK = 1;
    private static final int STATE_PATROL = 0;
    private static final int WAIT_MAX = 30;
    private static Animation herikoAnimation;
    private boolean IsFire = false;
    private int alert_state;
    private boolean dir = false;
    private int endPosX;
    private int journey;
    private int startPosX;
    private int state;
    private int wait_cn;

    protected Heriko(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y - 4, left, top, width, height);
        if (herikoAnimation == null) {
            herikoAnimation = new Animation("/animation/heriko");
        }
        this.drawer = herikoAnimation.getDrawer(0, true, 0);
        this.startPosX = this.posX;
        this.endPosX = this.posX + this.mWidth;
        this.dir = false;
        this.state = 0;
        this.wait_cn = 0;
        this.IsFire = false;
    }

    public static void releaseAllResource() {
        Animation.closeAnimation(herikoAnimation);
        herikoAnimation = null;
    }

    public void logic() {
        if (!this.dead) {
            this.alert_state = checkPlayerInEnemyAlertRange(this.posX >> 6, this.posY >> 6, 64, 128);
            int preX = this.posX;
            int preY = this.posY;
            switch (this.state) {
                case 0:
                    if (this.dir) {
                        this.posX -= 128;
                        this.journey += 128;
                        if (this.posX <= this.startPosX) {
                            this.dir = false;
                            this.posX = this.startPosX;
                        }
                    } else {
                        this.posX += 128;
                        this.journey += 128;
                        if (this.posX >= this.endPosX) {
                            this.dir = true;
                            this.posX = this.endPosX;
                        }
                    }
                    if (this.journey >= 2560 && this.alert_state == 0) {
                        this.state = 1;
                        this.journey = 0;
                        this.drawer.setActionId(1);
                        this.drawer.setLoop(false);
                        this.IsFire = true;
                    }
                    checkWithPlayer(preX, preY, this.posX, this.posY);
                    return;
                case 1:
                    if (this.wait_cn < 30) {
                        this.wait_cn++;
                        if (this.drawer.checkEnd() && this.IsFire) {
                            this.IsFire = false;
                            BulletObject.addBullet(12, this.posX, this.posY, 0, 512);
                        }
                    } else {
                        this.wait_cn = 0;
                        this.drawer.setActionId(0);
                        this.drawer.setTrans(0);
                        this.drawer.setLoop(true);
                        this.state = 0;
                    }
                    checkWithPlayer(preX, preY, this.posX, this.posY);
                    return;
                default:
                    return;
            }
        }
    }

    public void draw(MFGraphics g) {
        if (!this.dead) {
            drawInMap(g, this.drawer);
        }
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - MDPhone.SCREEN_HEIGHT, y - Boss6Block.COLLISION2_HEIGHT, COLLISION_WIDTH, COLLISION_HEIGHT);
    }
}
