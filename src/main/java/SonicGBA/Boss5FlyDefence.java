package SonicGBA;

import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: EnemyObject */
class Boss5FlyDefence extends EnemyObject {
    private static boolean Hurt = false;
    private static boolean IsAvailable = false;
    private int AttackDirection;
    private int COLLISION_HEIGHT = 1472;
    private int COLLISION_WIDTH = 1920;

    protected Boss5FlyDefence(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        this.posX -= this.iLeft * 8;
        this.posY -= this.iTop * 8;
        Hurt = false;
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (!IsAvailable) {
            Hurt = false;
        } else if (object != player) {
        } else {
            if (player.isAttackingEnemy()) {
                Hurt = true;
            } else {
                Hurt = false;
            }
        }
    }

    public void doWhileBeAttack(PlayerObject object, int direction, int animationID) {
    }

    public void logic() {
    }

    public void draw(MFGraphics g) {
        drawCollisionRect(g);
    }

    public void logic(int posx, int posy, int AttackDir) {
        this.posX = posx;
        this.posY = posy;
        int preX = this.posX;
        int preY = this.posY;
        this.AttackDirection = AttackDir;
        if (IsAvailable) {
            this.COLLISION_WIDTH = 1920;
            this.COLLISION_HEIGHT = 1472;
        } else {
            this.COLLISION_WIDTH = 0;
            this.COLLISION_HEIGHT = 0;
        }
        refreshCollisionRect(this.posX >> 6, this.posY >> 6);
        checkWithPlayer(preX, preY, this.posX, this.posY);
    }

    public boolean getHurtState() {
        return Hurt;
    }

    public void setHurtState(boolean state) {
        Hurt = state;
    }

    public void setCollAvailable(boolean state) {
        IsAvailable = state;
    }

    public void refreshCollisionRect(int x, int y) {
        if (this.AttackDirection > 0) {
            this.collisionRect.setRect(x - 448, y - this.COLLISION_HEIGHT, this.COLLISION_WIDTH, this.COLLISION_HEIGHT);
        } else {
            this.collisionRect.setRect(x - 1408, y - this.COLLISION_HEIGHT, this.COLLISION_WIDTH, this.COLLISION_HEIGHT);
        }
    }
}
