package SonicGBA;

import Lib.Animation;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: EnemyObject */
class Fish extends EnemyObject {
    private static final int ALERT_RANGE = 60;
    private static final int COLLISION_HEIGHT = 1024;
    private static final int COLLISION_WIDTH = 2560;
    private static final int FISH_XY_MAX_SPEED = 432;
    private static final int STATE_ATTACK = 2;
    private static final int STATE_BREAK = 1;
    private static final int STATE_READY = 0;
    private static Animation fishAnimation;
    private int alert_state;
    private int iTrans;
    private int state;
    private int velX;
    private int velY;

    protected Fish(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        if (fishAnimation == null) {
            fishAnimation = new Animation("/animation/ice_fish");
        }
        this.drawer = fishAnimation.getDrawer(0, true, 0);
        this.posX = x << 6;
        this.posY = y << 6;
    }

    public static void releaseAllResource() {
        Animation.closeAnimation(fishAnimation);
        fishAnimation = null;
    }

    private int checkEnemyFace() {
        if (this.posX - player.getFootPositionX() >= 0) {
            return 0;
        }
        return 2;
    }

    public void logic() {
        if (!this.dead) {
            this.alert_state = checkPlayerInEnemyAlertRange(this.posX >> 6, this.posY >> 6, 60, 60);
            int preX = this.posX;
            int preY = this.posY;
            switch (this.state) {
                case 0:
                    if (this.alert_state == 0) {
                        this.state = 1;
                        this.drawer.setActionId(1);
                        this.drawer.setLoop(false);
                        this.drawer.setTrans(0);
                        this.iTrans = checkEnemyFace();
                        break;
                    }
                    break;
                case 1:
                    if (this.drawer.checkEnd()) {
                        this.state = 2;
                        this.drawer.setActionId(2);
                        this.drawer.setLoop(true);
                        this.drawer.setTrans(this.iTrans);
                        this.velX = (-(this.posX - player.getCheckPositionX())) / 8;
                        this.velY = (-(this.posY - player.getCheckPositionY())) / 8;
                        if (this.velX > FISH_XY_MAX_SPEED) {
                            this.velX = FISH_XY_MAX_SPEED;
                        } else if (this.velX < -432) {
                            this.velX = -432;
                        }
                        if (this.velY > FISH_XY_MAX_SPEED) {
                            this.velY = FISH_XY_MAX_SPEED;
                        } else if (this.velY < -432) {
                            this.velY = -432;
                        }
                        if (Math.abs(this.velY) > Math.abs(this.velX)) {
                            if (this.velY <= 0) {
                                if (this.velY < 0) {
                                    this.velY = -Math.abs(this.velX);
                                    break;
                                }
                            }
                            this.velY = Math.abs(this.velX);
                            break;
                        }
                    }
                    break;
                case 2:
                    this.posX += this.velX;
                    this.posY += this.velY;
                    break;
            }
            checkWithPlayer(preX, preY, this.posX, this.posY);
        }
    }

    public void draw(MFGraphics g) {
        if (!this.dead) {
            drawInMap(g, this.drawer, this.posX, this.posY);
            drawCollisionRect(g);
        }
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 1280, y - 512, 2560, 1024);
    }
}
