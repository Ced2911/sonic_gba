package SonicGBA;

import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: EnemyObject */
class BossF3Defence extends EnemyObject {
    private static final int COLLISION_HEIGHT = 2432;
    private static final int COLLISION_WIDTH = 2688;
    private boolean isAvaliable = true;
    private boolean isHurt = false;
    private BossF3 mboss;

    protected BossF3Defence(int x, int y, BossF3 bossf3) {
        super(30, x, y, 0, 0, 0, 0);
        this.posX = x;
        this.posY = y;
        this.mboss = bossf3;
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (!this.isAvaliable) {
            return;
        }
        if (object != player) {
            this.isHurt = false;
        } else if (!isCircleNoneUpDefenceState() || direction == 1 || player.canBeHurt()) {
            player.beHurt();
            this.isHurt = true;
        }
    }

    public void doWhileBeAttack(PlayerObject object, int direction, int animationID) {
        if (!this.isAvaliable) {
            return;
        }
        if (object != player) {
            this.isHurt = false;
        } else if (!isCircleNoneUpDefenceState() || direction == 1 || player.canBeHurt()) {
            player.beHurt();
            this.isHurt = true;
        }
    }

    private boolean isCircleNoneUpDefenceState() {
        if (this.mboss == null) {
            return false;
        }
        if (this.mboss.pro_state == 1 || this.mboss.pro_state == 2 || this.mboss.pro_state == 3 || this.mboss.pro_state == 4) {
            return true;
        }
        return false;
    }

    public boolean getHurtState() {
        return this.isHurt;
    }

    public void resetHurtState() {
        this.isHurt = false;
    }

    public void setAvaliable(boolean state) {
        this.isAvaliable = state;
    }

    public void logic() {
    }

    public void logic(int x, int y) {
        this.posX = x;
        this.posY = y;
        int preX = this.posX;
        int preY = this.posY;
        refreshCollisionRect(this.posX, this.posY);
        checkWithPlayer(preX, preY, this.posX, this.posY);
    }

    public void draw(MFGraphics g) {
        drawCollisionRect(g);
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 1344, y - 960, COLLISION_WIDTH, COLLISION_HEIGHT);
    }
}
