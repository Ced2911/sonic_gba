package SonicGBA;

import Lib.Animation;
import com.sega.mobile.define.MDPhone;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: BulletObject */
class MiraBullet extends BulletObject {
    private static final int COLLISION_HEIGHT = 640;
    private static final int COLLISION_WIDTH = 640;

    protected MiraBullet(int x, int y, int velX, int velY) {
        super(x, y, velX, velY, false);
        if (mirabulletAnimation == null) {
            mirabulletAnimation = new Animation("/animation/mira_bullet");
        }
        this.drawer = mirabulletAnimation.getDrawer(0, true, 0);
        this.posX = x;
        this.posY = y;
        this.velX = velX;
        this.velY = velY;
    }

    public void bulletLogic() {
        checkWithPlayer(this.posX, this.posY, this.posX + this.velX, this.posY);
        this.posX += this.velX;
        this.posY += this.velY;
    }

    public void draw(MFGraphics g) {
        drawInMap(g, this.drawer);
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 320, y - 320, MDPhone.SCREEN_HEIGHT, MDPhone.SCREEN_HEIGHT);
    }

    public boolean chkDestroy() {
        return !isInCamera() || isFarAwayCamera();
    }
}
