package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: EnemyObject */
class AspirateBubble extends EnemyObject {
    private static final int BUBBLE_SHAKE_SPEED = 16;
    private static final int BUBBLE_UP_SPEED = 120;
    private static Animation BubbleAnimation = null;
    private static final int COLLISION_HEIGHT = 512;
    private static final int COLLISION_WIDTH = 512;
    private AnimationDrawer bubbledrawer;
    private int frame;
    private int velx;
    private int waterLevel;

    protected AspirateBubble(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        if (BubbleAnimation == null) {
            BubbleAnimation = new Animation("/animation/aspirate");
        }
        this.bubbledrawer = BubbleAnimation.getDrawer(0, true, 0);
        this.waterLevel = StageManager.getWaterLevel() << 6;
        this.posX = x;
        this.posY = y;
    }

    public void logic() {
        if (!this.dead) {
            if (!isInCamera() || this.posY - 256 < (StageManager.getWaterLevel() << 6)) {
                this.dead = true;
            }
            this.frame++;
            this.frame %= 96;
            this.posY -= BUBBLE_UP_SPEED;
            this.velx = this.frame <= 48 ? BUBBLE_SHAKE_SPEED : -BUBBLE_SHAKE_SPEED;
            this.posX += this.velx;
            refreshCollisionRect(this.posX, this.posY);
        }
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 256, y - 256, COLLISION_WIDTH, COLLISION_HEIGHT);
    }

    public int getPaintLayer() {
        return 1;
    }

    public void draw(MFGraphics g) {
        if (!this.dead) {
            drawInMap(g, this.bubbledrawer);
        }
    }

    public void doWhileCollision(PlayerObject object, int direction) {
    }

    public void doWhileBeAttack(PlayerObject object, int direction, int animationID) {
    }

    public static void releaseAllResource() {
        Animation.closeAnimation(BubbleAnimation);
        BubbleAnimation = null;
    }

    public void close() {
        this.bubbledrawer = null;
    }
}
