package SonicGBA;

import GameEngine.Def;
import Lib.Animation;
import Lib.AnimationDrawer;
import Lib.MyAPI;
import Lib.MyRandom;
import Lib.SoundSystem;
import Lib.crlFP32;
import com.sega.mobile.define.MDPhone;
import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.utility.MFMath;

/* compiled from: EnemyObject */
class BossF3 extends BossObject {
    private static final int ARM_CAUGHT_BACK_V = 1440;
    private static final int ARM_OUT_V = 1320;
    private static final int ARM_UNCAUGHT_BACK_V = 480;
    private static final int BOSS_APPEAR_POSX = 167680;
    private static final int BOSS_APPEAR_POSY = 32128;
    private static final int BOSS_ARMSTATE_V = 720;
    private static final int BOSS_FIRST_STOP_POSY = 24768;
    private static final int BOSS_HANG_BOTTOM = 25024;
    private static final int BOSS_HANG_LEFT_POSX = 152832;
    private static final int BOSS_HANG_RIGHT_POSX = 167680;
    private static final int BOSS_HANG_TOP = 24512;
    private static final int BOSS_LEFT_POSX = 147712;
    private static final int BOSS_RIGHT_POSX = 172800;
    private static final int CABIN_HURT = 1;
    private static final int CABIN_NORMAL = 0;
    private static final int CHAIN_DIAMETER = 1024;
    private static final int CHAIN_HEIGHT = 1024;
    private static final int CHAIN_WIDTH = 1024;
    private static final int CIRCLE_ARM_ATTATCK_1 = 8;
    private static final int CIRCLE_ARM_ATTATCK_2 = 9;
    private static final int CIRCLE_ARM_HURT = 10;
    private static final int CIRCLE_BOMB_ATTATCK_1 = 5;
    private static final int CIRCLE_BOMB_ATTATCK_2 = 6;
    private static final int CIRCLE_BOMB_HURT = 7;
    private static final int CIRCLE_FIRST_OFFSETPOSY = -1536;
    private static final int CIRCLE_HEIGHT = 1536;
    private static final int CIRCLE_NORMAL_OFFSETPOSY = 0;
    private static final int CIRCLE_RAY_ATTATCK_1 = 2;
    private static final int CIRCLE_RAY_ATTATCK_2 = 3;
    private static final int CIRCLE_RAY_HURT = 4;
    private static final int CIRCLE_ROTATE = 0;
    private static final int CIRCLE_ROTATE_HURT = 1;
    private static final int CIRCLE_WIDTH = 4096;
    private static final int COLLISION_HEIGHT = 1024;
    private static final int COLLISION_WIDTH = 2688;
    private static final int DEGREE_OFFSET = 18;
    private static final int FACE_ANGRY = 1;
    private static final int FACE_HURT = 3;
    private static final int FACE_NORMAL = 0;
    private static final int FACE_OFFSETY = -832;
    private static final int FACE_SMILE = 2;
    private static final int INIT_STOP_POSX = 160768;
    private static final int INIT_STOP_POSY1 = 21504;
    private static final int INIT_STOP_POSY2 = 27648;
    private static final int MOVE_SPEED = 240;
    public static final int PRO_ATTACK_ARM = 4;
    public static final int PRO_ATTACK_BOMB = 3;
    public static final int PRO_ATTACK_RAY = 2;
    public static final int PRO_MOVE = 1;
    public static final int PRO_WAIT = 0;
    private static final int SIDE_DOWN = 440;
    private static final int SIDE_LEFT = 2360;
    private static final int SIDE_RIGHT = 2648;
    private static final int SIDE_UP = 248;
    private static final int STATE_BROKEN = 3;
    private static final int STATE_ENTER_SHOW = 1;
    private static final int STATE_ESCAPE = 4;
    private static final int STATE_INIT = 0;
    private static final int STATE_PRO = 2;
    private static Animation boatAni = null;
    private static Animation cabinAni = null;
    private static Animation chainAni = null;
    private static Animation circleAni = null;
    private static final int cnt_max = 8;
    private static Animation escapefaceAni;
    private static Animation faceAni;
    private int WaitCnt;
    private int aim_max;
    private BossF3Arm arm;
    private boolean armDegreeLock;
    private int armStartPosY;
    private int arm_attack_step;
    private int arm_drop_vely;
    private int arm_posx;
    private int arm_posy;
    private int arm_pre_velx;
    private int arm_pre_vely;
    private int arm_velx;
    private int arm_vely;
    private int attack_state;
    private AnimationDrawer boatdrawer;
    private int boss_hang_posx;
    private BossBroken bossbroken;
    private int brokenFrame;
    private AnimationDrawer cabinDrawer;
    private int cabin_state;
    private AnimationDrawer chainDrawer;
    private BossF3Circle circle;
    private AnimationDrawer circleDrawer;
    private int circleOffsetY;
    private int circleState;
    private int circle_state;
    private BossF3Defence defence;
    private int degree;
    private boolean displayFlag;
    private int drop_vely;
    private int enter_cn;
    private int escape_v;
    private AnimationDrawer escapefacedrawer;
    private AnimationDrawer faceDrawer;
    private int face_cnt;
    private int face_state;
    private int fly_end;
    private int fly_top;
    private int fly_top_range;
    private boolean isAiming;
    private boolean isArmBack;
    private boolean isArmCaughtEnd;
    private boolean isArmMove;
    private boolean isArmUncaughtEnd;
    private boolean isCaught;
    private boolean isMachineShake;
    private boolean isinArmRange;
    public int pro_state;
    private int radius;
    private int rayOffsetY;
    private int recoil_offsetx;
    private int shakeFrame;
    private int shakeOffsetY;
    private int shakechainx;
    private int shakechainy;
    private int state;
    private int velocity;
    private int wait_cnt;
    private int wait_cnt_max;
    private int wait_frame_cn;
    private int wait_frame_max;
    private int wait_frame_offset_cn;

    protected BossF3(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        this.isMachineShake = false;
        this.isinArmRange = false;
        this.isCaught = false;
        this.isArmUncaughtEnd = false;
        this.isArmCaughtEnd = false;
        this.isArmMove = false;
        this.isArmBack = false;
        this.isAiming = false;
        this.wait_cnt_max = 10;
        this.escape_v = 512;
        this.fly_top_range = 4096;
        this.shakeFrame = 0;
        this.shakeOffsetY = 0;
        this.rayOffsetY = 0;
        this.circleOffsetY = 0;
        this.posX = 167680;
        this.posY = BOSS_APPEAR_POSY;
        this.enter_cn = 0;
        this.armStartPosY = 0;
        this.armDegreeLock = false;
        if (cabinAni == null) {
            cabinAni = new Animation("/animation/bossf3_cabin");
        }
        this.cabinDrawer = cabinAni.getDrawer(0, true, 0);
        if (circleAni == null) {
            circleAni = new Animation("/animation/bossf3_circle");
        }
        this.circleDrawer = circleAni.getDrawer(0, true, 0);
        if (faceAni == null) {
            faceAni = new Animation("/animation/bossf3_face");
        }
        this.faceDrawer = faceAni.getDrawer(0, true, 0);
        if (chainAni == null) {
            chainAni = new Animation("/animation/bossf3_chain");
        }
        this.chainDrawer = chainAni.getDrawer(0, true, 0);
        if (boatAni == null) {
            boatAni = new Animation("/animation/pod_boat");
        }
        this.boatdrawer = boatAni.getDrawer(0, true, 0);
        if (escapefaceAni == null) {
            escapefaceAni = new Animation("/animation/pod_face");
        }
        this.escapefacedrawer = escapefaceAni.getDrawer(0, false, 0);
        this.circle = new BossF3Circle(x, y, this);
        this.defence = new BossF3Defence(x, y, this);
        this.displayFlag = false;
        this.state = 0;
        setBossHP();
    }

    public static void releaseAllResource() {
        Animation.closeAnimation(cabinAni);
        Animation.closeAnimation(circleAni);
        Animation.closeAnimation(faceAni);
        cabinAni = null;
        circleAni = null;
        faceAni = null;
    }

    public void close() {
        this.cabinDrawer = null;
        this.circleDrawer = null;
        this.faceDrawer = null;
        this.circle = null;
        this.defence = null;
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (!this.dead) {
            if (this.arm != null && this.arm.getCaughtState()) {
                return;
            }
            if ((this.circle_state == 0 && !object.canBeHurt()) || this.state == 3 || this.state == 4 || this.state != 2 || object != player) {
                return;
            }
            if (!player.isAttackingEnemy() || this.pro_state == 0) {
                if (this.state != 3 && this.state != 4 && this.face_state != 3 && this.state == 2) {
                    player.beHurt();
                    this.face_state = 2;
                }
            } else if (this.face_state != 3) {
                this.HP--;
                player.doBossAttackPose(this, direction);
                this.circle.setAvaliable(false);
                this.defence.setAvaliable(false);
                if (this.arm != null) {
                    this.arm.setAvaliable(false);
                }
                if (this.HP > 0) {
                    this.face_state = 3;
                    this.face_cnt = 0;
                } else {
                    this.state = 3;
                    this.arm_drop_vely = 0;
                    this.drop_vely = 0;
                    this.shakeOffsetY = 0;
                    this.rayOffsetY = 0;
                    this.circleOffsetY = 0;
                    this.bossbroken = new BossBroken(30, this.posX >> 6, this.posY >> 6, 0, 0, 0, 0);
                    GameObject.addGameObject(this.bossbroken, this.posX >> 6, this.posY >> 6);
                    this.bossbroken.setTotalCntMax(60);
                    if (StageManager.isGoingToExtraStage()) {
                        MapManager.setCameraUpLimit(0);
                        player.setTerminal(3);
                    } else {
                        MapManager.setCameraUpLimit(0);
                        MapManager.setMapLoop(33, 37);
                        player.setTerminal(2);
                    }
                    this.face_state = 0;
                    player.setMeetingBoss(false);
                    if (this.circle_state == 0) {
                        this.circle_state = 5;
                    }
                    this.brokenFrame = 0;
                }
                if (this.HP == 0) {
                    SoundSystem.getInstance().playSe(35);
                } else {
                    SoundSystem.getInstance().playSe(34);
                }
            }
        }
    }

    public void doWhileBeAttack(PlayerObject object, int direction, int animationID) {
        if (this.state != 3 && this.state != 4) {
            if ((this.circle_state != 0 || object.canBeHurt()) && this.state == 2 && this.pro_state != 0 && this.face_state != 3) {
                this.HP--;
                player.doBossAttackPose(this, direction);
                this.circle.setAvaliable(false);
                this.defence.setAvaliable(false);
                if (this.arm != null) {
                    this.arm.setAvaliable(false);
                }
                if (this.HP > 0) {
                    this.face_state = 3;
                    this.face_cnt = 0;
                } else {
                    this.state = 3;
                    this.arm_drop_vely = 0;
                    this.drop_vely = 0;
                    this.shakeOffsetY = 0;
                    this.rayOffsetY = 0;
                    this.circleOffsetY = 0;
                    this.bossbroken = new BossBroken(30, this.posX >> 6, this.posY >> 6, 0, 0, 0, 0);
                    GameObject.addGameObject(this.bossbroken, this.posX >> 6, this.posY >> 6);
                    this.bossbroken.setTotalCntMax(60);
                    if (StageManager.isGoingToExtraStage()) {
                        MapManager.setCameraUpLimit(0);
                        player.setTerminal(3);
                    } else {
                        MapManager.setCameraUpLimit(0);
                        MapManager.setMapLoop(33, 37);
                        player.setTerminal(2);
                    }
                    this.face_state = 0;
                    player.setMeetingBoss(false);
                    if (this.circle_state == 0) {
                        this.circle_state = 5;
                    }
                    this.brokenFrame = 0;
                }
                if (this.HP == 0) {
                    SoundSystem.getInstance().playSe(35);
                } else {
                    SoundSystem.getInstance().playSe(34);
                }
            }
        }
    }

    private void changeAniState(AnimationDrawer AniDrawer, int state) {
        if (this.velocity > 0) {
            AniDrawer.setActionId(state);
            AniDrawer.setTrans(2);
            AniDrawer.setLoop(true);
            return;
        }
        AniDrawer.setActionId(state);
        AniDrawer.setTrans(0);
        AniDrawer.setLoop(true);
    }

    private boolean isCircleNoneUpDefenceState() {
        if (this.pro_state == 1 || this.pro_state == 2 || this.pro_state == 3 || this.pro_state == 4) {
            return true;
        }
        return false;
    }

    private void actionInit() {
        if (MyRandom.nextInt(0, 100) <= 85) {
            this.pro_state = 0;
            this.wait_frame_max = (MyRandom.nextInt(0, 10) * 4) + 32;
            this.wait_frame_cn = 0;
            return;
        }
        this.pro_state = 1;
        this.wait_frame_max = (MyRandom.nextInt(0, 10) * 4) + 32;
        this.wait_frame_cn = 0;
    }

    private void setActionMode() {
        int random;
        if (player.collisionState == (byte) 1) {
            random = MyRandom.nextInt(0, 100);
            if (random < 15) {
                this.attack_state = 4;
                return;
            } else if (random < 65) {
                this.attack_state = 2;
                return;
            } else {
                this.attack_state = 3;
                return;
            }
        }
        random = MyRandom.nextInt(0, 100);
        if (random < 50) {
            this.attack_state = 4;
        } else if (random < 75) {
            this.attack_state = 2;
        } else {
            this.attack_state = 3;
        }
    }

    private void playerLoseRings() {
        PlayerObject playerObject = player;
        if (PlayerObject.getRingNum() > 5) {
            player.loseRing(5);
            playerObject = player;
            playerObject = player;
            PlayerObject.setRingNum(PlayerObject.getRingNum() - 5);
            return;
        }
        playerObject = player;
        if (PlayerObject.getRingNum() > 0) {
            playerObject = player;
            PlayerObject playerObject2 = player;
            playerObject.loseRing(PlayerObject.getRingNum());
            playerObject = player;
            PlayerObject.setRingNum(0);
        }
    }

    public void logic() {
        if (!this.dead) {
            int preX = this.posX;
            int preY = this.posY;
            if (this.state > 0 && this.state < 4) {
                isBossEnter = true;
            } else if (this.state == 4) {
                isBossEnter = false;
            }
            switch (this.state) {
                case 0:
                    if (player.getFootPositionX() >= INIT_STOP_POSX) {
                        MapManager.setCameraLeftLimit(SIDE_LEFT);
                        MapManager.setCameraRightLimit(SIDE_RIGHT);
                    }
                    if (player.getFootPositionX() >= INIT_STOP_POSX && player.getFootPositionY() >= INIT_STOP_POSY1 && player.getFootPositionY() <= INIT_STOP_POSY2) {
                        this.state = 1;
                        MapManager.setCameraUpLimit(SIDE_UP);
                        MapManager.setCameraDownLimit(SIDE_DOWN);
                        MapManager.setCameraLeftLimit(SIDE_LEFT);
                        MapManager.setCameraRightLimit(SIDE_RIGHT);
                        this.enter_cn = 0;
                        this.isMachineShake = false;
                        this.circleState = 0;
                        if (!this.IsPlayBossBattleBGM) {
                            bossFighting = true;
                            bossID = 30;
                            SoundSystem.getInstance().playBgm(19, true);
                            this.IsPlayBossBattleBGM = true;
                            break;
                        }
                    }
                    break;
                case 1:
                    this.enter_cn++;
                    if (this.enter_cn == 11) {
                        MapManager.setShake(30);
                    }
                    if (this.enter_cn == 13) {
                        this.displayFlag = true;
                    }
                    if (this.enter_cn >= 20) {
                        this.posY -= 240;
                        if (this.posY <= BOSS_FIRST_STOP_POSY) {
                            this.posY = BOSS_FIRST_STOP_POSY;
                        }
                    }
                    if (this.enter_cn == 44) {
                        this.isMachineShake = true;
                    }
                    if (this.enter_cn == 56) {
                        this.circle_state = 5;
                    }
                    if (this.enter_cn == 66) {
                        this.circleState = 1;
                    }
                    if (this.enter_cn == 76) {
                        this.face_state = 1;
                    }
                    if (this.enter_cn == 92) {
                        this.face_state = 0;
                        this.circleState = 2;
                    }
                    if (this.enter_cn == 97) {
                        this.circle_state = 0;
                        this.state = 2;
                        actionInit();
                        break;
                    }
                    break;
                case 2:
                    if (this.face_state != 0) {
                        if (this.face_cnt < 8) {
                            this.face_cnt++;
                            if (this.circle_state != 0) {
                                if (this.pro_state == 2) {
                                    this.circle_state = 2;
                                }
                                if (this.pro_state == 3) {
                                    this.circle_state = 5;
                                }
                                if (this.pro_state == 4) {
                                    this.circle_state = 8;
                                }
                            }
                        } else {
                            this.cabin_state = 0;
                            this.face_state = 0;
                            if (this.pro_state == 0 || this.pro_state == 1) {
                                this.circle_state = 0;
                            }
                            this.circle.setAvaliable(true);
                            this.defence.setAvaliable(true);
                            if (this.arm != null) {
                                this.arm.setAvaliable(false);
                            }
                            this.face_cnt = 0;
                        }
                    }
                    switch (this.pro_state) {
                        case 0:
                            this.isMachineShake = true;
                            if (this.wait_frame_cn >= this.wait_frame_max) {
                                this.wait_frame_cn = 0;
                                this.pro_state = this.attack_state;
                                break;
                            }
                            this.wait_frame_cn++;
                            if (this.wait_frame_cn == 18) {
                                setActionMode();
                                break;
                            }
                            break;
                        case 1:
                            this.wait_frame_cn++;
                            if (this.wait_frame_cn == 25) {
                                this.circleState = 1;
                            }
                            if (this.wait_frame_cn == 31) {
                                if (this.posX == BOSS_HANG_LEFT_POSX) {
                                    this.velocity = BOSS_ARMSTATE_V;
                                } else if (this.posX == 167680) {
                                    this.velocity = -720;
                                }
                            }
                            if (this.wait_frame_cn > 31 && Math.abs(this.velocity) != ARM_UNCAUGHT_BACK_V) {
                                this.posX += this.velocity;
                            }
                            if (this.wait_frame_cn > 31) {
                                if (this.posX > 172672) {
                                    this.velocity = -480;
                                    this.circleDrawer.setTrans(0);
                                    this.faceDrawer.setTrans(0);
                                } else if (this.posX < 147840) {
                                    this.velocity = ARM_UNCAUGHT_BACK_V;
                                    this.circleDrawer.setTrans(2);
                                    this.faceDrawer.setTrans(2);
                                }
                                if (this.velocity == -480) {
                                    if (this.posX + this.velocity > 167680) {
                                        this.posX += this.velocity;
                                    } else {
                                        this.posX = 167680;
                                        this.pro_state = 0;
                                        this.wait_frame_max = (MyRandom.nextInt(0, 10) * 4) + 32;
                                        this.wait_frame_cn = 0;
                                        this.circleState = 2;
                                    }
                                }
                                if (this.velocity == ARM_UNCAUGHT_BACK_V) {
                                    if (this.posX + this.velocity >= BOSS_HANG_LEFT_POSX) {
                                        this.posX = BOSS_HANG_LEFT_POSX;
                                        this.pro_state = 0;
                                        this.wait_frame_max = (MyRandom.nextInt(0, 10) * 4) + 32;
                                        this.wait_frame_cn = 0;
                                        this.circleState = 2;
                                        break;
                                    }
                                    this.posX += this.velocity;
                                    break;
                                }
                            }
                            break;
                        case 2:
                            this.wait_frame_cn++;
                            if (this.wait_frame_cn == 3) {
                                this.circle_state = 2;
                                this.circleState = 1;
                            }
                            if (this.wait_frame_cn == 9) {
                                this.circle_state = 3;
                                this.isMachineShake = false;
                                this.rayOffsetY = 256;
                                SoundSystem.getInstance().playSe(61);
                                BulletObject.addBullet(21, this.posX + (this.posX == BOSS_HANG_LEFT_POSX ? 2048 : -2048), ((this.posY + 0) + PlayerSonic.BACK_JUMP_SPEED_X) + this.rayOffsetY, this.posX == BOSS_HANG_LEFT_POSX ? 1 : 0, 0);
                            }
                            if (this.wait_frame_cn == 17) {
                                this.isMachineShake = true;
                                this.circle_state = 2;
                                this.rayOffsetY = 0;
                            }
                            if (this.wait_frame_cn == 20) {
                                this.circle_state = 0;
                                this.circleState = 2;
                            }
                            if (this.wait_frame_cn == 23) {
                                actionInit();
                                break;
                            }
                            break;
                        case 3:
                            this.wait_frame_cn++;
                            if (this.wait_frame_cn == 3) {
                                this.circle_state = 5;
                                this.circleState = 1;
                                this.recoil_offsetx = 0;
                            }
                            if (this.wait_frame_cn == 13) {
                                this.isMachineShake = false;
                                this.circle_state = 6;
                            }
                            if (this.wait_frame_cn == 14) {
                                BulletObject.addBullet(20, this.posX + (this.posX == BOSS_HANG_LEFT_POSX ? 2048 : -2048), this.posY, MyRandom.nextInt(0, 10) > 5 ? 1 : 0, this.posX == BOSS_HANG_LEFT_POSX ? 1 : 0);
                                SoundSystem.getInstance().playSe(36);
                            }
                            if (this.wait_frame_cn >= 14 && this.wait_frame_cn < 16) {
                                this.recoil_offsetx += this.posX == BOSS_HANG_LEFT_POSX ? Def.TOUCH_HELP_LEFT_X : 128;
                            }
                            if (this.wait_frame_cn >= 16 && this.wait_frame_cn < 18) {
                                this.recoil_offsetx -= this.posX == BOSS_HANG_LEFT_POSX ? 128 : Def.TOUCH_HELP_LEFT_X;
                            }
                            if (this.wait_frame_cn == 16) {
                                this.isMachineShake = true;
                                this.circle_state = 5;
                            }
                            if (this.wait_frame_cn == 21) {
                                this.recoil_offsetx = 0;
                                this.circleState = 2;
                            }
                            if (this.wait_frame_cn == 24) {
                                actionInit();
                                this.circle_state = 0;
                                break;
                            }
                            break;
                        case 4:
                            this.wait_frame_cn++;
                            if (!player.isDead) {
                                if (this.wait_frame_cn == 3) {
                                    this.circle_state = 8;
                                    this.circleState = 1;
                                    this.isinArmRange = false;
                                    this.isMachineShake = false;
                                    this.aim_max = 48;
                                    this.isArmUncaughtEnd = false;
                                    this.isArmCaughtEnd = false;
                                    this.boss_hang_posx = 0;
                                    this.armDegreeLock = true;
                                    this.isAiming = false;
                                    if (this.posX == BOSS_HANG_LEFT_POSX) {
                                        this.degree = 1;
                                    }
                                }
                                if (this.wait_frame_cn == 9) {
                                    this.arm_posx = 0;
                                    this.arm_posy = 0;
                                    this.arm = new BossF3Arm(this.posX + this.arm_posx, this.posY + this.arm_posy, this.posX == BOSS_HANG_LEFT_POSX ? 1 : 0);
                                    this.arm_velx = this.posX == BOSS_HANG_LEFT_POSX ? MDPhone.SCREEN_HEIGHT : -640;
                                    this.arm_vely = 0;
                                    this.circle_state = 9;
                                }
                                if (this.wait_frame_cn >= 9 && this.wait_frame_cn < 13) {
                                    if (this.posX == BOSS_HANG_LEFT_POSX) {
                                        if (this.arm_posx + this.arm_velx >= this.arm_velx * 4) {
                                            this.arm_posx = this.arm_velx * 4;
                                            this.armDegreeLock = false;
                                        } else {
                                            this.arm_posx += this.arm_velx;
                                        }
                                    } else if (this.arm_posx + this.arm_velx <= this.arm_velx * 4) {
                                        this.arm_posx = this.arm_velx * 4;
                                        this.armDegreeLock = false;
                                    } else {
                                        this.arm_posx += this.arm_velx;
                                    }
                                }
                                if (this.wait_frame_cn == 13) {
                                    this.aim_max = MyRandom.nextInt(0, 100) < 80 ? 48 : 16;
                                    this.isAiming = true;
                                }
                                if (this.wait_frame_cn == (this.aim_max + 13) - 1) {
                                    if (this.arm != null) {
                                        this.arm.setAvaliable(true);
                                    }
                                    this.isAiming = false;
                                }
                                if (this.wait_frame_cn == this.aim_max + 13) {
                                    int offsetx;
                                    int offsety;
                                    int sqrtv;
                                    if (this.posX == BOSS_HANG_LEFT_POSX) {
                                        if (player.getFootPositionX() - this.posX > 0) {
                                            offsetx = player.getFootPositionX() - this.posX;
                                            offsety = player.getFootPositionY() - this.posY;
                                            sqrtv = MFMath.sqrt((offsetx * offsetx) + (offsety * offsety)) >> 6;
                                            this.arm_velx = (offsetx * ARM_OUT_V) / sqrtv;
                                            this.arm_vely = (offsety * ARM_OUT_V) / sqrtv;
                                            this.arm_pre_velx = this.arm_velx;
                                            this.arm_pre_vely = this.arm_vely;
                                            this.isinArmRange = true;
                                        } else {
                                            this.arm_pre_velx = 0;
                                            this.arm_velx = 0;
                                            this.arm_pre_vely = 0;
                                            this.arm_vely = 0;
                                            this.isinArmRange = false;
                                        }
                                    } else if (player.getFootPositionX() - this.posX < 0) {
                                        offsetx = player.getFootPositionX() - this.posX;
                                        offsety = player.getFootPositionY() - this.posY;
                                        sqrtv = MFMath.sqrt((offsetx * offsetx) + (offsety * offsety)) >> 6;
                                        this.arm_velx = (offsetx * ARM_OUT_V) / sqrtv;
                                        this.arm_vely = (offsety * ARM_OUT_V) / sqrtv;
                                        this.arm_pre_velx = this.arm_velx;
                                        this.arm_pre_vely = this.arm_vely;
                                        this.isinArmRange = true;
                                    } else {
                                        this.arm_pre_velx = 0;
                                        this.arm_velx = 0;
                                        this.arm_pre_vely = 0;
                                        this.arm_vely = 0;
                                        this.isinArmRange = false;
                                    }
                                    this.isCaught = false;
                                    this.arm_attack_step = 1;
                                    SoundSystem.getInstance().playSe(83);
                                }
                                if (this.wait_frame_cn > this.aim_max + 13 && !this.isCaught && !this.isinArmRange && this.arm_attack_step == 1) {
                                    this.isinArmRange = false;
                                    this.arm_attack_step = 3;
                                }
                                if (this.wait_frame_cn > this.aim_max + 13 && !this.isCaught && this.isinArmRange && this.arm_attack_step == 1) {
                                    if (this.posX == BOSS_HANG_LEFT_POSX) {
                                        if (this.arm.getCaughtState()) {
                                            this.arm_posx = player.getFootPositionX() - this.posX;
                                            this.arm_posy = (player.getFootPositionY() - this.posY) - (player.getCollisionRect().getHeight() >> 1);
                                            this.isCaught = true;
                                            this.arm_attack_step = 2;
                                        } else {
                                            this.arm_posx += this.arm_velx;
                                            this.arm_posy += this.arm_vely;
                                            if (((this.posX + this.arm_posx) + this.arm_velx) - BarHorbinV.COLLISION_WIDTH >= player.getFootPositionX()) {
                                                this.isinArmRange = false;
                                                this.arm_attack_step = 2;
                                                this.arm.setCatchState(1);
                                            }
                                        }
                                    } else if (this.arm.getCaughtState()) {
                                        this.arm_posx = player.getFootPositionX() - this.posX;
                                        this.arm_posy = (player.getFootPositionY() - this.posY) - (player.getCollisionRect().getHeight() >> 1);
                                        this.isCaught = true;
                                        this.arm_attack_step = 2;
                                    } else {
                                        this.arm_posx += this.arm_velx;
                                        this.arm_posy += this.arm_vely;
                                        if (((this.posX + this.arm_posx) + this.arm_velx) + BarHorbinV.COLLISION_WIDTH <= player.getFootPositionX()) {
                                            this.isinArmRange = false;
                                            this.arm_attack_step = 2;
                                            this.arm.setCatchState(1);
                                        }
                                    }
                                }
                                if (this.wait_frame_cn > (this.aim_max + 13) + 3 && this.arm_attack_step == 2) {
                                    this.arm_attack_step = 3;
                                    this.arm.setAniState(1);
                                }
                                if (this.wait_frame_cn > (this.aim_max + 13) + 3 && this.arm_attack_step == 3 && !this.isArmUncaughtEnd && !this.isArmCaughtEnd) {
                                    if (this.isCaught) {
                                        this.arm_velx = ((-this.arm_pre_velx) * ARM_CAUGHT_BACK_V) / ARM_OUT_V;
                                        this.arm_vely = ((-this.arm_pre_vely) * ARM_CAUGHT_BACK_V) / ARM_OUT_V;
                                        this.arm_posx += this.arm_velx;
                                        this.arm_posy += this.arm_vely;
                                        if (this.posX == BOSS_HANG_LEFT_POSX) {
                                            if (this.arm_posx <= 4864) {
                                                this.arm_posx = 4864;
                                                this.arm_posy = 0;
                                                this.isArmCaughtEnd = true;
                                                this.isArmBack = false;
                                                this.radius = 2304;
                                                this.wait_frame_offset_cn = 0;
                                                this.isArmMove = false;
                                                this.arm.setShakeState(true);
                                                this.boss_hang_posx = BOSS_HANG_LEFT_POSX;
                                            }
                                        } else if (this.arm_posx >= (-4864)) {
                                            this.arm_posx = -4864;
                                            this.arm_posy = 0;
                                            this.isArmCaughtEnd = true;
                                            this.isArmBack = false;
                                            this.radius = 2304;
                                            this.wait_frame_offset_cn = 0;
                                            this.isArmMove = false;
                                            this.arm.setShakeState(true);
                                            this.boss_hang_posx = 167680;
                                        }
                                    } else {
                                        if (!(this.arm_posx == 0 && this.arm_posy == 0)) {
                                            this.arm_velx = ((-this.arm_pre_velx) * ARM_UNCAUGHT_BACK_V) / ARM_OUT_V;
                                            this.arm_vely = ((-this.arm_pre_vely) * ARM_UNCAUGHT_BACK_V) / ARM_OUT_V;
                                            if (this.degree == 0) {
                                                if (this.posX == 167680 && this.arm_velx < 256) {
                                                    this.arm_velx = 256;
                                                }
                                                if (this.posX == BOSS_HANG_LEFT_POSX && this.arm_velx > -256) {
                                                    this.arm_velx = GimmickObject.PLATFORM_OFFSET_Y;
                                                }
                                            }
                                            this.arm_posx += this.arm_velx;
                                            if (this.arm_vely > 0) {
                                                if (this.arm_posy < 0) {
                                                    this.arm_posy += this.arm_vely;
                                                } else {
                                                    this.arm_posy = 0;
                                                    this.armDegreeLock = true;
                                                    if (this.posX == BOSS_HANG_LEFT_POSX) {
                                                        this.degree = 0;
                                                    } else {
                                                        this.degree = 0;
                                                    }
                                                }
                                            } else if (this.arm_vely < 0) {
                                                if (this.arm_posy > 0) {
                                                    this.arm_posy += this.arm_vely;
                                                } else {
                                                    this.arm_posy = 0;
                                                    this.armDegreeLock = true;
                                                    if (this.posX == BOSS_HANG_LEFT_POSX) {
                                                        this.degree = 0;
                                                    } else {
                                                        this.degree = 0;
                                                    }
                                                }
                                            }
                                        }
                                        if (this.posX == BOSS_HANG_LEFT_POSX) {
                                            if (this.arm_velx == 0 && this.arm_posx > 0) {
                                                this.arm_posx -= 480;
                                            }
                                            if (this.arm_posx <= 0) {
                                                this.arm_posx = 0;
                                                this.arm_posy = 0;
                                                this.isArmUncaughtEnd = true;
                                                this.wait_frame_offset_cn = 0;
                                                if (this.arm_posy == this.armStartPosY) {
                                                    this.armDegreeLock = true;
                                                    this.degree = 0;
                                                }
                                            }
                                            if (player.getFootPositionX() <= BOSS_HANG_LEFT_POSX) {
                                                this.arm_posy = 0;
                                                this.wait_frame_offset_cn = 0;
                                                if (this.arm_posy == this.armStartPosY) {
                                                    this.armDegreeLock = true;
                                                    this.degree = 0;
                                                }
                                            }
                                        } else {
                                            if (this.arm_velx == 0 && this.arm_posx < 0) {
                                                this.arm_posx += ARM_UNCAUGHT_BACK_V;
                                            }
                                            if (this.arm_posx >= 0) {
                                                this.arm_posx = 0;
                                                this.arm_posy = 0;
                                                this.isArmUncaughtEnd = true;
                                                this.wait_frame_offset_cn = 0;
                                                if (this.arm_posy == this.armStartPosY) {
                                                    this.armDegreeLock = true;
                                                    this.degree = 0;
                                                }
                                            }
                                            if (player.getFootPositionX() >= 167680) {
                                                this.arm_posy = 0;
                                                this.wait_frame_offset_cn = 0;
                                                if (this.arm_posy == this.armStartPosY) {
                                                    this.armDegreeLock = true;
                                                    this.degree = 0;
                                                }
                                            }
                                        }
                                    }
                                }
                                if (this.isArmUncaughtEnd) {
                                    this.wait_frame_offset_cn++;
                                    if (this.wait_frame_offset_cn == 13) {
                                        this.circleState = 2;
                                    }
                                    if (this.wait_frame_offset_cn == 16) {
                                        this.circle_state = 0;
                                        this.arm = null;
                                        this.arm_attack_step = 0;
                                        actionInit();
                                    }
                                }
                                if (this.isArmCaughtEnd) {
                                    this.wait_frame_offset_cn++;
                                    int i;
                                    if (this.boss_hang_posx == BOSS_HANG_LEFT_POSX) {
                                        if (!this.isArmBack) {
                                            i = 0;
                                            while (i < 5) {
                                                if (this.wait_frame_offset_cn > (i * 10) + 0 && this.wait_frame_offset_cn <= (i * 10) + 5) {
                                                    this.arm_posx = (((this.radius * MyAPI.dCos(MDPhone.SCREEN_WIDTH - ((this.wait_frame_offset_cn - (i * 10)) * 18))) / 100) + 512) + 1280;
                                                    this.shakechainx = (((MyAPI.dCos(MDPhone.SCREEN_WIDTH - ((this.wait_frame_offset_cn - (i * 10)) * 18)) * 768) / 100) + 512) + 1280;
                                                    this.arm_posy = (this.radius * MyAPI.dSin(MDPhone.SCREEN_WIDTH - ((this.wait_frame_offset_cn - (i * 10)) * 18))) / 100;
                                                    this.shakechainy = (MyAPI.dSin(MDPhone.SCREEN_WIDTH - ((this.wait_frame_offset_cn - (i * 10)) * 18)) * 768) / 100;
                                                } else if (this.wait_frame_offset_cn > (i * 10) + 5 && this.wait_frame_offset_cn <= (i * 10) + 10) {
                                                    this.arm_posx = (((this.radius * MyAPI.dCos(MDPhone.SCREEN_WIDTH - (90 - (((this.wait_frame_offset_cn - (i * 10)) - 5) * 18)))) / 100) + 512) + 1280;
                                                    this.shakechainx = (((MyAPI.dCos(MDPhone.SCREEN_WIDTH - (90 - (((this.wait_frame_offset_cn - (i * 10)) - 5) * 18))) * 768) / 100) + 512) + 1280;
                                                    this.arm_posy = (this.radius * MyAPI.dSin(MDPhone.SCREEN_WIDTH - (90 - (((this.wait_frame_offset_cn - (i * 10)) - 5) * 18)))) / 100;
                                                    this.shakechainy = (MyAPI.dSin(MDPhone.SCREEN_WIDTH - (90 - (((this.wait_frame_offset_cn - (i * 10)) - 5) * 18))) * 768) / 100;
                                                    if (this.wait_frame_offset_cn == (i * 10) + 9 && this.wait_frame_offset_cn < 45) {
                                                        playerLoseRings();
                                                    }
                                                }
                                                if (this.wait_frame_offset_cn == 35) {
                                                    this.isArmMove = true;
                                                }
                                                if (this.wait_frame_offset_cn == 45) {
                                                    this.arm.setAvaliable(false);
                                                    this.arm.setCaughtFlag(false);
                                                    this.arm.releasePlayer();
                                                }
                                                if (this.isArmMove) {
                                                    this.posX += 144;
                                                }
                                                i++;
                                            }
                                        }
                                        if (this.wait_frame_offset_cn == 50) {
                                            this.arm_posx = 0;
                                            this.arm_posy = 0;
                                            this.shakechainx = 0;
                                            this.shakechainy = 0;
                                            this.circle_state = 8;
                                            this.isArmBack = false;
                                        }
                                        if (this.wait_frame_offset_cn > 50 && !this.isArmBack) {
                                            this.posX += BOSS_ARMSTATE_V;
                                            if (this.posX >= 172672) {
                                                this.posX = 172672;
                                                this.isArmBack = true;
                                                this.degree = 0;
                                            }
                                        }
                                        if (this.isArmBack) {
                                            this.posX -= ARM_UNCAUGHT_BACK_V;
                                            if (this.posX <= 167680) {
                                                this.posX = 167680;
                                                this.velocity = -480;
                                                this.circle_state = 0;
                                                this.circleDrawer.setTrans(0);
                                                this.faceDrawer.setTrans(0);
                                                this.pro_state = 0;
                                                this.wait_frame_max = (MyRandom.nextInt(0, 10) * 4) + 32;
                                                this.wait_frame_cn = 0;
                                                this.circleState = 2;
                                                this.arm_attack_step = 0;
                                                this.wait_frame_offset_cn = 0;
                                                this.isinArmRange = false;
                                                this.isMachineShake = false;
                                                this.aim_max = 48;
                                                this.isArmUncaughtEnd = false;
                                                this.isArmCaughtEnd = false;
                                                this.arm = null;
                                            }
                                        }
                                    } else if (this.boss_hang_posx == 167680) {
                                        if (!this.isArmBack) {
                                            i = 0;
                                            while (i < 5) {
                                                if (this.wait_frame_offset_cn > (i * 10) + 0 && this.wait_frame_offset_cn <= (i * 10) + 5) {
                                                    this.arm_posx = ((-((this.radius * MyAPI.dCos(MDPhone.SCREEN_WIDTH - ((this.wait_frame_offset_cn - (i * 10)) * 18))) / 100)) - 512) - 1280;
                                                    this.shakechainx = ((-((MyAPI.dCos(MDPhone.SCREEN_WIDTH - ((this.wait_frame_offset_cn - (i * 10)) * 18)) * 768) / 100)) - 512) - 1280;
                                                    this.arm_posy = (this.radius * MyAPI.dSin(MDPhone.SCREEN_WIDTH - ((this.wait_frame_offset_cn - (i * 10)) * 18))) / 100;
                                                    this.shakechainy = (MyAPI.dSin(MDPhone.SCREEN_WIDTH - ((this.wait_frame_offset_cn - (i * 10)) * 18)) * 768) / 100;
                                                } else if (this.wait_frame_offset_cn > (i * 10) + 5 && this.wait_frame_offset_cn <= (i * 10) + 10) {
                                                    this.arm_posx = ((-((this.radius * MyAPI.dCos(MDPhone.SCREEN_WIDTH - (90 - (((this.wait_frame_offset_cn - (i * 10)) - 5) * 18)))) / 100)) - 512) - 1280;
                                                    this.shakechainx = ((-((MyAPI.dCos(MDPhone.SCREEN_WIDTH - (90 - (((this.wait_frame_offset_cn - (i * 10)) - 5) * 18))) * 768) / 100)) - 512) - 1280;
                                                    this.arm_posy = (this.radius * MyAPI.dSin(MDPhone.SCREEN_WIDTH - (90 - (((this.wait_frame_offset_cn - (i * 10)) - 5) * 18)))) / 100;
                                                    this.shakechainy = (MyAPI.dSin(MDPhone.SCREEN_WIDTH - (90 - (((this.wait_frame_offset_cn - (i * 10)) - 5) * 18))) * 768) / 100;
                                                    if (this.wait_frame_offset_cn == (i * 10) + 9 && this.wait_frame_offset_cn < 45) {
                                                        playerLoseRings();
                                                    }
                                                }
                                                if (this.wait_frame_offset_cn == 35) {
                                                    this.isArmMove = true;
                                                }
                                                if (this.wait_frame_offset_cn == 45) {
                                                    this.arm.setAvaliable(false);
                                                    this.arm.setCaughtFlag(false);
                                                    this.arm.releasePlayer();
                                                }
                                                if (this.isArmMove) {
                                                    this.posX -= 144;
                                                }
                                                i++;
                                            }
                                        }
                                        if (this.wait_frame_offset_cn == 50) {
                                            this.arm_posx = 0;
                                            this.arm_posy = 0;
                                            this.shakechainx = 0;
                                            this.shakechainy = 0;
                                            this.circle_state = 8;
                                        }
                                        if (this.wait_frame_offset_cn > 50 && !this.isArmBack) {
                                            this.posX -= BOSS_ARMSTATE_V;
                                            if (this.posX <= 147840) {
                                                this.posX = 147840;
                                                this.isArmBack = true;
                                                this.degree = 0;
                                            }
                                        }
                                        if (this.isArmBack) {
                                            this.posX += ARM_UNCAUGHT_BACK_V;
                                            if (this.posX >= BOSS_HANG_LEFT_POSX) {
                                                this.posX = BOSS_HANG_LEFT_POSX;
                                                this.velocity = ARM_UNCAUGHT_BACK_V;
                                                this.circle_state = 0;
                                                this.circleDrawer.setTrans(2);
                                                this.faceDrawer.setTrans(2);
                                                this.pro_state = 0;
                                                this.wait_frame_max = (MyRandom.nextInt(0, 10) * 4) + 32;
                                                this.wait_frame_cn = 0;
                                                this.circleState = 2;
                                                this.arm_attack_step = 0;
                                                this.wait_frame_offset_cn = 0;
                                                this.isinArmRange = false;
                                                this.isMachineShake = false;
                                                this.aim_max = 48;
                                                this.isArmUncaughtEnd = false;
                                                this.isArmCaughtEnd = false;
                                                this.arm = null;
                                            }
                                        }
                                    }
                                }
                                if (this.arm != null) {
                                    this.arm.logic(this.posX + this.arm_posx, this.posY + this.arm_posy);
                                    if (!this.armDegreeLock) {
                                        if (this.isAiming) {
                                            if (this.posX == BOSS_HANG_LEFT_POSX) {
                                                this.degree = calArmDegree(this.posY - player.getFootPositionY(), (this.posX - 1280) - player.getFootPositionX()) - RollPlatformSpeedC.DEGREE_VELOCITY;
                                            } else if (this.posX == 167680) {
                                                this.degree = calArmDegree(this.posY - player.getFootPositionY(), (this.posX + 1280) - player.getFootPositionX());
                                            }
                                        }
                                        if (!this.isArmCaughtEnd || this.isArmBack) {
                                            if (this.boss_hang_posx == BOSS_HANG_LEFT_POSX) {
                                                this.degree = calArmDegree(this.arm_posy, this.arm_posx);
                                            } else if (this.boss_hang_posx == 167680) {
                                                this.degree = calArmDegree(this.arm_posy, this.arm_posx) - RollPlatformSpeedC.DEGREE_VELOCITY;
                                            }
                                        } else if (this.boss_hang_posx == BOSS_HANG_LEFT_POSX) {
                                            this.degree = calArmDegree(this.arm_posy, this.arm_posx - 1280);
                                        } else if (this.boss_hang_posx == 167680) {
                                            this.degree = calArmDegree(this.arm_posy, this.arm_posx + 1280) - RollPlatformSpeedC.DEGREE_VELOCITY;
                                        }
                                    }
                                    if (this.posX == 167680 && this.degree > 80 && this.degree < 280 && this.arm != null && !this.arm.getCaughtState()) {
                                        if (this.degree < 180) {
                                            this.degree = 80;
                                        } else {
                                            this.degree = 280;
                                        }
                                    }
                                    if (this.posX == BOSS_HANG_LEFT_POSX && !((this.degree > -80 && this.degree < 80) || this.arm == null || this.arm.getCaughtState())) {
                                        if (this.degree < 0) {
                                            this.degree = -80;
                                        } else {
                                            this.degree = 80;
                                        }
                                    }
                                    if (this.wait_frame_offset_cn <= 50) {
                                        this.arm.setDegree(this.degree);
                                        break;
                                    }
                                }
                            }
                            return;
                    }
                    this.circle.logic(this.posX + this.recoil_offsetx, ((this.posY + this.shakeOffsetY) + this.circleOffsetY) + this.rayOffsetY);
                    this.defence.logic(this.posX + this.recoil_offsetx, (this.posY + this.shakeOffsetY) + this.rayOffsetY);
                    if (this.circle.getHurtState() || this.defence.getHurtState()) {
                        this.face_state = 2;
                        this.face_cnt = 0;
                        this.circle.resetHurtState();
                        this.defence.resetHurtState();
                        break;
                    }
                    break;
                case 3:
                    if (this.arm != null) {
                        this.arm.logic(this.posX + this.arm_posx, this.posY + this.arm_posy);
                        if ((this.arm_posy + this.posY) + this.arm_drop_vely >= getGroundY(this.posX + this.arm_posx, this.posY + this.arm_posy) - 320) {
                            this.arm_posy = (getGroundY(this.posX + this.arm_posx, this.posY + this.arm_posy) - this.posY) - 320;
                            this.arm_drop_vely = (-this.arm_drop_vely) / 2;
                            if (this.arm_drop_vely > (-GRAVITY) / 2) {
                                this.arm_drop_vely = 0;
                                this.arm = null;
                            }
                        } else {
                            this.arm_posy += this.arm_drop_vely;
                            this.arm_drop_vely += GRAVITY / 2;
                        }
                    }
                    if ((((this.posY + this.shakeOffsetY) + this.rayOffsetY) + 1600) + this.drop_vely >= getGroundY(this.posX, (this.posY + this.shakeOffsetY) + this.rayOffsetY)) {
                        this.posY = getGroundY(this.posX, (this.posY + this.shakeOffsetY) + this.rayOffsetY) - ((this.shakeOffsetY + this.rayOffsetY) + 1600);
                        this.drop_vely = (-this.drop_vely) / 2;
                        if (this.drop_vely > (-GRAVITY) / 2) {
                            this.drop_vely = 0;
                        }
                    } else {
                        this.posY += this.drop_vely;
                        this.drop_vely += GRAVITY / 2;
                    }
                    this.brokenFrame++;
                    int cameraX = MapManager.getCamera().f13x;
                    int cameraY = MapManager.getCamera().f14y;
                    if (!StageManager.isGoingToExtraStage()) {
                        if (this.brokenFrame % 3 == 0) {
                            GameObject.addGameObject(new Boom(37, (MyRandom.nextInt(0, SCREEN_WIDTH) + cameraX) << 6, (MyRandom.nextInt(0, SCREEN_HEIGHT) + cameraY) << 6, 0, 0, 0, 0));
                        }
                        if (this.brokenFrame % 3 == 0) {
                            GameObject.addGameObject(new BreakingParts(38, (MyRandom.nextInt(0, SCREEN_WIDTH) + cameraX) << 6, (MyRandom.nextInt(0, SCREEN_HEIGHT) + cameraY) << 6, 0, 0, 0, 0));
                        }
                    }
                    if (StageManager.isGoingToExtraStage()) {
                        if (this.brokenFrame < 43) {
                            this.bossbroken.logicBoom(this.posX, this.posY);
                        }
                        if (this.brokenFrame == 43) {
                            GameObject.addGameObject(new Boom(37, this.posX + 1920, this.posY, 0, 0, 0, 0));
                            GameObject.addGameObject(new Boom(37, this.posX - 1920, this.posY, 0, 0, 0, 0));
                            GameObject.addGameObject(new Boom(37, this.posX, this.posY - 1920, 0, 0, 0, 0));
                            GameObject.addGameObject(new Boom(37, this.posX, this.posY + 1920, 0, 0, 0, 0));
                            soundInstance.playSe(35);
                        }
                    } else if (this.brokenFrame < 68) {
                        this.bossbroken.logicBoom(this.posX, this.posY);
                    } else if (this.brokenFrame % 1 == 0) {
                        int i2 = player.posX;
                        PlayerObject playerObject = player;
                        i2 = (i2 - (1024 * 2)) + ((MyRandom.nextInt(0, 60) - 30) << 6);
                        int i3 = player.posY;
                        PlayerObject playerObject2 = player;
                        GameObject.addGameObject(new Boom(37, i2, (i3 - (1536 / 2)) + ((MyRandom.nextInt(0, 60) - 30) << 6), 0, 0, 0, 0));
                        soundInstance.playSe(35);
                    }
                    if (this.brokenFrame == 48) {
                        if (!StageManager.isGoingToExtraStage()) {
                            player.setMeetingBoss(true);
                            bossFighting = false;
                            MapManager.setCameraRightLimit(MapManager.getPixelWidth());
                            break;
                        }
                        this.state = 4;
                        this.escapefacedrawer.setActionId(4);
                        this.escapefacedrawer.setLoop(true);
                        this.fly_top = this.posY;
                        this.fly_end = 169472;
                        this.wait_cnt = 0;
                        break;
                    }
                    break;
                case 4:
                    this.wait_cnt++;
                    if (this.wait_cnt >= this.wait_cnt_max && this.posY >= this.fly_top - this.fly_top_range) {
                        this.posY -= this.escape_v;
                    }
                    if (this.posY <= this.fly_top - this.fly_top_range && this.WaitCnt == 0) {
                        this.posY = this.fly_top - this.fly_top_range;
                        this.escapefacedrawer.setActionId(0);
                        this.boatdrawer.setActionId(1);
                        this.boatdrawer.setLoop(false);
                        this.WaitCnt = 1;
                    }
                    if (this.WaitCnt == 1 && this.boatdrawer.checkEnd()) {
                        this.escapefacedrawer.setActionId(0);
                        this.escapefacedrawer.setTrans(2);
                        this.escapefacedrawer.setLoop(true);
                        this.boatdrawer.setActionId(1);
                        this.boatdrawer.setTrans(2);
                        this.boatdrawer.setLoop(false);
                        this.WaitCnt = 2;
                    }
                    if (this.WaitCnt == 2 && this.boatdrawer.checkEnd()) {
                        this.boatdrawer.setActionId(0);
                        this.boatdrawer.setTrans(2);
                        this.boatdrawer.setLoop(true);
                        this.WaitCnt = 3;
                    }
                    if (this.WaitCnt == 3 || this.WaitCnt == 4) {
                        this.posX += this.escape_v;
                        if (this.posX - this.fly_end > this.fly_top_range) {
                            this.posY -= this.fly_top_range;
                        }
                    }
                    if (this.posX - this.fly_end > this.fly_top_range && this.WaitCnt == 3) {
                        player.setMeetingBoss(true);
                        bossFighting = false;
                        MapManager.setCameraRightLimit(MapManager.getPixelWidth() - 1);
                        break;
                    }
            }
            changeAniState(this.cabinDrawer, this.cabin_state);
            changeAniState(this.circleDrawer, this.circle_state);
            changeAniState(this.faceDrawer, this.face_state);
            refreshCollisionRect(this.posX + this.recoil_offsetx, (this.posY + this.shakeOffsetY) + this.rayOffsetY);
            checkWithPlayer(preX, preY, this.posX, this.posY);
        }
    }

    private int calArmDegree(int y, int x) {
        return (crlFP32.actTanDegree(y, x) + MDPhone.SCREEN_WIDTH) % MDPhone.SCREEN_WIDTH;
    }

    private int shakeOffsetY() {
        if (!this.isMachineShake || IsGamePause) {
            this.shakeOffsetY = 0;
            return 0;
        }
        this.shakeFrame++;
        if (this.shakeFrame >= 0 && this.shakeFrame < 8) {
            this.shakeOffsetY -= 32;
        } else if (this.shakeFrame == 8 || this.shakeFrame == 9) {
            this.shakeOffsetY = GimmickObject.PLATFORM_OFFSET_Y;
        } else if (this.shakeFrame < 24) {
            this.shakeOffsetY += 32;
        } else if (this.shakeFrame == 24 || this.shakeFrame == 25) {
            this.shakeOffsetY = 256;
        } else if (this.shakeFrame < 34) {
            this.shakeOffsetY -= 32;
        } else if (this.shakeFrame == 34) {
            this.shakeOffsetY = 0;
            this.shakeFrame = 0;
        }
        return this.shakeOffsetY;
    }

    private int circleOffsetY() {
        if (this.circleState == 0) {
            this.circleOffsetY = CIRCLE_FIRST_OFFSETPOSY;
            return CIRCLE_FIRST_OFFSETPOSY;
        } else if (this.circleState == 1) {
            this.circleOffsetY += this.state == 2 ? ARM_UNCAUGHT_BACK_V : 240;
            if (this.circleOffsetY >= 0) {
                this.circleOffsetY = 0;
            }
            return this.circleOffsetY;
        } else if (this.circleState == 2) {
            this.circleOffsetY -= this.state == 2 ? ARM_UNCAUGHT_BACK_V : 240;
            if (this.circleOffsetY <= CIRCLE_FIRST_OFFSETPOSY) {
                this.circleOffsetY = CIRCLE_FIRST_OFFSETPOSY;
            }
            return this.circleOffsetY;
        } else {
            this.circleOffsetY = 0;
            return 0;
        }
    }

    private void drawChain(MFGraphics g) {
        int num;
        int chain_y;
        int chain_x;
        int i;
        if (this.posX == BOSS_HANG_LEFT_POSX) {
            num = (MFMath.sqrt(((this.arm_posx - 2048) * (this.arm_posx - 2048)) + (this.arm_posy * this.arm_posy)) >> 6) / 1024;
            if (num > 0) {
                chain_y = this.arm_posy / num;
            } else {
                chain_y = 0;
            }
            if (num > 0) {
                chain_x = (this.arm_posx - 2048) / num;
            } else {
                chain_x = 0;
            }
            for (i = 0; i < num; i++) {
                drawInMap(g, this.chainDrawer, (((this.posX + 2048) - 1024) + (chain_x * i)) + (chain_x / 2), (this.posY + (chain_y * i)) + (chain_y / 2));
            }
            return;
        }
        num = (MFMath.sqrt((((-this.arm_posx) - 2048) * ((-this.arm_posx) - 2048)) + (this.arm_posy * this.arm_posy)) >> 6) / 1024;
        if (num > 0) {
            chain_y = this.arm_posy / num;
        } else {
            chain_y = 0;
        }
        if (num > 0) {
            chain_x = ((-this.arm_posx) - 2048) / num;
        } else {
            chain_x = 0;
        }
        for (i = 0; i < num; i++) {
            drawInMap(g, this.chainDrawer, (((this.posX - 2048) + 1024) - (chain_x * i)) - (chain_x / 2), (this.posY + (chain_y * i)) + (chain_y / 2));
        }
    }

    private void drawChainShake(MFGraphics g) {
        if (this.posX == BOSS_HANG_LEFT_POSX) {
            drawInMap(g, this.chainDrawer, (this.posX + 2048) - 512, this.posY);
            drawInMap(g, this.chainDrawer, this.posX + this.shakechainx, this.posY + this.shakechainy);
            return;
        }
        drawInMap(g, this.chainDrawer, (this.posX - 2048) + 512, this.posY);
        drawInMap(g, this.chainDrawer, this.posX + this.shakechainx, this.posY + this.shakechainy);
    }

    public void draw(MFGraphics g) {
        if (this.displayFlag && !this.dead) {
            if (this.state != 4) {
                if (this.state != 3) {
                    shakeOffsetY();
                    circleOffsetY();
                }
                if (this.arm != null) {
                    if (this.state != 3 && this.arm_attack_step > 0) {
                        if (this.isArmCaughtEnd) {
                            drawChainShake(g);
                        } else {
                            drawChain(g);
                        }
                    }
                    this.arm.draw(g);
                }
                drawInMap(g, this.cabinDrawer, this.posX + this.recoil_offsetx, (this.posY + this.shakeOffsetY) + this.rayOffsetY);
                drawInMap(g, this.faceDrawer, this.posX + this.recoil_offsetx, ((this.posY + FACE_OFFSETY) + this.shakeOffsetY) + this.rayOffsetY);
                drawInMap(g, this.circleDrawer, this.posX + this.recoil_offsetx, ((this.posY + this.shakeOffsetY) + this.circleOffsetY) + this.rayOffsetY);
                if (this.circle != null) {
                    this.circle.draw(g);
                }
                if (this.defence != null) {
                    this.defence.draw(g);
                }
            }
            if (this.state == 4) {
                drawInMap(g, this.boatdrawer, this.posX, this.posY);
                drawInMap(g, this.escapefacedrawer, this.posX, this.posY - Boss6Block.COLLISION2_HEIGHT);
            }
            if (this.state == 3 && this.bossbroken != null && (!StageManager.isGoingToExtraStage() || this.brokenFrame <= 43)) {
                this.bossbroken.draw(g);
            }
            drawCollisionRect(g);
        }
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 1344, (y - 1024) - 768, COLLISION_WIDTH, 1024);
    }
}
