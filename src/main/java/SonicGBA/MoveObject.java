package SonicGBA;

public abstract class MoveObject extends GameObject {
    protected int totalVelocity;

    public int getVelX() {
        return this.velX;
    }

    public int getVelY() {
        return this.velY;
    }

    public void setVelX(int mVelX) {
        this.velX = mVelX;
    }

    public void setVelY(int mVelY) {
        this.velY = mVelY;
    }
}
