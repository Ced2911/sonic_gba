package SonicGBA;

import Lib.Animation;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: BulletObject */
class RobotBullet extends BulletObject {
    private static final int COLLISION_HEIGHT = 512;
    private static final int COLLISION_WIDTH = 512;

    protected RobotBullet(int x, int y, int velX, int velY) {
        super(x, y, velX, velY, false);
        if (robotbulletAnimation == null) {
            robotbulletAnimation = new Animation("/animation/yukimal_bullet");
        }
        this.drawer = robotbulletAnimation.getDrawer(0, true, 0);
        this.posX = x;
        this.posY = y;
        this.velX = velX;
        this.velY = velY;
    }

    public void bulletLogic() {
        checkWithPlayer(this.posX, this.posY, this.posX, this.posY);
        this.posX += this.velX;
        this.velY += GRAVITY;
        this.posY += this.velY;
    }

    public void draw(MFGraphics g) {
        drawInMap(g, this.drawer);
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 256, y - 256, 512, 512);
    }

    public boolean chkDestroy() {
        return !isInCamera() || isFarAwayCamera();
    }
}
