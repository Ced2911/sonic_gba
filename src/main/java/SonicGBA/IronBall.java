package SonicGBA;

import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;

/* compiled from: GimmickObject */
class IronBall extends GimmickObject {
    private static final int COLLISION_HEIGHT = 1920;
    private static final int COLLISION_WIDTH = 1920;
    private static MFImage image;
    private int initPos;
    private boolean isH;
    private MoveCalculator moveCal = null;
    private int offset_distance;

    protected IronBall(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        boolean moveDirection;
        if (this.mWidth >= this.mHeight) {
            this.isH = true;
            if (this.iLeft == 0) {
                moveDirection = false;
            } else {
                moveDirection = true;
            }
        } else {
            this.isH = false;
            if (this.iTop == 0) {
                moveDirection = false;
            } else {
                moveDirection = true;
            }
        }
        this.initPos = this.isH ? this.posX : this.posY;
        if (this.moveCal == null) {
            this.moveCal = new MoveCalculator(this.isH ? this.posX : this.posY, this.isH ? this.mWidth : this.mHeight, moveDirection);
        }
        if (image != null) {
            return;
        }
        if (StageManager.getCurrentZoneId() != 6) {
            image = MFImage.createImage("/gimmick/iron_ball.png");
        } else {
            image = MFImage.createImage("/gimmick/iron_ball6.png");
        }
    }

    public static void staticLogic() {
    }

    public void logic() {
        int preX = this.posX;
        int preY = this.posY;
        if (this.isH) {
            if (this.iLeft == 0) {
                this.posX = this.moveCal.getPosition();
            } else {
                this.offset_distance = this.initPos - this.moveCal.getPosition();
                this.posX = this.initPos + this.offset_distance;
            }
        } else if (this.iTop == 0) {
            this.posY = this.moveCal.getPosition();
        } else {
            this.offset_distance = this.initPos - this.moveCal.getPosition();
            this.posY = this.initPos + this.offset_distance;
        }
        checkWithPlayer(preX, preY, this.posX, this.posY);
    }

    public void draw(MFGraphics g) {
        drawInMap(g, image, 3);
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 960, y - 960, 1920, 1920);
    }

    public void doWhileCollision(PlayerObject obj, int direction) {
        obj.beHurt();
    }

    public void close() {
    }

    public static void releaseAllResource() {
        image = null;
    }
}
