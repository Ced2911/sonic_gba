package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: GimmickObject */
class PipeOut extends GimmickObject {
    private static int COLLISION_HEIGHT = BarHorbinV.COLLISION_WIDTH;
    private static int COLLISION_WIDTH = 2304;
    private static final int OUT_SPEED_1 = 1440;
    private static final int OUT_SPEED_2 = 1824;
    private static final int PIPE_OUT_VELOCITY = 28;
    private static final int[] TRANS;
    private int actionID;
    private int dirRect;
    private int direction;
    private AnimationDrawer drawer;
    private int outVel;
    private boolean touching;
    private int transID;

    static {
        int[] iArr = new int[6];
        iArr[1] = 3;
        iArr[2] = 6;
        iArr[3] = 5;
        iArr[4] = 2;
        iArr[5] = 1;
        TRANS = iArr;
    }

    protected PipeOut(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        if (PipeIn.pipeAnimation == null) {
            if (StageManager.getCurrentZoneId() != 6) {
                PipeIn.pipeAnimation = new Animation("/animation/pipe_in");
            } else {
                PipeIn.pipeAnimation = new Animation("/animation/pipe_in" + StageManager.getCurrentZoneId() + (StageManager.getStageID() - 9));
            }
        }
        this.direction = width;
        switch (width) {
            case 0:
                this.direction = 0;
                this.dirRect = 0;
                this.transID = 0;
                this.actionID = 1;
                break;
            case 1:
                this.direction = 1;
                this.dirRect = 1;
                this.transID = 5;
                this.actionID = 1;
                break;
            case 2:
                this.direction = 2;
                this.dirRect = 2;
                this.transID = 0;
                this.actionID = 3;
                break;
            case 3:
                this.direction = 3;
                this.dirRect = 3;
                this.transID = 4;
                this.actionID = 3;
                break;
        }
        int i;
        if (top == 20) {
            i = (this.direction == 1 || this.direction == 3) ? -1440 : OUT_SPEED_1;
            this.outVel = i;
        } else if (top == 30) {
            i = (this.direction == 1 || this.direction == 3) ? -1824 : OUT_SPEED_2;
            this.outVel = i;
        }
        if (StageManager.getCurrentZoneId() == 2 || StageManager.getStageID() == 10) {
            if (this.actionID == 1) {
                COLLISION_WIDTH = 2944;
                COLLISION_HEIGHT = 5632;
            } else if (this.actionID == 3) {
                COLLISION_WIDTH = 6144;
                COLLISION_HEIGHT = 2432;
            }
        } else if (StageManager.getStageID() == 11) {
            if (this.actionID == 1) {
                COLLISION_WIDTH = 3712;
                COLLISION_HEIGHT = 5504;
            } else if (this.actionID == 3) {
                COLLISION_WIDTH = 6912;
                COLLISION_HEIGHT = 3456;
            }
        }
        this.drawer = PipeIn.pipeAnimation.getDrawer(this.actionID, true, TRANS[this.transID]);
    }

    public void draw(MFGraphics g) {
        drawInMap(g, this.drawer);
        drawCollisionRect(g);
    }

    public void refreshCollisionRect(int x, int y) {
        if (StageManager.getCurrentZoneId() == 2 || StageManager.getStageID() == 10) {
            if (this.actionID == 1) {
                COLLISION_WIDTH = 2944;
                COLLISION_HEIGHT = 5632;
            } else if (this.actionID == 3) {
                COLLISION_WIDTH = 6144;
                COLLISION_HEIGHT = 2432;
            }
        } else if (StageManager.getStageID() == 11) {
            if (this.actionID == 1) {
                COLLISION_WIDTH = 3712;
                COLLISION_HEIGHT = 5504;
            } else if (this.actionID == 3) {
                COLLISION_WIDTH = 6912;
                COLLISION_HEIGHT = 3456;
            }
        }
        this.collisionRect.setRect(x - (COLLISION_WIDTH >> 1), y - (COLLISION_HEIGHT >> 1), COLLISION_WIDTH, COLLISION_HEIGHT);
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (this.firstTouch && player.piping) {
            this.touching = true;
            switch (this.direction) {
                case 0:
                case 1:
                    if (player.velX == 0) {
                        player.velY = this.outVel;
                        player.velX = 0;
                        break;
                    }
                    player.pipeSet(this.posX, this.posY, 0, this.outVel);
                    break;
                case 2:
                case 3:
                    if (player.velY != 0) {
                        player.pipeSet(this.posX, this.posY, this.outVel, 0);
                    } else {
                        player.velX = this.outVel;
                        player.velY = 0;
                    }
                    if (this.direction == 2) {
                        player.faceDirection = true;
                    }
                    if (this.direction == 3) {
                        player.faceDirection = false;
                        break;
                    }
                    break;
            }
        }
        if (player.piping && (this.direction == 2 || this.direction == 3)) {
            PlayerObject playerObject = player;
            int i = this.posY;
            PlayerObject playerObject2 = player;
            playerObject.footPointY = i + (1536 / 2);
            player.changeVisible(false);
        }
        if (this.touching && player.piping) {
            switch (direction) {
                case 0:
                    if (this.direction != 1) {
                        return;
                    }
                    return;
                case 1:
                    if (this.direction != 0) {
                        return;
                    }
                    return;
                case 2:
                    if (this.direction != 3) {
                        return;
                    }
                    return;
                case 3:
                    if (this.direction != 2) {
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
        player.beStop(0, direction, this);
    }

    public void doWhileNoCollision() {
        if (this.touching && player.piping) {
            player.pipeOut();
            player.changeVisible(true);
            this.touching = false;
            if (this.direction != 1) {
                player.setVelY(0);
            }
        }
    }

    public void close() {
        this.drawer = null;
    }

    public int getPaintLayer() {
        return 2;
    }
}
