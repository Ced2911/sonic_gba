package SonicGBA;

import Lib.MyAPI;
import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;

/* compiled from: GimmickObject */
class FinalShima extends GimmickObject {
    private static final int COLLISION_HEIGHT = 1536;
    private static final int COLLISION_WIDTH = 1024;
    private static final int DROP_STATE_READY = 1;
    private static final int DROP_STATE_START = 2;
    private static final int DROP_STATE_WAIT = 0;
    private static MFImage image;
    private long currentTime;
    private int dropState = 0;
    private long frameTime;
    private int posOriginalY;
    private long startTime;
    private int velY = 0;

    protected FinalShima(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        if (image == null) {
            image = MFImage.createImage("/gimmick/yuka_fi.png");
        }
        this.posOriginalY = this.posY;
        this.dropState = 0;
    }

    private void resetShima() {
        this.posY = this.posOriginalY;
        this.velY = 0;
        this.dropState = 0;
        this.used = false;
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        switch (direction) {
            case 1:
                object.beStop(this.collisionRect.y0, 1, this);
                this.used = true;
                return;
            case 4:
                if (object.getMoveDistance().f14y > 0 && object.getCollisionRect().y1 < this.collisionRect.y1) {
                    object.beStop(this.collisionRect.y0, 1, this);
                    this.used = true;
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void doWhileNoCollision() {
        if (player.collisionState == (byte) 1 || player.collisionState == (byte) 0) {
            this.used = false;
        }
    }

    public void logic() {
        if (this.dropState == 2) {
            this.currentTime = System.currentTimeMillis();
            if (this.currentTime - this.startTime >= 1000) {
                this.velY += GRAVITY;
                this.posY += this.velY;
            }
        } else if (this.used) {
            this.posY = MyAPI.calNextPosition((double) this.posY, (double) (this.posOriginalY + 768), 1, 6);
            if (this.posY > this.posOriginalY + 192 && this.dropState == 0) {
                this.dropState = 1;
                this.startTime = System.currentTimeMillis();
            }
            if (this.dropState == 1) {
                this.dropState = 2;
            }
        } else {
            this.posY = MyAPI.calNextPositionReverse(this.posY, this.posOriginalY + 768, this.posOriginalY, 1, 6);
        }
        if (isAwayFromCameraInWidth()) {
            resetShima();
        }
        refreshCollisionRect(this.posX, this.posY);
    }

    public void draw(MFGraphics g) {
        drawInMap(g, image, this.posX, this.posY - 768, 17);
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 512, y - 768, 1024, 1536);
    }

    public int getPaintLayer() {
        return 0;
    }

    public static void releaseAllResource() {
        image = null;
    }
}
