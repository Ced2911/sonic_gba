package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: GimmickObject */
class Terminal extends GimmickObject {
    private static final int COLLISION_HEIGHT = 9216;
    private static final int COLLISION_HEIGHT_61 = 1;
    private static final int COLLISION_WIDTH = 1024;
    private static final int COLLISION_WIDTH_61 = 1024;
    private static AnimationDrawer drawer;
    private boolean showDrawer;
    private int type;

    protected Terminal(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        boolean z = this.iLeft == 1 && StageManager.getStageID() % 2 == 0 && StageManager.getStageID() != 10;
        this.showDrawer = z;
        if (this.showDrawer) {
            drawer = null;
            if (drawer == null) {
                drawer = new Animation("/animation/terminal").getDrawer(0, false, 0);
            }
        } else {
            this.posX += 512;
        }
        this.type = 0;
        if (StageManager.getStageID() == 10) {
            this.type = 1;
        }
    }

    public void draw(MFGraphics g) {
        if (this.showDrawer) {
            drawInMap(g, drawer);
        }
    }

    public void refreshCollisionRect(int x, int y) {
        if (this.showDrawer) {
            this.collisionRect.setRect(x, y - this.mHeight, this.mWidth, this.mHeight);
        } else {
            this.collisionRect.setRect(x, y, 1024, COLLISION_HEIGHT);
        }
        if (StageManager.getStageID() == 10) {
            this.collisionRect.setRect(x - 512, y, 1024, 1);
        }
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (!PlayerObject.isTerminal) {
            switch (this.type) {
                case 0:
                    if (drawer != null) {
                        if (this.iLeft != 1) {
                            player.setTerminalSingle(this.type);
                        } else if ((PlayerObject.getCharacterID() == 0 && (player.getCharacterAnimationID() == 18 || player.getCharacterAnimationID() == 17)) || player.getAnimationId() == 4) {
                            player.setTerminalSingle(this.type);
                        } else {
                            object.setTerminal(this.type);
                        }
                        drawer.setActionId(1);
                        soundInstance.playSe(26);
                        return;
                    }
                    return;
                case 1:
                    if (this.iLeft == 0) {
                        object.setTerminal(this.type);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    public void close() {
    }

    public int getPaintLayer() {
        return 0;
    }

    public static void releaseAllResource() {
        Animation.closeAnimationDrawer(drawer);
        drawer = null;
    }
}
