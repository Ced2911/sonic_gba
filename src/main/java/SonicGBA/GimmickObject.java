package SonicGBA;

import GameEngine.Def;
import Lib.Animation;
import Lib.Line;
import Lib.SoundSystem;
import MFLib.MainState;
import State.StringIndex;

import com.sega.engine.action.ACCollision;
import com.sega.engine.action.ACObject;
import com.sega.engine.action.ACWorldCollisionCalculator;
import com.sega.mobile.define.MDPhone;
import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;

public class GimmickObject extends GameObject {
    public static final byte GIMMICK_ACCELERATOR_FORWARD = (byte) 31;
    public static final byte GIMMICK_ACCELERATOR_FORWARD_DOWN = (byte) 33;
    public static final byte GIMMICK_ACCELERATOR_FORWARD_UP = (byte) 32;
    public static final byte GIMMICK_ADD_DOUBLE_MAX_SPEED = (byte) 36;
    public static final byte GIMMICK_AIR_ROOT = (byte) 112;
    public static final byte GIMMICK_ARM = (byte) 80;
    public static final byte GIMMICK_AROUND_ENTRANCE = (byte) 34;
    public static final byte GIMMICK_AROUND_EXIT = (byte) 35;
    public static final byte GIMMICK_BALLOON = (byte) 59;
    public static final byte GIMMICK_BALL_HOBIN = (byte) 49;
    public static final byte GIMMICK_BANE_ISLAND = (byte) 81;
    public static final byte GIMMICK_BANPER = (byte) 71;
    public static final byte GIMMICK_BAR_H = (byte) 51;
    public static final byte GIMMICK_BAR_V = (byte) 52;
    public static final byte GIMMICK_BELT = (byte) 69;
    public static final byte GIMMICK_BIG_FLOATING_ISLAND = (byte) 55;
    public static final byte GIMMICK_BLOCK = (byte) 99;
    public static final byte GIMMICK_BOSS4_ICE = (byte) 121;
    public static final byte GIMMICK_BOSS6_BLOCK = (byte) 122;
    public static final byte GIMMICK_BOSS6_BLOCK_ARRAY = (byte) 123;
    public static final byte GIMMICK_BREAK_ISLAND = (byte) 24;
    public static final byte GIMMICK_BUBBLE = (byte) 94;
    public static final byte GIMMICK_CAPER_BED = (byte) 23;
    public static final byte GIMMICK_CAPER_BLOCK = (byte) 25;
    public static final byte GIMMICK_CHANGE_LAYER_A = (byte) 17;
    public static final byte GIMMICK_CHANGE_LAYER_B = (byte) 18;
    public static final byte GIMMICK_CHANGE_RECT_REGION = (byte) 124;
    public static final byte GIMMICK_CORNER_BAR = (byte) 53;
    public static final byte GIMMICK_DAMAGE = (byte) 105;
    public static final byte GIMMICK_DASH_PANEL_HIGH = (byte) 100;
    public static final byte GIMMICK_DASH_PANEL_LOW = (byte) 101;
    public static final byte GIMMICK_DASH_PANEL_TATE = (byte) 47;
    public static final byte GIMMICK_DEGREE_CHANGE_180 = (byte) 38;
    public static final byte GIMMICK_DOOR_H = (byte) 64;
    public static final byte GIMMICK_DOOR_V = (byte) 63;
    public static final byte GIMMICK_DOWN_SHIMA = (byte) 85;
    public static final byte GIMMICK_DUCT_ROTATE = (byte) 39;
    public static final byte GIMMICK_FALL = (byte) 66;
    public static final byte GIMMICK_FALLING_ISLAND = (byte) 22;
    public static final byte GIMMICK_FAN = (byte) 103;
    public static final byte GIMMICK_FIRE_MT = (byte) 98;
    public static final byte GIMMICK_FLIPPER = (byte) 54;
    public static final byte GIMMICK_FLIPPER_V = (byte) 56;
    public static final byte GIMMICK_FLOATING_ISLAND = (byte) 21;
    public static final byte GIMMICK_FREE_FALL = (byte) 74;
    public static final byte GIMMICK_FURIKO = (byte) 76;
    public static final byte GIMMICK_F_SHIMA_FALL = (byte) 104;
    public static final byte GIMMICK_GOAL = (byte) 0;
    public static final byte GIMMICK_GRAPHIC_PATCH = (byte) 116;
    public static final byte GIMMICK_GRAVITY = (byte) 109;
    public static final byte GIMMICK_HARI_DOWN = (byte) 2;
    public static final byte GIMMICK_HARI_ISLAND = (byte) 61;
    public static final byte GIMMICK_HARI_LEFT = (byte) 3;
    public static final byte GIMMICK_HARI_MOVE_DOWN = (byte) 6;
    public static final byte GIMMICK_HARI_MOVE_UP = (byte) 5;
    public static final byte GIMMICK_HARI_RIGHT = (byte) 4;
    public static final byte GIMMICK_HARI_UP = (byte) 1;
    public static final byte GIMMICK_HEX_HOBIN = (byte) 48;
    public static final byte GIMMICK_HOBBY_FAIR = (byte) 41;
    public static final byte GIMMICK_ICE = (byte) 95;
    public static final byte GIMMICK_INVISIBLE_CAPER = (byte) 26;
    public static final byte GIMMICK_IRON_BALL = (byte) 82;
    public static final byte GIMMICK_IRON_BAR = (byte) 83;
    public static final byte GIMMICK_KASSHA = (byte) 75;
    public static final byte GIMMICK_LEAF = (byte) 30;
    public static final byte GIMMICK_MARKER = (byte) 7;
    public static final byte GIMMICK_MINUS_DOUBLE_MAX_SPEED = (byte) 37;
    public static final byte GIMMICK_MOVE = (byte) 65;
    public static final byte GIMMICK_NEJI = (byte) 78;
    public static final byte GIMMICK_NET_ITEM = (byte) 115;
    public static final byte GIMMICK_NOTHING = (byte) 119;
    public static final byte GIMMICK_NO_KEY = (byte) 113;
    public static final int GIMMICK_NUM = 110;
    public static final byte GIMMICK_PIPE = (byte) 106;
    public static final byte GIMMICK_PIPE_IN = (byte) 110;
    public static final byte GIMMICK_PIPE_OUT = (byte) 111;
    public static final byte GIMMICK_POAL = (byte) 42;
    public static final byte GIMMICK_POAL_LEFT = (byte) 44;
    public static final byte GIMMICK_POAL_RIGHT = (byte) 45;
    public static final byte GIMMICK_RAIL_FLIPPER = (byte) 73;
    public static final byte GIMMICK_RAIL_IN = (byte) 67;
    public static final byte GIMMICK_RAIL_OUT = (byte) 68;
    protected static final String GIMMICK_RES_PATH = "/gimmick";
    public static final byte GIMMICK_ROLL_ASHIBA = (byte) 86;
    public static final byte GIMMICK_ROLL_HOBIN = (byte) 50;
    public static final byte GIMMICK_ROLL_SHIMA = (byte) 91;
    public static final byte GIMMICK_ROPE_END = (byte) 117;
    public static final byte GIMMICK_ROPE_TURN = (byte) 118;
    public static final byte GIMMICK_SEE = (byte) 70;
    public static final byte GIMMICK_SHATTER = (byte) 77;
    public static final byte GIMMICK_SHIP = (byte) 60;
    public static final byte GIMMICK_SLIP_END = (byte) 20;
    public static final byte GIMMICK_SLIP_START = (byte) 19;
    public static final byte GIMMICK_SPIN = (byte) 96;
    public static final byte GIMMICK_SPLIT = (byte) 107;
    public static final byte GIMMICK_SPRING_DOWN = (byte) 9;
    public static final byte GIMMICK_SPRING_ISLAND = (byte) 57;
    public static final byte GIMMICK_SPRING_LEFT = (byte) 10;
    public static final byte GIMMICK_SPRING_LEFT_UP = (byte) 12;
    public static final byte GIMMICK_SPRING_LEFT_UP_BURY = (byte) 14;
    public static final byte GIMMICK_SPRING_RIGHT = (byte) 11;
    public static final byte GIMMICK_SPRING_RIGHT_UP = (byte) 13;
    public static final byte GIMMICK_SPRING_RIGHT_UP_BURY = (byte) 15;
    public static final byte GIMMICK_SPRING_UP = (byte) 8;
    public static final byte GIMMICK_SP_BANE = (byte) 102;
    public static final byte GIMMICK_STEAM = (byte) 79;
    public static final byte GIMMICK_STONE = (byte) 16;
    public static final byte GIMMICK_STONE_BALL = (byte) 92;
    public static final byte GIMMICK_SUBEYUKA = (byte) 84;
    public static final byte GIMMICK_TAIMATU = (byte) 87;
    public static final byte GIMMICK_TEA_CUP = (byte) 62;
    public static final byte GIMMICK_TOGE_SHIMA = (byte) 93;
    public static final byte GIMMICK_TUTORIAL = (byte) 120;
    public static final byte GIMMICK_UG_BANE = (byte) 108;
    public static final byte GIMMICK_UP_ARM = (byte) 88;
    public static final byte GIMMICK_UP_SHIMA = (byte) 89;
    public static final byte GIMMICK_VIEW_LIGHTS = (byte) 58;
    public static final byte GIMMICK_WALL = (byte) 114;
    public static final byte GIMMICK_WALL_WALKER_ENTRANCE_LEFT = (byte) 28;
    public static final byte GIMMICK_WALL_WALKER_ENTRANCE_RIGHT = (byte) 29;
    public static final byte GIMMICK_WARP = (byte) 72;
    public static final byte GIMMICK_WATER_FALL = (byte) 27;
    public static final byte GIMMICK_WATER_PILLAR = (byte) 40;
    public static final byte GIMMICK_WATER_PILLAR_2 = (byte) 43;
    public static final byte GIMMICK_WATER_SLIP = (byte) 46;
    public static final byte GIMMICK_WIND = (byte) 90;
    public static final byte GIMMICK_WIND_PARTS = (byte) 97;
    public static final int PLATFORM_OFFSET_Y = -256;
    private static final int WIND_ACCELERATE = ((-GRAVITY) * 3);
    private static final int WIND_VELOCITY = (SmallAnimal.FLY_VELOCITY_Y - GRAVITY);
    private static boolean damageEnable = false;
    public static Animation doorAnimation = null;
    protected static Animation firemtAnimation;
    private static int framecnt;
    private static boolean furikoEnable = false;
    public static MFImage hookImage;
    private static boolean ironBallEnable = false;
    public static MFImage platformImage = null;
    private static boolean rollHobinEnable = false;
    private static boolean rollPlatformEnable = false;
    public static MFImage rolllinkImage;
    private static boolean seabedvolcanoEnable = false;
    public static MFImage shipRingImage;
    private static boolean steamEnable = false;
    private static boolean torchFireEnable = false;
    private static boolean waterFallEnable = false;
    private static boolean waterSlipEnable = false;
    protected int iHeight;
    protected int iLeft;
    protected int iTop;
    protected int iWidth;
    protected boolean used;

    public static GameObject getNewInstance(int id, int x, int y, int left, int top, int width, int height) {
        GameObject reElement = null;
        x <<= 6;
        y <<= 6;
        switch (id) {
            case 0:
                reElement = new Terminal(id, x, y, left, top, width, height);
                break;
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
                reElement = new Hari(id, x, y, left, top, width, height);
                break;
            case 7:
                reElement = new Marker(id, x, y, left, top, width, height);
                break;
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
                reElement = new Spring(id, x, y, left, top, width, height);
                break;
            case 16:
                reElement = new Stone(id, x, y, left, top, width, height);
                break;
            case 19:
                reElement = new SlipStart(id, x, y, left, top, width, height);
                break;
            case 21:
                reElement = new Platform(id, x, y, left, top, width, height);
                break;
            case 22:
                reElement = new FallingPlatform(id, x, y, left, top, width, height);
                break;
            case 23:
                reElement = new CaperBed(id, x, y, left, top, width, height);
                break;
            case 24:
                reElement = new BreakPlatform(id, x, y, left, top, width, height);
                break;
            case 25:
                reElement = new CaperBlock(id, x, y, left, top, width, height);
                break;
            case 31:
            case 32:
            case 33:
            case 100:
                reElement = new Accelerate(id, x, y, left, top, width, height);
                break;
            case 42:
            case 44:
            case 45:
                reElement = new Poal(id, x, y, left, top, width, height);
                break;
            case 69:
                reElement = new Belt(id, x, y, left, top, width, height);
                break;
            case 71:
                reElement = new Banper(id, x, y, left, top, width, height);
                break;
            case 75:
                reElement = new RopeStart(id, x, y, left, top, width, height);
                break;
            case 76:
                reElement = new Furiko(id, x, y, left, top, width, height);
                furikoEnable = true;
                break;
            case 77:
                reElement = new Shatter(id, x, y, left, top, width, height);
                break;
            case 78:
                reElement = new Neji(id, x, y, left, top, width, height);
                break;
            case 80:
                reElement = new Arm(id, x, y, left, top, width, height);
                break;
            case 102:
                if (stageModeState == 1) {
                    reElement = null;
                    break;
                }
                reElement = new SpSpring(id, x, y, left, top, width, height);
                break;
            case 105:
                reElement = new DamageArea(id, x, y, left, top, width, height);
                damageEnable = true;
                break;
            case 106:
                reElement = new PipeSet(id, x, y, left, top, width, height);
                break;
            case 110:
                reElement = new PipeIn(id, x, y, left, top, width, height);
                break;
            case 111:
                reElement = new PipeOut(id, x, y, left, top, width, height);
                break;
            case 116:
                reElement = new GraphicPatch(id, x, y, left, top, width, height);
                break;
            case 117:
                reElement = new RopeEnd(id, x, y, left, top, width, height);
                break;
            case 118:
                reElement = new RopeTurn(id, x, y, left, top, width, height);
                break;
            case 120:
                reElement = new TutorialPoint(id, x, y, left, top, width, height);
                break;
            case 124:
                reElement = new ChangeRectRegion(id, x, y, left, top, width, height);
                break;
            case 27:
                reElement = new WaterFall(id, x, y, left, top, width, height);
                waterFallEnable = true;
                break;
            case 28:
            case 29:
                reElement = new Bank(id, x, y, left, top, width, height);
                break;
            case 40:
                reElement = new Fountain(id, x, y, left, top, width, height);
                break;
            case 43:
                reElement = new FallFlush(id, x, y, left, top, width, height);
                break;
            case 46:
                reElement = new WaterSlip(id, x, y, left, top, width, height);
                waterSlipEnable = true;
                break;
            case 48:
                reElement = new HexHobin(id, x, y, left, top, width, height);
                break;
            case 49:
                reElement = new BallHobin(id, x, y, left, top, width, height);
                break;
            case 50:
                reElement = new RollHobin(id, x, y, left, top, width, height);
                rollHobinEnable = true;
                break;
            case 51:
                reElement = new BarHorbinH(id, x, y, left, top, width, height);
                break;
            case 52:
                reElement = new BarHorbinV(id, x, y, left, top, width, height);
                break;
            case 53:
                reElement = new CornerHobin(id, x, y, left, top, width, height);
                break;
            case 54:
                reElement = new FlipH(id, x, y, left, top, width, height);
                break;
            case 56:
                reElement = new FlipV(id, x, y, left, top, width, height);
                break;
            case 57:
                reElement = new SpringPlatform(id, x, y, left, top, width, height);
                break;
            case 58:
                reElement = new LightFont(id, x, y, left, top, width, height);
                break;
            case 59:
                reElement = new Balloon(id, x, y, left, top, width, height);
                break;
            case 60:
                reElement = new ShipSystem(id, x, y, left, top, width, height);
                break;
            case 62:
                reElement = new TeaCup(id, x, y, left, top, width, height);
                break;
            case 63:
            case 64:
                reElement = new Door(id, x, y, left, top, width, height);
                break;
            case 67:
                reElement = new RailIn(id, x, y, left, top, width, height);
                break;
            case 68:
                reElement = new RailOut(id, x, y, left, top, width, height);
                break;
            case 72:
                reElement = new TransPoint(id, x, y, left, top, width, height);
                break;
            case 73:
                reElement = new RailFlipper(id, x, y, left, top, width, height);
                break;
            case 74:
                reElement = new FreeFallSystem(id, x, y, left, top, width, height);
                break;
            case 79:
                reElement = new SteamBase(id, x, y, left, top, width, height);
                steamEnable = true;
                break;
            case 82:
                reElement = new IronBall(id, x, y, left, top, width, height);
                ironBallEnable = true;
                break;
            case 83:
                reElement = new IronBar(id, x, y, left, top, width, height);
                break;
            case 87:
                reElement = new TorchFire(id, x, y, left, top, width, height);
                torchFireEnable = true;
                break;
            case 91:
                if (top == 5) {
                    reElement = new RollPlatformSpeedA(id, x, y, left, top, width, height);
                } else if (top == 2) {
                    reElement = new RollPlatformSpeedB(id, x, y, left, top, width, height);
                } else if (top == 4) {
                    reElement = new RollPlatformSpeedC(id, x, y, left, top, width, height);
                }
                rollPlatformEnable = true;
                break;
            case 107:
                reElement = new Split(id, x, y, left, top, width, height);
                break;
            case 114:
                reElement = new BreakWall(id, x, y, left, top, width, height);
                break;
            case 30:
                reElement = new Leaf(id, x, y, left, top, width, height);
                break;
            case 47:
                reElement = new Accelerate(id, x, y, left, top, width, height);
                break;
            case 55:
                reElement = new DekaPlatform(id, x, y, left, top, width, height);
                break;
            case 61:
                reElement = new HariIsland(id, x, y, left, top, width, height);
                break;
            case 81:
                reElement = new SpringIsland(id, x, y, left, top, width, height);
                break;
            case 84:
                reElement = new Subeyuka(id, x, y, left, top, width, height);
                break;
            case 85:
                reElement = new DownIsland(id, x, y, left, top, width, height);
                break;
            case 86:
                reElement = new RollIsland(id, x, y, left, top, width, height);
                break;
            case 88:
                reElement = new UpArm(id, x, y, left, top, width, height);
                break;
            case 89:
                reElement = new UpPlatform(id, x, y, left, top, width, height);
                break;
            case 92:
                reElement = new StoneBall(id, x, y, left, top, width, height);
                break;
            case 93:
                reElement = new TogeShima(id, x, y, left, top, width, height);
                break;
            case 94:
                reElement = new Bubble(id, x, y, left, top, width, height);
                break;
            case 95:
                reElement = new Ice(id, x, y, left, top, width, height);
                break;
            case 97:
                reElement = new WindParts(id, x, y, left, top, width, height);
                break;
            case 98:
                if (left == 35 || left == 9 || left == -21) {
                    reElement = new SeabedVolcanoAsynBase(id, x, y, left, top, width, height);
                } else {
                    reElement = new SeabedVolcanoBase(id, x, y, left, top, width, height);
                }
                seabedvolcanoEnable = true;
                break;
            case 99:
                reElement = new Block(id, x, y, left, top, width, height);
                break;
            case 103:
                reElement = new Fan(id, x, y, left, top, width, height);
                break;
            case 104:
                reElement = new FinalShima(id, x, y, left, top, width, height);
                break;
            case 108:
                reElement = new UnseenSpring(id, x, y, left, top, width, height);
                break;
            case 109:
                reElement = new AntiGravity(id, x, y, left, top, width, height);
                break;
            case 112:
                reElement = new AirRoot(id, x, y, left, top, width, height);
                break;
            default:
                if (reElement == null) {
                    if (id == 90) {
                        isFirstTouchedWind = false;
                    }
                    reElement = new GimmickObject(id, x, y, left, top, width, height);
                    break;
                }
                break;
        }
        if (reElement != null) {
            reElement.refreshCollisionRectWrap();
        }
        return reElement;
    }

    public static void gimmickInit() {
        furikoEnable = false;
        ironBallEnable = false;
        torchFireEnable = false;
        rollPlatformEnable = false;
        steamEnable = false;
        waterFallEnable = false;
        waterSlipEnable = false;
        rollHobinEnable = false;
        damageEnable = false;
        seabedvolcanoEnable = false;
    }

    public static void gimmickStaticLogic() {
        if (furikoEnable) {
            Furiko.staticLogic();
        }
        if (damageEnable) {
            DamageArea.staticLogic();
        }
        MoveCalculator.staticLogic();
        if (waterFallEnable) {
            WaterFall.staticLogic();
        }
        if (waterSlipEnable) {
            WaterSlip.staticLogic();
        }
        if (torchFireEnable) {
            TorchFire.staticLogic();
        }
        if (steamEnable) {
            SteamBase.staticLogic();
        }
        if (ironBallEnable) {
            IronBall.staticLogic();
        }
        if (rollHobinEnable) {
            RollHobin.staticLogic();
        }
        if (rollPlatformEnable) {
            RollPlatformSpeedA.staticLogic();
            RollPlatformSpeedB.staticLogic();
            RollPlatformSpeedC.staticLogic();
        }
        if (seabedvolcanoEnable) {
            SeabedVolcanoBase.staticLogic();
            SeabedVolcanoAsynBase.staticLogic();
        }
    }

    public static void releaseGimmickResource() {
        doorAnimation = null;
        shipRingImage = null;
        platformImage = null;
        hookImage = null;
        Hari.releaseAllResource();
        Spring.releaseAllResource();
        Stone.releaseAllResource();
        Marker.releaseAllResource();
        BreakPlatform.releaseAllResource();
        CaperBed.releaseAllResource();
        CaperBlock.releaseAllResource();
        Accelerate.releaseAllResource();
        Poal.releaseAllResource();
        Furiko.releaseAllResource();
        Shatter.releaseAllResource();
        Neji.releaseAllResource();
        Arm.releaseAllResource();
        PipeIn.releaseAllResource();
        Terminal.releaseAllResource();
        Belt.releaseAllResource();
        DamageArea.releaseAllResource();
        GraphicPatch.releaseAllResource();
        TutorialPoint.releaseAllResource();
        RopeStart.releaseAllResource();
        Cage.releaseAllResource();
        FallFlush.releaseAllResource();
        WaterSlip.releaseAllResource();
        TorchFire.releaseAllResource();
        SteamBase.releaseAllResource();
        SteamPlatform.releaseAllResource();
        CageButton.releaseAllResource();
        IronBall.releaseAllResource();
        RailFlipper.releaseAllResource();
        LightFont.releaseAllResource();
        HexHobin.releaseAllResource();
        BallHobin.releaseAllResource();
        RollHobin.releaseAllResource();
        CornerHobin.releaseAllResource();
        BarHorbinV.releaseAllResource();
        BarHorbinH.releaseAllResource();
        Balloon.releaseAllResource();
        ShipSystem.releaseAllResource();
        Ship.releaseAllResource();
        ShipBase.releaseAllResource();
        FlipH.releaseAllResource();
        FlipV.releaseAllResource();
        TeaCup.releaseAllResource();
        Door.releaseAllResource();
        FreeFallSystem.releaseAllResource();
        FreeFallBar.releaseAllResource();
        FreeFallPlatform.releaseAllResource();
        BreakWall.releaseAllResource();
        SpringPlatform.releaseAllResource();
        RailIn.releaseAllResource();
        DekaPlatform.releaseAllResource();
        Subeyuka.releaseAllResource();
        HariIsland.releaseAllResource();
        SpringIsland.releaseAllResource();
        WindParts.releaseAllResource();
        StoneBall.releaseAllResource();
        DownIsland.releaseAllResource();
        RollIsland.releaseAllResource();
        TogeShima.releaseAllResource();
        Bubble.releaseAllResource();
        Ice.releaseAllResource();
        SeabedVolcanoBase.releaseAllResource();
        SeabedVolcanoAsynBase.releaseAllResource();
        Accelerate.releaseAllResource();
        Fan.releaseAllResource();
        FinalShima.releaseAllResource();
        AirRoot.releaseAllResource();
        SpSpring.releaseAllResource();
        Boss4Ice.releaseAllResource();
        Boss6Block.releaseAllResource();
    }

    protected GimmickObject(int id, int x, int y, int left, int top, int width, int height) {
        this.objId = id;
        this.posX = x;
        this.posY = y;
        this.iLeft = left;
        this.iTop = top;
        this.iWidth = width;
        this.iHeight = height;
        this.mWidth = width * 512;
        this.mHeight = height * 512;
        this.collisionRect.setRect(this.posX, this.posY, this.mWidth, this.mHeight);
    }

    public void draw(MFGraphics g) {
        drawCollisionRect(g);
    }

    public void logic() {
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (object == player) {
            switch (this.objId) {
                case 17:
                    if (!this.collisionRect.collisionChk(player.getCheckPositionX(), player.getCheckPositionY())) {
                        this.used = false;
                        return;
                    } else if (!this.used) {
                        player.setCollisionLayer(0);
                        this.used = true;
                        return;
                    } else {
                        return;
                    }
                case 18:
                    if (!this.collisionRect.collisionChk(player.getCheckPositionX(), player.getCheckPositionY())) {
                        this.used = false;
                        return;
                    } else if (!this.used) {
                        player.setCollisionLayer(1);
                        this.used = true;
                        return;
                    } else {
                        return;
                    }
                case 20:
                    if (player instanceof PlayerSonic) {
                        ((PlayerSonic) player).slipEnd();
                        return;
                    } else if (player instanceof PlayerAmy) {
                        ((PlayerAmy) player).slipEnd();
                        return;
                    } else {
                        return;
                    }
                case 26:
                    if (!this.used && player.beUnseenPop()) {
                        this.used = true;
                        return;
                    }
                    return;
                case 34:
                    if (!this.collisionRect.collisionChk(player.getCheckPositionX(), player.getCheckPositionY())) {
                        this.used = false;
                        return;
                    } else if (!this.used) {
                        player.ductIn();
                        this.used = true;
                        if (!(player instanceof PlayerAmy) && player.getAnimationId() != 4) {
                            soundInstance.playSe(4);
                            return;
                        }
                        return;
                    } else {
                        return;
                    }
                case 35:
                    if (!this.collisionRect.collisionChk(player.getCheckPositionX(), player.getCheckPositionY())) {
                        this.used = false;
                        return;
                    } else if (!this.used) {
                        player.velX = 0;
                        player.ductOut();
                        this.used = true;
                        return;
                    } else {
                        return;
                    }
                case 36:
                    if (player.isOnGound() && this.collisionRect.collisionChk(player.getCheckPositionX(), player.getCheckPositionY())) {
                        player.setVelX(PlayerObject.HUGE_POWER_SPEED);
                        return;
                    }
                    return;
                case 37:
                    if (player.isOnGound()) {
                        player.setVelX(-1900);
                        return;
                    }
                    return;
                case 38:
                    return;
                case 65:
                    if (!this.used && player.setRailLine(new Line(this.posX, this.posY, this.posX + this.iLeft, this.posY + this.iTop), this.posX, this.posY, this.iLeft, this.iTop, this.iWidth, this.iHeight, this)) {
                        this.used = true;
                        soundInstance.playSe(37);
                        return;
                    }
                    return;
                case 66:
                    if (this.firstTouch && StageManager.getCurrentZoneId() != 3) {
                        player.setFall(this.posX - 256, this.posY, this.iLeft, this.iTop);
                        player.stopMove();
                        return;
                    }
                    return;
                case 70:
                    if (!this.used && this.collisionRect.collisionChk(player.getCheckPositionX(), player.getCheckPositionY())) {
                        boolean z;
                        PlayerObject playerObject = player;
                        if (this.iLeft == 0) {
                            z = true;
                        } else {
                            z = false;
                        }
                        playerObject.changeVisible(z);
                        this.used = true;
                        return;
                    }
                    return;
                case 90:
                    framecnt++;
                    if (this.collisionRect.collisionChk(player.getCheckPositionX(), player.getCheckPositionY())) {
                        if (StageManager.getStageID() == 11) {
                            player.collisionState = (byte) 1;
                            player.isInGravityCircle = true;
                        }
                        if (player.collisionState == (byte) 1) {
                            player.collisionState = (byte) 1;
                            if (StageManager.getStageID() == 11) {
                                player.worldCal.stopMoveY();
                                ACWorldCollisionCalculator aCWorldCollisionCalculator = player.worldCal;
                                ACWorldCollisionCalculator aCWorldCollisionCalculator2 = player.worldCal;
                                aCWorldCollisionCalculator.actionState = (byte) 1;
                            }
                            if (player.getVelY() > WIND_VELOCITY) {
                                player.setVelY(player.getVelY() + WIND_ACCELERATE);
                            } else {
                                player.setVelY(WIND_VELOCITY);
                            }
                            if (StageManager.getStageID() == 11) {
                                player.setAnimationId(9);
                                return;
                            } else {
                                player.setAnimationId(29);
                                return;
                            }
                        }
                        soundInstance.stopLoopSe();
                        framecnt = 0;
                        isFirstTouchedWind = false;
                        player.isInGravityCircle = false;
                        return;
                    }
                    return;
                case 96:
                    if (!this.used) {
                        player.setAnimationId(4);
                        SoundSystem soundSystem = soundInstance;
                        SoundSystem soundSystem2 = soundInstance;
                        soundSystem.playSe(37);
                        this.used = true;
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    public void refreshCollisionRect(int x, int y) {
        switch (this.objId) {
            case 20:
                this.collisionRect.setRect(((this.mWidth >> 1) + x) - MDPhone.SCREEN_HEIGHT, y, 1280, this.mHeight);
                return;
            case 66:
                this.collisionRect.setRect(x - 1024, y, 1536, 64);
                return;
            case 70:
                this.collisionRect.setRect(x, y, this.mWidth, this.mHeight);
                return;
            case 73:
                this.collisionRect.setRect(this.posX, this.posY - 512, 512, 512);
                return;
            default:
                return;
        }
    }

    public void doWhileRail(PlayerObject object, int direction) {
        switch (this.objId) {
            case 65:
                if (!this.used && player.setRailLine(new Line(this.posX, this.posY, this.posX + this.iLeft, this.posY + this.iTop), this.posX, this.posY, this.iLeft, this.iTop, this.iWidth, this.iHeight, this)) {
                    this.used = true;
                    soundInstance.playSe(37);
                    return;
                }
                return;
            case 66:
                if (this.firstTouch) {
                    player.setFall(this.posX - 256, this.posY, this.iLeft, this.iTop);
                    return;
                }
                return;
            case 70:
                if (!this.used && this.collisionRect.collisionChk(player.getCheckPositionX(), player.getCheckPositionY())) {
                    player.changeVisible(this.iLeft == 0);
                    this.used = true;
                    return;
                }
                return;
            case 73:
                if (this.firstTouch) {
                    player.setRailFlip();
                    return;
                }
                return;
            case 96:
                if (!this.used) {
                    player.setAnimationId(4);
                    SoundSystem soundSystem = soundInstance;
                    SoundSystem soundSystem2 = soundInstance;
                    soundSystem.playSe(37);
                    this.used = true;
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void doWhileNoCollision() {
        switch (this.objId) {
            case 17:
            case 18:
            case 19:
            case 20:
            case 26:
            case 34:
            case 35:
            case 65:
            case 96:
                this.used = false;
                return;
            case 70:
                if (this.used) {
                    this.used = false;
                    if (this.iLeft == 0) {
                        player.setFallOver();
                        return;
                    }
                    return;
                }
                return;
            case 90:
                if (player.isInGravityCircle) {
                    player.isInGravityCircle = false;
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void close() {
    }

    public void doBeforeCollisionCheck() {
    }

    public void doWhileCollision(ACObject arg0, ACCollision arg1, int arg2, int arg3, int arg4, int arg5, int arg6) {
    }
}
