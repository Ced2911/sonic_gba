package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: GimmickObject */
class WindParts extends GimmickObject {
    private static final int MOVE_FRAME = 8;
    private static final int VELOCITY = 500;
    private static Animation animation;
    private AnimationDrawer drawer;
    private int moveCount;
    private int posOriginalY;

    protected WindParts(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        if (animation == null) {
            animation = new Animation("/animation/wind_parts");
        }
        this.drawer = animation.getDrawer(2 - this.iLeft, true, 0);
        this.posOriginalY = this.posY;
        this.moveCount = 0;
    }

    public void logic() {
        this.moveCount++;
        if (this.moveCount >= 8) {
            this.moveCount = 0;
            this.posY = this.posOriginalY;
        } else {
            this.posY -= 500;
        }
        refreshCollisionRect(this.posX, this.posY);
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x, y, 1, 1);
    }

    public void draw(MFGraphics g) {
        drawInMap(g, this.drawer);
    }

    public void close() {
        this.drawer = null;
    }

    public static void releaseAllResource() {
        Animation.closeAnimation(animation);
        animation = null;
    }
}
