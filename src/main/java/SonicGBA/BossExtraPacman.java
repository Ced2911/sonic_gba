package SonicGBA;

import GameEngine.Key;
import Lib.Animation;
import Lib.MyRandom;
import Lib.SoundSystem;
import Special.SSDef;
import com.sega.engine.action.ACMoveCalUser;
import com.sega.engine.action.ACMoveCalculator;
import com.sega.mobile.define.MDPhone;
import com.sega.mobile.framework.device.MFGraphics;
import java.lang.reflect.Array;

/* compiled from: BulletObject */
class BossExtraPacman extends BulletObject implements ACMoveCalUser {
    private static final int COLLISION_HEIGHT = 1536;
    private static final int COLLISION_OFFSET_Y = -960;
    private static final int COLLISION_WIDTH = 1024;
    private static final int PACMAN_LIFE = 64;
    private static final int RING_NUM = 7;
    private static final int STATE_BEGIN = 0;
    private static final int STATE_BROKE = 6;
    private static final int STATE_DASH = 2;
    private static final int STATE_DEAD = 3;
    private static final int STATE_PACKAGED = 5;
    private static final int STATE_PACKAGING = 4;
    private static final int STATE_SEARCH = 1;
    private int count;
    private boolean isHigher;
    private ACMoveCalculator moveCal;
    private int packageOffset;
    private int[][] pieceInfo = ((int[][]) Array.newInstance(Integer.TYPE, new int[]{2, 4}));
    private int state;
    private PlayerSuperSonic superSonic;

    protected BossExtraPacman(int x, int y) {
        super(x, y, 0, 0, false);
        if (pacmanAnimation == null) {
            pacmanAnimation = new Animation("/animation/boss_extra_pacman");
        }
        this.drawer = pacmanAnimation.getDrawer(0, true, 0);
        this.state = 0;
        this.count = 0;
        this.moveCal = new ACMoveCalculator(this, this);
    }

    public void bulletLogic() {
        if ((player instanceof PlayerSuperSonic) && ((PlayerSuperSonic) player).getBossDieFlag() && this.state != 6) {
            setDie();
        }
        this.count++;
        switch (this.state) {
            case 0:
                this.moveCal.actionLogic(-240, 0);
                if (this.count > 12) {
                    this.state = 1;
                    this.isHigher = this.posY < player.posY;
                    return;
                }
                return;
            case 1:
                if (this.posY < player.posY) {
                    if (this.isHigher) {
                        this.moveCal.actionLogic(0, Math.min(player.posY - this.posY, SSDef.PLAYER_MOVE_HEIGHT));
                    } else {
                        this.state = 2;
                        return;
                    }
                } else if (this.posY > player.posY) {
                    if (this.isHigher) {
                        this.state = 2;
                        return;
                    }
                    this.moveCal.actionLogic(0, Math.max(player.posY - this.posY, -240));
                }
                if (player.posY == this.posY) {
                    this.state = 2;
                    return;
                }
                return;
            case 2:
                this.moveCal.actionLogic(-720, 0);
                return;
            case 4:
                this.posX = player.posX;
                this.posY = player.posY;
                if (this.drawer.checkEnd()) {
                    this.state = 5;
                    this.count = 0;
                    if (this.superSonic != null) {
                        this.superSonic.setPackageObj(this);
                        return;
                    }
                    return;
                }
                return;
            case 5:
                this.posX = player.posX;
                this.posY = player.posY;
                if (Key.press(Key.gLeft) && this.packageOffset != GimmickObject.PLATFORM_OFFSET_Y) {
                    this.packageOffset = GimmickObject.PLATFORM_OFFSET_Y;
                    this.count += 5;
                } else if (!Key.press(Key.gRight) || this.packageOffset == 256) {
                    this.packageOffset = 0;
                } else {
                    this.packageOffset = 256;
                    this.count += 5;
                }
                if (this.count > 64) {
                    this.superSonic.setPackageObj(null);
                    this.state = 6;
                    doBroke();
                    return;
                }
                return;
            case 6:
                for (int i = 0; i < this.pieceInfo.length; i++) {
                    int[] iArr = this.pieceInfo[i];
                    iArr[3] = iArr[3] + (GRAVITY >> 2);
                    iArr = this.pieceInfo[i];
                    iArr[0] = iArr[0] + this.pieceInfo[i][2];
                    iArr = this.pieceInfo[i];
                    iArr[1] = iArr[1] + this.pieceInfo[i][3];
                }
                return;
            default:
                return;
        }
    }

    public void draw(MFGraphics g) {
        switch (this.state) {
            case 0:
                this.drawer.setActionId(0);
                this.drawer.setLoop(false);
                drawInMap(g, this.drawer);
                break;
            case 1:
            case 2:
                this.drawer.setActionId(1);
                this.drawer.setLoop(true);
                drawInMap(g, this.drawer);
                break;
            case 3:
                this.drawer.setActionId(4);
                drawInMap(g, this.drawer);
                break;
            case 4:
                this.drawer.setActionId(2);
                this.drawer.setLoop(false);
                drawInMap(g, this.drawer);
                break;
            case 5:
                this.drawer.setActionId(3);
                drawInMap(g, this.drawer, this.posX + this.packageOffset, this.posY);
                break;
            case 6:
                for (int i = 0; i < this.pieceInfo.length; i++) {
                    this.drawer.setActionId(i + 4);
                    drawInMap(g, this.drawer, this.pieceInfo[i][0], this.pieceInfo[i][1]);
                }
                break;
        }
        drawCollisionRect(g);
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x, y + COLLISION_OFFSET_Y, 1024, 1536);
    }

    public void didAfterEveryMove(int moveDistanceX, int moveDistanceY) {
        refreshCollisionRect(this.posX, this.posY);
        doWhileCollisionWrapWithPlayer();
    }

    public void doWhileCollision(PlayerObject player, int direction) {
        if (player instanceof PlayerSuperSonic) {
            this.superSonic = (PlayerSuperSonic) player;
            if (this.state != 1 && this.state != 2) {
                return;
            }
            if (player.isAttackingEnemy()) {
                this.state = 6;
                doBroke();
                ringBroke();
                return;
            }
            this.state = 4;
        }
    }

    public void setDie() {
        this.state = 6;
        doBroke();
    }

    public int getPaintLayer() {
        return 2;
    }

    public boolean chkDestroy() {
        return this.state == 3 || this.posX < 0 || (this.state == 6 && (this.pieceInfo[0][1] >> 6) > (camera.f14y + SCREEN_HEIGHT) + 40);
    }

    public static void releaseAllResource() {
        Animation.closeAnimation(pacmanAnimation);
        pacmanAnimation = null;
    }

    private void doBroke() {
        SoundSystem.getInstance().playSe(29);
        if (this.pieceInfo != null) {
            this.pieceInfo[0][0] = this.posX - MDPhone.SCREEN_HEIGHT;
            this.pieceInfo[1][0] = this.posX + MDPhone.SCREEN_HEIGHT;
            this.pieceInfo[0][1] = this.posY;
            this.pieceInfo[1][1] = this.posY;
            this.pieceInfo[0][2] = -200;
            this.pieceInfo[1][2] = 200;
            this.pieceInfo[0][3] = -200;
            this.pieceInfo[1][3] = -200;
        }
    }

    private void ringBroke() {
        for (int i = 0; i < 7; i++) {
            GameObject.addGameObject(new MoveRingObject(this.posX, this.posY, MyRandom.nextInt(-400, 400), MyRandom.nextInt(-800 - (GRAVITY * 2), SmallAnimal.FLY_VELOCITY_Y - (GRAVITY * 1)), 1, systemClock, 14));
        }
    }
}
