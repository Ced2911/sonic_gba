package SonicGBA;

import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;

/* compiled from: GimmickObject */
class RailIn extends GimmickObject {
    private static final int COLLISION_HEIGHT = 2560;
    private static final int COLLISION_WIDTH = 2560;
    private static final int IMAGE_HEIGHT = 1536;
    public static MFImage railInOutImage;

    protected RailIn(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        if (railInOutImage == null) {
            railInOutImage = MFImage.createImage("/gimmick/gimmick_67_68.png");
        }
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 1280, y - BarHorbinV.COLLISION_WIDTH, BarHorbinV.COLLISION_WIDTH, BarHorbinV.COLLISION_WIDTH);
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        switch (direction) {
            case 1:
                if (object == player && !this.used) {
                    player.railIn(this.posX, this.posY);
                    this.used = true;
                    return;
                }
                return;
            case 2:
            case 3:
                object.beStop(0, direction, this);
                return;
            default:
                return;
        }
    }

    public int getPaintLayer() {
        return 3;
    }

    public void doWhileRail(PlayerObject object, int direction) {
        object.setAnimationId(21);
    }

    public void draw(MFGraphics g) {
        drawInMap(g, railInOutImage, this.posX, (this.posY - BarHorbinV.COLLISION_WIDTH) + 1536, 17);
        drawCollisionRect(g);
    }

    public void doWhileNoCollision() {
        this.used = false;
    }

    public static void releaseAllResource() {
        railInOutImage = null;
    }
}
