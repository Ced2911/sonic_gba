package SonicGBA;

import State.StringIndex;
import com.sega.engine.action.ACCollision;
import com.sega.engine.action.ACDegreeGetter;
import com.sega.engine.action.ACDegreeGetter.DegreeReturner;
import com.sega.engine.action.ACParam;
import com.sega.mobile.define.MDPhone;

public class MyDegreeGetter extends ACDegreeGetter implements ACParam {
    private CollisionBlock block = ((CollisionBlock) CollisionMap.getInstance().getNewCollisionBlock());
    private CollisionMap world;

    public MyDegreeGetter(CollisionMap world) {
        this.world = world;
    }

    public void getDegreeFromCollisionByPosition(DegreeReturner re, ACCollision obj, int currentDegree, int x, int y, int z) {
    }

    public void getDegreeFromWorldByPosition(DegreeReturner re, int currentDegree, int x, int y, int z) {
        re.reset(x, y, currentDegree);
        CollisionMap.getInstance().getCollisionBlock(this.block, x, y, z);
        re.degree = this.block.getDegree(currentDegree, getDirectionByDegree(currentDegree));
    }

    private int getDirectionByDegree(int degree) {
        while (degree < 0) {
            degree += MDPhone.SCREEN_WIDTH;
        }
        degree %= MDPhone.SCREEN_WIDTH;
        if (degree >= 315 || degree <= 45) {
            return 0;
        }
        if (degree > 225 && degree < 315) {
            return 3;
        }
        if (degree < StringIndex.FONT_COLON_RED || degree > 225) {
            return 1;
        }
        return 2;
    }
}
