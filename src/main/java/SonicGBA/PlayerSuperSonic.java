package SonicGBA;

import GameEngine.Key;
import Lib.Animation;
import Lib.AnimationDrawer;
import Lib.MyRandom;
import Lib.SoundSystem;
import com.sega.engine.action.ACMoveCalculator;
import com.sega.mobile.define.MDPhone;
import com.sega.mobile.framework.android.Graphics;
import com.sega.mobile.framework.device.MFGamePad;
import com.sega.mobile.framework.device.MFGraphics;
import java.lang.reflect.Array;
import java.util.Vector;

public class PlayerSuperSonic extends PlayerObject {
    private static final int ATTACK_VELOCITY = 1200;
    private static final int HEIGHT = 1024;
    private static final int HURT_COUNT = 20;
    private static final int INIT_RING_NUM = 50;
    private static final int MOVE_MAX_VELOCITY = 960;
    private static final int MOVE_POWER = 240;
    private static final int ROLLING_COUNT = 7;
    private static final int SMALL_JUMP_COUNT = 9;
    private static final int START_JUMP_VELOCITY = (GRAVITY + PlayerObject.SHOOT_POWER);
    private static final int SUPER_ANI_ATTACK = 4;
    private static final int SUPER_ANI_ATTACK_EFFECT = 5;
    private static final int SUPER_ANI_DAMAGE = 6;
    private static final int SUPER_ANI_DIE_1 = 7;
    private static final int SUPER_ANI_DIE_2 = 8;
    private static final int SUPER_ANI_STAND = 3;
    private static final int SUPER_ANI_STAR_1 = 9;
    private static final int SUPER_ANI_STAR_2 = 10;
    private static final int VELOCITY_STAY = 400;
    private static final int WIDTH = 1536;
    private int attackEffectCount;
    private AnimationDrawer attackEffectDrawer;
    private boolean attackEffectShow;
    private boolean bossDie;
    private int frameCount;
    private int jumpframe;
    private boolean jumplocked;
    private ACMoveCalculator moveCal;
    private int myAnimationID;
    private int nextStarCount;
    private boolean noRingLose;
    private BossExtraPacman pacman;
    private AnimationDrawer shadowDrawer;
    private int[][] shadowPosition;
    private int smallJumpCount;
    private int starCount;
    private Vector starVec;
    private Animation superSonicAcnimation;
    private AnimationDrawer superSonicDrawer;

    public PlayerSuperSonic() {
        this.noRingLose = false;
        this.superSonicAcnimation = new Animation("/animation/player/chr_super_sonic");
        this.superSonicDrawer = this.superSonicAcnimation.getDrawer();
        this.shadowDrawer = this.superSonicAcnimation.getDrawer(0, true, 0);
        this.attackEffectDrawer = this.superSonicAcnimation.getDrawer(5, false, 0);
        this.moveCal = new ACMoveCalculator(this, this);
        this.myAnimationID = 3;
        this.drawer = this.superSonicDrawer;
        this.starVec = new Vector();
        this.shadowPosition = (int[][]) Array.newInstance(Integer.TYPE, new int[]{3, 2});
        for (int i = 0; i < 3; i++) {
            this.shadowPosition[i][0] = this.posX;
            this.shadowPosition[i][1] = this.posY;
        }
        ringNum = 50;
    }

    public void closeImpl() {
        Animation.closeAnimationDrawer(this.superSonicDrawer);
        this.superSonicDrawer = null;
        Animation.closeAnimationDrawer(this.shadowDrawer);
        this.shadowDrawer = null;
        Animation.closeAnimationDrawer(this.attackEffectDrawer);
        this.attackEffectDrawer = null;
        Animation.closeAnimation(this.superSonicAcnimation);
        this.superSonicAcnimation = null;
    }

    public void logic() {
        MapManager.setCameraUpLimit(MapManager.getPixelHeight() - 200);
        if (getBossDieFlag()) {
            timeStopped = true;
        }
        if (PlayerObject.getTimeCount() > 0 && !this.noRingLose && timeCount - lastTimeCount >= 1000) {
            lastTimeCount += 1000;
            if (ringNum > 0) {
                ringNum--;
                if (ringNum <= 10) {
                    SoundSystem.getInstance().playSe(30);
                }
            }
            if (ringNum == 0) {
                timeCount -= timeCount % 1000;
                setDieWithoutSE();
                if (this.pacman != null) {
                    this.pacman.setDie();
                }
                this.pacman = null;
                this.myAnimationID = 7;
            }
        }
        if (this.isDead) {
            timeCount -= timeCount % 1000;
            ringNum = 0;
            if (this.pacman != null) {
                this.pacman.setDie();
                this.pacman = null;
            }
            this.velY += getGravity();
            this.posX += this.velX;
            this.posY += this.velY;
            if (this.posY > ((MapManager.getCamera().f14y + MapManager.CAMERA_HEIGHT) << 6) + MFGamePad.KEY_NUM_6) {
                this.posY = ((MapManager.getCamera().f14y + MapManager.CAMERA_HEIGHT) << 6) + MFGamePad.KEY_NUM_6;
                if (!this.finishDeadStuff) {
                    if (stageModeState == 1) {
                        StageManager.setStageRestart();
                    } else if (lifeNum > 0) {
                        lifeNum--;
                        StageManager.setStageRestart();
                    } else {
                        StageManager.setStageGameover();
                    }
                    this.finishDeadStuff = true;
                }
            }
        } else if (this.pacman != null) {
            this.moveCal.actionLogic(0, 0);
        } else {
            this.shadowPosition[0][1] = this.shadowPosition[1][1];
            this.shadowPosition[1][1] = this.shadowPosition[2][1];
            this.shadowPosition[2][1] = this.posY;
            this.hurtCount--;
            if (this.hurtCount == 12) {
                this.myAnimationID = 3;
            }
            if (this.hurtCount < 0) {
                this.hurtCount = 0;
            }
            if (this.hurtCount < 13) {
                switch (this.collisionState) {
                    case (byte) 0:
                        inputWalkLogic();
                        break;
                    case (byte) 1:
                        inputJumpLogic();
                        break;
                }
            }
            this.velY = 0;
            this.velX = -1243;
            this.moveCal.actionLogic(this.velX, this.velY);
            int moveLength = ((this.velX + 960) * 2) / 3;
            this.shadowPosition[0][0] = this.posX - (moveLength * 3);
            this.shadowPosition[1][0] = this.posX - (moveLength * 2);
            this.shadowPosition[2][0] = this.posX - moveLength;
            addStar();
        }
    }

    public void didAfterEveryMove(int moveDistanceX, int moveDistanceY) {
        if (this.posX - 768 < (MapManager.actualLeftCameraLimit << 6)) {
            this.posX = (MapManager.actualLeftCameraLimit << 6) + 768;
            if (getVelX() < 0) {
                setVelX(0);
            }
        }
        if (MapManager.actualRightCameraLimit != MapManager.getPixelWidth() && this.posX + 768 > (MapManager.actualRightCameraLimit << 6)) {
            this.posX = (MapManager.actualRightCameraLimit << 6) - 768;
            if (getVelX() > 0) {
                setVelX(0);
            }
        }
        if (!this.isDead && this.footPointY > (MapManager.actualDownCameraLimit << 6)) {
            this.posY = MapManager.actualDownCameraLimit << 6;
            setDieWithoutSE();
        }
        if (this.leftStopped && this.rightStopped) {
            setDieWithoutSE();
        }
        int groundY = getGroundY(this.posX, this.posY) - 400;
        switch (this.collisionState) {
            case (byte) 0:
                this.posY = groundY;
                break;
            case (byte) 1:
                if (this.velY > 0 && groundY < this.posY) {
                    this.collisionState = (byte) 0;
                    this.posY = groundY;
                    break;
                }
        }
        super.didAfterEveryMove(moveDistanceX, moveDistanceY);
    }

    public void drawCharacter(MFGraphics g) {
        if (this.pacman == null) {
            this.drawer.setLoop(true);
            this.drawer.setTrans(0);
            this.drawer.setActionId(this.myAnimationID);
            if (this.myAnimationID == 7) {
                this.drawer.setLoop(false);
            }
            if (!this.isDead) {
                if (this.attackEffectShow && this.attackEffectCount % 2 == 1) {
                    drawInMap(g, this.attackEffectDrawer, this.posX, this.posY);
                }
                drawShadow(g);
            }
            if (this.myAnimationID == 6) {
                drawDamage(g, this.drawer, this.posX, this.posY);
            } else if (this.hurtCount % 2 == 0) {
                drawInMap(g, this.drawer, this.posX, this.posY);
            } else {
                this.drawer.moveOn();
                if (this.drawer.checkEnd() && this.myAnimationID == 7) {
                    this.myAnimationID = 8;
                }
            }
            if (!this.isDead && this.attackEffectShow) {
                if (this.attackEffectCount % 2 == 0) {
                    drawInMap(g, this.attackEffectDrawer, this.posX, this.posY);
                }
                if (!IsGamePause) {
                    this.attackEffectCount++;
                }
                if (this.attackEffectDrawer.checkEnd()) {
                    this.attackEffectShow = false;
                    if (this.myAnimationID == 4) {
                        this.myAnimationID = 3;
                    }
                }
            }
            if (this.isDead && StageManager.isStageTimeover()) {
                this.myAnimationID = 7;
                this.drawer.setLoop(false);
                if (!IsGamePause) {
                    this.velY += getGravity();
                    this.posX += this.velX;
                    this.posY += this.velY;
                    if (this.posY > ((MapManager.getCamera().f14y + MapManager.CAMERA_HEIGHT) << 6) + MFGamePad.KEY_NUM_6) {
                        this.posY = ((MapManager.getCamera().f14y + MapManager.CAMERA_HEIGHT) << 6) + MFGamePad.KEY_NUM_6;
                    }
                }
            }
            drawStar(g);
            drawCollisionRect(g);
        }
    }

    private void inputWalkLogic() {
        if (this.hurtCount < 13) {
            if (Key.repeat(Key.gLeft)) {
                if (this.velX > -960) {
                    this.velX -= 240;
                    if (this.velX < -960) {
                        this.velX = -960;
                    }
                }
            } else if (Key.repeat(Key.gRight)) {
                if (this.velX < 960) {
                    this.velX += 240;
                    if (this.velX > 960) {
                        this.velX = 960;
                    }
                }
            } else if (this.velX > 0) {
                this.velX -= 240;
                if (this.velX < 0) {
                    this.velX = 0;
                }
            } else if (this.velX < 0) {
                this.velX += 240;
                if (this.velX > 0) {
                    this.velX = 0;
                }
            }
            if (Key.press(16777216)) {
                this.jumplocked = false;
                this.velY = START_JUMP_VELOCITY;
                this.collisionState = (byte) 1;
                SoundSystem.getInstance().playSe(11);
                this.smallJumpCount = 9;
            }
            if (Key.press(Key.gSelect) && !this.attackEffectShow) {
                this.velX = 1200;
                this.attackEffectShow = true;
                this.attackEffectDrawer.restart();
                this.myAnimationID = 4;
                this.attackEffectCount = 0;
                SoundSystem.getInstance().playSe(7);
            }
        }
    }

    private void inputJumpLogic() {
        if (Key.repeat(Key.gLeft)) {
            if (this.velX > -960) {
                this.velX -= 240;
                if (this.velX < -960) {
                    this.velX = -960;
                }
            }
        } else if (Key.repeat(Key.gRight)) {
            if (this.velX < 960) {
                this.velX += 240;
                if (this.velX > 960) {
                    this.velX = 960;
                }
            }
        } else if (this.velX > 0) {
            this.velX -= 240;
            if (this.velX < 0) {
                this.velX = 0;
            }
        } else if (this.velX < 0) {
            this.velX += 240;
            if (this.velX > 0) {
                this.velX = 0;
            }
        }
        if (this.smallJumpCount > 0) {
            this.smallJumpCount--;
            if (!Key.repeat(Key.gUp | 16777216)) {
                this.velY += GRAVITY >> 1;
                this.velY += GRAVITY >> 2;
            }
        }
        if (this.attackEffectShow) {
            this.velY = 0;
        } else {
            this.velY += GRAVITY;
        }
        if (Key.press(Key.gSelect) && !this.attackEffectShow) {
            this.velX = 1200;
            this.attackEffectShow = true;
            this.attackEffectDrawer.restart();
            this.myAnimationID = 4;
            this.attackEffectCount = 0;
            SoundSystem.getInstance().playSe(7);
        }
    }

    private void jumpRelock() {
        this.jumpframe = 0;
        this.jumplocked = true;
    }

    private void addStar() {
        this.starCount++;
        if (this.starCount >= this.nextStarCount) {
            this.starVec.addElement(new StarEffect(this.superSonicAcnimation, MyRandom.nextInt(2) == 0 ? 9 : 10, this.posX + 1536, this.posY + MyRandom.nextInt(-400, 0)));
            this.starCount = 0;
            this.nextStarCount = MyRandom.nextInt(2, 6);
        }
    }

    private void drawStar(MFGraphics g) {
        int i = 0;
        while (i < this.starVec.size()) {
            StarEffect star = (StarEffect) this.starVec.elementAt(i);
            if (star.draw(g)) {
                star.close();
                this.starVec.removeElementAt(i);
                i--;
            }
            i++;
        }
    }

    private void drawShadow(MFGraphics g) {
        this.frameCount++;
        this.frameCount %= 3;
        this.shadowDrawer.setActionId(this.myAnimationID);
        for (int i = 0; i < this.shadowPosition.length; i++) {
            if (this.myAnimationID == 6) {
                drawDamage(g, this.shadowDrawer, this.shadowPosition[i][0], this.shadowPosition[i][1]);
            } else {
                if (IsGamePause) {
                    this.frameCount = 0;
                }
                if (this.frameCount % 2 == 0) {
                    this.shadowDrawer.draw(g, (this.shadowPosition[i][0] >> 6) - camera.f13x, (this.shadowPosition[i][1] >> 6) - camera.f14y);
                } else {
                    this.shadowDrawer.moveOn();
                }
            }
        }
    }

    public int getFocusX() {
        return super.getFocusX() + (SCREEN_WIDTH >> 2);
    }

    public void doWhileCollision() {
    }

    public void beHurt() {
        if (player.canBeHurt()) {
            this.myAnimationID = 6;
            this.hurtCount = 20;
            SoundSystem.getInstance().playSe(14);
        }
        if (this.pacman != null) {
            this.pacman.setDie();
            this.pacman = null;
        }
    }

    public void setBossDieFlag(boolean die) {
        this.bossDie = die;
    }

    public boolean getBossDieFlag() {
        return this.bossDie;
    }

    public void setPackageObj(BossExtraPacman pacman) {
        if (this.pacman != null) {
            this.pacman.setDie();
        }
        this.pacman = pacman;
    }

    public boolean isAttackingEnemy() {
        return super.isAttackingEnemy() || this.attackEffectShow;
    }

    private void drawDamage(MFGraphics g, AnimationDrawer drawer, int x, int y) {
        int degree = ((this.hurtCount - 20) * MDPhone.SCREEN_WIDTH) / 7;
        Graphics g2 = (Graphics) g.getSystemGraphics();
        g2.save();
        g2.translate((float) ((x >> 6) - camera.f13x), (float) ((y >> 6) - camera.f14y));
        g2.rotate((float) degree);
        drawer.draw(g, 0, 0);
        g2.restore();
    }

    public void refreshCollisionRectWrap() {
        this.collisionRect.setRect(this.posX - 768, this.posY - 1024, 1536, 1024);
    }

    public void doBossAttackPose(GameObject object, int direction) {
        if (this.collisionState == (byte) 1) {
            setVelX(-1243);
            this.attackEffectShow = false;
            this.myAnimationID = 3;
        }
    }

    public void getBossScore() {
        super.getBossScore();
        this.noRingLose = true;
    }
}
