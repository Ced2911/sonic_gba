package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: GimmickObject */
class UnseenSpring extends GimmickObject {
    private static final int COLLISION_HEIGHT = 1024;
    private static final int COLLISION_WIDTH = 64;
    private static int SPRING_POWER = Spring.SPRING_POWER[0];
    private AnimationDrawer drawer;

    protected UnseenSpring(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        if (Spring.springAnimation == null) {
            Spring.springAnimation = new Animation("/animation/se_bane_kiro");
        }
        this.drawer = Spring.springAnimation.getDrawer(16, false, 0);
    }

    public void draw(MFGraphics g) {
        SPRING_POWER = player.isInWater ? Spring.SPRING_INWATER_POWER[0] : Spring.SPRING_POWER[0];
        if (this.drawer.getActionId() == 17) {
            drawInMap(g, this.drawer, this.posX, this.posY);
            if (this.drawer.checkEnd()) {
                this.drawer.setActionId(16);
            }
        }
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (player.collisionState == (byte) 0) {
            player.beSpring(SPRING_POWER, 1);
            this.drawer.setActionId(17);
            soundInstance.playSe(37);
        }
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 32, y - 512, 64, 1024);
    }

    public void close() {
        this.drawer = null;
    }
}
