package SonicGBA;

import Lib.Animation;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: EnemyObject */
class Crab extends EnemyObject {
    private static final int COLLISION_HEIGHT = 1792;
    private static final int COLLISION_WIDTH = 2688;
    private static final int STATE_ATTACK = 1;
    private static final int STATE_WALK = 0;
    private static Animation crabAnimation;
    private int emenyid;
    private int fire_cnt;
    private int fire_start_speed = 300;
    private int limitLeftX = this.posX;
    private int limitRightX = (this.posX + this.mWidth);
    private int state;
    private int velocity = 192;

    public static void releaseAllResource() {
        Animation.closeAnimation(crabAnimation);
        crabAnimation = null;
    }

    protected Crab(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        if (crabAnimation == null) {
            crabAnimation = new Animation("/animation/crab");
        }
        this.drawer = crabAnimation.getDrawer(0, true, 0);
        this.emenyid = id;
    }

    public void logic() {
        if (!this.dead) {
            int preX = this.posX;
            int preY = this.posY;
            switch (this.state) {
                case 0:
                    if (this.velocity > 0) {
                        this.posX += this.velocity;
                        if (this.posX >= this.limitRightX) {
                            this.posX = this.limitRightX;
                            this.velocity = -this.velocity;
                            this.state = 1;
                            this.drawer.setActionId(1);
                            this.drawer.setLoop(false);
                            this.fire_cnt = 0;
                        }
                    } else {
                        this.posX += this.velocity;
                        if (this.posX <= this.limitLeftX) {
                            this.posX = this.limitLeftX;
                            this.velocity = -this.velocity;
                            this.state = 1;
                            this.drawer.setActionId(1);
                            this.drawer.setLoop(false);
                            this.fire_cnt = 0;
                        }
                    }
                    this.posY = getGroundY(this.posX, this.posY);
                    checkWithPlayer(preX, preY, this.posX, this.posY);
                    return;
                case 1:
                    if (this.fire_cnt == 0) {
                        BulletObject.addBullet(this.emenyid, this.posX - 1344, this.posY - COLLISION_HEIGHT, -this.fire_start_speed, -this.fire_start_speed);
                        BulletObject.addBullet(this.emenyid, this.posX + 1344, this.posY - COLLISION_HEIGHT, this.fire_start_speed, -this.fire_start_speed);
                    }
                    this.fire_cnt++;
                    if (this.drawer.checkEnd() || this.fire_cnt > 2) {
                        this.state = 0;
                        this.drawer.setActionId(0);
                        this.drawer.setLoop(true);
                    }
                    this.posY = getGroundY(this.posX, this.posY);
                    checkWithPlayer(preX, preY, this.posX, this.posY);
                    return;
                default:
                    return;
            }
        }
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 1344, y - COLLISION_HEIGHT, COLLISION_WIDTH, COLLISION_HEIGHT);
    }

    public void draw(MFGraphics g) {
        if (!this.dead) {
            drawInMap(g, this.drawer);
            drawCollisionRect(g);
        }
    }
}
