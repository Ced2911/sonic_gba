package SonicGBA;

import Lib.Animation;
import Lib.Record;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class GlobalResource {
    public static final int DEFAULT_SOUND_VOL = 9;
    public static int difficultyConfig = 1;
    public static int languageConfig = 0;
    public static int seConfig = 1;
    public static int sensorConfig = 1;
    public static int soundConfig = 9;
    public static int soundSwitchConfig = 1;
    public static int spsetConfig = 0;
    public static Animation statusAnimation;
    public static int timeLimit = 0;
    public static int vibrationConfig = 1;

    public static void initSystemConfig() {
        seConfig = 1;
        soundSwitchConfig = 1;
        if (soundConfig == 0) {
            soundConfig = 0;
        } else {
            soundConfig = 9;
        }
        difficultyConfig = 1;
        timeLimit = 0;
        vibrationConfig = 1;
        spsetConfig = 0;
        languageConfig = 0;
        sensorConfig = 1;
    }

    public static void loadSystemConfig() {
        ByteArrayInputStream bs = Record.loadRecordStream(Record.SYSTEM_RECORD);
        try {
            DataInputStream ds = new DataInputStream(bs);
            soundConfig = ds.readByte();
            difficultyConfig = ds.readByte();
            timeLimit = ds.readByte();
            seConfig = ds.readByte();
            vibrationConfig = ds.readByte();
            spsetConfig = ds.readByte();
            languageConfig = ds.readByte();
            sensorConfig = ds.readByte();
            if (bs != null) {
                try {
                    bs.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e2) {
            saveSystemConfig();
            if (bs != null) {
                try {
                    bs.close();
                } catch (IOException e3) {
                    e3.printStackTrace();
                }
            }
        } catch (Throwable th) {
            if (bs != null) {
                try {
                    bs.close();
                } catch (IOException e32) {
                    e32.printStackTrace();
                }
            }
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void saveSystemConfig() {
        /*
        r0 = new java.io.ByteArrayOutputStream;
        r0.<init>();
        r1 = new java.io.DataOutputStream;
        r1.<init>(r0);
        r3 = soundConfig;	 Catch:{ Exception -> 0x003b, all -> 0x0045 }
        r1.writeByte(r3);	 Catch:{ Exception -> 0x003b, all -> 0x0045 }
        r3 = difficultyConfig;	 Catch:{ Exception -> 0x003b, all -> 0x0045 }
        r1.writeByte(r3);	 Catch:{ Exception -> 0x003b, all -> 0x0045 }
        r3 = timeLimit;	 Catch:{ Exception -> 0x003b, all -> 0x0045 }
        r1.writeByte(r3);	 Catch:{ Exception -> 0x003b, all -> 0x0045 }
        r3 = seConfig;	 Catch:{ Exception -> 0x003b, all -> 0x0045 }
        r1.writeByte(r3);	 Catch:{ Exception -> 0x003b, all -> 0x0045 }
        r3 = vibrationConfig;	 Catch:{ Exception -> 0x003b, all -> 0x0045 }
        r1.writeByte(r3);	 Catch:{ Exception -> 0x003b, all -> 0x0045 }
        r3 = spsetConfig;	 Catch:{ Exception -> 0x003b, all -> 0x0045 }
        r1.writeByte(r3);	 Catch:{ Exception -> 0x003b, all -> 0x0045 }
        r3 = languageConfig;	 Catch:{ Exception -> 0x003b, all -> 0x0045 }
        r1.writeByte(r3);	 Catch:{ Exception -> 0x003b, all -> 0x0045 }
        r3 = sensorConfig;	 Catch:{ Exception -> 0x003b, all -> 0x0045 }
        r1.writeByte(r3);	 Catch:{ Exception -> 0x003b, all -> 0x0045 }
        r3 = "SONIC_SYSTEM_RECORD";
        Lib.Record.saveRecordStream(r3, r0);	 Catch:{ Exception -> 0x003b, all -> 0x0045 }
        r1.close();	 Catch:{ IOException -> 0x004f }
    L_0x003a:
        return;
    L_0x003b:
        r3 = move-exception;
        r1.close();	 Catch:{ IOException -> 0x0040 }
        goto L_0x003a;
    L_0x0040:
        r2 = move-exception;
        r2.printStackTrace();
        goto L_0x003a;
    L_0x0045:
        r3 = move-exception;
        r1.close();	 Catch:{ IOException -> 0x004a }
    L_0x0049:
        throw r3;
    L_0x004a:
        r2 = move-exception;
        r2.printStackTrace();
        goto L_0x0049;
    L_0x004f:
        r2 = move-exception;
        r2.printStackTrace();
        goto L_0x003a;
        */

        ByteArrayOutputStream bs = new ByteArrayOutputStream();
        try {
            DataOutputStream ds = new DataOutputStream(bs);
            ds.writeByte(soundConfig);
            ds.writeByte(difficultyConfig);
            ds.writeByte(timeLimit);
            ds.writeByte(seConfig);
            ds.writeByte(vibrationConfig);
            ds.writeByte(spsetConfig);
            ds.writeByte(languageConfig);
            ds.writeByte(sensorConfig);

            Record.saveRecordStream(Record.SYSTEM_RECORD, bs);


            ds.close();
            bs.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        //throw new UnsupportedOperationException("Method not decompiled: SonicGBA.GlobalResource.saveSystemConfig():void");
        //System.out.println("Method not decompiled: SonicGBA.GlobalResource.saveSystemConfig():void");
    }

    public static boolean isEasyMode() {
        return difficultyConfig == 0;
    }

    public static boolean timeIsLimit() {
        return timeLimit == 0;
    }
}
