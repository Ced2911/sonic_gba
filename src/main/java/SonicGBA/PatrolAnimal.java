package SonicGBA;

import Lib.MyAPI;
import Lib.MyRandom;
import com.sega.mobile.define.MDPhone;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: SmallAnimal */
class PatrolAnimal extends SmallAnimal {
    private static final int FLY_HEIGHT = 6400;
    private static final int FLY_RANGE = 640;
    private boolean direction;
    private int flyDegree;
    private int flyLimit;
    private int leftLimit;
    private int rightLimit;

    public PatrolAnimal(int type, int animalId, int x, int y, int layer, int left, int right) {
        super(type, animalId, x, y, layer);
        this.direction = MyRandom.nextInt(2) == 0;
        this.leftLimit = left;
        this.rightLimit = right;
        if (this.mObj != null) {
            this.mObj.setPosition(this.posX, this.posY, (this.direction ? 1 : -1) * MyRandom.nextInt(300), 0, this);
        }
        this.flyLimit = y - FLY_HEIGHT;
    }

    public void logic() {
        if (this.type == 2) {
            int i;
            if (this.direction) {
                if (this.posX > this.rightLimit) {
                    this.direction = !this.direction;
                }
            } else if (this.posX < this.leftLimit) {
                this.direction = !this.direction;
            }
            int i2 = this.posX;
            if (this.direction) {
                i = -1;
            } else {
                i = 1;
            }
            this.posX = i2 + (i * -250);
            this.posY += SmallAnimal.FLY_VELOCITY_Y;
            if (this.posY < this.flyLimit) {
                this.posY = this.flyLimit;
            }
            this.flyDegree += 30;
            this.flyDegree %= MDPhone.SCREEN_WIDTH;
        } else if (this.mObj != null) {
            this.mObj.logic();
            this.posX = this.mObj.getPosX();
            this.posY = this.mObj.getPosY();
        }
        refreshCollisionRect(this.posX, this.posY);
    }

    public void doWhileTouchGround(int vx, int vy) {
        if (this.type == 1) {
            if (this.direction) {
                if (this.mObj.getPosX() > this.rightLimit) {
                    this.direction = !this.direction;
                }
            } else if (this.mObj.getPosX() < this.leftLimit) {
                this.direction = !this.direction;
            }
            this.mObj.doJump((this.direction ? 1 : -1) * 300, -1000);
        }
    }

    public void draw(MFGraphics g) {
        if (this.direction) {
            this.drawer.setTrans(2);
        } else {
            this.drawer.setTrans(0);
        }
        if (this.type == 1) {
            drawInMap(g, this.drawer);
        } else {
            drawInMap(g, this.drawer, this.posX, this.posY + ((MyAPI.dSin(this.flyDegree) * 640) / 100));
        }
    }
}
