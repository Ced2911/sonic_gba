package SonicGBA;

import Lib.Animation;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: EnemyObject */
class Frog extends EnemyObject {
    private static final int ALERT_HEIGHT = 160;
    private static final int ALERT_RANGE = 3072;
    private static final int ALERT_WIDTH = 55;
    private static final int COLLISION_HEIGHT = 1280;
    private static final int COLLISION_HEIGHT_JUMP = 768;
    private static final int COLLISION_WIDTH = 1024;
    private static final int COLLISION_WIDTH_JUMP = 896;
    private static final int DRIP_ACC = (GRAVITY - 25);
    private static final int FROG_JUMP_START_SPEED = -1050;
    private static final int STATE_COAXAR = 2;
    private static final int STATE_JUMP = 1;
    private static final int STATE_WAIT = 0;
    private static Animation frogAnimation;
    private boolean IsFirstCoaxar;
    private int alert_state;
    private int attack_state;
    private boolean beRight;
    private int frog_time;
    private int interval;
    private int limitLeftX;
    private int limitRightX;
    private int starty;
    private int state;
    private int velY;
    private int velocity;

    public static void releaseAllResource() {
        Animation.closeAnimation(frogAnimation);
        frogAnimation = null;
    }

    protected Frog(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        this.velY = 0;
        this.velocity = 75;
        this.IsFirstCoaxar = false;
        this.frog_time = 0;
        this.interval = 1;
        this.beRight = false;
        this.mWidth = 5120;
        this.limitLeftX = this.posX;
        this.limitRightX = this.posX + this.mWidth;
        if (frogAnimation == null) {
            frogAnimation = new Animation("/animation/frog");
        }
        this.drawer = frogAnimation.getDrawer(0, true, 0);
        this.starty = this.posY;
        this.posX = (this.posX + (this.mWidth >> 1)) + 2048;
        this.posY = getGroundY(this.posX, this.posY);
        this.IsFirstCoaxar = false;
        this.beRight = false;
    }

    public void doInitWhileInCamera() {
        this.posX = (this.limitLeftX + (this.mWidth >> 1)) + 2048;
        this.posY = getGroundY(this.posX, this.posY);
        this.IsFirstCoaxar = false;
        refreshCollisionRect(this.posX >> 6, this.posY >> 6);
        this.beRight = false;
    }

    private void jumpInit() {
        this.state = 1;
        if (this.drawer.getTransId() == 2) {
            this.drawer.setActionId(1);
            this.drawer.setTrans(2);
            this.drawer.setLoop(false);
            this.beRight = false;
        } else if (this.drawer.getTransId() == 0) {
            this.drawer.setActionId(1);
            this.drawer.setTrans(0);
            this.drawer.setLoop(false);
            this.beRight = true;
        }
        this.velY = FROG_JUMP_START_SPEED;
    }

    private boolean jumpChk() {
        if (this.alert_state == 0) {
            return true;
        }
        if (this.posX < player.getCheckPositionX()) {
            if (player.getVelX() < 0 && player.getVelX() > -488 && player.getFootPositionX() - this.posX < 3520 && player.getFootPositionX() - this.posX > 0) {
                return true;
            }
            if (player.getVelX() <= -488 && player.getVelX() > -672 && player.getFootPositionX() - this.posX < 7040 && player.getFootPositionX() - this.posX >= 3520) {
                return true;
            }
            if (player.getVelX() > -672 || player.getFootPositionX() - this.posX >= 10560 || player.getFootPositionX() - this.posX < 7040) {
                return false;
            }
            return true;
        } else if (player.getVelX() > 0 && player.getVelX() < PlayerObject.SONIC_ATTACK_LEVEL_1_V0 && this.posX - player.getFootPositionX() < 3520 && this.posX - player.getFootPositionX() > 0) {
            return true;
        } else {
            if (player.getVelX() >= PlayerObject.SONIC_ATTACK_LEVEL_1_V0 && player.getVelX() < PlayerObject.SONIC_ATTACK_LEVEL_2_V0 && this.posX - player.getFootPositionX() < 7040 && this.posX - player.getFootPositionX() >= 3520) {
                return true;
            }
            if (player.getVelX() < PlayerObject.SONIC_ATTACK_LEVEL_2_V0 || this.posX - player.getFootPositionX() >= 10560 || this.posX - player.getFootPositionX() < 7040) {
                return false;
            }
            return true;
        }
    }

    public void logic() {
        if (!this.dead) {
            this.alert_state = checkPlayerInEnemyAlertRange(this.posX >> 6, this.posY >> 6, 55, ALERT_HEIGHT, this.limitLeftX >> 6, this.limitRightX >> 6, 16);
            int preX = this.posX;
            int preY = this.posY;
            switch (this.state) {
                case 0:
                    if (this.posX < player.getCheckPositionX()) {
                        this.drawer.setActionId(0);
                        this.drawer.setTrans(2);
                        this.drawer.setLoop(true);
                    } else {
                        this.drawer.setActionId(0);
                        this.drawer.setTrans(0);
                        this.drawer.setLoop(true);
                    }
                    if (isInCamera() && !this.IsFirstCoaxar) {
                        this.frog_time = PlayerObject.getTimeCount();
                        this.IsFirstCoaxar = true;
                    }
                    if (this.IsFirstCoaxar) {
                        this.interval = ((PlayerObject.getTimeCount() - this.frog_time) + 1) % 5000;
                        if (this.interval <= 100) {
                            this.state = 2;
                            if (this.drawer.getTransId() == 2) {
                                this.drawer.setActionId(2);
                                this.drawer.setTrans(2);
                                this.drawer.setLoop(false);
                            } else if (this.drawer.getTransId() == 0) {
                                this.drawer.setActionId(2);
                                this.drawer.setTrans(0);
                                this.drawer.setLoop(false);
                            }
                        }
                    }
                    if (jumpChk()) {
                        if (this.posX < player.getCheckPositionX()) {
                            if (this.velocity < 0) {
                                this.velocity = -this.velocity;
                            }
                        } else if (this.velocity > 0) {
                            this.velocity = -this.velocity;
                        }
                        if (this.posX + (this.velocity * 10) > this.limitLeftX && this.posX + (this.velocity * 10) < this.limitLeftX + this.mWidth) {
                            jumpInit();
                        }
                    }
                    this.posY = getGroundY(this.posX, this.posY);
                    checkWithPlayer(preX, preY, this.posX, this.posY);
                    return;
                case 1:
                    this.posX += this.velocity;
                    if (this.posY + this.velY > getGroundY(this.posX, this.posY)) {
                        this.posY = getGroundY(this.posX, this.posY);
                        this.state = 0;
                        if (this.posX < player.getCheckPositionX()) {
                            this.drawer.setActionId(2);
                            this.drawer.setTrans(2);
                            this.drawer.setLoop(false);
                        } else {
                            this.drawer.setActionId(2);
                            this.drawer.setTrans(0);
                            this.drawer.setLoop(false);
                        }
                    } else {
                        this.velY += DRIP_ACC;
                        this.posY += this.velY;
                    }
                    if (this.drawer.checkEnd()) {
                        this.state = 0;
                        if (this.posX < player.getCheckPositionX()) {
                            this.drawer.setActionId(2);
                            this.drawer.setTrans(2);
                            this.drawer.setLoop(false);
                        } else {
                            this.drawer.setActionId(2);
                            this.drawer.setTrans(0);
                            this.drawer.setLoop(false);
                        }
                    }
                    checkWithPlayer(preX, preY, this.posX, this.posY);
                    return;
                case 2:
                    if (this.drawer.checkEnd()) {
                        this.state = 0;
                        if (this.drawer.getTransId() == 2) {
                            this.drawer.setActionId(0);
                            this.drawer.setTrans(2);
                            this.drawer.setLoop(true);
                        } else if (this.drawer.getTransId() == 0) {
                            this.drawer.setActionId(0);
                            this.drawer.setTrans(0);
                            this.drawer.setLoop(true);
                        }
                    }
                    if (jumpChk()) {
                        if (this.posX < player.getCheckPositionX()) {
                            if (this.velocity < 0) {
                                this.velocity = -this.velocity;
                            }
                        } else if (this.velocity > 0) {
                            this.velocity = -this.velocity;
                        }
                        jumpInit();
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    public void draw(MFGraphics g) {
        if (!this.dead) {
            drawInMap(g, this.drawer);
            drawCollisionRect(g);
            if (SonicDebug.showCollisionRect) {
                drawAlertRangeLine(g, this.alert_state, this.posX >> 6, this.posY >> 6, MapManager.getCamera());
            }
        }
    }

    public void refreshCollisionRect(int x, int y) {
        if (this.state == 1) {
            this.collisionRect.setRect(x - 448, y - COLLISION_HEIGHT, COLLISION_WIDTH_JUMP, 768);
        } else {
            this.collisionRect.setRect(x - 512, y - COLLISION_HEIGHT, 1024, COLLISION_HEIGHT);
        }
    }
}
