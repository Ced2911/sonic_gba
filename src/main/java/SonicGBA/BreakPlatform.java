package SonicGBA;

import Lib.Coordinate;
import Lib.MyAPI;
import Lib.SoundSystem;
import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;
import java.lang.reflect.Array;

/* compiled from: GimmickObject */
class BreakPlatform extends GimmickObject {
    private static final int BREAK_HEIGHT = 8;
    private static final int BREAK_WIDTH = 8;
    private static final int COLLISION_HEIGHT = 2048;
    private static final int COLLISION_WIDTH = 4096;
    private static final int REST_FRAME = 3;
    private static MFImage platformImage = null;
    private int blockNumX;
    private int blockNumY;
    private int breakCount;
    private boolean breakFlag = false;
    private int[][] breakVelY;
    private int[][] breakY;

    protected BreakPlatform(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        if (platformImage != null) {
            return;
        }
        if (StageManager.getCurrentZoneId() != 6) {
            platformImage = MFImage.createImage("/gimmick/break_platform_" + StageManager.getCurrentZoneId() + ".png");
        } else {
            platformImage = MFImage.createImage("/gimmick/break_platform_" + StageManager.getCurrentZoneId() + (StageManager.getStageID() - 9) + ".png");
        }
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (!this.used || !isStandingOver()) {
            switch (direction) {
                case 1:
                    object.beStop(this.collisionRect.y0, 1, this);
                    if (!this.used) {
                        this.used = true;
                        initialBreaking(platformImage);
                        SoundSystem.getInstance().playSe(45);
                        return;
                    }
                    return;
                case 4:
                    if (object.getMoveDistance().f14y > 0 && object.getCollisionRect().y1 < this.collisionRect.y1) {
                        object.beStop(this.collisionRect.y0, 1, this);
                        if (!this.used) {
                            this.used = true;
                            initialBreaking(platformImage);
                            SoundSystem.getInstance().playSe(45);
                            return;
                        }
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    public void logic() {
        if (this.used && isStandingOver()) {
            player.cancelFootObject(this);
        }
    }

    public void doInitWhileInCamera() {
        this.used = false;
        this.breakFlag = false;
    }

    public int getPaintLayer() {
        return 0;
    }

    public void draw(MFGraphics g) {
        if (this.breakFlag) {
            breakingDraw(g, camera);
        } else {
            drawInMap(g, platformImage, 0, 0, MyAPI.zoomIn(platformImage.getWidth()), MyAPI.zoomIn(platformImage.getHeight()), this.iLeft == 0 ? 0 : 2, this.posX, this.posY, (this.iLeft == 0 ? 4 : 8) | 16);
        }
    }

    public void refreshCollisionRect(int x, int y) {
        if (this.iLeft == 0) {
            this.collisionRect.setRect(x, y, 4096, 2048);
        } else {
            this.collisionRect.setRect(x - 4096, y, 4096, 2048);
        }
    }

    private void initialBreaking(MFImage image) {
        this.blockNumX = MyAPI.zoomIn(image.getWidth() / 8);
        this.blockNumY = MyAPI.zoomIn(image.getHeight() / 8);
        this.breakY = (int[][]) Array.newInstance(Integer.TYPE, new int[]{this.blockNumX, this.blockNumY});
        this.breakVelY = (int[][]) Array.newInstance(Integer.TYPE, new int[]{this.blockNumX, this.blockNumY});
        this.breakCount = 0;
        this.breakFlag = true;
    }

    private void breakingDraw(MFGraphics g, Coordinate camera) {
        if (this.breakFlag) {
            int i;
            int[] iArr;
            int i2;
            if (!GameObject.IsGamePause) {
                for (i = 0; i < this.breakCount; i++) {
                    iArr = this.breakVelY[(this.blockNumX - 1) - (i % this.blockNumX)];
                    i2 = i / this.blockNumX;
                    iArr[i2] = iArr[i2] + GRAVITY;
                }
                this.breakCount++;
                if (this.breakCount > this.blockNumX * this.blockNumY) {
                    this.breakCount = this.blockNumX * this.blockNumY;
                }
            }
            for (i = 0; i < this.blockNumX * this.blockNumY; i++) {
                int i3;
                if (!GameObject.IsGamePause) {
                    iArr = this.breakY[i % this.blockNumX];
                    i2 = i / this.blockNumX;
                    iArr[i2] = iArr[i2] + this.breakVelY[i % this.blockNumX][i / this.blockNumX];
                }
                MFImage mFImage = platformImage;
                int i4 = (i % this.blockNumX) * 8;
                int i5 = ((this.blockNumY - 1) - (i / this.blockNumX)) * 8;
                int i6 = this.iLeft == 0 ? 0 : 2;
                int i7 = (((this.iLeft == 0 ? 1 : -1) * (i % this.blockNumX)) * 512) + this.posX;
                int i8 = ((((this.blockNumY - 1) - (i / this.blockNumX)) * 512) + this.posY) + this.breakY[i % this.blockNumX][i / this.blockNumX];
                if (this.iLeft == 0) {
                    i3 = 4;
                } else {
                    i3 = 8;
                }
                drawInMap(g, mFImage, i4, i5, 8, 8, i6, i7, i8, i3 | 16);
            }
        }
    }

    private boolean isBreakingOver() {
        if (this.breakCount == this.blockNumX * this.blockNumY) {
            return true;
        }
        return false;
    }

    private boolean isStandingOver() {
        if (this.breakCount >= this.blockNumX * (this.blockNumY - 1)) {
            return true;
        }
        return false;
    }

    public void close() {
    }

    public static void releaseAllResource() {
        platformImage = null;
    }
}
