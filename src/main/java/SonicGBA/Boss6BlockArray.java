package SonicGBA;

import GameEngine.Def;
import com.sega.mobile.define.MDPhone;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: GimmickObject */
class Boss6BlockArray extends GimmickObject {
    private static final int BLOCK_NUM = 9;
    private static final int BLOCK_SIZE = 2048;
    private static final int COLLISION_HEIGHT = 1536;
    private static final int COLLISION_WIDTH = 18432;
    private static final int DEEP_STANDARD_OFFSET = 8;
    private static final int TYPE_DEEP = 1;
    private static final int TYPE_NORMAL = 0;
    private Boss6Block[] block;
    private int[] blockOffsetY;
    private int blockOrgPosY;
    private int blockStartX;
    private boolean collisionFlag;
    private int deep_cn;
    private int normal_cn;
    private int playerPosY;
    private int preblockID = -1;
    private int type;

    protected Boss6BlockArray(int x, int y) {
        super(Def.TOUCH_A_Y, x, y, 0, 0, 0, 0);
        this.posX += 1024;
        this.blockStartX = this.posX - 8192;
        this.blockOrgPosY = this.posY;
        this.playerPosY = this.posY - 1024;
        this.block = new Boss6Block[9];
        this.blockOffsetY = new int[9];
        for (int i = 0; i < 9; i++) {
            this.block[i] = new Boss6Block(this.blockStartX + (i * 2048), this.blockOrgPosY);
        }
        this.collisionFlag = false;
        this.deep_cn = 0;
        this.normal_cn = 0;
        this.preblockID = -1;
    }

    public void draw(MFGraphics g) {
        for (int i = 0; i < 9; i++) {
            this.block[i].draw(g);
        }
        drawCollisionRect(g);
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        this.collisionFlag = true;
        if (player.getVelY() > 1000) {
            this.type = 1;
            this.deep_cn = 0;
        } else if (player.getVelY() >= 0 && this.deep_cn == 5) {
            this.type = 0;
            this.deep_cn = 0;
        }
    }

    public void doWhileNoCollision() {
        this.collisionFlag = false;
        this.deep_cn = 0;
    }

    public int getBossY(int bossPosX) {
        return this.blockOrgPosY + this.blockOffsetY[((bossPosX - this.blockStartX) + 1024) / 2048];
    }

    private void logicShake() {
        int i;
        int[] iArr;
        int[] iArr2;
        int blockID = ((player.getFootPositionX() - this.blockStartX) + 1024) / 2048;
        if (this.collisionFlag && this.type == 1) {
            switch (this.deep_cn) {
                case 0:
                    if (blockID == 1) {
                        this.blockOffsetY[0] = 256;
                        for (i = 1; i < 9; i++) {
                            this.blockOffsetY[i] = ((9 - i) * 2) << 6;
                        }
                    } else if (blockID == 7) {
                        this.blockOffsetY[8] = 256;
                        for (i = 0; i < 8; i++) {
                            this.blockOffsetY[i] = ((i + 1) * 2) << 6;
                        }
                    } else if (blockID == 0) {
                        this.blockOffsetY[0] = 256;
                        iArr = this.blockOffsetY;
                        this.blockOffsetY[2] = PlayerSonic.BACK_JUMP_SPEED_X;
                        iArr[1] = PlayerSonic.BACK_JUMP_SPEED_X;
                        iArr = this.blockOffsetY;
                        iArr2 = this.blockOffsetY;
                        this.blockOffsetY[5] = 256;
                        iArr2[4] = 256;
                        iArr[3] = 256;
                        iArr = this.blockOffsetY;
                        iArr2 = this.blockOffsetY;
                        this.blockOffsetY[8] = 128;
                        iArr2[7] = 128;
                        iArr[6] = 128;
                    } else if (blockID == 8) {
                        this.blockOffsetY[8] = 256;
                        iArr = this.blockOffsetY;
                        this.blockOffsetY[6] = PlayerSonic.BACK_JUMP_SPEED_X;
                        iArr[7] = PlayerSonic.BACK_JUMP_SPEED_X;
                        iArr = this.blockOffsetY;
                        iArr2 = this.blockOffsetY;
                        this.blockOffsetY[3] = 256;
                        iArr2[4] = 256;
                        iArr[5] = 256;
                        iArr = this.blockOffsetY;
                        iArr2 = this.blockOffsetY;
                        this.blockOffsetY[0] = 128;
                        iArr2[1] = 128;
                        iArr[2] = 128;
                    } else {
                        i = 0;
                        while (i < 9) {
                            if (i == blockID - 4 || i == blockID + 4) {
                                this.blockOffsetY[i] = 128;
                            } else if (i == blockID - 3 || i == blockID + 3) {
                                this.blockOffsetY[i] = 256;
                            } else if (i == blockID - 2 || i == blockID + 2) {
                                this.blockOffsetY[i] = MDPhone.SCREEN_HEIGHT;
                            } else if (i == blockID - 1 || i == blockID + 1) {
                                this.blockOffsetY[i] = 896;
                            } else if (i == blockID) {
                                this.blockOffsetY[i] = 1024;
                            } else {
                                this.blockOffsetY[i] = 128;
                            }
                            i++;
                        }
                    }
                    if (this.blockOffsetY[0] >= 256) {
                        this.blockOffsetY[0] = 256;
                    }
                    if (this.blockOffsetY[8] >= 256) {
                        this.blockOffsetY[8] = 256;
                    }
                    this.deep_cn++;
                    break;
                case 1:
                    if (blockID == 1) {
                        this.blockOffsetY[0] = 256;
                        for (i = 1; i < 9; i++) {
                            this.blockOffsetY[i] = (9 - i) << 6;
                        }
                    } else if (blockID == 7) {
                        this.blockOffsetY[8] = 256;
                        for (i = 0; i < 8; i++) {
                            this.blockOffsetY[i] = (i + 1) << 6;
                        }
                    } else if (blockID == 0) {
                        this.blockOffsetY[0] = 256;
                        iArr = this.blockOffsetY;
                        this.blockOffsetY[2] = 192;
                        iArr[1] = 192;
                        iArr = this.blockOffsetY;
                        iArr2 = this.blockOffsetY;
                        this.blockOffsetY[5] = 128;
                        iArr2[4] = 128;
                        iArr[3] = 128;
                        iArr = this.blockOffsetY;
                        iArr2 = this.blockOffsetY;
                        this.blockOffsetY[8] = 64;
                        iArr2[7] = 64;
                        iArr[6] = 64;
                    } else if (blockID == 8) {
                        this.blockOffsetY[8] = 256;
                        iArr = this.blockOffsetY;
                        this.blockOffsetY[6] = 192;
                        iArr[7] = 192;
                        iArr = this.blockOffsetY;
                        iArr2 = this.blockOffsetY;
                        this.blockOffsetY[3] = 128;
                        iArr2[4] = 128;
                        iArr[5] = 128;
                        iArr = this.blockOffsetY;
                        iArr2 = this.blockOffsetY;
                        this.blockOffsetY[0] = 64;
                        iArr2[1] = 64;
                        iArr[2] = 64;
                    } else {
                        i = 0;
                        while (i < 9) {
                            if (i == blockID - 4 || i == blockID + 4) {
                                this.blockOffsetY[i] = 64;
                            } else if (i == blockID - 3 || i == blockID + 3) {
                                this.blockOffsetY[i] = 128;
                            } else if (i == blockID - 2 || i == blockID + 2) {
                                this.blockOffsetY[i] = 320;
                            } else if (i == blockID - 1 || i == blockID + 1) {
                                this.blockOffsetY[i] = 448;
                            } else if (i == blockID) {
                                this.blockOffsetY[i] = 512;
                            } else {
                                this.blockOffsetY[i] = 64;
                            }
                            i++;
                        }
                    }
                    if (this.blockOffsetY[0] >= 256) {
                        this.blockOffsetY[0] = 256;
                    }
                    if (this.blockOffsetY[8] >= 256) {
                        this.blockOffsetY[8] = 256;
                    }
                    this.deep_cn++;
                    break;
                case 2:
                    for (i = 0; i < 9; i++) {
                        this.blockOffsetY[i] = 0;
                    }
                    this.deep_cn++;
                    break;
                case 3:
                    if (blockID == 1) {
                        this.blockOffsetY[0] = 0;
                        for (i = 1; i < 9; i++) {
                            this.blockOffsetY[i] = (-(9 - i)) << 6;
                        }
                    } else if (blockID == 7) {
                        this.blockOffsetY[8] = 0;
                        for (i = 0; i < 8; i++) {
                            this.blockOffsetY[i] = (-(i + 1)) << 6;
                        }
                    } else if (blockID == 0) {
                        this.blockOffsetY[0] = 0;
                        iArr = this.blockOffsetY;
                        this.blockOffsetY[2] = -192;
                        iArr[1] = -192;
                        iArr = this.blockOffsetY;
                        iArr2 = this.blockOffsetY;
                        this.blockOffsetY[5] = Def.TOUCH_HELP_LEFT_X;
                        iArr2[4] = Def.TOUCH_HELP_LEFT_X;
                        iArr[3] = Def.TOUCH_HELP_LEFT_X;
                        iArr = this.blockOffsetY;
                        iArr2 = this.blockOffsetY;
                        this.blockOffsetY[8] = -64;
                        iArr2[7] = -64;
                        iArr[6] = -64;
                    } else if (blockID == 8) {
                        this.blockOffsetY[8] = 0;
                        iArr = this.blockOffsetY;
                        this.blockOffsetY[6] = -192;
                        iArr[7] = -192;
                        iArr = this.blockOffsetY;
                        iArr2 = this.blockOffsetY;
                        this.blockOffsetY[3] = Def.TOUCH_HELP_LEFT_X;
                        iArr2[4] = Def.TOUCH_HELP_LEFT_X;
                        iArr[5] = Def.TOUCH_HELP_LEFT_X;
                        iArr = this.blockOffsetY;
                        iArr2 = this.blockOffsetY;
                        this.blockOffsetY[0] = -64;
                        iArr2[1] = -64;
                        iArr[2] = -64;
                    } else {
                        i = 0;
                        while (i < 9) {
                            if (i == blockID - 4 || i == blockID + 4) {
                                this.blockOffsetY[i] = -64;
                            } else if (i == blockID - 3 || i == blockID + 3) {
                                this.blockOffsetY[i] = Def.TOUCH_HELP_LEFT_X;
                            } else if (i == blockID - 2 || i == blockID + 2) {
                                this.blockOffsetY[i] = -320;
                            } else if (i == blockID - 1 || i == blockID + 1) {
                                this.blockOffsetY[i] = -448;
                            } else if (i == blockID) {
                                this.blockOffsetY[i] = -512;
                            } else {
                                this.blockOffsetY[i] = -64;
                            }
                            i++;
                        }
                    }
                    if (this.blockOffsetY[0] <= 0) {
                        this.blockOffsetY[0] = 0;
                    }
                    if (this.blockOffsetY[8] <= 0) {
                        this.blockOffsetY[8] = 0;
                    }
                    this.deep_cn++;
                    break;
                case 4:
                    for (i = 0; i < 9; i++) {
                        this.blockOffsetY[i] = 0;
                    }
                    this.deep_cn++;
                    break;
                case 5:
                    this.type = 0;
                    this.normal_cn = 0;
                    break;
            }
        }
        if (this.collisionFlag && this.type == 0) {
            if (this.preblockID != blockID) {
                this.normal_cn = 0;
            }
            this.preblockID = blockID;
            this.deep_cn = 0;
            if (blockID == 1) {
                this.blockOffsetY[0] = 256;
                for (i = 1; i < 9; i++) {
                    this.blockOffsetY[i] = (9 - i) << 6;
                }
            } else if (blockID == 7) {
                this.blockOffsetY[8] = 256;
                for (i = 0; i < 8; i++) {
                    this.blockOffsetY[i] = (i + 1) << 6;
                }
            } else if (blockID == 0) {
                this.blockOffsetY[0] = 256;
                iArr = this.blockOffsetY;
                this.blockOffsetY[2] = 192;
                iArr[1] = 192;
                iArr = this.blockOffsetY;
                iArr2 = this.blockOffsetY;
                this.blockOffsetY[5] = 128;
                iArr2[4] = 128;
                iArr[3] = 128;
                iArr = this.blockOffsetY;
                iArr2 = this.blockOffsetY;
                this.blockOffsetY[8] = 64;
                iArr2[7] = 64;
                iArr[6] = 64;
            } else if (blockID == 8) {
                this.blockOffsetY[8] = 256;
                iArr = this.blockOffsetY;
                this.blockOffsetY[6] = 192;
                iArr[7] = 192;
                iArr = this.blockOffsetY;
                iArr2 = this.blockOffsetY;
                this.blockOffsetY[3] = 128;
                iArr2[4] = 128;
                iArr[5] = 128;
                iArr = this.blockOffsetY;
                iArr2 = this.blockOffsetY;
                this.blockOffsetY[0] = 64;
                iArr2[1] = 64;
                iArr[2] = 64;
            } else {
                i = 0;
                while (i < 9) {
                    if (i == blockID - 4 || i == blockID + 4) {
                        this.blockOffsetY[i] = 64;
                    } else if (i == blockID - 3 || i == blockID + 3) {
                        this.blockOffsetY[i] = 128;
                    } else if (i == blockID - 2 || i == blockID + 2) {
                        this.blockOffsetY[i] = 320;
                    } else if (i == blockID - 1 || i == blockID + 1) {
                        this.blockOffsetY[i] = 448;
                    } else if (i == blockID) {
                        this.blockOffsetY[i] = 512;
                    } else {
                        this.blockOffsetY[i] = 64;
                    }
                    i++;
                }
            }
            if (this.blockOffsetY[0] >= 256) {
                this.blockOffsetY[0] = 256;
            }
            if (this.blockOffsetY[8] >= 256) {
                this.blockOffsetY[8] = 256;
            }
        }
    }

    public void logic() {
        int i;
        refreshCollisionRect(this.posX, this.posY);
        if (collisionChkWithObject(player)) {
            doWhileCollisionWrap(player);
        } else if (this.deep_cn == 0) {
            doWhileNoCollision();
        }
        if (player.collisionState == (byte) 1) {
            for (i = 0; i < 9; i++) {
                this.blockOffsetY[i] = 0;
            }
        }
        logicShake();
        for (i = 0; i < 9; i++) {
            this.block[i].logic(this.blockStartX + (i * 2048), this.blockOrgPosY + this.blockOffsetY[i]);
        }
    }

    public void setDisplayState() {
        for (int i = 0; i < 9; i++) {
            this.block[i].setDisplayState(false);
            Effect.showEffect(destroyEffectAnimation, 0, (this.blockStartX + (i * 2048)) >> 6, (this.posY >> 6) - 10, 0);
        }
        PlayerObject playerObject = player;
        PlayerObject playerObject2 = player;
        playerObject.collisionState = (byte) 1;
        player.setAnimationId(10);
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 9216, y - 768, COLLISION_WIDTH, 1536);
    }
}
