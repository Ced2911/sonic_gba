package SonicGBA;

import GameEngine.Def;
import Lib.MyAPI;
import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;

/* compiled from: BackGroundManager */
class BackManagerStage5 extends BackGroundManager {
    private static int BG_WIDTH = 256;
    private static final int CLOUD_Y = 0;
    private static int IMAGE_WIDTH = 256;
    private MFImage backImage;
    private MFImage cloudImage;
    private int cloudPosX;

    public BackManagerStage5() {
        try {
            this.backImage = MFImage.createImage("/map/stage5_bg_0.png");
            this.cloudImage = MFImage.createImage("/map/stage5_bg_1.png");
            IMAGE_WIDTH = MyAPI.zoomIn(this.cloudImage.getWidth(), true);
            BG_WIDTH = MyAPI.zoomIn(this.backImage.getWidth(), true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void close() {
        this.backImage = null;
        this.cloudImage = null;
    }

    public void draw(MFGraphics g) {
        MyAPI.drawImage(g, this.backImage, 0, 0, 20);
        if (BG_WIDTH < Def.SCREEN_WIDTH) {
            MyAPI.drawImage(g, this.backImage, BG_WIDTH, 0, 20);
        }
        if (!GameObject.IsGamePause) {
            this.cloudPosX++;
            this.cloudPosX %= IMAGE_WIDTH;
        }
        for (int j = -IMAGE_WIDTH; j < MapManager.CAMERA_WIDTH + this.cloudPosX; j += IMAGE_WIDTH) {
            MyAPI.drawImage(g, this.cloudImage, this.cloudPosX + j, 0, 20);
        }
    }
}
