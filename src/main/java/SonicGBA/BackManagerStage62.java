package SonicGBA;

import Lib.MyAPI;
import com.sega.mobile.framework.device.MFGamePad;
import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;

/* compiled from: BackGroundManager */
class BackManagerStage62 extends BackGroundManager {
    private static int IMAGE_HEIGHT = 232;
    private static int IMAGE_WIDTH = 256;
    private static final int[][] LIGHT_PARAM = null;
    private static MFImage[] bgImage;
    private int[] lightX = new int[LIGHT_PARAM.length];

    /*
    static {
        r0 = new int[2][];
        int[] iArr = new int[]{120, 256, 16, iArr};
        int[] iArr2 = new int[]{176, 256, 16, iArr2};
        LIGHT_PARAM = r0;
    }
    */

    public BackManagerStage62() {
        if (bgImage == null) {
            bgImage = new MFImage[4];
            for (int i = 0; i < 4; i++) {
                bgImage[i] = MFImage.createImage("/map/stage6_bg_3/#" + (i + 1) + ".png");
            }
            IMAGE_WIDTH = bgImage[0].getWidth();
            IMAGE_HEIGHT = bgImage[0].getHeight();
        }
    }

    public void close() {
        if (bgImage != null) {
            for (int i = 0; i < 4; i++) {
                bgImage[i] = null;
            }
        }
        bgImage = null;
    }

    public void draw(MFGraphics g) {
        int cameraX = MapManager.getCamera().f13x;
        int cameraY = MapManager.getCamera().f14y;
        int tmpframe = (frame % (bgImage.length * 2)) / 2;
        MFGraphics mFGraphics = g;
        MyAPI.drawImage(mFGraphics, bgImage[tmpframe], 0, 0, IMAGE_WIDTH, IMAGE_HEIGHT, 0, (-cameraX) / 46, (-cameraY) / 33, 0);
        MyAPI.drawImage(g, bgImage[tmpframe], 0, 0, IMAGE_WIDTH, IMAGE_HEIGHT, 2, IMAGE_WIDTH + ((-cameraX) / 46), (-cameraY) / 33, 0);
        for (int i = 0; i < LIGHT_PARAM.length; i++) {
            for (int j = 0; j < ((SCREEN_WIDTH + 255) / 256) + 2; j++) {
                MyAPI.drawImage(g, bgImage[tmpframe], LIGHT_PARAM[i][0], LIGHT_PARAM[i][1], LIGHT_PARAM[i][2], LIGHT_PARAM[i][3], 0, ((j - 1) * 256) + (((-(cameraX / 38)) % 256) + (this.lightX[i] >> 6)), LIGHT_PARAM[i][1] + ((-cameraY) / 33), 0);
            }
            if (!GameObject.IsGamePause) {
                int[] iArr;
                if (i == 0) {
                    iArr = this.lightX;
                    iArr[i] = iArr[i] + 120;
                } else {
                    iArr = this.lightX;
                    iArr[i] = iArr[i] - 120;
                }
                iArr = this.lightX;
                iArr[i] = iArr[i] + MFGamePad.KEY_NUM_8;
                iArr = this.lightX;
                iArr[i] = iArr[i] % MFGamePad.KEY_NUM_8;
            }
        }
    }
}
