package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import Lib.SoundSystem;
import com.sega.engine.action.ACWorldCollisionCalculator;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: GimmickObject */
class PipeIn extends GimmickObject {
    private static int COLLISION_HEIGHT = BarHorbinV.COLLISION_WIDTH;
    private static int COLLISION_WIDTH = 2304;
    private static final int[] TRANS;
    public static Animation pipeAnimation;
    private int actionID;
    private int dirRect;
    private int direction;
    private AnimationDrawer drawer;
    private int transID;
    private int velx;
    private int vely;

    static {
        int[] iArr = new int[6];
        iArr[1] = 3;
        iArr[2] = 6;
        iArr[3] = 5;
        iArr[4] = 2;
        iArr[5] = 1;
        TRANS = iArr;
    }

    protected PipeIn(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        if (pipeAnimation == null) {
            if (StageManager.getCurrentZoneId() != 6) {
                pipeAnimation = new Animation("/animation/pipe_in");
            } else {
                pipeAnimation = new Animation("/animation/pipe_in" + StageManager.getCurrentZoneId() + (StageManager.getStageID() - 9));
            }
        }
        this.direction = width;
        switch (width) {
            case 0:
                this.direction = 0;
                this.dirRect = 0;
                this.transID = 0;
                this.actionID = 0;
                break;
            case 1:
                this.direction = 1;
                this.dirRect = 1;
                this.transID = 5;
                this.actionID = 0;
                break;
            case 2:
                this.direction = 2;
                this.dirRect = 2;
                this.transID = 0;
                this.actionID = 2;
                break;
            case 3:
                this.direction = 3;
                this.dirRect = 3;
                this.transID = 4;
                this.actionID = 2;
                break;
        }
        this.velx = (left * 96) >> 6;
        this.vely = (top * 96) >> 6;
        if (StageManager.getCurrentZoneId() == 2 || StageManager.getStageID() == 10) {
            if (this.actionID == 0) {
                COLLISION_WIDTH = 2944;
                COLLISION_HEIGHT = 5632;
            } else if (this.actionID == 2) {
                COLLISION_WIDTH = 6144;
                COLLISION_HEIGHT = 2432;
            }
        } else if (StageManager.getStageID() == 11) {
            if (this.actionID == 0) {
                COLLISION_WIDTH = 3712;
                COLLISION_HEIGHT = 5504;
            } else if (this.actionID == 2) {
                COLLISION_WIDTH = 6912;
                COLLISION_HEIGHT = 3456;
            }
        }
        this.drawer = pipeAnimation.getDrawer(this.actionID, true, TRANS[this.transID]);
    }

    public void draw(MFGraphics g) {
        drawInMap(g, this.drawer);
        drawCollisionRect(g);
    }

    public void refreshCollisionRect(int x, int y) {
        if (StageManager.getCurrentZoneId() == 2 || StageManager.getStageID() == 10) {
            if (this.actionID == 0) {
                COLLISION_WIDTH = 2944;
                COLLISION_HEIGHT = 5632;
            } else if (this.actionID == 2) {
                COLLISION_WIDTH = 6144;
                COLLISION_HEIGHT = 2432;
            }
        } else if (StageManager.getStageID() == 11) {
            if (this.actionID == 0) {
                COLLISION_WIDTH = 3712;
                COLLISION_HEIGHT = 5504;
            } else if (this.actionID == 2) {
                COLLISION_WIDTH = 6912;
                COLLISION_HEIGHT = 3456;
            }
        }
        this.collisionRect.setRect(x - (COLLISION_WIDTH >> 1), y - (COLLISION_HEIGHT >> 1), COLLISION_WIDTH, COLLISION_HEIGHT);
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (!player.piping) {
            if (this.direction == direction) {
                switch (direction) {
                    case 0:
                        player.collisionState = (byte) 1;
                        ACWorldCollisionCalculator aCWorldCollisionCalculator = player.worldCal;
                        ACWorldCollisionCalculator aCWorldCollisionCalculator2 = player.worldCal;
                        aCWorldCollisionCalculator.actionState = (byte) 1;
                        player.posX = this.posX;
                        break;
                    case 1:
                        player.posX = this.posX;
                        break;
                    case 2:
                        player.posY = this.posY + 768;
                        break;
                    case 3:
                        player.posY = this.posY + 768;
                        break;
                }
                player.pipeIn(this.posX, this.posY, this.velx, this.vely);
                PlayerObject playerObject = player;
                if (PlayerObject.getCharacterID() == 3) {
                    SoundSystem.getInstance().playSe(25);
                } else {
                    SoundSystem.getInstance().playSe(4);
                }
                player.setAnimationId(4);
                player.stopMove();
                return;
            }
            object.beStop(0, direction, this);
        }
    }

    public void close() {
        this.drawer = null;
    }

    public int getPaintLayer() {
        return 2;
    }

    public static void releaseAllResource() {
        Animation.closeAnimation(pipeAnimation);
        pipeAnimation = null;
    }
}
