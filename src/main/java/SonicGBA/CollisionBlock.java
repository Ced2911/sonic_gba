package SonicGBA;

import com.sega.engine.action.ACBlock;
import com.sega.engine.action.ACWorld;

public class CollisionBlock extends ACBlock implements SonicDef {
    private static final byte[] BLANK_COLLISION_INFO = new byte[8];
    private boolean FLIP_X;
    private boolean FLIP_Y;
    private byte[] collisionInfo = BLANK_COLLISION_INFO;
    private int degree;
    public boolean extendsDegree;
    public boolean throughable;

    public CollisionBlock(ACWorld world) {
        super(world);
    }

    public void setProperty(byte[] collisionInfo, boolean FLIP_X, boolean FLIP_Y, int degree, boolean attr) {
        int i;
        this.collisionInfo = collisionInfo;
        this.FLIP_X = FLIP_X;
        this.FLIP_Y = FLIP_Y;
        if (degree < 0) {
            i = degree + 256;
        } else {
            i = degree;
        }
        this.degree = i;
        if (this.degree == 255) {
            this.extendsDegree = true;
        } else {
            this.extendsDegree = false;
        }
        boolean allEight = true;
        int i2 = 0;
        while (i2 < collisionInfo.length) {
            if (((collisionInfo[i2] & 240) >> 4) != 8 || ((collisionInfo[i2] & 15) >> 0) != 8) {
                allEight = false;
                break;
            }
            i2++;
        }
        if (allEight && (degree == 0 || degree == 180)) {
            this.extendsDegree = true;
        }
        this.throughable = attr;
    }

    public void setProperty(CollisionBlock anotherBlock) {
        setProperty(anotherBlock.collisionInfo, anotherBlock.FLIP_X, anotherBlock.FLIP_Y, anotherBlock.degree, anotherBlock.throughable);
    }

    public int getCollisionY(int x) {
        while (x < 0) {
            x += 8;
        }
        x %= 8;
        if (this.FLIP_X) {
            x = 7 - x;
        }
        int colInfo = (this.collisionInfo[x] & 240) >> 4;
        int re = 0;
        if (colInfo == 8) {
            return 0;
        }
        if (colInfo == 0) {
            return -1;
        }
        if (colInfo < 8) {
            re = colInfo;
        } else if (colInfo > 8) {
            re = 15 - colInfo;
        }
        if (this.FLIP_Y) {
            re = 7 - re;
        }
        return re;
    }

    public int getCollisionX(int y) {
        while (y < 0) {
            y += 8;
        }
        y %= 8;
        if (this.FLIP_Y) {
            y = 7 - y;
        }
        int colInfo = (this.collisionInfo[y] & 15) >> 0;
        int re = 0;
        if (colInfo == 8) {
            return 0;
        }
        if (colInfo == 0) {
            return -1;
        }
        if (colInfo < 8) {
            re = colInfo;
        } else if (colInfo > 8) {
            re = 15 - colInfo;
        }
        if (this.FLIP_X) {
            re = 7 - re;
        }
        return re;
    }

    public int getActualX(int y) {
        while (y < 0) {
            y += 8;
        }
        y %= 8;
        if (this.FLIP_Y) {
            y = 7 - y;
        }
        int re = (this.collisionInfo[y] & 15) >> 0;
        if (re == 8 || re == 0) {
            return re;
        }
        if (this.FLIP_X) {
            re = 7 - re;
        }
        return re;
    }

    public int getActualY(int x) {
        while (x < 0) {
            x += 8;
        }
        x %= 8;
        if (this.FLIP_X) {
            x = 7 - x;
        }
        int re = (this.collisionInfo[x] & 240) >> 4;
        if (re == 8 || re == 0) {
            return re;
        }
        if (this.FLIP_Y) {
            re = 7 - re;
        }
        return re;
    }

    public int getDegree() {
        int re = (this.degree * 360) / 256;
        if (re == 0) {
            return re;
        }
        if (this.FLIP_X) {
            re = -re;
        }
        if (this.FLIP_Y) {
            re = -re;
        }
        return (re + 360) % 360;
    }

    public int getDegreeNearby(int degree) {
        if (this.extendsDegree) {
            return degree;
        }
        int re = getDegree();
        int degreeDiff = Math.abs(degree - re);
        if (degreeDiff > 180) {
            degreeDiff = 360 - degreeDiff;
        }
        if (degreeDiff > 90) {
            re = (re + 180) % 360;
        }
        return re;
    }

    public boolean getReverseX(int y, int degree) {
        boolean reverse = false;
        while (y < 0) {
            y += 8;
        }
        y %= 8;
        if (this.FLIP_Y) {
            y = 7 - y;
        }
        int colInfo = (this.collisionInfo[y] & 15) >> 0;
        if (colInfo > 8) {
            if (null != null) {
                reverse = false;
            } else {
                reverse = true;
            }
        } else if (colInfo == 8 || colInfo == 0) {
            boolean allEight = true;
            for (int i = 0; i < 8; i++) {
                colInfo = (this.collisionInfo[(y + i) % 8] & 15) >> 0;
                if (colInfo > 0) {
                    if (colInfo > 8) {
                        if (null != null) {
                            reverse = false;
                        } else {
                            reverse = true;
                        }
                        allEight = false;
                    } else if (colInfo < 8) {
                        allEight = false;
                        break;
                    }
                }
            }
            if (allEight) {
                if (degree > 45 && degree < 135) {
                    return true;
                }
                if (degree > 225 && degree < 315) {
                    return false;
                }
            }
        }
        if (this.FLIP_X) {
            if (reverse) {
                reverse = false;
            } else {
                reverse = true;
            }
        }
        return reverse;
    }

    public boolean getReverseY(int x, int degree) {
        boolean reverse = false;
        while (x < 0) {
            x += 8;
        }
        x %= 8;
        if (this.FLIP_X) {
            x = 7 - x;
        }
        int colInfo = (this.collisionInfo[x] & 240) >> 4;
        if (colInfo > 8) {
            if (null != null) {
                reverse = false;
            } else {
                reverse = true;
            }
        } else if (colInfo == 8) {
            boolean allEight = true;
            for (int i = 0; i < 8; i++) {
                colInfo = (this.collisionInfo[(x + i) % 8] & 240) >> 4;
                if (colInfo >= 0) {
                    if (colInfo > 8) {
                        if (null != null) {
                            reverse = false;
                        } else {
                            reverse = true;
                        }
                        allEight = false;
                    } else if (colInfo < 8) {
                        allEight = false;
                        break;
                    }
                }
            }
            if (allEight) {
                if (degree > 135 && degree < 225) {
                    return true;
                }
                if (degree < 45 || degree > 315) {
                    return false;
                }
            }
        }
        if (this.FLIP_Y) {
            if (reverse) {
                reverse = false;
            } else {
                reverse = true;
            }
        }
        return reverse;
    }

    public boolean needJudgeX(int y) {
        if (this.degree != 0 && !this.extendsDegree) {
            return false;
        }
        boolean needJudge = true;
        if (((this.collisionInfo[y % 8] & 15) >> 0) != 8) {
            needJudge = false;
        }
        return needJudge;
    }

    public boolean needJudgeY(int x) {
        if (this.degree != 0 && !this.extendsDegree) {
            return false;
        }
        boolean needJudge = true;
        for (byte b : this.collisionInfo) {
            if (((b & 240) >> 4) != 8) {
                needJudge = false;
                break;
            }
        }
        return needJudge;
    }

    public int getCollisionYFromDown(int x) {
        if (this.throughable) {
            return -1000;
        }
        while (x < 0) {
            x += getWidth();
        }
        x = (x % getWidth()) >> 6;
        if (this.FLIP_X) {
            x = 7 - x;
        }
        int colInfo = (this.collisionInfo[x] & 240) >> 4;
        if (colInfo == 8) {
            return downSide;
        }
        if (colInfo == 0) {
            return -1000;
        }
        if ((colInfo > 8 && this.FLIP_Y) || (colInfo < 8 && !this.FLIP_Y)) {
            return downSide;
        }
        if (colInfo > 8) {
            colInfo = 15 - colInfo;
        }
        if (this.FLIP_Y) {
            colInfo = 7 - colInfo;
        }
        return ((colInfo + 1) << 6) - 1;
    }

    public int getCollisionYFromUp(int x) {
        while (x < 0) {
            x += getWidth();
        }
        x = (x % getWidth()) >> 6;
        if (this.FLIP_X) {
            x = 7 - x;
        }
        int colInfo = (this.collisionInfo[x] & 240) >> 4;
        if (colInfo == 8) {
            return 0;
        }
        if (colInfo == 0) {
            return -1000;
        }
        if ((colInfo > 8 && !this.FLIP_Y) || (colInfo < 8 && this.FLIP_Y)) {
            return 0;
        }
        if (colInfo > 8) {
            colInfo = 15 - colInfo;
        }
        if (this.FLIP_Y) {
            colInfo = 7 - colInfo;
        }
        return colInfo << 6;
    }

    public int getCollisionXFromLeft(int y) {
        if (this.throughable) {
            return -1000;
        }
        while (y < 0) {
            y += getHeight();
        }
        y = (y % getHeight()) >> 6;
        if (this.FLIP_Y) {
            y = 7 - y;
        }
        int colInfo = (this.collisionInfo[y] & 15) >> 0;
        if (colInfo == 8) {
            return 0;
        }
        if (colInfo == 0) {
            return -1000;
        }
        if ((colInfo > 8 && !this.FLIP_X) || (colInfo < 8 && this.FLIP_X)) {
            return 0;
        }
        if (colInfo > 8) {
            colInfo = 15 - colInfo;
        }
        if (this.FLIP_X) {
            colInfo = 7 - colInfo;
        }
        return colInfo << 6;
    }

    public int getCollisionXFromRight(int y) {
        if (this.throughable) {
            return -1000;
        }
        while (y < 0) {
            y += getHeight();
        }
        y = (y % getHeight()) >> 6;
        if (this.FLIP_Y) {
            y = 7 - y;
        }
        int colInfo = (this.collisionInfo[y] & 15) >> 0;
        if (colInfo == 8) {
            return rightSide;
        }
        if (colInfo == 0) {
            return -1000;
        }
        if ((colInfo > 8 && this.FLIP_X) || (colInfo < 8 && !this.FLIP_X)) {
            return rightSide;
        }
        if (colInfo > 8) {
            colInfo = 15 - colInfo;
        }
        if (this.FLIP_X) {
            colInfo = 7 - colInfo;
        }
        return ((colInfo + 1) << 6) - 1;
    }

    public int getDegree(int degree, int direction) {
        if (this.extendsDegree) {
            return degree;
        }
        int re = getDegree();
        if (re == 90 || re == 270) {
            if (direction == 0) {
                re = 0;
            } else if (direction == 2) {
                re = RollPlatformSpeedC.DEGREE_VELOCITY;
            }
        }
        if (re == RollPlatformSpeedC.DEGREE_VELOCITY || re == 0) {
            if (direction == 1) {
                re = 90;
            } else if (direction == 3) {
                re = 270;
            }
        }
        int degreeDiff = Math.abs(degree - re);
        if (degreeDiff > RollPlatformSpeedC.DEGREE_VELOCITY) {
            degreeDiff = 360 - degreeDiff;
        }
        if (degreeDiff > 90) {
            re = (re + RollPlatformSpeedC.DEGREE_VELOCITY) % 360;
        }
        return re;
    }

    public void doBeforeCollisionCheck() {
    }
}
