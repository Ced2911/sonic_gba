package SonicGBA;

import Lib.SoundSystem;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: GimmickObject */
class PipeSet extends GimmickObject {
    private static final int COLLISION_HEIGHT = 1024;
    private static final int COLLISION_WIDTH = 1024;
    private int posx;
    private int posy;
    private boolean terminal;
    private boolean touching;
    private int velx;
    private int vely;

    protected PipeSet(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        this.velx = (left * 1920) / width;
        this.vely = (top * 1920) / width;
        this.terminal = height == 127;
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 512, y - 512, 1024, 1024);
    }

    private void pipeSetLogic() {
        PlayerObject playerObject = null;
        if (this.terminal) {
            player.pipeSet(this.posX, this.posY, this.velx, this.vely);
            this.touching = true;
            playerObject = player;
            if (PlayerObject.getCharacterID() == 3) {
                SoundSystem.getInstance().playSe(25);
                return;
            } else {
                SoundSystem.getInstance().playSe(4);
                return;
            }
        }
        player.pipeSet(this.posX, this.posY, this.velx, this.vely);
        playerObject = player;
        if (PlayerObject.getCharacterID() == 3) {
            SoundSystem.getInstance().playSe(25);
        } else {
            SoundSystem.getInstance().playSe(4);
        }
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (!this.firstTouch) {
            return;
        }
        if (StageManager.getStageID() == 11) {
            if (player.piping) {
                pipeSetLogic();
            }
        } else if (this.iHeight == 0 || player.piping) {
            pipeSetLogic();
        }
    }

    public void doWhileNoCollision() {
        if (this.touching) {
            if (StageManager.getStageID() != 11) {
                player.pipeOut();
            }
            this.touching = false;
        }
    }

    public void draw(MFGraphics g) {
        drawCollisionRect(g);
    }

    public int getPaintLayer() {
        return 2;
    }
}
