package SonicGBA;

import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;

/* compiled from: GimmickObject */
class SpringPlatform extends GimmickObject {
    private static final int COLLISION_HEIGHT = 2304;
    private static final int COLLISION_WIDTH = 2048;
    private static final int FAR_DISTANCE = 3840;
    private static final int MOST_ACCELATE = 150;
    private static MFImage image;
    private int centerY;
    private int velY;

    protected SpringPlatform(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        if (image == null) {
            try {
                image = MFImage.createImage("/gimmick/spring_platform.png");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        this.centerY = this.posY;
        this.velY = 0;
    }

    public void draw(MFGraphics g) {
        drawInMap(g, image, 3);
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 1024, y - BarHorbinV.HOBIN_POWER, 2048, COLLISION_HEIGHT);
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        switch (direction) {
            case 1:
                if (this.firstTouch) {
                    this.velY += object.getVelY();
                }
                object.beStop(0, direction, this);
                break;
            case 4:
                if (object.getVelY() > 0 && object.getCollisionRect().y1 < this.collisionRect.y1) {
                    object.beStop(0, 1, this);
                    break;
                }
        }
        if (player.isFootOnObject(this)) {
            player.setSqueezeEnable(false);
        }
    }

    public void logic() {
        int accelerate = ((this.centerY - this.posY) * MOST_ACCELATE) / FAR_DISTANCE;
        int preX = this.posX;
        int preY = this.posY;
        this.velY += accelerate;
        if (this.velY > 0) {
            this.velY -= 10;
        } else if (this.velY < 0) {
            this.velY += 10;
        }
        this.posY += this.velY;
        if (this.posY > this.centerY + FAR_DISTANCE) {
            this.posY = this.centerY + FAR_DISTANCE;
            this.velY = 0;
        }
        if (this.posY < this.centerY - FAR_DISTANCE) {
            this.posY = this.centerY - FAR_DISTANCE;
            this.velY = 0;
        }
        checkWithPlayer(preX, preY, this.posX, this.posY);
        if (!player.isFootOnObject(this)) {
            player.setSqueezeEnable(true);
        }
    }

    public static void releaseAllResource() {
        image = null;
    }

    public boolean collisionChkWithObject(PlayerObject object) {
        CollisionRect objectRect = object.getCollisionRect();
        CollisionRect thisRect = getCollisionRect();
        rectV.setRect(objectRect.x0 + 192, objectRect.y0, objectRect.getWidth() - PlayerSonic.BACK_JUMP_SPEED_X, objectRect.getHeight());
        return thisRect.collisionChk(rectV);
    }

    public int getPaintLayer() {
        return 0;
    }
}
