package SonicGBA;

import Lib.Animation;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: BulletObject */
class LaserDamage extends BulletObject {
    private boolean isDead;
    private byte[] rect;

    protected LaserDamage(int x, int y) {
        super(x, y, 0, 0, false);
        if (laserAnimation == null) {
            laserAnimation = new Animation("/animation/boss_extra_bullet");
        }
        this.drawer = laserAnimation.getDrawer(0, false, 0);
        this.drawer.setPause(true);
        this.isDead = false;
        this.posY = getGroundY(this.posX, this.posY);
    }

    public void bulletLogic() {
        if (this.drawer.checkEnd()) {
            this.isDead = true;
            return;
        }
        this.drawer.moveOn();
        this.rect = this.drawer.getARect();
        refreshCollisionRect(this.posX, this.posY);
        doWhileCollisionWrapWithPlayer();
    }

    public void draw(MFGraphics g) {
        drawInMap(g, this.drawer);
    }

    public void refreshCollisionRect(int x, int y) {
        if (this.rect != null) {
            this.collisionRect.setRect((this.rect[0] << 6) + x, (this.rect[1] << 6) + y, this.rect[2] << 6, this.rect[3] << 6);
        }
    }

    public boolean chkDestroy() {
        return this.isDead;
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (!player.isAttackingEnemy()) {
            player.beHurt();
        }
    }
}
