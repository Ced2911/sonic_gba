package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: GimmickObject */
class Bubble extends GimmickObject {
    private static final int BREATHE_BUBBLE_TIME = 2500;
    private static final int UP_BUBBLE_TIME = 1000;
    private static Animation baseAnimation;
    private static final int[] createPuyo = new int[]{70, 80, 32, 54, 32, 80, 64, 32};
    private AnimationDrawer baseDrawer;
    private boolean breatheBubbleFlag = false;
    private int createPuyoCount = 0;
    private int createPuyoType = 0;
    private boolean upBubbleFlag = false;

    protected Bubble(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        if (baseAnimation == null) {
            baseAnimation = new Animation("/animation/bubble_base");
        }
        if (baseAnimation != null) {
            this.baseDrawer = baseAnimation.getDrawer(0, true, 0);
        }
    }

    public void logic() {
        if ((System.currentTimeMillis() / 1000) % 2 == 0 && !this.upBubbleFlag) {
            GameObject.addGameObject(new UpBubble(this.posX, this.posY), this.posX, this.posY);
            this.upBubbleFlag = true;
        } else if ((System.currentTimeMillis() / 1000) % 2 != 0) {
            this.upBubbleFlag = false;
        }
        this.createPuyoCount++;
        if (this.createPuyoType >= createPuyo.length) {
            this.createPuyoType = 0;
        }
        if (this.createPuyoCount >= createPuyo[this.createPuyoType]) {
            this.createPuyoCount = 0;
            this.createPuyoType++;
            GameObject.addGameObject(new BreatheBubble(this.posX, this.posY), this.posX, this.posY);
            this.breatheBubbleFlag = true;
        } else if ((System.currentTimeMillis() / 2500) % 2 != 0) {
            this.breatheBubbleFlag = false;
        }
    }

    public void draw(MFGraphics g) {
        drawInMap(g, this.baseDrawer);
    }

    public int getPaintLayer() {
        return 0;
    }

    public void close() {
        this.baseDrawer = null;
    }

    public static void releaseAllResource() {
        Animation.closeAnimation(baseAnimation);
        baseAnimation = null;
    }
}
