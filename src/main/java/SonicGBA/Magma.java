package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import com.sega.mobile.define.MDPhone;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: EnemyObject */
class Magma extends EnemyObject {
    private static final int COLLISION_HEIGHT = 1280;
    private static final int COLLISION_WIDTH = 1280;
    private static boolean IsFire = false;
    private static final int STATE_UP = 0;
    private static final int STATE_WAIT = 1;
    private static Animation magmaAnimation;
    private static AnimationDrawer magmaDrawer;
    private static int state;
    private static int velocity = -768;
    private static int wait_cnt;
    private static int wait_cnt_max = 64;
    private int emenyid;
    private int fire_start_speed = PlayerSonic.BACK_JUMP_SPEED_X;
    private int limitBottomY;
    private int limitTopY;

    public static void releaseAllResource() {
        Animation.closeAnimation(magmaAnimation);
        magmaAnimation = null;
    }

    protected Magma(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        this.posY += 1920;
        this.limitTopY = this.posY - this.mHeight;
        this.limitBottomY = this.posY;
        if (magmaAnimation == null) {
            magmaAnimation = new Animation("/animation/magma");
        }
        magmaDrawer = magmaAnimation.getDrawer(0, false, 0);
        wait_cnt = 0;
        this.emenyid = id;
        magmaDrawer.setPause(true);
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (!this.dead && object == player && state == 0) {
            player.beHurt();
        }
    }

    public void doWhileBeAttack(PlayerObject object, int direction, int animationID) {
    }

    public static void staticlogic() {
        magmaDrawer.moveOn();
        switch (state) {
            case 0:
                if (magmaDrawer.getCurrentFrame() < 6) {
                    velocity = -768;
                    IsFire = false;
                } else if (magmaDrawer.getCurrentFrame() == 6) {
                    velocity = 0;
                    IsFire = true;
                } else {
                    velocity = 0;
                    IsFire = false;
                }
                if (magmaDrawer.checkEnd()) {
                    state = 1;
                    wait_cnt = 0;
                    return;
                }
                return;
            case 1:
                if (wait_cnt < wait_cnt_max) {
                    wait_cnt++;
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void logic() {
        if (!this.dead) {
            int preX = this.posX;
            int preY = this.posY;
            switch (state) {
                case 0:
                    this.posY += velocity;
                    if (IsFire) {
                        BulletObject.addBullet(this.emenyid, this.posX, this.posY, -this.fire_start_speed, (-this.fire_start_speed) * 2);
                        BulletObject.addBullet(this.emenyid, this.posX, this.posY, this.fire_start_speed, (-this.fire_start_speed) * 2);
                    }
                    checkWithPlayer(preX, preY, this.posX, this.posY);
                    return;
                case 1:
                    this.posY = this.limitBottomY;
                    if (wait_cnt == wait_cnt_max) {
                        state = 0;
                        magmaDrawer.restart();
                        IsFire = false;
                    }
                    checkWithPlayer(preX, preY, this.posX, this.posY);
                    return;
                default:
                    return;
            }
        }
    }

    public void draw(MFGraphics g) {
        if (!this.dead && !magmaDrawer.checkEnd()) {
            drawInMap(g, magmaDrawer);
            drawCollisionRect(g);
        }
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - MDPhone.SCREEN_HEIGHT, y - MDPhone.SCREEN_HEIGHT, 1280, 1280);
    }
}
