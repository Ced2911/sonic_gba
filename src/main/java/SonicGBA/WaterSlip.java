package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import Lib.MyAPI;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: GimmickObject */
class WaterSlip extends GimmickObject {
    private static final int COLLISION_HEIGHT = 2048;
    private static final int COLLISION_WIDTH = 4096;
    private static final int DRAW_OFFSET_X = -1024;
    private static AnimationDrawer drawer;
    private static AnimationDrawer drawer2;
    private static AnimationDrawer drawer3;
    private static AnimationDrawer drawer4;
    private static int frame;
    private boolean isActive;
    private boolean isTouchSand = false;
    private int spaceX = -6144;
    private int spaceY = 3072;

    protected WaterSlip(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        if (StageManager.getCurrentZoneId() == 1) {
            if (drawer == null) {
                drawer = new Animation("/animation/water_fall_02").getDrawer(0, true, 0);
                drawer.setPause(true);
            }
        } else if (StageManager.getCurrentZoneId() == 5) {
            Animation a;
            if (this.iLeft == 1) {
                if (drawer == null || drawer2 == null) {
                    a = new Animation("/animation/sand_06");
                    drawer = a.getDrawer(1, true, 0);
                    drawer.setPause(true);
                    drawer2 = a.getDrawer(0, true, 0);
                    drawer2.setPause(true);
                }
                this.spaceY = 3072;
                this.spaceX = -6144;
            } else if (this.iLeft == 0) {
                if (drawer3 == null || drawer4 == null) {
                    a = new Animation("/animation/sand_04");
                    drawer3 = a.getDrawer(0, true, 0);
                    drawer3.setPause(true);
                    drawer4 = a.getDrawer(1, true, 0);
                    drawer4.setPause(true);
                }
                this.spaceY = 4096;
                this.spaceX = 4096;
            }
        }
        this.isTouchSand = false;
        this.isActive = false;
    }

    public void draw(MFGraphics g) {
        if (StageManager.getCurrentZoneId() == 1) {
            drawInMap(g, drawer);
        } else if (StageManager.getCurrentZoneId() == 5) {
            int j = 0;
            int i;
            if (this.iLeft == 1) {
                i = this.collisionRect.y0 + 1024;
                while (i < this.collisionRect.y1 - 1536) {
                    if (i == this.collisionRect.y0 + 1024 && this.iTop == 0) {
                        drawInMap(g, drawer2, (this.collisionRect.x1 + DRAW_OFFSET_X) + j, i);
                    } else {
                        drawInMap(g, drawer, (this.collisionRect.x1 + DRAW_OFFSET_X) + j, i);
                    }
                    i += this.spaceY;
                    j += this.spaceX;
                }
            } else {
                MyAPI.setClip(g, (this.collisionRect.x0 >> 6) - camera.f13x, (this.collisionRect.y0 >> 6) - camera.f14y, this.collisionRect.getWidth() >> 6, this.collisionRect.getHeight() >> 6);
                i = this.collisionRect.y0 + 1024;
                while (i < this.collisionRect.y1) {
                    drawInMap(g, drawer3, this.collisionRect.x0 + j, i);
                    i += this.spaceY;
                    j += this.spaceX;
                }
                MyAPI.setClip(g, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
            }
        }
        drawCollisionRect(g);
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (object.collisionState == (byte) 0) {
            object.setSlip();
            frame++;
            this.isActive = true;
            if (this.iLeft == 0) {
                player.fallinSandSlipState = 1;
            } else if (this.iLeft == 1) {
                player.fallinSandSlipState = 2;
            }
        }
    }

    public void doWhileNoCollision() {
        super.doWhileNoCollision();
        this.isTouchSand = false;
        if (this.isActive) {
            player.fallinSandSlipState = 0;
            this.isActive = false;
        }
    }

    public static void staticLogic() {
        if (drawer != null) {
            drawer.moveOn();
        }
        if (drawer2 != null) {
            drawer2.moveOn();
        }
        if (drawer3 != null) {
            drawer3.moveOn();
        }
        if (drawer4 != null) {
            drawer4.moveOn();
        }
    }

    public static void releaseAllResource() {
        Animation.closeAnimationDrawer(drawer);
        Animation.closeAnimationDrawer(drawer2);
        Animation.closeAnimationDrawer(drawer3);
        Animation.closeAnimationDrawer(drawer4);
        drawer = null;
        drawer2 = null;
        drawer3 = null;
        drawer4 = null;
    }
}
