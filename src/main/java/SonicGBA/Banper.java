package SonicGBA;

import Lib.SoundSystem;

/* compiled from: GimmickObject */
class Banper extends GimmickObject {
    private boolean isActived = false;

    protected Banper(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
    }

    public void doWhileBeAttack(PlayerObject object, int direction, int animationID) {
        super.doWhileBeAttack(object, direction, animationID);
        if ((player instanceof PlayerAmy) && !this.isActived) {
            this.isActived = true;
        }
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if ((player.getCollisionRect().y0 >= this.collisionRect.y0 && player.getCollisionRect().y1 <= this.collisionRect.y1 + 512) || this.iHeight > this.iWidth) {
            player.setCollisionState((byte) 1);
            if ((player instanceof PlayerKnuckles) && ((PlayerKnuckles) player).flying) {
                player.setAnimationId(4);
                ((PlayerKnuckles) player).flying = false;
            } else {
                player.setAnimationId(4);
            }
            int preVelX = player.getVelX();
            int preVelY = player.getVelY();
            PlayerObject playerObject = player;
            int i = (this.iLeft == 2 || this.iLeft == 4) ? -1200 : PlayerObject.SONIC_ATTACK_LEVEL_3_V0;
            playerObject.setVelX(i);
            playerObject = player;
            i = (this.iLeft == 1 || this.iLeft == 2) ? -1536 : 1536;
            playerObject.setVelY(i);
            boolean sePlayed = false;
            if (player.getVelX() * preVelX <= 0) {
                player.getCal().stopMoveX();
                SoundSystem.getInstance().playSe(55);
                sePlayed = true;
            }
            if (player.getVelY() * preVelY <= 0) {
                player.getCal().stopMoveY();
                if (!sePlayed) {
                    SoundSystem.getInstance().playSe(55);
                }
            }
            if (!this.isActived) {
                this.isActived = true;
            }
        }
    }

    public void doWhileNoCollision() {
        if (this.isActived) {
            this.isActived = false;
        }
    }
}
