package SonicGBA;

import com.sega.mobile.define.MDPhone;
import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;

/* compiled from: GimmickObject */
class SteamPlatform extends GimmickObject {
    private static final int COLLISION_HEIGHT = 448;
    private static final int COLLISION_WIDTH = 1280;
    private static int endCount;
    private static MFImage image;
    private static boolean moving;
    public static int sPosY;
    private static int velocity;
    private int originalPosY = this.posY;
    private SteamBase sb;

    protected SteamPlatform(int x, int y, SteamBase sb) {
        super(0, x, y, 0, 0, 0, 0);
        if (image == null) {
            try {
                image = MFImage.createImage("/gimmick/steam_platform.png");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        this.sb = sb;
    }

    public static void staticLogic() {
        if (moving) {
            velocity += GRAVITY;
            sPosY += velocity;
            if (sPosY >= 0) {
                sPosY = 0;
                endCount--;
                if (endCount <= 0) {
                    moving = false;
                } else {
                    velocity = (-velocity) / 3;
                }
            }
        }
    }

    public static void shot() {
        moving = true;
        velocity = -1450;
        endCount = 3;
    }

    public static boolean isShotting() {
        return moving && endCount == 3;
    }

    public void logic() {
        int preY = this.posY;
        this.posY = this.originalPosY + sPosY;
        checkWithPlayer(this.posX, preY, this.posX, this.posY);
        this.sb.sh.refreshCollisionRect(this.posX, this.posY);
    }

    public void drawPlatform(MFGraphics g) {
        drawInMap(g, image, 33);
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - MDPhone.SCREEN_HEIGHT, y - COLLISION_HEIGHT, COLLISION_WIDTH, COLLISION_HEIGHT);
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (direction == 1 || direction == 4) {
            object.beStop(0, 1, this);
        }
    }

    public void close() {
        this.sb = null;
    }

    public static void releaseAllResource() {
        image = null;
    }

    public boolean collisionChkWithObject(PlayerObject object) {
        CollisionRect objectRect = object.getCollisionRect();
        CollisionRect thisRect = getCollisionRect();
        rectV.setRect(objectRect.x0 + 192, objectRect.y0, objectRect.getWidth() - PlayerSonic.BACK_JUMP_SPEED_X, objectRect.getHeight());
        return thisRect.collisionChk(rectV);
    }
}
