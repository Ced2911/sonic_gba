package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import Lib.SoundSystem;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: GimmickObject */
class TransPoint extends GimmickObject {
    private static final int COLLISION_HEIGHT = 1024;
    private static final int COLLISION_WIDTH = 1024;
    public static Animation caveAnimation;
    private int desX;
    private int desY;
    private AnimationDrawer drawer;

    protected TransPoint(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        this.desX = ((this.iLeft * 32) * 512) + (width << 6);
        this.desY = ((this.iTop * 32) * 512) + (height << 6);
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 512, y - 512, 1024, 1024);
    }

    public void draw(MFGraphics g) {
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        object.beTrans(this.desX, this.desY);
        object.pipeOut();
        SoundSystem.getInstance().playSe(37);
        player.setVelX(0);
        player.setVelY(0);
    }

    public int getPaintLayer() {
        return 0;
    }
}
