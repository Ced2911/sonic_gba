package SonicGBA;

import GameEngine.Def;

public interface SonicDef extends Def {
    public static final boolean ALPHA_CHANGE_STATE = true;
    public static final String ANIMATION_PATH = "/animation";
    public static final int DIRECTION_DOWN = 1;
    public static final int DIRECTION_LEFT = 2;
    public static final int DIRECTION_NONE = 4;
    public static final int DIRECTION_RIGHT = 3;
    public static final int DIRECTION_UP = 0;
    public static final int MENU_SPACE = 20;
    public static final int NO_WATER = -1;
    public static final int OVER_TIME = 599999;
    public static final int PI = 201;
    public static final String STAGE_PATH = "/map";
    public static final int TILE_HEIGHT = 8;
    public static final int TILE_WIDTH = 8;
    public static final int TILE_WIDTH_ZOOM = 512;
    public static final int TILE_ZOOM = 3;
    public static final String TITLE_PATH = "/title";
    public static final String TOUCH_ANI_PACH = "/tuch";
    public static final String UTL_PATH = "/utl";
    public static final String UTL_RES = "/animation/utl_res";
    public static final int ZOOM = 6;
}
