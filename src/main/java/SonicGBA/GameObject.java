package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import Lib.Coordinate;
import Lib.MyAPI;
import Lib.SoundSystem;
import com.sega.engine.action.ACBlock;
import com.sega.engine.action.ACObject;
import com.sega.mobile.framework.device.MFDevice;
import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;
import java.io.DataInputStream;
import java.lang.reflect.Array;
import java.util.Vector;

public abstract class GameObject extends ACObject implements SonicDef {
    private static final int AVAILABLE_RANGE = 1;
    public static final int CHECK_OFFSET = 192;
    private static final int CLOSE_NUM_IN_ONE_LOOP = 10;
    private static final int DESTORY_RANGE = 2;
    public static final int DIRECTION_DOWN = 1;
    public static final int DIRECTION_LEFT = 2;
    public static final int DIRECTION_NONE = 4;
    public static final int DIRECTION_RIGHT = 3;
    public static final int DIRECTION_UP = 0;
    public static final int DRAW_AFTER_MAP = 2;
    public static final int DRAW_AFTER_SONIC = 1;
    public static final int DRAW_BEFORE_BEFORE_SONIC = 3;
    public static final int DRAW_BEFORE_SONIC = 0;
    protected static int GRAVITY = 172;
    public static final int INIT_DISTANCE = 14720;
    public static boolean IsGamePause = false;
    public static final int LOAD_CONTENT = 1;
    public static final int LOAD_END = 2;
    public static final int LOAD_INDEX_ENEMY = 2;
    public static final int LOAD_INDEX_GIMMICK = 0;
    public static final int LOAD_INDEX_ITEM = 3;
    public static final int LOAD_INDEX_RING = 1;
    public static final int LOAD_NUM_IN_ONE_LOOP = 20;
    public static final int LOAD_OPEN_FILE = 0;
    private static final int PAINT_LAYER_NUM = 4;
    public static final int REACTION_ATTACK = 1;
    public static final int REACTION_STOP = 0;
    public static final int ROOM_HEIGHT = 256;
    public static final int ROOM_WIDTH = 256;
    private static final int SEARCH_COUNT = 3;
    private static final int SEARCH_RANGE = 10;
    public static final int STATE_NORMAL_MODE = 0;
    public static final int STATE_RACE_MODE = 1;
    public static final int VELOCITY_DIVIDE = 512;
    public static Vector[][] allGameObject;
    public static boolean bossFighting = false;
    public static int bossID;
    public static Coordinate camera;
    private static int closeStep = 0;
    public static int currentLoadIndex;
    private static int currentX;
    private static int currentY;
    private static int cursorX;
    private static int cursorY;
    public static Animation destroyEffectAnimation = null;
    public static DataInputStream ds = null;
    private static int endX;
    private static int endY;
    private static boolean gettingObject;
    private static ACBlock groundblock = CollisionMap.getInstance().getNewCollisionBlock();
    public static Animation iceBreakAnimation = null;
    public static boolean isBossHalf;
    public static boolean isDamageSandActive;
    public static boolean isFirstTouchedSandSlip = false;
    public static boolean isFirstTouchedWind = false;
    public static boolean isGotRings = false;
    public static boolean isUnlockCage;
    public static int loadNum;
    public static int loadStep = 0;
    private static int objVecHeight;
    private static int objVecWidth;
    private static int objectCursor;
    public static Animation platformBreakAnimation = null;
    public static PlayerObject player;
    private static int preCenterX = -1;
    private static int preCenterY = -1;
    public static AnimationDrawer ringDrawer;
    protected static Animation rockBreakAnimation;
    protected static SoundSystem soundInstance = null;
    public static int stageModeState;
    private static int startX;
    private static int startY;
    public static long systemClock;
    protected int currentLayer;
    protected boolean firstTouch = true;
    protected int mHeight;
    protected int mWidth;
    private boolean needInit;
    protected int objId;


    public static CollisionRect rectH = new CollisionRect();
    public static CollisionRect rectV = new CollisionRect();
    private static CollisionRect resetRect = new CollisionRect();
    public static CollisionRect screenRect = new CollisionRect();
    public CollisionRect collisionRect = new CollisionRect();
    public CollisionRect preCollisionRect = new CollisionRect();
    public Coordinate moveDistance = new Coordinate();
    public static Vector[] paintVec = new Vector[4];
    public static Vector mainObjectLogicVec = new Vector();
    public static Vector playerCheckVec = new Vector();
    public static Vector bossObjVec = new Vector();

/*
    public static CollisionRect rectH = null;
    public static CollisionRect rectV =null;
    private static CollisionRect resetRect = null;
    public static CollisionRect screenRect = null;
    public CollisionRect collisionRect = null;
    public CollisionRect preCollisionRect = null;
    public Coordinate moveDistance =null;
    public static Vector[] paintVec =null;
    public static Vector mainObjectLogicVec = null;
    public static Vector playerCheckVec = null;

    public static Vector bossObjVec = null;
    */

    public abstract void close();

    public abstract void doWhileCollision(PlayerObject playerObject, int i);

    public abstract void draw(MFGraphics mFGraphics);

    public abstract void logic();

    public abstract void refreshCollisionRect(int i, int i2);

    public GameObject() {
        super(CollisionMap.getInstance());
    }

    public int getObjectID() {
        return this.objId;
    }

    static {
        for (int i = 0; i < PAINT_LAYER_NUM; i++) {
            paintVec[i] = new Vector();
        }
    }

    public static void initObject(int mapPixelWidth, int mapPixelHeight, boolean sameStage) {
        closeObject(sameStage);
        player = PlayerObject.getPlayer();
        bossObjVec.removeAllElements();
        allGameObject = null;
        objVecWidth = ((mapPixelWidth + 256) - 1) / 256;
        objVecHeight = ((mapPixelHeight + 256) - 1) / 256;
        allGameObject = (Vector[][]) Array.newInstance(Vector.class, new int[]{objVecWidth, objVecHeight});
        for (int x = 0; x < objVecWidth; x++) {
            for (int y = 0; y < objVecHeight; y++) {
                allGameObject[x][y] = new Vector();
            }
        }
        if (destroyEffectAnimation == null) {
            destroyEffectAnimation = new Animation("/animation/destroy_effect");
        }
        if (rockBreakAnimation == null) {
            rockBreakAnimation = new Animation("/animation/iwa_patch");
        }
        if (iceBreakAnimation == null) {
            iceBreakAnimation = new Animation("/animation/ice_patch");
        }
        if (platformBreakAnimation == null) {
            platformBreakAnimation = new Animation("/animation/subehahen_5");
        }
        preCenterX = -1;
        preCenterY = -1;
        IsGamePause = false;
        if (soundInstance == null) {
            soundInstance = SoundSystem.getInstance();
        }
        RingObject.ringInit();
        GimmickObject.gimmickInit();
        EnemyObject.enemyinit();
        bossFighting = false;
    }

    public static void ObjectClear() {
        GimmickObject.gimmickInit();
        bossObjVec.removeAllElements();
    }

    public static void logicObjects() {
        if (!IsGamePause) {
            GameObject currentObj;
            if (systemClock < Long.MAX_VALUE) {
                systemClock++;
            } else {
                systemClock = 0;
            }
            for (int i = 0; i < 4; i++) {
                paintVec[i].removeAllElements();
            }
            GimmickObject.gimmickStaticLogic();
            EnemyObject.enemyStaticLogic();
            RingObject.ringLogic();
            RocketSeparateEffect.getInstance().logic();
            player.logic();
            if (!player.outOfControl) {
                MapManager.cameraLogic();
            }
            checkObjWhileMoving(player);
            int objIndex = 0;
            int vecIndex = 0;
            getGameObjectVecArray(player, mainObjectLogicVec);
            while (vecIndex < mainObjectLogicVec.size()) {
                Vector currentVec = (Vector) mainObjectLogicVec.elementAt(vecIndex);
                if (objIndex < currentVec.size()) {
                    currentObj = (GameObject) currentVec.elementAt(objIndex);
                    if (!player.isControlObject(currentObj)) {
                        currentObj.logic();
                        if (currentObj.objectChkDestroy()) {
                            currentObj.close();
                            currentVec.removeElementAt(objIndex);
                            objIndex--;
                        } else if (currentObj.checkInit()) {
                            currentVec.removeElementAt(objIndex);
                            objIndex--;
                        }
                    }
                    if (checkPaintNecessary(currentObj)) {
                        paintVec[currentObj.getPaintLayer()].addElement(currentObj);
                    }
                    objIndex++;
                } else {
                    objIndex = 0;
                    vecIndex++;
                }
            }
            if (bossObjVec != null) {
                for (objIndex = 0; objIndex < bossObjVec.size(); objIndex++) {
                    currentObj = (GameObject) bossObjVec.elementAt(objIndex);
                    currentObj.logic();
                    if (!currentObj.isFarAwayCamera()) {
                        paintVec[currentObj.getPaintLayer()].addElement(currentObj);
                    }
                }
            }
            BulletObject.bulletLogicAll();
            SmallAnimal.animalLogic();
            if (player.outOfControl) {
                MapManager.cameraLogic();
            }
        }
    }

    public static void setNoInput() {
        if (player != null) {
            player.setNoKey();
        }
    }

    public static void drawPlayer(MFGraphics g) {
        camera = MapManager.getCamera();
        player.draw(g);
    }

    public static void drawObjectBeforeSonic(MFGraphics g) {
        int i;
        camera = MapManager.getCamera();
        for (i = 0; i < paintVec[3].size(); i++) {
            ((GameObject) paintVec[3].elementAt(i)).draw(g);
        }
        for (i = 0; i < paintVec[0].size(); i++) {
            ((GameObject) paintVec[0].elementAt(i)).draw(g);
        }
        if (!isUnlockCage) {
            SmallAnimal.animalDraw(g);
        }
    }

    public static void drawObjectAfterEveryThing(MFGraphics g) {
        camera = MapManager.getCamera();
        for (int i = 0; i < paintVec[2].size(); i++) {
            ((GameObject) paintVec[2].elementAt(i)).draw(g);
        }
        RingObject.ringDraw(g);
    }

    public static void drawObjects(MFGraphics g) {
        if (!(ringDrawer == null || IsGamePause)) {
            ringDrawer.moveOn();
        }
        camera = MapManager.getCamera();
        for (int i = 0; i < paintVec[1].size(); i++) {
            ((GameObject) paintVec[1].elementAt(i)).draw(g);
        }
        if (isUnlockCage) {
            SmallAnimal.animalDraw(g);
        }
    }

    public static boolean loadObjectStep(String fileName, int loadId) {
        boolean nextStep = true;
        switch (loadStep) {
            case LOAD_OPEN_FILE:
                try {
                    ds = new DataInputStream(MFDevice.getResourceAsStream(fileName));
                    loadNum = (short)ds.readShort();
                    if (loadNum < 0) {
                        System.err.println("loadObjectStep LOAD_OPEN_FILE Error ???");
                    }
                    currentLoadIndex = 0;
                } catch (Exception e) {
                    e.printStackTrace();
                    ds = null;
                }
                break;
            case LOAD_CONTENT:
                try {
                    if (ds != null) {
                        int i = 0;
                        while (i < LOAD_NUM_IN_ONE_LOOP && currentLoadIndex < loadNum) {
                            switch (loadId) {
                                case LOAD_INDEX_GIMMICK:
                                    loadGimmickByStream(ds);
                                    break;
                                case LOAD_INDEX_RING:
                                    loadRingByStream(ds);
                                    break;
                                case LOAD_INDEX_ENEMY:
                                    loadEnemyByStream(ds);
                                    break;
                                case LOAD_INDEX_ITEM:
                                    loadItemByStream(ds);
                                    break;
                                default:
                                    break;
                            }
                            i++;
                            currentLoadIndex++;
                        }
                        if (currentLoadIndex < loadNum) {
                            nextStep = false;
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case LOAD_END:
                try {
                    if (ds != null) {
                        ds.close();
                    }
                    loadStep = 0;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return true;
        }
        if (nextStep) {
            loadStep++;
        }
        return false;
    }

    public static void loadGimmickByStream(DataInputStream ds) {
        try {
            int x = ds.readShort()& 0xFFFF;
            int y = ds.readShort()& 0xFFFF;
            int id = ds.readByte()& 0xFF;
            int width = ds.readByte()& 0xFF;
            int height = ds.readByte()& 0xFF;
            int left = ds.readByte()& 0xFF;
            int top = ds.readByte()& 0xFF;
            if (x < 0) {
                x += 256;
            }
            if (y < 0) {
                y += 256;
            }
            if (width < 0) {
                width += 256;
            }
            if (height < 0) {
                height += 256;
            }
            GameObject gimmick = GimmickObject.getNewInstance(id, x, y, left, top, width, height);
            if (gimmick != null) {
                addGameObject(gimmick);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void loadRingByStream(DataInputStream ds) {
        try {
            addGameObject(RingObject.getNewInstance(ds.readShort(), ds.readShort()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void loadEnemyByStream(DataInputStream ds) {
        try {
            int x = ds.readShort() & 0xFFFF;
            int y = ds.readShort() & 0xFFFF;
            int id = ds.readByte() & 0xFF;
            int width = ds.readByte()& 0xFF;
            int height = ds.readByte()& 0xFF;
            int left = ds.readByte()& 0xFF;
            int top = ds.readByte()& 0xFF;
            if (x < 0) {
                x += 256;
            }
            if (y < 0) {
                y += 256;
            }
            if (width < 0) {
                width += 256;
            }
            if (height < 0) {
                height += 256;
            }
            EnemyObject enemy = EnemyObject.getNewInstance(id, x, y, left, top, width, height);
            if (enemy != null && !EnemyObject.IsBoss) {
                addGameObject(enemy);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void loadItemByStream(DataInputStream ds) {
        try {
            addGameObject(ItemObject.getNewInstance(ds.readByte() & 0xFF, ds.readShort() & 0xFFFF, ds.readShort() & 0xFFFF));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void addGameObject(GameObject object, int x, int y) {
        if (object != null) {
            int roomX = (x >> 6) / 256;
            if (roomX >= objVecWidth) {
                roomX = objVecWidth - 1;
            }
            int roomY = (y >> 6) / 256;
            if (roomY >= objVecHeight) {
                roomY = objVecHeight - 1;
            }
            if (roomX > -1) {
                try {
                    allGameObject[roomX][roomY].addElement(object);
                } catch (Exception e) {
                    e.printStackTrace();
                    return;
                }
            }
            object.refreshCollisionRect(object.posX, object.posY);
        }
    }

    public static void addGameObject(GameObject object) {
        addGameObject(object, object.posX, object.posY);
    }

    public static void collisionChkWithAllGameObject(PlayerObject object) {
        GameObject currentObject;
        int i;
        RingObject ring;
        boolean attackFlag = false;
        if (object.attackRectVec.size() > 0) {
            attackFlag = true;
        }
        int objIndex = 0;
        int vecIndex = 0;
        getGameObjectVecArray(player, mainObjectLogicVec);
        while (vecIndex < mainObjectLogicVec.size()) {
            Vector currentVec = (Vector) mainObjectLogicVec.elementAt(vecIndex);
            if (objIndex < currentVec.size()) {
                currentObject = (GameObject) currentVec.elementAt(objIndex);
                if (attackFlag) {
                    for (i = 0; i < object.attackRectVec.size(); i++) {
                        ((PlayerAnimationCollisionRect) object.attackRectVec.elementAt(i)).collisionChkWithObject(currentObject);
                    }
                }
                if (object.isAttracting() && (currentObject instanceof RingObject)) {
                    ring = (RingObject) currentObject;
                    if (object.attractRect.collisionChk(ring.getCollisionRect())) {
                        ring.beAttract();
                    }
                }
                if (currentObject.collisionChkWithObject(object)) {
                    currentObject.doWhileCollisionWrap(object);
                    currentObject.firstTouch = false;
                } else {
                    currentObject.doWhileNoCollision();
                    currentObject.firstTouch = true;
                }
                objIndex++;
            } else {
                objIndex = 0;
                vecIndex++;
            }
        }
        if (bossObjVec != null) {
            for (objIndex = 0; objIndex < bossObjVec.size(); objIndex++) {
                currentObject = (GameObject) bossObjVec.elementAt(objIndex);
                if (attackFlag) {
                    for (i = 0; i < object.attackRectVec.size(); i++) {
                        ((PlayerAnimationCollisionRect) object.attackRectVec.elementAt(i)).collisionChkWithObject(currentObject);
                    }
                }
                if (object.isAttracting() && (currentObject instanceof RingObject)) {
                    ring = (RingObject) currentObject;
                    if (object.attractRect.collisionChk(ring.getCollisionRect())) {
                        ring.beAttract();
                    }
                }
                if (currentObject.collisionChkWithObject(object)) {
                    currentObject.doWhileCollisionWrap(object);
                    currentObject.firstTouch = false;
                } else {
                    currentObject.doWhileNoCollision();
                    currentObject.firstTouch = true;
                }
            }
        }
        BulletObject.checkWithAllBullet(object);
    }

    public static void closeObject() {
        int i;
        if (player != null) {
            player.close();
        }
        if (allGameObject != null) {
            for (int w = 0; w < objVecWidth; w++) {
                for (int h = 0; h < objVecHeight; h++) {
                    for (i = 0; i < allGameObject[w][h].size(); i++) {
                        ((GameObject) allGameObject[w][h].elementAt(i)).close();
                    }
                    allGameObject[w][h].removeAllElements();
                }
            }
            allGameObject = null;
        }
        for (i = 0; i < 4; i++) {
            paintVec[i].removeAllElements();
        }
        SmallAnimal.animalClose();
        BulletObject.bulletClose();
        System.gc();
    }

    public static void closeObject(boolean sameStage) {
        closeObject();
        if (!sameStage) {
            EnemyObject.releaseAllEnemyResource();
            GimmickObject.releaseGimmickResource();
        }
        System.gc();
    }

    public static boolean closeObjectStep(boolean sameStage) {
        boolean nextStep = true;
        int i;
        switch (closeStep) {
            case 0:
                if (player != null) {
                    player.close();
                }
                currentX = 0;
                currentY = 0;
                break;
            case 1:
                if (allGameObject != null) {
                    nextStep = false;
                    for (int j = 0; j < CLOSE_NUM_IN_ONE_LOOP; j++) {
                        for (i = 0; i < allGameObject[currentX][currentY].size(); i++) {
                            ((GameObject) allGameObject[currentX][currentY].elementAt(i)).close();
                        }
                        allGameObject[currentX][currentY].removeAllElements();
                        currentY++;
                        if (currentY >= objVecHeight) {
                            currentY = 0;
                            currentX++;
                            if (currentX >= objVecWidth) {
                                nextStep = true;
                                allGameObject = null;
                                break;
                            }
                        }
                    }
                    break;
                }
                break;
            case 2:
                for (i = 0; i < 4; i++) {
                    paintVec[i].removeAllElements();
                }
                break;
            case 3:
                SmallAnimal.animalClose();
                BulletObject.bulletClose();
                break;
            case 4:
                if (!sameStage) {
                    EnemyObject.releaseAllEnemyResource();
                    GimmickObject.releaseGimmickResource();
                    break;
                }
                break;
            case 5:
                System.gc();
                break;
            case 6:
                closeStep = 0;
                return true;
        }
        if (nextStep) {
            closeStep++;
        }
        return false;
    }

    public static void quitGameState() {
        closeObject(false);
        ItemObject.closeItem();
        SmallAnimal.releaseAllResource();
        PlayerObject.doWhileQuitGame();
        System.gc();
    }

    public static void setPlayerPosition(int x, int y) {
        if (player != null) {
            PlayerObject playerObject = player;
            int i = x << 6;
            player.footPointX = i;
            playerObject.posX = i;
            playerObject = player;
            i = y << 6;
            player.footPointY = i;
            playerObject.posY = i;
        }
    }

    private static void initGetAvailableObject(GameObject currentObject) {
        objectCursor = 0;
        int centerX = (currentObject.getCheckPositionX() >> 6) / 256;
        int centerY = (currentObject.getCheckPositionY() >> 6) / 256;
        if (centerX >= 0 && centerX < objVecWidth && centerY >= 0 && centerY < objVecHeight) {
            startX = centerX - 1;
            startY = centerY - 1;
            endX = centerX + 1;
            endY = centerY + 1;
            if (startX < 0) {
                startX = 0;
            }
            if (startY < 0) {
                startY = 0;
            }
            if (endX >= objVecWidth) {
                endX = objVecWidth - 1;
            }
            if (endY >= objVecHeight) {
                endY = objVecHeight - 1;
            }
            cursorX = startX;
            cursorY = startY;
            gettingObject = true;
            if (preCenterX == -1 && preCenterY == -1) {
                preCenterX = centerX;
                preCenterY = centerY;
            } else if (!(preCenterX == centerX && preCenterY == centerY)) {
                int i;
                GameObject obj;
                int objBlockX;
                int objBlockY;
                int xOffset = centerX - preCenterX;
                int yOffset = centerY - preCenterY;
                if (xOffset != 0) {
                    xOffset = (xOffset > 0 ? -2 : 2) + centerX;
                    if (xOffset >= 0 && xOffset < objVecWidth) {
                        int yo = -2;
                        while (yo <= 2) {
                            if (centerY + yo >= 0 && centerY + yo < objVecHeight) {
                                i = 0;
                                while (i < allGameObject[xOffset][centerY + yo].size()) {
                                    obj = (GameObject) allGameObject[xOffset][centerY + yo].elementAt(i);
                                    objBlockX = (obj.getCheckPositionX() >> 6) / 256;
                                    objBlockY = (obj.getCheckPositionY() >> 6) / 256;
                                    if (objBlockX >= 0 && objBlockX < objVecWidth && objBlockY >= 0 && objBlockY < objVecHeight && !(objBlockX == xOffset && objBlockY == centerY + yo)) {
                                        allGameObject[xOffset][centerY + yo].removeElementAt(i);
                                        i--;
                                        allGameObject[objBlockX][objBlockY].addElement(obj);
                                    }
                                    i++;
                                }
                            }
                            yo++;
                        }
                    }
                }
                if (yOffset != 0) {
                    xOffset = (yOffset > 0 ? -2 : 2) + centerY;
                    if (xOffset >= 0 && xOffset < objVecHeight) {
                        yOffset = -2;
                        while (yOffset <= 2) {
                            if (centerX + yOffset >= 0 && centerX + yOffset < objVecWidth) {
                                i = 0;
                                while (i < allGameObject[centerX + yOffset][xOffset].size()) {
                                    obj = (GameObject) allGameObject[centerX + yOffset][xOffset].elementAt(i);
                                    objBlockX = (obj.getCheckPositionX() >> 6) / 256;
                                    objBlockY = (obj.getCheckPositionY() >> 6) / 256;
                                    if (objBlockX >= 0 && objBlockX < objVecWidth && objBlockY >= 0 && objBlockY < objVecHeight && !(objBlockX == centerX + yOffset && objBlockY == xOffset)) {
                                        allGameObject[centerX + yOffset][xOffset].removeElementAt(i);
                                        i--;
                                        allGameObject[objBlockX][objBlockY].addElement(obj);
                                    }
                                    i++;
                                }
                            }
                            yOffset++;
                        }
                    }
                }
                preCenterX = centerX;
                preCenterY = centerY;
            }
            nextCursor();
        }
    }

    public static void checkObjWhileMoving(GameObject currentObject) {
        int centerX = (MapManager.getCamera().f13x + (MapManager.CAMERA_WIDTH >> 1)) / 256;
        int centerY = (MapManager.getCamera().f14y + (MapManager.CAMERA_HEIGHT >> 1)) / 256;
        int yo;
        int i;
        if (preCenterX == -1 && preCenterY == -1) {
            int xo = centerX - 1;
            while (xo <= centerX + 1) {
                if (xo >= 0 && xo < objVecWidth) {
                    yo = centerY - 1;
                    while (yo <= centerY + 1) {
                        if (yo >= 0 && yo < objVecHeight) {
                            for (i = 0; i < allGameObject[xo][yo].size(); i++) {
                                ((GameObject) allGameObject[xo][yo].elementAt(i)).doInitWhileInCamera();
                            }
                        }
                        yo++;
                    }
                }
                xo++;
            }
            preCenterX = centerX;
            preCenterY = centerY;
        } else if (preCenterX != centerX || preCenterY != centerY) {
            int i2;
            GameObject obj;
            int objBlockY;
            int xOffset = centerX - preCenterX;
            int yOffset = centerY - preCenterY;
            if (xOffset != 0) {
                i = (xOffset > 0 ? -2 : 2) + centerX;
                if (i >= 0 && i < objVecWidth) {
                    int yo2 = -2;
                    while (yo2 <= 2) {
                        if (centerY + yo2 >= 0 && centerY + yo2 < objVecHeight) {
                            i2 = 0;
                            while (i2 < allGameObject[i][centerY + yo2].size()) {
                                obj = (GameObject) allGameObject[i][centerY + yo2].elementAt(i2);
                                yo = (obj.getCheckPositionX() >> 6) / 256;
                                objBlockY = (obj.getCheckPositionY() >> 6) / 256;
                                if (yo >= 0 && yo < objVecWidth && objBlockY >= 0 && objBlockY < objVecHeight && !(yo == i && objBlockY == centerY + yo2)) {
                                    allGameObject[i][centerY + yo2].removeElementAt(i2);
                                    i2--;
                                    allGameObject[yo][objBlockY].addElement(obj);
                                }
                                i2++;
                            }
                        }
                        yo2++;
                    }
                }
                i2 = centerX + (xOffset > 0 ? 1 : -1);
                if (i2 >= 0 && i2 < objVecWidth) {
                    xOffset = centerY - 1;
                    while (xOffset <= centerY + 1) {
                        if (xOffset >= 0 && xOffset < objVecHeight) {
                            i = 0;
                            while (i < allGameObject[i2][xOffset].size()) {
                                obj = (GameObject) allGameObject[i2][xOffset].elementAt(i);
                                yo = (obj.getCheckPositionX() >> 6) / 256;
                                objBlockY = (obj.getCheckPositionY() >> 6) / 256;
                                if (yo >= 0 && yo < objVecWidth && objBlockY >= 0 && objBlockY < objVecHeight && !(yo == i2 && objBlockY == xOffset)) {
                                    allGameObject[i2][xOffset].removeElementAt(i);
                                    i--;
                                    allGameObject[yo][objBlockY].addElement(obj);
                                }
                                i++;
                            }
                        }
                        xOffset++;
                    }
                }
            }
            if (yOffset != 0) {
                i = (yOffset > 0 ? -2 : 2) + centerY;
                if (i >= 0 && i < objVecHeight) {
                    xOffset = -2;
                    while (xOffset <= 2) {
                        if (centerX + xOffset >= 0 && centerX + xOffset < objVecWidth) {
                            i2 = 0;
                            while (i2 < allGameObject[centerX + xOffset][i].size()) {
                                obj = (GameObject) allGameObject[centerX + xOffset][i].elementAt(i2);
                                yo = (obj.getCheckPositionX() >> 6) / 256;
                                objBlockY = (obj.getCheckPositionY() >> 6) / 256;
                                if (yo >= 0 && yo < objVecWidth && objBlockY >= 0 && objBlockY < objVecHeight && !(yo == centerX + xOffset && objBlockY == i)) {
                                    allGameObject[centerX + xOffset][i].removeElementAt(i2);
                                    i2--;
                                    allGameObject[yo][objBlockY].addElement(obj);
                                }
                                i2++;
                            }
                        }
                        xOffset++;
                    }
                }
                i2 = centerY + (yOffset > 0 ? 1 : -1);
                if (i2 >= 0 && i2 < objVecHeight) {
                    xOffset = centerX - 1;
                    while (xOffset <= centerX + 1) {
                        if (xOffset >= 0 && xOffset < objVecWidth) {
                            i = 0;
                            while (i < allGameObject[xOffset][i2].size()) {
                                obj = (GameObject) allGameObject[xOffset][i2].elementAt(i);
                                yo = (obj.getCheckPositionX() >> 6) / 256;
                                objBlockY = (obj.getCheckPositionY() >> 6) / 256;
                                if (yo >= 0 && yo < objVecWidth && objBlockY >= 0 && objBlockY < objVecHeight && !(yo == xOffset && objBlockY == i2)) {
                                    allGameObject[xOffset][i2].removeElementAt(i);
                                    i--;
                                    allGameObject[yo][objBlockY].addElement(obj);
                                }
                                i++;
                            }
                        }
                        xOffset++;
                    }
                }
            }
            preCenterX = centerX;
            preCenterY = centerY;
        }
    }

    private static void getGameObjectVecArray(GameObject currentObject, Vector objVec) {
        objVec.removeAllElements();
        int centerX = (MapManager.getCamera().f13x + (MapManager.CAMERA_WIDTH >> 1)) / 256;
        int centerY = (MapManager.getCamera().f14y + (MapManager.CAMERA_HEIGHT >> 1)) / 256;
        if (centerX >= 0 && centerX < objVecWidth && centerY >= 0 && centerY < objVecHeight) {
            int startX = centerX - 1;
            int startY = centerY - 1;
            int endX = centerX + 1;
            int endY = centerY + 1;
            if (startX < 0) {
                startX = 0;
            }
            if (startY < 0) {
                startY = 0;
            }
            if (endX >= objVecWidth) {
                endX = objVecWidth - 1;
            }
            if (endY >= objVecHeight) {
                endY = objVecHeight - 1;
            }
            if (allGameObject != null) {
                for (int x = startX; x <= endX; x++) {
                    for (int y = startY; y <= endY; y++) {
                        objVec.addElement(allGameObject[x][y]);
                    }
                }
            }
        }
    }

    private static GameObject getAvailableObject() {
        if (!gettingObject) {
            return null;
        }
        GameObject re = (GameObject) allGameObject[cursorX][cursorY].elementAt(objectCursor);
        objectCursor++;
        nextCursor();
        return re;
    }

    private static void nextCursor() {
        while (gettingObject && objectCursor >= allGameObject[cursorX][cursorY].size()) {
            objectCursor = 0;
            cursorX++;
            if (cursorX > endX) {
                cursorX = startX;
                cursorY++;
                if (cursorY > endY) {
                    gettingObject = false;
                    return;
                }
            }
        }
    }

    public static boolean checkPaintNecessary(GameObject object) {
        return object.isInCamera();
    }

    public static void setNewParam(int[] newParam) {
        PlayerObject.setNewParam(newParam);
        GRAVITY = newParam[10];
    }

    public boolean collisionChkWithObject(PlayerObject object) {
        CollisionRect objectRect = object.getCollisionRect();
        CollisionRect thisRect = getCollisionRect();
        rectH.setRect(objectRect.x0, objectRect.y0 + 192, objectRect.getWidth(), objectRect.getHeight() - PlayerSonic.BACK_JUMP_SPEED_X);
        rectV.setRect(objectRect.x0 + 192, objectRect.y0, objectRect.getWidth() - PlayerSonic.BACK_JUMP_SPEED_X, objectRect.getHeight());
        return thisRect.collisionChk(rectH) || thisRect.collisionChk(rectV);
    }

    public boolean onObjectChk(PlayerObject object) {
        CollisionRect objectRect = object.getCollisionRect();
        CollisionRect thisRect = getCollisionRect();
        rectH.setRect(objectRect.x0, objectRect.y0 + 192, objectRect.getWidth(), objectRect.getHeight() - PlayerSonic.BACK_JUMP_SPEED_X);
        rectV.setRect(objectRect.x0 + 192, objectRect.y0, objectRect.getWidth() - PlayerSonic.BACK_JUMP_SPEED_X, objectRect.getHeight());
        return thisRect.collisionChk(rectV);
    }

    public CollisionRect getCollisionRect() {
        return this.collisionRect;
    }

    public int getCheckPositionX() {
        return this.posX;
    }

    public int getCheckPositionY() {
        return this.posY;
    }

    public Coordinate getMoveDistance() {
        return this.moveDistance;
    }

    public void doWhileCollisionWrap(PlayerObject object) {
        boolean xFirst;
        int direction = 4;
        int moveDistanceX = object.getMoveDistance().f13x;
        int moveDistanceY = object.getMoveDistance().f14y;
        CollisionRect collisionRectCurrent = object.getCollisionRect();
        CollisionRect collisionRectBefore = object.preCollisionRect;
        if (Math.abs(collisionRectCurrent.x0 - collisionRectBefore.x0) >= Math.abs(collisionRectCurrent.y0 - collisionRectBefore.y0)) {
            xFirst = true;
        } else {
            xFirst = false;
        }
        rectH.setRect(collisionRectCurrent.x0, collisionRectCurrent.y0 + 192, collisionRectCurrent.getWidth(), collisionRectCurrent.getHeight() - (192 * 2));
        rectV.setRect(collisionRectCurrent.x0 + 192, collisionRectCurrent.y0, collisionRectCurrent.getWidth() - (192 * 2), collisionRectCurrent.getHeight());
        if (xFirst && rectH.collisionChk(getCollisionRect())) {
            if ((collisionRectCurrent.x1 - collisionRectBefore.x1 > 0 && collisionRectBefore.isLeftOf(this.collisionRect, 192)) || (!rectV.collisionChk(getCollisionRect()) && collisionRectCurrent.x0 < this.collisionRect.x0 && object.getVelX() >= -192)) {
                direction = 3;
            } else if ((collisionRectCurrent.x0 - collisionRectBefore.x0 < 0 && collisionRectBefore.isRightOf(this.collisionRect, 192)) || (!rectV.collisionChk(getCollisionRect()) && collisionRectCurrent.x1 > this.collisionRect.x1 && object.getVelX() <= 192)) {
                direction = 2;
            }
        }
        if (direction == 4 && rectV.collisionChk(getCollisionRect())) {
            if (collisionRectCurrent.y1 - collisionRectBefore.y1 > 0 && collisionRectBefore.isUpOf(this.collisionRect, 192 + 5)) {
                direction = 1;
            } else if (collisionRectCurrent.y0 - collisionRectBefore.y0 < 0 && collisionRectBefore.isDownOf(this.collisionRect, 192)) {
                direction = 0;
            }
        }
        if (direction == 4 && rectH.collisionChk(getCollisionRect())) {
            if ((collisionRectCurrent.x1 - collisionRectBefore.x1 > 0 && collisionRectBefore.isLeftOf(this.collisionRect, 192)) || (!rectV.collisionChk(getCollisionRect()) && collisionRectCurrent.x0 < this.collisionRect.x0 && object.getVelX() >= -192)) {
                direction = 3;
            } else if ((collisionRectCurrent.x0 - collisionRectBefore.x0 < 0 && collisionRectBefore.isRightOf(this.collisionRect, 192)) || (!rectV.collisionChk(getCollisionRect()) && collisionRectCurrent.x1 > this.collisionRect.x1 && object.getVelX() <= 192)) {
                direction = 2;
            }
        }
        if (player.inRailState()) {
            doWhileRail(object, direction);
        } else {
            doWhileCollision(object, direction);
        }
    }

    public void doWhileCollisionWrapWithPlayer() {
        if (player != null && !player.isDead) {
            boolean xFirst;
            int direction = 4;
            int moveDistanceX = this.collisionRect.x0 - this.preCollisionRect.x0;
            if (Math.abs(moveDistanceX) >= Math.abs(this.collisionRect.y0 - this.preCollisionRect.y0)) {
                xFirst = true;
            } else {
                xFirst = false;
            }
            player.refreshCollisionRectWrap();
            rectH.setRect(player.collisionRect.x0, player.collisionRect.y0 + 192, player.collisionRect.getWidth(), player.collisionRect.getHeight() - (192 * 2));
            rectV.setRect(player.collisionRect.x0 + 192, player.collisionRect.y0, player.collisionRect.getWidth() - (192 * 2), player.collisionRect.getHeight());
            if (xFirst && rectH.collisionChk(getCollisionRect())) {
                if (this.collisionRect.x1 > this.preCollisionRect.x1 && this.preCollisionRect.isLeftOf(player.collisionRect, 192)) {
                    direction = 2;
                } else if (this.collisionRect.x0 < this.preCollisionRect.x0 && this.preCollisionRect.isRightOf(player.collisionRect, 192)) {
                    direction = 3;
                }
            }
            if (direction == 4 && rectV.collisionChk(getCollisionRect())) {
                if (this.collisionRect.y1 > this.preCollisionRect.y1 && this.preCollisionRect.isUpOf(player.collisionRect, 192)) {
                    direction = 0;
                } else if (this.collisionRect.y0 < this.preCollisionRect.y0 && this.preCollisionRect.isDownOf(player.collisionRect, 192)) {
                    direction = 1;
                }
            }
            if (direction == 4 && rectH.collisionChk(getCollisionRect())) {
                if (this.collisionRect.x1 > this.preCollisionRect.x1 && this.preCollisionRect.isLeftOf(player.collisionRect, 192)) {
                    direction = 2;
                } else if (this.collisionRect.x0 < this.preCollisionRect.x0 && this.preCollisionRect.isRightOf(player.collisionRect, 192)) {
                    direction = 3;
                }
            }
            if (player.isFootOnObject(this)) {
                player.moveOnObject(player.footPointX + moveDistanceX, player.isAntiGravity ? this.collisionRect.y1 : this.collisionRect.y0);
            } else if (collisionChkWithObject(player)) {
                if (player.railing) {
                    doWhileRail(player, direction);
                } else {
                    doWhileCollision(player, direction);
                }
            }
            this.preCollisionRect.setTwoPosition(this.collisionRect.x0, this.collisionRect.y0, this.collisionRect.x1, this.collisionRect.y1);
        }
    }

    public int getGroundY(int x, int y) {
        int re0 = getGroundY(x, y, 0);
        int re1 = getGroundY(x, y, 1);
        if (re0 == -1000 && re1 == -1000) {
            return y;
        }
        if (re0 == -1000) {
            return re1;
        }
        if (re1 == -1000) {
            return re0;
        }
        if (re0 < re1) {
            return re0;
        }
        return re1;
    }

    public int getGroundY(int x, int y, int layer) {
        for (int i = 0; i < 10; i++) {
            y += this.worldInstance.getTileHeight() * 1;
            int re = this.worldInstance.getWorldY(x, y, layer, 0);
            if (re != -1000) {
                return re;
            }
        }
        return y;
    }

    public void drawInMap(MFGraphics g, AnimationDrawer drawer, int x, int y) {
        drawer.draw(g, (x >> 6) - camera.f13x, (y >> 6) - camera.f14y);
    }

    public void drawInMap(MFGraphics g, AnimationDrawer drawer) {
        drawInMap(g, drawer, this.posX, this.posY);
    }

    public void drawInMap(MFGraphics g, MFImage image, int x, int y, int anchor) {
        MyAPI.drawImage(g, image, (x >> 6) - camera.f13x, (y >> 6) - camera.f14y, anchor);
    }

    public void drawInMap(MFGraphics g, MFImage image, int anchor) {
        drawInMap(g, image, this.posX, this.posY, anchor);
    }

    public void drawInMap(MFGraphics g, MFImage image, int srcX, int srcY, int width, int height, int trans, int x, int y, int anchor) {
        MyAPI.drawRegion(g, image, srcX, srcY, width, height, trans, (x >> 6) - camera.f13x, (y >> 6) - camera.f14y, anchor);
    }

    public void drawInMap(MFGraphics g, MFImage image, int srcX, int srcY, int width, int height, int trans, int anchor) {
        drawInMap(g, image, srcX, srcY, width, height, trans, this.posX, this.posY, anchor);
    }

    public int getBlockLeftSide(int blockX, int blockY) {
        return ((blockX + 0) << 3) << 6;
    }

    public int getBlockRightSide(int blockX, int blockY) {
        return (((blockX + 1) << 3) - 1) << 6;
    }

    public int getBlockUpSide(int blockX, int blockY) {
        return ((blockY + 0) << 3) << 6;
    }

    public int getBlockDownSide(int blockX, int blockY) {
        return (((blockY + 1) << 3) - 1) << 6;
    }

    public void checkWithPlayer(int preX, int preY, int currentX, int currentY) {
        int moveDistanceX = currentX - preX;
        int moveDistanceY = currentY - preY;
        if (moveDistanceX == 0 && moveDistanceY == 0) {
            refreshCollisionRect(currentX, currentY);
            doWhileCollisionWrapWithPlayer();
            return;
        }
        int moveDistance;
        if (Math.abs(moveDistanceX) >= Math.abs(moveDistanceY)) {
            moveDistance = Math.abs(moveDistanceX);
        } else {
            moveDistance = Math.abs(moveDistanceY);
        }
        int preCheckX = preX;
        int preCheckY = preY;
        int i = 0;
        while (i <= moveDistance && i < moveDistance) {
            i += 512;
            if (i >= moveDistance) {
                i = moveDistance;
            }
            int tmpCurrentX = preX + ((moveDistanceX * i) / moveDistance);
            int tmpCurrentY = preY + ((moveDistanceY * i) / moveDistance);
            refreshCollisionRect(tmpCurrentX, tmpCurrentY);
            doWhileCollisionWrapWithPlayer();
            preCheckX = tmpCurrentX;
            preCheckY = tmpCurrentY;
        }
    }

    public boolean isInCamera() {
        return isInCamera(80);
    }

    public boolean isInCameraSmaller() {
        return isInCamera(0);
    }

    public boolean isFarAwayCamera() {
        return !isInCamera(256);
    }

    public boolean isInCamera(int offset) {
        Coordinate camera = MapManager.getCamera();
        screenRect.setRect((camera.f13x + MapManager.CAMERA_OFFSET_X) << 6, (camera.f14y + MapManager.CAMERA_OFFSET_Y) << 6, MapManager.CAMERA_WIDTH << 6, MapManager.CAMERA_HEIGHT << 6);
        CollisionRect collisionRect = screenRect;
        collisionRect.x0 -= offset << 6;
        collisionRect = screenRect;
        collisionRect.x1 += offset << 6;
        collisionRect = screenRect;
        collisionRect.y0 -= offset << 6;
        collisionRect = screenRect;
        collisionRect.y1 += offset << 6;
        return this.collisionRect.collisionChk(screenRect);
    }

    public boolean isInCamera(int width, int height) {
        Coordinate camera = MapManager.getCamera();
        screenRect.setRect((camera.f13x + MapManager.CAMERA_OFFSET_X) << 6, (camera.f14y + MapManager.CAMERA_OFFSET_Y) << 6, MapManager.CAMERA_WIDTH << 6, MapManager.CAMERA_HEIGHT << 6);
        CollisionRect collisionRect = screenRect;
        collisionRect.x0 -= (width >> 1) << 6;
        collisionRect = screenRect;
        collisionRect.x1 += (width >> 1) << 6;
        collisionRect = screenRect;
        collisionRect.y0 -= (height >> 1) << 6;
        collisionRect = screenRect;
        collisionRect.y1 += (height >> 1) << 6;
        return this.collisionRect.collisionChk(screenRect);
    }

    public boolean isInCameraOnlyWidth(int width) {
        isInCamera(width, 0);
        return this.collisionRect.collisionChkWidth(screenRect);
    }

    public boolean isInCameraOnlyHeight(int height) {
        isInCamera(0, height);
        return this.collisionRect.collisionChkWidth(screenRect);
    }

    public boolean isAwayFromCameraInWidth() {
        return !isInCameraOnlyWidth(MapManager.CAMERA_WIDTH >> 1);
    }

    public void doWhileNoCollision() {
    }

    public boolean objectChkDestroy() {
        return false;
    }

    public void doInitWhileInCamera() {
    }

    public boolean canBeInit() {
        return true;
    }

    public int getPaintLayer() {
        return 1;
    }

    public boolean releaseWhileBeHurt() {
        return false;
    }

    public void refreshCollisionRectWrap() {
        refreshCollisionRect(this.posX, this.posY);
        this.preCollisionRect.setTwoPosition(this.collisionRect.x0, this.collisionRect.y0, this.collisionRect.x1, this.collisionRect.y1);
    }

    public void transportTo(int x, int y) {
        this.posX = x;
        this.posY = y;
        refreshCollisionRect(this.posX, this.posY);
        this.preCollisionRect.setTwoPosition(this.collisionRect.x0, this.collisionRect.y0, this.collisionRect.x1, this.collisionRect.y1);
    }

    public void doWhileBeAttack(PlayerObject object, int direction, int animationID) {
    }

    public boolean checkInit() {
        resetRect.setRect(((MapManager.getCamera().f13x + (MapManager.CAMERA_WIDTH >> 1)) << 6) - INIT_DISTANCE, ((MapManager.getCamera().f14y + (MapManager.CAMERA_HEIGHT >> 1)) << 6) - INIT_DISTANCE, 29440, 29440);
        if (this.needInit) {
            if (!resetRect.collisionChk(this.collisionRect) && canBeInit()) {
                doInitWhileInCamera();
                refreshCollisionRect(this.posX, this.posY);
                addGameObject(this);
                this.needInit = false;
                return true;
            }
        } else if (resetRect.collisionChk(this.collisionRect)) {
            this.needInit = true;
        }
        return false;
    }

    public void doWhileRail(PlayerObject object, int direction) {
    }

    public void checkWithMap(int preX, int preY, int posX, int posY) {
        int moveDistanceX = posX - preX;
        int moveDistanceY = posY - preY;
        int moveDistanceX2 = moveDistanceX;
        int moveDistanceY2 = moveDistanceY;
        if (moveDistanceY < 0) {
            boolean isUp = true;
        } else {
            Object obj = null;
        }
        if (moveDistanceX < 0) {
            boolean isLeft = true;
        } else {
            Object obj2 = null;
        }
        int quaNumX = ((Math.abs(moveDistanceX2) + 512) - 1) / 512;
        int quaNumY = ((Math.abs(moveDistanceY2) + 512) - 1) / 512;
        boolean xFirst = quaNumX > quaNumY;
        int startPointX = preX;
        int startPointY = preY;
        if (moveDistanceY2 > 0) {
            int i = 0;
            while (true) {
                int i2;
                if (xFirst) {
                    i2 = quaNumX;
                } else {
                    i2 = quaNumY;
                }
                if (i <= i2) {
                    int positionX;
                    int positionY;
                    if (xFirst) {
                        positionX = startPointX + (((moveDistanceX2 >= 0 ? 1 : -1) * 512) * i);
                        positionY = ((startPointY * quaNumX) + (moveDistanceY2 * i)) / quaNumX;
                        if (moveDistanceX2 > 0) {
                            if (positionX > startPointX + moveDistanceX2) {
                                positionX = startPointX + moveDistanceX2;
                                positionY = startPointY + moveDistanceY2;
                            }
                        } else if (moveDistanceX2 >= 0) {
                            positionX = startPointX;
                        } else if (positionX < startPointX + moveDistanceX2) {
                            positionX = startPointX + moveDistanceX2;
                            positionY = startPointY + moveDistanceY2;
                        }
                    } else {
                        positionY = startPointY + (((moveDistanceY2 >= 0 ? 1 : -1) * 512) * i);
                        positionX = ((startPointX * quaNumY) + (moveDistanceX2 * i)) / quaNumY;
                        if (positionY > startPointY + moveDistanceY2) {
                            positionX = startPointX + moveDistanceX2;
                            positionY = startPointY + moveDistanceY2;
                        }
                    }
                    int checkY = getDownCheckPointY(positionX, positionY);
                    int checkOffset = checkY - positionY;
                    int newPositionY = downSideCollisionChk(positionX, checkY);
                    if (newPositionY >= 0) {
                        this.posY = newPositionY - checkOffset;
                        this.posX = positionX;
                        return;
                    }
                    i++;
                } else {
                    return;
                }
            }
        }
    }

    private int getDownCheckPointY(int x, int y) {
        refreshCollisionRect(x, y);
        return this.collisionRect.y1;
    }

    private int getUpCheckPointY(int x, int y) {
        refreshCollisionRect(x, y);
        return this.collisionRect.y0;
    }

    private int downSideCollisionChk(int bodyPositionX, int bodyPositionY) {
        return -1;
    }

    public int getQuaParam(int x, int divide) {
        if (x > 0) {
            return x / divide;
        }
        return (x - (divide - 1)) / divide;
    }

    public void drawCollisionRect(MFGraphics g) {
        if (SonicDebug.showCollisionRect) {
            g.setColor(16711680);
            g.drawRect((this.collisionRect.x0 >> 6) - camera.f13x, (this.collisionRect.y0 >> 6) - camera.f14y, this.collisionRect.getWidth() >> 6, this.collisionRect.getHeight() >> 6);
            g.drawRect(((this.collisionRect.x0 >> 6) - camera.f13x) + 1, ((this.collisionRect.y0 >> 6) - camera.f14y) + 1, (this.collisionRect.getWidth() >> 6) - 2, (this.collisionRect.getHeight() >> 6) - 2);
        }
    }
}
