package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import Lib.SoundSystem;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: GimmickObject */
class SeabedVolcanoAsynBase extends GimmickObject {
    private static final int COLLISION_HEIGHT = 768;
    private static final int COLLISION_WIDTH = 4096;
    private static final int FIRE_OFFSET_Y = 192;
    private static byte count;
    private AnimationDrawer drawer;
    public SeabedVolcanoAsynHurt sh;
    public SeabedVolcanoAsynPlatform sp;

    protected SeabedVolcanoAsynBase(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        if (firemtAnimation == null) {
            firemtAnimation = new Animation("/animation/firemt");
        }
        if (firemtAnimation != null) {
            this.drawer = firemtAnimation.getDrawer(0, false, 0);
        }
        this.sh = new SeabedVolcanoAsynHurt(this.posX, this.posY, this);
        this.sp = new SeabedVolcanoAsynPlatform(this.posX, this.posY, this);
        GameObject.addGameObject(this.sh, this.posX, this.posY);
        GameObject.addGameObject(this.sp, this.posX, this.posY);
    }

    public static void staticLogic() {
        count = (byte) (count + 1);
        count = (byte) (count % 50);
        if (count == (byte) 25) {
            SeabedVolcanoAsynPlatform.shot();
        }
        SeabedVolcanoAsynPlatform.staticLogic();
    }

    public void draw(MFGraphics g) {
        if (count == (byte) 22) {
            this.drawer.setActionId(1);
            this.drawer.restart();
            SoundSystem soundSystem = soundInstance;
            SoundSystem soundSystem2 = soundInstance;
            soundSystem.playSe(53);
            count = (byte) 23;
        }
        drawInMap(g, this.drawer, this.posX, this.posY + 192);
        this.sp.drawPlatform(g);
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 2048, y - 768, 4096, 768);
    }

    public void doWhileCollision(PlayerObject object, int direction) {
    }

    public void close() {
        this.drawer = null;
        this.sp = null;
        this.sh = null;
    }

    public static void releaseAllResource() {
        Animation.closeAnimation(firemtAnimation);
        firemtAnimation = null;
    }
}
