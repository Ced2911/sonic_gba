package SonicGBA;

import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;

/* compiled from: GimmickObject */
class RailOut extends GimmickObject {
    private static final int COLLISION_HEIGHT = 2560;
    private static final int COLLISION_WIDTH = 2688;
    private static final int IMAGE_HEIGHT = 1536;

    protected RailOut(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        if (RailIn.railInOutImage == null) {
            RailIn.railInOutImage = MFImage.createImage("/gimmick/gimmick_67_68.png");
        }
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 1344, y - 2560, COLLISION_WIDTH, 2560);
    }

    public void doWhileRail(PlayerObject object, int direction) {
        if (!this.used) {
            CollisionRect collisionRect = this.collisionRect;
            int bodyPositionX = player.getBodyPositionX();
            int bodyPositionY = player.getBodyPositionY();
            PlayerObject playerObject = player;
            if (collisionRect.collisionChk(bodyPositionX, bodyPositionY - (1536 >> 1)) || this.collisionRect.collisionChk(player.getBodyPositionX(), player.getBodyPositionY())) {
                player.railOut(this.posX, this.posY);
                this.used = true;
            }
        }
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        switch (direction) {
            case 1:
            case 2:
            case 3:
                object.beStop(0, direction, this);
                return;
            default:
                return;
        }
    }

    public void doWhileNoCollision() {
        this.used = false;
    }

    public int getPaintLayer() {
        return 3;
    }

    public void draw(MFGraphics g) {
        drawInMap(g, RailIn.railInOutImage, this.posX, (this.posY - 2560) + 1536, 17);
        drawCollisionRect(g);
    }
}
