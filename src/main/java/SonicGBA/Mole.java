package SonicGBA;

import Lib.Animation;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: EnemyObject */
class Mole extends EnemyObject {
    private static int COLLISION_HEIGHT = 1600;
    private static int COLLISION_WIDTH = 1024;
    private static final int HEIGHT_OFFSET = 2;
    private static final int STATE_UP = 1;
    private static final int STATE_WAIT = 0;
    private static Animation moleAnimation;
    private int state;
    private int wait_cnt;
    private int wait_cnt_max = 16;

    public static void releaseAllResource() {
        Animation.closeAnimation(moleAnimation);
        moleAnimation = null;
    }

    protected Mole(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y + 2, left, top, width, height);
        this.posX -= this.iLeft * 8;
        this.posY -= this.iTop * 8;
        refreshCollisionRect(this.posX, this.posY);
        if (moleAnimation == null) {
            moleAnimation = new Animation("/animation/mole");
        }
        this.drawer = moleAnimation.getDrawer(0, true, 0);
        this.wait_cnt = 0;
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (this.dead || object != player) {
            return;
        }
        if (player.isAttackingEnemy()) {
            if (this.state == 1) {
                beAttack();
                player.doAttackPose(this, direction);
            }
        } else if (this.state == 1) {
            player.beHurt();
        }
    }

    public void doWhileBeAttack(PlayerObject object, int direction, int animationID) {
        if (!this.dead && object == player && this.state == 1) {
            beAttack();
            player.doAttackPose(this, direction);
        }
    }

    public void logic() {
        if (this.dead) {
            this.drawer.setActionId(0);
            return;
        }
        int preX = this.posX;
        int preY = this.posY;
        switch (this.state) {
            case 0:
                if (this.wait_cnt < this.wait_cnt_max) {
                    this.wait_cnt++;
                } else if (this.posX < player.getCheckPositionX()) {
                    this.drawer.setActionId(1);
                    this.drawer.setTrans(2);
                    this.drawer.setLoop(false);
                    this.state = 1;
                } else {
                    this.drawer.setActionId(1);
                    this.drawer.setTrans(0);
                    this.drawer.setLoop(false);
                    this.state = 1;
                }
                checkWithPlayer(preX, preY, this.posX, this.posY);
                return;
            case 1:
                if (this.drawer.checkEnd()) {
                    this.drawer.setActionId(0);
                    this.drawer.setLoop(true);
                    this.state = 0;
                    this.wait_cnt = 0;
                }
                checkWithPlayer(preX, preY, this.posX, this.posY);
                return;
            default:
                return;
        }
    }

    public void draw(MFGraphics g) {
        drawInMap(g, this.drawer);
        drawCollisionRect(g);
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - (COLLISION_WIDTH >> 1), y - COLLISION_HEIGHT, COLLISION_WIDTH, COLLISION_HEIGHT);
    }
}
