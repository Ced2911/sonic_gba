package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;

/* compiled from: GimmickObject */
class LightFont extends GimmickObject {
    private static final int CLOSE_FRAME = 72;
    private static final int COLLISION_HEIGHT = 1792;
    private static final int COLLISION_WIDTH = 1408;
    private static final int FONT_A = 7;
    private static final int FONT_C = 4;
    private static final int FONT_E = 5;
    private static final int FONT_G = 6;
    private static final int FONT_I = 3;
    private static final int FONT_N = 2;
    private static final int FONT_NUM = 8;
    private static final int FONT_O = 1;
    private static final int FONT_S = 0;
    private static final int OPENING_FRAME = 50;
    private static Animation[] lightFontAnimation;
    public AnimationDrawer drawer;
    private int logicDelay;

    protected LightFont(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        if (lightFontAnimation == null) {
            lightFontAnimation = new Animation[8];
        }
        if (lightFontAnimation[left] == null) {
            try {
                lightFontAnimation[left] = new Animation(MFImage.createImage("/animation/light_font_" + left + ".png"), "/animation/light_font");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        this.drawer = lightFontAnimation[left].getDrawer(1, false, 0);
        this.drawer.setPause(true);
        this.logicDelay = width;
    }

    public void logic() {
        this.drawer.moveOn();
        if (this.drawer.checkEnd()) {
            if (this.drawer.getActionId() == 0) {
                this.drawer.setActionId(1);
            } else if (this.drawer.getActionId() == 2) {
                this.drawer.setActionId(3);
            }
        }
        long timetemp = (((systemClock + 50) + 72) - ((long) this.logicDelay)) % 122;
        if ((((systemClock + 50) + 72) - ((long) this.logicDelay)) % 122 < 72) {
            if (this.drawer.getActionId() == 1) {
                this.drawer.setActionId(2);
            }
        } else if (this.drawer.getActionId() == 3) {
            this.drawer.setActionId(0);
        }
        refreshCollisionRect(this.posX, this.posY);
    }

    public int getPaintLayer() {
        return 0;
    }

    public void draw(MFGraphics g) {
        drawInMap(g, this.drawer);
        drawCollisionRect(g);
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - BarHorbinV.COLLISION_HEIGHT, y - 896, COLLISION_WIDTH, COLLISION_HEIGHT);
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (this.drawer.getActionId() == 3) {
            object.cancelFootObject(this);
        } else {
            object.beStop(this.collisionRect.x0, direction, this);
        }
    }

    public void close() {
        this.drawer = null;
    }

    public static void releaseAllResource() {
        Animation.closeAnimationArray(lightFontAnimation);
        lightFontAnimation = null;
    }
}
