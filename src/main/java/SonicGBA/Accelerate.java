package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: GimmickObject */
class Accelerate extends GimmickObject {
    private static final int ACCELERATE_POWER = 3072;
    private static final int COLLISION_HEIGHT = 640;
    private static final int COLLISION_WIDTH = 1536;
    private static Animation accelerate2Animation;
    private static Animation accelerateAnimation;
    private AnimationDrawer drawer;
    private boolean touching = false;
    private boolean transMirror = false;

    protected Accelerate(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        this.transMirror = this.iTop != 0;
        Animation animation;
        int i;
        if (this.objId == 100) {
            if (accelerate2Animation == null) {
                accelerate2Animation = new Animation("/animation/accelerate2");
            }
            animation = accelerate2Animation;
            if (this.transMirror) {
                i = 2;
            } else {
                i = 0;
            }
            this.drawer = animation.getDrawer(0, true, i);
            this.posY += 512;
        } else if (this.objId == 47) {
            if (accelerateAnimation == null) {
                accelerateAnimation = new Animation("/animation/accelerate");
            }
            this.drawer = accelerateAnimation.getDrawer(0, true, this.transMirror ? 7 : 6);
        } else {
            int i2;
            if (accelerateAnimation == null) {
                accelerateAnimation = new Animation("/animation/accelerate");
            }
            animation = accelerateAnimation;
            i = this.objId - 31;
            if (this.transMirror) {
                i2 = 2;
            } else {
                i2 = 0;
            }
            this.drawer = animation.getDrawer(i, true, i2);
        }
    }

    public void draw(MFGraphics g) {
        drawInMap(g, this.drawer);
        drawCollisionRect(g);
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (this.touching || object != player) {
            return;
        }
        PlayerObject playerObject;
        int i;
        if (this.objId != 47) {
            playerObject = player;
            if (this.transMirror) {
                i = -1;
            } else {
                i = 1;
            }
            if (playerObject.beAccelerate(i * 3072, true, this)) {
                this.touching = true;
                soundInstance.playSe(44);
                return;
            }
            return;
        }
        playerObject = player;
        if (this.transMirror) {
            i = -1;
        } else {
            i = 1;
        }
        if (playerObject.beAccelerate(i * 3072, false, this)) {
            this.touching = true;
            soundInstance.playSe(44);
        }
    }

    public void doWhileNoCollision() {
        this.touching = false;
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 768, y - 640, 1536, 640);
    }

    public void close() {
        this.drawer = null;
    }

    public static void releaseAllResource() {
        Animation.closeAnimation(accelerateAnimation);
        Animation.closeAnimation(accelerate2Animation);
        accelerateAnimation = null;
        accelerate2Animation = null;
    }

    public int getPaintLayer() {
        return 0;
    }
}
