package SonicGBA;

public interface MapBehavior {
    void doWhileToucRoof(int i, int i2);

    void doWhileTouchGround(int i, int i2);

    int getGravity();

    boolean hasDownCollision();

    boolean hasSideCollision();

    boolean hasTopCollision();
}
