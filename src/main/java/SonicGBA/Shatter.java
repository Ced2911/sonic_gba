package SonicGBA;

import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;

/* compiled from: GimmickObject */
class Shatter extends GimmickObject {
    private static final int COLLISION_HEIGHT = 3072;
    private static final int COLLISION_WIDTH = 1792;
    private static final int VELOCITY = 600;
    private static MFImage image;
    private int posYOriginal;
    private boolean trigger;

    protected Shatter(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        this.trigger = false;
        this.trigger = false;
        this.posYOriginal = this.posY - 3072;
        if (image == null) {
            try {
                image = MFImage.createImage("/gimmick/shatter.png");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void draw(MFGraphics g) {
        if (this.trigger) {
            drawInMap(g, image, 17);
        }
        drawCollisionRect(g);
    }

    public void doWhileCollision(PlayerObject player, int direction) {
        if (!this.trigger) {
            this.collisionRect.collisionChk(player.getBodyPositionX(), player.getBodyPositionY());
            this.trigger = true;
            this.posY = this.posYOriginal;
            refreshCollisionRect(this.posX, this.posY);
        } else if (direction == 2 || direction == 3) {
            player.beStop(0, direction, this);
        } else {
            player.beStop(0, 2, this);
        }
    }

    public void doWhileNoCollision() {
        if (this.trigger && !isInCamera()) {
            this.trigger = false;
            this.posY = this.posYOriginal + 3072;
            refreshCollisionRect(this.posX, this.posY);
        }
    }

    public void logic() {
        if (this.trigger) {
            int preY = this.posY;
            this.posY += VELOCITY;
            if (this.posY >= this.posYOriginal + 3072) {
                this.posY = this.posYOriginal + 3072;
            }
            checkWithPlayer(this.posX, preY, this.posX, this.posY);
        }
    }

    public void refreshCollisionRect(int x, int y) {
        if (this.trigger) {
            this.collisionRect.setRect(x - 896, y, COLLISION_WIDTH, 3072);
        } else {
            this.collisionRect.setRect(x + 512, y, this.mWidth, this.mHeight);
        }
    }

    public void close() {
    }

    public static void releaseAllResource() {
        image = null;
    }
}
