package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import Lib.MyAPI;
import Lib.MyRandom;
import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;

/* compiled from: BackGroundManager */
class BackManagerStage3 extends BackGroundManager {
    private static int[] CLIP_SPEED_N = null;
    private static final int[] CLIP_SPEED_N1 = new int[]{427, 32, 16, 8, 4, 2};
    private static final int[] CLIP_SPEED_N2 = new int[]{463, 32, 16, 8, 4, 2};
    private static final String FILE_NAME = "/stage3_bg.png";
    private static final int[][] IMAGE_CUT_N = null;
    private static final int[][] IMAGE_CUT_W= null;
    private static int[][] IMAGE_CUT = IMAGE_CUT_N;
    private static int IMAGE_WIDTH = 256;
    private AnimationDrawer firework;
    private int fireworkx;
    private int fireworky;
    private MFImage[] imageBG;
    private boolean isDrawFireWork;

    /*
    static {
        r0 = new int[6][];
        int[] iArr = new int[4];
        iArr[2] = 300;
        iArr[3] = 128;
        r0[0] = iArr;
        int[] iArr2 = new int[]{128, 256, 12, iArr2};
        iArr2 = new int[]{140, 256, 12, iArr2};
        iArr2 = new int[]{152, 256, 16, iArr2};
        iArr2 = new int[]{168, 256, 24, iArr2};
        iArr = new int[]{192, 256, 64, iArr};
        IMAGE_CUT_N = r0;
        r0 = new int[6][];
        iArr = new int[4];
        iArr[2] = MyAPI.zoomIn(PlayerObject.SONIC_ATTACK_LEVEL_2_V0, true);
        iArr[3] = MyAPI.zoomIn(276, true);
        r0[0] = iArr;
        iArr2 = new int[]{MyAPI.zoomIn(276, true), MyAPI.zoomIn(PlayerObject.SONIC_ATTACK_LEVEL_2_V0, true), MyAPI.zoomIn(24, true), iArr2};
        iArr2 = new int[]{MyAPI.zoomIn(300, true), MyAPI.zoomIn(PlayerObject.SONIC_ATTACK_LEVEL_2_V0, true), MyAPI.zoomIn(24, true), iArr2};
        iArr2 = new int[]{MyAPI.zoomIn(324, true), MyAPI.zoomIn(PlayerObject.SONIC_ATTACK_LEVEL_2_V0, true), MyAPI.zoomIn(32, true), iArr2};
        iArr2 = new int[]{MyAPI.zoomIn(356, true), MyAPI.zoomIn(PlayerObject.SONIC_ATTACK_LEVEL_2_V0, true), MyAPI.zoomIn(48, true), iArr2};
        iArr = new int[]{MyAPI.zoomIn(404, true), MyAPI.zoomIn(PlayerObject.SONIC_ATTACK_LEVEL_2_V0, true), MyAPI.zoomIn(148, true), iArr};
        IMAGE_CUT_W = r0;
    }
    */

    public BackManagerStage3() {
        try {
            this.imageBG = new MFImage[7];
            this.imageBG[0] = MFImage.createImage("/map/stage3_bg/#1.png");
            for (int i = 1; i < this.imageBG.length; i++) {
                this.imageBG[i] = MFImage.createPaletteImage("/map/stage3_bg/#" + (i + 1) + ".pal");
            }
            if (stageId == 4) {
                CLIP_SPEED_N = CLIP_SPEED_N1;
            } else if (stageId == 5) {
                CLIP_SPEED_N = CLIP_SPEED_N2;
            }
            if (this.firework == null) {
                this.firework = new Animation("/map/stage3_bg/st3_firework").getDrawer(0, false, 0);
            }
            this.fireworkx = (IMAGE_CUT[0][0] - ((MapManager.getCamera().f13x / CLIP_SPEED_N[0]) % IMAGE_WIDTH)) + MyRandom.nextInt(0, 316);
            this.fireworky = (IMAGE_CUT[0][1] - (MapManager.getCamera().f14y / 26)) + MyRandom.nextInt(0, 112);
            this.isDrawFireWork = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void close() {
        if (this.imageBG != null) {
            for (int i = 0; i < this.imageBG.length; i++) {
                this.imageBG[i] = null;
            }
        }
        this.imageBG = null;
    }

    public void draw(MFGraphics g) {
        int cameraX = MapManager.getCamera().f13x;
        int cameraY = MapManager.getCamera().f14y;
        int yOffset = -(cameraY / 29);
        if (stageId == 4) {
            yOffset = -(cameraY / 26);
        } else if (stageId == 5) {
            yOffset = -(cameraY / 29);
        }
        int tmpframe = (frame % (this.imageBG.length * 2)) / 2;
        for (int i = 0; i < IMAGE_CUT.length; i++) {
            int xOffset = -((cameraX / CLIP_SPEED_N[i]) % IMAGE_WIDTH);
            for (int j = 0; j < MapManager.CAMERA_WIDTH - xOffset; j += IMAGE_WIDTH) {
                MyAPI.drawImage(g, this.imageBG[tmpframe], IMAGE_CUT[i][0], IMAGE_CUT[i][1], IMAGE_CUT[i][2], IMAGE_CUT[i][3], 0, xOffset + j, IMAGE_CUT[i][1] + (yOffset + 0), 20);
            }
        }
        if (this.isDrawFireWork && this.firework.checkEnd()) {
            this.isDrawFireWork = MyRandom.nextInt(0, 10) > 5;
            if (GameObject.IsGamePause) {
                this.isDrawFireWork = false;
            }
            if (this.isDrawFireWork) {
                this.fireworkx = (IMAGE_CUT[0][0] - ((cameraX / CLIP_SPEED_N[0]) % IMAGE_WIDTH)) + MyRandom.nextInt(0, 316);
                this.fireworky = (IMAGE_CUT[0][1] + yOffset) + MyRandom.nextInt(0, 112);
                this.firework.setActionId(MyRandom.nextInt(0, 10) > 5 ? 0 : 1);
                this.firework.restart();
            }
        } else if (this.isDrawFireWork && !this.firework.checkEnd()) {
            this.firework.draw(g, this.fireworkx, this.fireworky);
        } else if (!this.isDrawFireWork) {
            this.isDrawFireWork = MyRandom.nextInt(0, 10) > 5;
            if (GameObject.IsGamePause) {
                this.isDrawFireWork = false;
            }
        }
    }
}
