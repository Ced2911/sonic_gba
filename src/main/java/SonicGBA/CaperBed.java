package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import Lib.SoundSystem;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: GimmickObject */
class CaperBed extends GimmickObject {
    private static final int COLLISION_HEIGHT = 768;
    private static final int COLLISION_WIDTH = 2944;
    private static final int MAX_POWER = 2948;
    private static final int MIN_POWER = 1082;
    private static Animation caperAnimation;
    private AnimationDrawer drawer;

    protected CaperBed(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        if (caperAnimation == null) {
            if (StageManager.getCurrentZoneId() != 6) {
                caperAnimation = new Animation("/animation/se_toramporin_" + StageManager.getCurrentZoneId());
            } else {
                caperAnimation = new Animation("/animation/se_toramporin_2");
            }
        }
        this.drawer = caperAnimation.getDrawer(0, false, 0);
    }

    public void draw(MFGraphics g) {
        drawInMap(g, this.drawer);
        if (this.drawer.checkEnd()) {
            this.drawer.setActionId(0);
        }
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        int power;
        int refVelY = player.getVelY();
        object.beStop(this.collisionRect.y0, direction, this);
        if (direction == 4 && (object instanceof PlayerKnuckles) && object.myAnimationID == 33) {
            power = (Math.abs(refVelY) * 4) / 3;
            if (power > MAX_POWER) {
                power = MAX_POWER;
            }
            if (power < MIN_POWER) {
                power = MIN_POWER;
            }
            player.beSpring(power, 1);
            this.drawer.setActionId(1);
            player.setAnimationId(14);
            SoundSystem.getInstance().playSe(48);
        }
        switch (direction) {
            case 1:
                if (object == player) {
                    power = (Math.abs(refVelY) * 4) / 3;
                    if (power > MAX_POWER) {
                        power = MAX_POWER;
                    }
                    if (power < MIN_POWER) {
                        power = MIN_POWER;
                    }
                    player.beSpring(power, 1);
                    this.drawer.setActionId(1);
                    player.setAnimationId(14);
                    SoundSystem.getInstance().playSe(48);
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 1472, y - 768, COLLISION_WIDTH, 768);
    }

    public void close() {
        this.drawer = null;
    }

    public static void releaseAllResource() {
        Animation.closeAnimation(caperAnimation);
        caperAnimation = null;
    }
}
