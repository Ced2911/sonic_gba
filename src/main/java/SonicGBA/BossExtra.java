package SonicGBA;

import GameEngine.Def;
import Lib.Animation;
import Lib.AnimationDrawer;
import Lib.Coordinate;
import Lib.MyRandom;
import Lib.SoundSystem;
import PyxEditor.PyxAnimation;
import PyxEditor.PyxAnimation.NodeInfo;
import com.sega.mobile.define.MDPhone;
import com.sega.mobile.framework.android.Graphics;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: EnemyObject */
class BossExtra extends BossObject {
    private static final int ANI_APPEAR_1 = 0;
    private static final int ANI_APPEAR_2 = 8;
    private static final int ANI_ATTACK_1 = 2;
    private static final int ANI_ATTACK_2 = 3;
    private static final int ANI_DEAD = 9;
    private static final int ANI_DEFENCE = 7;
    private static final int ANI_FIRST_DEFENCE = 1;
    private static final int ANI_GROUND = 5;
    private static final int ANI_GROUND_READY = 4;
    private static final int ANI_LASER = 6;
    private static final int[] ANI_SEQUENCE_1 = new int[]{2, 3, 4};
    private static final int APPEAR_VELOCITY_1 = 1920;
    private static final int APPEAR_VELOCITY_2 = -960;
    private static final int APPEAR_X = -3584;
    private static final int APPEAR_Y = 8192;
    private static final String[] COLLISION_BODY_NAME = new String[]{"body", "head", "hand_f", "hand_b", "leg_front", "leg_back"};
    private static final int COLLISION_HEIGHT = 18432;
    private static final int COLLISION_WIDTH = 30720;
    private static final int DEAD_DISTANCE_TO_GROUND = 1200;
    private static final int GROUND_COUNT = 60;
    private static final int LASER_COUNT = 18;
    private static final int LASER_DEGREE_END = 10880;
    private static final int LASER_DEGREE_START = 8320;
    private static final int LASER_DIVIDE_COUNT = 110;
    private static final int LASER_NUM = 4;
    private static final int LASER_VELOCITY_ACCELERATE = 80;
    private static final int PYX_ANI_SPEED = 240;
    private static final int PYX_ANI_SPEED_GROUND = 64;
    private static final int STATE_APPEAR_1 = 1;
    private static final int STATE_APPEAR_2 = 2;
    private static final int STATE_DEAD = 8;
    private static final int STATE_DEFENCE = 7;
    private static final int STATE_FIRST_DEFENCE = 3;
    private static final int STATE_GROUND = 5;
    private static final int STATE_LASER = 6;
    private static final int STATE_NONE = 0;
    private static final int STATE_SEQUENCE_1 = 4;
    private static final int WARNING_COUNT = 48;
    private static CollisionRect bodyRect = new CollisionRect();
    private static int damageframe;
    private static NodeInfo nodeInfo = new NodeInfo();
    private int actionID;
    private int afterLaserCount;
    private Animation[] bossAnimation = new Animation[1];
    private boolean bulletShowing;
    private int count;
    private int damageCount;
    private int distanceToGround;
    private AnimationDrawer headFlashDrawer;
    private boolean isOnLand;
    private boolean isShotting;
    private int laserCount = 0;
    private int laserDegree;
    private int laserDegreeVelocity;
    private int laserHeight;
    private int laserNumCount = 0;
    private int pacmanCount;
    private boolean pacmanFlag;
    private PyxAnimation pyxAnimation;
    private int state = 0;
    private AnimationDrawer warningDrawer;

    protected BossExtra(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        this.bossAnimation[0] = new Animation("/animation/boss_extra");
        this.pyxAnimation = new PyxAnimation("/animation/aaa.pyx", this.bossAnimation);
        this.warningDrawer = Animation.getInstanceFromQi("/animation/utl_res/warning.dat")[0].getDrawer(0, true, 0);
        this.headFlashDrawer = this.bossAnimation[0].getDrawer(15, false, 0);
        this.posX = APPEAR_X;
        this.posY = 8192;
        this.velY = 0;
        this.distanceToGround = 2800;
        this.pyxAnimation.setSpeed(240);
        if (GlobalResource.isEasyMode() && stageModeState == 0) {
            this.HP = 6;
        } else {
            this.HP = 8;
        }
    }

    public void logic() {
        switch (this.state) {
            case 0:
                this.posX = APPEAR_X;
                this.posY = 8192;
                this.isOnLand = false;
                if (PlayerObject.getTimeCount() > 0) {
                    this.count++;
                    if (this.count == 48) {
                        this.state = 1;
                        this.count = 0;
                        this.pyxAnimation.setAction(8);
                        this.velX = APPEAR_VELOCITY_1;
                        this.velY = 0;
                        break;
                    }
                }
                break;
            case 1:
                this.posX += this.velX;
                this.posY += this.velY;
                this.count++;
                if (this.count == 32) {
                    this.pyxAnimation.changeToAction(0, 14);
                    this.velY = 128;
                    this.state = 2;
                    this.count = 0;
                    break;
                }
                break;
            case 2:
                this.posX += this.velX;
                this.posY += this.velY;
                this.count++;
                if (this.count > 14) {
                    this.velX = APPEAR_VELOCITY_2;
                    this.posY = getGroundY(this.posX, this.posY + this.distanceToGround) - this.distanceToGround;
                    if (this.posX < 26368) {
                        this.posX = 26368;
                        this.state = 3;
                        this.pyxAnimation.setAction(1);
                        this.actionID = 0;
                        this.isOnLand = true;
                        this.pacmanFlag = true;
                        this.pacmanCount = 0;
                        break;
                    }
                }
                break;
            case 3:
                if (this.pyxAnimation.chkEnd()) {
                    this.state = 4;
                    this.pyxAnimation.setAction(ANI_SEQUENCE_1[this.actionID]);
                    this.actionID++;
                    break;
                }
                break;
            case 4:
                if (this.pyxAnimation.chkEnd()) {
                    if (this.actionID >= ANI_SEQUENCE_1.length) {
                        this.state = 5;
                        this.pyxAnimation.setAction(5);
                        this.pyxAnimation.setLoop(true);
                        this.count = 0;
                        break;
                    }
                    this.pyxAnimation.setAction(ANI_SEQUENCE_1[this.actionID]);
                    this.actionID++;
                    break;
                }
                break;
            case 5:
                this.count++;
                if (this.count % 15 == 0) {
                    BulletObject.addBullet(24, this.posX + MyRandom.nextInt(GimmickObject.PLATFORM_OFFSET_Y, 256), (this.posY + 3072) + MyRandom.nextInt(Def.TOUCH_HELP_LEFT_X, 128), 0, 0);
                    BulletObject.addBullet(24, this.posX + MyRandom.nextInt(GimmickObject.PLATFORM_OFFSET_Y, 256), (this.posY + 3072) + MyRandom.nextInt(Def.TOUCH_HELP_LEFT_X, 128), 0, 0);
                }
                if (this.count > 60) {
                    this.state = 6;
                    this.pyxAnimation.setAction(6);
                    this.pyxAnimation.setLoop(false);
                    this.count = 0;
                    this.laserNumCount = 0;
                    break;
                }
                break;
            case 6:
                this.pyxAnimation.chkEnd();
                if (this.count % 110 == 0 && this.laserNumCount < 4) {
                    this.laserDegree = LASER_DEGREE_START;
                    this.laserDegreeVelocity = 0;
                    this.laserCount = 0;
                    this.isShotting = true;
                    this.afterLaserCount = 0;
                    this.laserHeight = 1;
                    this.laserNumCount++;
                    this.pyxAnimation.changeAnimation("head", 0, 3);
                }
                if (!this.isShotting && this.laserNumCount == 4) {
                    this.state = 7;
                    this.pyxAnimation.setAction(7);
                }
                this.count++;
                break;
            case 7:
                if (this.pyxAnimation.chkEnd()) {
                    this.state = 4;
                    this.actionID = 0;
                    this.pyxAnimation.setAction(ANI_SEQUENCE_1[this.actionID]);
                    this.actionID++;
                    break;
                }
                break;
            case 8:
                damageframe++;
                damageframe %= 11;
                if (damageframe % 2 == 0) {
                    SoundSystem.getInstance().playSe(35);
                }
                int cameraX = MapManager.getCamera().f13x;
                int cameraY = MapManager.getCamera().f14y;
                if (damageframe % 3 == 0) {
                    GameObject.addGameObject(new Boom(37, (((this.posX >> 6) - 50) + MyRandom.nextInt(0, 100)) << 6, (((this.posY >> 6) - 50) + MyRandom.nextInt(0, 100)) << 6, 0, 0, 0, 0));
                }
                this.distanceToGround -= 120;
                if (this.distanceToGround < 1200) {
                    this.distanceToGround = 1200;
                }
                if (this.distanceToGround == 1200) {
                    this.posX -= MDPhone.SCREEN_HEIGHT;
                }
                if (this.posX < -12800) {
                    StageManager.setStagePass();
                    break;
                }
                break;
        }
        if (this.isOnLand) {
            this.posY = getGroundY(this.posX, this.posY + this.distanceToGround) - this.distanceToGround;
        }
        if (this.isShotting) {
            if (this.laserDegree > LASER_DEGREE_END) {
                this.isShotting = false;
                this.bulletShowing = true;
                this.pyxAnimation.changeAnimation("head", 0, 2);
            } else {
                this.laserDegree += this.laserDegreeVelocity;
                this.laserDegreeVelocity += 80;
            }
        }
        if (this.state != 8) {
            if (this.pacmanFlag) {
                this.pacmanCount++;
                if (this.pacmanCount >= 64) {
                    this.pacmanCount = 0;
                    BulletObject.addBullet(22, this.posX - BarHorbinV.HOBIN_POWER, this.posY - 896, 0, 0);
                }
            }
            if (this.bulletShowing) {
                this.afterLaserCount++;
                if (this.afterLaserCount >= 5 && (this.afterLaserCount - 5) % 4 == 0) {
                    int x = 320 - (((this.afterLaserCount - 5) * 32) / 4);
                    if (x > 0) {
                        BulletObject.addBullet(23, x << 6, this.posY + this.distanceToGround, 0, 0);
                        SoundSystem.getInstance().playSe(81);
                        return;
                    }
                    this.bulletShowing = false;
                }
            }
        }
    }

    public void draw(MFGraphics g) {
        if (PlayerObject.getTimeCount() != 0) {
            Graphics g2;
            Coordinate camera = MapManager.getCamera();
            this.pyxAnimation.drawAction(g, (this.posX >> 6) - camera.f13x, (this.posY >> 6) - camera.f14y);
            switch (this.state) {
                case 0:
                    this.warningDrawer.draw(g, SCREEN_WIDTH >> 1, SCREEN_HEIGHT >> 1);
                    break;
            }
            if (this.isShotting) {
                int laserX = this.pyxAnimation.getNodeXByAnimationNamed("head", -20, -12);
                int laserY = this.pyxAnimation.getNodeYByAnimationNamed("head", -20, -12);
                g2 = (Graphics) g.getSystemGraphics();
                g2.save();
                g2.translate((float) (((this.posX >> 6) + laserX) - camera.f13x), (float) (((this.posY >> 6) + laserY) - camera.f14y));
                g2.rotate((float) (this.laserDegree >> 6));
                g.setColor(MapManager.END_COLOR);
                g.fillRect(0, (-this.laserHeight) >> 1, PlayerObject.BANKING_MIN_SPEED, this.laserHeight);
                this.laserHeight++;
                if (this.laserHeight > 4) {
                    this.laserHeight = 4;
                }
                g2.restore();
            }
            if (!IsGamePause) {
                this.damageCount--;
            }
            if (this.damageCount < 0) {
                this.damageCount = 0;
            }
            if ((this.damageCount / 1) % 2 == 1) {
                this.pyxAnimation.getNodeInfo(nodeInfo, "head");
                if (nodeInfo.hasNode()) {
                    g2 = (Graphics) g.getSystemGraphics();
                    g2.save();
                    g2.translate((float) ((nodeInfo.animationX + (this.posX >> 6)) - camera.f13x), (float) ((nodeInfo.animationY + (this.posY >> 6)) - camera.f14y));
                    g2.rotate((float) nodeInfo.degree);
                    this.headFlashDrawer.draw(g, 0, 0);
                    g2.restore();
                }
            }
        }
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(15360, 0, 15360, COLLISION_HEIGHT);
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (this.state != 8) {
            for (int i = 0; i < COLLISION_BODY_NAME.length; i++) {
                this.pyxAnimation.getNodeInfo(nodeInfo, COLLISION_BODY_NAME[i]);
                if (nodeInfo.hasNode()) {
                    byte[] animationRect = nodeInfo.drawer.getCRect();
                    if (animationRect != null) {
                        bodyRect.setRect(this.posX + ((nodeInfo.animationX + animationRect[0]) << 6), this.posY + ((nodeInfo.animationY + animationRect[1]) << 6), animationRect[2] << 6, animationRect[3] << 6);
                        bodyRect.setRotate(nodeInfo.degree, ((-animationRect[0]) + (nodeInfo.rotateX - nodeInfo.animationX)) << 6, ((-animationRect[1]) + (nodeInfo.rotateY - nodeInfo.animationY)) << 6);
                        if (bodyRect.collisionChk(player.getCollisionRect())) {
                            if (i != 1) {
                                player.beHurt();
                                return;
                            } else if (player.isAttackingEnemy() && this.damageCount == 0) {
                                this.HP--;
                                if (this.HP == 0) {
                                    this.state = 8;
                                    this.pyxAnimation.changeToAction(9, 10);
                                    this.pyxAnimation.setLoop(false);
                                    player.getBossScore();
                                    if (player instanceof PlayerSuperSonic) {
                                        ((PlayerSuperSonic) player).setBossDieFlag(true);
                                    }
                                } else {
                                    this.damageCount = 10;
                                }
                                player.doBossAttackPose(this, 2);
                                return;
                            } else if (this.damageCount == 0) {
                                player.beHurt();
                                return;
                            } else {
                                return;
                            }
                        }
                    } else {
                        continue;
                    }
                }
            }
        }
    }

    public void doWhileBeAttack(PlayerObject object, int direction, int animationID) {
        if (this.state != 8) {
            for (int i = 0; i < COLLISION_BODY_NAME.length; i++) {
                this.pyxAnimation.getNodeInfo(nodeInfo, COLLISION_BODY_NAME[i]);
                if (nodeInfo.hasNode()) {
                    byte[] animationRect = nodeInfo.drawer.getCRect();
                    if (animationRect != null) {
                        bodyRect.setRect(this.posX + ((nodeInfo.animationX + animationRect[0]) << 6), this.posY + ((nodeInfo.animationY + animationRect[1]) << 6), animationRect[2] << 6, animationRect[3] << 6);
                        bodyRect.setRotate(nodeInfo.degree, ((-animationRect[0]) + (nodeInfo.rotateX - nodeInfo.animationX)) << 6, ((-animationRect[1]) + (nodeInfo.rotateY - nodeInfo.animationY)) << 6);
                        if (bodyRect.collisionChk(player.getCollisionRect())) {
                            if (i == 1 && this.damageCount == 0) {
                                this.HP--;
                                if (this.HP == 0) {
                                    this.state = 8;
                                    this.pyxAnimation.changeToAction(9, 10);
                                    this.pyxAnimation.setLoop(false);
                                    player.getBossScore();
                                    if (player instanceof PlayerSuperSonic) {
                                        ((PlayerSuperSonic) player).setBossDieFlag(true);
                                    }
                                } else {
                                    this.damageCount = 10;
                                }
                                player.doBossAttackPose(this, 2);
                                return;
                            }
                            return;
                        }
                    } else {
                        continue;
                    }
                }
            }
        }
    }
}
