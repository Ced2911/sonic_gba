package SonicGBA;

import GameEngine.Key;
import Lib.MyAPI;
import Lib.Record;
import Special.SSDef;
import com.sega.mobile.framework.device.MFGraphics;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;

public class ParamSetting implements SonicDef {
    private static int DRAW_LINE = 10;
    private static final int[] PARAM_RESET = new int[]{PlayerObject.MOVE_POWER, PlayerObject.MOVE_POWER_REVERSE, PlayerObject.MAX_VELOCITY, PlayerObject.MOVE_POWER_REVERSE_BALL, PlayerObject.SPIN_START_SPEED_1, PlayerObject.SPIN_START_SPEED_2, PlayerObject.JUMP_START_VELOCITY, PlayerObject.HURT_POWER_X, PlayerObject.HURT_POWER_Y, PlayerObject.JUMP_RUSH_SPEED_PLUS, PlayerObject.GRAVITY, PlayerObject.JUMP_REVERSE_POWER, PlayerObject.FAKE_GRAVITY_ON_WALK, PlayerObject.FAKE_GRAVITY_ON_BALL};
    private static final String[] PARAM_STR = new String[]{"平地x方向加速度:", "平地x方向反向加速度:", "平地自主跑动最大速度:", "spin x方向反向加速度:", "spin 加速_01 x初速度", "spin 加速_02 x初速度", "大跳速度:", "受伤弹开X:", "受伤弹开Y:", "空中冲刺加速值:", "重力:", "空中横向阻力为速度的XX分之一:", "斜面上伪重力（站立）:", "斜面上伪重力（球）:"};
    private static int cursol;
    private static int drawStart = 0;
    private static int[] paramArray = new int[PARAM_RESET.length];

    public static void init() {
        int i;
        cursol = 0;
        for (i = 0; i < PARAM_RESET.length; i++) {
            paramArray[i] = PARAM_RESET[i];
        }
        byte[] record = Record.loadRecord(Record.PARAM_RECORD);
        if (record != null) {
            DataInputStream ds = new DataInputStream(new ByteArrayInputStream(record));
            i = 0;
            while (i < paramArray.length) {
                try {
                    paramArray[i] = ds.readInt();
                    i++;
                } catch (Exception e) {
                }
            }
        }
        GameObject.setNewParam(paramArray);
    }

    public static boolean logic() {
        int[] iArr = null;
        int i;
        if (Key.press(Key.B_UP)) {
            cursol--;
            cursol += PARAM_STR.length;
            cursol %= PARAM_STR.length;
        }
        if (Key.press(Key.B_DOWN)) {
            cursol++;
            cursol += PARAM_STR.length;
            cursol %= PARAM_STR.length;
        }
        if (Key.repeat(Key.B_LEFT)) {
            iArr = paramArray;
            i = cursol;
            iArr[i] = iArr[i] - 1;
        }
        if (Key.repeat(Key.B_RIGHT)) {
            iArr = paramArray;
            i = cursol;
            iArr[i] = iArr[i] + 1;
        }
        if (Key.press(Key.B_S1)) {
            paramArray[cursol] = PARAM_RESET[cursol];
        }
        if (Key.press(2)) {
        }
        if (!Key.press(524288)) {
            return false;
        }
        setOver();
        return true;
    }

    public static void draw(MFGraphics g) {
        g.setColor(MapManager.END_COLOR);
        MyAPI.fillRect(g, 0, 0, SSDef.PLAYER_MOVE_HEIGHT, 320);
        g.setColor(0);
        drawStart = (cursol - DRAW_LINE) + 1;
        if (drawStart < 0) {
            drawStart = 0;
        }
        int i = drawStart;
        while (i < drawStart + DRAW_LINE && i < PARAM_STR.length) {
            MyAPI.drawString(g, new StringBuilder(String.valueOf(PARAM_STR[i])).append(":").append(paramArray[i]).toString(), 40, (i - drawStart) * 24, 0);
            i++;
        }
        g.setColor(16711680);
        MyAPI.drawRect(g, 20, ((cursol - drawStart) * 24) - 2, 200, 22);
        g.setColor(0);
        MyAPI.drawString(g, "复位", 0, 320, 36);
        MyAPI.drawString(g, "退出", SSDef.PLAYER_MOVE_HEIGHT, 320, 40);
    }

    public static void setOver() {
        GameObject.setNewParam(paramArray);
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        DataOutputStream ds = new DataOutputStream(os);
        int i = 0;
        while (i < paramArray.length) {
            try {
                ds.writeInt(paramArray[i]);
                i++;
            } catch (Exception e) {
                return;
            }
        }
        Record.saveRecord(Record.PARAM_RECORD, os.toByteArray());
    }
}
