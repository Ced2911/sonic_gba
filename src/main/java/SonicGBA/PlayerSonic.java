package SonicGBA;

import GameEngine.Key;
import Lib.Animation;
import Lib.AnimationDrawer;
import Lib.Coordinate;
import State.StringIndex;
import com.sega.engine.lib.MyAPI;
import com.sega.mobile.framework.device.MFGraphics;

public class PlayerSonic extends PlayerObject {
    private static final int AIR_DASH_TIME_COUNT = 5;
    private static final int[] ANIMATION_CONVERT = new int[]{0, 1, 2, 3, 4, 10, 5, 6, 31, 19, 23, -1, 28, 39, 20, -1, -1, 54, -1, -1, -1, 52, 34, 35, 38, 32, 33, 41, 42, 43, 28, 40, 44, 45, 46, 47, 48, 49, 7, 8, 7, 30, 21, 22, 28, 29, 9, 36, 37, 51, 55, 56, 57, 50};

    private static final int ATTACK4_ISINWATER_JUMP_START_V = (-1354 - GRAVITY);
    private static final int ATTACK4_JUMP_START_V = (-1188 - GRAVITY);
    public static final int ATTACK_COUNT_LEVEL_1 = 6;
    public static final int ATTACK_COUNT_LEVEL_2 = 12;
    public static final int BACK_JUMP_SPEED_X = 384;
    private static final int EFFECT_JUMP = 1;
    private static final int EFFECT_NONE = 0;
    private static final int EFFECT_SLIP = 2;
    private static final int LOOP = -1;
    private static final int[] LOOP_INDEX = new int[]{-1, -1, -1, -1, -1, -1, -1, 8, -1, -3, -1, 4, -2, -1, -2, -1, -1, 18, -1, 22, -1, 22, 23, -1, -1, -1, -1, 28, -1, 30, -1, -1, 33, 32, 35, 34, -1, -1, -1, 20, 3, -1, -1, -1, -1, -1, -1, 48, -1, -2, 0, -2, -1, -1, -1, 56, -1, -1, 0};

    private static final int NO_ANIMATION = -1;
    private static final int NO_LOOP = -2;
    private static final int NO_LOOP_DEPAND = -3;
    private static final int SNOW_DIVIDE_COUNT = 70;
    public static final int SONIC_ANI_ATTACK_1 = 13;
    public static final int SONIC_ANI_ATTACK_2 = 14;
    public static final int SONIC_ANI_ATTACK_3 = 15;
    public static final int SONIC_ANI_ATTACK_4 = 16;
    public static final int SONIC_ANI_BANK_1 = 44;
    public static final int SONIC_ANI_BANK_2 = 45;
    public static final int SONIC_ANI_BANK_3 = 46;
    public static final int SONIC_ANI_BAR_MOVE = 42;
    public static final int SONIC_ANI_BAR_STAY = 41;
    public static final int SONIC_ANI_BRAKE = 54;
    public static final int SONIC_ANI_BREATHE = 51;
    public static final int SONIC_ANI_CAUGHT = 57;
    public static final int SONIC_ANI_CELEBRATE_1 = 47;
    public static final int SONIC_ANI_CELEBRATE_2 = 48;
    public static final int SONIC_ANI_CELEBRATE_3 = 49;
    public static final int SONIC_ANI_CLIFF_1 = 36;
    public static final int SONIC_ANI_CLIFF_2 = 37;
    public static final int SONIC_ANI_DEAD_1 = 29;
    public static final int SONIC_ANI_DEAD_2 = 30;
    public static final int SONIC_ANI_ENTER_SP = 53;
    public static final int SONIC_ANI_HURT_1 = 27;
    public static final int SONIC_ANI_HURT_2 = 28;
    public static final int SONIC_ANI_JUMP = 4;
    public static final int SONIC_ANI_JUMP_ATTACK_BODY = 11;
    public static final int SONIC_ANI_JUMP_ATTACK_EFFECT = 12;
    public static final int SONIC_ANI_JUMP_DASH_1 = 17;
    public static final int SONIC_ANI_JUMP_DASH_2 = 18;
    public static final int SONIC_ANI_LOOK_UP_1 = 7;
    public static final int SONIC_ANI_LOOK_UP_2 = 8;
    public static final int SONIC_ANI_POLE_H = 40;
    public static final int SONIC_ANI_POLE_V = 39;
    public static final int SONIC_ANI_PUSH_WALL = 31;
    public static final int SONIC_ANI_RAIL_BODY = 52;
    public static final int SONIC_ANI_ROLL_H_1 = 34;
    public static final int SONIC_ANI_ROLL_H_2 = 35;
    public static final int SONIC_ANI_ROLL_V_1 = 32;
    public static final int SONIC_ANI_ROLL_V_2 = 33;
    public static final int SONIC_ANI_RUN = 3;
    public static final int SONIC_ANI_SLIDE_D0 = 24;
    public static final int SONIC_ANI_SLIDE_D45 = 25;
    public static final int SONIC_ANI_SLIDE_D45_EFFECT = 26;
    public static final int SONIC_ANI_SPIN_1 = 5;
    public static final int SONIC_ANI_SPIN_2 = 6;
    public static final int SONIC_ANI_SPRING_1 = 19;
    public static final int SONIC_ANI_SPRING_2 = 20;
    public static final int SONIC_ANI_SPRING_3 = 21;
    public static final int SONIC_ANI_SPRING_4 = 22;
    public static final int SONIC_ANI_SPRING_5 = 23;
    public static final int SONIC_ANI_SQUAT_1 = 9;
    public static final int SONIC_ANI_SQUAT_2 = 10;
    public static final int SONIC_ANI_STAND = 0;
    public static final int SONIC_ANI_UP_ARM = 38;
    public static final int SONIC_ANI_VS_KNUCKLE = 50;
    public static final int SONIC_ANI_WAITING_1 = 55;
    public static final int SONIC_ANI_WAITING_2 = 56;
    public static final int SONIC_ANI_WALK_1 = 1;
    public static final int SONIC_ANI_WALK_2 = 2;
    public static final int SONIC_ANI_WIND = 43;
    public static final int SONIC_ATTACK_LEVEL_1_V0_IN_WATER = 650;
    public static final int SONIC_ATTACK_LEVEL_2_V0_IN_WATER = 1036;
    public static final int SONIC_ATTACK_LEVEL_3_V0_IN_WATER = 1620;
    private static final int SUPER_SONIC_ANI_CHANGE_1 = 1;
    private static final int SUPER_SONIC_ANI_CHANGE_2 = 2;
    private static final int SUPER_SONIC_ANI_GO = 3;
    private static final int SUPER_SONIC_ANI_LOOK_MOON = 0;
    private static final boolean[] SUPER_SONIC_LOOP = new boolean[]{false, false, true, true};
    private int attackCount;
    private int attackLevel;
    private PlayerAnimationCollisionRect attackRect;
    private AnimationDrawer effectDrawer;
    private int effectID;
    private boolean firstJump = false;
    private boolean isFirstAttack;
    private boolean jumpRollEnable;
    private int leftCount;
    private int rightCount;
    private int superSonicAnimationID;
    private AnimationDrawer superSonicDrawer;

    public PlayerSonic() {
        Animation animation = new Animation("/animation/player/chr_sonic");
        this.drawer = animation.getDrawer();
        this.effectDrawer = animation.getDrawer();
        this.attackRect = new PlayerAnimationCollisionRect(this);
        if (StageManager.getStageID() == 12) {
            this.superSonicDrawer = new Animation("/animation/player/chr_super_sonic").getDrawer();
        }
    }

    public void closeImpl() {
        Animation.closeAnimationDrawer(this.superSonicDrawer);
        this.superSonicDrawer = null;
        Animation.closeAnimationDrawer(this.effectDrawer);
        this.effectDrawer = null;
    }

    public void slipStart() {
        this.currentLayer = 0;
        this.slipping = true;
        this.slideSoundStart = true;
        soundInstance.playSe(8);
        slidingFrame = 0;
        this.collisionState = (byte) 1;
        this.worldCal.actionState = (byte) 1;
        setMinSlipSpeed();
        this.worldCal.setMovedState(true);
    }

    public void slipJumpOut() {
        if (this.slipping) {
            this.currentLayer = 1;
            this.slipping = false;
            calDivideVelocity();
            setVelY(this.isInWater ? JUMP_INWATER_START_VELOCITY : JUMP_START_VELOCITY);
            this.collisionState = (byte) 1;
            this.worldCal.actionState = (byte) 1;
            this.collisionChkBreak = true;
            this.worldCal.stopMove();
            this.worldCal.setMovedState(false);
        }
    }

    public void slipEnd() {
        if (this.slipping) {
            this.currentLayer = 1;
            this.slipping = false;
            calDivideVelocity();
            this.collisionState = (byte) 1;
            this.worldCal.actionState = (byte) 1;
            this.velY = -1540;
            this.animationID = 9;
            this.collisionChkBreak = true;
            this.worldCal.stopMove();
            soundInstance.stopLoopSe();
            soundInstance.playSequenceSe(11);
            this.worldCal.setMovedState(false);
        }
    }

    public void setSlideAni() {
        this.animationID = -1;
        this.myAnimationID = 24;
    }

    protected void extraLogicJump() {
        if (!this.hurtNoControl) {
            if (!this.slipping && Key.press(Key.gLeft)) {
                if (!this.jumpRollEnable) {
                    this.leftCount = 5;
                }
                this.rightCount = 0;
            }
            if (Key.press(Key.gRight)) {
                this.leftCount = 0;
                if (!this.jumpRollEnable) {
                    this.rightCount = 5;
                }
            }
        }
        if (this.animationID == 4 && this.firstJump) {
            if (this.leftCount > 0) {
                this.leftCount--;
                if (!Key.repeat(Key.gLeft)) {
                    this.jumpRollEnable = true;
                }
                if (this.jumpRollEnable && Key.repeat(Key.gLeft)) {
                    this.animationID = -1;
                    this.myAnimationID = 17;
                    this.leftCount = 0;
                    this.velY = 0;
                    this.velX -= this.maxVelocity >> 2;
                    soundInstance.playSe(7);
                    this.firstJump = false;
                }
            }
            if (this.rightCount > 0) {
                this.rightCount--;
                if (!Key.repeat(Key.gRight)) {
                    this.jumpRollEnable = true;
                }
                if (this.jumpRollEnable && Key.repeat(Key.gRight)) {
                    this.animationID = -1;
                    this.myAnimationID = 17;
                    this.rightCount = 0;
                    this.velY = 0;
                    this.velX += this.maxVelocity >> 2;
                    soundInstance.playSe(7);
                    this.firstJump = false;
                }
            }
            if (this.firstJump && Key.press(Key.gUp | 16777216)) {
                this.effectID = 1;
                this.firstJump = false;
                soundInstance.playSe(79);
                this.effectDrawer.restart();
                this.effectDrawer.setActionId(12);
                byte[] rect = this.effectDrawer.getARect();
                if (rect != null) {
                    this.attackRect.initCollision(rect[0] << 6, rect[1] << 6, rect[2] << 6, rect[3] << 6, 12);
                    this.attackRectVec.addElement(this.attackRect);
                }
            }
        }
        if (this.myAnimationID == 14 && this.drawer.checkEnd()) {
            this.animationID = 0;
        }
    }

    private int startSpeedSet(boolean confFlag, int sourceSpeed, int conf) {
        return confFlag ? (sourceSpeed * conf) / 100 : sourceSpeed;
    }

    public boolean isOnSlip0() {
        return this.myAnimationID == 24;
    }

    public void setSlip0() {
        if (this.collisionState == (byte) 0) {
            this.animationID = -1;
            this.myAnimationID = 24;
        }
        setMinSlipSpeed();
    }

    private void setMinSlipSpeed() {
        if (getVelX() < SPEED_LIMIT_LEVEL_1) {
            setVelX(SPEED_LIMIT_LEVEL_1);
        }
    }

    protected void extraLogicWalk() {
        if (this.slipping) {
            if (Key.repeat(Key.gLeft) && this.myAnimationID == 25) {
                this.totalVelocity -= 30;
            } else if (Key.repeat(Key.gDown | Key.gRight) && this.faceDegree < StringIndex.FONT_COLON_RED) {
                this.totalVelocity += (MyAPI.dSin(this.faceDegree) * 150) / 100;
            }
            this.totalVelocity -= 30;
            this.totalVelocity = Math.max(this.totalVelocity, 192);
            this.animationID = -1;
            this.faceDirection = true;
            if (this.faceDegree == 45) {
                this.myAnimationID = 25;
                this.effectID = 2;
            } else {
                setMinSlipSpeed();
                this.myAnimationID = 24;
            }
            slidingFrame++;
            System.out.println("~~slidingFrame:" + slidingFrame);
            if (slidingFrame == 2) {
                soundInstance.playLoopSe(9);
            }
        }
        if (this.attackCount > 0) {
            this.attackCount--;
        }
        if ((this.myAnimationID == 13 || this.myAnimationID == 14 || this.myAnimationID == 15) && this.faceDegree != 90 && this.faceDegree != 270 && this.attackLevel == 0) {
            this.animationID = 0;
            this.myAnimationID = ANIMATION_CONVERT[0];
            if (this.collisionState == (byte) 1) {
                this.animationID = 4;
                this.myAnimationID = ANIMATION_CONVERT[4];
            }
        }
        int v0;
        int startSpeedSet;
        switch (this.myAnimationID) {
            case 13:
                if ((this.attackCount == 0 || getVelX() == 0) && !this.isStopByObject) {
                    this.attackLevel = 0;
                } else {
                    this.myAnimationID = 13;
                }
                if (this.isStopByObject && this.attackCount == 0) {
                    this.attackLevel = 0;
                }
                if (Key.press(Key.gSelect) && !this.isCrashPipe) {
                    this.attackLevel = 2;
                    this.myAnimationID = 14;
                    soundInstance.playSe(17);
                    v0 = this.isInWater ? SONIC_ATTACK_LEVEL_2_V0_IN_WATER : PlayerObject.SONIC_ATTACK_LEVEL_2_V0;
                    if (this.collisionState == (byte) 0) {
                        if (this.faceDirection) {
                            startSpeedSet = startSpeedSet(this.isInSnow, v0, SNOW_DIVIDE_COUNT);
                        } else {
                            startSpeedSet = startSpeedSet(this.isInSnow, -v0, SNOW_DIVIDE_COUNT);
                        }
                        this.totalVelocity = startSpeedSet;
                    } else {
                        if (this.faceDirection) {
                            startSpeedSet = startSpeedSet(this.isInSnow, v0, SNOW_DIVIDE_COUNT);
                        } else {
                            startSpeedSet = startSpeedSet(this.isInSnow, -v0, SNOW_DIVIDE_COUNT);
                        }
                        setVelX(startSpeedSet);
                    }
                    soundInstance.playSe(17);
                    break;
                }
                break;
            case 14:
                if (!this.drawer.checkEnd()) {
                    this.myAnimationID = 14;
                } else if (this.attackLevel == 3 && (getVelX() != 0 || this.isStopByObject)) {
                    this.myAnimationID = 15;
                    v0 = this.isInWater ? SONIC_ATTACK_LEVEL_3_V0_IN_WATER : PlayerObject.SONIC_ATTACK_LEVEL_3_V0;
                    if (this.collisionState == (byte) 0) {
                        if (this.faceDirection) {
                            startSpeedSet = startSpeedSet(this.isInSnow, v0, SNOW_DIVIDE_COUNT);
                        } else {
                            startSpeedSet = startSpeedSet(this.isInSnow, -v0, SNOW_DIVIDE_COUNT);
                        }
                        this.totalVelocity = startSpeedSet;
                    } else {
                        if (this.faceDirection) {
                            startSpeedSet = startSpeedSet(this.isInSnow, v0, SNOW_DIVIDE_COUNT);
                        } else {
                            startSpeedSet = startSpeedSet(this.isInSnow, -v0, SNOW_DIVIDE_COUNT);
                        }
                        setVelX(startSpeedSet);
                    }
                    this.attackCount = 12;
                    soundInstance.playSe(6);
                } else if (this.attackLevel == 4) {
                    int preVelX;
                    if (this.isStopByObject) {
                        preVelX = (this.faceDirection ^ this.isAntiGravity) != false ? BACK_JUMP_SPEED_X : -384;
                    } else {
                        preVelX = getVelX();
                    }
                    if (this.isInSnow) {
                        preVelX >>= 1;
                    }
                    int start_v = this.isInWater ? ATTACK4_ISINWATER_JUMP_START_V : ATTACK4_JUMP_START_V;
                    if (this.isAntiGravity) {
                        startSpeedSet = -start_v;
                    } else {
                        startSpeedSet = start_v;
                    }
                    super.doJumpV(startSpeedSet);
                    this.attackLevel = 0;
                    setVelX(-preVelX);
                    this.animationID = -1;
                    this.myAnimationID = 16;
                    this.noVelMinus = true;
                } else {
                    this.animationID = 0;
                    this.attackLevel = 0;
                    setVelX(0);
                    this.totalVelocity = 0;
                }
                if (this.attackLevel != 0) {
                    if (!Key.press(Key.gSelect)) {
                        if (Key.press(16777216)) {
                            this.attackLevel = 4;
                            break;
                        }
                    }
                    this.attackLevel = 3;
                    break;
                }
                break;
            case 15:
                if (getVelX() != 0 || this.isStopByObject) {
                    this.myAnimationID = 15;
                } else {
                    this.attackLevel = 0;
                }
                if (this.isStopByObject && this.attackCount == 0) {
                    this.attackLevel = 0;
                    break;
                }
                break;
            default:
                if (!(this.animationID == 4 || !Key.press(Key.gSelect) || this.slipping || this.animationID == 8 || this.isCrashFallingSand)) {
                    if (this.onBank) {
                        this.onBank = false;
                    }
                    this.attackLevel = 1;
                    this.attackCount = (this.isInWater ? 2 : 1) * 6;
                    this.animationID = -1;
                    this.myAnimationID = 13;
                    this.isFirstAttack = true;
                    v0 = this.isInWater ? SONIC_ATTACK_LEVEL_1_V0_IN_WATER : PlayerObject.SONIC_ATTACK_LEVEL_1_V0;
                    if (this.collisionState == (byte) 0) {
                        if (this.faceDirection) {
                            startSpeedSet = startSpeedSet(this.isInSnow, v0, SNOW_DIVIDE_COUNT);
                        } else {
                            startSpeedSet = startSpeedSet(this.isInSnow, -v0, SNOW_DIVIDE_COUNT);
                        }
                        this.totalVelocity = startSpeedSet;
                    } else {
                        if (this.faceDirection) {
                            startSpeedSet = startSpeedSet(this.isInSnow, v0, SNOW_DIVIDE_COUNT);
                        } else {
                            startSpeedSet = startSpeedSet(this.isInSnow, -v0, SNOW_DIVIDE_COUNT);
                        }
                        setVelX(startSpeedSet);
                    }
                    this.drawer.setActionId(13);
                    byte[] rect = this.drawer.getARect();
                    if (rect != null) {
                        this.attackRect.initCollision(rect[0] << 6, rect[1] << 6, rect[2] << 6, rect[3] << 6, 13);
                        this.attackRectVec.addElement(this.attackRect);
                        break;
                    }
                }
                break;
        }
        this.isStopByObject = false;
        if (this.attackLevel == 0) {
            this.isAttacking = false;
        } else {
            this.isAttacking = true;
        }
    }

    protected void extraLogicOnObject() {
        this.firstJump = false;
        if (this.attackCount > 0) {
            this.attackCount--;
        }
        if ((this.myAnimationID == 13 || this.myAnimationID == 14 || this.myAnimationID == 15) && this.attackLevel == 0) {
            this.animationID = 0;
            this.myAnimationID = ANIMATION_CONVERT[0];
        }
        int v0;
        int startSpeedSet;
        switch (this.myAnimationID) {
            case 13:
                if ((this.attackCount == 0 || getVelX() == 0) && !this.isStopByObject) {
                    this.attackLevel = 0;
                } else {
                    this.myAnimationID = 13;
                }
                if (this.isStopByObject && this.attackCount == 0) {
                    this.attackLevel = 0;
                }
                if (Key.press(Key.gSelect) && !this.isCrashPipe) {
                    this.attackLevel = 2;
                    this.myAnimationID = 14;
                    soundInstance.playSe(17);
                    v0 = this.isInWater ? SONIC_ATTACK_LEVEL_2_V0_IN_WATER : PlayerObject.SONIC_ATTACK_LEVEL_2_V0;
                    if (this.collisionState == (byte) 0) {
                        if ((this.isAntiGravity ^ this.faceDirection) != false) {
                            startSpeedSet = startSpeedSet(this.isInSnow, v0, SNOW_DIVIDE_COUNT);
                        } else {
                            startSpeedSet = startSpeedSet(this.isInSnow, -v0, SNOW_DIVIDE_COUNT);
                        }
                        this.totalVelocity = startSpeedSet;
                    } else {
                        if ((this.isAntiGravity ^ this.faceDirection) != false) {
                            startSpeedSet = startSpeedSet(this.isInSnow, v0, SNOW_DIVIDE_COUNT);
                        } else {
                            startSpeedSet = startSpeedSet(this.isInSnow, -v0, SNOW_DIVIDE_COUNT);
                        }
                        setVelX(startSpeedSet);
                    }
                    soundInstance.playSe(17);
                    break;
                }
                break;
            case 14:
                if (!this.drawer.checkEnd()) {
                    this.myAnimationID = 14;
                } else if (this.attackLevel < 3 || (getVelX() == 0 && !this.isStopByObject)) {
                    this.animationID = 0;
                    this.attackLevel = 0;
                    setVelX(0);
                    this.totalVelocity = 0;
                } else {
                    this.myAnimationID = 15;
                    v0 = this.isInWater ? SONIC_ATTACK_LEVEL_3_V0_IN_WATER : PlayerObject.SONIC_ATTACK_LEVEL_3_V0;
                    if (this.collisionState == (byte) 0) {
                        if ((this.isAntiGravity ^ this.faceDirection) != false) {
                            startSpeedSet = startSpeedSet(this.isInSnow, v0, SNOW_DIVIDE_COUNT);
                        } else {
                            startSpeedSet = startSpeedSet(this.isInSnow, -v0, SNOW_DIVIDE_COUNT);
                        }
                        this.totalVelocity = startSpeedSet;
                    } else {
                        if ((this.isAntiGravity ^ this.faceDirection) != false) {
                            startSpeedSet = startSpeedSet(this.isInSnow, v0, SNOW_DIVIDE_COUNT);
                        } else {
                            startSpeedSet = startSpeedSet(this.isInSnow, -v0, SNOW_DIVIDE_COUNT);
                        }
                        setVelX(startSpeedSet);
                    }
                    this.attackCount = 12;
                    soundInstance.playSe(6);
                }
                if (Key.press(Key.gSelect)) {
                    this.attackLevel = 3;
                    break;
                }
                break;
            case 15:
                if (getVelX() != 0 || this.isStopByObject) {
                    this.myAnimationID = 15;
                } else {
                    this.attackLevel = 0;
                }
                if (this.isStopByObject && this.attackCount == 0) {
                    this.attackLevel = 0;
                    break;
                }
                break;
            default:
                if (!(this.animationID == 4 || !Key.press(Key.gSelect) || this.animationID == 8 || this.isCrashFallingSand)) {
                    this.attackLevel = 1;
                    this.attackCount = (this.isInWater ? 2 : 1) * 6;
                    this.animationID = -1;
                    this.myAnimationID = 13;
                    this.isFirstAttack = true;
                    v0 = this.isInWater ? SONIC_ATTACK_LEVEL_1_V0_IN_WATER : PlayerObject.SONIC_ATTACK_LEVEL_1_V0;
                    if (this.collisionState == (byte) 0) {
                        if ((this.isAntiGravity ^ this.faceDirection) != false) {
                            startSpeedSet = startSpeedSet(this.isInSnow, v0, SNOW_DIVIDE_COUNT);
                        } else {
                            startSpeedSet = startSpeedSet(this.isInSnow, -v0, SNOW_DIVIDE_COUNT);
                        }
                        this.totalVelocity = startSpeedSet;
                    } else {
                        if ((this.isAntiGravity ^ this.faceDirection) != false) {
                            startSpeedSet = startSpeedSet(this.isInSnow, v0, SNOW_DIVIDE_COUNT);
                        } else {
                            startSpeedSet = startSpeedSet(this.isInSnow, -v0, SNOW_DIVIDE_COUNT);
                        }
                        setVelX(startSpeedSet);
                    }
                    this.drawer.setActionId(13);
                    byte[] rect = this.drawer.getARect();
                    if (rect != null) {
                        this.attackRect.initCollision(rect[0] << 6, rect[1] << 6, rect[2] << 6, rect[3] << 6, 13);
                        this.attackRectVec.addElement(this.attackRect);
                        break;
                    }
                }
                break;
        }
        this.isStopByObject = false;
        if (this.attackLevel == 0) {
            this.isAttacking = false;
        } else {
            this.isAttacking = true;
        }
    }

    public void doJump() {
        if (this.myAnimationID != 13 && this.myAnimationID != 14 && this.myAnimationID != 15) {
            if (this.slipping) {
                if (!isHeadCollision()) {
                    this.currentLayer = 1;
                } else {
                    return;
                }
            }
            if (this.slipping && this.totalVelocity == 192) {
                super.doJumpV();
            } else {
                super.doJump();
            }
            this.leftCount = 0;
            this.rightCount = 0;
            this.jumpRollEnable = false;
            if (this.slipping) {
                this.currentLayer = 1;
                this.slipping = false;
            }
            this.firstJump = true;
            if (this.bankwalking) {
                this.firstJump = false;
            }
        }
    }

    public void doHurt() {
        super.doHurt();
        if (this.slipping) {
            this.currentLayer = 1;
            this.slipping = false;
        }
    }

    public void drawCharacter(MFGraphics g) {
        Coordinate camera = MapManager.getCamera();
        if (isTerminal && terminalType == 3 && terminalState >= (byte) 2) {
            switch (terminalState) {
                case (byte) 2:
                case (byte) 3:
                    this.superSonicAnimationID = 0;
                    break;
                case (byte) 4:
                case (byte) 5:
                    if (!(this.superSonicAnimationID == 1 || this.superSonicAnimationID == 2)) {
                        this.superSonicAnimationID = 1;
                        break;
                    }
                case (byte) 6:
                    this.superSonicAnimationID = 3;
                    break;
            }
            this.superSonicDrawer.setActionId(this.superSonicAnimationID);
            this.superSonicDrawer.setLoop(SUPER_SONIC_LOOP[this.superSonicAnimationID]);
            drawInMap(g, this.superSonicDrawer, this.posX + this.terminalOffset, this.posY);
            if (this.superSonicDrawer.checkEnd()) {
                switch (this.superSonicAnimationID) {
                    case 1:
                        this.superSonicAnimationID = 2;
                        return;
                    default:
                        return;
                }
            }
            return;
        }
        if (this.animationID != -1) {
            this.myAnimationID = ANIMATION_CONVERT[this.animationID];
        }
        if (this.myAnimationID != -1) {
            boolean loop;
            int trans;
            int drawY;
            byte[] rect;
            if (LOOP_INDEX[this.myAnimationID] == -1) {
                loop = true;
            } else {
                loop = false;
            }
            if (this.hurtCount % 2 == 0) {
                int bodyCenterX;
                int bodyCenterY;
                if (this.animationID != 4) {
                    if (this.animationID != 6 && this.animationID != 7) {
                        switch (this.myAnimationID) {
                            case 24:
                                this.drawer.draw(g, this.myAnimationID, ((this.footPointX >> 6) - camera.f13x) + 0, ((this.footPointY >> 6) - camera.f14y) + 0, loop, 0);
                                if (!this.isInWater) {
                                    this.effectDrawer.setSpeed(1, 1);
                                    break;
                                } else {
                                    this.effectDrawer.setSpeed(1, 2);
                                    break;
                                }
                            case 25:
                                this.effectDrawer.draw(g, 26, ((this.footPointX >> 6) - camera.f13x) + 8, ((this.footPointY >> 6) - camera.f14y) + 0, loop, 0);
                                this.drawer.draw(g, this.myAnimationID, ((this.footPointX >> 6) - camera.f13x) + 8, ((this.footPointY >> 6) - camera.f14y) + 0, loop, 0);
                                if (!this.isInWater) {
                                    this.effectDrawer.setSpeed(1, 1);
                                    break;
                                } else {
                                    this.effectDrawer.setSpeed(1, 2);
                                    break;
                                }
                            default:
                                if (this.isInWater) {
                                    this.drawer.setSpeed(1, 2);
                                } else {
                                    this.drawer.setSpeed(1, 1);
                                }
                                if (this.myAnimationID == 36 || this.myAnimationID == 37 || this.myAnimationID == 7 || this.myAnimationID == 8) {
                                    this.degreeForDraw = this.degreeStable;
                                    this.faceDegree = this.degreeStable;
                                }
                                if (this.myAnimationID == 54) {
                                    this.degreeForDraw = this.degreeStable;
                                }
                                if (!(this.myAnimationID == 1 || this.myAnimationID == 2 || this.myAnimationID == 3 || this.myAnimationID == 57)) {
                                    this.degreeForDraw = this.degreeStable;
                                }
                                if (this.fallinSandSlipState != 0) {
                                    if (this.fallinSandSlipState == 1) {
                                        this.faceDirection = true;
                                    } else if (this.fallinSandSlipState == 2) {
                                        this.faceDirection = false;
                                    }
                                }
                                if (this.faceDirection) {
                                    trans = 0;
                                } else {
                                    trans = 2;
                                }
                                if (this.degreeForDraw == this.faceDegree) {
                                    drawDrawerByDegree(g, this.drawer, this.myAnimationID, (this.footPointX >> 6) - camera.f13x, (this.footPointY >> 6) - camera.f14y, loop, this.degreeForDraw, !this.faceDirection);
                                    break;
                                }
                                bodyCenterX = getNewPointX(this.footPointX, 0, (-this.collisionRect.getHeight()) >> 1, this.faceDegree);
                                bodyCenterY = getNewPointY(this.footPointY, 0, (-this.collisionRect.getHeight()) >> 1, this.faceDegree);
                                g.saveCanvas();
                                g.translateCanvas((bodyCenterX >> 6) - camera.f13x, (bodyCenterY >> 6) - camera.f14y);
                                g.rotateCanvas((float) this.degreeForDraw);
                                this.drawer.draw(g, this.myAnimationID, 0, (this.collisionRect.getHeight() >> 1) >> 6, loop, trans);
                                g.restoreCanvas();
                                break;
                        }
                    }
                    drawDrawerByDegree(g, this.drawer, this.myAnimationID, (this.footPointX >> 6) - camera.f13x, (this.footPointY >> 6) - camera.f14y, loop, this.degreeForDraw, !this.faceDirection);
                } else {
                    bodyCenterX = getNewPointX(this.footPointX, 0, -512, this.faceDegree);
                    bodyCenterY = getNewPointY(this.footPointY, 0, -512, this.faceDegree);
                    int drawX = getNewPointX(bodyCenterX, 0, 512, 0);
                    drawY = getNewPointY(bodyCenterY, 0, 512, 0);
                    if (this.collisionState == (byte) 0) {
                        if (this.isAntiGravity) {
                            if (this.faceDirection) {
                                trans = this.totalVelocity >= 0 ? 3 : 1;
                                drawY -= 1024;
                            } else {
                                trans = this.totalVelocity > 0 ? 3 : 1;
                                drawY -= 1024;
                            }
                        } else if (this.faceDirection) {
                            trans = this.totalVelocity >= 0 ? 0 : 2;
                        } else {
                            trans = this.totalVelocity > 0 ? 0 : 2;
                        }
                    } else if (this.isAntiGravity) {
                        if (this.faceDirection) {
                            trans = this.velX <= 0 ? 3 : 1;
                            drawY -= 1024;
                        } else {
                            trans = this.velX < 0 ? 3 : 1;
                            drawY -= 1024;
                        }
                    } else if (this.faceDirection) {
                        trans = this.velX >= 0 ? 0 : 2;
                    } else {
                        trans = this.velX > 0 ? 0 : 2;
                    }
                    this.drawer.draw(g, this.myAnimationID, (drawX >> 6) - camera.f13x, (drawY >> 6) - camera.f14y, loop, trans);
                }
            } else {
                if (this.myAnimationID != this.drawer.getActionId()) {
                    this.drawer.setActionId(this.myAnimationID);
                }
                if (!AnimationDrawer.isAllPause()) {
                    this.drawer.moveOn();
                }
            }
            switch (this.effectID) {
                case 1:
                    drawY = 0;
                    if (this.collisionState == (byte) 0) {
                        if (!this.faceDirection) {
                            trans = this.totalVelocity > 0 ? 0 : 2;
                        } else if (this.totalVelocity >= 0) {
                            trans = 0;
                        } else {
                            trans = 2;
                        }
                    } else if (this.isAntiGravity) {
                        if (this.faceDirection) {
                            trans = this.velX <= 0 ? 3 : 1;
                            drawY = 0 - 1024;
                        } else {
                            trans = this.velX < 0 ? 3 : 1;
                            drawY = 0 - 1024;
                        }
                    } else if (this.faceDirection) {
                        trans = this.velX >= 0 ? 0 : 2;
                    } else {
                        trans = this.velX > 0 ? 0 : 2;
                    }
                    this.effectDrawer.draw(g, 12, (this.posX >> 6) - camera.f13x, (((this.posY + (this.isAntiGravity ? 1024 : 0)) >> 6) + (drawY >> 6)) - camera.f14y, false, trans);
                    if (!this.isInWater) {
                        this.effectDrawer.setSpeed(1, 1);
                        break;
                    } else {
                        this.effectDrawer.setSpeed(1, 2);
                        break;
                    }
            }
            if (this.effectDrawer.checkEnd()) {
                this.effectID = 0;
            }
            this.attackRectVec.removeAllElements();
            if (this.effectID == 1) {
                rect = this.effectDrawer.getARect();
            } else {
                rect = this.drawer.getARect();
            }
            if (this.isAntiGravity) {
                byte[] rectTmp = this.drawer.getARect();
                if (rectTmp != null) {
                    rect[0] = (byte) ((-rectTmp[0]) - rectTmp[2]);
                    rect[1] = (byte) ((-rectTmp[1]) - rectTmp[3]);
                }
            }
            if (rect != null) {
                if (SonicDebug.showCollisionRect) {
                    g.setColor(65280);
                    g.drawRect(((this.footPointX >> 6) + rect[0]) - camera.f13x, ((this.footPointY >> 6) + rect[1]) - camera.f14y, rect[2], rect[3]);
                }
                this.attackRect.initCollision(rect[0] << 6, rect[1] << 6, rect[2] << 6, rect[3] << 6, this.effectID == 1 ? 12 : this.myAnimationID);
                this.attackRectVec.addElement(this.attackRect);
            } else {
                this.attackRect.reset();
            }
            if (this.animationID == -1 && this.drawer.checkEnd() && LOOP_INDEX[this.myAnimationID] >= 0) {
                this.myAnimationID = LOOP_INDEX[this.myAnimationID];
            }
            if (this.isFirstAttack) {
                if (this.myAnimationID == 13 && !this.isCrashFallingSand) {
                    soundInstance.playSe(4);
                }
                this.isFirstAttack = false;
            }
        }
    }

    public void doWhileLand(int degree) {
        super.doWhileLand(degree);
        this.firstJump = false;
    }

    public void extraInputLogic() {
        if (isTerminal && terminalState >= (byte) 2) {
            switch (terminalState) {
                case (byte) 4:
                    this.velX = 0;
                    return;
                default:
                    return;
            }
        }
    }

    public int getRetPower() {
        int retPower = super.getRetPower();
        if (speedCount > 0 && this.myAnimationID >= 13 && this.myAnimationID <= 14) {
            retPower /= 2;
        }
        if (this.myAnimationID == 15) {
            return 150;
        }
        return retPower;
    }

    public boolean needRetPower() {
        if (this.slipping) {
            return false;
        }
        if (this.myAnimationID == 13 || this.myAnimationID == 14 || this.myAnimationID == 15) {
            return true;
        }
        return super.needRetPower();
    }

    public int getSlopeGravity() {
        if (this.slipping) {
            return 0;
        }
        return super.getSlopeGravity();
    }

    public boolean noRotateDraw() {
        if (this.myAnimationID == 13 || this.myAnimationID == 14 || this.myAnimationID == 15) {
            return true;
        }
        return super.noRotateDraw();
    }

    public void beSpring(int springPower, int direction) {
        super.beSpring(springPower, direction);
        if (this.attackLevel != 0) {
            this.attackLevel = 0;
            this.attackCount = 0;
            switch (direction) {
                case 2:
                case 3:
                    this.animationID = 3;
                    if (Key.repeat(Key.gDown)) {
                        this.animationID = 4;
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    public boolean beAccelerate(int power, boolean IsX, GameObject sender) {
        boolean re = super.beAccelerate(power, IsX, sender);
        if (this.attackLevel != 0) {
            this.attackLevel = 0;
            this.attackCount = 0;
            this.animationID = 3;
            if (Key.repeat(Key.gDown)) {
                this.animationID = 4;
            }
        }
        return re;
    }
}
