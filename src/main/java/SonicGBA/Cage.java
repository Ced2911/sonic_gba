package SonicGBA;

import Lib.MyAPI;
import com.sega.mobile.define.MDPhone;
import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;

/* compiled from: GimmickObject */
class Cage extends GimmickObject implements MapBehavior {
    private static final int ANIMAL_CREATE_COUNT = 7;
    private static final int BUTTON_OFFSET_Y = -3904;
    private static final int COLLISION_HEIGHT = 3392;
    private static final int COLLISION_WIDTH = 3712;
    private static final int DOOR_SPEED_X = 300;
    private static final int DRAW_HEIGHT = 72;
    private static final int DRAW_WIDTH = 72;
    private static final int STATE_EXPLOSION = 2;
    private static final int STATE_FALL = 0;
    private static final int STATE_STAY = 1;
    private static MFImage cageExplosiveImage = null;
    private static MFImage cageImage = null;
    private int animalCount;
    private boolean attacking;
    private CageButton button;
    private int count;
    private int doorPosX;
    private int doorPosX2;
    private int doorPosY;
    private int doorVelocityY;
    private MapObject mapObj;
    private int state;

    protected Cage(int x, int y) {
        super(0, x, y, 0, 0, 0, 0);
        if (cageImage == null) {
            try {
                cageImage = MFImage.createImage("/gimmick/cage.png");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (cageExplosiveImage == null) {
            try {
                cageExplosiveImage = MFImage.createImage("/gimmick/cage_door.png");
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        this.button = new CageButton(this.posX, this.posY + BUTTON_OFFSET_Y);
        GameObject.addGameObject(this.button, this.posX, this.posY);
        this.mapObj = new MapObject(this.posX, this.posY, 0, 0, this, 1);
        this.mapObj.setBehavior(this);
        this.state = 0;
        this.attacking = true;
        MapManager.setCameraLeftLimit(MapManager.getCamera().f13x);
        MapManager.setCameraRightLimit(MapManager.getCamera().f13x + MapManager.CAMERA_WIDTH);
    }

    public void logic() {
        switch (this.state) {
            case 0:
                this.mapObj.logic();
                checkWithPlayer(this.posX, this.posY, this.mapObj.getPosX(), this.mapObj.getPosY());
                this.posX = this.mapObj.getPosX();
                this.posY = this.mapObj.getPosY();
                if (!this.button.used) {
                    this.button.posX = this.posX;
                    this.button.posY = this.posY + BUTTON_OFFSET_Y;
                    return;
                }
                return;
            case 1:
                this.attacking = false;
                if (this.button.used) {
                    this.state = 2;
                    soundInstance.playSe(28);
                    this.doorPosX = this.posX;
                    this.doorPosX2 = this.posX;
                    this.doorPosY = this.posY;
                    this.doorVelocityY = -600;
                    this.animalCount = 7;
                    this.count = 0;
                    return;
                }
                return;
            case 2:
                this.doorPosX -= 300;
                this.doorPosX2 += 300;
                this.doorVelocityY += GRAVITY;
                this.doorPosY += this.doorVelocityY;
                if (this.count > 0) {
                    this.count--;
                }
                if (this.count == 0 && this.animalCount > 0) {
                    SmallAnimal.addPatrolAnimal(2, this.posX, this.posY - MDPhone.SCREEN_HEIGHT, 1, this.posX - 3200, this.posX + 3200);
                    SmallAnimal.addPatrolAnimal(1, this.posX, this.posY - MDPhone.SCREEN_HEIGHT, 1, this.posX - 3200, this.posX + 3200);
                    this.count = 4;
                    this.animalCount--;
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void draw(MFGraphics g) {
        switch (this.state) {
            case 2:
                drawInMap(g, cageImage, 72, 0, 72, 72, 0, this.posX, this.posY, 33);
                this.button.drawButton(g);
                drawInMap(g, cageExplosiveImage, this.doorPosX, this.doorPosY, 40);
                drawInMap(g, cageExplosiveImage, 0, 0, MyAPI.zoomIn(cageExplosiveImage.getWidth()), MyAPI.zoomIn(cageExplosiveImage.getHeight()), 2, this.doorPosX2, this.doorPosY, 36);
                return;
            default:
                drawInMap(g, cageImage, 0, 0, 72, 72, 0, this.posX, this.posY, 33);
                this.button.drawButton(g);
                return;
        }
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 1856, y - COLLISION_HEIGHT, COLLISION_WIDTH, COLLISION_HEIGHT);
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (this.attacking) {
            object.beHurtByCage();
        } else {
            object.beStop(0, direction, this);
        }
    }

    public void doWhileTouchGround(int vx, int vy) {
        this.state = 1;
        soundInstance.playSe(27);
        MapManager.setShake(8);
    }

    public void close() {
        this.mapObj = null;
        this.button = null;
    }

    public static void releaseAllResource() {
        cageImage = null;
        cageExplosiveImage = null;
    }

    public boolean hasSideCollision() {
        return false;
    }

    public boolean hasTopCollision() {
        return false;
    }

    public int getPaintLayer() {
        return 0;
    }

    public boolean hasDownCollision() {
        return true;
    }

    public void doWhileToucRoof(int vx, int vy) {
    }

    public int getGravity() {
        return GRAVITY;
    }
}
