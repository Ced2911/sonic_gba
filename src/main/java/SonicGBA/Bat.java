package SonicGBA;

import Lib.Animation;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: EnemyObject */
class Bat extends EnemyObject {
    private static final int ALERT_RANGE = 7168;
    private static final int COLLISION_HEIGHT = 1792;
    private static final int COLLISION_WIDTH = 1792;
    private static final int FLY_TOP = 1536;
    private static final int STATE_ATTACK = 1;
    private static final int STATE_FLY = 0;
    private static Animation batAnimation;
    private int alert_state;
    private int attack_cnt;
    private int attack_cnt_max = 30;
    private int emenyid;
    private int limitBottomY;
    private int limitLeftX;
    private int limitRightX;
    private int limitTopY;
    private int offsety = PlayerSonic.BACK_JUMP_SPEED_X;
    private int state;
    private int velocity = 128;

    public static void releaseAllResource() {
        Animation.closeAnimation(batAnimation);
        batAnimation = null;
    }

    protected Bat(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        this.posY -= 1024;
        this.limitLeftX = this.posX;
        this.limitRightX = this.posX + this.mWidth;
        this.limitTopY = this.posY - 1536;
        this.limitBottomY = this.posY;
        if (batAnimation == null) {
            batAnimation = new Animation("/animation/bat");
        }
        this.drawer = batAnimation.getDrawer(0, true, 0);
        this.emenyid = id;
    }

    public void logic() {
        if (!this.dead) {
            int preX = this.posX;
            int preY = this.posY;
            this.alert_state = checkPlayerInEnemyAlertRange(this.posX >> 6, this.posY >> 6, ALERT_RANGE);
            switch (this.state) {
                case 0:
                    if (this.velocity > 0) {
                        this.posX += this.velocity;
                        if (this.posX >= this.limitRightX) {
                            this.posX = this.limitRightX;
                            this.velocity = -this.velocity;
                        }
                    } else {
                        this.posX += this.velocity;
                        if (this.posX <= this.limitLeftX) {
                            this.posX = this.limitLeftX;
                            this.velocity = -this.velocity;
                        }
                    }
                    if (this.offsety > 0) {
                        this.posY += this.offsety;
                        if (this.posY >= this.limitBottomY) {
                            this.posY = this.limitBottomY;
                            this.offsety = -this.offsety;
                        }
                    } else {
                        this.posY += this.offsety;
                        if (this.posY <= this.limitTopY) {
                            this.posY = this.limitTopY;
                            this.offsety = -this.offsety;
                        }
                    }
                    if (Math.abs(player.getCheckPositionX() - this.posX) < 1792 && this.alert_state == 0) {
                        if (this.attack_cnt >= this.attack_cnt_max) {
                            this.state = 1;
                        } else {
                            this.attack_cnt++;
                        }
                    }
                    checkWithPlayer(preX, preY, this.posX, this.posY);
                    return;
                case 1:
                    this.drawer.setActionId(1);
                    this.drawer.setLoop(false);
                    if (this.drawer.checkEnd()) {
                        BulletObject.addBullet(this.emenyid, this.posX, this.posY, 0, 0);
                        this.state = 0;
                        this.drawer.setActionId(0);
                        this.drawer.setLoop(true);
                        this.attack_cnt = 0;
                    }
                    checkWithPlayer(preX, preY, this.posX, this.posY);
                    return;
                default:
                    return;
            }
        }
    }

    public void draw(MFGraphics g) {
        if (!this.dead) {
            drawInMap(g, this.drawer);
            drawCollisionRect(g);
        }
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 896, (y - 896) - 320, 1792, 1792);
    }
}
