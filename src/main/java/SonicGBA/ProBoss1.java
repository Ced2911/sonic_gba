package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import Lib.SoundSystem;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: EnemyObject */
class ProBoss1 extends EnemyObject {
    private static final int COLLISION_HEIGHT = 64;
    private static final int COLLISION_WIDTH = 1792;
    private static final int SIDE = 671232;
    private static final int SIDE_DOWN_MIDDLE = 720;
    private static final int STATE_ANGRY = 2;
    private static final int STATE_FIND = 1;
    private static final int STATE_GO = 5;
    private static final int STATE_TURN1 = 3;
    private static final int STATE_TURN2 = 4;
    private static final int STATE_WAIT = 0;
    private static Animation boatAni;
    private static Animation faceAni;
    private int StartX;
    private AnimationDrawer boatdrawer;
    private AnimationDrawer facedrawer;
    private int state;
    private int velocity = 576;

    public static void releaseAllResource() {
        Animation.closeAnimation(boatAni);
        Animation.closeAnimation(faceAni);
        boatAni = null;
        faceAni = null;
    }

    protected ProBoss1(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        this.posX -= this.iLeft * 8;
        this.posY -= this.iTop * 8;
        this.posY += 1280;
        this.StartX = this.posX;
        refreshCollisionRect(this.posX >> 6, this.posY >> 6);
        if (boatAni == null) {
            boatAni = new Animation("/animation/pod_boat");
        }
        this.boatdrawer = boatAni.getDrawer(0, true, 0);
        if (faceAni == null) {
            faceAni = new Animation("/animation/pod_face");
        }
        this.facedrawer = faceAni.getDrawer(0, true, 0);
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (!this.dead) {
        }
    }

    public void doWhileBeAttack(PlayerObject object, int direction, int animationID) {
    }

    public void logic() {
        if (!this.dead) {
            if (this.state > 0) {
                isBossEnter = true;
            }
            switch (this.state) {
                case 0:
                    if (player.getFootPositionX() >= SIDE) {
                        if (!this.IsPlayBossBattleBGM) {
                            bossFighting = true;
                            bossID = 21;
                            SoundSystem.getInstance().playBgm(23, true);
                            this.IsPlayBossBattleBGM = true;
                            int side_down = (MapManager.CAMERA_HEIGHT / 4) + SIDE_DOWN_MIDDLE;
                            MapManager.setCameraDownLimit(side_down);
                            MapManager.setCameraUpLimit(side_down - SCREEN_HEIGHT);
                            MapManager.setCameraLeftLimit(MapManager.getCamera().f13x);
                        }
                        this.state = 1;
                        player.setMeetingBoss(false);
                        this.facedrawer.setActionId(1);
                        this.facedrawer.setLoop(false);
                        return;
                    }
                    return;
                case 1:
                    if (this.facedrawer.checkEnd()) {
                        this.state = 2;
                        this.facedrawer.setActionId(2);
                        this.facedrawer.setLoop(false);
                        return;
                    }
                    return;
                case 2:
                    if (this.facedrawer.checkEnd()) {
                        this.state = 3;
                        this.facedrawer.setActionId(0);
                        this.facedrawer.setLoop(true);
                        this.boatdrawer.setActionId(1);
                        this.boatdrawer.setLoop(false);
                        return;
                    }
                    return;
                case 3:
                    if (this.boatdrawer.checkEnd()) {
                        this.state = 4;
                        this.facedrawer.setActionId(0);
                        this.facedrawer.setTrans(2);
                        this.facedrawer.setLoop(true);
                        this.boatdrawer.setActionId(1);
                        this.boatdrawer.setTrans(2);
                        this.boatdrawer.setLoop(false);
                        return;
                    }
                    return;
                case 4:
                    if (this.boatdrawer.checkEnd()) {
                        this.state = 5;
                        this.boatdrawer.setActionId(0);
                        this.boatdrawer.setTrans(2);
                        this.boatdrawer.setLoop(true);
                        return;
                    }
                    return;
                case 5:
                    this.posX += this.velocity;
                    if (((this.posX - this.StartX) >> 6) >= 200) {
                        player.setMeetingBoss(true);
                        MapManager.lockCamera(false);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    public void draw(MFGraphics g) {
        if (!this.dead) {
            drawInMap(g, this.boatdrawer);
            drawInMap(g, this.facedrawer, this.posX, this.posY - Boss6Block.COLLISION2_HEIGHT);
        }
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x, y, COLLISION_WIDTH, 64);
    }

    public void close() {
        this.boatdrawer = null;
        this.facedrawer = null;
        super.close();
    }
}
