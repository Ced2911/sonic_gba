package SonicGBA;

/* compiled from: GimmickObject */
class SeabedVolcanoHurt extends GimmickObject {
    private static final int COLLISION_HEIGHT = 1152;
    private static final int COLLISION_WIDTH = 3072;
    private SeabedVolcanoBase sb;

    public SeabedVolcanoHurt(int x, int y, SeabedVolcanoBase sb) {
        super(0, x, y, 0, 0, 0, 0);
        this.sb = sb;
    }

    public void refreshCollisionRect(int x, int y) {
        int height = SeabedVolcanoPlatform.sPosY;
        if (height > 0) {
            height = 0;
        }
        if (height < -1152) {
            height = -1152;
        }
        this.collisionRect.setRect(this.posX - 1536, this.posY + height, 3072, Math.abs(height));
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (this.collisionRect.getHeight() != 0 && !object.isFootOnObject(this.sb.sp)) {
            object.beHurt();
        }
    }

    public void close() {
        this.sb = null;
    }
}
