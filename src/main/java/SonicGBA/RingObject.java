package SonicGBA;

import Lib.Animation;
import Lib.MyAPI;
import Lib.crlFP32;
import com.sega.engine.action.ACCollision;
import com.sega.engine.action.ACObject;
import com.sega.mobile.define.MDPhone;
import com.sega.mobile.framework.device.MFGraphics;
import java.util.Vector;

public class RingObject extends GameObject {
    private static final int DEGREE_CAL_START = 270;
    private static final int DEGREE_SPACE = 20;
    private static final int DEGREE_START = 20;
    private static final int MAX_RING_POP = 50;
    private static final int MAX_VELOCITY = 4200;
    private static final int VELOCITY_ADD = 300;
    private static final int VELOCITY_CHANGE = 300;
    private static final int VELOCITY_OFFSET_Y = (GRAVITY * -3);
    private static final int VELOCITY_OFFSET_Y2 = (GRAVITY * -4);
    private static final int VELOCITY_START = 800;
    private static Vector moveRingVec = new Vector();
    private static Animation ringAnimation;
    private boolean beAttractive;
    private boolean used;
    private int velocity = 0;

    public static RingObject getNewInstance(int x, int y) {
        RingObject reElement = new RingObject(x << 6, y << 6);
        reElement.refreshCollisionRect(reElement.posX, reElement.posY);
        return reElement;
    }

    public static void hurtRingExplosion(int ringNum, int x, int y, int layer, boolean antiGrivity) {
        /*
        if (ringNum > 50) {
            ringNum = 50;
        }
        if (antiGrivity) {
            y += 3072;
        }
        int degree = 0;
        if (antiGrivity) {
            //int velocity = true;
            int velocity = 1;
        } else {
            boolean velocity2 = true;
        }
        if (ringNum == 1) {
            ringNum = 0 * 20;
            antiGrivity = (antiGrivity ? true : true) + ((ringNum / 170) * 300);
            ringNum = (((ringNum % 170) + DEGREE_CAL_START) + 10) % MDPhone.SCREEN_WIDTH;
            GameObject.addGameObject(new MoveRingObject(x, y, ((-antiGrivity) * MyAPI.dCos(ringNum)) / 100, ((MyAPI.dSin(ringNum) * antiGrivity) / 100) + VELOCITY_OFFSET_Y2, layer, systemClock));
            x = 0;
            boolean y2 = antiGrivity;
        } else {
            int ringCount = 0;
            int ringCount2 = velocity;
            while (ringCount < ringNum / 2) {
                degree = ringCount * 20;
                int velocity3 = ((degree / 170) * 300) + (antiGrivity ? -800 : VELOCITY_START);
                ringCount2 = degree % 170;
                int degree2 = ((ringCount2 + DEGREE_CAL_START) + 10) % MDPhone.SCREEN_WIDTH;
                GameObject.addGameObject(new MoveRingObject(x, y, (MyAPI.dCos(degree2) * velocity3) / 100, ((MyAPI.dSin(degree2) * velocity3) / 100) + VELOCITY_OFFSET_Y, layer, systemClock));
                ringCount2 = ((DEGREE_CAL_START - ringCount2) - 10) % MDPhone.SCREEN_WIDTH;
                GameObject.addGameObject(new MoveRingObject(x, y, (MyAPI.dCos(ringCount2) * velocity3) / 100, ((MyAPI.dSin(ringCount2) * velocity3) / 100) + VELOCITY_OFFSET_Y, layer, systemClock));
                ringCount++;
                degree = ringCount2;
                ringCount2 = velocity3;
            }
            if ((ringNum % 2)>0) {
                ringNum = ((((ringCount * 20) % 170) + DEGREE_CAL_START) + 10) % MDPhone.SCREEN_WIDTH;
                GameObject.addGameObject(new MoveRingObject(x, y, (MyAPI.dCos(ringNum) * ringCount2) / 100, (MyAPI.dSin(ringNum) * ringCount2) / 100, layer, systemClock));
                x = ringCount;
                y = ringCount2;
            } else {
                x = ringCount;
                y = ringCount2;
                ringNum = degree;
            }
        }
        */
        soundInstance.playSe(13);
    }

    public static void ringInit() {
        moveRingVec.removeAllElements();
    }

    public static void ringLogic() {
        int i = 0;
        while (i < moveRingVec.size()) {
            RingObject ring = (RingObject) moveRingVec.elementAt(i);
            ring.ringMoveLogic();
            if (ring.objectChkDestroy()) {
                moveRingVec.removeElementAt(i);
                i--;
            }
            i++;
        }
    }

    public static void ringDraw(MFGraphics g) {
        for (int i = 0; i < moveRingVec.size(); i++) {
            ((RingObject) moveRingVec.elementAt(i)).draw(g);
        }
    }

    protected RingObject(int x, int y) {
        this.posX = x;
        this.posY = y;
        if (ringDrawer == null) {
            ringAnimation = new Animation("/animation/ring");
            ringDrawer = ringAnimation.getDrawer();
            ringDrawer.setPause(true);
        }
        this.used = false;
        this.beAttractive = false;
        this.mWidth = 1024;
        this.mHeight = 1024;
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (!this.used && !object.hurtNoControl && !object.isSharked) {
            this.used = true;
            Effect.showEffect(ringAnimation, 1, this.posX >> 6, this.posY >> 6, 0);
            PlayerObject.getRing(1);
            soundInstance.playSe(12);
            isGotRings = true;
        }
    }

    public void doWhileRail(PlayerObject object, int direction) {
        doWhileCollision(object, direction);
    }

    public void draw(MFGraphics g) {
        if (!this.used) {
            drawInMap(g, ringDrawer);
        }
    }

    public void logic() {
    }

    public void ringMoveLogic() {
        if (this.beAttractive) {
            this.velocity += 300;
            if (this.velocity > MAX_VELOCITY) {
                this.velocity = MAX_VELOCITY;
            }
            int distanceX = player.getCheckPositionX() - this.posX;
            int distanceY = player.getCheckPositionY() - this.posY;
            if (distanceX == 0 && distanceY == 0) {
                doWhileCollision(player, 4);
                return;
            }
            int degree = (crlFP32.actTanDegree(distanceY, distanceX) + MDPhone.SCREEN_WIDTH) % MDPhone.SCREEN_WIDTH;
            int powerX = (this.velocity * MyAPI.dCos(degree)) / 100;
            int powerY = (this.velocity * MyAPI.dSin(degree)) / 100;
            if (Math.abs(powerX) > Math.abs(distanceX)) {
                powerX = distanceX;
            }
            if (Math.abs(powerY) > Math.abs(distanceY)) {
                powerY = distanceY;
            }
            this.posX += powerX;
            this.posY += powerY;
            refreshCollisionRect(this.posX, this.posY);
            if (collisionChkWithObject(player)) {
                doWhileCollision(player, 4);
            }
        }
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - (this.mWidth >> 1), y - this.mHeight, this.mWidth, this.mHeight + 128);
    }

    public void close() {
    }

    public void beAttract() {
        if (!this.beAttractive) {
            this.beAttractive = true;
            moveRingVec.addElement(this);
        }
    }

    public boolean objectChkDestroy() {
        return this.used;
    }

    public void doBeforeCollisionCheck() {
    }

    public void doWhileCollision(ACObject arg0, ACCollision arg1, int arg2, int arg3, int arg4, int arg5, int arg6) {
    }
}
