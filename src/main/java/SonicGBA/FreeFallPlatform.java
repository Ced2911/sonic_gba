package SonicGBA;

import Lib.Coordinate;
import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;

/* compiled from: GimmickObject */
class FreeFallPlatform extends GimmickObject {
    private static final int COLLISION_HEIGHT = 28;
    private static final int DRAW_HEIGHT = 40;
    private static final int DRAW_WIDTH = 96;
    private static MFImage image = null;
    private int posXorg;
    private int posYorg;
    private int rollCount;
    private FreeFallSystem system;

    protected FreeFallPlatform(FreeFallSystem system, int x, int y) {
        super(0, x, y, 0, 0, 0, 0);
        this.system = system;
        if (image == null) {
            try {
                image = MFImage.createImage("/gimmick/freefall_platform.png");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        Coordinate co = system.getBarPosition();
        this.posXorg = co.f13x;
        this.posYorg = co.f14y;
    }

    public void init() {
        this.posX = this.posXorg;
        this.posY = this.posYorg;
        refreshCollisionRect(this.posX, this.posY);
    }

    public void platformLogic() {
        Coordinate co = this.system.getBarPosition();
        checkWithPlayer(this.posX, this.posY, co.f13x, co.f14y);
        this.posX = co.f13x;
        this.posY = co.f14y;
    }

    public void draw(MFGraphics g) {
        if (!this.system.initFlag) {
            if (this.system.moving) {
                drawInMap(g, image, ((int) ((systemClock / 5) % 2)) * 96, 0, 96, 40, 0, 17);
            } else {
                drawInMap(g, image, 0, 0, 96, 40, 0, 17);
            }
            drawCollisionRect(g);
        }
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 3072, y + 768, 6144, 1792);
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (direction != 0) {
            object.beStop(0, direction, this);
        }
    }

    public void close() {
        this.system = null;
    }

    public static void releaseAllResource() {
        image = null;
    }

    public int getPaintLayer() {
        return 0;
    }
}
