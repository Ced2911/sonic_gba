package SonicGBA;

/* compiled from: RingObject */
class ExtraRing extends MoveRingObject {
    protected ExtraRing(int x, int y, int vx, int vy, int layer, long appearTime, int unTouchCount) {
        super(x, y, vx, vy, layer, appearTime, unTouchCount);
    }

    public int getGravity() {
        return GRAVITY >> 2;
    }
}
