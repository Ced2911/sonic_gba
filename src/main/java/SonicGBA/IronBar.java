package SonicGBA;

import GameEngine.Key;

/* compiled from: GimmickObject */
class IronBar extends GimmickObject {
    private static final int BAR_VELOCITY = 500;
    private boolean touching = false;

    protected IronBar(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
    }

    public void logic() {
        if (this.touching) {
            int preX = player.getFootPositionX();
            int preY = player.getFootPositionY();
            player.doPullBarMotion(this.posY);
            if (Key.repeat(Key.gLeft)) {
                player.setFootPositionX(player.getFootPositionX() - 500);
                player.setAnimationId(28);
                player.faceDirection = false;
            } else if (Key.repeat(Key.gRight)) {
                player.setFootPositionX(player.getFootPositionX() + 500);
                player.setAnimationId(28);
                player.faceDirection = true;
            }
            if (player.getCollisionRect().x1 < this.collisionRect.x0 || player.getCollisionRect().x0 > this.collisionRect.x1 || Key.press(Key.gUp | 16777216)) {
                player.outOfControl = false;
                player.setAnimationId(10);
                this.touching = false;
                player.leavingBar = true;
                System.out.println("do leave bar");
            }
            player.checkWithObject(preX, preY, player.getFootPositionX(), player.getFootPositionY());
        }
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (!this.touching && !player.leavingBar && !player.outOfControl && player.getVelY() >= 0) {
            this.touching = true;
            player.setOutOfControl(this);
            player.doPullBarMotion(this.posY);
            player.degreeForDraw = player.faceDegree;
        }
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(this.posX + ((this.iLeft * 8) << 6), this.posY, this.mWidth, 2);
    }
}
