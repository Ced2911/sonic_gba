package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import Lib.MyAPI;
import com.sega.mobile.define.MDPhone;
import com.sega.mobile.framework.android.Graphics;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: EnemyObject */
class BossF3Arm extends EnemyObject {
    private static final int COLLISION_HEIGHT = 1280;
    private static final int COLLISION_WIDTH = 1280;
    private static Animation armAni;
    private int aniState;
    private AnimationDrawer armDrawer;
    private int catchState;
    private int collisionOffsetX;
    private int collisionOffsetY;
    private int degree;
    private int directFlag;
    private boolean isAvaliable;
    private boolean isCaught;
    private boolean isShaking;

    protected BossF3Arm(int x, int y, int direct) {
        super(30, x, y, 0, 0, 0, 0);
        this.directFlag = direct;
        this.posX = x;
        this.posY = y;
        if (armAni == null) {
            armAni = new Animation("/animation/bossf3_arm");
        }
        this.armDrawer = armAni.getDrawer(0, true, this.directFlag == 0 ? 0 : 2);
        this.isAvaliable = false;
        this.isCaught = false;
        this.isShaking = false;
        this.catchState = 0;
        this.collisionOffsetX = 0;
        this.collisionOffsetY = 0;
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        PlayerObject playerObject;
        if (this.isAvaliable) {
            if (object == player && this.catchState == 0 && player.canBeHurt()) {
                playerObject = player;
                if (PlayerObject.getRingNum() > 0) {
                    player.changeVisible(false);
                    player.setAnimationId(52);
                } else if (this.isShaking) {
                    player.setAnimationId(52);
                } else {
                    player.setDie(false);
                }
                this.isCaught = true;
                if (player instanceof PlayerTails) {
                    ((PlayerTails) player).stopFly();
                    player.animationID = 52;
                    player.myAnimationID = 47;
                }
            }
        } else if (this.catchState == 0) {
            playerObject = player;
            if (!PlayerObject.isTerminal && player.canBeHurt()) {
                player.beHurt();
            }
        }
    }

    public void doWhileBeAttack(PlayerObject object, int direction, int animationID) {
    }

    public void setAniState(int state) {
        this.aniState = state;
        this.armDrawer.setActionId(state);
        this.armDrawer.setLoop(false);
    }

    public void setAvaliable(boolean state) {
        this.isAvaliable = state;
    }

    public boolean getCaughtState() {
        return this.isCaught;
    }

    public void setCaughtFlag(boolean state) {
        this.isCaught = state;
    }

    public void setCatchState(int state) {
        this.catchState = state;
    }

    public void setDegree(int degree) {
        int i;
        this.degree = degree;
        int dCos = (MyAPI.dCos(degree) * 15) / 100;
        if (this.directFlag == 0) {
            i = -1;
        } else {
            i = 1;
        }
        this.collisionOffsetX = (dCos * i) << 6;
        dCos = (MyAPI.dSin(degree) * 40) / 100;
        if (this.directFlag == 0) {
            i = -1;
        } else {
            i = 1;
        }
        this.collisionOffsetY = (dCos * i) << 6;
    }

    public void setShakeState(boolean state) {
        this.isShaking = state;
        player.isSharked = state;
    }

    public void releasePlayer() {
        player.changeVisible(true);
        player.setMeetingBoss(true);
        player.beHurtNoRingLose();
        player.setVelX(this.directFlag == 0 ? 800 : -800);
        player.setVelY(-1200);
        player.isSharked = false;
    }

    public void logic() {
    }

    public void logic(int x, int y) {
        this.posX = x;
        this.posY = y;
        int preX = this.posX;
        int preY = this.posY;
        if (this.isCaught && !player.isDead) {
            player.setFootPositionX(this.posX);
            player.setFootPositionY(this.posY);
            player.setVelX(0);
            player.setVelY(0);
        }
        refreshCollisionRect(this.posX, this.posY);
        checkWithPlayer(preX, preY, this.posX, this.posY);
    }

    public void draw(MFGraphics g) {
        if (this.isCaught && !player.isDead) {
            player.degreeForDraw = this.degree;
            player.drawCharacter(g);
        }
        if (this.degree != 0 && this.degree != MDPhone.SCREEN_WIDTH) {
            Graphics g2 = (Graphics) g.getSystemGraphics();
            g2.save();
            if (this.isShaking) {
                g2.translate((float) ((this.posX >> 6) - camera.f13x), (float) ((this.posY >> 6) - camera.f14y));
            } else {
                g2.translate((float) (((this.posX >> 6) - camera.f13x) + (this.directFlag == 0 ? 16 : -16)), (float) ((this.posY >> 6) - camera.f14y));
            }
            g2.rotate((float) this.degree);
            if (this.isShaking) {
                int i;
                if (this.directFlag == 0) {
                    i = 16;
                } else {
                    i = -16;
                }
                g2.translate((float) i, 0.0f);
            }
            this.armDrawer.draw(g, 0, 0);
            g2.restore();
        } else if (this.isShaking) {
            drawInMap(g, this.armDrawer, this.posX, this.posY);
        } else {
            drawInMap(g, this.armDrawer, this.posX + ((this.directFlag == 0 ? 16 : -16) << 6), this.posY);
        }
        drawCollisionRect(g);
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect((x - MDPhone.SCREEN_HEIGHT) + this.collisionOffsetX, (y - MDPhone.SCREEN_HEIGHT) + this.collisionOffsetY, 1280, 1280);
    }
}
