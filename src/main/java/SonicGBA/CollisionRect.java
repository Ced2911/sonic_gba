package SonicGBA;

import Lib.Coordinate;
import Lib.Line;
import Lib.Line.CrossPoint;
import Lib.MyAPI;
import com.sega.mobile.framework.device.MFGraphics;

public class CollisionRect {
    private static CrossPoint point = new CrossPoint();
    public int centerX;
    public int centerY;
    public int degree;
    public int[] rX;
    public int[] rY;
    public int x0;
    public int x1;
    public int y0;
    public int y1;

    public CollisionRect() {
        this.rX = new int[4];
        this.rY = new int[4];
    }

    public CollisionRect(int x0, int y0, int x1, int y1) {
        this.rX = new int[4];
        this.rY = new int[4];
        setTwoPosition(x0, y0, x1, y1);
    }

    public CollisionRect(int x, int y, int width, int height, boolean a) {
        this.rX = new int[4];
        this.rY = new int[4];
        setRect(x, y, width, height);
    }

    public void setTwoPosition(int x0, int y0, int x1, int y1) {
        this.x0 = Math.min(x0, x1);
        this.x1 = Math.max(x0, x1);
        this.y0 = Math.min(y0, y1);
        this.y1 = Math.max(y0, y1);
        setRotate(this.degree, 0, 0);
    }

    public void setRect(int x, int y, int width, int height) {
        if (width < 0) {
            this.x0 = x + width;
            this.x1 = x;
        } else {
            this.x0 = x;
            this.x1 = x + width;
        }
        if (height < 0) {
            this.y0 = y + height;
            this.y1 = y;
        } else {
            this.y0 = y;
            this.y1 = y + height;
        }
        setRotate(this.degree, 0, 0);
    }

    public boolean collisionChk(CollisionRect rect) {
        if (this.degree != 0 || rect.degree != 0) {
            return collisionChkWithDegree(rect);
        }
        if (this.y1 <= rect.y0 || this.y0 > rect.y1) {
            return false;
        }
        if (this.x1 <= rect.x0 || this.x0 > rect.x1) {
            return false;
        }
        return true;
    }

    public boolean collisionChkWidth(CollisionRect rect) {
        if (this.degree != 0 || rect.degree != 0) {
            return collisionChkWithDegree(rect);
        }
        if (this.x1 <= rect.x0 || this.x0 > rect.x1) {
            return false;
        }
        return true;
    }

    public boolean collisionChk(int x, int y) {
        if (this.y1 <= y || this.y0 > y) {
            return false;
        }
        if (this.x1 <= x || this.x0 > x) {
            return false;
        }
        return true;
    }

    public void addVelocity(int vx, int vy) {
        if (vx < 0) {
            this.x0 += vx;
        } else if (vx > 0) {
            this.x1 += vx;
        }
        if (vy < 0) {
            this.y0 += vy;
        } else if (vy > 0) {
            this.y1 += vy;
        }
    }

    public CollisionRect getClone(int xOffset, int yOffset) {
        return new CollisionRect(this.x0 + xOffset, this.y0 + yOffset, this.x1 + xOffset, this.y1 + yOffset);
    }

    public boolean isLeftOf(CollisionRect collisionRect) {
        return isLeftOf(collisionRect, 0);
    }

    public boolean isRightOf(CollisionRect collisionRect) {
        return isRightOf(collisionRect, 0);
    }

    public boolean isUpOf(CollisionRect collisionRect) {
        return isUpOf(collisionRect, 0);
    }

    public boolean isDownOf(CollisionRect collisionRect) {
        return isDownOf(collisionRect, 0);
    }

    public boolean isLeftOf(CollisionRect collisionRect, int offset) {
        if (this.x1 <= collisionRect.x0 + offset) {
            return true;
        }
        return false;
    }

    public boolean isRightOf(CollisionRect collisionRect, int offset) {
        if (this.x0 >= collisionRect.x1 - offset) {
            return true;
        }
        return false;
    }

    public boolean isUpOf(CollisionRect collisionRect, int offset) {
        if (this.y1 <= collisionRect.y0 + offset) {
            return true;
        }
        return false;
    }

    public boolean isDownOf(CollisionRect collisionRect, int offset) {
        if (this.y0 >= collisionRect.y1 - offset) {
            return true;
        }
        return false;
    }

    public void draw(MFGraphics g, Coordinate camera) {
        g.setColor(16711680);
        g.drawRect(this.x0 - camera.f13x, this.y0 - camera.f14y, this.x1 - this.x0, this.y1 - this.y0);
    }

    public int getWidth() {
        return this.x1 - this.x0;
    }

    public int getHeight() {
        return this.y1 - this.y0;
    }

    public int getCenterX() {
        return (this.x0 + this.x1) >> 1;
    }

    public int getCenterY() {
        return (this.y0 + this.y1) >> 1;
    }

    public String toString() {
        return "x0:" + this.x0 + "|x1:" + this.x1 + "|y0:" + this.y0 + "|y1:" + this.y1;
    }

    public void setRotate(int degree, int centerX, int centerY) {
        this.degree = degree;
        if (degree == 0) {
            this.rX[0] = this.x0;
            this.rY[0] = this.y0;
            this.rX[1] = this.x1;
            this.rY[1] = this.y0;
            this.rX[2] = this.x1;
            this.rY[2] = this.y1;
            this.rX[3] = this.x0;
            this.rY[3] = this.y1;
        }
        int realCenterX = this.x0 + centerX;
        int realCenterY = this.y0 + centerY;
        this.rX[0] = MyAPI.getRelativePointX(realCenterX, -centerX, -centerY, degree);
        this.rY[0] = MyAPI.getRelativePointY(realCenterY, -centerX, -centerY, degree);
        this.rX[1] = MyAPI.getRelativePointX(realCenterX, (this.x1 - this.x0) - centerX, -centerY, degree);
        this.rY[1] = MyAPI.getRelativePointY(realCenterY, (this.x1 - this.x0) - centerX, -centerY, degree);
        this.rX[2] = MyAPI.getRelativePointX(realCenterX, (this.x1 - this.x0) - centerX, (this.y1 - this.y0) - centerY, degree);
        this.rY[2] = MyAPI.getRelativePointY(realCenterY, (this.x1 - this.x0) - centerX, (this.y1 - this.y0) - centerY, degree);
        this.rX[3] = MyAPI.getRelativePointX(realCenterX, -centerX, (this.y1 - this.y0) - centerY, degree);
        this.rY[3] = MyAPI.getRelativePointY(realCenterY, -centerX, (this.y1 - this.y0) - centerY, degree);
    }

    public boolean collisionChkWithDegree(CollisionRect rect) {
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                Line.getCrossPoint(point, this.rX[i], this.rY[i], this.rX[(i + 1) % 4], this.rY[(i + 1) % 4], rect.rX[j], rect.rY[j], rect.rX[(j + 1) % 4], rect.rY[(j + 1) % 4]);
                if (point.hasPoint) {
                    return true;
                }
            }
        }
        return false;
    }
}
