package SonicGBA;

import Lib.MyAPI;
import Lib.SoundSystem;
import Lib.crlFP32;
import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;

/* compiled from: GimmickObject */
class RopeStart extends GimmickObject {
    private static final int DEGREE = crlFP32.actTanDegree(1, 2);
    private static final int DRAW_HEIGHT = 24;
    private static final int DRAW_WIDTH = 16;
    private static final int MAX_VELOCITY = 1800;
    private static MFImage hookImage2;
    private boolean controlling;
    public int degree;
    private boolean initFlag = false;
    private int posOriginalX;
    private int posOriginalY;
    private int velocity;

    protected RopeStart(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        if (StageManager.getCurrentZoneId() == 4) {
            if (hookImage2 == null) {
                try {
                    hookImage2 = MFImage.createImage("/gimmick/hook_4.png");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else if (hookImage == null) {
            try {
                hookImage = MFImage.createImage("/gimmick/hook.png");
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        this.used = false;
        this.controlling = false;
        this.initFlag = false;
        if (this.iLeft == 0) {
            this.degree = RollPlatformSpeedC.DEGREE_VELOCITY - DEGREE;
        } else {
            this.degree = DEGREE;
        }
        this.posOriginalX = this.posX;
        this.posOriginalY = this.posY;
    }

    public int getPaintLayer() {
        return 0;
    }

    public void draw(MFGraphics g) {
        if (!this.initFlag) {
            if (StageManager.getCurrentZoneId() == 4) {
                drawInMap(g, hookImage2, 0, 0, 16, 24, 0, this.posX, this.posY, 17);
                return;
            }
            drawInMap(g, hookImage, 0, 0, 16, 24, 0, this.posX, this.posY, 17);
        }
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (!this.initFlag) {
            if (!this.used) {
                isGotRings = false;
            }
            if (!this.used) {
                this.velocity = Math.abs(object.getVelX());
                this.used = true;
                this.controlling = true;
                object.setOutOfControl(this);
                object.railing = true;
                object.setCollisionState((byte) 1);
                object.faceDirection = true;
                player.doPullMotion(this.posX, this.posY + 1408);
            }
        }
    }

    public void logic() {
        if (this.initFlag) {
            refreshCollisionRect(this.posX, this.posY);
            if (!screenRect.collisionChk(this.collisionRect)) {
                this.initFlag = false;
            }
        } else if (player.outOfControl && player.outOfControlObject == this) {
            this.velocity += (GRAVITY * MyAPI.dSin(this.degree)) / 100;
            if (this.velocity > 0) {
                this.velocity -= 30;
                if (this.velocity < 0) {
                    this.velocity = 0;
                }
            }
            if (this.velocity < 0) {
                this.velocity += 30;
                if (this.velocity > 0) {
                    this.velocity = 0;
                }
            }
            this.posX += (this.velocity * MyAPI.dCos(this.degree)) / 100;
            this.posY += (this.velocity * MyAPI.dSin(this.degree)) / 100;
            refreshCollisionRect(this.posX, this.posY);
            if (player.outOfControl) {
                int preX = player.getFootPositionX();
                int preY = player.getFootPositionY();
                player.doPullMotion(this.posX, this.posY + 1408);
                player.setVelX((this.velocity * MyAPI.dCos(this.degree)) / 100);
                player.setVelY((this.velocity * MyAPI.dSin(this.degree)) / 100);
                player.checkWithObject(preX, preY, player.getFootPositionX(), player.getFootPositionY());
                if (!isGotRings) {
                    SoundSystem.getInstance().playSequenceSe(50);
                }
            }
        }
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x, y, 1024, BarHorbinV.COLLISION_WIDTH);
    }

    public void turn() {
        this.degree = RollPlatformSpeedC.DEGREE_VELOCITY - this.degree;
        this.velocity = -this.velocity;
        if (Math.abs(this.velocity) <= MAX_VELOCITY) {
            return;
        }
        if (this.velocity < 0) {
            this.velocity = PlayerObject.SHOOT_POWER;
        } else {
            this.velocity = MAX_VELOCITY;
        }
    }

    public void doInitWhileInCamera() {
        this.posX = this.posOriginalX;
        this.posY = this.posOriginalY;
        this.used = false;
        this.controlling = false;
        if (this.iLeft == 0) {
            this.degree = RollPlatformSpeedC.DEGREE_VELOCITY - DEGREE;
        } else {
            this.degree = DEGREE;
        }
    }

    public static void releaseAllResource() {
        hookImage2 = null;
    }
}
