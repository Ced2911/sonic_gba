package SonicGBA;

import Lib.MyAPI;
import com.sega.mobile.define.MDPhone;

public class HobinCal implements SonicDef {
    private static final int MAX_DISTANCE = 512;
    private static final int MAX_POWER = 800;
    private int degree;
    private int distance;
    private int power;
    private int timeCount;

    public void startHobin(int power, int degree, int time) {
        this.power = PlayerObject.SONIC_ATTACK_LEVEL_3_V0;
        while (degree < 0) {
            degree += MDPhone.SCREEN_WIDTH;
        }
        this.degree = degree % MDPhone.SCREEN_WIDTH;
        this.timeCount = 10;
    }

    public void logic() {
        if (this.timeCount > 0) {
            this.timeCount--;
        }
        if (this.timeCount > 0) {
            if (this.timeCount == 9) {
                this.distance = this.power;
            } else {
                this.distance = (-this.distance) >> 1;
            }
            if (this.timeCount == 1) {
                this.distance = 0;
                this.power = 0;
            }
        }
    }

    public int getPosOffsetX() {
        return (this.distance * MyAPI.dCos(this.degree)) / 100;
    }

    public int getPosOffsetY() {
        return (this.distance * MyAPI.dSin(this.degree)) / 100;
    }

    public boolean isStop() {
        return this.timeCount == 0;
    }
}
