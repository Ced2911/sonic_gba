package SonicGBA;

import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: EnemyObject */
class CaterpillarBody extends EnemyObject {
    private static final int COLLISION_HEIGHT = 1024;
    private static final int COLLISION_WIDTH = 1024;
    private Caterpillar controller;
    private boolean isHead = false;

    protected CaterpillarBody(int id, int x, int y, int left, int top, int width, int height, boolean IsHead, Caterpillar controller) {
        super(id, x, y, left, top, width, height);
        this.isHead = IsHead;
        this.controller = controller;
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (!this.dead) {
            if (this.controller.dead) {
                this.dead = true;
            } else if (object != player) {
            } else {
                if (!this.isHead) {
                    player.beHurt();
                } else if (player.isAttackingEnemy()) {
                    player.doAttackPose(this, direction);
                    beAttack();
                    this.controller.setDead();
                    this.dead = true;
                } else {
                    player.beHurt();
                }
            }
        }
    }

    public void doWhileBeAttack(PlayerObject object, int direction, int animationID) {
        if (!this.dead) {
            if (this.controller.dead) {
                this.dead = true;
            } else if (object != player) {
            } else {
                if (this.isHead) {
                    player.doAttackPose(this, direction);
                    beAttack();
                    this.controller.setDead();
                    this.dead = true;
                    return;
                }
                player.beHurt();
            }
        }
    }

    public void logic() {
    }

    public void logic(int x, int y) {
        if (!this.dead) {
            if (this.controller.dead) {
                this.dead = true;
            }
            int preX = this.posX;
            int preY = this.posY;
            this.posX = x;
            this.posY = y;
            checkWithPlayer(preX, preY, this.posX, this.posY);
        }
    }

    public void draw(MFGraphics g) {
        if (!this.dead) {
            drawCollisionRect(g);
        }
    }

    public void refreshCollisionRect(int x, int y) {
        if (!this.dead) {
            this.collisionRect.setRect(x - 512, y - 512, 1024, 1024);
        }
    }
}
