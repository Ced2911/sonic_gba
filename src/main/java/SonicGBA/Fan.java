package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: GimmickObject */
class Fan extends GimmickObject {
    private static Animation animation;
    private AnimationDrawer drawer;

    protected Fan(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        if (animation == null) {
            animation = new Animation("/animation/bigfan");
        }
        if (animation != null) {
            this.drawer = animation.getDrawer(0, true, 0);
        }
    }

    public void draw(MFGraphics g) {
        drawInMap(g, this.drawer);
        drawCollisionRect(g);
    }

    public void close() {
        this.drawer = null;
    }

    public static void releaseAllResource() {
        Animation.closeAnimation(animation);
        animation = null;
    }
}
