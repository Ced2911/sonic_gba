package SonicGBA;

import GameEngine.Key;
import Lib.Animation;
import Lib.AnimationDrawer;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: GimmickObject */
class RollIsland extends GimmickObject {
    private static final int COLLISION_HEIGHT = 1792;
    private static final int COLLISION_WIDTH = 1792;
    public static final int[][] DIRECTION;
    private static final int MAX_VELOCITY = 1280;
    private static final int MOVE_POWER = 44;
    private static final int ROLL_MAX_VELOCITY = 256;
    private static final int VELOCITY = 250;
    private static final int VELOCITY_X = 200;
    private static Animation animation = null;
    private boolean controlling;
    private AnimationDrawer drawer;
    private int frame;
    private boolean isActive;
    private int moveDistance;
    private byte noCollisionCount;
    private int posOriginalX;
    private int posOriginalY;
    private int velocity;

    static {
        DIRECTION =new int[8][];
    /*
        r0 = new int[8][];
        int[] iArr = new int[]{1, iArr};
        r0[1] = new int[]{1, 1};
        iArr = new int[]{1, iArr};
        r0[3] = new int[]{-1, 1};
        int[] iArr2 = new int[]{-1, iArr2};
        r0[5] = new int[]{-1, -1};
        iArr2 = new int[]{-1, iArr2};
        r0[7] = new int[]{1, -1};
        DIRECTION = r0;
*/
    }
    protected RollIsland(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        this.posX += 256;
        this.posY += 256;
        if (animation == null) {
            animation = new Animation("/animation/roll_island");
        }
        this.drawer = animation.getDrawer(0, true, 0);
        this.moveDistance = ((width * 42) * 4) + (top * 4);
        if (this.iLeft % 2 == 1) {
            this.moveDistance >>= 1;
        }
        this.posOriginalX = this.posX;
        this.posOriginalY = this.posY;
        this.controlling = false;
        this.isActive = false;
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 896, y - 896, 1792, 1792);
    }

    public int getPaintLayer() {
        return 0;
    }

    public void logic() {
        if (this.noCollisionCount > (byte) 0) {
            this.noCollisionCount = (byte) (this.noCollisionCount - 1);
        }
        if (player.outOfControl && player.outOfControlObject == this) {
            if (Key.repeat(Key.gRight)) {
                this.velocity += 44;
                if (this.velocity > MAX_VELOCITY) {
                    this.velocity = MAX_VELOCITY;
                }
                player.faceDirection = true;
            } else if (Key.repeat(Key.gLeft)) {
                this.velocity -= 44;
                if (this.velocity < -1280) {
                    this.velocity = -1280;
                }
                player.faceDirection = false;
            } else if (this.posX == this.posOriginalX && this.posY == this.posOriginalY) {
                player.spinLogic2();
                if (player instanceof PlayerAmy) {
                    player.dashRollingLogicCheck();
                }
                if (Math.abs(player.getVelX()) >= 64 && player.isAfterSpinDash) {
                    this.velocity = player.getVelX();
                    player.isAfterSpinDash = false;
                }
            } else {
                this.velocity -= 44;
            }
            rollLogic();
            int preX = player.getFootPositionX();
            int preY = player.getFootPositionY();
            player.setFootPositionX(this.posX);
            player.setFootPositionY(this.collisionRect.y0);
            player.checkWithObject(preX, preY, player.getFootPositionX(), player.getFootPositionY());
            if (player.outOfControl && player.outOfControlObject == this) {
                if (!Key.repeat(Key.gUp | 33554432)) {
                    if (player.getAnimationId() == 4) {
                        if (!spinCheck()) {
                            if (Math.abs(this.velocity) == 0) {
                                if (!Key.repeat(Key.gDown)) {
                                    player.setAnimationId(0);
                                }
                            } else if (Math.abs(this.velocity) < PlayerObject.SPEED_LIMIT_LEVEL_1) {
                                player.setAnimationId(1);
                            } else if (Math.abs(this.velocity) < PlayerObject.SPEED_LIMIT_LEVEL_2) {
                                player.setAnimationId(2);
                            } else {
                                player.setAnimationId(3);
                            }
                        }
                    } else if (Math.abs(this.velocity) == 0) {
                        if (!Key.repeat(Key.gDown)) {
                            player.setAnimationId(0);
                        }
                    } else if (Math.abs(this.velocity) < PlayerObject.SPEED_LIMIT_LEVEL_1) {
                        player.setAnimationId(1);
                    } else if (Math.abs(this.velocity) < PlayerObject.SPEED_LIMIT_LEVEL_2) {
                        player.setAnimationId(2);
                    } else {
                        player.setAnimationId(3);
                    }
                }
                if (Key.press(Key.gUp | 16777216) && !Key.repeat(Key.gDown)) {
                    player.outOfControl = false;
                    this.controlling = false;
                    if (player.getAnimationId() == 4) {
                        player.doJumpV();
                    } else {
                        player.doJump();
                    }
                    if (this.velocity > 0) {
                        player.setVelX(200);
                    } else if (this.velocity < 0) {
                        player.setVelX(-200);
                    } else {
                        player.setVelX(0);
                    }
                }
            }
            if (this.posX == this.posOriginalX || this.posY == this.posOriginalY) {
                player.lookUpCheck();
                return;
            }
            return;
        }
        this.velocity -= 308;
        rollLogic();
    }

    private boolean spinCheck() {
        boolean result = false;
        if (player.faceDirection && this.velocity > 0) {
            result = true;
        }
        if (player.faceDirection || this.velocity >= 0) {
            return result;
        }
        return true;
    }

    public boolean releaseWhileBeHurt() {
        this.noCollisionCount = (byte) 0;
        return true;
    }

    public void draw(MFGraphics g) {
        drawInMap(g, this.drawer);
        drawCollisionRect(g);
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (this.noCollisionCount <= (byte) 0) {
            object.beStop(0, direction, this);
            if (direction == 1) {
                object.setOutOfControl(this);
                this.controlling = true;
            }
            this.isActive = true;
        }
    }

    public void rollLogic() {
        int vx = ((DIRECTION[this.iLeft][0] * this.velocity) * 20) / 100;
        int vy = ((DIRECTION[this.iLeft][1] * this.velocity) * 20) / 100;
        if (vx > 0) {
            if (vx > 256) {
                vx = 256;
            }
        } else if (vx < 0 && vx < GimmickObject.PLATFORM_OFFSET_Y) {
            vx = GimmickObject.PLATFORM_OFFSET_Y;
        }
        if (vy > 0) {
            if (vy > 256) {
                vy = 256;
            }
        } else if (vy < 0 && vy < GimmickObject.PLATFORM_OFFSET_Y) {
            vy = GimmickObject.PLATFORM_OFFSET_Y;
        }
        this.posX += vx;
        this.posY += vy;
        if (this.velocity > 0) {
            if (DIRECTION[this.iLeft][0] > 0) {
                if (this.posX > this.posOriginalX + this.moveDistance) {
                    this.posX = this.posOriginalX + this.moveDistance;
                }
            } else if (DIRECTION[this.iLeft][0] < 0 && this.posX < this.posOriginalX - this.moveDistance) {
                this.posX = this.posOriginalX - this.moveDistance;
            }
            if (DIRECTION[this.iLeft][1] > 0) {
                if (this.posY > this.posOriginalY + this.moveDistance) {
                    this.posY = this.posOriginalY + this.moveDistance;
                }
            } else if (DIRECTION[this.iLeft][1] < 0 && this.posY < this.posOriginalY - this.moveDistance) {
                this.posY = this.posOriginalY - this.moveDistance;
            }
            this.drawer.setActionId(1);
        } else {
            if (DIRECTION[this.iLeft][0] > 0) {
                if (this.posX < this.posOriginalX) {
                    this.posX = this.posOriginalX;
                }
            } else if (DIRECTION[this.iLeft][0] < 0 && this.posX > this.posOriginalX) {
                this.posX = this.posOriginalX;
            }
            if (DIRECTION[this.iLeft][1] > 0) {
                if (this.posY < this.posOriginalY) {
                    this.posY = this.posOriginalY;
                }
            } else if (DIRECTION[this.iLeft][1] < 0 && this.posY > this.posOriginalY) {
                this.posY = this.posOriginalY;
            }
            this.drawer.setActionId(1);
        }
        this.drawer.setActionId(1);
        if (this.posX == this.posOriginalX && this.posY == this.posOriginalY) {
            this.velocity = 0;
            if (this.isActive) {
                soundInstance.stopLoopSe();
                this.isActive = false;
            }
        }
        if (this.velocity == 0) {
            this.drawer.setActionId(0);
        }
        if (!(this.posX == this.posOriginalX && this.posY == this.posOriginalY)) {
            this.frame++;
            if (Math.abs(this.velocity) != 0) {
                if (Math.abs(this.velocity) < 160) {
                    if (this.frame % 7 == 0) {
                        soundInstance.playSe(76);
                    }
                } else if (Math.abs(this.velocity) < 1020) {
                    if (this.frame % 4 == 0) {
                        soundInstance.playSe(77);
                    }
                } else if (this.frame % 2 == 0) {
                    soundInstance.playSe(78);
                }
            }
        }
        refreshCollisionRect(this.posX, this.posY);
    }

    public void close() {
        this.drawer = null;
    }

    public static void releaseAllResource() {
        Animation.closeAnimation(animation);
        animation = null;
    }
}
