package SonicGBA;

import com.sega.mobile.framework.device.MFGraphics;

public abstract class BackGroundManager implements SonicDef {
    public static final int DRAW_HEIGHT = MapManager.CAMERA_HEIGHT;
    public static final int DRAW_WIDTH = MapManager.CAMERA_WIDTH;
    public static final int DRAW_X = MapManager.CAMERA_OFFSET_X;
    public static final int DRAW_Y = MapManager.CAMERA_OFFSET_Y;
    public static final String MAP_FILE_PATH = "/map";
    public static int frame;
    private static BackGroundManager frontNatural;
    private static BackGroundManager instance;
    public static int stageId;

    public abstract void close();

    public abstract void draw(MFGraphics mFGraphics);

    public static void init(int id) {
        if (stageId != id) {
            if (instance != null) {
                instance.close();
                instance = null;
            }
            if (frontNatural != null) {
                frontNatural.close();
                frontNatural = null;
            }
        }
        stageId = id;
        switch (stageId) {
            case 0:
            case 1:
                instance = new BackManagerStage1();
                return;
            case 2:
            case 3:
                instance = new BackManagerStage2();
                if (stageId == 2) {
                    frontNatural = new FrontManagerStage2_1();
                    return;
                }
                return;
            case 4:
            case 5:
                instance = new BackManagerStage3();
                return;
            case 6:
            case 7:
                frontNatural = new SnowStage4();
                instance = new BackManagerStage4(stageId - 6);
                return;
            case 8:
            case 9:
                instance = new BackManagerStage5();
                return;
            case 10:
                instance = new BackManagerStage61();
                return;
            case 11:
                instance = new BackManagerStage62();
                return;
            case 12:
                instance = new BackManagerStageFinal();
                return;
            case 13:
                instance = new BackManagerStageExtra();
                return;
            default:
                return;
        }
    }

    public static void next() {
        if (instance != null) {
            instance.nextState();
        }
        if (frontNatural != null) {
            frontNatural.nextState();
        }
    }

    public static void drawBackGround(MFGraphics g) {
        if (instance != null) {
            instance.draw(g);
        }
    }

    public static void drawFrontNatural(MFGraphics g) {
        if (frontNatural != null) {
            frontNatural.draw(g);
        }
    }

    public void nextState() {
    }
}
