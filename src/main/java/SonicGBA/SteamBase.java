package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import Lib.SoundSystem;
import com.sega.mobile.define.MDPhone;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: GimmickObject */
class SteamBase extends GimmickObject {
    private static final int COLLISION_HEIGHT = 768;
    private static final int COLLISION_WIDTH = 1280;
    private static final int PLATFORM_OFFSET_Y = -768;
    private static Animation animation;
    private static byte count;
    private static boolean shotFlag;
    private AnimationDrawer drawer;
    public SteamHurt sh;
    public SteamPlatform sp;

    protected SteamBase(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        if (animation == null) {
            animation = new Animation("/animation/steam");
        }
        this.drawer = animation.getDrawer(0, true, 0);
        this.sh = new SteamHurt(this.posX, this.posY, this);
        this.sp = new SteamPlatform(this.posX, this.posY + PLATFORM_OFFSET_Y, this);
        GameObject.addGameObject(this.sh, this.posX, this.posY);
        GameObject.addGameObject(this.sp, this.posX, this.posY);
    }

    public static void staticLogic() {
        if (count > (byte) 0) {
            count = (byte) (count - 1);
        }
        if (count == (byte) 0) {
            SteamPlatform.shot();
            count = (byte) 50;
        }
        SteamPlatform.staticLogic();
    }

    public void draw(MFGraphics g) {
        this.drawer.setActionId(SteamPlatform.isShotting() ? 1 : 0);
        drawInMap(g, this.drawer);
        this.sp.drawPlatform(g);
        if (isInCamera() && count == (byte) 50) {
            SoundSystem.getInstance().playSe(53);
            count = (byte) 49;
        }
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - MDPhone.SCREEN_HEIGHT, y - 768, COLLISION_WIDTH, 768);
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        object.beStop(0, direction, this);
    }

    public void close() {
        this.drawer = null;
        this.sp = null;
        this.sh = null;
    }

    public static void releaseAllResource() {
        Animation.closeAnimation(animation);
        animation = null;
    }
}
