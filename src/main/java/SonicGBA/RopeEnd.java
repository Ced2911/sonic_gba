package SonicGBA;

/* compiled from: GimmickObject */
class RopeEnd extends GimmickObject {
    protected RopeEnd(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
    }

    public void doWhileRail(PlayerObject object, int direction) {
        if (object.outOfControl) {
            if (object.outOfControlObject instanceof RopeStart) {
                RopeStart rope = (RopeStart)object.outOfControlObject;
                rope.posX = this.posX;
                rope.posY = this.posY;
            }
            if (player.getFootPositionX() < this.posX) {
                player.setFootPositionX(this.posX);
            }
            player.setFootPositionY(this.collisionRect.y1);
            object.outOfControl = false;
            object.setCollisionState((byte) 1);
            object.stopMove();
            object.collisionChkBreak = true;
            object.railing = false;
            int i = object.velX;
        }
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x, y, 1024, BarHorbinV.COLLISION_WIDTH);
    }
}
