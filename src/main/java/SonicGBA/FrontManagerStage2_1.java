package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import Lib.MyAPI;
import com.sega.mobile.define.MDPhone;
import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;

/* compiled from: BackGroundManager */
class FrontManagerStage2_1 extends BackGroundManager {
    private static final int LIGHT_CIRCLE_1 = 32;
    private static final int LIGHT_CIRCLE_2 = 120;
    private static final int LIGHT_DEGREE_CENTER_1 = -90;
    private static final int LIGHT_DEGREE_CENTER_2 = -90;
    private static final int LIGHT_RANGE_1 = 40;
    private static final int LIGHT_RANGE_2 = 68;
    private int degree1 = 0;
    private int degree2 = 0;
    private AnimationDrawer drawer = Animation.getInstanceFromQi("/animation/searchlight.dat")[0].getDrawer();
    private MFImage screenMask = MFImage.createImage(SCREEN_WIDTH, SCREEN_HEIGHT);
    private int[] screenMaskRGB = new int[(SCREEN_WIDTH * SCREEN_HEIGHT)];

    public void close() {
        this.screenMask = null;
        this.drawer = null;
        this.screenMaskRGB = null;
    }

    public void draw(MFGraphics g) {
        if (MapManager.getCamera().f13x <= 1456) {
            if (!GameObject.IsGamePause) {
                this.degree1 += 11;
                this.degree1 %= MDPhone.SCREEN_WIDTH;
                this.degree2 += 3;
                this.degree2 %= MDPhone.SCREEN_WIDTH;
            }
            MFGraphics g2 = this.screenMask.getGraphics();
            g2.setColor(MapManager.END_COLOR);
            MyAPI.fillRect(g2, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
            g2.saveCanvas();
            g2.translateCanvas((SCREEN_WIDTH >> 1) - 60, (SCREEN_HEIGHT >> 1) + 119);
            g2.rotateCanvas((float) (((MyAPI.dSin(this.degree1) * 40) / 100) + 0));
            this.drawer.draw(g2, 0, 0, 0, false, 0);
            g2.restoreCanvas();
            g2.saveCanvas();
            g2.translateCanvas((SCREEN_WIDTH >> 1) + 80, (SCREEN_HEIGHT >> 1) + 161);
            g2.rotateCanvas((float) (((MyAPI.dSin(this.degree2) * LIGHT_RANGE_2) / 100) + 0));
            this.drawer.draw(g2, 1, 0, 0, false, 0);
            g2.restoreCanvas();
            this.screenMask.getRGB(this.screenMaskRGB, 0, SCREEN_WIDTH, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
            for (int i = 0; i < this.screenMaskRGB.length; i++) {
                if (this.screenMaskRGB[i] == -1) {
                    this.screenMaskRGB[i] = Integer.MIN_VALUE;
                } else {
                    this.screenMaskRGB[i] = 0;
                }
            }
            g.drawRGB(this.screenMaskRGB, 0, SCREEN_WIDTH, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, true);
        }
    }
}
