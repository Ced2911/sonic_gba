package SonicGBA;

import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: EnemyObject */
class BossF1Ball extends EnemyObject {
    private static final int COLLISION_HEIGHT = 2048;
    private static final int COLLISION_WIDTH = 2048;
    private boolean isPlayerHurt;

    protected BossF1Ball(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        this.isPlayerHurt = false;
        this.posX = 0;
        this.posY = 0;
        this.isPlayerHurt = false;
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (!this.dead && object == player) {
            player.beHurt();
            this.isPlayerHurt = true;
        }
    }

    public void doWhileBeAttack(PlayerObject object, int direction, int animationID) {
    }

    public void doWhileNoCollision() {
        this.isPlayerHurt = false;
    }

    public void setEnd() {
        this.dead = true;
    }

    public boolean getPlayerHurt() {
        return this.isPlayerHurt;
    }

    public void resetPlayerHurt() {
        this.isPlayerHurt = false;
    }

    public void logic(int x, int y) {
        this.posX = x;
        this.posY = y;
        int preX = this.posX;
        int preY = this.posY;
        refreshCollisionRect(this.posX, this.posY);
        checkWithPlayer(preX, preY, this.posX, this.posY);
    }

    public void draw(MFGraphics g) {
        drawCollisionRect(g);
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 1024, y - 1024, 2048, 2048);
    }

    public void logic() {
    }
}
