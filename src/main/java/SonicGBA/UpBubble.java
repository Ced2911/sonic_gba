package SonicGBA;

import GameEngine.Def;
import Lib.Animation;
import Lib.AnimationDrawer;
import Lib.MyRandom;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: GimmickObject */
class UpBubble extends GimmickObject {
    private static Animation bubbleAnimation;
    private final int LIFE_RANGE = 6400;
    private int direct = 0;
    private AnimationDrawer drawer;
    private int[] group;
    private boolean initFlag;
    private int posOriginalY;
    private int velx = 0;
    private int vely = Def.TOUCH_CHARACTER_SELECT_LEFT_ARROW_OFFSET_X;

    protected UpBubble(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        int[] iArr = new int[12];
        iArr[1] = 1;
        iArr[2] = 2;
        iArr[4] = 1;
        iArr[5] = 2;
        iArr[7] = 1;
        iArr[8] = 2;
        iArr[10] = 1;
        iArr[11] = 2;
        this.group = iArr;
    }

    public UpBubble(int x, int y) {
        super(0, x, y, 0, 0, 0, 0);
        int[] iArr = new int[12];
        iArr[1] = 1;
        iArr[2] = 2;
        iArr[4] = 1;
        iArr[5] = 2;
        iArr[7] = 1;
        iArr[8] = 2;
        iArr[10] = 1;
        iArr[11] = 2;
        this.group = iArr;
        if (bubbleAnimation == null) {
            bubbleAnimation = new Animation("/animation/bubble_up");
        }
        if (bubbleAnimation != null) {
            this.drawer = bubbleAnimation.getDrawer(this.group[MyRandom.nextInt(this.group.length)], true, 0);
            this.direct = MyRandom.nextInt(-1, 1);
        }
        this.posOriginalY = this.posY;
        this.initFlag = false;
    }

    public int getPaintLayer() {
        return 0;
    }

    public void logic() {
        if (!this.initFlag) {
            if (this.direct >= 0) {
                this.velx = MyRandom.nextInt(0, 20);
            } else {
                this.velx = MyRandom.nextInt(-20, 0);
            }
            this.posX += this.velx;
            this.posY += this.vely;
            refreshCollisionRect(this.posX, this.posY);
            if (this.posY <= (StageManager.getWaterLevel() << 6)) {
                this.initFlag = true;
            }
        }
    }

    public boolean IsDie() {
        return this.initFlag;
    }

    public void draw(MFGraphics g) {
        if (!this.initFlag) {
            drawInMap(g, this.drawer, this.posX, this.posY);
        }
    }

    public void close() {
        this.drawer = null;
    }

    public static void releaseAllResource() {
        Animation.closeAnimation(bubbleAnimation);
        bubbleAnimation = null;
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 5, y - 5, 10, 10);
    }

    public boolean objectChkDestroy() {
        return this.initFlag;
    }
}
