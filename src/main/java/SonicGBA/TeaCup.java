package SonicGBA;

import Lib.MyAPI;
import Lib.SoundSystem;
import Lib.crlFP32;
import com.sega.mobile.define.MDPhone;
import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;

/* compiled from: GimmickObject */
class TeaCup extends GimmickObject {
    private static final int COLLISION_HEIGHT = 2560;
    private static final int COLLISION_OFFSET_Y = -1536;
    private static final int COLLISION_WIDTH = 6144;
    private static final int COLLISION_Y1_ORIBINAL = 768;
    private static final int COLLISION_Y1_ROLLING_2 = 256;
    private static final int MAX_RADIUS = 2240;
    private static final int MIN_RADIUS = 320;
    private static final int MOVE_FRAME = 37;
    private static final int MOVE_LENGTH = 7168;
    private static final int MOVE_SPEED = 192;
    private static final int ROLLING_2_COUNT = 16;
    private static final byte STATE_ROLLING = (byte) 1;
    private static final byte STATE_ROLLING_2 = (byte) 2;
    private static final byte STATE_SHOOT = (byte) 3;
    private static final byte STATE_STAY = (byte) 0;
    private static MFImage teacupImage;
    private int changeTime;
    private int changeTimeCount;
    private int collisionHeight;
    private int count;
    private int degreeSpeed;
    private byte drawCount;
    private int playerDegree;
    private int playerX;
    private int prePosY;
    private byte state;

    protected TeaCup(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        if (teacupImage == null) {
            try {
                teacupImage = MFImage.createImage("/gimmick/teacup_" + StageManager.getCurrentZoneId() + ".png");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        this.changeTime = 0;
        this.state = (byte) 0;
        this.degreeSpeed = 0;
    }

    public void logic() {
        if (!player.isDead) {
            if (this.changeTime > 0) {
                this.changeTimeCount++;
                if (this.changeTimeCount >= this.changeTime) {
                    this.drawCount = (byte) ((this.drawCount + 1) % 2);
                    this.changeTimeCount = 0;
                }
            }
            if (this.count > 0) {
                this.count--;
            }
            switch (this.state) {
                case (byte) 0:
                    this.collisionHeight = 768;
                    return;
                case (byte) 1:
                    player.setNoKey();
                    player.setAnimationId(4);
                    this.playerDegree += this.degreeSpeed;
                    this.playerDegree %= MDPhone.SCREEN_WIDTH;
                    player.setFootPositionX(this.posX + (((MAX_RADIUS - (((this.posY - this.prePosY) * 1920) / MOVE_LENGTH)) * MyAPI.dSin(this.playerDegree)) / 100));
                    checkWithPlayer(this.posX, this.posY, this.posX, this.posY + 192);
                    this.posY += 192;
                    if (this.posY >= this.prePosY + MOVE_LENGTH) {
                        this.posY = this.prePosY + MOVE_LENGTH;
                        player.setFootPositionX(this.posX);
                        this.state = (byte) 2;
                        this.count = 16;
                        this.changeTime = 1;
                        this.collisionHeight = 256;
                    }
                    this.degreeSpeed = (((this.posY - this.prePosY) * 60) / MOVE_LENGTH) + 0;
                    return;
                case (byte) 2:
                    player.setNoKey();
                    player.setAnimationId(4);
                    this.playerDegree += this.degreeSpeed;
                    this.playerDegree %= MDPhone.SCREEN_WIDTH;
                    player.setFootPositionX(this.posX + ((MyAPI.dSin(this.playerDegree) * MIN_RADIUS) / 100));
                    checkWithPlayer(this.posX, this.posY, this.posX, this.posY);
                    if (this.count == 0) {
                        player.setFootPositionX(this.posX);
                        this.state = (byte) 3;
                        this.changeTime = 2;
                        PlayerObject playerObject = player;
                        PlayerObject playerObject2 = player;
                        playerObject.beSpring(-PlayerObject.SHOOT_POWER, 0);
                        player.isPowerShoot = true;
                        player.collisionState = (byte) 1;
                        player.setAnimationId(4);
                        return;
                    }
                    return;
                case (byte) 3:
                    if (this.changeTime > 0 && this.changeTimeCount == 0) {
                        this.changeTime++;
                        if (this.changeTime > 6) {
                            this.changeTime = 0;
                            SoundSystem.getInstance().stopLongSe();
                            return;
                        }
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (object == player) {
            switch (this.state) {
                case (byte) 0:
                    this.playerX = player.getFootPositionX() - this.posX;
                    if (this.playerX > MAX_RADIUS) {
                        this.playerX = MAX_RADIUS;
                    } else if (this.playerX < -2240) {
                        this.playerX = -2240;
                    }
                    this.playerDegree = crlFP32.actTanDegree(this.playerX, crlFP32.sqrt(5017600 - (this.playerX * this.playerX)) >> 3);
                    object.beStop(this.collisionRect.y0, 1, this);
                    object.setNoKey();
                    this.prePosY = this.posY;
                    this.state = (byte) 1;
                    this.changeTime = 2;
                    SoundSystem.getInstance().playLongSe(72);
                    return;
                default:
                    return;
            }
        }
    }

    public int getPaintLayer() {
        return 2;
    }

    public void draw(MFGraphics g) {
        drawInMap(g, teacupImage, 0, this.drawCount * 40, 96, 40, 0, this.posX, this.posY + COLLISION_OFFSET_Y, 17);
        drawCollisionRect(g);
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 3072, y - this.collisionHeight, COLLISION_WIDTH, 1024);
    }

    public void close() {
    }

    public static void releaseAllResource() {
        teacupImage = null;
    }
}
