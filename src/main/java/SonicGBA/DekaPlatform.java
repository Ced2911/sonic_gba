package SonicGBA;

import Lib.MyAPI;
import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;

/* compiled from: GimmickObject */
class DekaPlatform extends GimmickObject {
    private static final int VELOCITY = 250;
    private static MFImage image;
    private static MFImage image2;
    private int COLLISION_HEIGHT1;
    private int COLLISION_HEIGHT2;
    private int COLLISION_WIDTH1;
    private int COLLISION_WIDTH2;
    private int initPos;
    private boolean isActived = false;
    private boolean isDirectionDown;
    private boolean isH;
    private int lastDirection = 4;
    private MoveCalculator mCalc;
    private int offset_distance;

    protected DekaPlatform(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        boolean moveDirection;
        int i;
        if (this.mWidth >= this.mHeight) {
            this.isH = true;
            if (this.iLeft == 0) {
                moveDirection = false;
            } else {
                moveDirection = true;
            }
        } else {
            this.isH = false;
            if (this.iTop == 0) {
                moveDirection = false;
            } else {
                moveDirection = true;
            }
        }
        this.mCalc = new MoveCalculator(this.isH ? this.posX : this.posY, this.isH ? this.mWidth : this.mHeight, moveDirection);
        if (this.isH) {
            i = this.posX;
        } else {
            i = this.posY;
        }
        this.initPos = i;
        if (StageManager.getCurrentZoneId() != 6) {
            if (image == null) {
                image = MFImage.createImage("/gimmick/deka_platform_" + StageManager.getCurrentZoneId() + ".png");
            }
            this.COLLISION_WIDTH1 = MyAPI.zoomIn(image.getWidth()) << 6;
            this.COLLISION_HEIGHT1 = MyAPI.zoomIn(image.getHeight()) << 6;
        } else if (this.iTop == 1) {
            if (image == null) {
                image = MFImage.createImage("/gimmick/deka_platform_" + StageManager.getCurrentZoneId() + (StageManager.getStageID() - 9) + "_2.png");
            }
            this.COLLISION_WIDTH1 = MyAPI.zoomIn(image.getWidth()) << 6;
            this.COLLISION_HEIGHT1 = MyAPI.zoomIn(image.getHeight()) << 6;
        } else {
            if (image2 == null) {
                image2 = MFImage.createImage("/gimmick/deka_platform_" + StageManager.getCurrentZoneId() + (StageManager.getStageID() - 9) + ".png");
            }
            this.COLLISION_WIDTH2 = MyAPI.zoomIn(image2.getWidth()) << 6;
            this.COLLISION_HEIGHT2 = MyAPI.zoomIn(image2.getHeight()) << 6;
        }
        if (StageManager.getCurrentZoneId() == 5 && this.iTop == 0 && this.iLeft == 0) {
            if (image2 == null) {
                try {
                    image2 = MFImage.createImage("/gimmick/deka_platform_" + StageManager.getCurrentZoneId() + "_2.png");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            this.COLLISION_WIDTH1 = MyAPI.zoomIn(image2.getWidth()) << 6;
            this.COLLISION_HEIGHT1 = MyAPI.zoomIn(image2.getHeight()) << 6;
        }
    }

    public void logic() {
        this.mCalc.logic();
        int preX = this.posX;
        int preY = this.posY;
        if (this.isH) {
            if (this.iLeft == 0) {
                this.posX = this.mCalc.getPosition();
            } else {
                this.offset_distance = this.initPos - this.mCalc.getPosition();
                this.posX = this.initPos + this.offset_distance;
            }
        } else if (this.iTop == 0) {
            this.posY = this.mCalc.getPosition();
        } else {
            this.offset_distance = this.initPos - this.mCalc.getPosition();
            this.posY = this.initPos + this.offset_distance;
        }
        if (!player.isFootOnObject(this) || this.worldInstance.getWorldY(player.collisionRect.x0, player.collisionRect.y0, 1, 0) == -1000 || this.worldInstance.getWorldY(player.collisionRect.x1, player.collisionRect.y0, 1, 0) == -1000) {
            checkWithPlayer(preX, preY, this.posX, this.posY);
        } else {
            player.setDie(false);
        }
    }

    public void draw(MFGraphics g) {
        if (StageManager.getCurrentZoneId() == 5 && this.iTop == 0 && this.iLeft == 0) {
            drawInMap(g, image2, 3);
        } else if (StageManager.getCurrentZoneId() != 6) {
            drawInMap(g, image, 3);
        } else if (this.iTop == 1) {
            drawInMap(g, image, 3);
        } else {
            drawInMap(g, image2, 3);
        }
    }

    public void refreshCollisionRect(int x, int y) {
        if (StageManager.getCurrentZoneId() != 6) {
            this.collisionRect.setRect(x - (this.COLLISION_WIDTH1 >> 1), y - (this.COLLISION_HEIGHT1 >> 1), this.COLLISION_WIDTH1, this.COLLISION_HEIGHT1);
        } else if (this.iTop == 1) {
            this.collisionRect.setRect(x - (this.COLLISION_WIDTH1 >> 1), y - (this.COLLISION_HEIGHT1 >> 1), this.COLLISION_WIDTH1, this.COLLISION_HEIGHT1);
        } else {
            this.collisionRect.setRect(x - (this.COLLISION_WIDTH2 >> 1), y - (this.COLLISION_HEIGHT2 >> 1), this.COLLISION_WIDTH2, this.COLLISION_HEIGHT2);
        }
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (direction == 4) {
            direction = this.lastDirection;
        } else {
            this.lastDirection = direction;
        }
        if (direction == 1) {
            this.isDirectionDown = true;
        } else {
            this.isDirectionDown = false;
        }
        if (!((object instanceof PlayerAmy) && ((PlayerAmy) object).skipBeStop)) {
            if (direction == 4 && object.collisionRect.x0 < this.collisionRect.x1 && object.collisionRect.x1 > this.collisionRect.x1) {
                direction = 2;
            }
            if (((object.velX >= 0 || direction != 3) && (object.velX <= 0 || direction != 2)) || ((object.collisionRect.x1 + object.velX) + (1024 / 2) >= this.collisionRect.x0 && (object.collisionRect.x0 + object.velX) - (1024 / 2) <= this.collisionRect.x1)) {
                object.beStop(0, direction, this, this.isDirectionDown);
            } else {
                object.posX += object.velX;
            }
        }
        if (direction == 3 || direction == 2) {
            this.isActived = true;
            player.isSidePushed = direction;
            if (player.movedSpeedX > 0) {
                player.movedSpeedX = 1;
            } else if (player.movedSpeedX < 0) {
                player.movedSpeedX = -1;
            }
        }
    }

    public void doWhileNoCollision() {
        this.lastDirection = 4;
        if (this.isActived) {
            player.movedSpeedX = 0;
            this.isActived = false;
        }
        if (player.isSidePushed != 4) {
            player.isSidePushed = 4;
        }
    }

    public void close() {
        this.mCalc = null;
    }

    public static void releaseAllResource() {
        image = null;
        image2 = null;
    }

    public int getPaintLayer() {
        return 0;
    }
}
