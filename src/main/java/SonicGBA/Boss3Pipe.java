package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: EnemyObject */
class Boss3Pipe extends EnemyObject {
    private static final int COLLISION_HEIGHT = 2048;
    private static final int COLLISION_WIDTH = 3584;
    private static Animation pipeAni;
    private boolean IsNeedCollision = false;
    private boolean IsNeedDraw = true;
    private int drawLayer = 0;
    private AnimationDrawer pipedrawer;

    protected Boss3Pipe(int id, int x, int y, boolean isCollision, int direction, int layer) {
        super(id, x, y, 0, 0, 0, 0);
        this.IsNeedCollision = isCollision;
        this.drawLayer = layer;
        if (pipeAni == null) {
            pipeAni = new Animation("/animation/boss3_pipe");
        }
        this.pipedrawer = pipeAni.getDrawer();
        switch (direction) {
            case 0:
                this.pipedrawer.setTrans(1);
                return;
            case 1:
                this.pipedrawer.setTrans(0);
                return;
            case 2:
                this.pipedrawer.setTrans(7);
                return;
            case 3:
                this.pipedrawer.setTrans(6);
                return;
            default:
                return;
        }
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (this.IsNeedCollision && object == player) {
            object.beStop(0, direction, this);
        }
    }

    public void doWhileBeAttack(PlayerObject object, int direction, int animationID) {
        switch (direction) {
            case 2:
            case 3:
                object.isCrashPipe = true;
                return;
            default:
                object.isCrashPipe = false;
                return;
        }
    }

    public void setisDraw(boolean state) {
        this.IsNeedDraw = state;
    }

    public void logic() {
    }

    public int getPaintLayer() {
        return this.drawLayer;
    }

    public void logic(int posx, int posy) {
        int preX = this.posX;
        int preY = this.posY;
        this.posX = posx;
        this.posY = posy;
        refreshCollisionRect(this.posX, this.posY);
        checkWithPlayer(preX, preY, this.posX, this.posY);
    }

    public void draw(MFGraphics g) {
        refreshCollisionRect(this.posX, this.posY);
        if (this.IsNeedDraw) {
            drawInMap(g, this.pipedrawer, this.posX, this.posY);
        }
        drawCollisionRect(g);
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 1792, y - 2048, COLLISION_WIDTH, 2048);
    }
}
