package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import Lib.SoundSystem;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: GimmickObject */
class Hari extends GimmickObject {
    public static final int IMAGE_COLLISION_OFFSET = 192;
    private static final int OFFFSET = 128;
    private static Animation hariAnimation;
    private AnimationDrawer drawer;
    private int firstCollisionDirection = 4;
    private byte hariId;

    protected Hari(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        if (hariAnimation == null) {
            hariAnimation = new Animation("/animation/se_hari");
        }
        this.hariId = (byte) (id - 1);
        this.drawer = hariAnimation.getDrawer(this.hariId, true, 0);
        this.mWidth = this.drawer.getCurrentFrameWidth() << 6;
        this.mHeight = this.drawer.getCurrentFrameHeight() << 6;
        this.drawer.setPause(true);
    }

    public void draw(MFGraphics g) {
        drawInMap(g, this.drawer, this.posX, this.posY);
        drawCollisionRect(g);
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        PlayerObject playerObject = null;
        if (this.firstCollisionDirection != 4 && direction == 4) {
            direction = this.firstCollisionDirection;
        }
        if (this.firstCollisionDirection == 4 && direction != 4) {
            this.firstCollisionDirection = direction;
        }
        switch (this.objId) {
            case 5:
            case 6:
                if (this.drawer.getCurrentFrameHeight() != 0) {
                    object.beStop(this.collisionRect.x0, direction, this);
                    break;
                }
                break;
            default:
                object.beStop(this.collisionRect.x0, direction, this);
                break;
        }
        if ((player instanceof PlayerKnuckles) && direction == 4 && player.canAttackByHari) {
            playerObject = player;
            if (PlayerObject.getRingNum() > 0) {
                SoundSystem.getInstance().playSe(43);
            }
            player.beHurt();
            player.canAttackByHari = false;
            player.beAttackByHari = true;
        }
        if (object == player && player.canBeHurt()) {
            switch (this.objId) {
                case 1:
                case 5:
                    if (direction == 1 && this.drawer.getCurrentFrameHeight() != 0) {
                        playerObject = player;
                        if (PlayerObject.getRingNum() > 0) {
                            SoundSystem.getInstance().playSe(43);
                        }
                        player.beHurt();
                        player.beAttackByHari = true;
                        return;
                    }
                    return;
                case 2:
                case 6:
                    if (direction == 0 && this.drawer.getCurrentFrameHeight() != 0) {
                        playerObject = player;
                        if (PlayerObject.getRingNum() > 0) {
                            SoundSystem.getInstance().playSe(43);
                        }
                        player.beHurt();
                        player.beAttackByHari = true;
                        return;
                    }
                    return;
                case 3:
                    if (direction == 3 && player.velX > 0) {
                        playerObject = player;
                        if (PlayerObject.getRingNum() > 0) {
                            SoundSystem.getInstance().playSe(43);
                        }
                        player.beHurt();
                        player.beAttackByHari = true;
                        return;
                    } else if (direction == 3 && player.getAnimationId() == 0) {
                        playerObject = player;
                        if (PlayerObject.getRingNum() > 0) {
                            SoundSystem.getInstance().playSe(43);
                        }
                        player.beHurt();
                        player.beAttackByHari = true;
                        return;
                    } else {
                        return;
                    }
                case 4:
                    if (direction == 2 && player.velX < 0) {
                        playerObject = player;
                        if (PlayerObject.getRingNum() > 0) {
                            SoundSystem.getInstance().playSe(43);
                        }
                        player.beHurt();
                        player.beAttackByHari = true;
                        return;
                    } else if (direction == 2 && player.getAnimationId() == 0) {
                        playerObject = player;
                        if (PlayerObject.getRingNum() > 0) {
                            SoundSystem.getInstance().playSe(43);
                        }
                        player.beHurt();
                        player.beAttackByHari = true;
                        return;
                    } else {
                        return;
                    }
                default:
                    return;
            }
        }
    }

    public void doWhileNoCollision() {
        this.firstCollisionDirection = 4;
    }

    public void logic() {
        this.drawer.moveOn();
        checkWithPlayer(this.posX, this.posY, this.posX, this.posY);
    }

    public void refreshCollisionRect(int x, int y) {
        switch (this.objId) {
            case 1:
            case 5:
                this.collisionRect.setRect(x - (this.mWidth >> 1), ((y + 128) - (this.drawer.getCurrentFrameHeight() << 6)) + 192, this.mWidth, (this.drawer.getCurrentFrameHeight() << 6) - 192);
                return;
            case 2:
            case 6:
                this.collisionRect.setRect(x - (this.mWidth >> 1), y - 128, this.mWidth, (this.drawer.getCurrentFrameHeight() << 6) - 192);
                return;
            case 3:
                this.collisionRect.setRect((x - (this.drawer.getCurrentFrameWidth() << 6)) + 128, (y + 128) - (this.drawer.getCurrentFrameHeight() << 6), this.drawer.getCurrentFrameWidth() << 6, this.drawer.getCurrentFrameHeight() << 6);
                return;
            case 4:
                this.collisionRect.setRect(x - 128, (y + 128) - (this.drawer.getCurrentFrameHeight() << 6), this.drawer.getCurrentFrameWidth() << 6, this.drawer.getCurrentFrameHeight() << 6);
                return;
            default:
                return;
        }
    }

    public void close() {
        this.drawer = null;
    }

    public int getPaintLayer() {
        return 0;
    }

    public static void releaseAllResource() {
        Animation.closeAnimation(hariAnimation);
        hariAnimation = null;
    }
}
