package SonicGBA;

import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: GimmickObject */
class UpPlatform extends Platform {
    private boolean initFlag = false;
    private int offsetY = 256;
    private int posOriginalY = this.posY;
    private int velocity = 128;

    protected UpPlatform(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
    }

    public void logic() {
        int preX = this.posX;
        int preY = this.posY;
        if (this.initFlag) {
            refreshCollisionRect(this.posX, this.posY);
            if (!screenRect.collisionChk(this.collisionRect)) {
                this.initFlag = false;
                return;
            }
            return;
        }
        if (player.isFootOnObject(this)) {
            checkWithPlayer(this.posX, this.posY, this.posX, this.posY + this.velocity);
            this.posY -= this.velocity;
        } else {
            checkWithPlayer(this.posX, this.posY, this.posX, this.posY + this.velocity);
            if (this.posY < this.posOriginalY) {
                this.posY += this.velocity;
            } else {
                this.posY = this.posOriginalY;
            }
        }
        checkWithPlayer(preX, preY, this.posX, this.posY);
    }

    public void doInitWhileInCamera() {
        this.posY = this.posOriginalY;
        this.used = false;
        this.initFlag = true;
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (!this.initFlag) {
            if (!(!player.isFootOnObject(this) || this.worldInstance.getWorldY(object.collisionRect.x0, object.footPointY - 1536, 1, 0) == -1000 || this.worldInstance.getWorldY(object.collisionRect.x1, object.footPointY - 1536, 1, 0) == -1000)) {
                object.setDie(false);
            }
            super.doWhileCollision(object, direction);
        }
    }

    public void draw(MFGraphics g) {
        if (!this.initFlag) {
            drawInMap(g, platformImage, this.posX, (this.posY + 768) + this.offsetY, 33);
        }
    }
}
