package SonicGBA;

import Lib.MyAPI;
import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;

/* compiled from: GimmickObject */
class ShipSystem extends GimmickObject {
    private static final int RADIUS = 5632;
    private static final int RING_NUM = 6;
    private int degree = 11520;
    private int lineVelocity = 0;
    private GimmickObject ship;

    protected ShipSystem(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        this.ship = new Ship(id, x, y, this);
        GameObject.addGameObject(this.ship, this.posX, this.posY);
        GameObject.addGameObject(new ShipBase(x, y), this.posX, this.posY);
        if (shipRingImage == null) {
            try {
                shipRingImage = MFImage.createImage("/gimmick/ship_ring.png");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void draw(MFGraphics g) {
        for (int i = 0; i < 6; i++) {
            drawInMap(g, shipRingImage, this.posX + ((((i * RADIUS) / 5) * MyAPI.dCos(this.degree >> 6)) / 100), this.posY + ((((i * RADIUS) / 5) * MyAPI.dSin(this.degree >> 6)) / 100), 3);
        }
    }

    public void getNewShipPosition(Ship ship) {
        this.lineVelocity += (((GRAVITY * 2) * MyAPI.dSin((this.degree >> 6) - 90)) / 100) / 3;
        this.degree -= ((((this.lineVelocity << 6) / RADIUS) << 6) * RollPlatformSpeedC.DEGREE_VELOCITY) / SonicDef.PI;
        int preX = ship.posX;
        int preY = ship.posY;
        ship.posX = this.posX + ((MyAPI.dCos(this.degree >> 6) * RADIUS) / 100);
        ship.posY = this.posY + ((MyAPI.dSin(this.degree >> 6) * RADIUS) / 100);
        ship.checkWithPlayer(preX, preY, ship.posX, ship.posY);
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - RADIUS, y, 11264, RADIUS);
    }

    public void close() {
        this.ship = null;
    }

    public static void releaseAllResource() {
    }

    public int getPaintLayer() {
        return 0;
    }
}
