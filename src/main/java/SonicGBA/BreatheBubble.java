package SonicGBA;

import GameEngine.Def;
import Lib.Animation;
import Lib.AnimationDrawer;
import Lib.MyRandom;
import Lib.SoundSystem;
import com.sega.mobile.define.MDPhone;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: GimmickObject */
class BreatheBubble extends UpBubble {
    private static final int BUBBLE_LIFE = 8;
    private static final int BUBBLE_RANGE = 1280;
    private static Animation bubbleAnimation;
    private boolean CanBreathe = false;
    private final int LIFE_RANGE = 6400;
    private int breathCnt = 0;
    private int direct = 0;
    private AnimationDrawer drawer;
    private boolean initFlag;
    private boolean isBeginPlayStageBGM = false;
    private boolean isFirstUp = true;
    private int posOriginalY;
    private int velx = 0;
    private int vely = Def.TOUCH_CHARACTER_SELECT_LEFT_ARROW_OFFSET_X;

    protected BreatheBubble(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
    }

    public BreatheBubble(int x, int y) {
        super(0, x, y, 0, 0, 0, 0);
        if (bubbleAnimation == null) {
            bubbleAnimation = new Animation("/animation/bubble_up");
        }
        if (bubbleAnimation != null) {
            this.drawer = bubbleAnimation.getDrawer(3, false, 0);
            this.direct = MyRandom.nextInt(-1, 1);
        }
        this.posOriginalY = this.posY;
        this.initFlag = false;
        this.CanBreathe = false;
        this.isFirstUp = true;
        this.breathCnt = 0;
    }

    public void logic() {
        if (!this.used && !this.initFlag) {
            if (this.direct >= 0) {
                this.velx = MyRandom.nextInt(0, 20);
            } else {
                this.velx = MyRandom.nextInt(-20, 0);
            }
            if (this.breathCnt >= 1) {
                this.breathCnt++;
            }
            if (this.breathCnt < 8) {
                this.posX += this.velx;
                this.posY += this.vely;
            } else {
                this.used = true;
            }
            refreshCollisionRect(this.posX, this.posY);
            if (this.drawer != null && this.drawer.getCurrentFrame() == 5) {
                this.CanBreathe = true;
            }
            if (!isInCamera()) {
                this.used = true;
                this.CanBreathe = false;
            }
            if (this.posY <= (StageManager.getWaterLevel() << 6) + MDPhone.SCREEN_HEIGHT) {
                this.initFlag = true;
                this.CanBreathe = false;
            }
        }
    }

    public void draw(MFGraphics g) {
        if (!this.used && !this.initFlag) {
            if (this.drawer.checkEnd()) {
                this.drawer = bubbleAnimation.getDrawer(4, true, 0);
            }
            drawInMap(g, this.drawer, this.posX, this.posY);
            drawCollisionRect(g);
        }
    }

    public int getPaintLayer() {
        return 1;
    }

    public void doWhileNoCollision() {
        this.isBeginPlayStageBGM = true;
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (this.CanBreathe) {
            if (object.doBreatheBubble()) {
                if (this.isFirstUp) {
                    SoundSystem.getInstance().playSe(59);
                    this.isFirstUp = false;
                }
                this.CanBreathe = false;
                this.breathCnt++;
            }
            if (this.isBeginPlayStageBGM && SoundSystem.getInstance().getPlayingBGMIndex() != StageManager.getBgmId()) {
                if (PlayerObject.IsInvincibility()) {
                    SoundSystem.getInstance().playBgm(44);
                    return;
                }
                SoundSystem.getInstance().playBgm(StageManager.getBgmId());
                this.isBeginPlayStageBGM = false;
            }
        }
    }

    public void refreshCollisionRect(int x, int y) {
        if (this.CanBreathe) {
            this.collisionRect.setRect(x - MDPhone.SCREEN_HEIGHT, y - MDPhone.SCREEN_HEIGHT, BUBBLE_RANGE, BUBBLE_RANGE);
        }
    }

    public void close() {
        this.drawer = null;
    }

    public static void releaseAllResource() {
        Animation.closeAnimation(bubbleAnimation);
        bubbleAnimation = null;
    }
}
