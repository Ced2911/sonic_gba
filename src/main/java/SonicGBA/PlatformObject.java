package SonicGBA;

/* compiled from: GimmickObject */
abstract class PlatformObject extends GimmickObject {
    protected PlatformObject(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
    }

    protected PlatformObject(int x, int y) {
        super(0, x, y, 0, 0, 0, 0);
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        switch (direction) {
            case 1:
                object.beStop(this.collisionRect.y0, 1, this);
                return;
            case 4:
                if (object.getMoveDistance().f14y > 0 && object.getCollisionRect().y1 < this.collisionRect.y1) {
                    object.beStop(this.collisionRect.y0, 1, this);
                    return;
                }
                return;
            default:
                return;
        }
    }
}
