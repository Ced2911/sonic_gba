package SonicGBA;

import com.sega.engine.action.ACCollision;
import com.sega.engine.action.ACObject;

/* compiled from: GimmickObject */
class SlipStart extends GimmickObject {
    private static final int ACTIVE_SPEED_X = 128;
    private static final int ACTIVE_SPEED_Y = 128;
    private static final int COLLISION_HEIGHT = 1792;
    private static final int COLLISION_OFFSET_Y = -128;
    private boolean isActived;

    protected SlipStart(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
    }

    public void logic() {
    }

    public void doWhileCollision(ACObject arg0, ACCollision arg1, int arg2, int arg3, int arg4, int arg5, int arg6) {
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (player.slipping && ((player instanceof PlayerSonic) || (player instanceof PlayerAmy))) {
            player.setSlip0();
        }
        if (!this.isActived) {
            PlayerObject playerObject;
            if (player instanceof PlayerSonic) {
                PlayerSonic sonic = (PlayerSonic)player;
                if (!this.isActived) {
                    if (player.getVelX() > 128 && player.getVelY() > 128 && this.collisionRect.collisionChk(player.getFootPositionX(), player.getFootPositionY()) && player.currentLayer == 1) {
                        playerObject = player;
                        PlayerObject.slidingFrame = 1;
                        sonic.slipStart();
                    }
                    this.isActived = true;
                }
            } else if (player instanceof PlayerAmy) {
                PlayerAmy sonic2 = (PlayerAmy)player;
                if (player.getCharacterAnimationID() >= 4 && player.getCharacterAnimationID() <= 8) {
                    return;
                }
                if ((player.getCharacterAnimationID() < 11 || player.getCharacterAnimationID() > 12) && !this.isActived) {
                    if (player.getVelX() > 128 && player.getVelY() > 128 && this.collisionRect.collisionChk(player.getFootPositionX(), player.getFootPositionY()) && player.currentLayer == 1) {
                        playerObject = player;
                        PlayerObject.slidingFrame = 1;
                        sonic2.slipStart();
                    }
                    this.isActived = true;
                }
            }
        }
    }

    public void doWhileNoCollision() {
        if (this.isActived) {
            if (player.slipping) {
                player.faceDegree = 45;
                player.calDivideVelocity();
                player.calTotalVelocity();
            }
            player.worldCal.setMovedState(false);
            this.isActived = false;
        }
    }
}
