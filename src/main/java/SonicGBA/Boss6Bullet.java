package SonicGBA;

import Lib.Animation;
import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.utility.MFMath;

/* compiled from: BulletObject */
class Boss6Bullet extends BulletObject {
    private static final int COLLISION_HEIGHT = 1024;
    private static final int COLLISION_WIDTH = 1024;
    private static final int TOTAL_SPEED = 240;

    protected Boss6Bullet(int x, int y, int playerx, int playery) {
        super(x, y, playerx, playery, false);
        if (boss6bulletAnimation == null) {
            boss6bulletAnimation = new Animation("/animation/boss6_bullet");
        }
        this.drawer = boss6bulletAnimation.getDrawer(0, true, 0);
        this.posX = x;
        this.posY = y;
        int root = MFMath.sqrt(((playerx - this.posX) * (playerx - this.posX)) + ((playery - this.posY) * (playery - this.posY))) >> 6;
        this.velX = ((playerx - this.posX) * 240) / root;
        this.velY = ((playery - this.posY) * 240) / root;
    }

    public void bulletLogic() {
        checkWithPlayer(this.posX, this.posY, this.posX + this.velX, this.posY + this.velY);
        this.posX += this.velX;
        this.posY += this.velY;
    }

    public void draw(MFGraphics g) {
        drawInMap(g, this.drawer);
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 512, y - 512, 1024, 1024);
    }

    public boolean chkDestroy() {
        return !isInCamera() || isFarAwayCamera();
    }
}
