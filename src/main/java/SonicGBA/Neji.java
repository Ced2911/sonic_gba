package SonicGBA;

import GameEngine.Key;
import Lib.Animation;
import Lib.AnimationDrawer;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: GimmickObject */
class Neji extends GimmickObject {
    private static final int PLAYER_OFFSET = 0;
    private static final int STAD_GMK_NEJI_ADD_SPEED = 48;
    private static final int STAD_GMK_NEJI_MAX_SPEED = 2304;
    private static final int STAD_GMK_NEJI_MIN_SPEED = 232;
    private static final int VELOCITY = 300;
    private static final int VELOCITY_CHANGE = 200;
    public static Animation nejiAnimation;
    public AnimationDrawer drawer;
    private int frameCnt;
    private boolean touching;
    private int velocity;

    protected Neji(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        this.touching = false;
        this.frameCnt = 0;
        this.frameCnt = 0;
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(this.posX + (this.iLeft * 512), this.posY, this.mWidth, 512);
    }

    public void draw(MFGraphics g) {
        drawCollisionRect(g);
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (this.firstTouch) {
            isGotRings = false;
            player.degreeForDraw = 0;
        }
        if (this.firstTouch && !this.touching && !player.hurtNoControl) {
            object.setOutOfControl(this);
            object.setAnimationId(25);
            object.setFootPositionY(this.posY + 0);
            this.touching = true;
            this.velocity = 0;
        }
    }

    public void logic() {
        if (this.touching) {
            this.frameCnt++;
            if (this.velocity < STAD_GMK_NEJI_MIN_SPEED) {
                this.velocity += 48;
            }
            if (this.velocity > STAD_GMK_NEJI_MAX_SPEED) {
                this.velocity = STAD_GMK_NEJI_MAX_SPEED;
            }
            if (Key.repeat(Key.gLeft)) {
                if (this.velocity > STAD_GMK_NEJI_MIN_SPEED) {
                    this.velocity -= 48;
                }
            } else if (Key.repeat(Key.gRight)) {
                this.velocity += 48;
            }
            int positionDes = player.getFootPositionX() + this.velocity;
            if (positionDes > this.collisionRect.x1) {
                positionDes = this.collisionRect.x1;
            }
            player.checkWithObject(player.getFootPositionX(), player.getFootPositionY(), positionDes, player.getFootPositionY());
            player.setFootPositionX(positionDes);
            player.setFootPositionY(this.posY + 0);
            boolean leaving = false;
            boolean isUp = false;
            if (Key.press(16777216)) {
                leaving = true;
            }
            if (!Key.repeat(Key.gUp | 33554432) && !Key.repeat(Key.gDown)) {
                int frame = player.drawer.getCurrentFrame();
                if (player.getAnimationId() != 25) {
                    player.getAnimationId();
                } else if (frame < 4) {
                    isUp = true;
                }
            } else if (Key.repeat(Key.gUp | 33554432)) {
                isUp = true;
            } else if (Key.repeat(Key.gDown)) {
                isUp = false;
            }
            if (leaving) {
                int i;
                player.outOfControl = false;
                player.setVelX(0);
                player.collisionState = (byte) 1;
                PlayerObject playerObject = player;
                if (isUp) {
                    i = -1;
                } else {
                    i = 1;
                }
                playerObject.setVelY(i * 1800);
                player.setFootPositionY(this.posY + 768);
                player.setAnimationId(4);
                if (!(player instanceof PlayerAmy) && this.firstTouch) {
                    soundInstance.playSe(4);
                }
                this.touching = false;
            }
            this.firstTouch = false;
        }
    }

    public void close() {
        this.drawer = null;
    }

    public static void releaseAllResource() {
        Animation.closeAnimation(nejiAnimation);
        nejiAnimation = null;
    }
}
