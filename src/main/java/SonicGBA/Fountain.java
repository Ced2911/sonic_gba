package SonicGBA;

import Lib.AnimationDrawer;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: GimmickObject */
class Fountain extends GimmickObject {
    private static final int CRASH_WAIT_FRAME = 8;
    private static final int DRAWER_NUM = 5;
    private static final int DRAW_OFFSET_X = 448;
    private static final int DRAW_OFFSET_Y = -384;
    private static final int LINE_HEIGHT = 80;
    private static final int LINE_WIDTH = 188;
    private static final int PLAYER_CROSS_END_X = 152704;
    private static final int PLAYER_CROSS_START_X = 147200;
    private static final int PLAYER_ON_WATER_MAX_SPEED = 1920;
    private static final int[][] POSITION_OFFSET = new int[][]{new int[2], new int[]{1024, -1024}, new int[]{7168, -3072}, new int[]{12288, -6144}, new int[]{18432, -5120}};
    private static AnimationDrawer[] drawer;
    private int crashCnt;
    private boolean isActived;
    private boolean nonecrashable;
    private boolean touching;

    protected Fountain(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        this.touching = false;
        this.touching = false;
        this.isActived = false;
        this.crashCnt = 0;
        this.nonecrashable = false;
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 13312, (this.iTop * 512) + y, 24064, this.mHeight);
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (this.touching || (!(player.collisionState == (byte) 0 || player.collisionState == (byte) 2) || player.getFootPositionX() <= PLAYER_CROSS_START_X || player.getFootPositionX() >= PLAYER_CROSS_END_X)) {
            if (!this.touching && this.collisionRect.collisionChk(object.getFootPositionX(), object.getFootPositionY())) {
                int objY = object.getFootPositionY();
                int lineY = getLineY(object.getFootPositionX());
                if (player.collisionState == (byte) 2) {
                    this.nonecrashable = true;
                } else if (objY >= lineY && !this.nonecrashable) {
                    this.touching = true;
                    player.setOutOfControl(this);
                    player.setBodyPositionY(lineY);
                    player.setAnimationId(30);
                    player.setCollisionState((byte) 1);
                    player.setNoKey();
                    player.showWaterFlush = true;
                    this.crashCnt = 0;
                }
            }
            if (player.showWaterFlush) {
                this.crashCnt = 0;
                if (this.crashCnt > 8) {
                    player.showWaterFlush = false;
                }
                if (!this.isActived) {
                    soundInstance.playSe(82);
                    this.isActived = true;
                    return;
                }
                return;
            }
            return;
        }
        this.nonecrashable = true;
        player.setFootPositionX(player.getFootPositionX() - 3840);
        player.setAnimationId(0);
        player.collisionState = (byte) 1;
        player.setVelX(0);
        player.faceDirection = false;
    }

    public void doWhileNoCollision() {
        if (this.touching) {
            this.touching = false;
            player.outOfControl = false;
            player.outOfControlObject = null;
            soundInstance.stopLoopSe();
            if (player.collisionState == (byte) 1) {
                player.doWhileLand(0);
            }
            this.isActived = false;
            this.crashCnt = 0;
        }
        this.nonecrashable = false;
    }

    public void logic() {
        if (this.touching && player.outOfControl) {
            if (player.getVelX() < 0) {
                player.setVelX(player.getVelX() + 300);
            } else {
                player.setVelX(player.getVelX() + 100);
            }
            if (player.getVelX() > PLAYER_ON_WATER_MAX_SPEED) {
                player.setVelX(PLAYER_ON_WATER_MAX_SPEED);
            }
            player.setVelY(0);
            player.setFaceDegree(0);
            int preX = player.getFootPositionX();
            int preY = player.getFootPositionY();
            player.setAnimationId(30);
            player.setNoKey();
            player.showWaterFlush = true;
            player.justLeaveLand = false;
            player.moveOnObject(player.getVelX() + preX, getLineY(player.getVelX() + preX) + 768, true);
            if (player.collisionState == (byte) 0) {
                this.touching = false;
                player.outOfControl = false;
                player.outOfControlObject = null;
                PlayerObject playerObject = player;
                if (PlayerObject.getCharacterID() == 2 && player.getCharacterAnimationID() == 22) {
                    player.setAnimationId(1);
                }
            }
        }
        if (player.getFootPositionX() >= ((((this.posX >> 6) + 188) - 10) << 6)) {
            this.nonecrashable = false;
            this.isActived = false;
        }
    }

    public int getLineY(int playerX) {
        int objX = playerX - this.posX;
        return (((((objX >> 6) * (objX >> 6)) * 80) / 35344) << 6) + this.collisionRect.y0;
    }

    public void draw(MFGraphics g) {
        drawCollisionRect(g);
    }

    public void close() {
        drawer = null;
    }

    public int getPaintLayer() {
        return 0;
    }
}
