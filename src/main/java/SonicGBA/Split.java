package SonicGBA;

import GameEngine.Key;
import Lib.MyAPI;
import Lib.SoundSystem;

/* compiled from: GimmickObject */
class Split extends GimmickObject {
    private static final int COLLISION_HEIGHT = 1024;
    private static final int COLLISION_WIDTH = 1024;
    private static final int DEGREE_VELOCITY = 2560;
    private static final int RADIUS = 512;
    private static final int SHOOT_SPEED = 2700;
    private boolean controlling;
    private int degree;

    protected Split(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        this.posY -= 256;
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 512, y - 512, 1024, 1024);
    }

    public void logic() {
        if (this.controlling) {
            this.firstTouch = false;
            this.degree += 2560;
            this.degree %= 23040;
            player.setBodyPositionX(this.posX + ((MyAPI.dCos(this.degree >> 6) * 512) / 100));
            player.setBodyPositionY(this.posY + ((MyAPI.dSin(this.degree >> 6) * 512) / 100));
            boolean left = false;
            int nextVelX = 0;
            int nextVelY = 0;
            PlayerObject playerObject;
            SoundSystem soundSystem;
            SoundSystem soundSystem2;
            switch (this.iLeft) {
                case 0:
                    if (!Key.press(Key.gDown)) {
                        if (Key.press(Key.gRight)) {
                            player.setBodyPositionX(this.posX + 512);
                            player.setBodyPositionY(this.posY);
                            left = true;
                            nextVelX = SHOOT_SPEED;
                            nextVelY = 0 - GRAVITY;
                            player.faceDirection = true;
                            playerObject = player;
                            if (PlayerObject.getCharacterID() != 3) {
                                soundSystem = soundInstance;
                                soundSystem2 = soundInstance;
                                soundSystem.playSe(4);
                                break;
                            }
                            soundSystem = soundInstance;
                            soundSystem2 = soundInstance;
                            soundSystem.playSe(25);
                            break;
                        }
                    }
                    player.setBodyPositionX(this.posX);
                    player.setBodyPositionY(this.posY + 512);
                    left = true;
                    nextVelX = 0;
                    nextVelY = SHOOT_SPEED;
                    playerObject = player;
                    if (PlayerObject.getCharacterID() != 3) {
                        soundSystem = soundInstance;
                        soundSystem2 = soundInstance;
                        soundSystem.playSe(4);
                        break;
                    }
                    soundSystem = soundInstance;
                    soundSystem2 = soundInstance;
                    soundSystem.playSe(25);
                    break;
                case 1:
                    if (!Key.press(Key.gLeft)) {
                        if (Key.press(Key.gRight)) {
                            player.setBodyPositionX(this.posX);
                            player.setBodyPositionY(this.posY);
                            left = true;
                            nextVelX = SHOOT_SPEED;
                            nextVelY = SHOOT_SPEED - GRAVITY;
                            player.faceDirection = true;
                            playerObject = player;
                            if (PlayerObject.getCharacterID() != 3) {
                                soundSystem = soundInstance;
                                soundSystem2 = soundInstance;
                                soundSystem.playSe(4);
                                break;
                            }
                            soundSystem = soundInstance;
                            soundSystem2 = soundInstance;
                            soundSystem.playSe(25);
                            break;
                        }
                    }
                    player.setBodyPositionX(this.posX);
                    player.setBodyPositionY(this.posY);
                    left = true;
                    nextVelX = -2700;
                    nextVelY = SHOOT_SPEED - GRAVITY;
                    player.faceDirection = false;
                    playerObject = player;
                    if (PlayerObject.getCharacterID() != 3) {
                        soundSystem = soundInstance;
                        soundSystem2 = soundInstance;
                        soundSystem.playSe(4);
                        break;
                    }
                    soundSystem = soundInstance;
                    soundSystem2 = soundInstance;
                    soundSystem.playSe(25);
                    break;
                case 2:
                    if (Key.press(Key.gRight)) {
                        player.setBodyPositionX(this.posX + 512);
                        player.setBodyPositionY(this.posY);
                        left = true;
                        nextVelX = SHOOT_SPEED;
                        nextVelY = 0 - GRAVITY;
                        player.faceDirection = true;
                        playerObject = player;
                        if (PlayerObject.getCharacterID() != 3) {
                            soundSystem = soundInstance;
                            soundSystem2 = soundInstance;
                            soundSystem.playSe(4);
                            break;
                        }
                        soundSystem = soundInstance;
                        soundSystem2 = soundInstance;
                        soundSystem.playSe(25);
                        break;
                    }
                    break;
                case 3:
                    if (Key.press(Key.gDown)) {
                        player.setBodyPositionX(this.posX);
                        player.setBodyPositionY(this.posY + 512);
                        left = true;
                        nextVelX = 0;
                        nextVelY = SHOOT_SPEED;
                        playerObject = player;
                        if (PlayerObject.getCharacterID() != 3) {
                            soundSystem = soundInstance;
                            soundSystem2 = soundInstance;
                            soundSystem.playSe(4);
                            break;
                        }
                        soundSystem = soundInstance;
                        soundSystem2 = soundInstance;
                        soundSystem.playSe(25);
                        break;
                    }
                    break;
            }
            if (left) {
                player.collisionState = (byte) 1;
                player.setAnimationId(4);
                player.setVelX(nextVelX);
                player.setVelY(nextVelY);
                this.controlling = false;
                player.outOfControl = false;
            }
        }
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (this.firstTouch) {
            this.controlling = true;
            player.setAnimationId(4);
            player.setOutOfControlInPipe(this);
            SoundSystem soundSystem = soundInstance;
            SoundSystem soundSystem2 = soundInstance;
            soundSystem.playSe(37);
        }
    }
}
