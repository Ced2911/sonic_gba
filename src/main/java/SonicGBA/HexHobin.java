package SonicGBA;

import Lib.SoundSystem;
import State.StringIndex;
import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;

/* compiled from: GimmickObject */
class HexHobin extends GimmickObject {
    private static final int COLLISION_HEIGHT = 2176;
    private static final int COLLISION_WIDTH = 3072;
    private static final int HOBIN_POWER = 1152;
    private static final int VELOCITY = 250;
    private static MFImage hexHobinImage = null;
    public HobinCal hobinCal;
    private boolean isH;
    public MoveCalculator moveCal;

    protected HexHobin(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        boolean moveDirection;
        if (hexHobinImage == null) {
            try {
                hexHobinImage = MFImage.createImage("/gimmick/hex_hobin.png");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (this.mWidth >= this.mHeight) {
            this.isH = true;
            if (this.iLeft == 0) {
                moveDirection = false;
            } else {
                moveDirection = true;
            }
        } else {
            this.isH = false;
            if (this.iTop == 0) {
                moveDirection = false;
            } else {
                moveDirection = true;
            }
        }
        this.moveCal = new MoveCalculator(this.isH ? this.posX : this.posY, this.isH ? this.mWidth : this.mHeight, moveDirection);
        this.hobinCal = new HobinCal();
    }

    public void logic() {
        this.moveCal.logic();
        int preX = this.posX;
        int preY = this.posY;
        if (this.isH) {
            this.posX = this.moveCal.getPosition();
        } else {
            this.posY = this.moveCal.getPosition();
        }
        checkWithPlayer(preX, preY, this.posX, this.posY);
    }

    public void draw(MFGraphics g) {
        drawInMap(g, hexHobinImage, this.posX + this.hobinCal.getPosOffsetX(), this.posY + this.hobinCal.getPosOffsetY(), 3);
        this.hobinCal.logic();
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        object.beStop(this.collisionRect.y0, direction, this);
        if (direction == 3) {
            player.rightStopped = false;
            if (((player instanceof PlayerKnuckles) && (player.getCharacterAnimationID() == 19 || player.getCharacterAnimationID() == 20 || player.getCharacterAnimationID() == 21 || player.getCharacterAnimationID() == 22 || player.getCharacterAnimationID() == 29 || player.getCharacterAnimationID() == 30 || player.getCharacterAnimationID() == 31 || player.getCharacterAnimationID() == 32 || player.getCharacterAnimationID() == 33)) || ((player instanceof PlayerTails) && (player.getCharacterAnimationID() == 12 || player.getCharacterAnimationID() == 13 || player.getCharacterAnimationID() == 14))) {
                player.animationID = 4;
            }
        } else if (direction == 2) {
            player.leftStopped = false;
            if (((player instanceof PlayerKnuckles) && (player.getCharacterAnimationID() == 19 || player.getCharacterAnimationID() == 20 || player.getCharacterAnimationID() == 21 || player.getCharacterAnimationID() == 22 || player.getCharacterAnimationID() == 29 || player.getCharacterAnimationID() == 30 || player.getCharacterAnimationID() == 31 || player.getCharacterAnimationID() == 32 || player.getCharacterAnimationID() == 33)) || ((player instanceof PlayerTails) && (player.getCharacterAnimationID() == 12 || player.getCharacterAnimationID() == 13 || player.getCharacterAnimationID() == 14))) {
                player.animationID = 4;
            }
        }
        if (player.collisionState != (byte) 0) {
            player.collisionState = (byte) 1;
        }
        player.dashRolling = false;
        switch (direction) {
            case 0:
                player.setVelY(1152);
                if (((player.collisionRect.x0 + player.collisionRect.x1) >> 1) < ((this.collisionRect.x0 + this.collisionRect.x1) >> 1)) {
                    player.setVelX(-1152);
                    this.hobinCal.startHobin(400, StringIndex.FONT_COLON_RED, 10);
                } else if (((player.collisionRect.x0 + player.collisionRect.x1) >> 1) > ((this.collisionRect.x0 + this.collisionRect.x1) >> 1)) {
                    player.setVelX(1152);
                    this.hobinCal.startHobin(400, 45, 10);
                }
                SoundSystem.getInstance().playSe(54);
                return;
            case 1:
                player.setVelY(-1152);
                if (((player.collisionRect.x0 + player.collisionRect.x1) >> 1) < ((this.collisionRect.x0 + this.collisionRect.x1) >> 1)) {
                    player.setVelX(-1152);
                    this.hobinCal.startHobin(400, 225, 10);
                } else if (((player.collisionRect.x0 + player.collisionRect.x1) >> 1) > ((this.collisionRect.x0 + this.collisionRect.x1) >> 1)) {
                    player.setVelX(1152);
                    this.hobinCal.startHobin(400, -45, 10);
                }
                SoundSystem.getInstance().playSe(54);
                return;
            case 2:
                player.setVelX(1152);
                this.hobinCal.startHobin(400, RollPlatformSpeedC.DEGREE_VELOCITY, 10);
                SoundSystem.getInstance().playSe(54);
                return;
            case 3:
                player.setVelX(-1152);
                this.hobinCal.startHobin(400, 0, 10);
                SoundSystem.getInstance().playSe(54);
                return;
            default:
                return;
        }
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 1536, y - 1088, 3072, 2176);
    }

    public void close() {
    }

    public static void releaseAllResource() {
        hexHobinImage = null;
    }

    public int getPaintLayer() {
        return 0;
    }
}
