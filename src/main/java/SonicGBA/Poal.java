package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import Lib.SoundSystem;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: GimmickObject */
class Poal extends GimmickObject {
    private static final int COLLISION_HEIGHT = 640;
    private static final int COLLISION_WIDTH = 1600;
    private static final int[] PULL_OFFSET;
    private static final byte STATE_POP = (byte) 2;
    private static final byte STATE_PULL = (byte) 1;
    private static final byte STATE_STAY = (byte) 0;
    private static Animation poalAnimation;
    private int drawIdStart;
    private AnimationDrawer drawer;
    private boolean isH;
    private int offsetCount;
    private int restCount;
    private byte state;

    static {
        int[] iArr = new int[3];
        iArr[1] = 256;
        iArr[2] = 320;
        PULL_OFFSET = iArr;
    }

    protected Poal(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        if (poalAnimation == null) {
            poalAnimation = new Animation("/animation/poal_" + StageManager.getCurrentZoneId());
        }
        this.isH = false;
        switch (this.objId) {
            case 42:
                if (this.iTop >= 0) {
                    this.drawer = poalAnimation.getDrawer(3, false, 0);
                } else {
                    this.drawer = poalAnimation.getDrawer(3, false, 2);
                }
                this.drawIdStart = 3;
                this.collisionRect.setRect(this.posX - 320, this.posY - COLLISION_WIDTH, 640, COLLISION_WIDTH);
                this.isH = true;
                break;
            case 44:
                this.drawer = poalAnimation.getDrawer(0, false, 0);
                this.collisionRect.setRect(this.posX, this.posY, COLLISION_WIDTH, 640);
                this.drawIdStart = 0;
                break;
            case 45:
                this.drawer = poalAnimation.getDrawer(0, false, 2);
                this.collisionRect.setRect(this.posX - COLLISION_WIDTH, this.posY, COLLISION_WIDTH, 640);
                this.drawIdStart = 0;
                break;
        }
        this.state = (byte) 0;
    }

    public void draw(MFGraphics g) {
        this.drawer.setActionId(this.drawIdStart + this.state);
        drawInMap(g, this.drawer);
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (this.state == (byte) 0 && object == player) {
            PlayerObject playerObject;
            int i;
            int i2;
            boolean z;
            switch (this.objId) {
                case 42:
                    this.offsetCount = 0;
                    playerObject = player;
                    i = this.posX;
                    i2 = this.posY;
                    if (this.iTop >= 0) {
                        z = true;
                    } else {
                        z = false;
                    }
                    if (playerObject.doPoalMotion2(i, i2, z)) {
                        this.state = (byte) 1;
                        SoundSystem.getInstance().playSe(47);
                        return;
                    }
                    return;
                default:
                    this.offsetCount = 0;
                    playerObject = player;
                    i = this.collisionRect.x0 + 800;
                    i2 = this.collisionRect.y0 + PULL_OFFSET[this.offsetCount];
                    if (this.objId == 44) {
                        z = true;
                    } else {
                        z = false;
                    }
                    if (playerObject.doPoalMotion(i, i2, z)) {
                        this.state = (byte) 1;
                        SoundSystem.getInstance().playSe(47);
                        player.setFaceDegree(0);
                        player.degreeForDraw = player.faceDegree;
                        return;
                    }
                    return;
            }
        }
    }

    public void logic() {
        switch (this.state) {
            case (byte) 1:
                PlayerObject playerObject;
                int i;
                if (this.drawer.getActionId() == this.drawIdStart + 1 && this.drawer.checkEnd()) {
                    this.restCount = 10;
                    this.state = (byte) 2;
                    playerObject = player;
                    i = this.isH ? this.iTop >= 0 ? 2 : 3 : 1;
                    playerObject.bePop(2400, i);
                    return;
                } else if (this.isH) {
                    player.doPoalMotion2(this.posX, this.posY, this.iTop >= 0);
                    return;
                } else {
                    boolean z;
                    playerObject = player;
                    int i2 = this.collisionRect.x0 + 800;
                    i = this.collisionRect.y0 + PULL_OFFSET[this.offsetCount];
                    if (this.objId == 44) {
                        z = true;
                    } else {
                        z = false;
                    }
                    playerObject.doPoalMotion(i2, i, z);
                    this.offsetCount++;
                    if (this.offsetCount >= PULL_OFFSET.length) {
                        this.offsetCount = PULL_OFFSET.length - 1;
                        return;
                    }
                    return;
                }
            case (byte) 2:
                if (this.restCount > 0) {
                    this.restCount--;
                }
                if (this.restCount == 0 && this.drawer.checkEnd()) {
                    this.state = (byte) 0;
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void close() {
        this.drawer = null;
    }

    public int getPaintLayer() {
        return 0;
    }

    public static void releaseAllResource() {
        Animation.closeAnimation(poalAnimation);
        poalAnimation = null;
    }
}
