package SonicGBA;

import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;

/* compiled from: GimmickObject */
class SeabedVolcanoPlatform extends GimmickObject {
    private static final int COLLISION_HEIGHT = 1536;
    private static final int COLLISION_OFFSET_Y = 0;
    private static final int COLLISION_WIDTH = 3072;
    private static final int LAUNCH_VEL = -1400;
    private static int endCount;
    private static MFImage image;
    private static boolean moving;
    public static int sPosY;
    private static int velocity;
    private int posOriginalY = this.posY;
    private SeabedVolcanoBase sb;

    protected SeabedVolcanoPlatform(int x, int y, SeabedVolcanoBase sb) {
        super(0, x, y, 0, 0, 0, 0);
        if (image == null) {
            image = MFImage.createImage("/gimmick/platform4_fire_mt.png");
        }
        this.sb = sb;
    }

    public static void staticLogic() {
        if (moving) {
            velocity += GRAVITY;
            sPosY += velocity;
            if (sPosY >= 0) {
                sPosY = 0;
                endCount--;
                if (endCount <= 0) {
                    moving = false;
                } else {
                    velocity = (-velocity) / 2;
                }
            }
        }
    }

    public static void shot() {
        moving = true;
        velocity = LAUNCH_VEL;
        endCount = 3;
    }

    public static boolean isShotting() {
        return moving && endCount == 3;
    }

    public void logic() {
        if (player.isFootOnObject(this) && (player instanceof PlayerKnuckles)) {
            ((PlayerKnuckles) player).setFloating(false);
        }
        int preY = this.posY;
        this.posY = this.posOriginalY + sPosY;
        checkWithPlayer(this.posX, preY, this.posX, this.posY);
        this.sb.sh.refreshCollisionRect(this.posX, this.posY);
    }

    public void drawPlatform(MFGraphics g) {
        drawInMap(g, image, this.posX, this.posY + 768, 33);
        drawCollisionRect(g);
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 1536, (y - 768) - 0, 3072, 1536);
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (direction == 1 || (direction == 4 && object.getVelY() > 0)) {
            object.beStop(0, 1, this);
        }
        if (direction == 4 && (player instanceof PlayerKnuckles) && player.myAnimationID == 33) {
            PlayerObject playerObject = player;
            playerObject.posY -= 268;
        }
    }

    public void close() {
        this.sb = null;
    }

    public static void releaseAllResource() {
        image = null;
    }

    public boolean collisionChkWithObject(PlayerObject object) {
        CollisionRect objectRect = object.getCollisionRect();
        CollisionRect thisRect = getCollisionRect();
        rectV.setRect(objectRect.x0 + 192, objectRect.y0, objectRect.getWidth() - PlayerSonic.BACK_JUMP_SPEED_X, objectRect.getHeight());
        return thisRect.collisionChk(rectV);
    }
}
