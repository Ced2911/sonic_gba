package SonicGBA;

import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: RingObject */
class MoveRingObject extends RingObject implements MapBehavior {
    private static final int LIFE_COUNT = 90;
    private long appearTime;
    private final boolean isAntiGravity;
    private MapObject mapObj;
    private int popVelX;
    private int popVelY;
    private boolean poping;
    private int reserveVelX;
    private int unTouchCount;

    protected MoveRingObject(int x, int y, int vx, int vy, int layer, long appearTime, int unTouchCount) {
        this(x, y, vx, vy, layer, appearTime);
        this.unTouchCount = unTouchCount;
    }

    protected MoveRingObject(int x, int y, int vx, int vy, int layer, long appearTime) {
        super(x, y);
        this.mapObj = new MapObject(x, y, vx, vy, this, layer);
        this.mapObj.setBehavior(this);
        this.reserveVelX = vx;
        this.appearTime = appearTime;
        this.isAntiGravity = player.isAntiGravity;
        this.mapObj.setAntiGravity(this.isAntiGravity);
        this.unTouchCount = 0;
    }

    public void logic() {
        if (this.poping) {
            this.mapObj.doJump(this.popVelX, this.popVelY, true);
            this.poping = false;
        }
        this.mapObj.logic();
        checkWithPlayer(this.posX, this.posY, this.mapObj.getPosX(), this.mapObj.getPosY());
        this.posX = this.mapObj.getPosX();
        this.posY = this.mapObj.getPosY();
        if (this.unTouchCount > 0) {
            this.unTouchCount--;
        }
    }

    public void draw(MFGraphics g) {
        if (systemClock - this.appearTime <= 60) {
            super.draw(g);
        } else if (systemClock % 2 == 0) {
            super.draw(g);
        }
    }

    public CollisionRect getCollisionRect() {
        return this.collisionRect;
    }

    public void doWhileTouchGround(int vx, int vy) {
        if (Math.abs(vy) < PlayerObject.BANKING_MIN_SPEED) {
            vy = PlayerObject.BANKING_MIN_SPEED;
        }
        this.reserveVelX = (this.reserveVelX * 9) / 10;
        this.mapObj.doJump(this.reserveVelX, ((-vy) * 9) / 10);
    }

    public boolean objectChkDestroy() {
        return super.objectChkDestroy() || systemClock - this.appearTime > 90 || systemClock - this.appearTime < 0;
    }

    public boolean hasSideCollision() {
        return false;
    }

    public boolean hasTopCollision() {
        return this.isAntiGravity;
    }

    public boolean canBeInit() {
        return false;
    }

    public boolean hasDownCollision() {
        return !this.isAntiGravity;
    }

    public void doWhileToucRoof(int vx, int vy) {
        if (!this.poping) {
            this.poping = true;
            if (Math.abs(vy) < PlayerObject.BANKING_MIN_SPEED) {
                vy = -500;
            }
            this.reserveVelX = (this.reserveVelX * 9) / 10;
            this.popVelX = this.reserveVelX;
            this.popVelY = ((-vy) * 9) / 10;
        }
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (this.unTouchCount <= 0) {
            super.doWhileCollision(object, direction);
        }
    }

    public int getGravity() {
        return GRAVITY;
    }
}
