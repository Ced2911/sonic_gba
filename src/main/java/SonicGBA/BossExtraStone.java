package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import Lib.MyAPI;
import Lib.MyRandom;
import Special.SSDef;
import com.sega.engine.action.ACMoveCalUser;
import com.sega.engine.action.ACMoveCalculator;
import com.sega.mobile.define.MDPhone;
import com.sega.mobile.framework.android.Graphics;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: BulletObject */
class BossExtraStone extends BulletObject implements ACMoveCalUser {
    private static final int[][] VELOCITY_LIST = new int[][]{new int[]{24, 5}, new int[]{96, 5}, new int[]{168, 5}, new int[]{SSDef.PLAYER_MOVE_HEIGHT, 15}, new int[]{312, 15}, new int[]{PlayerSonic.BACK_JUMP_SPEED_X, 15}, new int[]{456, 15}, new int[]{528, 15}, new int[]{600, 10}};
    private boolean dead = false;
    private int degree;
    private int degreeSpeed;
    private ACMoveCalculator moveCal;
    private byte[] rect;
    private int type;

    /* compiled from: BulletObject */
    class StonePiece extends GimmickObject {
        private int degree = 0;
        private AnimationDrawer drawer;

        protected StonePiece(AnimationDrawer drawer, int posX, int posY, int vx, int vy) {
            super(0, posX, posY, 0, 0, 0, 0);
            this.drawer = drawer;
            this.velX = vx;
            this.velY = vy;
        }

        public void logic() {
            this.velY += GRAVITY >> 2;
            this.posX += this.velX;
            this.posY += this.velY;
            this.degree += 20;
            this.degree %= MDPhone.SCREEN_WIDTH;
        }

        public void draw(MFGraphics g) {
            g.saveCanvas();
            g.translateCanvas((this.posX >> 6) - camera.f13x, (this.posY >> 6) - camera.f14y);
            g.rotateCanvas((float) this.degree);
            this.drawer.draw(g, 0, 0);
            g.restoreCanvas();
        }

        public void close() {
            this.drawer = null;
        }

        public boolean chkDestroy() {
            return (this.posY >> 6) > (camera.f14y + SCREEN_HEIGHT) + 40;
        }
    }

    protected BossExtraStone(int x, int y) {
        super(x, y, 0, 0, false);
        int degree;
        int random = MyRandom.nextInt(100);
        int probability = 0;
        int velocity = 0;
        for (int i = 0; i < VELOCITY_LIST.length; i++) {
            probability += VELOCITY_LIST[i][1];
            if (random < probability) {
                velocity = VELOCITY_LIST[i][0];
                break;
            }
        }
        if (MyRandom.nextInt(100) < 85) {
            degree = MyRandom.nextInt(225, 270);
        } else {
            degree = MyRandom.nextInt(270, 315);
        }
        this.velX = (MyAPI.dCos(degree) * velocity) / 100;
        this.velY = (MyAPI.dSin(degree) * velocity) / 100;
        this.type = MyRandom.nextInt(5);
        this.moveCal = new ACMoveCalculator(this, this);
        if (stoneAnimation == null) {
            stoneAnimation = new Animation("/animation/boss_extra_stone");
        }
        this.drawer = stoneAnimation.getDrawer(this.type, false, MyRandom.nextInt(2) == 0 ? 0 : 2);
        this.rect = this.drawer.getARect();
        this.degreeSpeed = MyRandom.nextInt(-20, 20);
    }

    public void bulletLogic() {
        this.velY += GRAVITY >> 4;
        this.moveCal.actionLogic(this.velX, this.velY);
        this.degree += this.degreeSpeed;
        while (this.degree < 0) {
            this.degree += MDPhone.SCREEN_WIDTH;
        }
        this.degree %= MDPhone.SCREEN_WIDTH;
        if (this.posY > 19200) {
            this.dead = true;
        }
    }

    public void draw(MFGraphics g) {
        Graphics g2 = (Graphics) g.getSystemGraphics();
        g2.save();
        g2.translate((float) ((this.posX >> 6) - camera.f13x), (float) ((this.posY >> 6) - camera.f14y));
        g2.rotate((float) this.degree);
        this.drawer.draw(g, 0, 0);
        g2.restore();
    }

    public boolean chkDestroy() {
        return this.dead;
    }

    public void refreshCollisionRect(int x, int y) {
        if (this.rect != null) {
            this.collisionRect.setRect((this.rect[0] << 6) + x, (this.rect[1] << 6) + y, this.rect[2] << 6, this.rect[3] << 6);
        }
    }

    public void didAfterEveryMove(int arg0, int arg1) {
        refreshCollisionRect(this.posX, this.posY);
        doWhileCollisionWrapWithPlayer();
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (player.isAttackingEnemy()) {
            this.dead = true;
            explode();
            return;
        }
        player.beHurt();
    }

    private void explode() {
        int i = 0;
        while (true) {
            if (i < (this.type == 4 ? 2 : 3)) {
                GameObject.addGameObject(new StonePiece(stoneAnimation.getDrawer(5, true, 0), this.posX, this.posY, MyRandom.nextInt(-200, 200), MyRandom.nextInt((-GRAVITY) * 2, -GRAVITY)));
                i++;
            } else {
                return;
            }
        }
    }
}
