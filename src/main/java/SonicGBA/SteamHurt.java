package SonicGBA;

/* compiled from: GimmickObject */
class SteamHurt extends GimmickObject {
    private static final int COLLISION_HEIGHT = 1024;
    private static final int COLLISION_OFFSET_Y = -1280;
    private static final int COLLISION_WIDTH = 128;
    private SteamBase sb;

    protected SteamHurt(int x, int y, SteamBase sb) {
        super(0, x, y, 0, 0, 0, 0);
        this.sb = sb;
    }

    public void refreshCollisionRect(int x, int y) {
        int height = SteamPlatform.sPosY + 768;
        if (height > 0) {
            height = 0;
        }
        if (height < -1024) {
            height = -1024;
        }
        this.collisionRect.setRect(this.posX - 64, (this.posY + COLLISION_OFFSET_Y) + height, 128, Math.abs(height));
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (this.collisionRect.getHeight() != 0 && !object.isFootOnObject(this.sb.sp)) {
            object.beHurt();
        }
    }

    public void close() {
        this.sb = null;
    }
}
