package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: EnemyObject */
class BossF2Drill extends EnemyObject {
    private static final int COLLISION_HEIGHT = 1344;
    private static final int COLLISION_WIDTH = 1728;
    private static Animation drillAni;
    private AnimationDrawer drillDrawer;
    private boolean isPlayerHurt = false;

    protected BossF2Drill(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        if (drillAni == null) {
            drillAni = new Animation("/animation/bossf2_drill");
        }
        this.drillDrawer = drillAni.getDrawer(0, true, 0);
        this.isPlayerHurt = false;
    }

    public void logic() {
    }

    public void logic(int x, int y, boolean isTrans) {
        if (!this.dead) {
            this.posX = x;
            this.posY = y;
            int preX = this.posX;
            int preY = this.posY;
            if (isTrans) {
                this.drillDrawer.setTrans(2);
            } else {
                this.drillDrawer.setTrans(0);
            }
            refreshCollisionRect(this.posX, this.posY);
            checkWithPlayer(preX, preY, this.posX, this.posY);
        }
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (!this.dead && object == player) {
            player.beHurt();
            this.isPlayerHurt = true;
        }
    }

    public void doWhileBeAttack(PlayerObject object, int direction, int animationID) {
    }

    public void doWhileNoCollision() {
        this.isPlayerHurt = false;
    }

    public void setEnd() {
        this.dead = true;
    }

    public boolean getPlayerHurt() {
        return this.isPlayerHurt;
    }

    public void resetPlayerHurt() {
        this.isPlayerHurt = false;
    }

    public void draw(MFGraphics g) {
        if (!this.dead) {
            drawInMap(g, this.drillDrawer);
            drawCollisionRect(g);
        }
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 864, y - PlayerObject.SONIC_ATTACK_LEVEL_2_V0, COLLISION_WIDTH, COLLISION_HEIGHT);
    }
}
