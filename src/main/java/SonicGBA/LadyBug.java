package SonicGBA;

import Lib.Animation;
import Lib.MyAPI;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: EnemyObject */
class LadyBug extends EnemyObject {
    private static final int ALERT_HEIGHT = 64;
    private static final int ALERT_RANGE = 4992;
    private static final int ALERT_WIDTH = 72;
    private static final int COLLISION_HEIGHT = 1024;
    private static final int COLLISION_WIDTH = 1664;
    private static final int STATE_ATTACK = 2;
    private static final int STATE_FLY = 1;
    private static final int STATE_WAIT = 0;
    private static Animation ladybugAnimation;
    private int alert_state;
    private int attack_cnt;
    private int attack_cnt_max;
    private int circleCenterX;
    private int circleCenterY;
    private int dg;
    private int emenyid;
    private int fire_start_speed;
    private int plus;
    private int plus_cnt;
    private int state;
    private int wait_cnt;
    private int wait_cnt_max;

    public static void releaseAllResource() {
        Animation.closeAnimation(ladybugAnimation);
        ladybugAnimation = null;
    }

    protected LadyBug(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        this.dg = 10;
        this.plus_cnt = 0;
        this.plus = 1;
        this.wait_cnt = 0;
        this.wait_cnt_max = 16;
        this.attack_cnt = 0;
        this.attack_cnt_max = 4;
        this.fire_start_speed = 300;
        this.circleCenterX = this.posX + (this.mWidth >> 1);
        this.circleCenterY = this.posY;
        this.plus_cnt = 0;
        if (ladybugAnimation == null) {
            ladybugAnimation = new Animation("/animation/ladybug");
        }
        this.drawer = ladybugAnimation.getDrawer(0, false, 0);
        this.emenyid = id;
    }

    public void logic() {
        if (!this.dead) {
            this.alert_state = checkPlayerInEnemyAlertRange(this.posX >> 6, this.posY >> 6, 72, 64);
            int preX = this.posX;
            int preY = this.posY;
            switch (this.state) {
                case 0:
                    if (this.alert_state == 0) {
                        this.state = 2;
                        this.drawer.setActionId(2);
                        this.drawer.setLoop(true);
                        this.attack_cnt = 0;
                    } else if (this.wait_cnt < this.wait_cnt_max) {
                        this.wait_cnt++;
                    } else {
                        this.state = 1;
                    }
                    checkWithPlayer(preX, preY, this.posX, this.posY);
                    return;
                case 1:
                    this.drawer.setActionId(1);
                    this.drawer.setLoop(true);
                    if (this.plus > 0) {
                        this.plus_cnt += this.plus;
                        if (this.plus_cnt >= RollPlatformSpeedC.DEGREE_VELOCITY / this.dg) {
                            this.plus_cnt = RollPlatformSpeedC.DEGREE_VELOCITY / this.dg;
                            this.plus = -this.plus;
                            this.state = 0;
                            this.drawer.setActionId(0);
                            this.drawer.setLoop(true);
                            this.wait_cnt = 0;
                        }
                    } else {
                        this.plus_cnt += this.plus;
                        if (this.plus_cnt <= 0) {
                            this.plus_cnt = 0;
                            this.plus = -this.plus;
                            this.state = 0;
                            this.drawer.setActionId(0);
                            this.drawer.setLoop(true);
                            this.wait_cnt = 0;
                        }
                    }
                    this.posX = this.circleCenterX - (((this.mWidth >> 1) * MyAPI.dCos(this.dg * this.plus_cnt)) / 100);
                    this.posY = this.circleCenterY + (((this.mWidth >> 1) * MyAPI.dSin(this.dg * this.plus_cnt)) / 100);
                    checkWithPlayer(preX, preY, this.posX, this.posY);
                    return;
                case 2:
                    if (this.attack_cnt < this.attack_cnt_max) {
                        this.attack_cnt++;
                    } else {
                        BulletObject.addBullet(this.emenyid, this.posX - 832, this.posY, -this.fire_start_speed, -this.fire_start_speed);
                        BulletObject.addBullet(this.emenyid, this.posX + 832, this.posY, this.fire_start_speed, -this.fire_start_speed);
                        this.state = 1;
                    }
                    checkWithPlayer(preX, preY, this.posX, this.posY);
                    return;
                default:
                    return;
            }
        }
    }

    public void draw(MFGraphics g) {
        if (!this.dead) {
            drawInMap(g, this.drawer);
            drawCollisionRect(g);
        }
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 832, y, 1664, 1024);
    }
}
