package SonicGBA;

import Lib.Animation;
import Lib.MyAPI;
import com.sega.mobile.framework.device.MFGraphics;
import java.lang.reflect.Array;

/* compiled from: EnemyObject */
class Caterpillar extends EnemyObject {
    private static final int COLLISION_HEIGHT = 1024;
    private static final int COLLISION_WIDTH = 1024;
    private static final int STATE_MOVE = 0;
    private static Animation caterpillarAnimation;
    private CaterpillarBody[] body;
    private int circleCenterX;
    private int circleCenterY;
    private int dg = 10;
    private int leftx;
    private int lefty;
    private int plus = 1;
    private int plus_cnt = 0;
    private int[][] pos;
    private int rightx;
    private int righty;
    private int state;

    public static void releaseAllResource() {
        Animation.closeAnimation(caterpillarAnimation);
        caterpillarAnimation = null;
    }

    protected Caterpillar(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        this.posY += BarHorbinV.COLLISION_WIDTH;
        this.circleCenterX = this.posX + (this.mWidth >> 1);
        this.circleCenterY = this.posY;
        this.plus_cnt = 0;
        if (caterpillarAnimation == null) {
            caterpillarAnimation = new Animation("/animation/caterpillar");
        }
        this.drawer = caterpillarAnimation.getDrawer(0, false, 2);
        this.pos = (int[][]) Array.newInstance(Integer.TYPE, new int[]{5, 2});
        this.body = new CaterpillarBody[5];
        this.body[0] = new CaterpillarBody(id, x, y, left, top, width, height, true, this);
        for (int i = 1; i < this.body.length; i++) {
            this.body[i] = new CaterpillarBody(id, x, y, left, top, width, height, false, this);
            GameObject.addGameObject(this.body[i], x, y);
        }
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (!this.dead) {
        }
    }

    public void logic() {
        if (!this.dead) {
            int i;
            int preX = this.posX;
            int preY = this.posY;
            switch (this.state) {
                case 0:
                    this.plus_cnt += this.plus;
                    for (i = 0; i < this.pos.length; i++) {
                        this.pos[i][0] = this.circleCenterX - (((this.mWidth >> 1) * MyAPI.dCos(this.dg * (this.plus_cnt - (i * 3)))) / 100);
                        this.pos[i][1] = this.circleCenterY + (((this.mWidth >> 1) * MyAPI.dSin(this.dg * (this.plus_cnt - (i * 3)))) / 200);
                    }
                    this.posX = this.pos[0][0];
                    this.posY = this.pos[0][1];
                    for (i = 0; i < this.body.length; i++) {
                        this.body[i].logic(this.pos[i][0], this.pos[i][1]);
                    }
                    checkWithPlayer(preX, preY, this.posX, this.posY);
                    break;
            }
            for (i = 1; i < this.body.length; i++) {
                this.body[i].dead = this.body[0].dead;
            }
            this.dead = this.body[0].dead;
        }
    }

    public void draw(MFGraphics g) {
        if (!this.dead) {
            int i;
            for (i = this.pos.length - 1; i >= 0; i--) {
                switch (i) {
                    case 0:
                        this.drawer.setActionId(0);
                        break;
                    case 1:
                    case 2:
                        this.drawer.setActionId(1);
                        break;
                    case 3:
                    case 4:
                        this.drawer.setActionId(2);
                        break;
                }
                if (this.pos[i][0] == this.circleCenterX - (this.mWidth >> 1)) {
                    this.drawer.setTrans(2);
                } else if (this.pos[i][0] == this.circleCenterX + (this.mWidth >> 1)) {
                    this.drawer.setTrans(0);
                }
                drawInMap(g, this.drawer, this.pos[i][0], this.pos[i][1]);
            }
            for (CaterpillarBody draw : this.body) {
                draw.draw(g);
            }
            drawCollisionRect(g);
        }
    }

    public void refreshCollisionRect(int x, int y) {
        if (this.pos == null) {
            this.pos = (int[][]) Array.newInstance(Integer.TYPE, new int[]{5, 2});
        }
        this.leftx = this.pos[0][0] - 512;
        this.lefty = this.pos[0][1] - 512;
        this.rightx = this.pos[0][0] + 512;
        this.righty = this.pos[0][1] + 512;
        for (int i = 1; i < this.pos.length; i++) {
            if (this.pos[i][0] - 512 < this.leftx) {
                this.leftx = this.pos[i][0] - 512;
            }
            if (this.pos[i][1] - 512 < this.lefty) {
                this.lefty = this.pos[i][1] - 512;
            }
            if (this.pos[i][0] + 512 > this.rightx) {
                this.rightx = this.pos[i][0] + 512;
            }
            if (this.pos[i][1] + 512 > this.righty) {
                this.righty = this.pos[i][1] + 512;
            }
        }
        this.collisionRect.setRect(this.leftx, this.lefty, this.rightx - this.leftx, this.righty - this.lefty);
    }

    public void close() {
        this.pos = null;
        if (this.body != null) {
            for (int i = 0; i < this.body.length; i++) {
                this.body[i] = null;
            }
        }
        this.body = null;
        super.close();
    }

    public void setDead() {
        for (CaterpillarBody caterpillarBody : this.body) {
            caterpillarBody.dead = true;
        }
        this.dead = true;
    }
}
