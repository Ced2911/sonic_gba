package SonicGBA;

import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: GimmickObject */
class FallingPlatform extends Platform {
    private static final int FALLING_COUNT = 20;
    private int fallingCount;
    private int posOriginalX;
    private int posOriginalY;
    private int velocity;

    protected FallingPlatform(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        this.velocity = 0;
        this.fallingCount = 20;
        this.posOriginalX = this.posX;
        this.posOriginalY = this.posY;
        this.velocity = 0;
    }

    public void logic() {
        refreshCollisionRect(this.posX, this.posY);
        if (this.fallingCount > 0 && this.used) {
            this.fallingCount--;
        }
        if (this.fallingCount == 0) {
            this.velocity += GRAVITY;
        }
        if (player.isFootOnObject(this)) {
            this.offsetY = 192;
        } else {
            this.offsetY = 0;
        }
        if (StageManager.getCurrentZoneId() >= 3 && StageManager.getCurrentZoneId() <= 6) {
            this.offsetY2 = GimmickObject.PLATFORM_OFFSET_Y;
        }
        checkWithPlayer(this.posX, this.posY, this.posX, this.posY + this.velocity);
        this.posY += this.velocity;
    }

    public void doInitWhileInCamera() {
    }

    public boolean checkInit() {
        if ((Math.abs(player.getFootPositionX() - this.posOriginalX) >> 6) < (MapManager.CAMERA_WIDTH >> 1) || (Math.abs(player.getFootPositionY() - this.posOriginalY) >> 6) < (MapManager.CAMERA_HEIGHT >> 1)) {
            return false;
        }
        this.posY = this.posOriginalY;
        this.used = false;
        this.fallingCount = 20;
        this.velocity = 0;
        refreshCollisionRect(this.posX, this.posY);
        return false;
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 1536, (((COLLISION_OFFSET_Y + y) - 768) + this.offsetY) + this.offsetY2, 3072, COLLISION_HEIGHT);
    }

    public boolean collisionChkWithObject(PlayerObject object) {
        CollisionRect objectRect = object.getCollisionRect();
        CollisionRect thisRect = getCollisionRect();
        rectV.setRect(objectRect.x0 + 192, objectRect.y0 + this.offsetY2, objectRect.getWidth() - PlayerSonic.BACK_JUMP_SPEED_X, objectRect.getHeight());
        return thisRect.collisionChk(rectV);
    }

    public void draw(MFGraphics g) {
        super.draw(g);
    }
}
