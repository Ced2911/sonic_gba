package SonicGBA;

import GameEngine.Def;
import Lib.MyAPI;
import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;

/* compiled from: BackGroundManager */
class BackManagerStage2 extends BackGroundManager {
    private static final String FILE_NAME_1 = "/stage2_bg_0.png";
    private static final String FILE_NAME_2 = "/stage2_bg_1.png";
    private static final int[][] IMAGE_2_CUT_N = null;
    private static int IMAGE_WIDTH = 256;
    private static final int[] LINE_2_OFFSET_N = new int[]{64, 64, 128};
    private static int STAGE_BG_WIDTH = 256;
    private static final int X_DRAW_NUM = ((Def.SCREEN_WIDTH + ((STAGE_BG_WIDTH - 1) * 2)) / STAGE_BG_WIDTH);
    private MFImage image1;
    private MFImage[] imageBG2;

    /*
    static {
        r0 = new int[3][];
        int[] iArr = new int[4];
        iArr[2] = 256;
        iArr[3] = 64;
        r0[0] = iArr;
        int[] iArr2 = new int[]{64, 256, 16, iArr2};
        iArr2 = new int[]{80, 256, 24, iArr2};
        IMAGE_2_CUT_N = r0;
    }
    */

    public BackManagerStage2() {
        try {
            if (stageId == 2) {
                this.image1 = MFImage.createImage("/map/stage2_bg_0.png");
                IMAGE_WIDTH = MyAPI.zoomIn(this.image1.getWidth(), true);
            }
            this.imageBG2 = new MFImage[8];
            this.imageBG2[0] = MFImage.createImage("/map/stage2_bg_1/#1.png");
            for (int i = 1; i < this.imageBG2.length; i++) {
                this.imageBG2[i] = MFImage.createPaletteImage("/map/stage2_bg_1/#" + (i + 1) + ".pal");
            }
            STAGE_BG_WIDTH = MyAPI.zoomIn(this.imageBG2[0].getWidth(), true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void close() {
        this.image1 = null;
        if (this.imageBG2 != null) {
            for (int i = 0; i < this.imageBG2.length; i++) {
                this.imageBG2[i] = null;
            }
        }
        this.imageBG2 = null;
    }

    public void draw(MFGraphics g) {
        if (stageId == 2) {
            drawStage1(g);
        } else {
            drawStage2(g);
        }
    }

    public void drawStage1(MFGraphics g) {
        int cameraX = MapManager.getCamera().f13x;
        if (cameraX <= 1456) {
            g.setColor(1054752);
            MyAPI.fillRect(g, DRAW_X, DRAW_Y, DRAW_WIDTH, DRAW_HEIGHT);
            for (int j = 0; j < MapManager.CAMERA_WIDTH; j += IMAGE_WIDTH) {
                MyAPI.drawImage(g, this.image1, (0 - (cameraX / 74)) + j, SCREEN_HEIGHT >> 1, 6);
            }
            return;
        }
        drawType2(g);
    }

    public void drawStage2(MFGraphics g) {
        drawType2(g);
    }

    public void drawType2(MFGraphics g) {
        int x;
        int y2 = 0;
        int tmpframe = (frame % (this.imageBG2.length * 3)) / 3;
        int cameraX = MapManager.getCamera().f13x;
        int cameraY = MapManager.getCamera().f14y;
        int startX = -((cameraX / 4) % STAGE_BG_WIDTH);
        int startY = -((cameraY / 4) % 64);
        for (x = 0; x < X_DRAW_NUM; x++) {
            for (int y = 0; y < 6; y++) {
                MyAPI.drawImage(g, this.imageBG2[tmpframe], IMAGE_2_CUT_N[0][0], IMAGE_2_CUT_N[0][1], IMAGE_2_CUT_N[0][2], IMAGE_2_CUT_N[0][3], 0, (STAGE_BG_WIDTH * x) + startX, (y * 64) + startY, 20);
            }
        }
        startX = -((cameraX / 2) % STAGE_BG_WIDTH);
        startY = (-((cameraY / 2) % 256)) + 112;
        for (x = 0; x < X_DRAW_NUM; x++) {
            int i = 0;
            y2 = startY;
            while (y2 < DRAW_HEIGHT + 16) {
                if (y2 > -16) {
                    MyAPI.drawImage(g, this.imageBG2[tmpframe], IMAGE_2_CUT_N[1][0], IMAGE_2_CUT_N[1][1], IMAGE_2_CUT_N[1][2], IMAGE_2_CUT_N[1][3], 0, (STAGE_BG_WIDTH * x) + startX, y2, 20);
                }
                y2 += LINE_2_OFFSET_N[i];
                i = (i + 1) % LINE_2_OFFSET_N.length;
            }
        }
        startX = -(((cameraX * 2) / 3) % STAGE_BG_WIDTH);
        startY = (-(((cameraY * 2) / 3) % 256)) + 200;
        for (x = 0; x < X_DRAW_NUM; x++) {
            for (y2 = startY; y2 < DRAW_HEIGHT + 24; y2 += 256) {
                if (y2 > -24) {
                    MyAPI.drawImage(g, this.imageBG2[tmpframe], IMAGE_2_CUT_N[2][0], IMAGE_2_CUT_N[2][1], IMAGE_2_CUT_N[2][2], IMAGE_2_CUT_N[2][3], 0, (STAGE_BG_WIDTH * x) + startX, y2, 20);
                }
            }
        }
    }
}
