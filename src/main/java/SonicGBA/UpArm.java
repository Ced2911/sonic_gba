package SonicGBA;

import GameEngine.Key;
import Lib.SoundSystem;
import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;

/* compiled from: GimmickObject */
class UpArm extends GimmickObject {
    private static final int ARM_HEIGHT = 19;
    private static final int ARM_OFFSET = 14;
    private static final int ARM_PULL_OFFSET_Y = 128;
    private static final int ARM_WIDTH = 24;
    private static final int ARM_X = 0;
    private static final int ARM_Y = 32;
    private static final int BAR_HEIGHT = 32;
    private static final int BAR_WIDTH = 4;
    private static final int BAR_X = 10;
    private static final int BAR_Y = 0;
    private static final int COLLISION_HEIGHT = 320;
    private static final int COLLISION_WIDTH = 896;
    private static final int DOWNVELOCITY = 960;
    private static final int DRAW_HEIGHT = 3264;
    private static final int DRAW_WIDTH = 1536;
    private static final int MOVE_DISTANCE = 7680;
    private static final byte STATE_PULL = (byte) 1;
    private static final byte STATE_RETURN = (byte) 3;
    private static final byte STATE_WAIT = (byte) 0;
    private static final byte STATE_WAIT_2 = (byte) 2;
    private static final int UPVELOCITY = 480;
    private static final int WAIT_COUNT_2 = 15;
    private static int frame;
    private static MFImage image;
    private CollisionRect collisionRect2;
    private byte state;
    private int upLimit = (this.posY - MOVE_DISTANCE);
    private int waitCount;

    protected UpArm(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        this.posX += 128;
        if (image == null) {
            try {
                image = MFImage.createImage("/gimmick/up_arm.png");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void draw(MFGraphics g) {
        int y = this.upLimit;
        while (y < (this.posY - COLLISION_WIDTH) - 2048) {
            drawInMap(g, image, 10, 0, 4, 32, 0, this.posX, y, 17);
            y += 2048;
        }
        if (y < this.posY - COLLISION_WIDTH) {
            drawInMap(g, image, 10, 0, 4, ((this.posY - COLLISION_WIDTH) - y) >> 6, 0, this.posX, y, 17);
        }
        drawInMap(g, image, 0, 32, 24, 19, 0, this.posX, this.posY - COLLISION_WIDTH, 17);
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (this.collisionRect2.collisionChk(object.collisionRect)) {
            switch (this.state) {
                case (byte) 0:
                    this.state = (byte) 1;
                    player.resetPlayerDegree();
                    object.setOutOfControl(this);
                    player.doPullMotion(this.posX, this.posY + 128);
                    SoundSystem.getInstance().playSe(51);
                    frame = 1;
                    return;
                default:
                    return;
            }
        }
    }

    public void logic() {
        if (this.waitCount > 0) {
            this.waitCount--;
        }
        switch (this.state) {
            case (byte) 1:
                frame++;
                frame %= 100;
                this.posY -= UPVELOCITY;
                if (this.posY < this.upLimit) {
                    this.posY = this.upLimit;
                    soundInstance.stopLoopSe();
                } else if (frame == 4) {
                    soundInstance.playLoopSe(52);
                }
                player.doPullMotion(this.posX, this.posY + 128);
                if (this.posY == this.upLimit && Key.press(Key.gUp | 16777216)) {
                    player.outOfControl = false;
                    player.doJump();
                    player.isOnlyJump = true;
                    this.state = (byte) 2;
                    this.waitCount = 15;
                    soundInstance.stopLoopSe();
                    return;
                }
                return;
            case (byte) 2:
                if (this.waitCount == 0) {
                    this.state = (byte) 3;
                    return;
                }
                return;
            case (byte) 3:
                this.posY += 960;
                if (this.posY >= this.upLimit + MOVE_DISTANCE) {
                    this.posY = this.upLimit + MOVE_DISTANCE;
                    this.state = (byte) 0;
                    player.isOnlyJump = false;
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 768, this.upLimit, 1536, y - this.upLimit <= 0 ? 1 : y - this.upLimit);
        if (this.collisionRect2 == null) {
            this.collisionRect2 = new CollisionRect();
        }
        this.collisionRect2.setRect(x - 448, y, COLLISION_WIDTH, COLLISION_HEIGHT);
    }
}
