package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import Lib.SoundSystem;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: BulletObject */
class MissileBullet extends BulletObject {
    private static final int COLLISION_HEIGHT = 768;
    private static final int COLLISION_WIDTH = 1920;
    private static final int SIDE_RIGHT = 607744;
    private static final int SIDE_LEFT = (SIDE_RIGHT - (SCREEN_WIDTH << 6));
    private AnimationDrawer boomdrawer;
    private int flyCounter = 0;
    private int frame;
    boolean isBoom = false;
    private boolean isboomed;
    private boolean isbooming;
    private int mvel;
    private int velA = 64;
    private int velA2 = 256;

    protected MissileBullet(int x, int y, int velX, int velY) {
        super(x, y, velX, velY, true);
        if (missileAnimation == null) {
            missileAnimation = new Animation("/animation/missile");
        }
        if (velX > 0) {
            this.velA = Math.abs(this.velA);
            this.velA2 = Math.abs(this.velA2);
            this.drawer = missileAnimation.getDrawer(0, true, 2);
        } else if (velX < 0) {
            this.velA = -Math.abs(this.velA);
            this.velA2 = -Math.abs(this.velA2);
            this.drawer = missileAnimation.getDrawer(0, true, 0);
        }
        this.mvel = velX;
        if (boomAnimation == null) {
            boomAnimation = new Animation("/animation/boom");
        }
        this.boomdrawer = boomAnimation.getDrawer(0, true, 0);
        this.isboomed = false;
        SoundSystem.getInstance().playSequenceSe(68);
    }

    private void missleNoiseControl() {
        if ((IsHitted() || !isInCamera()) && SoundSystem.getInstance().getPlayingLoopSeIndex() == 68) {
            SoundSystem.getInstance().stopLoopSe();
        }
        if (IsHitted() && !this.isboomed) {
            this.isboomed = true;
        }
    }

    public void bulletLogic() {
        int preX = this.posX;
        int preY = this.posY;
        missleNoiseControl();
        if (IsHitted()) {
            this.boomdrawer.setActionId(0);
            this.boomdrawer.setLoop(false);
            this.isbooming = true;
        } else {
            this.flyCounter++;
            if (this.flyCounter < 8) {
                this.velX += this.velA;
            } else {
                this.velX += this.velA2;
            }
            this.posX += this.velX;
            this.velY = -((this.posY - player.getCheckPositionY()) >> 3);
            this.posY += this.velY;
        }
        checkWithPlayer(preX, preY, this.posX, this.posY);
    }

    public boolean screenOutChek() {
        if (this.mvel > 0) {
            if (this.posX > SIDE_RIGHT) {
                return true;
            }
            return false;
        } else if (this.posX < SIDE_LEFT) {
            return true;
        } else {
            return false;
        }
    }

    public boolean chkDestroy() {
        return super.chkDestroy() || this.boomdrawer.checkEnd() || screenOutChek() || !isInCamera();
    }

    public void draw(MFGraphics g) {
        if (!this.isbooming) {
            drawInMap(g, this.drawer);
        }
        if (this.isbooming && !this.boomdrawer.checkEnd()) {
            drawInMap(g, this.boomdrawer);
        }
        this.collisionRect.draw(g, camera);
    }

    public void refreshCollisionRect(int x, int y) {
        if (this.posX < player.getFootPositionX()) {
            this.collisionRect.setRect(x - COLLISION_WIDTH, y - PlayerSonic.BACK_JUMP_SPEED_X, COLLISION_WIDTH, 768);
        } else {
            this.collisionRect.setRect(x, y - PlayerSonic.BACK_JUMP_SPEED_X, COLLISION_WIDTH, 768);
        }
    }

    public void close() {
        super.close();
        this.boomdrawer = null;
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        super.doWhileCollision(object, direction);
        if (!this.isBoom) {
            SoundSystem.getInstance().playSe(35);
            this.isBoom = true;
        }
    }
}
