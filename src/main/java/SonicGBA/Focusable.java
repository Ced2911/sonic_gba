package SonicGBA;

public interface Focusable {
    int getFocusX();

    int getFocusY();
}
