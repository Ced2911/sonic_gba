package SonicGBA;

import Lib.Animation;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: EnemyObject */
class Motor extends EnemyObject {
    private static final int ALERT_HEIGHT = 20;
    private static final int ALERT_RANGE = 6144;
    private static final int ALERT_WIDTH = 88;
    private static final int COLLISION_HEIGHT = 2048;
    private static final int COLLISION_WIDTH = 2048;
    private static final int STATE_ATTACK = 1;
    private static final int STATE_MOVE = 0;
    private static Animation motorAnimation;
    private int alert_state;
    private int attck_cnt;
    private int limitLeftX;
    private int limitRightX;
    private int release_cnt;
    private int release_cnt_max;
    private int starty;
    private int state;
    private int velocity;

    public static void releaseAllResource() {
        Animation.closeAnimation(motorAnimation);
        motorAnimation = null;
    }

    protected Motor(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        this.velocity = 192;
        this.attck_cnt = 0;
        this.release_cnt = 0;
        this.release_cnt_max = 5;
        this.limitLeftX = this.posX;
        this.limitRightX = this.posX + this.mWidth;
        this.starty = this.posY;
        this.posX = this.limitLeftX + (this.mWidth >> 1);
        if (motorAnimation == null) {
            motorAnimation = new Animation("/animation/motor");
        }
        this.drawer = motorAnimation.getDrawer(1, true, 0);
    }

    public void logic() {
        if (!this.dead) {
            this.alert_state = checkPlayerInEnemyAlertRange(this.posX >> 6, this.posY >> 6, 88, 20);
            int preX = this.posX;
            int preY = this.posY;
            switch (this.state) {
                case 0:
                    if (this.velocity > 0) {
                        this.posX += this.velocity;
                        this.drawer.setActionId(0);
                        this.drawer.setTrans(2);
                        if (this.posX >= this.limitRightX) {
                            this.posX = this.limitRightX;
                            this.velocity = -this.velocity;
                            this.drawer.setActionId(1);
                            this.drawer.setTrans(0);
                        }
                    } else {
                        this.posX += this.velocity;
                        this.drawer.setActionId(0);
                        this.drawer.setTrans(0);
                        if (this.posX <= this.limitLeftX) {
                            this.posX = this.limitLeftX;
                            this.velocity = -this.velocity;
                            this.drawer.setActionId(1);
                            this.drawer.setTrans(0);
                        }
                    }
                    if (this.release_cnt < this.release_cnt_max) {
                        this.release_cnt++;
                    }
                    if (this.alert_state == 0 && this.release_cnt == this.release_cnt_max && this.posX != this.limitLeftX && this.posX != this.limitRightX) {
                        if (this.posX < player.getCheckPositionX()) {
                            if (this.drawer.getTransId() == 2 && this.drawer.getActionId() == 0) {
                                this.state = 1;
                                this.attck_cnt = 0;
                            }
                        } else if (this.posX > player.getCheckPositionX() && this.drawer.getTransId() == 0 && this.drawer.getActionId() == 0) {
                            this.state = 1;
                            this.attck_cnt = 0;
                        }
                    }
                    this.posY = getGroundY(this.posX, this.posY);
                    checkWithPlayer(preX, preY, this.posX, this.posY);
                    return;
                case 1:
                    if (this.drawer.getTransId() == 2 && this.drawer.getActionId() == 0) {
                        if (this.velocity < 0) {
                            this.velocity = -this.velocity;
                        }
                        if (this.attck_cnt < 5) {
                            this.attck_cnt++;
                        } else {
                            this.posX += this.velocity * 3;
                            if (this.posX >= this.limitRightX) {
                                this.posX = this.limitRightX;
                            }
                        }
                    }
                    if (this.drawer.getTransId() == 0 && this.drawer.getActionId() == 0) {
                        if (this.velocity > 0) {
                            this.velocity = -this.velocity;
                        }
                        if (this.attck_cnt < 5) {
                            this.attck_cnt++;
                        } else {
                            this.posX += this.velocity * 3;
                            if (this.posX <= this.limitLeftX) {
                                this.posX = this.limitLeftX;
                            }
                        }
                    }
                    if (this.posX == this.limitLeftX || this.posX == this.limitRightX) {
                        this.state = 0;
                        this.release_cnt = 0;
                    }
                    this.posY = getGroundY(this.posX, this.posY);
                    checkWithPlayer(preX, preY, this.posX, this.posY);
                    return;
                default:
                    return;
            }
        }
    }

    public void draw(MFGraphics g) {
        if (!this.dead) {
            drawInMap(g, this.drawer);
            drawCollisionRect(g);
        }
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 1024, y - 2048, 2048, 2048);
    }
}
