package SonicGBA;

import GameEngine.Def;
import Lib.Animation;
import Lib.AnimationDrawer;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: GimmickObject */
class Belt extends GimmickObject {
    private static final int DRAW_Y_OFFSET = 1024;
    private static final int MOVE_SPEED = 128;
    private static final int SNOW_MAX_SPEED = 118;
    private static final int SPIN_DASH_ATTENUATE_PERCENTAGE = 82;
    public static Animation beltAnimation;
    private AnimationDrawer drawer;
    private boolean isActived;

    protected Belt(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        switch (StageManager.getCurrentZoneId()) {
            case 4:
                this.isActived = false;
                return;
            default:
                if (beltAnimation == null) {
                    beltAnimation = new Animation("/animation/conveyor_" + StageManager.getCurrentZoneId() + (StageManager.getCurrentZoneId() == 6 ? (StageManager.getStageID() - 9) : ""));
                }
                if (beltAnimation != null) {
                    int kind;
                    if (StageManager.getCurrentZoneId() == 6) {
                        kind = (width / 2 < 6 ? 0 : 2) + (this.iLeft == 0 ? 0 : 1);
                    } else {
                        kind = (width / 2 == 6 ? 0 : 2) + (this.iLeft == 0 ? 0 : 1);
                    }
                    this.drawer = beltAnimation.getDrawer(kind, true, 0);
                }
                this.isActived = false;
                return;
        }
    }

    public void draw(MFGraphics g) {
        switch (StageManager.getCurrentZoneId()) {
            case 4:
                break;
            case 6:
                if (this.iTop != 1) {
                    drawInMap(g, this.drawer, this.posX + (this.mWidth >> 1), this.posY + 1024);
                    break;
                }
                break;
            default:
                drawInMap(g, this.drawer, this.posX + (this.mWidth >> 1), this.posY + 1024);
                break;
        }
        drawCollisionRect(g);
    }

    public void logic() {
        switch (StageManager.getCurrentZoneId()) {
            case 4:
                if (this.collisionRect.collisionChk(player.getCollisionRect()) && player.collisionState == (byte) 0) {
                    player.isStopByObject = true;
                    int animationId = player.getAnimationId();
                    PlayerObject playerObject = player;
                    if (animationId == 4) {
                        if (player.getVelX() > 118) {
                            player.setVelXPercent(82);
                        } else if (player.getVelX() < -118) {
                            player.setVelXPercent(82);
                        }
                    } else if ((!(player instanceof PlayerSonic) || player.getCharacterAnimationID() < 13 || player.getCharacterAnimationID() > 15) && !((player instanceof PlayerAmy) && player.myAnimationID == 7)) {
                        if (player.getVelX() > 118) {
                            player.setVelX(118);
                        } else if (player.getVelX() < -118) {
                            player.setVelX(-118);
                        }
                    }
                    this.isActived = true;
                    player.isInSnow = true;
                    return;
                }
                return;
            default:
                if (StageManager.getStageID() != 10 && this.collisionRect.collisionChk(player.getCollisionRect()) && player.collisionState == (byte) 0) {
                    player.speedLock = true;
                    player.moveOnObject(player.footPointX + (this.iLeft == 0 ? Def.TOUCH_HELP_LEFT_X : 128), player.footPointY);
                    if (player.getAnimationId() != 47) {
                        player.getAnimationId();
                    }
                    this.isActived = true;
                    return;
                }
                return;
        }
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if ((player instanceof PlayerKnuckles) && player.collisionState == (byte) 4) {
            player.collisionState = (byte) 1;
            player.animationID = 1;
            soundInstance.stopLoopSe();
        }
    }

    public void doWhileNoCollision() {
        if (this.isActived) {
            switch (StageManager.getCurrentZoneId()) {
                case 4:
                    player.isInSnow = false;
                    player.isStopByObject = false;
                    break;
                default:
                    if (player.getAnimationId() == 47 || player.getAnimationId() == 48) {
                        player.setAnimationId(0);
                    }
                    player.speedLock = false;
                    break;
            }
            this.isActived = false;
        }
    }

    public void close() {
        this.drawer = null;
    }

    public static void releaseAllResource() {
        Animation.closeAnimation(beltAnimation);
        beltAnimation = null;
    }

    public int getPaintLayer() {
        return 0;
    }
}
