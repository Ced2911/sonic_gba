package SonicGBA;

/* compiled from: GimmickObject */
class RopeTurn extends GimmickObject {
    protected RopeTurn(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
    }

    public void doWhileRail(PlayerObject object, int direction) {
        if (!this.used && object.outOfControl && (object.outOfControlObject instanceof RopeStart)) {
            RopeStart rope = (RopeStart)object.outOfControlObject;
            if (rope.degree > 90) {
                rope.posX = this.posX;
                rope.posY = this.posY;
                rope.turn();
                this.used = true;
            }
        }
    }

    public void doWhileNoCollision() {
        this.used = false;
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 512, y, 1024, BarHorbinV.COLLISION_WIDTH);
    }
}
