package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import Lib.SoundSystem;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: EnemyObject */
class Boss1 extends BossObject {
    private static final int ALERT_RANGE = 6912;
    private static final int BOSS_MOVE_LIMIT_LEFT = 670336;
    private static final int BOSS_MOVE_LIMIT_RIGHT = 702336;
    private static final int BOSS_WAKEN_POINT = 686080;
    private static final int CAMERA_SET_POINT = 678912;
    private static final int CAMERA_SIDE_LEFT = 10544;
    private static final int CAMERA_SIDE_RIGHT = 10904;
    private static final int CAR_HURT = 1;
    private static final int CAR_MOVE = 0;
    private static final int COLLISION_HEIGHT = 3840;
    private static final int COLLISION_WIDTH = 4608;
    private static final int DEGREE_MAX = 23680;
    private static final int DEGREE_MAX_2 = 22400;
    private static final int DEGREE_MIN = 10880;
    private static final int DEGREE_MIN_2 = 12160;
    private static final int FACE_HURT = 2;
    private static final int FACE_NORMAL = 0;
    private static final int FACE_SMILE = 1;
    private static final int STATE_ATTACK_1 = 1;
    private static final int STATE_ATTACK_2 = 3;
    private static final int STATE_ATTACK_3 = 4;
    private static final int STATE_BROKEN = 5;
    private static final int STATE_ESCAPE = 6;
    private static final int STATE_READY = 2;
    private static final int STATE_WAIT = 0;
    private static Animation boatAni = null;
    private static Animation brokencarAni = null;
    private static Animation carAni = null;
    private static final int cnt_max = 8;
    private static Animation escapefaceAni = null;
    private static Animation faceAni = null;
    private static final int stop_wait_cnt_max = 32;
    private int[] ArmSharpPos = new int[2];
    private boolean IsBreaking;
    private boolean IsStopWait = false;
    private int WaitCnt;
    private int alert_state;
    private Boss1Arm arm;
    private int ball_size = 1536;
    private AnimationDrawer boatdrawer;
    private BossBroken bossbroken;
    private AnimationDrawer brokencardrawer;
    private int car_cnt;
    private int car_state;
    private AnimationDrawer cardrawer;
    private int con_size = BarHorbinV.HOBIN_POWER;
    private int degree = 0;
    private int dg_plus = 320;
    private int drop_cnt;
    private int escape_v = 512;
    private AnimationDrawer escapefacedrawer;
    private int face_cnt;
    private int face_state;
    private AnimationDrawer facedrawer;
    private int fly_end;
    private int fly_top;
    private int fly_top_range = COLLISION_HEIGHT;
    private int flywheel_lx;
    private int flywheel_rx;
    private int flywheel_vx;
    private int flywheel_vy;
    private int flywheel_y;
    private int limitLeftX;
    private int limitRightX;
    private int offsetY = 2112;
    private int plus = 1;
    private int range = 44800;
    private int side_left = 0;
    private int side_right = 0;
    private int state;
    private int stop_wait_cnt = 0;
    private int velX = 0;
    private int velY = 0;
    private int velocity = -240;
    private int wait_cnt;
    private int wait_cnt_max = 10;

    public static void releaseAllResource() {
        Animation.closeAnimation(carAni);
        Animation.closeAnimation(faceAni);
        Animation.closeAnimation(brokencarAni);
        Animation.closeAnimation(BoomAni);
        Animation.closeAnimation(boatAni);
        Animation.closeAnimation(escapefaceAni);
        carAni = null;
        faceAni = null;
        brokencarAni = null;
        BoomAni = null;
        boatAni = null;
        escapefaceAni = null;
    }

    protected Boss1(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        this.posX -= this.iLeft * 8;
        this.posY -= this.iTop * 8;
        this.limitRightX = BOSS_MOVE_LIMIT_RIGHT;
        this.limitLeftX = BOSS_MOVE_LIMIT_LEFT;
        refreshCollisionRect(this.posX >> 6, this.posY >> 6);
        if (carAni == null) {
            carAni = new Animation("/animation/boss1_car");
        }
        this.cardrawer = carAni.getDrawer(0, true, 0);
        if (faceAni == null) {
            faceAni = new Animation("/animation/boss1_egg");
        }
        this.facedrawer = faceAni.getDrawer(0, true, 0);
        if (brokencarAni == null) {
            brokencarAni = new Animation("/animation/boss1_body");
        }
        this.brokencardrawer = brokencarAni.getDrawer(2, true, 0);
        if (boatAni == null) {
            boatAni = new Animation("/animation/pod_boat");
        }
        this.boatdrawer = boatAni.getDrawer(0, true, 0);
        if (escapefaceAni == null) {
            escapefaceAni = new Animation("/animation/pod_face");
        }
        this.escapefacedrawer = escapefaceAni.getDrawer(4, true, 0);
        this.arm = new Boss1Arm(31, x, y, left, top, width, height);
        GameObject.addGameObject(this.arm, x, y);
        this.IsBreaking = false;
        this.WaitCnt = 0;
        this.IsStopWait = false;
        setBossHP();
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void doWhileCollision(SonicGBA.PlayerObject r6, int r7) {
        /*
        r5 = this;
        r1 = 5;
        r4 = 6;
        r3 = 2;
        r2 = 1;
        r0 = r5.dead;
        if (r0 == 0) goto L_0x0009;
    L_0x0008:
        return;
    L_0x0009:
        r0 = r5.state;
        if (r0 == r1) goto L_0x0008;
    L_0x000d:
        r0 = r5.state;
        if (r0 == r4) goto L_0x0008;
    L_0x0011:
        r0 = player;
        if (r6 != r0) goto L_0x0008;
    L_0x0015:
        r0 = player;
        r0 = r0.isAttackingEnemy();
        if (r0 == 0) goto L_0x0072;
    L_0x001d:
        r0 = r5.face_state;
        if (r0 != r3) goto L_0x0044;
    L_0x0021:
        r0 = r5.car_cnt;
        r1 = 8;
        if (r0 != r1) goto L_0x0044;
    L_0x0027:
        r0 = player;
        r0 = r0.getAnimationId();
        r1 = player;
        if (r0 == r4) goto L_0x003c;
    L_0x0031:
        r0 = player;
        r0 = r0.getAnimationId();
        r1 = player;
        r1 = 7;
        if (r0 != r1) goto L_0x0044;
    L_0x003c:
        r0 = player;
        r0.beHurt();
        r5.face_state = r2;
        goto L_0x0008;
    L_0x0044:
        r0 = r5.HP;
        if (r0 <= 0) goto L_0x0008;
    L_0x0048:
        r0 = r5.face_state;
        if (r0 == r3) goto L_0x0008;
    L_0x004c:
        r0 = r5.HP;
        r0 = r0 - r2;
        r5.HP = r0;
        r0 = player;
        r0.doBossAttackPose(r5, r7);
        r5.face_state = r3;
        r5.car_state = r2;
        r0 = r5.HP;
        if (r0 != 0) goto L_0x0068;
    L_0x005e:
        r0 = Lib.SoundSystem.getInstance();
        r1 = 35;
        r0.playSe(r1);
        goto L_0x0008;
    L_0x0068:
        r0 = Lib.SoundSystem.getInstance();
        r1 = 34;
        r0.playSe(r1);
        goto L_0x0008;
    L_0x0072:
        r0 = r5.state;
        if (r0 == r1) goto L_0x0008;
    L_0x0076:
        r0 = r5.state;
        if (r0 == r4) goto L_0x0008;
    L_0x007a:
        r0 = player;
        r0 = r0.canBeHurt();
        if (r0 == 0) goto L_0x0008;
    L_0x0082:
        r0 = player;
        r0.beHurt();
        r5.face_state = r2;
        goto L_0x0008;
        */
        throw new UnsupportedOperationException("Method not decompiled: SonicGBA.Boss1.doWhileCollision(SonicGBA.PlayerObject, int):void");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void doWhileBeAttack(SonicGBA.PlayerObject r5, int r6, int r7) {
        /*
        r4 = this;
        r3 = 2;
        r2 = 1;
        r0 = r4.face_state;
        if (r0 != r3) goto L_0x002a;
    L_0x0006:
        r0 = r4.car_cnt;
        r1 = 8;
        if (r0 != r1) goto L_0x002a;
    L_0x000c:
        r0 = player;
        r0 = r0.getAnimationId();
        r1 = player;
        r1 = 6;
        if (r0 == r1) goto L_0x0022;
    L_0x0017:
        r0 = player;
        r0 = r0.getAnimationId();
        r1 = player;
        r1 = 7;
        if (r0 != r1) goto L_0x002a;
    L_0x0022:
        r0 = player;
        r0.beHurt();
        r4.face_state = r2;
    L_0x0029:
        return;
    L_0x002a:
        r0 = r4.HP;
        if (r0 <= 0) goto L_0x0029;
    L_0x002e:
        r0 = r4.face_state;
        if (r0 == r3) goto L_0x0029;
    L_0x0032:
        r0 = r4.HP;
        r0 = r0 - r2;
        r4.HP = r0;
        r0 = player;
        r0.doBossAttackPose(r4, r6);
        r4.face_state = r3;
        r4.car_state = r2;
        r0 = r4.HP;
        if (r0 != 0) goto L_0x004e;
    L_0x0044:
        r0 = Lib.SoundSystem.getInstance();
        r1 = 35;
        r0.playSe(r1);
        goto L_0x0029;
    L_0x004e:
        r0 = Lib.SoundSystem.getInstance();
        r1 = 34;
        r0.playSe(r1);
        goto L_0x0029;
        */
        throw new UnsupportedOperationException("Method not decompiled: SonicGBA.Boss1.doWhileBeAttack(SonicGBA.PlayerObject, int, int):void");
    }

    public void logic() {
        if (!this.dead) {
            int preX = this.posX;
            int preY = this.posY;
            int tmpX = this.posX;
            int tmpY = this.posY;
            if (this.HP == 1) {
                if (this.state == 1) {
                    this.state = 4;
                } else if (this.arm.getArmState() == 3 && !this.arm.getTurnState()) {
                    this.state = 3;
                    this.dg_plus = 914;
                    this.arm.setTurnState(true);
                    this.arm.setDegreeSpeed(this.dg_plus);
                } else if (this.arm.getArmState() == 4 && !this.arm.getTurnState()) {
                    this.state = 4;
                }
            } else if (this.HP == 0 && !this.IsBreaking) {
                this.state = 5;
                changeAniState(this.facedrawer, 2);
                changeAniState(this.brokencardrawer, 2);
                this.posY -= this.con_size;
                this.velY = -600;
                if (player.getVelX() < 0) {
                    this.velX = -150;
                } else {
                    this.velX = 150;
                }
                if (this.velocity > 0) {
                    this.flywheel_lx = this.posX - 147456;
                    this.flywheel_rx = this.posX + 147456;
                } else {
                    this.flywheel_lx = this.posX + 147456;
                    this.flywheel_rx = this.posX - 147456;
                }
                this.flywheel_y = this.posY - this.con_size;
                this.flywheel_vx = 300;
                this.flywheel_vy = SmallAnimal.FLY_VELOCITY_Y;
                this.bossbroken = new BossBroken(22, this.posX >> 6, this.posY >> 6, 0, 0, 0, 0);
                GameObject.addGameObject(this.bossbroken, this.posX >> 6, this.posY >> 6);
                this.IsBreaking = true;
                this.side_left = MapManager.getCamera().f13x;
                this.side_right = this.side_left + MapManager.CAMERA_WIDTH;
                MapManager.setCameraLeftLimit(this.side_left);
                MapManager.setCameraRightLimit(this.side_right);
            }
            if (this.state > 0) {
                isBossEnter = true;
            }
            switch (this.state) {
                case 0:
                    if (player.getFootPositionX() >= CAMERA_SET_POINT) {
                        MapManager.setCameraLeftLimit(CAMERA_SIDE_LEFT);
                        MapManager.setCameraRightLimit(CAMERA_SIDE_RIGHT);
                    }
                    if (player.getFootPositionX() >= BOSS_WAKEN_POINT) {
                        this.IsStopWait = true;
                        bossFighting = true;
                        bossID = 22;
                        SoundSystem.getInstance().playBgm(22, true);
                    }
                    if (this.IsStopWait) {
                        if (this.stop_wait_cnt >= 32) {
                            this.state = 1;
                            break;
                        } else {
                            this.stop_wait_cnt++;
                            break;
                        }
                    }
                    break;
                case 1:
                case 2:
                    if (this.face_state != 0) {
                        if (this.face_cnt < 8) {
                            this.face_cnt++;
                        } else {
                            this.face_state = 0;
                            this.face_cnt = 0;
                        }
                    }
                    if (this.car_state == 1) {
                        if (this.car_cnt < 8) {
                            this.car_cnt++;
                        } else {
                            this.car_state = 0;
                            this.car_cnt = 0;
                        }
                    }
                    changeAniState(this.cardrawer, this.car_state);
                    changeAniState(this.facedrawer, this.face_state);
                    if (this.state != 4) {
                        if (this.velocity > 0) {
                            this.posX += this.velocity;
                            if (this.posX >= this.limitRightX) {
                                this.posX = this.limitRightX;
                                this.velocity = -this.velocity;
                            }
                        } else {
                            this.posX += this.velocity;
                            if (this.posX <= this.limitLeftX) {
                                this.posX = this.limitLeftX;
                                this.velocity = -this.velocity;
                            }
                        }
                    } else if (this.velocity > 0) {
                        if (this.posX >= this.limitRightX) {
                            this.posX = this.limitRightX;
                            this.velocity = -this.velocity;
                        }
                    } else if (this.posX <= this.limitLeftX) {
                        this.posX = this.limitLeftX;
                        this.velocity = -this.velocity;
                    }
                    this.arm.logic(this.posX, this.posY - this.offsetY, this.state, this.velocity);
                    break;
                case 3:
                    if (this.face_state != 0) {
                        if (this.face_cnt < 8) {
                            this.face_cnt++;
                        } else {
                            this.face_state = 0;
                            this.face_cnt = 0;
                        }
                    }
                    if (this.car_state == 1) {
                        if (this.car_cnt < 8) {
                            this.car_cnt++;
                        } else {
                            this.car_state = 0;
                            this.car_cnt = 0;
                        }
                    }
                    changeAniState(this.cardrawer, this.car_state);
                    changeAniState(this.facedrawer, this.face_state);
                    this.ArmSharpPos = this.arm.logic(this.posX, this.posY - this.offsetY, this.state, this.velocity);
                    this.posX = this.ArmSharpPos[0];
                    this.posY = this.ArmSharpPos[1] + this.offsetY;
                    break;
                case 4:
                    if (this.face_state != 0) {
                        if (this.face_cnt < 8) {
                            this.face_cnt++;
                        } else {
                            this.face_state = 0;
                            this.face_cnt = 0;
                        }
                    }
                    if (this.car_state == 1) {
                        if (this.car_cnt < 8) {
                            this.car_cnt++;
                        } else {
                            this.car_state = 0;
                            this.car_cnt = 0;
                        }
                    }
                    changeAniState(this.cardrawer, this.car_state);
                    changeAniState(this.facedrawer, this.face_state);
                    this.posX = tmpX;
                    this.posY = tmpY;
                    if (this.velocity > 0) {
                        this.posX += this.velocity;
                        if (this.posX >= this.limitRightX) {
                            this.posX = this.limitRightX;
                            this.velocity = -this.velocity;
                        }
                    } else {
                        this.posX += this.velocity;
                        if (this.posX <= this.limitLeftX) {
                            this.posX = this.limitLeftX;
                            this.velocity = -this.velocity;
                        }
                    }
                    this.arm.logic(this.posX, this.posY - this.offsetY, this.state, this.velocity);
                    break;
                case 5:
                    if (this.face_state != 0) {
                        if (this.face_cnt < 8) {
                            this.face_cnt++;
                        } else {
                            this.face_state = 0;
                            this.face_cnt = 0;
                        }
                    }
                    changeAniState(this.facedrawer, this.face_state);
                    if (this.posY + this.velY >= getGroundY(this.posX, this.posY)) {
                        this.posY = getGroundY(this.posX, this.posY);
                        switch (this.drop_cnt) {
                            case 0:
                                this.velY = -450;
                                this.drop_cnt = 1;
                                break;
                            case 1:
                                this.velY = SmallAnimal.FLY_VELOCITY_Y;
                                this.drop_cnt = 2;
                                break;
                        }
                    }
                    this.posX += this.velX;
                    this.velY += GRAVITY;
                    this.posY += this.velY;
                    if (this.flywheel_y - this.con_size >= getGroundY(this.flywheel_lx, this.flywheel_y)) {
                        this.flywheel_y = getGroundY(this.flywheel_lx, this.flywheel_y) + this.con_size;
                    } else {
                        if (this.velocity > 0) {
                            this.flywheel_lx -= this.flywheel_vx;
                            this.flywheel_rx += this.flywheel_vx;
                        } else {
                            this.flywheel_lx += this.flywheel_vx;
                            this.flywheel_rx -= this.flywheel_vx;
                        }
                        this.flywheel_vy += GRAVITY >> 1;
                        this.flywheel_y += this.flywheel_vy;
                    }
                    this.arm.logic(this.posX, this.posY, this.state, this.velocity);
                    this.bossbroken.logicBoom(this.posX, this.posY);
                    if (this.bossbroken.getEndState()) {
                        this.state = 6;
                        this.fly_top = this.posY;
                        this.fly_end = this.side_right;
                        bossFighting = false;
                        player.getBossScore();
                        SoundSystem.getInstance().playBgm(StageManager.getBgmId(), true);
                        break;
                    }
                    break;
                case 6:
                    this.wait_cnt++;
                    if (this.wait_cnt >= this.wait_cnt_max && this.posY >= this.fly_top - this.fly_top_range) {
                        this.posY -= this.escape_v;
                    }
                    if (this.posY <= this.fly_top - this.fly_top_range && this.WaitCnt == 0) {
                        this.posY = this.fly_top - this.fly_top_range;
                        this.escapefacedrawer.setActionId(0);
                        this.boatdrawer.setActionId(1);
                        this.boatdrawer.setLoop(false);
                        this.WaitCnt = 1;
                    }
                    if (this.WaitCnt == 1 && this.boatdrawer.checkEnd()) {
                        this.escapefacedrawer.setActionId(0);
                        this.escapefacedrawer.setTrans(2);
                        this.escapefacedrawer.setLoop(true);
                        this.boatdrawer.setActionId(1);
                        this.boatdrawer.setTrans(2);
                        this.boatdrawer.setLoop(false);
                        this.WaitCnt = 2;
                    }
                    if (this.WaitCnt == 2 && this.boatdrawer.checkEnd()) {
                        this.boatdrawer.setActionId(0);
                        this.boatdrawer.setTrans(2);
                        this.boatdrawer.setLoop(true);
                        this.WaitCnt = 3;
                    }
                    if (this.WaitCnt == 3 || this.WaitCnt == 4) {
                        this.posX += this.escape_v;
                    }
                    if (this.posX > (this.side_right << 6) && this.WaitCnt == 3) {
                        GameObject.addGameObject(new Cage((MapManager.getCamera().f13x + (MapManager.CAMERA_WIDTH >> 1)) << 6, MapManager.getCamera().f14y << 6));
                        MapManager.lockCamera(true);
                        this.WaitCnt = 4;
                        break;
                    }
            }
            checkWithPlayer(preX, preY, this.posX, this.posY);
        }
    }

    public void draw(MFGraphics g) {
        if (!this.dead) {
            System.out.println("draw boss~!");
            if (this.state < 5) {
                drawInMap(g, this.cardrawer);
                drawInMap(g, this.facedrawer, this.posX, this.posY - 3008);
            } else if (this.state == 5) {
                changeAniState(this.brokencardrawer, 2);
                drawInMap(g, this.brokencardrawer);
                if (this.drop_cnt < 2) {
                    changeAniState(this.brokencardrawer, 0);
                    drawInMap(g, this.brokencardrawer, this.flywheel_lx, this.flywheel_y);
                    changeAniState(this.brokencardrawer, 1);
                    drawInMap(g, this.brokencardrawer, this.flywheel_rx, this.flywheel_y);
                }
                drawInMap(g, this.facedrawer, this.posX, this.posY - 2496);
            } else if (this.state == 6) {
                drawInMap(g, this.boatdrawer, this.posX, this.posY - 960);
                drawInMap(g, this.escapefacedrawer, this.posX, this.posY - 2624);
            }
            this.arm.drawArm(g);
            if (this.bossbroken != null) {
                this.bossbroken.draw(g);
            }
            drawCollisionRect(g);
        }
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 2304, y - COLLISION_HEIGHT, COLLISION_WIDTH, COLLISION_HEIGHT);
    }

    private void changeAniState(AnimationDrawer AniDrawer, int state) {
        if (this.velocity > 0) {
            AniDrawer.setActionId(state);
            AniDrawer.setTrans(2);
            AniDrawer.setLoop(true);
            return;
        }
        AniDrawer.setActionId(state);
        AniDrawer.setTrans(0);
        AniDrawer.setLoop(true);
    }

    public void close() {
        this.cardrawer = null;
        this.facedrawer = null;
        this.brokencardrawer = null;
        this.boatdrawer = null;
        this.escapefacedrawer = null;
        super.close();
    }
}
