package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import com.sega.engine.action.ACCollision;
import com.sega.engine.action.ACObject;
import java.util.Vector;

public abstract class BulletObject extends MoveObject {
    protected static final int BULLET_ANIMAL = 0;
    protected static final int BULLET_BAT = 9;
    protected static final int BULLET_BEE = 1;
    protected static final int BULLET_BOSS5 = 16;
    protected static final int BULLET_BOSS6 = 19;
    protected static final int BULLET_BOSSF3BOMB = 20;
    protected static final int BULLET_BOSSF3RAY = 21;
    protected static final int BULLET_BOSS_EXTRA_LASER = 23;
    protected static final int BULLET_BOSS_EXTRA_PACMAN = 22;
    protected static final int BULLET_BOSS_STONE = 24;
    protected static final int BULLET_BOSS_STONE_SMALL = 25;
    protected static final int BULLET_CATERPILLAR = 13;
    protected static final int BULLET_CHAMELEON = 11;
    protected static final int BULLET_CLOWN = 10;
    protected static final int BULLET_CRAB = 2;
    protected static final int BULLET_FROG = 4;
    protected static final int BULLET_HERIKO = 14;
    protected static final int BULLET_LADYBUG = 6;
    protected static final int BULLET_LIZARD = 8;
    protected static final int BULLET_MAGMA = 7;
    protected static final int BULLET_MIRA = 12;
    protected static final int BULLET_MOLE = 15;
    protected static final int BULLET_MONKEY = 0;
    protected static final int BULLET_MOTOR = 3;
    protected static final int BULLET_NATURE = 1;
    protected static final int BULLET_PENGUIN = 18;
    protected static final int BULLET_RABBIT_FISH = 5;
    protected static final int BULLET_ROBOT = 17;
    protected static Animation batbulletAnimation;
    protected static Animation beebulletAnimation;
    protected static Animation boomAnimation;
    protected static Animation boss6bulletAnimation;
    protected static Animation bossf3bombAnimation;
    private static Vector bulletVec = new Vector();
    protected static Animation doublegravityflashbulletAnimation;
    protected static Animation laserAnimation;
    protected static Animation lizardbulletAnimation;
    protected static Animation mirabulletAnimation;
    protected static Animation missileAnimation;
    protected static Animation monkeybulletAnimation;
    protected static Animation pacmanAnimation;
    protected static Animation penguinbulletAnimation;
    protected static Animation robotbulletAnimation;
    protected static Animation stoneAnimation;
    protected AnimationDrawer drawer;
    private final boolean hitAndDestroy;
    private boolean hitted;
    private boolean isInCamera;

    public abstract void bulletLogic();

    public static void addBullet(int Id, int x, int y, int velX, int velY) {
        BulletObject element = null;
        switch (Id) {
            case 1:
                element = new BeeBullet(x, y, velX, velY);
                break;
            case 2:
            case 6:
                element = new DoubleGravityFlashBullet(x, y, velX, velY, 0);
                break;
            case 7:
                element = new DoubleGravityFlashBullet(x, y, velX, velY, 1);
                break;
            default:
                switch (Id) {
                    case 0:
                        element = new MonkeyBullet(x, y, velX, velY);
                        break;
                    case 8:
                        element = new LizardBullet(x, y, velX, velY);
                        break;
                    case 9:
                        element = new BatBullet(x, y, velX, velY);
                        break;
                    case 12:
                        element = new MiraBullet(x, y, velX, velY);
                        break;
                    case 16:
                        element = new MissileBullet(x, y, velX, velY);
                        break;
                    case 17:
                        element = new RobotBullet(x, y, velX, velY);
                        break;
                    case 18:
                        element = new PenguinBullet(x, y, velX, velY);
                        break;
                    case 19:
                        element = new Boss6Bullet(x, y, velX, velY);
                        break;
                    case 20:
                        element = new BossF3Bomb(x, y, velX, velY);
                        break;
                    case 21:
                        element = new BossF3Ray(x, y, velX);
                        break;
                    case 22:
                        element = new BossExtraPacman(x, y);
                        break;
                    case 23:
                        element = new LaserDamage(x, y);
                        break;
                    case 24:
                        element = new BossExtraStone(x, y);
                        break;
                }
                break;
        }
        if (element != null) {
            bulletVec.addElement(element);
        }
    }

    public static void bulletLogicAll() {
        int i = 0;
        while (i < bulletVec.size()) {
            BulletObject tmpBullet = (BulletObject) bulletVec.elementAt(i);
            tmpBullet.bulletLogic();
            if (tmpBullet.chkDestroy()) {
                bulletVec.removeElementAt(i);
                i--;
            } else if (GameObject.checkPaintNecessary(tmpBullet)) {
                paintVec[tmpBullet.getPaintLayer()].addElement(tmpBullet);
            }
            i++;
        }
    }

    public static void checkWithAllBullet(PlayerObject object) {
        int i = 0;
        while (i < bulletVec.size()) {
            BulletObject bullet = (BulletObject) bulletVec.elementAt(i);
            if (bullet.collisionChkWithObject(object)) {
                bullet.doWhileCollisionWrap(object);
            } else {
                bullet.doWhileNoCollision();
            }
            if (bullet.chkDestroy()) {
                bulletVec.removeElementAt(i);
                i--;
            }
            i++;
        }
    }

    public static void bulletClose() {
        beebulletAnimation = null;
        monkeybulletAnimation = null;
        doublegravityflashbulletAnimation = null;
        lizardbulletAnimation = null;
        batbulletAnimation = null;
        missileAnimation = null;
        boomAnimation = null;
        mirabulletAnimation = null;
        robotbulletAnimation = null;
        penguinbulletAnimation = null;
        boss6bulletAnimation = null;
        bossf3bombAnimation = null;
        stoneAnimation = null;
        laserAnimation = null;
        pacmanAnimation = null;
        for (int i = 0; i < bulletVec.size(); i++) {
            ((BulletObject) bulletVec.elementAt(i)).close();
        }
        bulletVec.removeAllElements();
    }

    protected BulletObject(int x, int y, int velX, int velY, boolean hitDestroy) {
        this.posX = x;
        this.posY = y;
        this.velX = velX;
        this.velY = velY;
        this.hitAndDestroy = hitDestroy;
        refreshCollisionRect(this.posX, this.posY);
    }

    public void logic() {
        if (!this.isInCamera && isInCamera()) {
            this.isInCamera = true;
        }
    }

    public boolean chkDestroy() {
        return (this.isInCamera && !isInCamera()) || isFarAwayCamera() || (this.hitAndDestroy && this.hitted);
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (object == player && player.canBeHurt()) {
            player.beHurt();
            this.hitted = true;
        }
    }

    public boolean IsHitted() {
        return this.hitted;
    }

    public void close() {
        this.drawer = null;
    }

    public void doBeforeCollisionCheck() {
    }

    public void doWhileCollision(ACObject arg0, ACCollision arg1, int arg2, int arg3, int arg4, int arg5, int arg6) {
    }
}
