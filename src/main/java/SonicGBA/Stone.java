package SonicGBA;

import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;

/* compiled from: GimmickObject */
class Stone extends GimmickObject {
    private static final int STONE_HEIGHT = 1728;
    private static final int STONE_OFFSETY = -128;
    private static final int STONE_WIDTH = 2624;
    private static MFImage image;

    public Stone(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        if (image == null) {
            try {
                image = MFImage.createImage("/gimmick/stone.png");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        this.used = false;
    }

    public int getPaintLayer() {
        return 0;
    }

    public void draw(MFGraphics g) {
        if (!this.used) {
            drawInMap(g, image, this.posX, this.posY - 128, 33);
        }
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (!this.used) {
            switch (direction) {
                case 0:
                    object.beStop(0, direction, this);
                    break;
                case 1:
                    if (object != player || !object.isAttackingEnemy() || !this.firstTouch) {
                        object.beStop(this.collisionRect.y0, direction, this);
                        break;
                    }
                    this.used = true;
                    object.doAttackPose(this, direction);
                    soundInstance.playSe(29);
                    break;
                case 2:
                case 3:
                    if (object != player || !object.isAttackingItem() || !this.firstTouch || player.collisionState == (byte) 0) {
                        object.beStop(this.collisionRect.y0, direction, this);
                        break;
                    }
                    this.used = true;
                    object.doAttackPose(this, direction);
                    soundInstance.playSe(29);
                    break;
            }
            if (this.used) {
                Effect.showEffect(rockBreakAnimation, 0, this.posX >> 6, this.posY >> 6, 0);
            }
        }
    }

    public void doWhileBeAttack(PlayerObject object, int direction, int animationID) {
        if (!this.used && animationID != 7 && animationID != 6 && animationID != 22 && animationID != 19 && animationID != 12) {
            object.doAttackPose(this, direction);
            soundInstance.playSe(29);
            this.used = true;
            Effect.showEffect(rockBreakAnimation, 0, this.posX >> 6, this.posY >> 6, 0);
            player.cancelFootObject();
            player.setVelY(GRAVITY);
        }
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 1312, y - STONE_HEIGHT, STONE_WIDTH, STONE_HEIGHT);
    }

    public void close() {
    }

    public static void releaseAllResource() {
        image = null;
    }
}
