package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import Lib.Coordinate;
import Lib.SoundSystem;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: GimmickObject */
class StoneBall extends GimmickObject {
    private static final int COLLISION_HEIGHT = 1792;
    private static final int COLLISION_WIDTH = 1792;
    public static final int OUT_VELOCITY = 300;
    private static final int STARTUP_HEIGHT = 9216;
    private static final int STARTUP_WIDTH = 16384;
    public static final byte STATE_GO = (byte) 2;
    public static final byte STATE_NONE = (byte) 0;
    public static final byte STATE_OUT = (byte) 1;
    private static Animation animation;
    private final int WAIT = 20;
    private AnimationDrawer drawer;
    private MapObject mapObj;
    private boolean noBall;
    private int originalX;
    private int originalY;
    public byte state;
    private int waitCount;

    protected StoneBall(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        if (animation == null) {
            animation = new Animation("/animation/stone_ball");
        }
        this.drawer = animation.getDrawer(0, true, 0);
        this.noBall = true;
        this.originalX = this.posX;
        this.originalY = this.posY;
        this.mapObj = new MapObject(this.posX + 2048, this.posY, 0, 0, this, this.iLeft);
    }

    public void draw(MFGraphics g) {
        if (this.state != (byte) 0) {
            drawInMap(g, this.drawer);
            drawCollisionRect(g);
        }
    }

    public void drawCollisionRect(MFGraphics g) {
        super.drawCollisionRect(g);
        if (SonicDebug.showCollisionRect) {
            g.setColor(16711680);
            g.drawRect(((this.originalX - 16384) >> 6) - camera.f13x, ((this.originalY - STARTUP_HEIGHT) >> 6) - camera.f14y, 512, 288);
            g.drawRect((((this.originalX - 16384) >> 6) - camera.f13x) + 1, (((this.originalY - STARTUP_HEIGHT) >> 6) - camera.f14y) + 1, 510, 286);
        }
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 896, y - 1792, 1792, 1792);
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (this.state != (byte) 0) {
            object.beHurt();
        }
    }

    private boolean inScreen() {
        Coordinate camera = MapManager.getCamera();
        if ((this.posX >> 6) <= camera.f13x || (this.posX >> 6) >= camera.f13x + SCREEN_WIDTH || (this.posY >> 6) <= camera.f14y || (this.posY >> 6) >= camera.f14y + SCREEN_HEIGHT) {
            return false;
        }
        return true;
    }

    public void logic() {
        int preX = this.posX;
        int preY = this.posY;
        if (this.waitCount > 0) {
            this.waitCount--;
        }
        switch (this.state) {
            case (byte) 0:
                this.posX = this.originalX;
                this.posY = this.originalY;
                transportTo(this.posX, this.posY);
                if (this.waitCount == 0 && player.getFootPositionX() > this.originalX - 16384 && player.getFootPositionX() < this.originalX + 16384 && player.getFootPositionY() > this.originalY - STARTUP_HEIGHT && player.getFootPositionY() < this.originalY + STARTUP_HEIGHT) {
                    this.state = (byte) 1;
                    return;
                }
                return;
            case (byte) 1:
                this.posX += 300;
                if (this.posX > this.originalX + 2048) {
                    this.state = (byte) 2;
                    this.mapObj.setPosition(this.posX, this.posY, 300, 0, this);
                    this.mapObj.setCrashCount(2);
                }
                checkWithPlayer(preX, preY, this.posX, this.posY);
                return;
            case (byte) 2:
                this.mapObj.logic();
                checkWithPlayer(this.posX, this.posY, this.mapObj.getPosX(), this.mapObj.getPosY());
                this.posX = this.mapObj.getPosX();
                this.posY = this.mapObj.getPosY();
                if (this.mapObj.chkCrash()) {
                    this.state = (byte) 0;
                    this.waitCount = 20;
                    Effect.showEffect(rockBreakAnimation, 0, this.posX >> 6, this.posY >> 6, 0);
                    if (inScreen()) {
                        SoundSystem.getInstance().playSe(35);
                        return;
                    }
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void close() {
        this.drawer = null;
    }

    public static void releaseAllResource() {
        Animation.closeAnimation(animation);
        animation = null;
    }
}
