package SonicGBA;

/* compiled from: EnemyObject */
abstract class BossObject extends EnemyObject {
    protected int HP = 8;

    protected BossObject(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        bossObjVec.addElement(this);
    }

    public void setBossHP() {
        if (GlobalResource.isEasyMode() && stageModeState == 0) {
            this.HP = 6;
        } else {
            this.HP = 8;
        }
    }
}
