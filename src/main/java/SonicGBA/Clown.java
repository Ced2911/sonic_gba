package SonicGBA;

import Lib.Animation;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: EnemyObject */
class Clown extends EnemyObject {
    private static final int COLLISION_HEIGHT = 3072;
    private static final int COLLISION_WIDTH = 1792;
    private static Animation clownAnimation;
    private int limitLeftX = this.posX;
    private int limitRightX = (this.posX + this.mWidth);
    private boolean touching;
    private int velocity = 192;

    public static void releaseAllResource() {
        Animation.closeAnimation(clownAnimation);
        clownAnimation = null;
    }

    protected Clown(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        if (clownAnimation == null) {
            clownAnimation = new Animation("/animation/clown");
        }
        this.drawer = clownAnimation.getDrawer(0, true, 0);
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (!this.dead && object == player && !this.touching) {
            if (!player.isAttackingEnemy()) {
                player.beHurt();
            } else if (player.isOnGound()) {
                PlayerHurtBall(object, direction);
            } else {
                player.doAttackPose(this, direction);
                beAttack();
            }
            this.touching = true;
        }
    }

    public void doWhileNoCollision() {
        this.touching = false;
    }

    public void PlayerHurtBall(PlayerObject object, int direction) {
        int playerVelX = -player.getVelX();
        if (this.velocity > 0) {
            if (player.getVelX() < 0) {
                this.velocity = -this.velocity;
                this.drawer.setActionId(0);
                this.drawer.setTrans(2);
            }
        } else if (player.getVelX() > 0) {
            this.velocity = -this.velocity;
            this.drawer.setActionId(0);
            this.drawer.setTrans(0);
        }
        int preAnimationId = player.getAnimationId();
        switch (direction) {
            case 0:
                object.beStop(this.collisionRect.y1, direction, this);
                break;
            case 1:
            case 4:
                object.beStop(this.collisionRect.y0, direction, this);
                break;
            case 2:
                object.beStop(this.collisionRect.x1, direction, this);
                break;
            case 3:
                object.beStop(this.collisionRect.x0, direction, this);
                break;
        }
        player.setVelX(playerVelX);
        player.setAnimationId(preAnimationId);
    }

    public void logic() {
        if (!this.dead) {
            int preX = this.posX;
            int preY = this.posY;
            if (this.velocity > 0) {
                this.posX += this.velocity;
                this.drawer.setActionId(0);
                this.drawer.setTrans(2);
                if (this.posX >= this.limitRightX) {
                    this.posX = this.limitRightX;
                    this.velocity = -this.velocity;
                }
            } else {
                this.posX += this.velocity;
                this.drawer.setActionId(0);
                this.drawer.setTrans(0);
                if (this.posX <= this.limitLeftX) {
                    this.posX = this.limitLeftX;
                    this.velocity = -this.velocity;
                }
            }
            this.posY = getGroundY(this.posX, this.posY);
            checkWithPlayer(preX, preY, this.posX, this.posY);
        }
    }

    public void draw(MFGraphics g) {
        if (!this.dead) {
            drawInMap(g, this.drawer);
            drawCollisionRect(g);
        }
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 896, y - 3072, COLLISION_WIDTH, 3072);
    }
}
