package SonicGBA;

import Lib.SoundSystem;
import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;

/* compiled from: GimmickObject */
class Ice extends GimmickObject {
    private static final int ICE_SIZE = 2560;
    private static MFImage image;

    protected Ice(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        if (image == null) {
            image = MFImage.createImage("/gimmick/ice.png");
        }
        this.used = false;
    }

    public void draw(MFGraphics g) {
        if (!this.used) {
            drawInMap(g, image, this.posX, this.posY, 33);
        }
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (!this.used) {
            switch (direction) {
                case 0:
                    object.beStop(0, direction, this);
                    break;
                case 1:
                    if (object != player || !object.isAttackingEnemy() || !this.firstTouch) {
                        object.beStop(this.collisionRect.y0, direction, this);
                        break;
                    }
                    this.used = true;
                    object.setVelY(0);
                    object.animationID = 1;
                    break;
                case 2:
                case 3:
                    if (object != player || !object.isAttackingEnemy() || !this.firstTouch || player.collisionState == (byte) 0) {
                        object.beStop(this.collisionRect.y0, direction, this);
                        break;
                    }
                    this.used = true;
                    object.setVelY(0);
                    break;
            }
            if (this.used) {
                Effect.showEffect(destroyEffectAnimation, 0, this.posX >> 6, (this.posY - 1280) >> 6, 0);
                Effect.showEffect(iceBreakAnimation, 0, this.posX >> 6, (this.posY - 1280) >> 6, 0);
                SoundSystem.getInstance().playSe(61);
            }
        }
    }

    public void doWhileBeAttack(PlayerObject object, int direction, int animationID) {
        if (!this.used) {
            this.used = true;
            if (this.used) {
                Effect.showEffect(destroyEffectAnimation, 0, this.posX >> 6, (this.posY - 1280) >> 6, 0);
                Effect.showEffect(iceBreakAnimation, 0, this.posX >> 6, (this.posY - 1280) >> 6, 0);
                SoundSystem.getInstance().playSe(61);
            }
        }
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 1280, y - 2560, 2560, 2560);
    }

    public void close() {
    }

    public static void releaseAllResource() {
        image = null;
    }

    public int getPaintLayer() {
        return 2;
    }
}
