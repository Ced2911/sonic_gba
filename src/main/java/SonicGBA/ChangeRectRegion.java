package SonicGBA;

/* compiled from: GimmickObject */
class ChangeRectRegion extends GimmickObject {
    private boolean isChanged;

    protected ChangeRectRegion(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (object.animationID == 4 && object.collisionState == (byte) 1) {
            object.changeRectHeight = true;
            this.isChanged = true;
            return;
        }
        object.changeRectHeight = false;
        this.isChanged = false;
    }

    public void doWhileNoCollision() {
        if (this.isChanged && player.changeRectHeight) {
            player.changeRectHeight = false;
            this.isChanged = false;
        }
    }
}
