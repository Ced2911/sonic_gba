package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import Lib.SoundSystem;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: GimmickObject */
class FlipV extends GimmickObject {
    private static final int ACCELERATE_POWER = 3000;
    private static final int COLLISION_HEIGHT = 3072;
    private static final int COLLISION_HEIGHT_OFFSET = 512;
    private static final int COLLISION_WIDTH = 1088;
    private static Animation flipAnimation;
    private int count = 0;
    private AnimationDrawer drawer;

    protected FlipV(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        if (flipAnimation == null) {
            flipAnimation = new Animation("/animation/flip");
        }
        this.drawer = flipAnimation.getDrawer(0, true, 0);
        this.count = 0;
    }

    public void draw(MFGraphics g) {
        if (this.count != 0) {
            this.count++;
        }
        if (this.count == 5 && (player instanceof PlayerAmy)) {
            ((PlayerAmy) player).setCannotAttack(false);
        }
        if (this.count >= 10) {
            this.count = 0;
        }
        drawInMap(g, this.drawer);
        if (this.drawer.checkEnd()) {
            this.drawer.setActionId(0);
            this.drawer.setLoop(true);
            this.drawer.setTrans(0);
        }
        drawCollisionRect(g);
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 544, (y - 3072) + 512, COLLISION_WIDTH, 3072);
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (object == player && this.drawer.getActionId() == 0) {
            switch (direction) {
                case 1:
                    player.beStop(this.collisionRect.y0, direction, this);
                    return;
                case 2:
                    if (object.collisionState == (byte) 1) {
                        object.beStop(this.collisionRect.y0, direction, this);
                        return;
                    } else if (this.count == 0) {
                        if (player.beAccelerate(ACCELERATE_POWER, true, this)) {
                            if (player instanceof PlayerAmy) {
                                ((PlayerAmy) player).resetAttackLevel();
                                ((PlayerAmy) player).setCannotAttack(true);
                            }
                            this.drawer.setActionId(1);
                            this.drawer.setLoop(false);
                            SoundSystem.getInstance().playSe(54);
                        }
                        this.count++;
                        return;
                    } else {
                        return;
                    }
                case 3:
                    if (object.collisionState == (byte) 1) {
                        object.beStop(this.collisionRect.y0, direction, this);
                        return;
                    } else if (this.count == 0) {
                        if (player.beAccelerate(-3000, true, this)) {
                            if (player instanceof PlayerAmy) {
                                ((PlayerAmy) player).resetAttackLevel();
                                ((PlayerAmy) player).setCannotAttack(true);
                            }
                            this.drawer.setActionId(1);
                            this.drawer.setLoop(false);
                            this.drawer.setTrans(2);
                            SoundSystem.getInstance().playSe(54);
                        }
                        this.count++;
                        return;
                    } else {
                        return;
                    }
                default:
                    return;
            }
        }
    }

    public void close() {
        this.drawer = null;
    }

    public static void releaseAllResource() {
        Animation.closeAnimation(flipAnimation);
        flipAnimation = null;
    }
}
