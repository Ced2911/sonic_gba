package SonicGBA;

import GameEngine.Key;
import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;

/* compiled from: GimmickObject */
class CaperBlock extends GimmickObject {
    private static final int COLLISION_HEIGHT = 1024;
    private static final int COLLISION_WIDTH = 2048;
    private static final int COLLISION_Y_OFFSET = 192;
    private static MFImage blockImage;
    private HobinCal hobinCal;

    protected CaperBlock(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        this.posY -= 512;
        if (blockImage == null) {
            if (StageManager.getCurrentZoneId() != 6) {
                blockImage = MFImage.createImage("/gimmick/caper_block_" + StageManager.getCurrentZoneId() + ".png");
            } else {
                blockImage = MFImage.createImage("/gimmick/caper_block_" + StageManager.getCurrentZoneId() + (StageManager.getStageID() - 9) + ".png");
            }
        }
        this.hobinCal = new HobinCal();
    }

    public void draw(MFGraphics g) {
        drawInMap(g, blockImage, this.posX + this.hobinCal.getPosOffsetX(), this.posY + this.hobinCal.getPosOffsetY(), 17);
        this.hobinCal.logic();
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (this.hobinCal.isStop()) {
            object.beStop(this.collisionRect.x0, direction, this);
            if (Key.repeat(Key.gLeft)) {
                player.faceDirection = false;
            } else if (Key.repeat(Key.gRight)) {
                player.faceDirection = true;
            }
            switch (direction) {
                case 1:
                    if (object == player) {
                        player.beSpring(1350, 1);
                        this.hobinCal.startHobin(400, 90, 10);
                        if ((player instanceof PlayerKnuckles) && (player.getCharacterAnimationID() == 19 || player.getCharacterAnimationID() == 20 || player.getCharacterAnimationID() == 21 || player.getCharacterAnimationID() == 22)) {
                            player.setAnimationId(1);
                        } else {
                            player.setAnimationId(1);
                        }
                        if (player.faceDirection) {
                            player.degreeForDraw = 1;
                            player.degreeRotateMode = 1;
                        } else {
                            player.degreeForDraw = 359;
                            player.degreeRotateMode = 2;
                        }
                        soundInstance.playSe(37);
                        return;
                    }
                    return;
                case 4:
                    if (object.getMoveDistance().f14y > 0 && object.getCollisionRect().y1 < this.collisionRect.y1 && object == player) {
                        player.beSpring(1350, 1);
                        this.hobinCal.startHobin(400, 90, 10);
                        player.setAnimationId(1);
                        if (player.faceDirection) {
                            player.degreeForDraw = 1;
                            player.degreeRotateMode = 1;
                        } else {
                            player.degreeForDraw = 359;
                            player.degreeRotateMode = 2;
                        }
                        soundInstance.playSe(37);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 1024, y + 192, 2048, 1024);
    }

    public void close() {
        this.hobinCal = null;
    }

    public static void releaseAllResource() {
        blockImage = null;
    }
}
