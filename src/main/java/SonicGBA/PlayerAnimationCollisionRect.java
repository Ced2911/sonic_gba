package SonicGBA;

public class PlayerAnimationCollisionRect implements SonicDef {
    public static final int CHECK_OFFSET = 192;
    private static CollisionRect rectH = new CollisionRect();
    private static CollisionRect rectV = new CollisionRect();
    private int animationID;
    public CollisionRect collisionRect = new CollisionRect();
    private boolean initFlag = true;
    private PlayerObject player;
    public CollisionRect preCollisionRect = new CollisionRect();

    public PlayerAnimationCollisionRect(PlayerObject player) {
        this.player = player;
    }

    public void initCollision(int xOffset, int yOffset, int width, int height, int animationID) {
        this.collisionRect.setRect(this.player.posX + xOffset, this.player.posY + (this.player.isAntiGravity ? (-yOffset) - height : yOffset), width, height);
        if (this.initFlag) {
            int i;
            CollisionRect collisionRect = this.preCollisionRect;
            int i2 = this.player.posX + xOffset;
            int i3 = this.player.posY;
            if (this.player.isAntiGravity) {
                i = (-yOffset) - height;
            } else {
                i = yOffset;
            }
            collisionRect.setRect(i2, i3 + i, width, height);
            this.initFlag = false;
        }
        this.animationID = animationID;
    }

    public void reset() {
        this.initFlag = true;
    }

    public void calPreCollision() {
        this.preCollisionRect.setTwoPosition(this.collisionRect.x0, this.collisionRect.y0, this.collisionRect.x1, this.collisionRect.y1);
    }

    public void collisionChkWithObject(GameObject obj) {
        CollisionRect objectRect = obj.getCollisionRect();
        CollisionRect thisRect = this.collisionRect;
        rectH.setRect(thisRect.x0, thisRect.y0 + 192, thisRect.getWidth(), thisRect.getHeight() - PlayerSonic.BACK_JUMP_SPEED_X);
        rectV.setRect(thisRect.x0 + 192, thisRect.y0, thisRect.getWidth() - PlayerSonic.BACK_JUMP_SPEED_X, thisRect.getHeight());
        if (objectRect.collisionChk(rectH) || objectRect.collisionChk(rectV)) {
            doWhileCollisionWrap(obj);
        }
    }

    public void doWhileCollisionWrap(GameObject object) {
        boolean xFirst;
        int direction = 4;
        int moveDistanceX = object.getMoveDistance().f13x;
        int moveDistanceY = object.getMoveDistance().f14y;
        CollisionRect collisionRectCurrent = this.collisionRect;
        CollisionRect collisionRectBefore = this.preCollisionRect;
        CollisionRect objCollisionRect = object.getCollisionRect();
        if (Math.abs(collisionRectCurrent.x0 - collisionRectBefore.x0) >= Math.abs(collisionRectCurrent.y0 - collisionRectBefore.y0)) {
            xFirst = true;
        } else {
            xFirst = false;
        }
        rectH.setRect(collisionRectCurrent.x0, collisionRectCurrent.y0 + 192, collisionRectCurrent.getWidth(), collisionRectCurrent.getHeight() - (192 * 2));
        rectV.setRect(collisionRectCurrent.x0 + 192, collisionRectCurrent.y0, collisionRectCurrent.getWidth() - (192 * 2), collisionRectCurrent.getHeight());
        if (xFirst && rectH.collisionChk(objCollisionRect)) {
            if ((collisionRectCurrent.x1 - collisionRectBefore.x1 > 0 && collisionRectBefore.isLeftOf(objCollisionRect, 192)) || (!rectV.collisionChk(objCollisionRect) && collisionRectCurrent.x0 < objCollisionRect.x0 && this.player.getVelX() >= -192)) {
                direction = 3;
            } else if ((collisionRectCurrent.x0 - collisionRectBefore.x0 < 0 && collisionRectBefore.isRightOf(objCollisionRect, 192)) || (!rectV.collisionChk(objCollisionRect) && collisionRectCurrent.x1 > objCollisionRect.x1 && this.player.getVelX() <= 192)) {
                direction = 2;
            }
        }
        if (direction == 4 && rectV.collisionChk(objCollisionRect)) {
            if (collisionRectCurrent.y1 - collisionRectBefore.y1 > 0 && collisionRectBefore.isUpOf(objCollisionRect, 192 + 5)) {
                direction = 1;
            } else if (collisionRectCurrent.y0 - collisionRectBefore.y0 < 0 && collisionRectBefore.isDownOf(objCollisionRect, 192)) {
                direction = 0;
            }
        }
        if (direction == 4 && rectH.collisionChk(objCollisionRect)) {
            if ((collisionRectCurrent.x1 - collisionRectBefore.x1 > 0 && collisionRectBefore.isLeftOf(objCollisionRect, 192)) || (!rectV.collisionChk(objCollisionRect) && collisionRectCurrent.x0 < objCollisionRect.x0 && this.player.getVelX() >= -192)) {
                direction = 3;
            } else if ((collisionRectCurrent.x0 - collisionRectBefore.x0 < 0 && collisionRectBefore.isRightOf(this.collisionRect, 192)) || (!rectV.collisionChk(objCollisionRect) && collisionRectCurrent.x1 > this.collisionRect.x1 && this.player.getVelX() <= 192)) {
                direction = 2;
            }
        }
        object.doWhileBeAttack(this.player, direction, this.animationID);
        calPreCollision();
    }
}
