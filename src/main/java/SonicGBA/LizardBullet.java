package SonicGBA;

import Lib.Animation;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: BulletObject */
class LizardBullet extends BulletObject {
    private static final int COLLISION_HEIGHT = 768;
    private static final int COLLISION_WIDTH = 768;
    private boolean isboom;

    protected LizardBullet(int x, int y, int velX, int velY) {
        super(x, y, velX, velY, true);
        if (lizardbulletAnimation == null) {
            lizardbulletAnimation = new Animation("/animation/lizard_bullet");
        }
        this.drawer = lizardbulletAnimation.getDrawer(0, true, 0);
        this.isboom = false;
    }

    public void bulletLogic() {
        int preX = this.posX;
        int preY = this.posY;
        if (this.velY < 0) {
            this.velY += GRAVITY;
            this.posY += this.velY;
        } else {
            this.velY = 0;
            this.drawer.setActionId(1);
            this.drawer.setLoop(false);
            this.isboom = true;
        }
        checkWithPlayer(preX, preY, this.posX, this.posY);
    }

    public boolean chkDestroy() {
        return this.drawer.checkEnd();
    }

    public void draw(MFGraphics g) {
        if (!this.drawer.checkEnd()) {
            drawInMap(g, this.drawer);
        }
        this.collisionRect.draw(g, camera);
    }

    public void refreshCollisionRect(int x, int y) {
        if (!this.isboom) {
            this.collisionRect.setRect(x - PlayerSonic.BACK_JUMP_SPEED_X, y - PlayerSonic.BACK_JUMP_SPEED_X, 768, 768);
        }
    }
}
