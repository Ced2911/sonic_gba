package SonicGBA;

import GameEngine.Key;
import Lib.Animation;
import Lib.AnimationDrawer;
import Lib.Coordinate;
import Lib.MyAPI;
import State.StringIndex;
import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;

public class PlayerAmy extends PlayerObject {
    public static final int AMY_ANI_ATTACK_1 = 18;
    public static final int AMY_ANI_ATTACK_2 = 19;
    public static final int AMY_ANI_BANK_1 = 46;
    public static final int AMY_ANI_BANK_2 = 47;
    public static final int AMY_ANI_BANK_3 = 48;
    public static final int AMY_ANI_BAR_MOVE = 44;
    public static final int AMY_ANI_BAR_STAY = 43;
    public static final int AMY_ANI_BIG_JUMP = 13;
    public static final int AMY_ANI_BRAKE = 54;
    public static final int AMY_ANI_BREATHE = 53;
    public static final int AMY_ANI_CAUGHT = 58;
    public static final int AMY_ANI_CELEBRATE_1 = 49;
    public static final int AMY_ANI_CELEBRATE_2 = 50;
    public static final int AMY_ANI_CELEBRATE_3 = 51;
    public static final int AMY_ANI_CLIFF_1 = 33;
    public static final int AMY_ANI_CLIFF_2 = 34;
    public static final int AMY_ANI_CLIFF_3 = 35;
    public static final int AMY_ANI_CLIFF_4 = 36;
    public static final int AMY_ANI_DASH_1 = 4;
    public static final int AMY_ANI_DASH_2 = 5;
    public static final int AMY_ANI_DASH_3 = 6;
    public static final int AMY_ANI_DASH_4 = 7;
    public static final int AMY_ANI_DASH_5 = 8;
    public static final int AMY_ANI_DEAD_1 = 26;
    public static final int AMY_ANI_DEAD_2 = 27;
    public static final int AMY_ANI_ENTER_SP = 56;
    public static final int AMY_ANI_HEART_SYMBOL = 57;
    public static final int AMY_ANI_HURT_1 = 24;
    public static final int AMY_ANI_HURT_2 = 25;
    public static final int AMY_ANI_JUMP_ATTACK_1 = 20;
    public static final int AMY_ANI_JUMP_ATTACK_2 = 21;
    public static final int AMY_ANI_JUMP_ATTACK_3 = 22;
    public static final int AMY_ANI_LOOK_UP_1 = 9;
    public static final int AMY_ANI_LOOK_UP_2 = 10;
    public static final int AMY_ANI_POLE_H = 42;
    public static final int AMY_ANI_POLE_V = 41;
    public static final int AMY_ANI_PUSH_WALL = 28;
    public static final int AMY_ANI_RAIL_BODY = 55;
    public static final int AMY_ANI_ROLL = 39;
    public static final int AMY_ANI_ROLL_H_1 = 31;
    public static final int AMY_ANI_ROLL_H_2 = 32;
    public static final int AMY_ANI_ROLL_V_1 = 29;
    public static final int AMY_ANI_ROLL_V_2 = 30;
    public static final int AMY_ANI_RUN = 3;
    public static final int AMY_ANI_SLIP_D0 = 37;
    public static final int AMY_ANI_SLIP_D45 = 38;
    public static final int AMY_ANI_SPRING_1 = 23;
    public static final int AMY_ANI_SPRING_2 = 14;
    public static final int AMY_ANI_SPRING_3 = 15;
    public static final int AMY_ANI_SPRING_4 = 16;
    public static final int AMY_ANI_SPRING_5 = 17;
    public static final int AMY_ANI_SQUAT_1 = 11;
    public static final int AMY_ANI_SQUAT_2 = 12;
    public static final int AMY_ANI_STAND = 0;
    public static final int AMY_ANI_UP_ARM = 40;
    public static final int AMY_ANI_VS_KNUCKLE = 52;
    public static final int AMY_ANI_WAITING_1 = 59;
    public static final int AMY_ANI_WAITING_2 = 60;
    public static final int AMY_ANI_WALK_1 = 1;
    public static final int AMY_ANI_WALK_2 = 2;
    public static final int AMY_ANI_WIND = 45;
    private static final int[] ANIMATION_CONVERT;
    private static final int ATTACK_COUNT_MAX = 10;
    private static final int BIG_JUMP_INWATER_POWER = 1728;
    private static final int BIG_JUMP_POWER = 1536;
    private static final int[][][] HEART_SYMBOL_PARAM;
    private static final int LOOP = -1;
    private static final int[] LOOP_INDEX;
    private static final int NO_ANIMATION = -1;
    private static final int NO_LOOP = -2;
    private static final int NO_LOOP_DEPAND = -2;
    private static final int SLIDING_BRAKE = 64;
    private static final int STEP_JUMP_INWATER_X = 1504;
    private static final int STEP_JUMP_INWATER_Y = 388;
    private static final int STEP_JUMP_LIMIT_Y = -820;
    private static final int STEP_JUMP_X = 1024;
    private static final int STEP_JUMP_X_V0 = 912;
    private static final int STEP_JUMP_Y = 320;
    public static boolean isCanJump = false;
    private Animation amyAnimation;
    private AnimationDrawer amyDrawer1;
    private AnimationDrawer amyDrawer2;
    private boolean attack1Flag;
    private boolean attack2Flag;
    private int attackCount;
    private int attackLevel;
    private PlayerAnimationCollisionRect attackRect;
    private boolean cannotAttack = false;
    private boolean isinBigJumpAttack = false;
    private boolean jumpAttackUsed;
    public boolean skipBeStop;
    int slideframe;
    static {
        int[] iArr = new int[54];
        iArr[1] = 1;
        iArr[2] = 2;
        iArr[3] = 3;
        iArr[4] = 39;
        iArr[5] = 12;
        iArr[6] = -1;
        iArr[7] = -1;
        iArr[8] = 28;
        iArr[9] = 23;
        iArr[10] = 17;
        iArr[11] = -1;
        iArr[12] = 25;
        iArr[13] = 41;
        iArr[14] = 14;
        iArr[15] = -1;
        iArr[16] = -1;
        iArr[17] = 54;
        iArr[18] = -1;
        iArr[19] = -1;
        iArr[20] = -1;
        iArr[21] = 55;
        iArr[22] = 31;
        iArr[23] = 32;
        iArr[24] = 40;
        iArr[25] = 29;
        iArr[26] = 30;
        iArr[27] = 43;
        iArr[28] = 44;
        iArr[29] = 45;
        iArr[30] = 25;
        iArr[31] = 42;
        iArr[32] = 46;
        iArr[33] = 47;
        iArr[34] = 48;
        iArr[35] = 49;
        iArr[36] = 50;
        iArr[37] = 51;
        iArr[38] = 9;
        iArr[39] = 10;
        iArr[40] = 9;
        iArr[41] = 27;
        iArr[42] = 15;
        iArr[43] = 16;
        iArr[44] = 25;
        iArr[45] = 26;
        iArr[46] = 11;
        iArr[47] = 34;
        iArr[48] = 36;
        iArr[49] = 53;
        iArr[50] = 59;
        iArr[51] = 60;
        iArr[52] = 58;
        iArr[53] = 52;
        ANIMATION_CONVERT = iArr;
        LOOP_INDEX = new int[62];
        HEART_SYMBOL_PARAM =  new int[5][][];
/*
        int[][][] iArr2 = new int[5][][];
        r1 = new int[6][];
        int[] iArr3 = new int[]{2, -41, 13, -36, iArr3};
        r1[4] = new int[]{1, 23, -27};
        r1[5] = new int[]{1, 26, -14};
        iArr2[0] = r1;
        iArr2[1] = new int[][]{new int[1], new int[1], new int[1], new int[]{1, 7, -41}, new int[]{1, 20, -36}, new int[]{1, 30, -27}, new int[]{1, 33, -14}};
        iArr2[2] = new int[][]{new int[]{1, -10, -41}, new int[]{1, 8, -43}, new int[]{1, 22, -32}, new int[]{1, 28, -16}, new int[]{1, 23, 1}, new int[]{1, 10, 11}};
        r1 = new int[3][];
        iArr3 = new int[]{1, -1, iArr3};
        r1[2] = new int[]{1, 19, -9};
        iArr2[3] = r1;
        int[][] iArr4 = new int[3][];
        iArr4[0] = new int[]{1, 28, -11};
        iArr4[1] = new int[]{1, -28, -11};
        int[] iArr5 = new int[]{1, -9, iArr5};
        iArr2[4] = iArr4;
        HEART_SYMBOL_PARAM = iArr2;
*/
    }
    public PlayerAmy() {
        MFImage amyImage = MFImage.createImage("/animation/player/chr_amy.png");
        this.amyAnimation = new Animation(amyImage, "/animation/player/chr_amy_01");
        this.amyDrawer1 = this.amyAnimation.getDrawer();
        this.drawer = this.amyDrawer1;
        this.amyDrawer2 = new Animation(amyImage, "/animation/player/chr_amy_02").getDrawer();
        this.attackRect = new PlayerAnimationCollisionRect(this);
        this.cannotAttack = false;
    }

    public void closeImpl() {
        Animation.closeAnimationDrawer(this.amyDrawer1);
        this.amyDrawer1 = null;
        Animation.closeAnimation(this.amyAnimation);
        this.amyAnimation = null;
        Animation.closeAnimationDrawer(this.amyDrawer2);
        this.amyDrawer2 = null;
    }

    public void drawCharacter(MFGraphics g) {
        Coordinate camera = MapManager.getCamera();
        if (this.animationID != -1) {
            this.myAnimationID = ANIMATION_CONVERT[this.animationID];
        }
        if (this.myAnimationID != -1) {
            boolean loop = LOOP_INDEX[this.myAnimationID] == -1;
            int effectID = -1;
            switch (this.myAnimationID) {
                case 18:
                    effectID = 0;
                    this.isAttacking = true;
                    break;
                case 19:
                    effectID = 1;
                    this.isAttacking = true;
                    break;
                case 20:
                    effectID = 2;
                    checkBreatheReset();
                    this.isAttacking = true;
                    break;
                case 21:
                    effectID = 3;
                    this.isAttacking = true;
                    break;
                case 22:
                    effectID = 4;
                    this.isAttacking = true;
                    break;
            }
            if (effectID >= 0) {
                int frame = this.drawer.getCurrentFrame();
                if (frame >= 0 && frame < HEART_SYMBOL_PARAM[effectID].length) {
                    for (int i = 0; i < HEART_SYMBOL_PARAM[effectID][frame][0]; i++) {
                        Effect.showEffect(this.amyAnimation, 57, (this.footPointX >> 6) + (((this.isAntiGravity ^ this.faceDirection) != false ? 1 : -1) * HEART_SYMBOL_PARAM[effectID][frame][(i * 2) + 1]), (this.footPointY >> 6) + ((this.isAntiGravity ? -1 : 1) * HEART_SYMBOL_PARAM[effectID][frame][(i * 2) + 2]), 0, 1);
                    }
                }
            }
            this.drawer = this.amyDrawer1;
            int drawerActionID = this.myAnimationID;
            if (this.myAnimationID >= 59) {
                this.drawer = this.amyDrawer2;
                drawerActionID = this.myAnimationID - 59;
                if (this.myAnimationID == 59 && this.isResetWaitAni) {
                    this.drawer.restart();
                    this.isResetWaitAni = false;
                }
            }
            if (this.isInWater) {
                this.drawer.setSpeed(1, 2);
            } else {
                this.drawer.setSpeed(1, 1);
            }
            if (this.hurtCount % 2 == 0) {
                int trans;
                if (this.fallinSandSlipState != 0) {
                    if (this.fallinSandSlipState == 1) {
                        this.faceDirection = true;
                    } else if (this.fallinSandSlipState == 2) {
                        this.faceDirection = false;
                    }
                }
                if (this.faceDirection) {
                    trans = 0;
                } else {
                    trans = 2;
                }
                if (this.animationID != 4) {
                    if (this.animationID != 6 && this.animationID != 7) {
                        switch (this.myAnimationID) {
                            case 37:
                                this.drawer.draw(g, drawerActionID, ((this.footPointX >> 6) - camera.f13x) + 0, ((this.footPointY >> 6) - camera.f14y) + 0, loop, 0);
                                break;
                            case 38:
                                this.drawer.draw(g, drawerActionID, ((this.footPointX >> 6) - camera.f13x) + 8, ((this.footPointY >> 6) - camera.f14y) + 0, loop, 0);
                                break;
                            default:
                                if (this.myAnimationID == 22 && this.drawer.checkEnd()) {
                                    soundInstance.playSe(23);
                                }
                                if (this.myAnimationID == 33 || this.myAnimationID == 34 || this.myAnimationID == 9 || this.myAnimationID == 10) {
                                    this.degreeForDraw = this.degreeStable;
                                    this.faceDegree = this.degreeStable;
                                }
                                if (this.myAnimationID >= 4 && this.myAnimationID <= 8) {
                                    this.degreeForDraw = this.degreeStable;
                                }
                                if (this.myAnimationID == 54) {
                                    this.degreeForDraw = this.degreeStable;
                                }
                                if (!(this.myAnimationID == 1 || this.myAnimationID == 2 || this.myAnimationID == 3 || this.myAnimationID == 58)) {
                                    this.degreeForDraw = this.degreeStable;
                                }
                                if (this.degreeForDraw == this.faceDegree) {
                                    boolean z;
                                    AnimationDrawer animationDrawer = this.drawer;
                                    int i2 = (this.footPointX >> 6) - camera.f13x;
                                    int i3 = (this.footPointY >> 6) - camera.f14y;
                                    int i4 = this.degreeForDraw;
                                    if (this.faceDirection) {
                                        z = false;
                                    } else {
                                        z = true;
                                    }
                                    drawDrawerByDegree(g, animationDrawer, drawerActionID, i2, i3, loop, i4, z);
                                    break;
                                }
                                int bodyCenterX = getNewPointX(this.footPointX, 0, (-this.collisionRect.getHeight()) >> 1, this.faceDegree);
                                int bodyCenterY = getNewPointY(this.footPointY, 0, (-this.collisionRect.getHeight()) >> 1, this.faceDegree);
                                g.saveCanvas();
                                g.translateCanvas((bodyCenterX >> 6) - camera.f13x, (bodyCenterY >> 6) - camera.f14y);
                                g.rotateCanvas((float) this.degreeForDraw);
                                this.drawer.draw(g, drawerActionID, 0, (this.collisionRect.getHeight() >> 1) >> 6, loop, trans);
                                g.restoreCanvas();
                                break;
                        }
                    }
                    this.drawer.draw(g, drawerActionID, (this.footPointX >> 6) - camera.f13x, (this.footPointY >> 6) - camera.f14y, loop, trans);
                } else {
                    this.drawer.draw(g, drawerActionID, (getNewPointX(getNewPointX(this.footPointX, 0, -512, this.faceDegree), 0, 512, 0) >> 6) - camera.f13x, (getNewPointY(getNewPointY(this.footPointY, 0, -512, this.faceDegree), 0, 512, 0) >> 6) - camera.f14y, loop, 0);
                }
                this.attackRectVec.removeAllElements();
                byte[] rect = this.drawer.getARect();
                if (this.isAntiGravity) {
                    byte[] rectTmp = this.drawer.getARect();
                    if (rectTmp != null) {
                        rect[0] = (byte) ((-rectTmp[0]) - rectTmp[2]);
                    }
                }
                if (rect != null) {
                    if (SonicDebug.showCollisionRect) {
                        g.setColor(65280);
                        g.drawRect(((this.footPointX >> 6) + rect[0]) - camera.f13x, ((this.footPointY >> 6) + (this.isAntiGravity ? (-rect[1]) - rect[3] : rect[1])) - camera.f14y, rect[2], rect[3]);
                    }
                    this.attackRect.initCollision(rect[0] << 6, rect[1] << 6, rect[2] << 6, rect[3] << 6, this.myAnimationID);
                    this.attackRectVec.addElement(this.attackRect);
                } else {
                    this.attackRect.reset();
                }
            } else {
                if (drawerActionID != this.drawer.getActionId()) {
                    this.drawer.setActionId(drawerActionID);
                }
                if (!AnimationDrawer.isAllPause()) {
                    this.drawer.moveOn();
                }
            }
            if (this.animationID == -1 && this.drawer.checkEnd()) {
                switch (this.myAnimationID) {
                    case 8:
                        this.animationID = 0;
                        break;
                    case 18:
                        if (this.attackLevel != 2) {
                            this.animationID = 0;
                            this.attackLevel = 0;
                            this.isAttacking = false;
                            break;
                        }
                        this.myAnimationID = 19;
                        this.attack2Flag = true;
                        this.worldCal.actionLogic(((this.isAntiGravity ^ this.faceDirection) != false ? 1 : -1) * 768, 0, (this.faceDirection ? 1 : -1) * 768);
                        return;
                    case 19:
                        this.animationID = 0;
                        this.attackLevel = 0;
                        this.isAttacking = false;
                        break;
                    case 20:
                        this.animationID = 10;
                        this.isAttacking = false;
                        break;
                }
                if (LOOP_INDEX[this.myAnimationID] >= 0) {
                    this.myAnimationID = LOOP_INDEX[this.myAnimationID];
                }
            }
        }
    }

    protected boolean spinLogic() {
        if (!(Key.repeat(Key.gLeft) || Key.repeat(Key.gRight) || isTerminal || this.animationID == -1)) {
            if (Key.repeat(Key.gDown)) {
                if (!(this.animationID == 5 || this.animationID == 47 || this.animationID == 48)) {
                    this.animationID = 46;
                }
                int jump;
                if (Key.press(16777216 | Key.gUp) && ((!this.isAntiGravity && (this.faceDegree < 90 || this.faceDegree > 270)) || (this.isAntiGravity && this.faceDegree > 90 && this.faceDegree < 270))) {
                    int i;
                    int i2;
                    this.animationID = -1;
                    this.myAnimationID = 4;
                    this.collisionState = (byte) 1;
                    this.worldCal.actionState = (byte) 1;
                    int jump_x = this.isInWater ? STEP_JUMP_INWATER_X : 1024;
                    jump = this.isInWater ? STEP_JUMP_INWATER_Y : STEP_JUMP_Y;
                    if (this.faceDirection) {
                        i = 1;
                    } else {
                        i = -1;
                    }
                    this.velX = ((i * jump_x) * MyAPI.dCos(this.faceDegree)) / 100;
                    if (this.faceDirection) {
                        i = 1;
                    } else {
                        i = -1;
                    }
                    this.velY = ((i * jump_x) * MyAPI.dSin(this.faceDegree)) / 100;
                    if (this.velY < STEP_JUMP_LIMIT_Y) {
                        this.velY = STEP_JUMP_LIMIT_Y;
                    }
                    i = this.velY;
                    if (this.isAntiGravity) {
                        i2 = -1;
                    } else {
                        i2 = 1;
                    }
                    this.velY = i + (i2 * ((-jump) - getGravity()));
                    soundInstance.playSe(11);
                    return true;
                } else if (Key.press(Key.gSelect)) {
                    this.animationID = -1;
                    this.myAnimationID = 13;
                    this.isinBigJumpAttack = true;
                    this.collisionState = (byte) 1;
                    this.worldCal.actionState = (byte) 1;
                    jump = this.isInWater ? BIG_JUMP_INWATER_POWER : 1536;
                    this.velY += (((-jump) - getGravity()) * MyAPI.dCos(this.faceDegree)) / 100;
                    this.velX += (((-jump) - getGravity()) * (-MyAPI.dSin(this.faceDegree))) / 100;
                    soundInstance.playSe(11);
                } else if (Math.abs(getVelX()) <= 64 && getDegreeDiff(this.faceDegree, this.degreeStable) <= 45) {
                    this.focusMovingState = 2;
                }
            } else if (this.animationID == 5) {
                this.animationID = 46;
            }
        }
        return false;
    }

    public void doJump() {
        if (this.myAnimationID != 13 && this.myAnimationID != 7 && this.myAnimationID != 8) {
            this.jumpAttackUsed = false;
            if (this.slipping) {
                if (!isHeadCollision()) {
                    this.currentLayer = 1;
                } else {
                    return;
                }
            }
            if (this.slipping && this.totalVelocity == 192) {
                super.doJumpV();
            } else {
                super.doJump();
            }
            if (this.slipping) {
                this.currentLayer = 1;
                this.slipping = false;
            }
            this.animationID = 14;
        }
    }

    public boolean isOnSlip0() {
        return this.myAnimationID == 37;
    }

    public void setSlip0() {
        if (this.collisionState == (byte) 0) {
            this.animationID = -1;
            this.myAnimationID = 37;
        }
        setMinSlipSpeed();
    }

    protected void extraLogicWalk() {
        if (this.myAnimationID == 18 && this.drawer.getCurrentFrame() == 4) {
            if (this.attack1Flag) {
                soundInstance.playSe(21);
                this.attack1Flag = false;
            }
        } else if (this.myAnimationID == 19 && this.drawer.getCurrentFrame() == 5 && this.attack2Flag) {
            soundInstance.playSe(21);
            this.attack2Flag = false;
        }
        if (this.attackCount > 0) {
            this.attackCount--;
            if (this.attackCount == 0) {
                this.attackLevel = 0;
            }
        }
        if (this.attackLevel > 0) {
            this.totalVelocity = 0;
        }
        if (this.attackLevel == 0) {
            isCanJump = true;
        } else {
            isCanJump = false;
        }
        if (!(this.cannotAttack || !Key.press(Key.gSelect) || this.myAnimationID == 28 || this.collisionState == (byte) 1)) {
            if (this.animationID != -1 && !Key.repeat(Key.gDown)) {
                this.totalVelocity = 0;
                this.animationID = -1;
                this.myAnimationID = 18;
                this.attack1Flag = true;
                this.attackCount = this.isInWater ? 20 : 10;
                this.attackLevel = 1;
            } else if (this.attackCount > 0 && this.attackLevel < 2) {
                this.attackLevel++;
            }
        }
        if (this.myAnimationID == 7 && this.totalVelocity == 0) {
            this.myAnimationID = 8;
        }
        if (this.slipping) {
            if (Key.repeat(Key.gLeft) && this.myAnimationID == 38) {
                this.totalVelocity -= 30;
            } else if (Key.repeat(Key.gDown | Key.gRight) && this.faceDegree < StringIndex.FONT_COLON_RED) {
                this.totalVelocity += (MyAPI.dSin(this.faceDegree) * 150) / 100;
            }
            this.totalVelocity -= 30;
            this.totalVelocity = Math.max(this.totalVelocity, 192);
            this.animationID = -1;
            this.faceDirection = true;
            if (this.faceDegree == 45) {
                this.myAnimationID = 38;
                if (this.slideSoundStart) {
                    soundInstance.playSe(8);
                    this.slideSoundStart = false;
                }
            } else {
                setMinSlipSpeed();
                this.myAnimationID = 37;
                if (this.slideSoundStart) {
                    soundInstance.playSe(8);
                    this.slideSoundStart = false;
                }
            }
            slidingFrame++;
            if (slidingFrame == 4) {
                soundInstance.playLoopSe(9);
            }
        }
    }

    protected void extraLogicJump() {
        switch (this.myAnimationID) {
            case 4:
            case 5:
                if (!Key.press(Key.gSelect)) {
                    if (this.velX != 0) {
                        if (this.velX >= 0) {
                            this.velX = Math.max(this.velX, STEP_JUMP_X_V0);
                            break;
                        } else {
                            this.velX = Math.min(this.velX, -912);
                            break;
                        }
                    }
                }
                this.myAnimationID = 6;
                break;
            case 14:
            case 15:
            case 16:
            case 17:
            case 23:
                if (!(!Key.press(Key.gSelect) || this.isinBigJumpAttack || this.jumpAttackUsed)) {
                    this.jumpAttackUsed = true;
                    if (!Key.repeat(Key.gDown)) {
                        this.animationID = -1;
                        this.myAnimationID = 20;
                        soundInstance.playSe(22);
                        break;
                    }
                    this.animationID = -1;
                    this.myAnimationID = 21;
                    break;
                }
        }
        if (this.animationID >= 1 && this.animationID <= 3 && Key.press(Key.gSelect) && !this.jumpAttackUsed) {
            this.jumpAttackUsed = true;
            if (Key.repeat(Key.gDown)) {
                this.animationID = -1;
                this.myAnimationID = 21;
            } else {
                this.animationID = -1;
                this.myAnimationID = 20;
                soundInstance.playSe(22);
            }
        }
        if (this.myAnimationID == 23 || this.myAnimationID == 17) {
            this.skipBeStop = false;
        }
    }

    protected void extraLogicOnObject() {
        this.isinBigJumpAttack = false;
        if (this.myAnimationID == 18 && this.drawer.getCurrentFrame() == 4) {
            if (this.attack1Flag) {
                soundInstance.playSe(21);
                this.attack1Flag = false;
            }
        } else if (this.myAnimationID == 19 && this.drawer.getCurrentFrame() == 5 && this.attack2Flag) {
            soundInstance.playSe(21);
            this.attack2Flag = false;
        }
        if (this.attackCount > 0) {
            this.attackCount--;
            if (this.attackCount == 0) {
                this.attackLevel = 0;
            }
        }
        if (this.attackLevel > 0) {
            this.totalVelocity = 0;
        }
        if (this.attackLevel == 0) {
            isCanJump = true;
        } else {
            isCanJump = false;
        }
        if (this.myAnimationID == 7 && this.velX == 0 && this.velY == 0) {
            this.myAnimationID = 8;
        }
        if (!Key.press(Key.gSelect)) {
            return;
        }
        if (this.animationID != -1 && !Key.repeat(Key.gDown) && this.collisionState != (byte) 1) {
            this.totalVelocity = 0;
            this.animationID = -1;
            this.myAnimationID = 18;
            this.attack1Flag = true;
            this.attackCount = this.isInWater ? 20 : 10;
            this.attackLevel = 1;
        } else if (this.attackCount > 0 && this.attackLevel < 2) {
            this.attackLevel++;
        }
    }

    public void doWhileLand(int degree) {
        super.doWhileLand(degree);
        this.jumpAttackUsed = false;
        if (this.myAnimationID == 7 || this.myAnimationID == 6) {
            this.myAnimationID = 7;
            this.animationID = -1;
            soundInstance.playSe(6);
        }
        this.isinBigJumpAttack = false;
        if (this.myAnimationID == 21 || this.myAnimationID == 22) {
            this.isAttacking = false;
        }
    }

    public void slipStart() {
        this.currentLayer = 0;
        this.slipping = true;
        this.slideSoundStart = true;
        this.collisionState = (byte) 1;
        this.worldCal.actionState = (byte) 1;
        setMinSlipSpeed();
    }

    private void setMinSlipSpeed() {
        if (getVelX() < SPEED_LIMIT_LEVEL_1) {
            setVelX(SPEED_LIMIT_LEVEL_1);
        }
    }

    public void slipJumpOut() {
        if (this.slipping) {
            this.currentLayer = 1;
            this.slipping = false;
            calDivideVelocity();
            setVelY(this.isInWater ? JUMP_INWATER_START_VELOCITY : JUMP_START_VELOCITY);
            this.collisionState = (byte) 1;
            this.worldCal.actionState = (byte) 1;
            this.collisionChkBreak = true;
            this.worldCal.stopMove();
        }
    }

    public void slipEnd() {
        if (this.slipping) {
            this.currentLayer = 1;
            this.slipping = false;
            calDivideVelocity();
            this.collisionState = (byte) 1;
            this.worldCal.actionState = (byte) 1;
            this.velY = -1540;
            this.animationID = 9;
            this.collisionChkBreak = true;
            this.worldCal.stopMove();
            soundInstance.stopLoopSe();
            soundInstance.playSequenceSe(11);
        }
    }

    public void setSlideAni() {
        this.animationID = -1;
        this.myAnimationID = 37;
    }

    public void doHurt() {
        super.doHurt();
        if (this.slipping) {
            this.currentLayer = 1;
            this.slipping = false;
        }
    }

    public void beSpring(int springPower, int direction) {
        this.attackLevel = 0;
        this.attackCount = 0;
        this.jumpAttackUsed = false;
        super.beSpring(springPower, direction);
        if (this.myAnimationID == 6 || this.myAnimationID == 7) {
            if (!(this.animationID == 0 && this.animationID == 1 && this.animationID == 2 && this.animationID == 3)) {
                this.animationID = 0;
            }
            if (Key.repeat(Key.gDown)) {
                this.animationID = 5;
            }
        }
    }

    public int getRetPower() {
        if (this.animationID == 5 && this.fallTime == 0) {
            return MOVE_POWER_REVERSE;
        }
        if (this.myAnimationID != 6 && this.myAnimationID != 7) {
            return super.getRetPower();
        }
        Effect.showEffectPlayer(this.dustEffectAnimation, 2, this.posX >> 6, this.posY >> 6, 0);
        return 64;
    }

    public boolean beAccelerate(int power, boolean IsX, GameObject sender) {
        if (this.myAnimationID == 6 || this.myAnimationID == 7) {
            return false;
        }
        boolean re = super.beAccelerate(power, IsX, sender);
        if (!(this.animationID == 0 && this.animationID == 1 && this.animationID == 2 && this.animationID == 3)) {
            this.animationID = 0;
        }
        if (Key.repeat(Key.gDown)) {
            this.animationID = 5;
        }
        return re;
    }

    public boolean needRetPower() {
        boolean re = this.myAnimationID == 6 || this.myAnimationID == 7;
        return re | super.needRetPower();
    }

    public int getSlopeGravity() {
        if (this.myAnimationID == 6 || this.myAnimationID == 7) {
            return 0;
        }
        return super.getSlopeGravity();
    }

    public boolean noRotateDraw() {
        return this.myAnimationID == 18 || this.myAnimationID == 19 || this.myAnimationID == 6 || this.myAnimationID == 7 || this.myAnimationID == 8 || super.noRotateDraw();
    }

    public void resetAttackLevel() {
        this.attackLevel = 0;
        this.attackCount = 0;
    }

    public void setCannotAttack(boolean cannot) {
        this.cannotAttack = cannot;
    }
}
