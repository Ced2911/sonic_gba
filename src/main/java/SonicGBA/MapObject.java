package SonicGBA;

import Lib.MyAPI;
import com.sega.engine.action.ACBlock;
import com.sega.engine.action.ACCollision;
import com.sega.engine.action.ACObject;
import com.sega.engine.action.ACUtilities;
import com.sega.engine.action.ACWorldCalUser;
import com.sega.engine.action.ACWorldCollisionCalculator;
import com.sega.engine.action.ACWorldCollisionLimit;
import com.sega.mobile.framework.device.MFGraphics;

public class MapObject extends GameObject implements ACWorldCalUser, ACWorldCollisionLimit {
    private static final int STATE_GROUND = 0;
    private static final int STATE_SKY = 1;
    private static ACBlock groundBlock = CollisionMap.getInstance().getNewCollisionBlock();
    private static ACBlock skyBlock = CollisionMap.getInstance().getNewCollisionBlock();
    private int LEFT_WALK_COLLISION_CHECK_OFFSET_X;
    private int LEFT_WALK_COLLISION_CHECK_OFFSET_Y = this.RIGHT_WALK_COLLISION_CHECK_OFFSET_Y;
    private int RIGHT_WALK_COLLISION_CHECK_OFFSET_X;
    private int RIGHT_WALK_COLLISION_CHECK_OFFSET_Y = -512;
    private int centerOffsetX;
    private int centerOffsetY;
    private MapBehavior collisionBehavior;
    private boolean collisionChkBreak;
    private int crashCount = 1;
    private int footX;
    private int footY;
    private int gravity;
    private boolean isAntiGravity;
    private int moveDegree;
    private int moveDistanceX;
    private int moveDistanceY;
    private GameObject object;
    private int state;
    private int totalVelocity;
    private ACWorldCollisionCalculator worldCal;

    public MapObject(GameObject object, int layer) {
        this.object = object;
        this.currentLayer = layer;
        this.worldCal = new ACWorldCollisionCalculator(this, this);
        this.worldCal.setLimit(this);
        this.gravity = GameObject.GRAVITY;
    }

    public MapObject(int x, int y, int vx, int vy, GameObject object, int layer) {
        this.object = object;
        this.currentLayer = layer;
        setPosition(x, y, vx, vy, object);
        this.worldCal = new ACWorldCollisionCalculator(this, this);
        this.worldCal.setLimit(this);
        this.gravity = GameObject.GRAVITY;
    }

    public MapObject(int x, int y, int vx, int vy, GameObject object, int layer, int offsetvx) {
        this.object = object;
        this.currentLayer = layer;
        setPosition(x, y, vx, vy, object);
        this.worldCal = new ACWorldCollisionCalculator(this, this);
        this.worldCal.setLimit(this);
        this.gravity = GameObject.GRAVITY;
    }

    public void setPosition(int x, int y, int vx, int vy, GameObject object) {
        this.state = STATE_SKY;
        this.posX = x;
        this.posY = y;
        this.velX = vx;
        this.velY = vy;
        this.object = object;
        this.object.refreshCollisionRect(this.posX, this.posY);
        CollisionRect collisionRect = this.object.getCollisionRect();
        this.footX = (collisionRect.x0 + collisionRect.x1) >> 1;
        this.footY = collisionRect.y1;
        this.width = collisionRect.x1 - collisionRect.x0;
        this.height = collisionRect.y1 - collisionRect.y0;
        this.centerOffsetX = this.footX - this.posX;
        this.centerOffsetY = this.footY - this.posY;
        this.RIGHT_WALK_COLLISION_CHECK_OFFSET_X = collisionRect.getWidth() >> 1;
        this.LEFT_WALK_COLLISION_CHECK_OFFSET_X = -this.RIGHT_WALK_COLLISION_CHECK_OFFSET_X;
    }

    public void setCrashCount(int count) {
        this.crashCount = count;
    }

    public int getCurrentCrashCount() {
        return this.crashCount;
    }

    public void logic() {
        this.gravity = GameObject.GRAVITY;
        if (this.collisionBehavior != null) {
            this.gravity = this.collisionBehavior.getGravity();
        }
        switch (this.state) {
            case STATE_GROUND:
                if (this.totalVelocity != 0) {
                    this.totalVelocity += (this.gravity * MyAPI.dSin(this.moveDegree)) / 100;
                    break;
                }
                break;
            case STATE_SKY:
                this.velY += this.isAntiGravity ? -this.gravity : this.gravity;
                break;
        }
        checkWithMap();
    }

    public void logic2() {
        this.gravity = GameObject.GRAVITY;
        if (this.collisionBehavior != null) {
            this.gravity = this.collisionBehavior.getGravity();
        }
        switch (this.state) {
            case STATE_GROUND:
                this.totalVelocity = 0;
                break;
            case STATE_SKY:
                this.velY += this.isAntiGravity ? -this.gravity : this.gravity;
                break;
        }
        checkWithMap();
    }

    public int getPosX() {
        return this.posX;
    }

    public int getPosY() {
        return this.posY;
    }

    public void checkWithMap() {
        switch (this.state) {
            case STATE_GROUND:
                int dCos = (this.totalVelocity * MyAPI.dCos(this.moveDegree)) / 100;
                this.moveDistanceX = dCos;
                this.velX = dCos;
                dCos = (this.totalVelocity * MyAPI.dSin(this.moveDegree)) / 100;
                this.moveDistanceY = dCos;
                this.velY = dCos;
                break;
            case STATE_SKY:
                this.moveDistanceX = this.velX;
                this.moveDistanceY = this.velY;
                this.moveDegree = 0;
                break;
        }
        this.posZ = this.currentLayer;
        this.worldCal.actionLogic(this.moveDistanceX, this.moveDistanceY);
    }

    public void setLayer(int layer) {
        this.currentLayer = layer;
    }

    public int getQuaParam(int x, int divide) {
        if (x > 0) {
            return x / divide;
        }
        return (x - (divide - 1)) / divide;
    }

    public void doWhileCollision(PlayerObject object, int direction) {
    }

    public void draw(MFGraphics g) {
    }

    public void refreshCollisionRect(int x, int y) {
    }

    private int getNewPointX(int oriX, int xOffset, int yOffset, int degree) {
        return oriX + xOffset;
    }

    private int getNewPointY(int oriY, int xOffset, int yOffset, int degree) {
        return oriY + yOffset;
    }

    public void calDivideVelocity() {
        calDivideVelocity(this.moveDegree);
    }

    public void calDivideVelocity(int degree) {
        this.velX = (this.totalVelocity * MyAPI.dCos(degree)) / 100;
        this.velY = (this.totalVelocity * MyAPI.dSin(degree)) / 100;
    }

    public int getVelX() {
        return this.velX;
    }

    public int getVelY() {
        return this.velX;
    }

    public void setVel(int velx, int vely) {
        this.velX = velx;
        this.velY = vely;
        calTotalVelocity();
    }

    public void calTotalVelocity() {
        calTotalVelocity(this.moveDegree);
    }

    public void calTotalVelocity(int degree) {
        this.totalVelocity = ((this.velX * MyAPI.dCos(degree)) + (this.velY * MyAPI.dSin(degree))) / 100;
    }

    public boolean chkCrash() {
        if (this.crashCount <= 0) {
            return true;
        }
        return false;
    }

    public void setBehavior(MapBehavior behavior) {
        this.collisionBehavior = behavior;
    }

    public void doJump(int vx, int vy, boolean mustJump) {
        System.out.println("do jump:" + this.state);
        if (this.state == STATE_GROUND || mustJump) {
            this.state = STATE_SKY;
            this.velX = vx;
            this.velY = vy;
            this.worldCal.stopMove();
            this.worldCal.actionState = (byte) 1;
        }
    }

    public void doJump(int vx, int vy) {
        doJump(vx, vy, false);
    }

    public void doStop() {
        this.totalVelocity = 0;
        this.velX = 0;
        this.velY = 0;
        this.worldCal.stopMove();
        this.collisionChkBreak = true;
    }

    public void close() {
    }

    public void doBeforeCollisionCheck() {
    }

    public void doWhileCollision(ACObject arg0, ACCollision arg1, int arg2, int arg3, int arg4, int arg5, int arg6) {
    }

    public void doWhileLand(int degree) {
        this.crashCount--;
        this.state = 0;
        this.totalVelocity = ACUtilities.getTotalFromDegree(this.velX, this.velY, degree);
        if (this.collisionBehavior != null) {
            this.collisionBehavior.doWhileTouchGround(this.velX, this.velY);
        }
    }

    public void doWhileLeaveGround() {
    }

    public void doWhileTouchWorld(int direction, int degree) {
        boolean realTouch = false;
        switch (direction) {
            case 0:
                if (this.collisionBehavior != null) {
                    this.collisionBehavior.doWhileToucRoof(this.velX, this.velY);
                    break;
                }
                break;
            case 1:
                if (ACUtilities.getTotalFromDegree(this.velX, this.velY, 0) > 0) {
                    realTouch = true;
                    break;
                }
                break;
            case 3:
                if (ACUtilities.getTotalFromDegree(this.velX, this.velY, RollPlatformSpeedC.DEGREE_VELOCITY) > 0) {
                    realTouch = true;
                    break;
                }
                break;
        }
        if (realTouch) {
            this.crashCount--;
        }
    }

    public int getBodyDegree() {
        return this.worldCal.footDegree;
    }

    public int getBodyOffset() {
        return this.height >> 1;
    }

    public int getFootOffset() {
        return this.width >> 1;
    }

    public int getFootX() {
        return this.posX + this.centerOffsetX;
    }

    public int getFootY() {
        return this.posY + this.centerOffsetY;
    }

    public int getMinDegreeToLeaveGround() {
        return 30;
    }

    public int getPressToGround() {
        return GRAVITY;
    }

    public void didAfterEveryMove(int arg0, int arg1) {
        switch (this.worldCal.actionState) {
            case (byte) 0:
                this.state = STATE_GROUND;
                this.moveDegree = this.worldCal.footDegree;
                return;
            case (byte) 1:
                this.state = STATE_SKY;
                return;
            default:
                return;
        }
    }

    public boolean noSideCollision() {
        if (this.collisionBehavior != null) {
            return !this.collisionBehavior.hasSideCollision();
        } else {
            return false;
        }
    }

    public boolean noTopCollision() {
        if (this.collisionBehavior != null) {
            return !this.collisionBehavior.hasTopCollision();
        } else {
            return false;
        }
    }

    public boolean noDownCollision() {
        if (this.collisionBehavior != null) {
            return !this.collisionBehavior.hasDownCollision();
        } else {
            return false;
        }
    }

    public void setAntiGravity(boolean flag) {
        this.isAntiGravity = flag;
    }

    public void reset() {
        this.worldCal.setLimit(null);
        this.worldCal = null;
    }
}
