package SonicGBA;

import GameEngine.Def;
import Lib.Animation;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: EnemyObject */
class Mira extends EnemyObject {
    private static final int ALERT_HEIGHT = 64;
    private static final int ALERT_WIDTH = 128;
    private static final int ATTACK_WIDTH = 2048;
    private static final int BULLET_SPEED = 480;
    private static final int COLLISION_HEIGHT = 1920;
    private static final int DEFENCE_WIDTH = 1920;
    private static final int DISTANCE = 2560;
    private static final int SPEED = 128;
    private static final int STATE_ATTACK = 1;
    private static final int STATE_PATROL = 0;
    private static final int WAIT_MAX = 30;
    private static Animation miraAnimation;
    private boolean IsFire = false;
    private int alert_state;
    private boolean dir = false;
    private int endPosX;
    private int journey;
    private int startPosX;
    private int state;
    private int trans;
    private int wait_cn;

    protected Mira(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        if (miraAnimation == null) {
            miraAnimation = new Animation("/animation/mira");
        }
        this.drawer = miraAnimation.getDrawer(0, true, 0);
        this.startPosX = this.posX;
        this.endPosX = this.posX + this.mWidth;
        this.dir = false;
        this.state = 0;
        this.trans = 0;
        this.wait_cn = 0;
        this.IsFire = false;
        this.posY -= 960;
    }

    public static void releaseAllResource() {
        Animation.closeAnimation(miraAnimation);
        miraAnimation = null;
    }

    public void logic() {
        if (!this.dead) {
            this.alert_state = checkPlayerInEnemyAlertRange(this.posX >> 6, this.posY >> 6, 128, 64);
            int preX = this.posX;
            int preY = this.posY;
            switch (this.state) {
                case 0:
                    if (this.dir) {
                        this.posX -= 128;
                        this.journey += 128;
                        if (this.posX <= this.startPosX) {
                            this.dir = false;
                            this.posX = this.startPosX;
                        }
                    } else {
                        this.posX += 128;
                        this.journey += 128;
                        if (this.posX >= this.endPosX) {
                            this.dir = true;
                            this.posX = this.endPosX;
                        }
                    }
                    if (this.journey >= 2560 && this.alert_state == 0) {
                        this.state = 1;
                        this.journey = 0;
                        this.trans = checkEnemyFace();
                        this.drawer.setActionId(1);
                        this.drawer.setTrans(this.trans);
                        this.drawer.setLoop(false);
                        this.IsFire = true;
                    }
                    checkWithPlayer(preX, preY, this.posX, this.posY);
                    return;
                case 1:
                    if (this.wait_cn < 30) {
                        this.wait_cn++;
                        if (this.drawer.checkEnd() && this.IsFire) {
                            this.IsFire = false;
                            BulletObject.addBullet(12, this.posX + (this.trans == 0 ? Def.TOUCH_HELP_LEFT_X : 128), this.posY, this.trans == 0 ? -480 : BULLET_SPEED, 0);
                        }
                    } else {
                        this.wait_cn = 0;
                        this.drawer.setActionId(0);
                        this.drawer.setTrans(0);
                        this.drawer.setLoop(true);
                        this.state = 0;
                    }
                    checkWithPlayer(preX, preY, this.posX, this.posY);
                    return;
                default:
                    return;
            }
        }
    }

    private int checkEnemyFace() {
        if (this.posX - player.getFootPositionX() >= 0) {
            return 0;
        }
        return 2;
    }

    public void draw(MFGraphics g) {
        if (!this.dead) {
            drawInMap(g, this.drawer);
            drawCollisionRect(g);
        }
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (!this.dead) {
            switch (this.state) {
                case 0:
                    if (object == player) {
                        player.beHurt();
                        return;
                    }
                    return;
                case 1:
                    if (player.isAttackingEnemy()) {
                        player.doAttackPose(this, direction);
                        beAttack();
                        return;
                    }
                    player.beHurt();
                    return;
                default:
                    return;
            }
        }
    }

    public void doWhileBeAttack(PlayerObject object, int direction, int animationID) {
        if (!this.dead) {
            switch (this.state) {
                case 0:
                    return;
                case 1:
                    if (player.isAttackingEnemy()) {
                        player.doAttackPose(this, direction);
                        beAttack();
                        return;
                    } else if ((player instanceof PlayerTails) && (player.getCharacterAnimationID() == 12 || player.getCharacterAnimationID() == 13 || player.getCharacterAnimationID() == 14)) {
                        beAttack();
                        return;
                    } else {
                        player.beHurt();
                        return;
                    }
                default:
                    return;
            }
        }
    }

    public void refreshCollisionRect(int x, int y) {
        switch (this.state) {
            case 0:
                this.collisionRect.setRect(x - 960, y - 960, 1920, 1920);
                return;
            case 1:
                this.collisionRect.setRect(x - 1024, y - 960, 2048, 1920);
                return;
            default:
                return;
        }
    }
}
