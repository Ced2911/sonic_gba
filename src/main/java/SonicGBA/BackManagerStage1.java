package SonicGBA;

import Lib.MyAPI;
import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;

/* compiled from: BackGroundManager */
class BackManagerStage1 extends BackGroundManager {
    private static final int[] BG_CUT_HEIGHT_N = new int[]{8, 16, 16, 56, 2, 4, 5, 6, 7, 8, 9, 11, 12};
    private static int[] BG_CUT_HEIGHT = BG_CUT_HEIGHT_N;
    private static final int[] BG_CUT_HEIGHT_W = new int[]{MyAPI.zoomIn(36, true), MyAPI.zoomIn(32, true), MyAPI.zoomIn(32, true), MyAPI.zoomIn(112, true), MyAPI.zoomIn(32, true), MyAPI.zoomIn(32, true), MyAPI.zoomIn(32, true), MyAPI.zoomIn(32, true), MyAPI.zoomIn(20, true)};
    private static final int[] CAMERA_POS_X_SPEED_N =  new int[13];
    private static final int[] POS_X_SPEED_N = new int[]{32, 16, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    private static int[] CAMERA_POS_X_SPEED = CAMERA_POS_X_SPEED_N;
    private static final int[] CAMERA_POS_X_SPEED_W = new int[9];
    private static final String FILE_NAME = "/stage1_bg.png";
    private static int IMAGE_WIDTH = 256;
    private static int[] POS_X_SPEED = POS_X_SPEED_N;
    private static final int POS_X_SPEED_DIVIDE = 64;
    private static final int[] POS_X_SPEED_W = new int[9];
    private MFImage image;
    private MFImage[] imageBG;
    private int[] posX = new int[BG_CUT_HEIGHT.length];

    /*
    static {
        r0 = new int[13];
        POS_X_SPEED_N = r0;
        r0 = new int[9];
        POS_X_SPEED_W = r0;
        r0 = new int[13];
        CAMERA_POS_X_SPEED_N = r0;
        r0 = new int[9];
        r0[4] = MyAPI.zoomIn(4, true);
        r0[5] = MyAPI.zoomIn(8, true);
        r0[6] = MyAPI.zoomIn(12, true);
        r0[7] = MyAPI.zoomIn(16, true);
        r0[8] = MyAPI.zoomIn(20, true);
        CAMERA_POS_X_SPEED_W = r0;
    }
    */

    public BackManagerStage1() {
        try {
            this.imageBG = new MFImage[4];
            this.imageBG[0] = MFImage.createImage("/map/stage1_bg/#1.png");
            for (int i = 1; i < 4; i++) {
                this.imageBG[i] = MFImage.createPaletteImage("/map/stage1_bg/#" + (i + 1) + ".pal");
            }
            IMAGE_WIDTH = MyAPI.zoomIn(this.imageBG[0].getWidth(), true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void close() {
        this.posX = null;
        if (this.imageBG != null) {
            for (int i = 0; i < this.imageBG.length; i++) {
                this.imageBG[i] = null;
            }
        }
        this.imageBG = null;
    }

    public void draw(MFGraphics g) {
        int cameraX = MapManager.getCamera().f13x;
        int y = 0;
        int tmpframe = (frame % 16) / 4;
        for (int i = 0; i < BG_CUT_HEIGHT.length; i++) {
            if (!GameObject.IsGamePause) {
                int[] iArr = this.posX;
                iArr[i] = iArr[i] + POS_X_SPEED[i];
            }
            int drawX = -((((CAMERA_POS_X_SPEED[i] * cameraX) / 64) + (this.posX[i] >> 6)) % IMAGE_WIDTH);
            for (int j = 0; j < MapManager.CAMERA_WIDTH - drawX; j += IMAGE_WIDTH) {
                MyAPI.drawRegion(g, this.imageBG[tmpframe], 0, y, IMAGE_WIDTH, BG_CUT_HEIGHT[i], 0, drawX + j, y, 20);
            }
            y += BG_CUT_HEIGHT[i];
        }
    }
}
