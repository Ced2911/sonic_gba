package SonicGBA;

import Lib.MyAPI;
import com.sega.mobile.define.MDPhone;

/* compiled from: GimmickObject */
class RollHobin extends BallHobin {
    private static final int DEGREE_VELOCITY = -4;
    private static int degree;
    private int centerX;
    private int centerY;
    private int degreeOffset;
    private int radius;

    protected RollHobin(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        this.radius = this.mWidth > this.mHeight ? this.mWidth : this.mHeight;
        this.centerX = this.posX;
        this.centerY = this.posY;
        calHobinPosition();
        this.degreeOffset = (this.iLeft * MDPhone.SCREEN_WIDTH) / 8;
    }

    private void calHobinPosition() {
        this.posX = this.centerX + ((this.radius * MyAPI.dCos(degree + this.degreeOffset)) / 100);
        this.posY = this.centerY + ((this.radius * MyAPI.dSin(degree + this.degreeOffset)) / 100);
    }

    public void logic() {
        int preX = this.posX;
        int preY = this.posY;
        calHobinPosition();
        checkWithPlayer(preX, preY, this.posX, this.posY);
    }

    public static void staticLogic() {
        degree -= 4;
        degree += MDPhone.SCREEN_WIDTH;
        degree %= MDPhone.SCREEN_WIDTH;
    }

    public void close() {
    }

    public static void releaseAllResource() {
    }
}
