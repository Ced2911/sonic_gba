package SonicGBA;

import Lib.Animation;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: EnemyObject */
class Cement extends EnemyObject {
    private static final int ALERT_RANGE = 30;
    private static final int COLLISION_HEIGHT = 1152;
    private static final int COLLISION_WIDTH = 1152;
    private static final int DEFENCE_HEIGHT = 2560;
    private static final int DEFENCE_WIDTH = 2560;
    private static final int PATROL_FRAME = 136;
    private static final int SPEED = 60;
    private static final int STATE_DEFENCE = 1;
    private static final int STATE_PATROL = 0;
    private static final int WAIT_FRAME = 32;
    private static Animation cementAnimation;
    private int alert_state;
    private boolean dir = false;
    private int endPosX;
    private int patrol_cn;
    private int startPosX;
    private int state;
    private int wait_cn;

    protected Cement(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        if (cementAnimation == null) {
            cementAnimation = new Animation("/animation/kura");
        }
        this.drawer = cementAnimation.getDrawer(0, true, 0);
        this.startPosX = this.posX;
        this.endPosX = this.posX + this.mWidth;
        this.dir = false;
    }

    public static void releaseAllResource() {
        Animation.closeAnimation(cementAnimation);
        cementAnimation = null;
    }

    public void logic() {
        if (!this.dead) {
            int preX = this.posX;
            int preY = this.posY;
            switch (this.state) {
                case 0:
                    if (this.dir) {
                        this.posX -= 60;
                        if (this.posX <= this.startPosX) {
                            this.dir = false;
                            this.posX = this.startPosX;
                        }
                    } else {
                        this.posX += 60;
                        if (this.posX >= this.endPosX) {
                            this.dir = true;
                            this.posX = this.endPosX;
                        }
                    }
                    if (this.patrol_cn >= 136) {
                        this.patrol_cn = 0;
                        this.wait_cn = 0;
                        this.state = 1;
                        this.drawer.setActionId(1);
                        this.drawer.setLoop(true);
                        break;
                    }
                    this.patrol_cn++;
                    break;
                case 1:
                    if (this.wait_cn >= 32) {
                        this.patrol_cn = 0;
                        this.wait_cn = 0;
                        this.drawer.setActionId(0);
                        this.drawer.setTrans(0);
                        this.drawer.setLoop(true);
                        this.state = 0;
                        break;
                    }
                    this.wait_cn++;
                    break;
            }
            checkWithPlayer(preX, preY, this.posX, this.posY);
        }
    }

    public void draw(MFGraphics g) {
        if (!this.dead) {
            drawInMap(g, this.drawer);
        }
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (!this.dead) {
            switch (this.state) {
                case 0:
                    if (player.isAttackingEnemy()) {
                        player.doAttackPose(this, direction);
                        beAttack();
                        return;
                    }
                    player.beHurt();
                    return;
                case 1:
                    if (object == player) {
                        player.beHurt();
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    public void doWhileBeAttack(PlayerObject object, int direction, int animationID) {
        if (!this.dead) {
            switch (this.state) {
                case 0:
                    beAttack();
                    return;
                default:
                    return;
            }
        }
    }

    public void refreshCollisionRect(int x, int y) {
        switch (this.state) {
            case 0:
                this.collisionRect.setRect(x - 576, y - PlayerSonic.BACK_JUMP_SPEED_X, BarHorbinV.HOBIN_POWER, BarHorbinV.HOBIN_POWER);
                return;
            case 1:
                this.collisionRect.setRect(x - 1280, y - 1280, BarHorbinV.COLLISION_WIDTH, BarHorbinV.COLLISION_WIDTH);
                return;
            default:
                return;
        }
    }
}
