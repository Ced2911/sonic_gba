package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import Lib.Coordinate;
import com.sega.engine.action.ACCollision;
import com.sega.engine.action.ACObject;
import com.sega.mobile.define.MDPhone;
import com.sega.mobile.framework.device.MFGraphics;

public abstract class EnemyObject extends GameObject {
    protected static Animation BoomAni = null;
    public static final int CANNOT_BE_SEEN = 2;
    protected static final int ENEMY_100PTS = 39;
    protected static final int ENEMY_ASPIRATE_BUBBLE = 40;
    protected static final int ENEMY_BAT = 9;
    protected static final int ENEMY_BEE = 1;
    protected static final int ENEMY_BOOM = 37;
    public static final int ENEMY_BOSS1 = 22;
    protected static final int ENEMY_BOSS1_ARM = 31;
    public static final int ENEMY_BOSS2 = 23;
    protected static final int ENEMY_BOSS2_SPRING = 32;
    public static final int ENEMY_BOSS3 = 24;
    protected static final int ENEMY_BOSS3_HARDBASE = 35;
    protected static final int ENEMY_BOSS3_SHADOW = 34;
    public static final int ENEMY_BOSS4 = 25;
    public static final int ENEMY_BOSS5 = 26;
    protected static final int ENEMY_BOSS5_FLYDEFENCE = 33;
    public static final int ENEMY_BOSS6 = 27;
    public static final int ENEMY_BOSSF1 = 28;
    public static final int ENEMY_BOSSF2 = 29;
    public static final int ENEMY_BOSSF3 = 30;
    public static final int ENEMY_BOSS_EXTRA = 36;
    protected static final int ENEMY_BREAKING_PARTS = 38;
    protected static final int ENEMY_CATERPILLAR = 13;
    protected static final int ENEMY_CHAMELEON = 11;
    protected static final int ENEMY_CLOWN = 10;
    protected static final int ENEMY_CRAB = 2;
    protected static final int ENEMY_DORISAME = 18;
    protected static final int ENEMY_DROWN_BUBBLE = 41;
    protected static final int ENEMY_FROG = 4;
    protected static final int ENEMY_HERIKO = 14;
    protected static final int ENEMY_KORA = 19;
    protected static final int ENEMY_LADYBUG = 6;
    protected static final int ENEMY_LIZARD = 8;
    protected static final int ENEMY_MAGMA = 7;
    protected static final int ENEMY_MIRA = 12;
    protected static final int ENEMY_MOLE = 15;
    protected static final int ENEMY_MONKEY = 0;
    protected static final int ENEMY_MOTOR = 3;
    protected static final int ENEMY_PEN = 20;
    public static final int ENEMY_PROBOSS1 = 21;
    protected static final int ENEMY_RABBIT_FISH = 5;
    protected static final String ENEMY_RES_PATH = "/enemy";
    protected static final int ENEMY_YOKOYUKIMAL = 17;
    protected static final int ENEMY_YUKIMAL = 16;
    public static final int IN_ALERT_RANGE = 0;
    public static final int IN_AVAILABLE_RANGE = 1;
    public static boolean IsBoss = false;
    protected static final int POS_BOTTOM = 1;
    protected static final int POS_LEFT = 2;
    protected static final int POS_RIGHT = 3;
    protected static final int POS_TOP = 0;
    public static boolean isBossEnter = false;
    private static boolean magmaEnable = false;
    protected static Animation snowrobotAnimation;
    protected boolean IsPlayBossBattleBGM;
    private CollisionBlock currentBlock;
    protected boolean dead;
    protected AnimationDrawer drawer;
    private int faceDegree;
    protected int iLeft;
    protected int iTop;
    public int layer;
    private int objId;

    public static EnemyObject getNewInstance(int id, int x, int y, int left, int top, int width, int height) {
        IsBoss = false;
        switch (id) {
            case 1:
                if (GlobalResource.isEasyMode() && PlayerObject.stageModeState == 0) {
                    return null;
                }
                return new Bee(id, x, y, left, top, width, height);
            case 4:
                return new Frog(id, x, y, left, top, width, height);
            case 5:
                if (GlobalResource.isEasyMode() && PlayerObject.stageModeState == 0) {
                    return null;
                }
                return new RabbitFish(id, x, y, left, top, width, height);
            case 6:
                return new LadyBug(id, x, y, left, top, width, height);
            case 7:
                magmaEnable = true;
                return new Magma(id, x, y, left, top, width, height);
            case 15:
                return new Mole(id, x, y, left, top, width, height);
            case 23:
                IsBoss = true;
                return new Boss2(id, x, y, left, top, width, height);
            default:
                switch (id) {
                    case 0:
                        return new Monkey(id, x, y, left, top, width, height);
                    case 2:
                        return new Crab(id, x, y, left, top, width, height);
                    case 3:
                        return new Motor(id, x, y, left, top, width, height);
                    case 8:
                        return new Lizard(id, x, y, left, top, width, height);
                    case 9:
                        if (GlobalResource.isEasyMode() && PlayerObject.stageModeState == 0) {
                            return null;
                        }
                        return new Bat(id, x, y, left, top, width, height);
                    case 10:
                        return new Clown(id, x, y, left, top, width, height);
                    case 13:
                        return new Caterpillar(id, x, y, left, top, width, height);
                    case 21:
                        return new ProBoss1(id, x, y, left, top, width, height);
                    case 22:
                        IsBoss = true;
                        return new Boss1(id, x, y, left, top, width, height);
                    case 24:
                        IsBoss = true;
                        return new Boss3(id, x, y, left, top, width, height);
                    case 26:
                        IsBoss = true;
                        return new Boss5(id, x, y, left, top, width, height);
                    default:
                        switch (id) {
                            case 11:
                                if (GlobalResource.isEasyMode() && PlayerObject.stageModeState == 0) {
                                    return null;
                                }
                                return new Chameleon(id, x, y, left, top, width, height);
                            case 12:
                                if (GlobalResource.isEasyMode() && PlayerObject.stageModeState == 0) {
                                    return null;
                                }
                                return new Mira(id, x, y, left, top, width, height);
                            case 14:
                                return new Heriko(id, x, y, left, top, width, height);
                            case 16:
                                return new SnowRobotH(id, x, y, left, top, width, height);
                            case 17:
                                return new SnowRobotV(id, x, y, left, top, width, height);
                            case 18:
                                return new Fish(id, x, y, left, top, width, height);
                            case 19:
                                return new Cement(id, x, y, left, top, width, height);
                            case 20:
                                if (GlobalResource.isEasyMode() && PlayerObject.stageModeState == 0) {
                                    return null;
                                }
                                return new Penguin(id, x, y, left, top, width, height);
                            case 25:
                                IsBoss = true;
                                return new Boss4(id, x, y, left, top, width, height);
                            case 26:
                                IsBoss = true;
                                return new Boss2(id, x, y, left, top, width, height);
                            case 27:
                                IsBoss = true;
                                return new Boss6(id, x, y, left, top, width, height);
                            case 28:
                                IsBoss = true;
                                return new BossF1(id, x, y, left, top, width, height);
                            case 29:
                                IsBoss = true;
                                return new BossF2(id, x, y, left, top, width, height);
                            case 30:
                                IsBoss = true;
                                return new BossF3(id, x, y, left, top, width, height);
                            case 36:
                                IsBoss = true;
                                return new BossExtra(id, x, y, left, top, width, height);
                            default:
                                return null;
                        }
                }
        }
    }

    public static void releaseAllEnemyResource() {
        Bee.releaseAllResource();
        Frog.releaseAllResource();
        RabbitFish.releaseAllResource();
        LadyBug.releaseAllResource();
        Magma.releaseAllResource();
        Mole.releaseAllResource();
        BossBroken.releaseAllResource();
        Boss2.releaseAllResource();
        Boss2Spring.releaseAllResource();
        Motor.releaseAllResource();
        Monkey.releaseAllResource();
        Crab.releaseAllResource();
        Clown.releaseAllResource();
        Caterpillar.releaseAllResource();
        Bat.releaseAllResource();
        Boss1.releaseAllResource();
        Boss1Arm.releaseAllResource();
        Boss3.releaseAllResource();
        Boss5.releaseAllResource();
        ProBoss1.releaseAllResource();
        Lizard.releaseAllResource();
        Chameleon.releaseAllResource();
        Mira.releaseAllResource();
        Heriko.releaseAllResource();
        SnowRobotH.releaseAllResource();
        SnowRobotV.releaseAllResource();
        Fish.releaseAllResource();
        Cement.releaseAllResource();
        Penguin.releaseAllResource();
        Boss4.releaseAllResource();
        Boss6.releaseAllResource();
        BossF1.releaseAllResource();
        BossF2.releaseAllResource();
        BossF3.releaseAllResource();
        Boom.releaseAllResource();
        BreakingParts.releaseAllResource();
        Bonus100pts.releaseAllResource();
        AspirateBubble.releaseAllResource();
    }

    protected EnemyObject(int id, int x, int y, int left, int top, int width, int height) {
        this.objId = id;
        this.posX = x << 6;
        this.posY = y << 6;
        this.iLeft = left << 6;
        this.iTop = top << 6;
        this.mWidth = (width * 8) << 6;
        this.mHeight = (height * 8) << 6;
        this.posX += this.iLeft * 8;
        this.posY += this.iTop * 8;
        if (width < height) {
            if (width == 0) {
                this.layer = 0;
            } else {
                this.layer = 1;
            }
        } else if (height == 0) {
            this.layer = 0;
        } else {
            this.layer = 1;
        }
        this.IsPlayBossBattleBGM = false;
        refreshCollisionRect(this.posX, this.posY);
    }

    public int getPaintLayer() {
        return 0;
    }

    public void draw(MFGraphics g) {
        Coordinate camera = MapManager.getCamera();
        g.setColor(65280);
        g.drawRect(this.posX - camera.f13x, this.posY - camera.f14y, this.mWidth, this.mHeight);
        g.drawRect((this.posX + 1) - camera.f13x, (this.posY + 1) - camera.f14y, this.mWidth - 2, this.mHeight - 2);
        g.setColor(0);
        g.drawString("ID:" + this.objId + ";iLeft:" + this.iLeft + ";iTop:" + this.iTop, (this.posX - camera.f13x) - 1, (this.posY - camera.f14y) - 1, 36);
        g.drawString("ID:" + this.objId + ";iLeft:" + this.iLeft + ";iTop:" + this.iTop, (this.posX - camera.f13x) + 1, (this.posY - camera.f14y) - 1, 36);
        g.drawString("ID:" + this.objId + ";iLeft:" + this.iLeft + ";iTop:" + this.iTop, this.posX - camera.f13x, ((this.posY - camera.f14y) - 1) - 1, 36);
        g.drawString("ID:" + this.objId + ";iLeft:" + this.iLeft + ";iTop:" + this.iTop, this.posX - camera.f13x, ((this.posY - camera.f14y) - 1) + 1, 36);
        g.setColor(16776960);
        g.drawString("ID:" + this.objId + ";iLeft:" + this.iLeft + ";iTop:" + this.iTop, this.posX - camera.f13x, (this.posY - camera.f14y) - 1, 36);
    }

    public static void enemyinit() {
        magmaEnable = false;
    }

    public static void enemyStaticLogic() {
        if (magmaEnable) {
            Magma.staticlogic();
        }
    }

    public void beAttack() {
        if (!this.dead) {
            this.dead = true;
            Effect.showEffect(destroyEffectAnimation, 0, this.posX >> 6, (this.posY >> 6) - 10, 0);
            if (this.objId == 6 || this.objId == 14) {
                SmallAnimal.addAnimal(this.posX, this.posY, getLayer(), true);
            } else {
                SmallAnimal.addAnimal(this.posX, this.posY, getLayer());
            }
            player.getEnemyScore();
            soundInstance.playSe(29);
            GameObject.addGameObject(new Bonus100pts(39, this.posX, this.posY - MDPhone.SCREEN_HEIGHT, 0, 0, 0, 0));
        }
    }

    public void doWhileBeAttack(PlayerObject object, int direction, int animationID) {
        if (!this.dead) {
            if ((object instanceof PlayerAmy) && (object.getCharacterAnimationID() < 6 || object.getCharacterAnimationID() > 8)) {
                object.setVelY((object.isAntiGravity ? -1 : 1) * PlayerObject.MIN_ATTACK_JUMP);
            }
            if ((player instanceof PlayerTails) && player.myAnimationID == 12 && direction == 1) {
                player.velY = -600;
            }
        }
        beAttack();
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (this.dead || object != player) {
            return;
        }
        if (player.isAttackingEnemy()) {
            player.doAttackPose(this, direction);
            beAttack();
            return;
        }
        player.beHurt();
    }

    public int getLayer() {
        return player.currentLayer;
    }

    public void drawPatrolRect(MFGraphics g, int x, int y, int w, int h) {
        g.setColor(16711680);
        g.drawRect((x >> 6) - camera.f13x, (y >> 6) - camera.f14y, w >> 6, h >> 6);
    }

    public int checkPlayerInEnemyAlertRange(int currentX, int currentY, int range) {
        range >>= 6;
        int src_distance = ((currentX - (player.getCheckPositionX() >> 6)) * (currentX - (player.getCheckPositionX() >> 6))) + ((currentY - (player.getCheckPositionY() >> 6)) * (currentY - (player.getCheckPositionY() >> 6)));
        int range_distance = range * range;
        if (src_distance <= range_distance) {
            return 0;
        }
        if (src_distance <= range_distance || src_distance > range_distance * 6) {
            return 2;
        }
        return 1;
    }

    public int checkPlayerInEnemyAlertRange(int currentX, int currentY, int range_width, int range_height) {
        int range_x = Math.abs(currentX - (player.getFootPositionX() >> 6));
        int range_y = Math.abs(currentY - (player.getFootPositionY() >> 6));
        if (range_x <= range_width && range_y <= range_height) {
            return 0;
        }
        if (range_x > range_width * 2 || range_y > range_height * 2) {
            return 2;
        }
        return 1;
    }

    public boolean checkPlayerInEnemyAlertRangeScale(int currentX, int currentY, int min, int max) {
        int range_x = Math.abs(currentX - (player.getFootPositionX() >> 6));
        int range_y = Math.abs(currentY - (player.getFootPositionY() >> 6));
        if (range_x == 0) {
            return false;
        }
        int scale = (range_y * 100) / range_x;
        return scale >= min && scale <= max;
    }

    public int checkPlayerInEnemyAlertRange(int currentX, int currentY, int range, int limitMinx, int limitMaxX, int enemywidth) {
        int src_distance = ((currentX - (player.getFootPositionX() >> 6)) * (currentX - (player.getFootPositionX() >> 6))) + ((currentY - (player.getFootPositionY() >> 6)) * (currentY - (player.getFootPositionY() >> 6)));
        int range_distance = range * range;
        if ((player.getFootPositionX() >> 6) > (limitMinx - (enemywidth >> 1)) - 8 && (player.getFootPositionX() >> 6) < ((enemywidth >> 1) + limitMaxX) + 8 && (player.getFootPositionY() >> 6) <= currentY + 11 && (player.getFootPositionY() >> 6) >= currentY - 44 && src_distance <= range_distance) {
            return 0;
        }
        if (((player.getFootPositionX() >> 6) > (limitMinx - (enemywidth >> 1)) - 8 || (player.getFootPositionX() >> 6) < limitMinx - (enemywidth * 2)) && ((player.getFootPositionX() >> 6) < ((enemywidth >> 1) + limitMaxX) + 8 || (player.getFootPositionX() >> 6) > (enemywidth * 2) + limitMinx)) {
            return 2;
        }
        return 1;
    }

    public int checkPlayerInEnemyAlertRange(int currentX, int currentY, int range_width, int range_height, int limitMinx, int limitMaxX, int enemywidth) {
        int range_x = Math.abs(currentX - (player.getFootPositionX() >> 6));
        int range_y = Math.abs(currentY - (player.getFootPositionY() >> 6));
        if ((player.getFootPositionX() >> 6) > (limitMinx - (enemywidth >> 1)) - 8 && (player.getFootPositionX() >> 6) < ((enemywidth >> 1) + limitMaxX) + 8 && range_x <= range_width && range_y <= range_height) {
            return 0;
        }
        if (range_x > range_width * 2 || range_y > range_height * 2) {
            return 2;
        }
        return 1;
    }

    public void drawAlertRangeLine(MFGraphics g, int state, int currentX, int currentY, Coordinate camera) {
        switch (state) {
            case 0:
                g.setColor(16711680);
                g.drawLine((player.getFootPositionX() >> 6) - camera.f13x, (player.getFootPositionY() >> 6) - camera.f14y, currentX - camera.f13x, currentY - camera.f14y);
                return;
            case 1:
                g.setColor(65280);
                g.drawLine((player.getFootPositionX() >> 6) - camera.f13x, (player.getFootPositionY() >> 6) - camera.f14y, currentX - camera.f13x, currentY - camera.f14y);
                return;
            default:
                return;
        }
    }

    public void close() {
        this.drawer = null;
    }

    public void doBeforeCollisionCheck() {
    }

    public void doWhileCollision(ACObject arg0, ACCollision arg1, int arg2, int arg3, int arg4, int arg5, int arg6) {
    }
}
