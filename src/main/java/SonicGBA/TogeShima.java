package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: GimmickObject */
class TogeShima extends GimmickObject {
    private static final int COLLISION_HEIGHT = 2048;
    private static final int COLLISION_WIDTH = 3072;
    private static Animation animation;
    private boolean dir = false;
    private AnimationDrawer drawer;
    private int frame;
    public MoveCalculator moveCal;
    private int posEndY;
    private int posOriginalY;
    private int range;
    private ShimaSting ss;
    private int velY = 192;

    protected TogeShima(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        boolean moveDirection;
        if (animation == null) {
            animation = new Animation("/animation/togeshima");
        }
        if (animation != null) {
            this.drawer = animation.getDrawer(0, true, 0);
        }
        this.range = (height * 8) << 6;
        this.posOriginalY = this.posY;
        this.posEndY = this.posY + this.range;
        this.ss = new ShimaSting(x, y);
        GameObject.addGameObject(this.ss);
        this.dir = false;
        if (this.iTop == 0) {
            moveDirection = false;
        } else {
            moveDirection = true;
        }
        this.moveCal = new MoveCalculator(this.posY, this.mHeight, moveDirection);
    }

    public void logic() {
        this.frame = this.drawer.getCurrentFrame();
        this.moveCal.logic();
        int preX = this.posX;
        int preY = this.posY;
        if (this.iTop < 0) {
            this.posY = this.moveCal.getPosition();
        } else {
            this.posY = this.posOriginalY + (this.posOriginalY - this.moveCal.getPosition());
        }
        refreshCollisionRect(this.posX, this.posY);
        this.ss.logic(this.posX, this.posY, this.frame);
        checkWithPlayer(preX, preY, this.posX, this.posY);
    }

    public void draw(MFGraphics g) {
        drawInMap(g, this.drawer, this.posX, this.posY);
        drawCollisionRect(g);
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (direction == 1 || direction == 0) {
            object.beStop(0, direction, this);
        }
        if (direction != 2 && direction != 3) {
            return;
        }
        if (object.getFootPositionY() >= this.collisionRect.y0 + ((this.mHeight * 3) / 4)) {
            object.beStop(0, direction, this);
        } else if (object.getVelX() > 0 && direction == 3) {
            object.setFootPositionY(this.collisionRect.y0 - 64);
        } else if (object.getVelX() < 0 && direction == 2) {
            object.setFootPositionY(this.collisionRect.y0 - 64);
        }
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 1536, y - 1024, 3072, 2048);
    }

    public void close() {
        this.drawer = null;
    }

    public static void releaseAllResource() {
        Animation.closeAnimation(animation);
        animation = null;
    }
}
