package SonicGBA;

import GameEngine.Def;
import GameEngine.Key;
import Lib.Animation;
import Lib.AnimationDrawer;
import Lib.Coordinate;
import Lib.MyAPI;
import Lib.SoundSystem;
import com.sega.mobile.define.MDPhone;
import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;

public class PlayerKnuckles extends PlayerObject {
    private static final int[] ANIMATION_CONVERT;
    private static final int ATTACK_LEVEL_1 = 1;
    private static final int ATTACK_LEVEL_2 = 2;
    private static final int ATTACK_LEVEL_3 = 3;
    private static final int ATTACK_LEVEL_NONE = 0;
    public static final int COLLISION_STATE_CLIMB = 4;
    private static final int DRIP_SPEED_Y_LIMIT = 128;
    private static final int FALL_ADD_SPEED = 337;
    private static final int FLY_DEGREE_VELOCITY = 20;
    private static final int FLY_DOWN_SPEED = 90;
    private static final int FLY_GROUND_SPEED = 64;
    private static final int FLY_MAX_SPEED = 4608;
    private static final int FLY_MIN_SPEED = 768;
    private static final int FLY_SPEED_PLUS = 22;
    private static final int FLY_START_ADD_Y_SPEED = 384;
    private static final int FLY_START_X_SPEED = 768;
    private static final int FLY_VELOCITY = 600;
    public static final int KNUCKLES_ANI_ATTACK_1 = 11;
    public static final int KNUCKLES_ANI_ATTACK_2 = 12;
    public static final int KNUCKLES_ANI_ATTACK_3 = 13;
    public static final int KNUCKLES_ANI_BANK_1 = 49;
    public static final int KNUCKLES_ANI_BANK_2 = 50;
    public static final int KNUCKLES_ANI_BANK_3 = 51;
    public static final int KNUCKLES_ANI_BAR_MOVE = 47;
    public static final int KNUCKLES_ANI_BAR_STAY = 46;
    public static final int KNUCKLES_ANI_BRAKE = 56;
    public static final int KNUCKLES_ANI_BREATHE = 36;
    public static final int KNUCKLES_ANI_CAUGHT = 60;
    public static final int KNUCKLES_ANI_CELEBRATE_1 = 52;
    public static final int KNUCKLES_ANI_CELEBRATE_2 = 53;
    public static final int KNUCKLES_ANI_CELEBRATE_3 = 54;
    public static final int KNUCKLES_ANI_CLIFF_1 = 37;
    public static final int KNUCKLES_ANI_CLIFF_2 = 38;
    public static final int KNUCKLES_ANI_CLIMB_1 = 29;
    public static final int KNUCKLES_ANI_CLIMB_2 = 30;
    public static final int KNUCKLES_ANI_CLIMB_3 = 31;
    public static final int KNUCKLES_ANI_CLIMB_4 = 32;
    public static final int KNUCKLES_ANI_CLIMB_5 = 33;
    public static final int KNUCKLES_ANI_DEAD_1 = 26;
    public static final int KNUCKLES_ANI_DEAD_2 = 27;
    public static final int KNUCKLES_ANI_ENTER_SP = 58;
    public static final int KNUCKLES_ANI_FLY_1 = 19;
    public static final int KNUCKLES_ANI_FLY_2 = 20;
    public static final int KNUCKLES_ANI_FLY_3 = 21;
    public static final int KNUCKLES_ANI_FLY_4 = 22;
    public static final int KNUCKLES_ANI_HURT_1 = 24;
    public static final int KNUCKLES_ANI_HURT_2 = 25;
    public static final int KNUCKLES_ANI_JUMP = 4;
    public static final int KNUCKLES_ANI_LOOK_UP_1 = 7;
    public static final int KNUCKLES_ANI_LOOK_UP_2 = 8;
    public static final int KNUCKLES_ANI_POLE_H = 45;
    public static final int KNUCKLES_ANI_POLE_V = 44;
    public static final int KNUCKLES_ANI_PUSH_WALL = 23;
    public static final int KNUCKLES_ANI_RAIL_BODY = 57;
    public static final int KNUCKLES_ANI_ROLL_H_1 = 41;
    public static final int KNUCKLES_ANI_ROLL_H_2 = 42;
    public static final int KNUCKLES_ANI_ROLL_V_1 = 39;
    public static final int KNUCKLES_ANI_ROLL_V_2 = 40;
    public static final int KNUCKLES_ANI_RUN = 3;
    public static final int KNUCKLES_ANI_SPIN_1 = 5;
    public static final int KNUCKLES_ANI_SPIN_2 = 6;
    public static final int KNUCKLES_ANI_SPRING_1 = 14;
    public static final int KNUCKLES_ANI_SPRING_2 = 15;
    public static final int KNUCKLES_ANI_SPRING_3 = 16;
    public static final int KNUCKLES_ANI_SPRING_4 = 17;
    public static final int KNUCKLES_ANI_SPRING_5 = 18;
    public static final int KNUCKLES_ANI_SQUAT_1 = 9;
    public static final int KNUCKLES_ANI_SQUAT_2 = 10;
    public static final int KNUCKLES_ANI_STAND = 0;
    public static final int KNUCKLES_ANI_SWIM_1 = 28;
    public static final int KNUCKLES_ANI_SWIM_2 = 34;
    public static final int KNUCKLES_ANI_SWIM_3 = 35;
    public static final int KNUCKLES_ANI_SWIM_EFFECT = 59;
    public static final int KNUCKLES_ANI_UP_ARM = 43;
    public static final int KNUCKLES_ANI_VS_KNUCKLE = 55;
    public static final int KNUCKLES_ANI_WAITING_1 = 61;
    public static final int KNUCKLES_ANI_WAITING_2 = 62;
    public static final int KNUCKLES_ANI_WALK_1 = 1;
    public static final int KNUCKLES_ANI_WALK_2 = 2;
    public static final int KNUCKLES_ANI_WIND = 48;
    private static final int KNUCKLES_ATTACK_1_COUNT = 4;
    private static final int KNUCKLES_ATTACK_2_COUNT = 4;
    private static final int KNUCKLES_ATTACK_3_COUNT = 6;
    private static final int LOOP = -1;
    private static final int[] LOOP_INDEX;
    private static final int NORMAL_FRAME_INTERVAL = 3;
    private static final int NO_ANIMATION = -1;
    private static final int NO_LOOP = -2;
    private static final int NO_LOOP_DEPAND = -2;
    private static final int PUNCH_MOVE01 = 768;
    private static final int UPPER_INWATER_SPEED = 832;
    private static final int UPPER_SPEED = 719;
    private static final int WALL_CLIMB_SPEED = 192;
    private static final int WALL_CLIMB_SPEED_W = 256;
    private static final int WATER_FRAME_INTERVAL = 6;
    private static int slipbrakeFrame;
    private int attackCount;
    private int attackLevel;
    private int attackLevelNext;
    private PlayerAnimationCollisionRect attackRect;
    private AnimationDrawer effectDrawer;
    private boolean floating = false;
    private int flyDegree;
    private int flyDegreeStable;
    private int flySpeed;
    public boolean flying;
    private boolean isSlipActived;
    private AnimationDrawer knucklesDrawer1;
    private AnimationDrawer knucklesDrawer2;
    private boolean preWaterFlag;
    private boolean swimWaterEffectFlag = false;
    private int waterframe;

    static {
        int[] iArr = new int[54];
        iArr[1] = 1;
        iArr[2] = 2;
        iArr[3] = 3;
        iArr[4] = 4;
        iArr[5] = 10;
        iArr[6] = 5;
        iArr[7] = 6;
        iArr[8] = 23;
        iArr[9] = 14;
        iArr[10] = 18;
        iArr[11] = -1;
        iArr[12] = 25;
        iArr[13] = 44;
        iArr[14] = 15;
        iArr[15] = -1;
        iArr[16] = -1;
        iArr[17] = 56;
        iArr[18] = -1;
        iArr[19] = -1;
        iArr[20] = -1;
        iArr[21] = 57;
        iArr[22] = 41;
        iArr[23] = 42;
        iArr[24] = 43;
        iArr[25] = 39;
        iArr[26] = 40;
        iArr[27] = 46;
        iArr[28] = 47;
        iArr[29] = 48;
        iArr[30] = 25;
        iArr[31] = 45;
        iArr[32] = 49;
        iArr[33] = 50;
        iArr[34] = 51;
        iArr[35] = 52;
        iArr[36] = 53;
        iArr[37] = 54;
        iArr[38] = 7;
        iArr[39] = 8;
        iArr[40] = 7;
        iArr[41] = 27;
        iArr[42] = 16;
        iArr[43] = 17;
        iArr[44] = 25;
        iArr[45] = 26;
        iArr[46] = 9;
        iArr[47] = 37;
        iArr[48] = 38;
        iArr[49] = 36;
        iArr[50] = 61;
        iArr[51] = 62;
        iArr[52] = 60;
        iArr[53] = 55;
        ANIMATION_CONVERT = iArr;
        iArr = new int[64];
        iArr[0] = -1;
        iArr[1] = -1;
        iArr[2] = -1;
        iArr[3] = -1;
        iArr[4] = -1;
        iArr[5] = -1;
        iArr[6] = -1;
        iArr[7] = 8;
        iArr[8] = -1;
        iArr[9] = -2;
        iArr[10] = -1;
        iArr[13] = 18;
        iArr[14] = 17;
        iArr[15] = -1;
        iArr[16] = 17;
        iArr[17] = 18;
        iArr[18] = -1;
        iArr[19] = -1;
        iArr[20] = -1;
        iArr[21] = -1;
        iArr[22] = -1;
        iArr[23] = -1;
        iArr[24] = 25;
        iArr[25] = -1;
        iArr[26] = 27;
        iArr[27] = -1;
        iArr[28] = -1;
        iArr[29] = 30;
        iArr[30] = -1;
        iArr[31] = -1;
        iArr[32] = -1;
        iArr[34] = -1;
        iArr[35] = -1;
        iArr[36] = -2;
        iArr[37] = -1;
        iArr[38] = -1;
        iArr[39] = 40;
        iArr[40] = 39;
        iArr[41] = 42;
        iArr[42] = 41;
        iArr[43] = -1;
        iArr[44] = 15;
        iArr[45] = 3;
        iArr[46] = -1;
        iArr[47] = -1;
        iArr[48] = -1;
        iArr[49] = -1;
        iArr[50] = -1;
        iArr[51] = -1;
        iArr[52] = 53;
        iArr[53] = -1;
        iArr[54] = -2;
        iArr[56] = -1;
        iArr[57] = -1;
        iArr[58] = -1;
        iArr[59] = -1;
        iArr[60] = 62;
        iArr[61] = 62;
        iArr[62] = -1;
        LOOP_INDEX = iArr;
    }

    public PlayerKnuckles() {
        MFImage chaImage = MFImage.createImage("/animation/player/chr_knuckles.png");
        Animation animation1 = new Animation(chaImage, "/animation/player/chr_knuckles_01");
        this.knucklesDrawer1 = animation1.getDrawer();
        this.effectDrawer = animation1.getDrawer();
        this.knucklesDrawer2 = new Animation(chaImage, "/animation/player/chr_knuckles_02").getDrawer();
        this.drawer = this.knucklesDrawer1;
        this.attackRect = new PlayerAnimationCollisionRect(this);
    }

    public void closeImpl() {
        Animation.closeAnimationDrawer(this.knucklesDrawer1);
        this.knucklesDrawer1 = null;
        Animation.closeAnimationDrawer(this.knucklesDrawer2);
        this.knucklesDrawer2 = null;
        Animation.closeAnimationDrawer(this.effectDrawer);
        this.effectDrawer = null;
    }

    public void drawCharacter(MFGraphics g) {
        int trans;
        int bodyCenterY;
        int bodyCenterX;
        Coordinate camera = MapManager.getCamera();
        if (this.animationID != -1) {
            this.myAnimationID = ANIMATION_CONVERT[this.animationID];
        }
        if (this.myAnimationID != -1) {
            boolean loop;
            if (LOOP_INDEX[this.myAnimationID] == -1) {
                loop = true;
            } else {
                loop = false;
            }
            this.drawer = this.knucklesDrawer1;
            int drawerActionID = this.myAnimationID;
            if (this.myAnimationID >= 61) {
                this.drawer = this.knucklesDrawer2;
                drawerActionID = this.myAnimationID - 61;
                if (this.myAnimationID == 61 && this.isResetWaitAni) {
                    this.drawer.restart();
                    this.isResetWaitAni = false;
                }
            }
            if (this.isInWater) {
                this.drawer.setSpeed(1, 2);
            } else {
                this.drawer.setSpeed(1, 1);
            }
            if (this.hurtCount % 2 != 0) {
                if (drawerActionID != this.drawer.getActionId()) {
                    this.drawer.setActionId(drawerActionID);
                }
                if (!AnimationDrawer.isAllPause()) {
                    this.drawer.moveOn();
                }
            } else if (this.animationID == 4) {
                bodyCenterX = getNewPointX(this.footPointX, 0, -512, this.faceDegree);
                bodyCenterY = getNewPointY(this.footPointY, 0, -512, this.faceDegree);
                int drawX = getNewPointX(bodyCenterX, 0, 512, 0);
                int drawY = getNewPointY(bodyCenterY, 0, 512, 0);
                if (this.collisionState == (byte) 0) {
                    if (this.isAntiGravity) {
                        if (this.faceDirection) {
                            trans = this.totalVelocity >= 0 ? 3 : 1;
                            drawY -= 1024;
                        } else {
                            trans = this.totalVelocity > 0 ? 3 : 1;
                            drawY -= 1024;
                        }
                    } else if (this.faceDirection) {
                        trans = this.totalVelocity >= 0 ? 0 : 2;
                    } else {
                        trans = this.totalVelocity > 0 ? 0 : 2;
                    }
                } else if (this.isAntiGravity) {
                    if (this.faceDirection) {
                        trans = this.velX <= 0 ? 3 : 1;
                        drawY -= 1024;
                    } else {
                        trans = this.velX < 0 ? 3 : 1;
                        drawY -= 1024;
                    }
                } else if (this.faceDirection) {
                    trans = this.velX >= 0 ? 0 : 2;
                } else {
                    trans = this.velX > 0 ? 0 : 2;
                }
                this.drawer.draw(g, drawerActionID, (drawX >> 6) - camera.f13x, (drawY >> 6) - camera.f14y, loop, trans);
            } else if (this.animationID == 6 || this.animationID == 7) {
                drawDrawerByDegree(g, this.drawer, drawerActionID, (this.footPointX >> 6) - camera.f13x, (this.footPointY >> 6) - camera.f14y, loop, this.degreeForDraw, !this.faceDirection);
            } else {
                if (this.myAnimationID == 19 || this.myAnimationID == 20 || this.myAnimationID == 21 || this.myAnimationID == 28) {
                    int degre;
                    if (this.flyDegree >= 0) {
                        if (this.isAntiGravity) {
                            degre = 1;
                        } else {
                            degre = 0;
                        }
                    } else if (this.isAntiGravity) {
                        degre = 3;
                    } else {
                        degre = 2;
                    }
                    this.drawer.draw(g, drawerActionID, (this.footPointX >> 6) - camera.f13x, (this.footPointY >> 6) - camera.f14y, loop, degre);
                } else {
                    if (this.myAnimationID == 37 || this.myAnimationID == 38 || this.myAnimationID == 7 || this.myAnimationID == 8) {
                        this.degreeForDraw = this.degreeStable;
                        this.faceDegree = this.degreeStable;
                    }
                    if (this.myAnimationID == 56) {
                        this.degreeForDraw = this.degreeStable;
                    }
                    if (!(this.myAnimationID == 1 || this.myAnimationID == 2 || this.myAnimationID == 3 || this.myAnimationID == 60)) {
                        this.degreeForDraw = this.degreeStable;
                    }
                    if (this.fallinSandSlipState != 0) {
                        if (this.fallinSandSlipState == 1) {
                            this.faceDirection = true;
                        } else if (this.fallinSandSlipState == 2) {
                            this.faceDirection = false;
                        }
                    }
                    if (this.faceDirection) {
                        trans = 0;
                    } else {
                        trans = 2;
                    }
                    if (this.degreeForDraw != this.faceDegree) {
                        bodyCenterX = getNewPointX(this.footPointX, 0, (-this.collisionRect.getHeight()) >> 1, this.faceDegree);
                        bodyCenterY = getNewPointY(this.footPointY, 0, (-this.collisionRect.getHeight()) >> 1, this.faceDegree);
                        g.saveCanvas();
                        g.translateCanvas((bodyCenterX >> 6) - camera.f13x, (bodyCenterY >> 6) - camera.f14y);
                        g.rotateCanvas((float) this.degreeForDraw);
                        this.drawer.draw(g, drawerActionID, 0, (this.collisionRect.getHeight() >> 1) >> 6, loop, trans);
                        g.restoreCanvas();
                    } else {
                        boolean z;
                        AnimationDrawer animationDrawer = this.drawer;
                        int i = (this.footPointX >> 6) - camera.f13x;
                        int i2 = (this.footPointY >> 6) - camera.f14y;
                        int i3 = this.degreeForDraw;
                        if (this.faceDirection) {
                            z = false;
                        } else {
                            z = true;
                        }
                        drawDrawerByDegree(g, animationDrawer, drawerActionID, i, i2, loop, i3, z);
                    }
                }
                if (this.myAnimationID == 34 && this.swimWaterEffectFlag) {
                    this.effectDrawer.draw(g, 59, (this.footPointX >> 6) - camera.f13x, (((StageManager.getWaterLevel() << 6) >> 6) + 14) - camera.f14y, true, getTrans());
                }
            }
            this.attackRectVec.removeAllElements();
            byte[] rect = this.drawer.getARect();
            if (this.isAntiGravity) {
                byte[] rectTmp = this.drawer.getARect();
                if (rectTmp != null) {
                    if (this.attackLevel != 0) {
                        rect[0] = (byte) ((-rectTmp[0]) - rectTmp[2]);
                    } else {
                        if (this.flying && this.faceDirection) {
                            rect[0] = (byte) ((-rectTmp[0]) - rectTmp[2]);
                        }
                        rect[1] = (byte) ((-rectTmp[1]) - rectTmp[3]);
                    }
                }
            }
            if (rect != null) {
                if (SonicDebug.showCollisionRect) {
                    g.setColor(65280);
                    g.drawRect(((this.footPointX >> 6) + rect[0]) - camera.f13x, ((this.footPointY >> 6) + (this.isAntiGravity ? (-rect[1]) - rect[3] : rect[1])) - camera.f14y, rect[2], rect[3]);
                }
                this.attackRect.initCollision(rect[0] << 6, rect[1] << 6, rect[2] << 6, rect[3] << 6, this.myAnimationID);
                this.attackRectVec.addElement(this.attackRect);
            } else {
                this.attackRect.reset();
            }
            if (this.animationID == -1 && this.drawer.checkEnd() && LOOP_INDEX[this.myAnimationID] >= 0) {
                switch (this.myAnimationID) {
                    case 33:
                        this.canAttackByHari = true;
                        this.collisionState = (byte) 1;
                        this.worldCal.actionState = (byte) 1;
                        break;
                }
                this.myAnimationID = LOOP_INDEX[this.myAnimationID];
            }
        }
    }

    public void beSpring(int springPower, int direction) {
        if ((this.myAnimationID == 21 || this.myAnimationID == 22) && (direction == 3 || direction == 2)) {
            this.flying = false;
            this.animationID = 0;
        }
        super.beSpring(springPower, direction);
    }

    protected void extraLogicJump() {
        boolean z;
        if (this.myAnimationID == 19 || this.myAnimationID == 20 || this.myAnimationID == 21 || this.myAnimationID == 28) {
            z = true;
        } else {
            z = false;
        }
        this.flying = z;
        if (this.flying) {
            if (Key.repeat(16777216 | Key.gUp)) {
                if (this.flySpeed < 768) {
                    this.flySpeed += 22;
                } else if (this.flySpeed < FLY_MAX_SPEED && (this.flyDegree == FLY_DOWN_SPEED || this.flyDegree == -90)) {
                    this.flySpeed += 11;
                }
                this.flyDegreeStable = (this.isAntiGravity ^ this.faceDirection) != false ? FLY_DOWN_SPEED : -90;
                this.flyDegree = MyAPI.calNextPosition((double) this.flyDegree, (double) this.flyDegreeStable, 1, 100, 20.0d);
                this.velX = (this.flySpeed * MyAPI.dSin(this.flyDegree)) / 100;
                if (this.isAntiGravity) {
                    if (this.velY > Def.TOUCH_HELP_LEFT_X) {
                        this.velY -= FLY_DOWN_SPEED;
                    } else {
                        this.velY += FLY_DOWN_SPEED;
                    }
                    this.velY += getGravity();
                } else {
                    if (this.velY < 128) {
                        this.velY += FLY_DOWN_SPEED;
                    } else {
                        this.velY -= FLY_DOWN_SPEED;
                    }
                    this.velY -= getGravity();
                }
                if (Math.abs(this.flyDegree) >= 75) {
                    this.myAnimationID = 19;
                    if (this.isInWater) {
                        this.myAnimationID = 28;
                    }
                } else if (Math.abs(this.flyDegree) >= 25) {
                    this.myAnimationID = 20;
                } else {
                    this.myAnimationID = 21;
                }
            } else {
                this.flying = false;
                this.myAnimationID = 17;
                this.velX >>= 2;
            }
        } else if (this.animationID == 4 && this.doJumpForwardly && Key.press(16777216 | Key.gUp)) {
            this.animationID = -1;
            this.myAnimationID = 19;
            if (this.isInWater) {
                this.myAnimationID = 28;
            }
            this.flyDegreeStable = (this.isAntiGravity ^ this.faceDirection) != false ? FLY_DOWN_SPEED : -90;
            this.velY = 0;
            this.flyDegree = this.flyDegreeStable;
            this.flying = true;
            this.velY += 384;
            if (this.velY < 0) {
                this.velY = 0;
            }
            this.velY -= getGravity();
            this.flySpeed = 768;
            int i = this.footPointY - 512;
            this.footPointY = i;
            this.posY = i;
        }
        floatchk();
        int waterLevel = StageManager.getWaterLevel() << 6;
        if (this.floating) {
            this.breatheCount = 0;
        }
        if (!this.floating || !this.isInWater) {
            return;
        }
        if (Key.press(Key.gDown)) {
            this.floating = false;
            this.animationID = 10;
        } else if (Key.press(16777216)) {
            doJump();
            this.velY = (this.velY * 3) / 5;
            this.floating = false;
        } else {
            int bodyCenterY = getNewPointY(this.posY, 0, (-this.collisionRect.getHeight()) >> 1, this.faceDegree);
            this.animationID = -1;
            if (Key.repeat(33554432)) {
                this.myAnimationID = 35;
                this.focusMovingState = 1;
            } else if (bodyCenterY - 320 <= waterLevel) {
                this.myAnimationID = 34;
            }
            if (this.floating) {
                this.velY -= getGravity();
                this.velY -= FALL_ADD_SPEED;
                this.velY = Math.max(-1920, this.velY);
                this.velY = Math.max(waterLevel - bodyCenterY, this.velY);
            }
            this.swimWaterEffectFlag = false;
            if (Math.abs(bodyCenterY - waterLevel) < MDPhone.SCREEN_HEIGHT) {
                this.swimWaterEffectFlag = true;
            } else {
                resetBreatheCount();
            }
        }
    }

    public void dripDownUnderWater() {
        if (this.floating) {
            this.floating = false;
            this.animationID = 10;
        }
    }

    private void slipBrakeNoise() {
        if (!this.isSlipActived) {
            SoundSystem soundSystem = soundInstance;
            SoundSystem soundSystem2 = soundInstance;
            soundSystem.playSequenceSe(6);
            this.isSlipActived = true;
        }
    }

    protected void extraLogicWalk() {
        int i;
        int i2;
        this.flying = false;
        this.floating = false;
        if (this.myAnimationID == 22) {
            if (this.totalVelocity == 0) {
                this.animationID = 0;
                this.isSlipActived = false;
            } else if (!Key.repeat(16777216 | Key.gUp) || ((this.faceDegree >= 45 && this.faceDegree <= 315 && !this.isAntiGravity) || (this.faceDegree < 45 && this.faceDegree > 315 && this.isAntiGravity))) {
                this.animationID = 0;
                this.velX = 0;
                this.isSlipActived = false;
            } else {
                if (this.faceDirection) {
                    this.totalVelocity = Math.abs(this.totalVelocity);
                } else {
                    this.totalVelocity = -Math.abs(this.totalVelocity);
                }
                int preTotalVelocity = this.totalVelocity;
                i2 = this.totalVelocity;
                if (this.faceDirection) {
                    i = 1;
                } else {
                    i = -1;
                }
                this.totalVelocity = i2 - (i * 64);
                if (this.totalVelocity * preTotalVelocity <= 0) {
                    this.totalVelocity = 0;
                }
                Effect.showEffect(this.dustEffectAnimation, 2, this.posX >> 6, this.posY >> 6, 0);
                slipBrakeNoise();
            }
        }
        if (this.attackCount > 0) {
            this.attackCount--;
        }
        SoundSystem soundSystem;
        SoundSystem soundSystem2;
        switch (this.attackLevel) {
            case 0:
                if ((this.animationID == 0 || this.animationID == 1 || this.animationID == 2 || this.animationID == 3 || this.animationID == 47 || this.animationID == 48 || this.animationID == 5) && Key.press(Key.gSelect) && this.myAnimationID != 23) {
                    this.attackLevel = 1;
                    this.animationID = -1;
                    this.myAnimationID = 11;
                    calDivideVelocity();
                    this.velX = ((this.isAntiGravity ^ this.faceDirection) != false ? 1 : -1) * 768;
                    calTotalVelocity();
                    if (this.isInWater) {
                        i2 = 2;
                    } else {
                        i2 = 1;
                    }
                    this.attackCount = i2 * 4;
                    this.attackLevelNext = 0;
                    soundSystem = soundInstance;
                    soundSystem2 = soundInstance;
                    soundSystem.playSe(19);
                    this.drawer.setActionId(11);
                    break;
                }
            case 1:
                if (this.attackCount != 0) {
                    if (Key.press(Key.gSelect)) {
                        this.attackLevelNext = 2;
                        break;
                    }
                }
                this.attackLevel = this.attackLevelNext;
                switch (this.attackLevelNext) {
                    case 0:
                        this.animationID = 0;
                        break;
                    case 2:
                        this.animationID = -1;
                        this.myAnimationID = 12;
                        calDivideVelocity();
                        if ((this.isAntiGravity ^ this.faceDirection) != false) {
                            i2 = 1;
                        } else {
                            i2 = -1;
                        }
                        this.velX = i2 * 768;
                        calTotalVelocity();
                        if (this.isInWater) {
                            i2 = 2;
                        } else {
                            i2 = 1;
                        }
                        this.attackCount = i2 * 4;
                        soundSystem = soundInstance;
                        soundSystem2 = soundInstance;
                        soundSystem.playSe(19);
                        break;
                }
                this.attackLevelNext = 0;
                break;
            case 2:
                if (this.attackCount != 0) {
                    if (Key.press(Key.gSelect)) {
                        this.attackLevelNext = 3;
                        break;
                    }
                }
                this.attackLevel = this.attackLevelNext;
                switch (this.attackLevelNext) {
                    case 0:
                        this.animationID = 0;
                        break;
                    case 3:
                        this.animationID = -1;
                        this.myAnimationID = 13;
                        this.worldCal.actionState = (byte) 1;
                        this.collisionState = (byte) 1;
                        int jump = this.isInWater ? UPPER_INWATER_SPEED : UPPER_SPEED;
                        i2 = this.velY;
                        if (this.isAntiGravity) {
                            i = -1;
                        } else {
                            i = 1;
                        }
                        this.velY = i2 + (i * (-jump));
                        if ((this.isAntiGravity ^ this.faceDirection) != false) {
                            i2 = 1;
                        } else {
                            i2 = -1;
                        }
                        this.velX = i2 * jump;
                        if (this.isInWater) {
                            i2 = 2;
                        } else {
                            i2 = 1;
                        }
                        this.attackCount = i2 * 6;
                        soundSystem = soundInstance;
                        soundSystem2 = soundInstance;
                        soundSystem.playSe(20);
                        break;
                }
                this.attackLevelNext = 0;
                break;
            case 3:
                if (this.animationID != -1) {
                    this.attackLevel = 0;
                    break;
                }
                break;
        }
        this.preWaterFlag = this.isInWater;
        if (isBodyCenterOutOfWater()) {
            this.floating = false;
        }
        if (this.attackLevel == 0) {
            this.isAttacking = false;
        } else {
            this.isAttacking = true;
        }
    }

    protected void extraLogicOnObject() {
        this.flying = false;
        if (this.myAnimationID == 22) {
            if (this.velX == 0) {
                this.animationID = 0;
                this.isSlipActived = false;
            } else if (!Key.repeat(16777216 | Key.gUp) || ((this.faceDegree >= 45 && this.faceDegree <= 315 && !this.isAntiGravity) || (this.faceDegree < 45 && this.faceDegree > 315 && this.isAntiGravity))) {
                this.animationID = 0;
                this.velX = 0;
                this.isSlipActived = false;
            } else {
                int preTotalVelocity = this.velX;
                this.velX -= (this.faceDirection ? 1 : -1) * 64;
                if (this.velX * preTotalVelocity <= 0) {
                    this.velX = 0;
                }
                Effect.showEffect(this.dustEffectAnimation, 2, this.posX >> 6, this.posY >> 6, 0);
                slipBrakeNoise();
            }
        }
        if (this.attackCount > 0) {
            this.attackCount--;
        }
        int i;
        SoundSystem soundSystem;
        SoundSystem soundSystem2;
        switch (this.attackLevel) {
            case 0:
                if ((this.animationID == 0 || this.animationID == 1 || this.animationID == 2 || this.animationID == 3 || this.animationID == 47 || this.animationID == 48 || this.animationID == 5) && Key.press(Key.gSelect) && this.myAnimationID != 23) {
                    this.attackLevel = 1;
                    this.animationID = -1;
                    this.myAnimationID = 11;
                    if ((this.isAntiGravity ^ this.faceDirection) != false) {
                        i = 1;
                    } else {
                        i = -1;
                    }
                    this.velX = i * 768;
                    if (this.isInWater) {
                        i = 2;
                    } else {
                        i = 1;
                    }
                    this.attackCount = i * 4;
                    this.attackLevelNext = 0;
                    soundSystem = soundInstance;
                    soundSystem2 = soundInstance;
                    soundSystem.playSe(19);
                    this.drawer.setActionId(11);
                    break;
                }
            case 1:
                if (this.attackCount != 0) {
                    if (Key.press(Key.gSelect)) {
                        this.attackLevelNext = 2;
                        break;
                    }
                }
                this.attackLevel = this.attackLevelNext;
                switch (this.attackLevelNext) {
                    case 0:
                        this.animationID = 0;
                        break;
                    case 2:
                        this.animationID = -1;
                        this.myAnimationID = 12;
                        if ((this.isAntiGravity ^ this.faceDirection) != false) {
                            i = 1;
                        } else {
                            i = -1;
                        }
                        this.velX = i * 768;
                        if (this.isInWater) {
                            i = 2;
                        } else {
                            i = 1;
                        }
                        this.attackCount = i * 4;
                        soundSystem = soundInstance;
                        soundSystem2 = soundInstance;
                        soundSystem.playSe(19);
                        break;
                }
                this.attackLevelNext = 0;
                break;
            case 2:
                if (this.attackCount != 0) {
                    if (Key.press(Key.gSelect)) {
                        this.attackLevelNext = 3;
                        break;
                    }
                }
                this.attackLevel = this.attackLevelNext;
                switch (this.attackLevelNext) {
                    case 0:
                        this.animationID = 0;
                        break;
                    case 3:
                        int i2;
                        this.animationID = -1;
                        this.myAnimationID = 13;
                        this.worldCal.actionState = (byte) 1;
                        this.collisionState = (byte) 1;
                        int jump = this.isInWater ? UPPER_INWATER_SPEED : UPPER_SPEED;
                        i = this.velY;
                        if (this.isAntiGravity) {
                            i2 = -1;
                        } else {
                            i2 = 1;
                        }
                        this.velY = i + (i2 * (-jump));
                        if ((this.isAntiGravity ^ this.faceDirection) != false) {
                            i = 1;
                        } else {
                            i = -1;
                        }
                        this.velX = i * jump;
                        if (this.isInWater) {
                            i = 2;
                        } else {
                            i = 1;
                        }
                        this.attackCount = i * 6;
                        soundSystem = soundInstance;
                        soundSystem2 = soundInstance;
                        soundSystem.playSe(20);
                        break;
                }
                this.attackLevelNext = 0;
                break;
            case 3:
                if (this.animationID != -1) {
                    this.attackLevel = 0;
                    break;
                }
                break;
        }
        if (this.attackLevel == 0) {
            this.isAttacking = false;
        } else {
            this.isAttacking = true;
        }
    }

    protected void extraInputLogic() {
        this.swimWaterEffectFlag = false;
        switch (this.collisionState) {
            case (byte) 4:
                inputLogicClimb();
                return;
            default:
                return;
        }
    }

    private void inputLogicClimb() {
        this.animationID = -1;
        this.velX = 0;
        this.velY = 0;
        if (this.myAnimationID != 33) {
            SoundSystem soundSystem;
            SoundSystem soundSystem2;
            this.myAnimationID = 30;
            if ((Key.repeat(33554432) && !this.isAntiGravity) || (Key.repeat(Key.gDown) && this.isAntiGravity)) {
                if (this.isInWater) {
                    this.waterframe++;
                    this.waterframe %= 6;
                    if (this.waterframe == 1) {
                        soundSystem = soundInstance;
                        soundSystem2 = soundInstance;
                        soundSystem.playSequenceSe(18);
                    }
                } else {
                    this.waterframe++;
                    this.waterframe %= 3;
                    if (this.waterframe == 1) {
                        soundSystem = soundInstance;
                        soundSystem2 = soundInstance;
                        soundSystem.playSequenceSe(18);
                    }
                }
                this.velY = this.isInWater ? GimmickObject.PLATFORM_OFFSET_Y : -192;
                this.myAnimationID = this.isAntiGravity ? 32 : 31;
            }
            if ((Key.repeat(Key.gDown) && !this.isAntiGravity) || (Key.repeat(33554432) && this.isAntiGravity)) {
                if (this.isInWater) {
                    this.waterframe++;
                    this.waterframe %= 6;
                    if (this.waterframe == 1) {
                        soundSystem = soundInstance;
                        soundSystem2 = soundInstance;
                        soundSystem.playSequenceSe(18);
                    }
                } else {
                    this.waterframe++;
                    this.waterframe %= 3;
                    if (this.waterframe == 1) {
                        soundSystem = soundInstance;
                        soundSystem2 = soundInstance;
                        soundSystem.playSequenceSe(18);
                    }
                }
                this.velY = this.isInWater ? 256 : 192;
                this.myAnimationID = this.isAntiGravity ? 31 : 32;
            }
            if (Key.press(16777216 | Key.gUp)) {
                soundInstance.stopLoopSe();
                this.collisionState = (byte) 1;
                this.velY = this.isAntiGravity ? PlayerObject.SONIC_ATTACK_LEVEL_2_V0 : -672;
                this.velX = ((this.faceDirection ^ this.isAntiGravity) != false ? 1 : -1) * -768;
                this.faceDirection = !this.faceDirection;
                this.animationID = 4;
                return;
            }
            int playingLoopSeIndex;
            if (!(this.myAnimationID == 31 || this.myAnimationID == 32)) {
                playingLoopSeIndex = soundInstance.getPlayingLoopSeIndex();
                soundSystem2 = soundInstance;
                if (playingLoopSeIndex == 18) {
                    soundInstance.stopLoopSe();
                }
            }
            int climbPointX = this.posX + (((this.faceDirection ^ this.isAntiGravity) != false ? 1 : -1) * 1024);
            int climbPointY = (this.posY + ((this.isAntiGravity ? 1 : -1) * (getCollisionRectHeight() >> 1))) + this.velY;
            if (climbPointY < (MapManager.getPixelHeight() << 6) - 1536 || !this.isAntiGravity) {
                if (this.worldInstance.getWorldX(climbPointX, climbPointY, this.currentLayer, (this.faceDirection ^ this.isAntiGravity) != false ? 3 : 1) == -1000) {
                    int footX = this.posX + (((this.faceDirection ^ this.isAntiGravity) != false ? 1 : -1) * 1024);
                    int newY = this.worldInstance.getWorldY(footX, this.posY, this.currentLayer, this.isAntiGravity ? 2 : 0);
                    if (newY != -1000) {
                        playingLoopSeIndex = footX - (((this.faceDirection ^ this.isAntiGravity) != false ? 1 : -1) * (this.worldInstance.getTileWidth() >> 1));
                        this.posX = playingLoopSeIndex;
                        this.footPointX = playingLoopSeIndex;
                        this.posY = newY;
                        this.footPointY = newY;
                        this.myAnimationID = 33;
                        soundInstance.stopLoopSe();
                        this.velX = 0;
                        this.velY = 0;
                        return;
                    }
                    this.collisionState = (byte) 1;
                    this.animationID = 1;
                    soundInstance.stopLoopSe();
                    return;
                }
                int newPosX = this.worldInstance.getWorldX(climbPointX, (this.isAntiGravity ? 1536 : -1536) + climbPointY, this.currentLayer, (this.faceDirection ^ this.isAntiGravity) != false ? 3 : 1) - (((this.faceDirection ^ this.isAntiGravity) != false ? 1 : -1) * 512);
                if (newPosX >= 0 && (((newPosX - this.posX > 0 && (this.faceDirection ^ this.isAntiGravity) == false) || (newPosX - this.posX < 0 && (this.faceDirection ^ this.isAntiGravity) != false)) && ((this.velY < 0 && !this.isAntiGravity) || (this.velY > 0 && this.isAntiGravity)))) {
                    this.velY = 0;
                }
                if (this.worldInstance.getWorldX(climbPointX, climbPointY - (this.isAntiGravity ? PlayerObject.SONIC_ATTACK_LEVEL_3_V0 : -1200), this.currentLayer, (this.faceDirection ^ this.isAntiGravity) != false ? 3 : 1) - (((this.faceDirection ^ this.isAntiGravity) != false ? 1 : -1) * 512) == this.posX) {
                    return;
                }
                if ((this.velY > 0 && !this.isAntiGravity) || (this.velY < 0 && this.isAntiGravity)) {
                    this.collisionState = (byte) 1;
                    this.animationID = 1;
                    soundInstance.stopLoopSe();
                    return;
                }
                return;
            }
            this.velY = 0;
        }
    }

    public void doWhileTouchWorld(int direction, int degree) {
        super.doWhileTouchWorld(direction, degree);
        if (!this.flying) {
            return;
        }
        if (degree != FLY_DOWN_SPEED && degree != 270) {
            return;
        }
        if ((direction == 1 && this.faceDirection) || (direction == 3 && !this.faceDirection)) {
            this.collisionState = (byte) 4;
            this.flying = false;
            this.animationID = -1;
            this.myAnimationID = 29;
            this.worldCal.stopMove();
            int i = this.posY + 256;
            this.footPointY = i;
            this.posX = i;
        }
    }

    public void doWhileLand(int degree) {
        super.doWhileLand(degree);
        if (this.flying && this.hurtCount == 0) {
            this.animationID = -1;
            this.myAnimationID = 22;
        }
    }

    public boolean needRetPower() {
        if ((this.attackLevel == 0 || this.attackLevel == 3) && this.myAnimationID != 22) {
            return super.needRetPower();
        }
        return true;
    }

    public int getRetPower() {
        if (this.attackLevel == 0 || this.attackLevel == 3) {
            return super.getRetPower();
        }
        return 288;
    }

    public boolean noRotateDraw() {
        if (this.myAnimationID == 11 || this.myAnimationID == 12 || this.myAnimationID == 13) {
            return true;
        }
        return super.noRotateDraw();
    }

    public int getSlopeGravity() {
        if (this.myAnimationID == 22) {
            return 0;
        }
        return super.getSlopeGravity();
    }

    public boolean canDoJump() {
        if (this.myAnimationID == 11 || this.myAnimationID == 12) {
            return false;
        }
        return super.canDoJump();
    }

    public void doHurt() {
        if (this.floating) {
            this.floating = false;
        }
        super.doHurt();
    }

    public void doJump() {
        this.attackLevel = 0;
        this.attackLevelNext = 0;
        this.attackCount = 0;
        super.doJump();
    }

    public void refreshCollisionRectWrap() {
        super.refreshCollisionRectWrap();
        if (this.animationID != -1) {
            return;
        }
        if (this.myAnimationID == 19 || this.myAnimationID == 20 || this.myAnimationID == 21 || this.myAnimationID == 28) {
            this.checkPositionX = getNewPointX(this.footPointX, 0, (-1024) >> 1, 0) + 0;
            this.checkPositionY = getNewPointY(this.footPointY, 0, this.isAntiGravity ? 1024 >> 1 : (-1024) >> 1, 0) + 0;
            int i = 1280 >> 1;
            i = 1024 >> 1;
            this.collisionRect.setTwoPosition(this.checkPositionX - (1280 >> 1), this.checkPositionY - (1024 >> 1), this.checkPositionX + MDPhone.SCREEN_HEIGHT, this.checkPositionY + 512);
        }
    }

    public int getCollisionRectWidth() {
        if (this.animationID == -1 && (this.myAnimationID == 19 || this.myAnimationID == 20 || this.myAnimationID == 21 || this.myAnimationID == 28)) {
            return 1280;
        }
        return super.getCollisionRectWidth();
    }

    public int getCollisionRectHeight() {
        if (this.animationID == -1 && (this.myAnimationID == 19 || this.myAnimationID == 20 || this.myAnimationID == 21 || this.myAnimationID == 28)) {
            return 1024;
        }
        if (this.collisionState != (byte) 4 || this.myAnimationID == 33) {
            return super.getCollisionRectHeight();
        }
        return 1280;
    }

    public void beStop(int newPosition, int direction, GameObject object) {
        super.beStop(newPosition, direction, object);
        if (this.isAntiGravity) {
            if (direction == 1) {
                direction = 0;
            } else if (direction == 0) {
                direction = 1;
            }
        }
        if (direction == 1 && this.flying && this.hurtCount == 0) {
            this.animationID = -1;
            this.myAnimationID = 22;
        }
    }

    public boolean beAccelerate(int power, boolean IsX, GameObject sender) {
        if (this.myAnimationID == 22) {
            return false;
        }
        return super.beAccelerate(power, IsX, sender);
    }

    public void setPreWaterFlag(boolean state) {
        this.preWaterFlag = state;
    }

    public void floatchk() {
        if (!(this.preWaterFlag || !this.isInWater || this.flying)) {
            this.floating = true;
        }
        this.preWaterFlag = this.isInWater;
    }

    public void setFloating(boolean f) {
        this.floating = f;
    }
}
