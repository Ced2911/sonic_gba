package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: GimmickObject */
class Spring extends GimmickObject {
    public static final int[][] COLLISION_PARAM;
    public static final int[] SPRING_INWATER_POWER = new int[]{(GRAVITY >> 1) + 2064, (GRAVITY >> 1) + 2256, (GRAVITY >> 1) + 2440, 2624};
    public static final int[] SPRING_POWER = new int[]{(GRAVITY >> 1) + 1968, (GRAVITY >> 1) + 2351, (GRAVITY >> 1) + 2744, 3234};
    public static final int[] SPRING_POWER_ORIGINAL = new int[]{1920, 2304, 2688, 3072};
    private static final int[] VELOCITY_MINUS = new int[]{20, 85, 20, 20};
    private static final int[] VELOCITY_MINUS_2 = new int[]{30, 100, 30, 30};
    public static Animation springAnimation = null;
    public int centerPointX;
    private AnimationDrawer drawer;
    private int firstCollisionDirection = 4;
    public boolean springAttacked;
    public int springPower;

    static {
    /*
        int[][] iArr = new int[8][];
        iArr[0] = new int[]{-15, -15, 30, 15};
        int[] iArr2 = new int[]{-15, 30, 19, iArr2};
        iArr[2] = new int[]{-16, -28, 16, 26};
        iArr2 = new int[]{-28, 16, 26, iArr2};
        iArr[4] = new int[]{-22, -29, 26, 26};
        iArr[5] = new int[]{-4, -29, 26, 26};
        iArr[6] = new int[]{-22, -21, 26, 13};
        iArr[7] = new int[]{-4, -21, 26, 13};
        COLLISION_PARAM = iArr;
    **/
        int[][] var0 = new int[8][];
        var0[0] = new int[]{-15, -15, 30, 15};
        var0[1] = new int[]{-15, 0, 30, 19};
        var0[2] = new int[]{-16, -28, 16, 26};
        var0[3] = new int[]{0, -28, 16, 26};
        var0[4] = new int[]{-22, -29, 26, 26};
        var0[5] = new int[]{-4, -29, 26, 26};
        var0[6] = new int[]{-22, -21, 26, 13};
        var0[7] = new int[]{-4, -21, 26, 13};
        COLLISION_PARAM = var0;
    }

    public Spring(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        if (springAnimation == null) {
            springAnimation = new Animation("/animation/se_bane_kiro");
        }
        if (this.iLeft < 0) {
            this.iLeft = 0;
        } else if (this.iLeft > 3) {
            this.iLeft = 3;
        }
        this.springPower = SPRING_POWER[this.iLeft];
        this.drawer = springAnimation.getDrawer((this.objId - 8) * 2, false, 0);
        if ((this.objId == 8 || this.objId == 9) && this.iLeft == 2) {
            this.drawer.setActionId(this.objId * 2);
        }
        this.centerPointX = (this.collisionRect.x0 + this.collisionRect.x1) >> 1;
    }

    public void draw(MFGraphics g) {
        this.springPower = player.isInWater ? SPRING_INWATER_POWER[this.iLeft] : SPRING_POWER[this.iLeft];
        drawInMap(g, this.drawer, this.posX, this.posY);
        if (this.drawer.checkEnd()) {
            if ((this.objId == 8 || this.objId == 9) && this.iLeft == 2) {
                this.drawer.setActionId(this.objId * 2);
            } else {
                this.drawer.setActionId((this.objId - 8) * 2);
            }
        }
        drawCollisionRect(g);
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (!this.used) {
            if (PlayerObject.getCharacterID() != 3 || !this.springAttacked) {
                if (this.firstCollisionDirection != 4 && direction == 4) {
                    direction = this.firstCollisionDirection;
                }
                if (this.firstCollisionDirection == 4 && direction != 4) {
                    this.firstCollisionDirection = direction;
                }
                object.beStop(this.collisionRect.x0, direction, this);
                if (object == player) {
                    PlayerObject playerObject;
                    boolean z;
                    switch (direction) {
                        case 0:
                            switch (this.objId) {
                                case 9:
                                    if ((object instanceof PlayerKnuckles) && player.getCharacterAnimationID() >= 29 && player.getCharacterAnimationID() <= 33) {
                                        player.collisionState = (byte) 1;
                                        player.restartAniDrawer();
                                    }
                                    player.beSpring(this.springPower, direction);
                                    if ((this.objId == 8 || this.objId == 9) && this.iLeft == 2) {
                                        this.drawer.setActionId((this.objId * 2) + 1);
                                    } else {
                                        this.drawer.setActionId(((this.objId - 8) * 2) + 1);
                                    }
                                    if (player.isAntiGravity && this.iLeft == 1) {
                                        player.setAnimationId(14);
                                    }
                                    soundInstance.playSe(37);
                                    return;
                                case 10:
                                    if (player.isAntiGravity && this.firstTouch) {
                                        player.setAnimationId(0);
                                        return;
                                    }
                                    return;
                                case 11:
                                    if (player.isAntiGravity && this.firstTouch) {
                                        player.setAnimationId(0);
                                        return;
                                    }
                                    return;
                                default:
                                    return;
                            }
                        case 1:
                            switch (this.objId) {
                                case 8:
                                    player.beSpring(this.springPower, direction);
                                    if ((this.objId == 8 || this.objId == 9) && this.iLeft == 2) {
                                        this.drawer.setActionId((this.objId * 2) + 1);
                                    } else {
                                        this.drawer.setActionId(((this.objId - 8) * 2) + 1);
                                    }
                                    if (this.iLeft == 1) {
                                        player.setAnimationId(14);
                                    }
                                    if (this.firstTouch) {
                                        soundInstance.playSe(37);
                                        return;
                                    }
                                    return;
                                case 10:
                                    if (this.firstTouch) {
                                        player.setAnimationId(0);
                                        return;
                                    }
                                    return;
                                case 11:
                                    if (this.firstTouch) {
                                        player.setAnimationId(0);
                                    }
                                    if (StageManager.getStageID() == 4 && player.degreeForDraw != 0) {
                                        player.faceDirection = true;
                                        player.beSpring(this.springPower, 2);
                                        this.drawer.setActionId(((this.objId - 8) * 2) + 1);
                                        soundInstance.playSe(37);
                                        if (object instanceof PlayerAmy) {
                                            player.setAnimationId(3);
                                            player.restartAniDrawer();
                                            return;
                                        }
                                        return;
                                    }
                                    return;
                                case 12:
                                case 14:
                                    if (player.getCheckPositionX() < this.collisionRect.x0 + ((this.collisionRect.x1 - this.collisionRect.x0) / 4)) {
                                        player.beSpring((this.springPower * VELOCITY_MINUS[this.iLeft]) / VELOCITY_MINUS_2[this.iLeft], 3);
                                        player.beSpring(this.springPower, direction);
                                        if ((this.objId == 8 || this.objId == 9) && this.iLeft == 2) {
                                            this.drawer.setActionId((this.objId * 2) + 1);
                                        } else {
                                            this.drawer.setActionId(((this.objId - 8) * 2) + 1);
                                        }
                                        if (this.iLeft == 1) {
                                            player.setAnimationId(14);
                                        }
                                        player.setFootPositionY(this.collisionRect.y0);
                                        soundInstance.playSe(37);
                                        return;
                                    } else if (this.firstTouch) {
                                        player.setAnimationId(0);
                                        return;
                                    } else {
                                        return;
                                    }
                                case 13:
                                case 15:
                                    if (player.getCheckPositionX() > this.collisionRect.x0 + (((this.collisionRect.x1 - this.collisionRect.x0) * 3) / 4)) {
                                        player.beSpring((this.springPower * VELOCITY_MINUS[this.iLeft]) / VELOCITY_MINUS_2[this.iLeft], 2);
                                        player.beSpring(this.springPower, direction);
                                        if ((this.objId == 8 || this.objId == 9) && this.iLeft == 2) {
                                            this.drawer.setActionId((this.objId * 2) + 1);
                                        } else {
                                            this.drawer.setActionId(((this.objId - 8) * 2) + 1);
                                        }
                                        if (this.iLeft == 1) {
                                            player.setAnimationId(14);
                                        }
                                        player.setFootPositionY(this.collisionRect.y0);
                                        soundInstance.playSe(37);
                                        return;
                                    } else if (this.firstTouch) {
                                        player.setAnimationId(0);
                                        return;
                                    } else {
                                        return;
                                    }
                                default:
                                    if ((object instanceof PlayerAmy) && ((player.getCharacterAnimationID() >= 5 && player.getCharacterAnimationID() <= 7) || ((player.getCharacterAnimationID() >= 20 && player.getCharacterAnimationID() <= 22) || (player.getCharacterAnimationID() >= 14 && player.getCharacterAnimationID() <= 17)))) {
                                        player.setAnimationId(0);
                                        player.restartAniDrawer();
                                    }
                                    if ((object instanceof PlayerKnuckles) && player.getCharacterAnimationID() >= 29 && player.getCharacterAnimationID() <= 33) {
                                        player.collisionState = (byte) 2;
                                        player.setAnimationId(0);
                                        player.restartAniDrawer();
                                        return;
                                    }
                                    return;
                            }
                        case 2:
                            switch (this.objId) {
                                case 11:
                                    playerObject = player;
                                    if (player.isAntiGravity) {
                                        z = false;
                                    } else {
                                        z = true;
                                    }
                                    playerObject.faceDirection = z;
                                    player.beSpring(this.springPower, direction);
                                    this.drawer.setActionId(((this.objId - 8) * 2) + 1);
                                    soundInstance.playSe(37);
                                    if (object instanceof PlayerAmy) {
                                        player.setAnimationId(3);
                                        player.restartAniDrawer();
                                        return;
                                    }
                                    return;
                                case 13:
                                case 15:
                                    if (player.getCheckPositionX() > this.centerPointX) {
                                        player.beSpring((this.springPower * VELOCITY_MINUS[this.iLeft]) / VELOCITY_MINUS_2[this.iLeft], direction);
                                        player.beSpring(this.springPower, 1);
                                        this.drawer.setActionId(((this.objId - 8) * 2) + 1);
                                        if (this.iLeft == 1) {
                                            player.setAnimationId(14);
                                        }
                                        player.setFootPositionY(this.collisionRect.y0);
                                        player.setFootPositionX(this.collisionRect.x1);
                                        soundInstance.playSe(37);
                                        return;
                                    }
                                    return;
                                default:
                                    return;
                            }
                        case 3:
                            switch (this.objId) {
                                case 10:
                                    playerObject = player;
                                    if (player.isAntiGravity) {
                                        z = true;
                                    } else {
                                        z = false;
                                    }
                                    playerObject.faceDirection = z;
                                    player.beSpring(this.springPower, direction);
                                    this.drawer.setActionId(((this.objId - 8) * 2) + 1);
                                    soundInstance.playSe(37);
                                    if (object instanceof PlayerAmy) {
                                        player.setAnimationId(3);
                                        player.restartAniDrawer();
                                        return;
                                    }
                                    return;
                                case 12:
                                case 14:
                                    if (player.getCheckPositionX() < this.centerPointX) {
                                        player.beSpring((this.springPower * VELOCITY_MINUS[this.iLeft]) / VELOCITY_MINUS_2[this.iLeft], direction);
                                        player.beSpring(this.springPower, 1);
                                        this.drawer.setActionId(((this.objId - 8) * 2) + 1);
                                        if (this.iLeft == 1) {
                                            player.setAnimationId(14);
                                        }
                                        player.setFootPositionY(this.collisionRect.y0);
                                        player.setFootPositionX(this.collisionRect.x0);
                                        soundInstance.playSe(37);
                                        return;
                                    }
                                    return;
                                default:
                                    return;
                            }
                        case 4:
                            if (object instanceof PlayerKnuckles) {
                                switch (this.objId) {
                                    case 8:
                                        player.beSpring(this.springPower, 1);
                                        if ((this.objId == 8 || this.objId == 9) && this.iLeft == 2) {
                                            this.drawer.setActionId((this.objId * 2) + 1);
                                        } else {
                                            this.drawer.setActionId(((this.objId - 8) * 2) + 1);
                                        }
                                        if (this.iLeft == 1) {
                                            player.setAnimationId(14);
                                        }
                                        soundInstance.playSe(37);
                                        break;
                                    case 9:
                                        player.beSpring(this.springPower, 0);
                                        if ((this.objId == 8 || this.objId == 9) && this.iLeft == 2) {
                                            this.drawer.setActionId((this.objId * 2) + 1);
                                        } else {
                                            this.drawer.setActionId(((this.objId - 8) * 2) + 1);
                                        }
                                        if (player.isAntiGravity && this.iLeft == 1) {
                                            player.setAnimationId(14);
                                        }
                                        soundInstance.playSe(37);
                                        break;
                                }
                            }
                            if (this.objId == 14 && this.firstTouch) {
                                player.beSpring((this.springPower * VELOCITY_MINUS[this.iLeft]) / VELOCITY_MINUS_2[this.iLeft], 3);
                                player.beSpring(this.springPower, 1);
                                this.drawer.setActionId(((this.objId - 8) * 2) + 1);
                                if (this.iLeft == 1) {
                                    player.setAnimationId(14);
                                }
                                player.setFootPositionY(this.collisionRect.y0);
                                player.setFootPositionX(this.collisionRect.x0);
                                soundInstance.playSe(37);
                                return;
                            }
                            return;
                        default:
                            return;
                    }
                }
            }
        }
    }

    public void doWhileBeAttack(PlayerObject object, int direction, int animationID) {
        if (PlayerObject.getCharacterID() == 3 && object.myAnimationID != 7 && object.myAnimationID != 8) {
            if (!(this.objId == 10 || this.objId == 11)) {
                this.springAttacked = true;
            }
            switch (this.objId) {
                case 8:
                    if (animationID != 6 && animationID != 7) {
                        player.beSpring((this.springPower * 13) / 10, 1);
                        if ((this.objId == 8 || this.objId == 9) && this.iLeft == 2) {
                            this.drawer.setActionId((this.objId * 2) + 1);
                        } else {
                            this.drawer.setActionId(((this.objId - 8) * 2) + 1);
                        }
                        if (this.iLeft == 1) {
                            player.setAnimationId(14);
                        }
                        soundInstance.playSe(37);
                        return;
                    }
                    return;
                case 9:
                    if (animationID != 6 && animationID != 7) {
                        player.beSpring((this.springPower * 13) / 10, 0);
                        if ((this.objId == 8 || this.objId == 9) && this.iLeft == 2) {
                            this.drawer.setActionId((this.objId * 2) + 1);
                        } else {
                            this.drawer.setActionId(((this.objId - 8) * 2) + 1);
                        }
                        if (player.isAntiGravity && this.iLeft == 1) {
                            player.setAnimationId(14);
                        }
                        soundInstance.playSe(37);
                        return;
                    }
                    return;
                case 12:
                case 14:
                    if (animationID != 6 && animationID != 7) {
                        player.beSpring((((this.springPower * 13) / 10) * VELOCITY_MINUS[this.iLeft]) / VELOCITY_MINUS_2[this.iLeft], 3);
                        player.beSpring((this.springPower * 13) / 10, 1);
                        if ((this.objId == 8 || this.objId == 9) && this.iLeft == 2) {
                            this.drawer.setActionId((this.objId * 2) + 1);
                        } else {
                            this.drawer.setActionId(((this.objId - 8) * 2) + 1);
                        }
                        if (this.iLeft == 1) {
                            player.setAnimationId(14);
                        }
                        soundInstance.playSe(37);
                        return;
                    }
                    return;
                case 13:
                case 15:
                    if (animationID != 6 && animationID != 7) {
                        player.beSpring((((this.springPower * 13) / 10) * VELOCITY_MINUS[this.iLeft]) / VELOCITY_MINUS_2[this.iLeft], 2);
                        player.beSpring((this.springPower * 13) / 10, 1);
                        if ((this.objId == 8 || this.objId == 9) && this.iLeft == 2) {
                            this.drawer.setActionId((this.objId * 2) + 1);
                        } else {
                            this.drawer.setActionId(((this.objId - 8) * 2) + 1);
                        }
                        if (this.iLeft == 1) {
                            player.setAnimationId(14);
                        }
                        soundInstance.playSe(37);
                        return;
                    }
                    return;
                default:
                    if (direction == 1) {
                        player.setAnimationId(0);
                        player.restartAniDrawer();
                        return;
                    }
                    return;
            }
        }
    }

    public void doWhileNoCollision() {
        this.firstCollisionDirection = 4;
        this.springAttacked = false;
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect((COLLISION_PARAM[this.objId - 8][0] << 6) + x, (COLLISION_PARAM[this.objId - 8][1] << 6) + y, COLLISION_PARAM[this.objId - 8][2] << 6, COLLISION_PARAM[this.objId - 8][3] << 6);
    }

    public void close() {
        this.drawer = null;
    }

    public static void releaseAllResource() {
        Animation.closeAnimation(springAnimation);
        springAnimation = null;
    }

    public int getPaintLayer() {
        return 0;
    }
}
