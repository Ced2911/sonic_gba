package SonicGBA;

import Lib.Animation;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: BulletObject */
class PenguinBullet extends BulletObject {
    private static final int COLLISION_HEIGHT = 1024;
    private static final int COLLISION_WIDTH = 1024;
    private boolean broken;
    private MapObject mapObj;

    protected PenguinBullet(int x, int y, int velX, int velY) {
        super(x, y, velX, velY, false);
        if (penguinbulletAnimation == null) {
            penguinbulletAnimation = new Animation("/animation/pen_bullet");
        }
        this.drawer = penguinbulletAnimation.getDrawer(0, true, 0);
        this.posX = x;
        this.posY = y;
        this.velX = velX;
        this.velY = velY;
        this.mapObj = new MapObject(this.posX, this.posY, 0, 0, this, 1);
        this.mapObj.setPosition(this.posX, this.posY, this.velX, 0, this);
        this.mapObj.setCrashCount(2);
        this.broken = false;
    }

    public void bulletLogic() {
        if (this.drawer.checkEnd() || IsHitted() || (this.mapObj.getVelX() == 0 && this.mapObj.getVelY() == 0)) {
            this.broken = true;
        } else if (this.mapObj.chkCrash()) {
            this.drawer.setActionId(1);
            this.drawer.setLoop(false);
        } else {
            this.mapObj.logic();
            checkWithPlayer(this.posX, this.posY, this.mapObj.getPosX(), this.mapObj.getPosY());
            this.posX = this.mapObj.getPosX();
            this.posY = this.mapObj.getPosY();
        }
    }

    public void draw(MFGraphics g) {
        if (!this.drawer.checkEnd()) {
            drawInMap(g, this.drawer);
        }
        drawCollisionRect(g);
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 512, y - 1024, 1024, 1024);
    }

    public boolean chkDestroy() {
        return this.broken || !isInCamera();
    }
}
