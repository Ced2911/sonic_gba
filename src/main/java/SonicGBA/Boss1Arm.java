package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import Lib.MyAPI;
import Lib.SoundSystem;
import com.sega.mobile.framework.android.Graphics;
import com.sega.mobile.framework.device.MFGraphics;
import java.lang.reflect.Array;

/* compiled from: EnemyObject */
class Boss1Arm extends EnemyObject {
    private static Animation ArmAni = null;
    private static final int COLLISION_HEIGHT = 2176;
    private static final int COLLISION_WIDTH = 2176;
    private static final int DEGREE_MAX = 23680;
    private static final int DEGREE_MAX_2 = 10880;
    private static final int DEGREE_MIN = 10880;
    private static final int DEGREE_MIN_2 = 23680;
    private static final int STATE_ATTACK_1 = 1;
    private static final int STATE_ATTACK_2 = 3;
    private static final int STATE_ATTACK_3 = 4;
    private static final int STATE_BROKEN = 5;
    private static final int STATE_READY = 2;
    private static final int STATE_WAIT = 0;
    private boolean IsBreaking;
    private boolean IsTurn;
    private int Step2CenterX;
    private int Step2CenterY;
    private AnimationDrawer armdrawer;
    private int ball_size = 1536;
    private AnimationDrawer boomdrawer;
    private int con_size = BarHorbinV.HOBIN_POWER;
    private int degree = 0;
    private int dg_plus = 320;
    private int drop_cnt;
    private AnimationDrawer hammerdrawer;
    private int offsetY = 2112;
    private int plus = 1;
    private int[][] pos;
    private int[][] prepos;
    private int state;
    private int[] velX;
    private int velY;

    public static void releaseAllResource() {
        Animation.closeAnimation(ArmAni);
        Animation.closeAnimation(BoomAni);
        ArmAni = null;
        BoomAni = null;
    }

    protected Boss1Arm(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        this.posX -= this.iLeft * 8;
        this.posY -= this.iTop * 8;
        this.degree = 10560;
        if (ArmAni == null) {
            ArmAni = new Animation("/animation/boss1_arm");
        }
        this.armdrawer = ArmAni.getDrawer(0, true, 0);
        if (BoomAni == null) {
            BoomAni = new Animation("/animation/boom");
        }
        this.boomdrawer = BoomAni.getDrawer(0, true, 0);
        this.hammerdrawer = new Animation("/animation/boss1_hammer").getDrawer(0, false, 0);
        this.pos = (int[][]) Array.newInstance(Integer.TYPE, new int[]{6, 2});
        this.prepos = (int[][]) Array.newInstance(Integer.TYPE, new int[]{6, 2});
        this.IsBreaking = false;
        this.velX = new int[6];
        this.velX[0] = -450;
        this.velX[1] = SmallAnimal.FLY_VELOCITY_Y;
        this.velX[2] = -150;
        this.velX[3] = 150;
        this.velX[4] = 300;
        this.velX[5] = 450;
        this.velY = -1200;
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (!this.dead && object == player && this.state != STATE_BROKEN) {
            player.beHurt();
        }
    }

    public void doWhileBeAttack(PlayerObject object, int direction, int animationID) {
    }

    public int getArmState() {
        return this.state;
    }

    public int getArmAngel() {
        return this.degree;
    }

    public void setArmAngel(int degree) {
        this.degree = degree;
    }

    public boolean getTurnState() {
        return this.IsTurn;
    }

    public void setTurnState(boolean turn_state) {
        this.IsTurn = turn_state;
    }

    public void setArmState(int arm_state) {
        this.state = arm_state;
    }

    public void setDegreeSpeed(int speed) {
        this.dg_plus = speed;
    }

    public void logic() {
        if (!this.dead) {
        }
    }

    public int[] logic(int posx, int posy, int boss_state, int boss_velocity) {
        if (this.dead) {
            return this.pos[0];
        }
        int preX = this.pos[5][0];
        int preY = this.pos[5][1];
        int i;
        switch (boss_state) {
            case 1:
                if (this.plus > 0) {
                    this.degree += this.plus * this.dg_plus;
                    if (this.degree >= 23680) {
                        this.degree = 23680;
                        this.plus = -this.plus;
                        MapManager.setShake(8);
                        SoundSystem.getInstance().playSe(36);
                    }
                } else {
                    this.degree += this.plus * this.dg_plus;
                    if (this.degree <= 10880) {
                        this.degree = 10880;
                        this.plus = -this.plus;
                        MapManager.setShake(8);
                        SoundSystem.getInstance().playSe(36);
                    }
                }
                for (i = 0; i < this.pos.length - 1; i++) {
                    this.pos[i][0] = posx - (((this.con_size * i) * MyAPI.dCos(this.degree >> 6)) / 100);
                    this.pos[i][1] = (((this.con_size * i) * MyAPI.dSin(this.degree >> 6)) / 100) + posy;
                }
                this.pos[5][0] = posx - ((((this.con_size * 4) + this.ball_size) * MyAPI.dCos(this.degree >> 6)) / 100);
                this.pos[5][1] = ((((this.con_size * 4) + this.ball_size) * MyAPI.dSin(this.degree >> 6)) / 100) + posy;
                checkWithPlayer(preX, preY, this.pos[5][0], this.pos[5][1]);
                break;
            case 3:
                this.dg_plus = 1137;
                if (boss_velocity > 0) {
                    if (this.plus > 0) {
                        if (this.degree <= 10880) {
                            this.degree = 10880;
                            this.plus = -this.plus;
                        } else {
                            this.degree += this.plus * this.dg_plus;
                        }
                    } else if (this.degree + (this.plus * this.dg_plus) <= 23680) {
                        this.degree = 23680;
                        this.state = 4;
                        this.IsTurn = false;
                    } else {
                        this.degree += this.plus * this.dg_plus;
                    }
                } else if (this.plus > 0) {
                    if (this.degree + (this.plus * this.dg_plus) >= 10880) {
                        this.degree = 10880;
                        this.state = 4;
                        this.IsTurn = false;
                    } else {
                        this.degree += this.plus * this.dg_plus;
                    }
                } else if (this.degree >= 23680) {
                    this.degree = 23680;
                    this.plus = -this.plus;
                } else {
                    this.degree += this.plus * this.dg_plus;
                }
                this.pos[5][0] = this.Step2CenterX;
                this.pos[5][1] = this.Step2CenterY;
                for (i = 1; i < this.pos.length; i++) {
                    this.pos[5 - i][0] = this.Step2CenterX + ((((this.con_size * (i - 1)) + this.ball_size) * MyAPI.dCos(this.degree >> 6)) / 100);
                    this.pos[5 - i][1] = this.Step2CenterY - ((((this.con_size * (i - 1)) + this.ball_size) * MyAPI.dSin(this.degree >> 6)) / 100);
                }
                checkWithPlayer(preX, preY, this.pos[5][0], this.pos[5][1]);
                break;
            case 4:
                this.dg_plus = 914;
                if (boss_velocity > 0) {
                    if (this.plus > 0) {
                        if (this.degree >= 23680) {
                            this.degree = 23680;
                            this.plus = -this.plus;
                        } else {
                            this.degree += this.plus * this.dg_plus;
                        }
                    } else if (this.degree + (this.plus * this.dg_plus) <= 10880) {
                        this.degree = 10880;
                        this.state = 3;
                        this.IsTurn = false;
                    } else {
                        this.degree += this.plus * this.dg_plus;
                    }
                } else if (this.plus > 0) {
                    if (this.degree + (this.plus * this.dg_plus) >= 23680) {
                        this.degree = 23680;
                        this.state = 3;
                        this.IsTurn = false;
                    } else {
                        this.degree += this.plus * this.dg_plus;
                    }
                } else if (this.degree <= 10880) {
                    this.degree = 10880;
                    this.plus = -this.plus;
                } else {
                    this.degree += this.plus * this.dg_plus;
                }
                for (i = 0; i < this.pos.length - 1; i++) {
                    this.pos[i][0] = posx - (((this.con_size * i) * MyAPI.dCos(this.degree >> 6)) / 100);
                    this.pos[i][1] = (((this.con_size * i) * MyAPI.dSin(this.degree >> 6)) / 100) + posy;
                }
                this.pos[5][0] = posx - ((((this.con_size * 4) + this.ball_size) * MyAPI.dCos(this.degree >> 6)) / 100);
                this.pos[5][1] = ((((this.con_size * 4) + this.ball_size) * MyAPI.dSin(this.degree >> 6)) / 100) + posy;
                if (this.state == 3) {
                    this.Step2CenterX = this.pos[5][0];
                    this.Step2CenterY = this.pos[5][1];
                    if (this.degree == 23680) {
                        this.degree -= 23040;
                    }
                    if (this.degree == 10880) {
                        this.degree += 23040;
                    }
                    MapManager.setShake(8);
                    SoundSystem.getInstance().playSe(36);
                }
                checkWithPlayer(preX, preY, this.pos[5][0], this.pos[5][1]);
                break;
            case 5:
                this.state = 5;
                if (!this.IsBreaking) {
                    if (this.pos[5][0] < this.pos[0][0]) {
                        for (i = 0; i < this.velX.length; i++) {
                            this.velX[i] = -this.velX[i];
                        }
                    }
                    this.IsBreaking = true;
                }
                for (i = 0; i < this.velX.length; i++) {
                    int tmpcol = this.con_size >> 1;
                    if (i == 5) {
                        tmpcol = this.offsetY >> 1;
                    } else {
                        tmpcol = this.con_size >> 1;
                    }
                    if (this.pos[i][1] + tmpcol >= getGroundY(this.pos[i][0], this.pos[i][1])) {
                        this.pos[i][1] = getGroundY(this.pos[i][0], this.pos[i][1]) - tmpcol;
                        switch (this.drop_cnt) {
                            case 0:
                                this.velY = PlayerObject.MIN_ATTACK_JUMP;
                                this.drop_cnt = 1;
                                break;
                            case 1:
                                this.velY = -600;
                                this.drop_cnt = 2;
                                break;
                            default:
                                break;
                        }
                    }
                    this.prepos[i][0] = this.pos[i][0];
                    this.prepos[i][1] = this.pos[i][1];
                    int[] iArr = this.pos[i];
                    iArr[0] = iArr[0] + this.velX[i];
                    this.velY += GRAVITY >> 3;
                    iArr = this.pos[i];
                    iArr[1] = iArr[1] + this.velY;
                }
                checkWithPlayer(preX, preY, this.pos[5][0], this.pos[5][1]);
                break;
        }
        if (boss_state == 3) {
            return this.pos[0];
        }
        return this.pos[5];
    }

    public void draw(MFGraphics g) {
        if (!this.dead) {
        }
    }

    public void drawArm(MFGraphics g) {
        if (!this.dead) {
            for (int i = 0; i < this.pos.length; i++) {
                if (this.state == 5 && this.drop_cnt < 2) {
                    drawInMap(g, this.boomdrawer, this.prepos[i][0], this.prepos[i][1]);
                }
                if (i < this.pos.length - 1 && this.drop_cnt < 2) {
                    drawInMap(g, this.armdrawer, this.pos[i][0], this.pos[i][1]);
                }
            }
            if (this.drop_cnt < 2) {
                Graphics g2 = (Graphics) g.getSystemGraphics();
                g2.save();
                g2.translate((float) ((this.pos[5][0] >> 6) - camera.f13x), (float) ((this.pos[5][1] >> 6) - camera.f14y));
                g2.rotate((float) ((-this.degree) >> 6));
                this.hammerdrawer.draw(g, 0, 0);
                g2.restore();
            }
            drawCollisionRect(g);
        }
    }

    public void refreshCollisionRect(int x, int y) {
        if (this.pos == null) {
            this.pos = (int[][]) Array.newInstance(Integer.TYPE, new int[]{6, 2});
        }
        if (this.state != 5) {
            this.collisionRect.setRect(this.pos[5][0] - 1088, this.pos[5][1] - 1088, Boss6Block.COLLISION_WIDTH, Boss6Block.COLLISION_WIDTH);
        }
    }

    public void close() {
        this.armdrawer = null;
        this.boomdrawer = null;
        this.pos = null;
        this.prepos = null;
        this.velX = null;
    }
}
