package SonicGBA;

import Lib.Animation;
import com.sega.mobile.define.MDPhone;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: EnemyObject */
class Chameleon extends EnemyObject {
    private static final int STATE_ATTACK = 2;
    private static final int STATE_MOVE = 0;
    private static final int STATE_TURN = 1;
    private static final int attack_cnt_max = 10;
    private static Animation chameleonAnimation;
    private int ALERT_RANGE = 5760;
    private int COLLISION_HEIGHT = 1408;
    private int COLLISION_WIDTH = 2880;
    private int alert_state;
    private int attack_cnt;
    private int collision_offset_x = 1280;
    private int limitLeftX = this.posX;
    private int limitRightX = (this.posX + this.mWidth);
    private int state;
    private int velocity = 128;

    public static void releaseAllResource() {
        Animation.closeAnimation(chameleonAnimation);
        chameleonAnimation = null;
    }

    protected Chameleon(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        if (chameleonAnimation == null) {
            chameleonAnimation = new Animation("/animation/chameleon");
        }
        this.drawer = chameleonAnimation.getDrawer(0, true, 0);
        this.attack_cnt = 0;
        this.collision_offset_x = 0;
    }

    private boolean IsFacePlayer() {
        if ((this.posX <= player.getFootPositionX() || this.velocity >= 0) && (this.posX >= player.getFootPositionX() || this.velocity <= 0)) {
            return false;
        }
        return true;
    }

    public void logic() {
        if (!this.dead) {
            this.alert_state = checkPlayerInEnemyAlertRange(this.posX >> 6, this.posY >> 6, this.ALERT_RANGE >> 6, this.limitLeftX >> 6, this.limitRightX >> 6, this.COLLISION_WIDTH >> 6);
            int preX = this.posX;
            int preY = this.posY;
            switch (this.state) {
                case 0:
                    if (this.velocity > 0) {
                        this.posX += this.velocity;
                        this.drawer.setActionId(0);
                        this.drawer.setTrans(2);
                        this.drawer.setLoop(true);
                        if (this.posX >= this.limitRightX) {
                            this.posX = this.limitRightX;
                            this.velocity = -this.velocity;
                            this.drawer.setActionId(1);
                            this.drawer.setTrans(2);
                            this.drawer.setLoop(false);
                            this.state = 1;
                        }
                    } else {
                        this.posX += this.velocity;
                        this.drawer.setActionId(0);
                        this.drawer.setTrans(0);
                        this.drawer.setLoop(true);
                        if (this.posX <= this.limitLeftX) {
                            this.posX = this.limitLeftX;
                            this.velocity = -this.velocity;
                            this.drawer.setActionId(1);
                            this.drawer.setTrans(0);
                            this.drawer.setLoop(false);
                            this.state = 1;
                        }
                    }
                    if (this.attack_cnt < 10) {
                        this.attack_cnt++;
                    }
                    if (this.alert_state == 0 && this.attack_cnt == 10 && IsFacePlayer()) {
                        this.state = 2;
                        if (this.posX < player.getCheckPositionX()) {
                            this.drawer.setActionId(2);
                            this.drawer.setTrans(2);
                            this.drawer.setLoop(false);
                        } else {
                            this.drawer.setActionId(2);
                            this.drawer.setTrans(0);
                            this.drawer.setLoop(false);
                        }
                        this.attack_cnt = 0;
                    }
                    this.posY = getGroundY(this.posX, this.posY);
                    checkWithPlayer(preX, preY, this.posX, this.posY);
                    return;
                case 1:
                    if (this.drawer.checkEnd()) {
                        this.state = 0;
                    }
                    this.attack_cnt = 0;
                    this.posY = getGroundY(this.posX, this.posY);
                    checkWithPlayer(preX, preY, this.posX, this.posY);
                    return;
                case 2:
                    if (this.drawer.getCurrentFrame() == 3) {
                        if (this.posX < player.getCheckPositionX()) {
                            this.collision_offset_x = MDPhone.SCREEN_HEIGHT;
                        } else {
                            this.collision_offset_x = -640;
                        }
                        this.COLLISION_WIDTH = 3840;
                        refreshCollisionRect(this.posX, this.posY);
                    } else {
                        this.COLLISION_WIDTH = 2880;
                        this.collision_offset_x = 0;
                        refreshCollisionRect(this.posX, this.posY);
                    }
                    if (this.drawer.checkEnd()) {
                        this.state = 0;
                        this.collision_offset_x = 0;
                        this.COLLISION_WIDTH = 2880;
                    }
                    this.posY = getGroundY(this.posX, this.posY);
                    refreshCollisionRect(this.posX, this.posY);
                    checkWithPlayer(preX, preY, this.posX, this.posY);
                    return;
                default:
                    return;
            }
        }
    }

    public void draw(MFGraphics g) {
        if (!this.dead) {
            drawInMap(g, this.drawer);
            drawCollisionRect(g);
        }
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect((x - (this.COLLISION_WIDTH >> 1)) + this.collision_offset_x, y - this.COLLISION_HEIGHT, this.COLLISION_WIDTH, this.COLLISION_HEIGHT);
    }
}
