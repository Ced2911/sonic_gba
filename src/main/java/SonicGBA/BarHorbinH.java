package SonicGBA;

import Lib.MyAPI;
import State.StringIndex;
import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;

/* compiled from: GimmickObject */
public class BarHorbinH extends BarHorbinV {
    private int functionDirection;

    protected BarHorbinH(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        if (barImage == null) {
            try {
                barImage = MFImage.createImage("/gimmick/bar.png");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        this.functionDirection = 1;
        if (this.iLeft == 0) {
            this.functionDirection = 0;
        }
    }

    public void draw(MFGraphics g) {
        if (this.iLeft == 0) {
            drawInMap(g, barImage, 0, 0, MyAPI.zoomIn(barImage.getWidth()), MyAPI.zoomIn(barImage.getHeight()), 3, this.posX + this.hobinCal.getPosOffsetX(), this.posY + this.hobinCal.getPosOffsetY(), 3);
        } else {
            drawInMap(g, barImage, 0, 0, MyAPI.zoomIn(barImage.getWidth()), MyAPI.zoomIn(barImage.getHeight()), 0, this.posX + this.hobinCal.getPosOffsetX(), this.posY + this.hobinCal.getPosOffsetY(), 3);
        }
        this.hobinCal.logic();
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 1280, (y + 512) - BarHorbinV.COLLISION_HEIGHT, BarHorbinV.COLLISION_WIDTH, BarHorbinV.COLLISION_HEIGHT);
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (object == player) {
            player.beStop(0, direction, this);
            soundInstance.playSe(24);
            switch (direction) {
                case 0:
                case 1:
                    if (this.iLeft != 0 && direction == 0) {
                        return;
                    }
                    if (this.iLeft != 0 || direction != 1) {
                        if (player.getCheckPositionX() <= this.collisionRect.getCenterX()) {
                            player.bePop(BarHorbinV.HOBIN_POWER, 3);
                            if (this.functionDirection == 1) {
                                this.hobinCal.startHobin(0, 45, 10);
                            } else {
                                this.hobinCal.startHobin(0, -45, 10);
                            }
                        } else {
                            player.bePop(BarHorbinV.HOBIN_POWER, 2);
                            if (this.functionDirection == 1) {
                                this.hobinCal.startHobin(0, StringIndex.FONT_COLON_RED, 10);
                            } else {
                                this.hobinCal.startHobin(0, 225, 10);
                            }
                        }
                        player.bePop(BarHorbinV.HOBIN_POWER, direction);
                        return;
                    }
                    return;
                case 2:
                    player.bePop(BarHorbinV.HOBIN_POWER, direction);
                    player.bePop(BarHorbinV.HOBIN_POWER, this.functionDirection);
                    if (this.functionDirection == 1) {
                        this.hobinCal.startHobin(0, StringIndex.FONT_COLON_RED, 10);
                        return;
                    } else {
                        this.hobinCal.startHobin(0, 225, 10);
                        return;
                    }
                case 3:
                    player.bePop(BarHorbinV.HOBIN_POWER, direction);
                    player.bePop(BarHorbinV.HOBIN_POWER, this.functionDirection);
                    if (this.functionDirection == 1) {
                        this.hobinCal.startHobin(0, 45, 10);
                        return;
                    } else {
                        this.hobinCal.startHobin(0, -45, 10);
                        return;
                    }
                default:
                    return;
            }
        }
    }

    public static void releaseAllResource() {
        barImage = null;
    }
}
