package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import Lib.SoundSystem;
import MFLib.MainState;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: GimmickObject */
class Door extends GimmickObject {
    private static final int COLLISION_HEIGHT = 2048;
    private static final int COLLISION_H_OFFSET = 512;
    private static final int COLLISION_V_OFFSET = 512;
    private static final int COLLISION_WIDTH = 1024;
    private int actionId;
    private AnimationDrawer drawer;
    private boolean isActived;

    protected Door(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        if (doorAnimation == null) {
            doorAnimation = new Animation("/animation/door");
        }
        switch (this.objId) {
            case MainState.FRAME_SKIP /*63*/:
                this.actionId = 0;
                break;
            case 64:
                this.actionId = 2;
                break;
        }
        this.drawer = doorAnimation.getDrawer(this.actionId, false, 0);
        this.isActived = false;
    }

    public void draw(MFGraphics g) {
        drawInMap(g, this.drawer);
        if (this.drawer.checkEnd() && this.isActived) {
            this.drawer.setActionId(this.actionId);
            this.isActived = false;
        }
        drawCollisionRect(g);
    }

    public void doWhileNoCollision() {
        if (!isInCameraSmaller() && this.isActived) {
            this.isActived = false;
        }
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        switch (this.objId) {
            case MainState.FRAME_SKIP /*63*/:
                switch (direction) {
                    case 2:
                        if (this.iLeft != 0) {
                            this.drawer.setActionId(this.actionId + 1);
                            this.isActived = true;
                            SoundSystem.getInstance().playSe(56);
                            if (object.getVelX() <= 0 && object.getVelX() >= (-PlayerObject.MOVE_POWER)) {
                                object.outOfControl = true;
                                object.setFootPositionX(object.posX + 1024);
                                object.outOfControl = false;
                                break;
                            }
                        }
                        object.beStop(this.collisionRect.y0, direction, this);
                        break;
                    case 3:
                        if (this.iLeft != 0) {
                            object.beStop(this.collisionRect.y0, direction, this);
                            break;
                        }
                        this.drawer.setActionId(this.actionId + 1);
                        this.isActived = true;
                        SoundSystem.getInstance().playSe(56);
                        if (object.getVelX() >= 0 && object.getVelX() <= PlayerObject.MOVE_POWER) {
                            object.outOfControl = true;
                            object.setFootPositionX(object.posX - 1024);
                            object.outOfControl = false;
                            break;
                        }
                    default:
                        break;
                }
            case 64:
                switch (direction) {
                    case 0:
                        if (this.iLeft != 0) {
                            this.drawer.setActionId(this.actionId + 1);
                            this.isActived = true;
                            SoundSystem.getInstance().playSe(56);
                            break;
                        }
                        object.beStop(this.collisionRect.y0, direction, this);
                        break;
                    case 1:
                        if (this.iLeft != 0) {
                            object.beStopbyDoor(this.collisionRect.y0, direction, this);
                            break;
                        }
                        this.drawer.setActionId(this.actionId + 1);
                        this.isActived = true;
                        SoundSystem.getInstance().playSe(56);
                        break;
                    default:
                        break;
                }
        }
        if (this.firstTouch) {
            player.pipeOut();
        }
    }

    public void refreshCollisionRect(int x, int y) {
        switch (this.objId) {
            case MainState.FRAME_SKIP /*63*/:
                this.collisionRect.setRect(x - 512, y, 1024, 2048);
                return;
            case 64:
                this.collisionRect.setRect(x, y - 512, 2048, 1024);
                return;
            default:
                return;
        }
    }

    public void close() {
        this.drawer = null;
    }

    public static void releaseAllResource() {
    }
}
