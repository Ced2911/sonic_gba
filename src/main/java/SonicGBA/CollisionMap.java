package SonicGBA;

import com.sega.engine.action.ACBlock;
import com.sega.engine.action.ACDegreeGetter;
import com.sega.engine.action.ACUtilities;
import com.sega.engine.action.ACWorld;
import com.sega.mobile.framework.device.MFDevice;
import com.sega.mobile.framework.device.MFGamePad;
import java.io.DataInputStream;
import java.io.IOException;
import java.lang.reflect.Array;

public class CollisionMap extends ACWorld implements SonicDef {
    private static final byte[] BLANK_BLOCK = new byte[8];
    public static final String COLLISION_FILE_NAME = ".co";
    private static final byte[] FULL_BLOCK = new byte[8];
    private static final int GRID_NUM_PER_MODEL = 12;
    private static final int LOAD_COLLISION_INFO = 2;
    private static final int LOAD_MODEL_INFO = 1;
    private static final int LOAD_OPEN_FILE = 0;
    private static final int LOAD_OVER = 3;
    public static final String MODEL_FILE_NAME = ".ci";
    private static CollisionMap instance;
    private static int loadStep = 0;
    private byte[][] collisionInfo;
    private MyDegreeGetter degreeGetter;
    private byte[] directionInfo;
    private DataInputStream ds;
    private short[][][] modelInfo;

    public static CollisionMap getInstance() {
        if (instance == null) {
            instance = new CollisionMap();
        }
        return instance;
    }

    static {
        for (int i = 0; i < FULL_BLOCK.length; i++) {
            FULL_BLOCK[i] = (byte) -120;
        }
    }

    public boolean loadCollisionInfoStep(String stageName) {
        int i;
        switch (loadStep) {
            case LOAD_OPEN_FILE:
                this.ds = new DataInputStream(MFDevice.getResourceAsStream("/map/" + stageName + MODEL_FILE_NAME));
                break;
            case LOAD_MODEL_INFO:
                this.modelInfo = (short[][][]) Array.newInstance(Short.TYPE, new int[]{MapManager.mapModel.length, GRID_NUM_PER_MODEL, GRID_NUM_PER_MODEL});
                i = 0;
                while (i < this.modelInfo.length) {
                    try {
                        for (int y = 0; y < GRID_NUM_PER_MODEL; y++) {
                            for (int x = 0; x < GRID_NUM_PER_MODEL; x++) {
                                this.modelInfo[i][x][y] = (short) (this.ds.readShort() & 0xFFFF);
                            }
                        }
                        i++;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (this.ds != null) {
                    try {
                        this.ds.close();
                    } catch (IOException e22) {
                        e22.printStackTrace();
                    }
                    break;
                }
                break;
            case LOAD_COLLISION_INFO:
                this.ds = new DataInputStream(MFDevice.getResourceAsStream("/map/" + stageName + COLLISION_FILE_NAME));
                try {
                    int collisionKindNum = this.ds.readShort() & 0xFFFF;
                    this.collisionInfo = (byte[][]) Array.newInstance(Byte.TYPE, new int[]{collisionKindNum, 8});
                    this.directionInfo = new byte[collisionKindNum];
                    for (i = 0; i < collisionKindNum; i++) {
                        for (int n = 0; n < 8; n++) {
                            this.collisionInfo[i][n] = this.ds.readByte();
                        }
                        this.directionInfo[i] = this.ds.readByte();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (this.ds != null) {
                    try {
                        this.ds.close();
                    } catch (IOException e32) {
                        e32.printStackTrace();
                    }
                }
                break;
            case LOAD_OVER:
                loadStep = 0;
                return true;
        }
        loadStep++;
        return false;
    }

    public void getCollisionInfoWithBlock(int blockX, int blockY, int currentLayer, ACBlock block) {
        getCollisionBlock(block, getTileWidth() * blockX, getTileHeight() * blockY, currentLayer);
    }

    private int getBlockIndexWithBlock(int blockX, int blockY, int currentLayer) {
        if (currentLayer == 1) {
            return getTileId(MapManager.mapBack, blockX, blockY);
        }
        return getTileId(MapManager.mapFront, blockX, blockY);
    }

    private int getTileId(short[][] mapArray, int x, int y) {
        return this.modelInfo[mapArray[x / GRID_NUM_PER_MODEL][y / GRID_NUM_PER_MODEL]][x % GRID_NUM_PER_MODEL][y % GRID_NUM_PER_MODEL];
    }

    public void closeMap() {
        this.collisionInfo = null;
        this.directionInfo = null;
        this.modelInfo = null;
    }

    private CollisionMap() {
    }

    public void getCollisionBlock(ACBlock block, int x, int y, int layer) {
        if (block instanceof CollisionBlock) {
            int blockX = ACUtilities.getQuaParam(x - (MapManager.mapOffsetX << 6), getTileWidth());
            int blockY = ACUtilities.getQuaParam(y, getTileHeight());
            CollisionBlock myBlock = (CollisionBlock) block;
            myBlock.setPosition((getTileWidth() * blockX) + (MapManager.mapOffsetX << 6), getTileHeight() * blockY);
            if (blockX < 0) {
                myBlock.setProperty(BLANK_BLOCK, false, false, 64, false);
            } else if (blockY < 0 || blockY >= MapManager.mapHeight * 12) {
                myBlock.setProperty(BLANK_BLOCK, false, false, 0, false);
            } else {
                int tileId = getBlockIndexWithBlock((MapManager.getConvertX(blockX / GRID_NUM_PER_MODEL) * GRID_NUM_PER_MODEL) + (blockX % GRID_NUM_PER_MODEL), blockY, layer);
                int cell_id = tileId & 8191;
                myBlock.setProperty(this.collisionInfo[cell_id], (tileId & MFGamePad.KEY_NUM_8) != 0, (32768 & tileId) != 0, this.directionInfo[cell_id], (tileId & 8192) != 0);
            }
        }
    }

    public ACDegreeGetter getDegreeGetterForObject() {
        if (this.degreeGetter == null) {
            this.degreeGetter = new MyDegreeGetter(this);
        }
        return this.degreeGetter;
    }

    public ACBlock getNewCollisionBlock() {
        return new CollisionBlock(this);
    }

    public int getTileHeight() {
        return 512;
    }

    public int getTileWidth() {
        return 512;
    }

    public int getWorldHeight() {
        return MapManager.getPixelHeight() << 6;
    }

    public int getWorldWidth() {
        return MapManager.getPixelWidth() << 6;
    }

    public int getZoom() {
        return 6;
    }
}
