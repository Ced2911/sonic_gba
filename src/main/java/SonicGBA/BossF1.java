package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import Lib.MyAPI;
import Lib.SoundSystem;
import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.utility.MFMath;
import java.lang.reflect.Array;

/* compiled from: EnemyObject */
class BossF1 extends BossObject {
    private static final int Accy1 = 96;
    private static final int BALL_ACC_ACC = 22;
    private static final int BALL_ACC_ACC2 = 17;
    private static final int BALL_ACC_MAX = GRAVITY;
    private static final int BALL_BOTTOM_Y = 48064;
    private static final int BALL_RANGE = 2048;
    private static final int BALL_RASIUS = 4992;
    private static final int BALL_START_Y = 42240;
    private static final int BOSS_1_STOP_POSY = 39936;
    private static final int BOSS_2_STOP_POSY = 43264;
    private static final int BOSS_3_STOP_POSX = 55296;
    private static final int BOSS_3_STOP_POSY = 43008;
    private static final int BOSS_LEFT_POSX = 53760;
    private static final int BOSS_MOVE_1_POSX = 54528;
    private static final int BOSS_MOVE_1_POSY = 42880;
    private static final int BOSS_MOVE_2_POSY = 43136;
    private static final int BOSS_RIGHT_POSX = 56832;
    private static final int BOSS_START_POSX = 59392;
    private static final int BOSS_START_POSY = 33920;
    private static final int BROKEN_OFFSET_Y = 1280;
    private static final int BROKEN_STOP_POSY = 46336;
    private static final int COLLISION_HEIGHT = 2560;
    private static final int COLLISION_HEIGHT_OFFSET = -384;
    private static final int COLLISION_WIDTH = 3008;
    private static final int END_POSX = 117120;
    private static final int ENTER_SCREEN_FRAME_MAX = 18;
    private static final int ENTER_SPEED_1_Y = 270;
    private static final int ENTER_SPEED_3_X = -240;
    private static final int ESCAPE_OUT_POSX = 65536;
    private static final int ESCAPE_SPEED_X = 480;
    private static final int ESCAPE_STOP_POSY = 45056;
    private static final int FACE_BROKEN = 3;
    private static final int FACE_HURT = 2;
    private static final int FACE_NORMAL = 0;
    private static final int FACE_OFFSET_X = 192;
    private static final int FACE_OFFSET_Y = -1920;
    private static final int FACE_SMILE = 1;
    private static final int INIT_STOP_POSX = 55296;
    private static final int INIT_STOP_POSY = 43008;
    private static final int LAUGH_TIME = 10;
    private static final int MACHINE_ESCAPE_MOVE = 3;
    private static final int MACHINE_ESCAPE_WAIT = 2;
    private static final int MACHINE_MOVE = 1;
    private static final int MACHINE_MOVE_HURT = 5;
    private static final int MACHINE_WAIT = 0;
    private static final int MACHINE_WAIT_HURT = 4;
    private static final int PRO_STEP_BOTTOM_2_LEFT = 0;
    private static final int PRO_STEP_BOTTOM_2_RIGHT = 2;
    private static final int PRO_STEP_LEFT_2_BOTTOM = 1;
    private static final int PRO_STEP_RIGHT_2_BOTTOM = 3;
    private static final int RING_RANGE = 896;
    private static final int SHOW_BOSS_END = 4;
    private static final int SHOW_BOSS_ENTER_1 = 0;
    private static final int SHOW_BOSS_ENTER_2 = 1;
    private static final int SHOW_BOSS_ENTER_3 = 2;
    private static final int SHOW_BOSS_GOTO_PRO = 5;
    private static final int SHOW_BOSS_LAUGH = 3;
    private static final int SIDE_DOWN1 = 784;
    private static final int SIDE_DOWN2 = 784;
    private static final int SIDE_LEFT1 = 864;
    private static final int SIDE_RIGHT1 = 864;
    private static final int SIDE_UP0 = 544;
    private static final int SIDE_UP1 = 624;
    private static final int SIDE_UP2 = 544;
    private static final int STATE_BROKEN = 3;
    private static final int STATE_ENTER_SHOW = 1;
    private static final int STATE_ESCAPE = 4;
    private static final int STATE_INIT = 0;
    private static final int STATE_PRO = 2;
    private static Animation ballAni = null;
    private static final int cnt_max = 8;
    private static Animation faceAni;
    private static Animation machineAni;
    private int Accy = 0;
    private int RADIUS = BALL_RASIUS;
    private BossF1Ball ball;
    private AnimationDrawer[] ballDrawer;
    private int[][] ballPos;
    private int[] ballVel;
    private int ballvely;
    private BossBroken bossbroken;
    private int degree;
    private boolean directTrans = false;
    private boolean displayFlag;
    private int drop_cnt;
    private int drop_vely;
    private int enter_screen_frame_cn;
    private AnimationDrawer faceDrawer;
    private int face_cnt;
    private int face_state;
    private int frameCn;
    private boolean isDisplayBall = false;
    private int laugh_cn;
    private int lineVelocity;
    private AnimationDrawer machineDrawer;
    private int machine_state;
    private int oppoBallPosX;
    private int oppoBallPosY;
    private int pro_step;
    private int show_step;
    private int state;
    private int velocity;
    private int vely;

    protected BossF1(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        int i;
        this.posX -= this.iLeft * 8;
        this.posY -= this.iTop * 8;
        this.posX = BOSS_START_POSX;
        this.posY = BOSS_START_POSY;
        if (machineAni == null) {
            machineAni = new Animation("/animation/bossf1_machine");
        }
        this.machineDrawer = machineAni.getDrawer(0, true, 0);
        if (faceAni == null) {
            faceAni = new Animation("/animation/bossf2_face");
        }
        this.faceDrawer = faceAni.getDrawer(0, true, 0);
        if (ballAni == null) {
            ballAni = new Animation("/animation/bossf1_ring_ball");
        }
        this.ballDrawer = new AnimationDrawer[6];
        for (i = 0; i < 4; i++) {
            this.ballDrawer[i] = ballAni.getDrawer(0, true, 0);
        }
        this.ballDrawer[4] = ballAni.getDrawer(1, true, 0);
        this.ballDrawer[5] = ballAni.getDrawer(2, true, 0);
        this.ballPos = (int[][]) Array.newInstance(Integer.TYPE, new int[]{6, 2});
        for (i = 0; i < 6; i++) {
            this.ballPos[i][0] = 55296;
            this.ballPos[i][1] = BALL_START_Y;
        }
        this.ballVel = new int[6];
        this.isDisplayBall = false;
        this.directTrans = false;
        this.ball = new BossF1Ball(id, x, y, 0, 0, 0, 0);
        GameObject.addGameObject(this.ball, this.posX >> 6, this.posY >> 6);
        this.displayFlag = false;
        this.state = 0;
        this.HP = 4;
    }

    public static void releaseAllResource() {
        Animation.closeAnimation(machineAni);
        Animation.closeAnimation(faceAni);
        Animation.closeAnimation(ballAni);
        machineAni = null;
        faceAni = null;
        ballAni = null;
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (!this.dead && this.state != 3 && this.state != 4) {
            if ((this.state == 1 && this.show_step < 4) || object != player) {
                return;
            }
            if (player.isAttackingEnemy()) {
                if (this.face_state != 2) {
                    this.HP--;
                    player.doBossAttackPose(this, direction);
                    if (this.HP > 0) {
                        if (this.machine_state == 0) {
                            this.machine_state = 4;
                        }
                        if (this.machine_state == 1) {
                            this.machine_state = 5;
                        }
                        this.face_state = 2;
                        this.face_cnt = 0;
                    } else {
                        this.state = 3;
                        this.face_state = 3;
                        this.machine_state = 0;
                        this.bossbroken = new BossBroken(28, this.posX >> 6, this.posY >> 6, 0, 0, 0, 0);
                        GameObject.addGameObject(this.bossbroken, this.posX >> 6, this.posY >> 6);
                        this.directTrans = true;
                        this.drop_cnt = 0;
                    }
                    if (this.HP == 0) {
                        SoundSystem.getInstance().playSe(35);
                    } else {
                        SoundSystem.getInstance().playSe(34);
                    }
                }
            } else if (this.state != 3 && this.state != 4 && this.machine_state != 4 && this.machine_state != 5 && this.face_state != 2) {
                player.beHurt();
                this.face_state = 1;
            }
        }
    }

    public void doWhileBeAttack(PlayerObject object, int direction, int animationID) {
        if (this.state != 3 && this.state != 4 && this.state != 0) {
            if ((this.state != 1 || this.show_step >= 4) && this.face_state != 2) {
                this.HP--;
                player.doBossAttackPose(this, direction);
                if (this.HP > 0) {
                    if (this.machine_state == 0) {
                        this.machine_state = 4;
                    }
                    if (this.machine_state == 1) {
                        this.machine_state = 5;
                    }
                    this.face_state = 2;
                    this.face_cnt = 0;
                } else {
                    this.state = 3;
                    this.face_state = 3;
                    this.machine_state = 0;
                    this.bossbroken = new BossBroken(28, this.posX >> 6, this.posY >> 6, 0, 0, 0, 0);
                    GameObject.addGameObject(this.bossbroken, this.posX >> 6, this.posY >> 6);
                    this.directTrans = true;
                    this.drop_cnt = 0;
                }
                if (this.HP == 0) {
                    SoundSystem.getInstance().playSe(35);
                } else {
                    SoundSystem.getInstance().playSe(34);
                }
            }
        }
    }

    private void changeAniState(AnimationDrawer AniDrawer, int state) {
        if (this.velocity > 0) {
            AniDrawer.setActionId(state);
            AniDrawer.setTrans(2);
            AniDrawer.setLoop(true);
            return;
        }
        AniDrawer.setActionId(state);
        AniDrawer.setTrans(0);
        AniDrawer.setLoop(true);
    }

    private void bossStateChange() {
        if (this.ball != null && this.ball.getPlayerHurt()) {
            this.face_state = 1;
            this.ball.resetPlayerHurt();
            this.face_cnt = 0;
        }
        if (this.face_state != 0) {
            if (this.face_cnt < 8) {
                this.face_cnt++;
            } else {
                if (this.machine_state == 4) {
                    this.machine_state = 0;
                }
                if (this.machine_state == 5) {
                    this.machine_state = 1;
                }
                this.face_state = 0;
                this.face_cnt = 0;
            }
        }
        changeAniState(this.machineDrawer, this.machine_state);
        changeAniState(this.faceDrawer, this.face_state);
    }

    private void degreeCal() {
        this.lineVelocity += ((GRAVITY - 60) * MyAPI.dSin((this.degree >> 6) - 90)) / 100;
        this.degree -= ((((this.lineVelocity << 6) / this.RADIUS) << 6) * RollPlatformSpeedC.DEGREE_VELOCITY) / SonicDef.PI;
    }

    public void balllogic(boolean direct) {
        this.oppoBallPosY += this.ballvely;
        if (this.oppoBallPosY > BALL_RASIUS) {
            this.oppoBallPosY = BALL_RASIUS;
        }
        this.oppoBallPosX = MFMath.sqrt(24920064 - (this.oppoBallPosY * this.oppoBallPosY)) >> 6;
        for (int i = 0; i < 4; i++) {
            if (direct) {
                this.ballPos[i][0] = this.posX + ((this.oppoBallPosX * ((i * 14) + 13)) / 78);
            } else {
                this.ballPos[i][0] = this.posX - ((this.oppoBallPosX * ((i * 14) + 13)) / 78);
            }
            this.ballPos[i][1] = this.posY + ((this.oppoBallPosY * ((i * 14) + 13)) / 78);
        }
        if (direct) {
            this.ballPos[4][0] = this.posX + this.oppoBallPosX;
        } else {
            this.ballPos[4][0] = this.posX - this.oppoBallPosX;
        }
        this.ballPos[4][1] = this.posY + this.oppoBallPosY;
        this.ballPos[5][0] = this.posX;
        this.ballPos[5][1] = this.posY + 256;
        this.ball.logic(this.ballPos[4][0], this.ballPos[4][1]);
    }

    public void logic() {
        if (!this.dead) {
            int preX = this.posX;
            int preY = this.posY;
            if (this.state > 0 && this.state < 4) {
                isBossEnter = true;
            } else if (this.state == 4) {
                isBossEnter = false;
            }
            int[] iArr;
            switch (this.state) {
                case 0:
                    if (player.getFootPositionX() < END_POSX) {
                        if (player.getFootPositionX() >= 55296) {
                            MapManager.setCameraLeftLimit(864 - (SCREEN_WIDTH / 2));
                            MapManager.setCameraRightLimit((SCREEN_WIDTH / 2) + 864);
                        }
                        if (player.getFootPositionX() >= 55296 && player.getFootPositionX() < ((((SCREEN_WIDTH / 2) + 864) + 20) << 6) && player.getFootPositionY() >= 43008) {
                            this.state = 1;
                            bossFighting = true;
                            bossID = 28;
                            MapManager.setCameraUpLimit(SIDE_UP1);
                            MapManager.setCameraDownLimit(784);
                            MapManager.setCameraLeftLimit(864 - (SCREEN_WIDTH / 2));
                            MapManager.setCameraRightLimit((SCREEN_WIDTH / 2) + 864);
                            this.show_step = 0;
                            this.enter_screen_frame_cn = 0;
                            SoundSystem.getInstance().playBgm(46);
                            this.displayFlag = true;
                            break;
                        }
                    }
                    break;
                case 1:
                    int i;
                    int i2;
                    int i3;
                    switch (this.show_step) {
                        case 0:
                            if (this.enter_screen_frame_cn >= 18) {
                                this.posY += ENTER_SPEED_1_Y;
                                if (this.posY >= BOSS_1_STOP_POSY) {
                                    this.posY = BOSS_1_STOP_POSY;
                                    this.show_step = 1;
                                    break;
                                }
                            }
                            this.enter_screen_frame_cn++;
                            break;
                        case 1:
                            this.posY += ENTER_SPEED_1_Y;
                            if (this.posY >= BOSS_2_STOP_POSY) {
                                this.posY = BOSS_2_STOP_POSY;
                                this.show_step = 2;
                                changeAniState(this.machineDrawer, 1);
                                break;
                            }
                            break;
                        case 2:
                            this.posX += ENTER_SPEED_3_X;
                            if (this.posX <= 55296) {
                                this.posX = 55296;
                                this.posY = 43008;
                                this.show_step = 3;
                                changeAniState(this.faceDrawer, 1);
                                changeAniState(this.machineDrawer, 0);
                                this.isDisplayBall = true;
                                for (i = 4; i >= 0; i--) {
                                    iArr = this.ballVel;
                                    i2 = 5824 - ((4 - i) * RING_RANGE);
                                    if (i < 4) {
                                        i3 = 512;
                                    } else {
                                        i3 = 0;
                                    }
                                    iArr[i] = (i2 - i3) / 17;
                                }
                                this.ballVel[5] = this.ballVel[0] >> 1;
                                break;
                            }
                            break;
                        case 3:
                            if (this.laugh_cn < 10) {
                                this.laugh_cn++;
                            } else {
                                changeAniState(this.faceDrawer, 0);
                            }
                            for (i = 0; i < 6; i++) {
                                iArr = this.ballPos[i];
                                iArr[1] = iArr[1] + this.ballVel[i];
                            }
                            if (this.ballPos[4][1] >= BALL_BOTTOM_Y) {
                                for (i = 4; i >= 0; i--) {
                                    iArr = this.ballPos[i];
                                    i2 = BALL_BOTTOM_Y - ((4 - i) * RING_RANGE);
                                    if (i < 4) {
                                        i3 = 512;
                                    } else {
                                        i3 = 0;
                                    }
                                    iArr[1] = i2 - i3;
                                }
                                this.ballPos[5][0] = this.posX;
                                this.ballPos[5][1] = this.posY + 256;
                                this.show_step = 4;
                                this.machine_state = 1;
                                MapManager.setCameraUpLimit(544);
                                MapManager.setCameraDownLimit(784);
                                this.velocity = -45;
                                this.vely = -7;
                                changeAniState(this.machineDrawer, 1);
                                this.oppoBallPosX = 0;
                                this.oppoBallPosY = BALL_RASIUS;
                                this.frameCn = 0;
                                this.ballvely = 0;
                                break;
                            }
                            break;
                        case 4:
                            bossStateChange();
                            if (this.show_step != 5) {
                                if (this.frameCn < 17) {
                                    this.frameCn++;
                                } else {
                                    this.frameCn = 17;
                                }
                                if (this.frameCn < 8) {
                                    this.ballvely -= 96;
                                } else if (this.frameCn < 16) {
                                    this.ballvely += 96;
                                } else {
                                    this.ballvely = 0;
                                    this.oppoBallPosY = 0;
                                }
                            }
                            if (this.posX > BOSS_MOVE_1_POSX) {
                                this.posY += this.vely;
                                this.posX += this.velocity;
                            } else {
                                this.show_step = 5;
                                this.frameCn = 0;
                                this.posY = BOSS_MOVE_1_POSY;
                                this.posX = BOSS_MOVE_1_POSX;
                                this.vely = 7;
                            }
                            balllogic(true);
                            break;
                        case 5:
                            bossStateChange();
                            if (this.state != 2) {
                                if (this.frameCn < 17) {
                                    this.frameCn++;
                                    this.Accy = (BALL_ACC_MAX - (this.frameCn * 22)) / 2;
                                    if (this.Accy < 0) {
                                        this.Accy = 0;
                                    }
                                    this.ballvely += this.Accy;
                                } else {
                                    this.frameCn = 17;
                                    this.oppoBallPosY = BALL_RASIUS;
                                }
                            }
                            if (this.posX > BOSS_LEFT_POSX) {
                                this.posY += this.vely;
                                this.posX += this.velocity;
                            } else {
                                this.state = 2;
                                this.posY = BOSS_MOVE_2_POSY;
                                this.posX = BOSS_LEFT_POSX;
                                this.pro_step = 0;
                                this.frameCn = 0;
                                this.vely = -15;
                                this.ballvely = 0;
                                this.machine_state = 0;
                            }
                            balllogic(true);
                            break;
                        default:
                            break;
                    }
                case 2:
                    bossStateChange();
                    switch (this.pro_step) {
                        case 0:
                            if (this.frameCn < 17) {
                                this.frameCn++;
                                this.Accy = (BALL_ACC_MAX - (this.frameCn * 17)) / 2;
                                this.ballvely -= this.Accy;
                            } else {
                                this.frameCn = 17;
                                this.oppoBallPosY = 0;
                                this.oppoBallPosX = BALL_RASIUS;
                            }
                            if (this.posY > BOSS_MOVE_1_POSY) {
                                this.posY += this.vely;
                            } else {
                                this.posY = BOSS_MOVE_1_POSY;
                                this.pro_step = 1;
                                this.velocity = RollPlatformSpeedC.DEGREE_VELOCITY;
                                this.vely = 15;
                                this.frameCn = 0;
                                this.machine_state = 1;
                                this.face_state = 0;
                                this.directTrans = true;
                                this.ballvely = 0;
                            }
                            balllogic(false);
                            break;
                        case 1:
                            if (this.frameCn < 17) {
                                this.frameCn++;
                                this.Accy = (BALL_ACC_MAX - (this.frameCn * 17)) / 2;
                                this.ballvely += this.Accy;
                            } else {
                                this.frameCn = 17;
                                this.oppoBallPosY = BALL_RASIUS;
                                this.oppoBallPosX = 0;
                            }
                            if (this.posX < BOSS_RIGHT_POSX) {
                                this.posX += this.velocity;
                                this.posY += this.vely;
                            } else {
                                this.posX = BOSS_RIGHT_POSX;
                                this.posY = BOSS_MOVE_2_POSY;
                                this.pro_step = 2;
                                this.vely = -15;
                                this.machine_state = 0;
                                this.frameCn = 0;
                                this.ballvely = 0;
                            }
                            balllogic(false);
                            break;
                        case 2:
                            if (this.frameCn < 17) {
                                this.frameCn++;
                                this.Accy = (BALL_ACC_MAX - (this.frameCn * 17)) / 2;
                                this.ballvely -= this.Accy;
                            } else {
                                this.frameCn = 17;
                                this.oppoBallPosY = 0;
                                this.oppoBallPosX = BALL_RASIUS;
                            }
                            if (this.posY > BOSS_MOVE_1_POSY) {
                                this.posY += this.vely;
                            } else {
                                this.posY = BOSS_MOVE_1_POSY;
                                this.pro_step = 3;
                                this.frameCn = 0;
                                this.directTrans = false;
                                this.velocity = -180;
                                this.vely = 15;
                                this.machine_state = 1;
                                this.face_state = 0;
                                this.ballvely = 0;
                            }
                            balllogic(true);
                            break;
                        case 3:
                            if (this.frameCn < 17) {
                                this.frameCn++;
                                this.Accy = (BALL_ACC_MAX - (this.frameCn * 17)) / 2;
                                this.ballvely += this.Accy;
                            } else {
                                this.frameCn = 17;
                                this.oppoBallPosY = BALL_RASIUS;
                                this.oppoBallPosX = 0;
                            }
                            if (this.posX > BOSS_LEFT_POSX) {
                                this.posX += this.velocity;
                                this.posY += this.vely;
                            } else {
                                this.posX = BOSS_LEFT_POSX;
                                this.posY = BOSS_MOVE_2_POSY;
                                this.pro_step = 0;
                                this.vely = -15;
                                this.machine_state = 0;
                                this.frameCn = 0;
                                this.ballvely = 0;
                            }
                            balllogic(true);
                            break;
                        default:
                            break;
                    }
                case 3:
                    if (this.posY >= BROKEN_STOP_POSY) {
                        this.posY = BROKEN_STOP_POSY;
                    } else {
                        this.posY += ENTER_SPEED_1_Y;
                    }
                    this.bossbroken.logicBoom(this.posX, this.posY - BROKEN_OFFSET_Y);
                    if (this.ballPos[4][1] + this.drop_vely > getGroundY(this.ballPos[4][0], this.ballPos[4][1]) && this.drop_cnt == 0) {
                        this.ballPos[4][1] = getGroundY(this.ballPos[4][0], this.ballPos[4][1]);
                        this.drop_vely = -640;
                        this.drop_cnt = 1;
                    } else if (this.ballPos[4][1] + this.drop_vely > getGroundY(this.ballPos[4][0], this.ballPos[4][1]) && this.drop_cnt == 1) {
                        this.ballPos[4][1] = getGroundY(this.ballPos[4][0], this.ballPos[4][1]);
                        this.drop_vely = -320;
                        this.drop_cnt = 2;
                    } else if (this.ballPos[4][1] + this.drop_vely > getGroundY(this.ballPos[4][0], this.ballPos[4][1]) && this.drop_cnt == 2) {
                        this.ballPos[4][1] = getGroundY(this.ballPos[4][0], this.ballPos[4][1]);
                        this.drop_cnt = 3;
                    } else if (this.drop_cnt != 3) {
                        this.drop_vely += GRAVITY;
                        iArr = this.ballPos[4];
                        iArr[1] = iArr[1] + this.drop_vely;
                        this.ball.setEnd();
                        this.isDisplayBall = false;
                    }
                    if (this.bossbroken.getEndState()) {
                        this.state = 4;
                        bossFighting = false;
                        player.getBossScore();
                        this.velocity = ENTER_SPEED_1_Y;
                        this.machine_state = 1;
                        this.face_state = 0;
                        SoundSystem.getInstance().playBgm(18);
                    }
                    changeAniState(this.machineDrawer, this.machine_state);
                    changeAniState(this.faceDrawer, this.face_state);
                    break;
                case 4:
                    if (this.posX >= 65536) {
                        this.posX = 65536;
                        MapManager.releaseCamera();
                        MapManager.setCameraLeftLimit(864 - (SCREEN_WIDTH / 2));
                        MapManager.setCameraDownLimit(MapManager.getPixelHeight());
                        MapManager.setCameraRightLimit(MapManager.getPixelWidth());
                        this.dead = true;
                    } else {
                        this.posX += ESCAPE_SPEED_X;
                    }
                    if (this.posY < ESCAPE_STOP_POSY) {
                        this.posY += ENTER_SPEED_1_Y;
                        break;
                    } else {
                        this.posY = ESCAPE_STOP_POSY;
                        break;
                    }
            }
            refreshCollisionRect(this.posX, this.posY);
            checkWithPlayer(preX, preY, this.posX, this.posY);
        }
    }

    public int getPaintLayer() {
        return 3;
    }

    public void draw(MFGraphics g) {
        if (this.displayFlag && !this.dead) {
            if (this.state >= 1) {
                if (this.isDisplayBall) {
                    for (int i = 0; i < 6; i++) {
                        drawInMap(g, this.ballDrawer[i], this.ballPos[i][0], this.ballPos[i][1]);
                    }
                }
                drawInMap(g, this.machineDrawer);
                if (this.directTrans) {
                    drawInMap(g, this.faceDrawer, this.posX - 192, this.posY + FACE_OFFSET_Y);
                } else {
                    drawInMap(g, this.faceDrawer, this.posX + 192, this.posY + FACE_OFFSET_Y);
                }
                if (this.ball != null) {
                    this.ball.draw(g);
                }
                if (this.state == 3) {
                    this.bossbroken.draw(g);
                }
            }
            drawCollisionRect(g);
        }
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 1504, y - 2560, COLLISION_WIDTH, Boss6Block.COLLISION_WIDTH);
    }

    public void close() {
        this.machineDrawer = null;
        this.faceDrawer = null;
        this.bossbroken = null;
    }
}
