package SonicGBA;

import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;

/* compiled from: GimmickObject */
class Subeyuka extends GimmickObject {
    private static final int COLLISION_HEIGHT = 1920;
    private static final int COLLISION_WIDTH = 3072;
    private static final int DRAW_OFFSET_Y = 64;
    private static int frame;
    private static MFImage image;
    private boolean dead;
    private int deadCount;
    private int flyCount = 0;
    private MapObject mapObj;
    private boolean moving;
    private int startPosX;
    private int startPosY;
    private int tmpSpeedX = 0;
    private int tmpX = 0;
    private int tmpY = 0;

    protected Subeyuka(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        if (image == null) {
            if (StageManager.getCurrentZoneId() != 4) {
                image = MFImage.createImage("/gimmick/subeyuka_5.png");
            } else {
                image = MFImage.createImage("/gimmick/subeyuka_4.png");
            }
        }
        this.startPosX = this.posX;
        this.startPosY = this.posY;
        this.moving = false;
        this.mapObj = new MapObject(this.startPosX, this.startPosY, 0, 0, this, this.iLeft);
        this.flyCount = 0;
    }

    public void draw(MFGraphics g) {
        if (!this.dead) {
            drawInMap(g, image, this.posX, this.posY + 64, 33);
            drawCollisionRect(g);
        }
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (!this.dead) {
            switch (direction) {
                case 1:
                    if (!this.moving) {
                        this.moving = true;
                        this.mapObj.setPosition(this.posX, this.posY, 0, 0, this);
                        if (StageManager.getCurrentZoneId() != 4) {
                            this.mapObj.setCrashCount(2);
                        } else {
                            this.mapObj.setCrashCount(2);
                        }
                        if (!(StageManager.getCurrentZoneId() == 4 || StageManager.getCurrentZoneId() == 5)) {
                            soundInstance.playSe(64);
                        }
                        isGotRings = false;
                        frame = 1;
                    }
                    object.beStop(0, direction, this);
                    return;
                case 4:
                    if (object.getMoveDistance().f14y > 0 && object.getCollisionRect().y1 < this.collisionRect.y1) {
                        if (!this.moving) {
                            this.moving = true;
                            this.mapObj.setPosition(this.posX, this.posY, 0, 0, this);
                            this.mapObj.setCrashCount(2);
                            if (!(StageManager.getCurrentZoneId() == 4 || StageManager.getCurrentZoneId() == 5)) {
                                soundInstance.playSe(64);
                            }
                            isGotRings = false;
                            frame = 1;
                        }
                        object.beStop(0, 1, this);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    public void logic() {
        if (this.moving && !this.dead) {
            if ((StageManager.getCurrentZoneId() == 4 || StageManager.getCurrentZoneId() == 5) && this.iLeft == 1 && this.mapObj.getCurrentCrashCount() < 2) {
                int i = this.flyCount + 1;
                this.flyCount = i;
                if (i < 4) {
                    if (this.flyCount == 2) {
                        soundInstance.playSe(64);
                    }
                    if (StageManager.getCurrentZoneId() != 5) {
                        this.velX = (this.flyCount * 6) << 6;
                        this.mapObj.setVel(this.velX, this.velY);
                    }
                }
            }
            if (!(frame != 4 || StageManager.getCurrentZoneId() == 4 || StageManager.getCurrentZoneId() == 5)) {
                soundInstance.playLoopSe(65);
            }
            frame++;
            this.mapObj.logic();
            this.posX = this.mapObj.getPosX();
            this.posY = this.mapObj.getPosY();
            if (this.mapObj.getCurrentCrashCount() != 0) {
                this.tmpSpeedX = this.mapObj.getVelX();
                checkWithPlayer(this.posX, this.posY, this.mapObj.getPosX(), this.mapObj.getPosY());
                this.tmpX = this.mapObj.getPosX();
                this.tmpY = this.mapObj.getPosY();
            } else {
                this.mapObj.setPosition(this.tmpX, this.tmpY);
                this.posX = this.tmpX;
                this.posY = this.tmpY;
            }
            System.out.println("mapObj.getCurrentCrashCount()=" + this.mapObj.getCurrentCrashCount());
            if (this.mapObj.chkCrash()) {
                this.dead = true;
                if (player.isFootOnObject(this)) {
                    player.setVelX(this.tmpSpeedX);
                    player.doJump();
                }
                if (StageManager.getCurrentZoneId() == 4) {
                    Effect.showEffect(iceBreakAnimation, 0, this.posX >> 6, this.posY >> 6, 0);
                } else if (StageManager.getCurrentZoneId() == 5) {
                    Effect.showEffect(platformBreakAnimation, 0, this.posX >> 6, this.posY >> 6, 0);
                } else {
                    Effect.showEffect(rockBreakAnimation, 0, this.posX >> 6, this.posY >> 6, 0);
                }
                soundInstance.playSe(34);
            }
        }
        if (StageManager.getCurrentZoneId() != 4 && this.dead && soundInstance.getPlayingLoopSeIndex() == 65) {
            soundInstance.stopLoopSe();
        }
    }

    public boolean checkInit() {
        return false;
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 1536, y - COLLISION_HEIGHT, 3072, COLLISION_HEIGHT);
    }

    public void close() {
    }

    public static void releaseAllResource() {
        image = null;
    }

    public int getPaintLayer() {
        return 0;
    }
}
