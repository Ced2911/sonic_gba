package SonicGBA;

import GameEngine.Def;
import GameEngine.Key;
import Lib.Animation;
import Lib.AnimationDrawer;
import Lib.Line;
import Lib.SoundSystem;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: GimmickObject */
class FlipH extends GimmickObject {
    private static final int COLLISION_HEIGHT = 1536;
    private static final int COLLISION_WIDTH = 2880;
    private static final int FLIP_POWER = 2450;
    private static final int FLIP_POWER_MAX = 2858;
    private static final int FLIP_POWER_MIN = 2163;
    private static final int FLIP_POWER_RANGE = 659;
    private static final int FLIP_POWER_X = 200;
    private static Animation flipAnimation;
    private Line collisionLine;
    private AnimationDrawer drawer;
    private boolean isUp;
    private boolean justPop = false;
    private int leftBorder;
    private boolean noTouch = true;
    private int rightBorder;

    protected FlipH(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        if (flipAnimation == null) {
            flipAnimation = new Animation("/animation/flip");
        }
        this.drawer = flipAnimation.getDrawer(2, true, this.iLeft == 0 ? 2 : 0);
        if (this.iLeft == 0) {
            this.rightBorder = this.posX;
            this.leftBorder = this.posX - 2432;
            return;
        }
        this.leftBorder = this.posX;
        this.rightBorder = this.posX + 2432;
    }

    public void draw(MFGraphics g) {
        drawInMap(g, this.drawer);
        if (this.drawer.checkEnd()) {
            this.drawer.setActionId(2);
            this.drawer.setLoop(true);
        }
        drawCollisionRect(g);
    }

    public void refreshCollisionRect(int x, int y) {
        if (this.iLeft == 0) {
            this.collisionRect.setTwoPosition(x + 512, (y - 512) + 1, (x + 512) - COLLISION_WIDTH, ((y - 512) + 1) + 1536);
        } else {
            this.collisionRect.setRect(x - 512, (y - 512) + 1, COLLISION_WIDTH, 1536);
        }
        if (this.collisionLine == null) {
            this.collisionLine = new Line();
        }
        if (this.iLeft == 0) {
            this.collisionLine.setProperty(x, y - 512, x - 2432, y + BarHorbinV.COLLISION_HEIGHT);
        } else {
            this.collisionLine.setProperty(x, y - 512, x + 2432, y + BarHorbinV.COLLISION_HEIGHT);
        }
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (object == player) {
            if (this.noTouch && !this.justPop) {
                if (direction == 1 || direction == 2 || direction == 3) {
                    this.isUp = true;
                    this.noTouch = false;
                    if (this.firstTouch) {
                        if (player.getFootPositionX() < this.leftBorder) {
                            player.setFootPositionX(this.leftBorder);
                        } else if (player.getFootPositionX() > this.rightBorder) {
                            player.setFootPositionX(this.rightBorder);
                        }
                        if (!(player instanceof PlayerAmy) && this.firstTouch) {
                            soundInstance.playSe(4);
                        }
                    }
                } else if (direction == 0) {
                    this.isUp = false;
                    this.noTouch = false;
                }
            }
            if (!this.noTouch) {
                int lineY = this.collisionLine.getY(player.getFootPositionX());
                int playerY = player.getFootPositionY();
                if (this.isUp && playerY >= lineY && player.getMoveDistance().f14y >= 0) {
                    player.setNoKey();
                    player.beStop(lineY, 1, this);
                    player.setFootPositionY(lineY);
                    player.setVelX(this.iLeft == 0 ? Def.TOUCH_HELP_LEFT_X : 128);
                    player.setAnimationId(4);
                }
            }
            if (direction == 0) {
                player.beStop(0, 0, this);
            }
        }
    }

    public void logic() {
        if (this.justPop) {
            this.justPop = false;
        }
        if (!this.noTouch && Key.press(Key.gUp | 16777216)) {
            this.drawer.setActionId(3);
            this.drawer.setLoop(false);
            int power = ((Math.abs(player.getFootPositionX() - (this.iLeft == 0 ? this.posX : this.posX)) * FLIP_POWER_RANGE) / COLLISION_WIDTH) + FLIP_POWER_MIN;
            player.bePop(power, 1);
            player.setAnimationId(4);
            player.setVelX(this.iLeft == 0 ? (-power) / 10 : power / 10);
            this.noTouch = true;
            this.justPop = true;
            SoundSystem.getInstance().playSe(54);
        }
    }

    public void doWhileNoCollision() {
        this.noTouch = true;
    }

    public void close() {
        this.drawer = null;
    }

    public static void releaseAllResource() {
        Animation.closeAnimation(flipAnimation);
        flipAnimation = null;
    }

    public int getPaintLayer() {
        return 0;
    }
}
