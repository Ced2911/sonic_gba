package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import Lib.SoundSystem;
import com.sega.mobile.define.MDPhone;
import com.sega.mobile.framework.device.MFGraphics;
import java.lang.reflect.Array;

/* compiled from: EnemyObject */
class BossF2 extends BossObject {
    private static final int BOSS_ENTER_SPEED = -360;
    private static final int BOSS_ENTER_STOP_POSX = 104448;
    private static final int BOSS_MOVE_SIDE_LEFT = 91520;
    private static final int BOSS_MOVE_SIDE_RIGHT = 117376;
    private static final int BOSS_MOVE_SPEED = 480;
    private static final int BOSS_PLAYER_FIGHT_DISTANCE = 5760;
    private static final int COLLISION_BROKEN_HEIGHT = 2560;
    private static final int COLLISION_HEIGHT = 3072;
    private static final int COLLISION_WIDTH = 4224;
    private static final int DRILL_OFFSET_X = 2944;
    private static final int DRILL_OFFSET_Y = 1280;
    private static final int DRILL_SHOOT_SPEED = 400;
    private static final int END_POSX = 117120;
    private static final int ENTER_SCREEN_FRAME_MAX = 3;
    private static final int FACE_BROKEN = 3;
    private static final int FACE_HURT = 2;
    private static final int FACE_NORMAL = 0;
    private static final int FACE_OFFSET_Y = 2112;
    private static final int FACE_SMILE = 1;
    private static final int INIT_STOP_POSX = 105472;
    private static final int INIT_STOP_POSY = 33792;
    private static final int LAUGH_TIME = 11;
    private static final int MACHINE_BROKEN = 3;
    private static final int MACHINE_HURT = 2;
    private static final int MACHINE_MOVE = 0;
    private static final int MACHINE_NODRILL_MOVE = 1;
    private static final int SHOW_BOSS_END = 2;
    private static final int SHOW_BOSS_ENTER = 0;
    private static final int SHOW_BOSS_LAUGH = 1;
    private static final int SIDE_DOWN = 688;
    private static final int SIDE_LEFT = 1480;
    private static final int SIDE_RIGHT = 1784;
    private static final int SIDE_UP = 528;
    private static final int STATE_BROKEN = 3;
    private static final int STATE_ENTER_SHOW = 1;
    private static final int STATE_ESCAPE = 4;
    private static final int STATE_INIT = 0;
    private static final int STATE_PRO = 2;
    private static Animation boatAni = null;
    private static final int cnt_max = 8;
    private static Animation escapefaceAni;
    private static Animation faceAni;
    private static Animation machineAni;
    private static Animation wheelAni;
    private int WaitCnt;
    private AnimationDrawer boatdrawer;
    private BossBroken bossbroken;
    private boolean displayFlag;
    private BossF2Drill drill;
    private int drill_offsetx;
    private int drop_cnt;
    private int drop_velY;
    private int enter_screen_frame_cn;
    private int escape_v = 512;
    private AnimationDrawer escapefacedrawer;
    private AnimationDrawer faceDrawer;
    private int face_cnt;
    private int face_state;
    private int fly_end;
    private int fly_top;
    private int fly_top_range = BOSS_PLAYER_FIGHT_DISTANCE;
    private boolean isDisplayDrill;
    private boolean isFight = false;
    private boolean isShoot = false;
    private int laugh_cn;
    private AnimationDrawer machineDrawer;
    private int machine_state;
    private int show_step;
    private int state;
    private int velocity;
    private int wait_cnt;
    private int wait_cnt_max = 10;
    private AnimationDrawer[] wheelDrawer;
    private int wheel_velx;
    private int wheel_vely;
    private int[][] wheelpos;

    protected BossF2(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        this.posX -= this.iLeft * 8;
        this.posY -= this.iTop * 8;
        this.posX = 114176;
        this.posY = getGroundY(this.posX, this.posY);
        if (machineAni == null) {
            machineAni = new Animation("/animation/bossf2_machine");
        }
        this.machineDrawer = machineAni.getDrawer(0, true, 0);
        if (faceAni == null) {
            faceAni = new Animation("/animation/bossf2_face");
        }
        this.faceDrawer = faceAni.getDrawer(0, true, 0);
        if (wheelAni == null) {
            wheelAni = new Animation("/animation/bossf2_wheel");
        }
        this.wheelDrawer = new AnimationDrawer[2];
        this.wheelDrawer[0] = wheelAni.getDrawer(0, true, 0);
        this.wheelDrawer[1] = wheelAni.getDrawer(1, true, 0);
        this.wheelpos = (int[][]) Array.newInstance(Integer.TYPE, new int[]{4, 2});
        if (boatAni == null) {
            boatAni = new Animation("/animation/pod_boat");
        }
        this.boatdrawer = boatAni.getDrawer(0, true, 0);
        if (escapefaceAni == null) {
            escapefaceAni = new Animation("/animation/pod_face");
        }
        this.escapefacedrawer = escapefaceAni.getDrawer(0, false, 0);
        this.machine_state = 0;
        this.face_state = 0;
        this.isDisplayDrill = false;
        this.drill_offsetx = 0;
        this.state = 0;
        this.displayFlag = false;
        this.HP = 4;
    }

    public static void releaseAllResource() {
        Animation.closeAnimation(machineAni);
        Animation.closeAnimation(faceAni);
        machineAni = null;
        faceAni = null;
    }

    public void close() {
        this.machineDrawer = null;
        this.faceDrawer = null;
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (!this.dead && this.state != 4 && this.state != 3 && this.state == 2 && object == player) {
            if (player.isAttackingEnemy()) {
                if (this.face_state != 2) {
                    this.HP--;
                    player.doBossAttackPose(this, direction);
                    if (this.HP > 0) {
                        this.machine_state = 2;
                        this.face_state = 2;
                        this.face_cnt = 0;
                        if (this.HP == 1) {
                            this.isFight = true;
                            this.isShoot = false;
                        }
                    } else {
                        this.state = 3;
                        this.face_state = 3;
                        this.machine_state = 3;
                        this.bossbroken = new BossBroken(28, this.posX >> 6, this.posY >> 6, 0, 0, 0, 0);
                        GameObject.addGameObject(this.bossbroken, this.posX >> 6, this.posY >> 6);
                        this.drop_velY = 0;
                        for (int i = 0; i < 4; i++) {
                            this.wheelpos[i][1] = this.posY;
                        }
                        if (this.velocity <= 0) {
                            this.wheelpos[0][0] = (this.posX - 2112) + 256;
                            this.wheelpos[1][0] = this.posX - MDPhone.SCREEN_HEIGHT;
                            this.wheelpos[2][0] = this.posX + 320;
                            this.wheelpos[3][0] = (this.posX + 2112) - 768;
                            this.wheel_velx = -480;
                        } else {
                            this.wheelDrawer[0].setTrans(2);
                            this.wheelDrawer[1].setTrans(2);
                            this.wheelpos[3][0] = ((this.posX - 2112) + 256) + PlayerSonic.BACK_JUMP_SPEED_X;
                            this.wheelpos[2][0] = (this.posX - MDPhone.SCREEN_HEIGHT) + PlayerSonic.BACK_JUMP_SPEED_X;
                            this.wheelpos[1][0] = (this.posX + 320) + PlayerSonic.BACK_JUMP_SPEED_X;
                            this.wheelpos[0][0] = ((this.posX + 2112) - 768) + PlayerSonic.BACK_JUMP_SPEED_X;
                            this.wheel_velx = BOSS_MOVE_SPEED;
                        }
                        this.wheel_vely = -512;
                        this.posY -= 512;
                        this.isDisplayDrill = false;
                        this.drill.setEnd();
                        this.drop_cnt = 0;
                    }
                    if (this.HP == 0) {
                        SoundSystem.getInstance().playSe(35);
                    } else {
                        SoundSystem.getInstance().playSe(34);
                    }
                }
            } else if (this.state != 3 && this.state != 4) {
                player.beHurt();
                this.face_state = 1;
            }
        }
    }

    public void doWhileBeAttack(PlayerObject object, int direction, int animationID) {
        if (this.state != 4 && this.state != 3 && this.state == 2 && this.face_state != 2) {
            this.HP--;
            player.doBossAttackPose(this, direction);
            if (this.HP > 0) {
                this.machine_state = 2;
                this.face_state = 2;
                this.face_cnt = 0;
                if (this.HP == 1) {
                    this.isFight = true;
                    this.isShoot = false;
                }
            } else {
                this.state = 3;
                this.face_state = 3;
                this.machine_state = 3;
                this.bossbroken = new BossBroken(28, this.posX >> 6, this.posY >> 6, 0, 0, 0, 0);
                GameObject.addGameObject(this.bossbroken, this.posX >> 6, this.posY >> 6);
                this.drop_velY = 0;
                for (int i = 0; i < 4; i++) {
                    this.wheelpos[i][1] = this.posY;
                }
                if (this.velocity <= 0) {
                    this.wheelpos[0][0] = (this.posX - 2112) + 256;
                    this.wheelpos[1][0] = this.posX - MDPhone.SCREEN_HEIGHT;
                    this.wheelpos[2][0] = this.posX + 320;
                    this.wheelpos[3][0] = (this.posX + 2112) - 768;
                    this.wheel_velx = -480;
                } else {
                    this.wheelDrawer[0].setTrans(2);
                    this.wheelDrawer[1].setTrans(2);
                    this.wheelpos[3][0] = ((this.posX - 2112) + 256) + PlayerSonic.BACK_JUMP_SPEED_X;
                    this.wheelpos[2][0] = (this.posX - MDPhone.SCREEN_HEIGHT) + PlayerSonic.BACK_JUMP_SPEED_X;
                    this.wheelpos[1][0] = (this.posX + 320) + PlayerSonic.BACK_JUMP_SPEED_X;
                    this.wheelpos[0][0] = ((this.posX + 2112) - 768) + PlayerSonic.BACK_JUMP_SPEED_X;
                    this.wheel_velx = BOSS_MOVE_SPEED;
                }
                this.wheel_vely = -512;
                this.posY -= 512;
                this.isDisplayDrill = false;
                this.drill.setEnd();
                this.drop_cnt = 0;
            }
            if (this.HP == 0) {
                SoundSystem.getInstance().playSe(35);
            } else {
                SoundSystem.getInstance().playSe(34);
            }
        }
    }

    private void changeAniState(AnimationDrawer AniDrawer, int state) {
        if (this.velocity > 0) {
            AniDrawer.setActionId(state);
            AniDrawer.setTrans(2);
            AniDrawer.setLoop(true);
            return;
        }
        AniDrawer.setActionId(state);
        AniDrawer.setTrans(0);
        AniDrawer.setLoop(true);
    }

    public void logic() {
        if (!this.dead) {
            int preX = this.posX;
            int preY = this.posY;
            if (this.state > 0 && this.state < 4) {
                isBossEnter = true;
            } else if (this.state == 4) {
                isBossEnter = false;
            }
            switch (this.state) {
                case 0:
                    if (player.getFootPositionX() < END_POSX) {
                        if (player.getFootPositionX() >= INIT_STOP_POSX) {
                            MapManager.setCameraLeftLimit(SIDE_LEFT);
                            MapManager.setCameraRightLimit(SIDE_RIGHT);
                        }
                        if (player.getFootPositionX() >= INIT_STOP_POSX && player.getFootPositionX() < 116096 && player.getFootPositionY() >= INIT_STOP_POSY) {
                            this.state = 1;
                            bossFighting = true;
                            bossID = 29;
                            MapManager.setCameraUpLimit(SIDE_UP);
                            MapManager.setCameraDownLimit(SIDE_DOWN);
                            MapManager.setCameraLeftLimit(SIDE_LEFT);
                            MapManager.setCameraRightLimit(SIDE_RIGHT);
                            this.show_step = 0;
                            SoundSystem.getInstance().playBgm(47);
                            this.displayFlag = true;
                            break;
                        }
                    }
                    break;
                case 1:
                    switch (this.show_step) {
                        case 0:
                            if (this.posX + BOSS_ENTER_SPEED <= BOSS_ENTER_STOP_POSX) {
                                this.posX = BOSS_ENTER_STOP_POSX;
                                this.show_step = 1;
                                this.enter_screen_frame_cn = 0;
                                this.laugh_cn = 0;
                                break;
                            }
                            this.posX += BOSS_ENTER_SPEED;
                            break;
                        case 1:
                            if (this.enter_screen_frame_cn < 3) {
                                this.enter_screen_frame_cn++;
                            } else {
                                this.face_state = 1;
                            }
                            if (this.laugh_cn >= 14) {
                                this.show_step = 2;
                                this.face_state = 0;
                                break;
                            }
                            this.laugh_cn++;
                            break;
                        case 2:
                            this.state = 2;
                            this.velocity = -480;
                            this.drill = new BossF2Drill(29, this.posX, this.posY, 0, 0, 0, 0);
                            GameObject.addGameObject(this.drill, this.posX >> 6, this.posY >> 6);
                            break;
                        default:
                            break;
                    }
                case 2:
                    if (this.face_state != 0) {
                        if (this.face_cnt < 8) {
                            this.face_cnt++;
                        } else {
                            this.machine_state = 0;
                            this.face_state = 0;
                            this.face_cnt = 0;
                        }
                    }
                    changeAniState(this.machineDrawer, this.machine_state);
                    changeAniState(this.faceDrawer, this.face_state);
                    int drill_x = (this.posX + (this.velocity > 0 ? DRILL_OFFSET_X : -2944)) + this.drill_offsetx;
                    if ((drill_x > BOSS_MOVE_SIDE_RIGHT || drill_x < BOSS_MOVE_SIDE_LEFT) && this.drill_offsetx != 0) {
                        this.isDisplayDrill = false;
                        this.drill.setEnd();
                    }
                    if (this.drill != null) {
                        boolean z;
                        BossF2Drill bossF2Drill = this.drill;
                        int i = this.posY - DRILL_OFFSET_Y;
                        if (this.velocity > 0) {
                            z = true;
                        } else {
                            z = false;
                        }
                        bossF2Drill.logic(drill_x, i, z);
                    }
                    if (this.velocity > 0) {
                        if (this.posX < BOSS_MOVE_SIDE_RIGHT) {
                            this.posX += this.velocity;
                        } else {
                            this.posX = BOSS_MOVE_SIDE_RIGHT;
                            this.velocity = -this.velocity;
                        }
                    } else if (this.posX > BOSS_MOVE_SIDE_LEFT) {
                        this.posX += this.velocity;
                    } else {
                        this.posX = BOSS_MOVE_SIDE_LEFT;
                        this.velocity = -this.velocity;
                    }
                    if (this.isFight) {
                        if (this.velocity > 0) {
                            if (this.posX == BOSS_MOVE_SIDE_LEFT) {
                                this.isShoot = true;
                                this.isDisplayDrill = true;
                                this.isFight = false;
                            }
                        } else if (this.posX == BOSS_MOVE_SIDE_RIGHT) {
                            this.isShoot = true;
                            this.isDisplayDrill = true;
                            this.isFight = false;
                        }
                    }
                    if (this.isShoot) {
                        if (this.velocity > 0) {
                            this.drill_offsetx += 400;
                        } else {
                            this.drill_offsetx -= 400;
                        }
                        this.machine_state = 1;
                        break;
                    }
                    break;
                case 3:
                    this.bossbroken.logicBoom(this.posX, this.posY - DRILL_OFFSET_Y);
                    if (this.posY + this.drop_velY > getGroundY(this.posX, this.posY)) {
                        this.posY = getGroundY(this.posX, this.posY);
                    } else {
                        this.drop_velY += GRAVITY >> 1;
                        this.posY += this.drop_velY;
                    }
                    int[] iArr = this.wheelpos[0];
                    iArr[0] = iArr[0] + this.wheel_velx;
                    iArr = this.wheelpos[1];
                    iArr[0] = iArr[0] + this.wheel_velx;
                    iArr = this.wheelpos[2];
                    iArr[0] = iArr[0] - this.wheel_velx;
                    iArr = this.wheelpos[3];
                    iArr[0] = iArr[0] - this.wheel_velx;
                    if (this.wheelpos[0][1] + this.wheel_vely > getGroundY(this.wheelpos[0][0], this.wheelpos[0][1]) && this.drop_cnt == 0) {
                        this.wheelpos[0][1] = getGroundY(this.wheelpos[0][0], this.wheelpos[0][1]);
                        this.wheel_vely = -384;
                        this.drop_cnt = 1;
                    } else if (this.wheelpos[0][1] + this.wheel_vely > getGroundY(this.wheelpos[0][0], this.wheelpos[0][1]) && this.drop_cnt == 1) {
                        this.wheelpos[0][1] = getGroundY(this.wheelpos[0][0], this.wheelpos[0][1]);
                        this.wheel_vely = -192;
                        this.drop_cnt = 2;
                    } else if (this.wheelpos[0][1] + this.wheel_vely <= getGroundY(this.wheelpos[0][0], this.wheelpos[0][1]) || this.drop_cnt != 2) {
                        this.wheel_vely += GRAVITY;
                        iArr = this.wheelpos[0];
                        iArr[1] = iArr[1] + this.wheel_vely;
                    } else {
                        this.wheelpos[0][1] = getGroundY(this.wheelpos[0][0], this.wheelpos[0][1]);
                    }
                    this.wheelpos[1][1] = this.wheelpos[0][1];
                    this.wheelpos[2][1] = this.wheelpos[0][1];
                    this.wheelpos[3][1] = this.wheelpos[0][1];
                    if (this.bossbroken.getEndState()) {
                        this.escapefacedrawer.setActionId(4);
                        this.escapefacedrawer.setLoop(true);
                        this.state = 4;
                        bossFighting = false;
                        this.fly_top = this.posY;
                        this.fly_end = 114176;
                        player.getBossScore();
                        this.wait_cnt = 0;
                        break;
                    }
                    break;
                case 4:
                    this.wait_cnt++;
                    if (this.wait_cnt >= this.wait_cnt_max && this.posY >= this.fly_top - this.fly_top_range) {
                        this.posY -= this.escape_v;
                    }
                    if (this.posY <= this.fly_top - this.fly_top_range && this.WaitCnt == 0) {
                        this.posY = this.fly_top - this.fly_top_range;
                        this.escapefacedrawer.setActionId(0);
                        this.boatdrawer.setActionId(1);
                        this.boatdrawer.setLoop(false);
                        this.WaitCnt = 1;
                    }
                    if (this.WaitCnt == 1 && this.boatdrawer.checkEnd()) {
                        this.escapefacedrawer.setActionId(0);
                        this.escapefacedrawer.setTrans(2);
                        this.escapefacedrawer.setLoop(true);
                        this.boatdrawer.setActionId(1);
                        this.boatdrawer.setTrans(2);
                        this.boatdrawer.setLoop(false);
                        this.WaitCnt = 2;
                    }
                    if (this.WaitCnt == 2 && this.boatdrawer.checkEnd()) {
                        this.boatdrawer.setActionId(0);
                        this.boatdrawer.setTrans(2);
                        this.boatdrawer.setLoop(true);
                        this.WaitCnt = 3;
                    }
                    if (this.WaitCnt == 3 || this.WaitCnt == 4) {
                        this.posX += this.escape_v;
                    }
                    if (this.posX - this.fly_end > this.fly_top_range && this.WaitCnt == 3) {
                        MapManager.setCameraUpLimit(0);
                        MapManager.setCameraDownLimit(MapManager.getPixelHeight());
                        MapManager.setCameraRightLimit(MapManager.getPixelWidth());
                        this.WaitCnt = 4;
                        SoundSystem.getInstance().playBgm(18);
                        break;
                    }
            }
            changeAniState(this.machineDrawer, this.machine_state);
            changeAniState(this.faceDrawer, this.face_state);
            refreshCollisionRect(this.posX, this.posY);
            checkWithPlayer(preX, preY, this.posX, this.posY);
        }
    }

    public void draw(MFGraphics g) {
        if (this.displayFlag && !this.dead) {
            if (this.state == 3) {
                drawInMap(g, this.wheelDrawer[1], this.wheelpos[0][0], this.wheelpos[0][1]);
                drawInMap(g, this.wheelDrawer[1], this.wheelpos[2][0], this.wheelpos[2][1]);
            }
            if (this.state != 4) {
                drawInMap(g, this.machineDrawer);
            }
            if (this.state == 3) {
                drawInMap(g, this.wheelDrawer[0], this.wheelpos[1][0], this.wheelpos[1][1]);
                drawInMap(g, this.wheelDrawer[0], this.wheelpos[3][0], this.wheelpos[3][1]);
            }
            if (this.state != 4) {
                drawInMap(g, this.faceDrawer, this.posX, (this.posY - 2112) + (this.state == 3 ? 512 : 0));
            }
            if (this.state == 4) {
                drawInMap(g, this.boatdrawer, this.posX, this.posY);
                drawInMap(g, this.escapefacedrawer, this.posX, this.posY - Boss6Block.COLLISION2_HEIGHT);
            }
            if (this.isDisplayDrill && this.drill != null) {
                this.drill.draw(g);
            }
            if (this.state == 3 && this.bossbroken != null) {
                this.bossbroken.draw(g);
            }
            drawCollisionRect(g);
        }
    }

    public void refreshCollisionRect(int x, int y) {
        if (this.state != 3) {
            this.collisionRect.setRect(x - 2112, y - 3072, COLLISION_WIDTH, 3072);
        } else {
            this.collisionRect.setRect(x - 2112, y - 2560, COLLISION_WIDTH, 2560);
        }
    }
}
