package SonicGBA;

import GameEngine.Def;
import Lib.Animation;
import Lib.MyAPI;
import Lib.crlFP32;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: EnemyObject */
class Bee extends EnemyObject {
    private static final int BEE_BULLET_SPEED_ABS = 432;
    private static final int COLLISION_HEIGHT = 1536;
    private static final int COLLISION_WIDTH = 1536;
    private static final int HEIGHT_OFFSET = 16;
    private static final int STATE_ATTACK = 1;
    private static final int STATE_MOVE = 0;
    private static Animation beeAnimation;
    private int ALERT_HEIGHT = Def.TOUCH_SEL_1_W;
    private int ALERT_WIDTH = 128;
    private int ATTACK_MAX_SCALE = 373;
    private int ATTACK_MIN_SCALE = 36;
    private int alert_state;
    private int attack_base_range = -960;
    private int attack_cnt;
    private int attack_cnt_max = 35;
    private boolean attack_flag = false;
    private int bullset_v_x = 0;
    private int bullset_v_y = 0;
    private int enemyid;
    private int limitLeftX = this.posX;
    private int limitRightX = (this.posX + this.mWidth);
    private int state;
    private int velocity = 192;

    public static void releaseAllResource() {
        Animation.closeAnimation(beeAnimation);
        beeAnimation = null;
    }

    protected Bee(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y - 16, left, top, width, height);
        if (beeAnimation == null) {
            beeAnimation = new Animation("/animation/bee");
        }
        this.drawer = beeAnimation.getDrawer(1, true, 0);
        this.enemyid = id;
    }

    private int beeBulletVx(int distanceX, int distanceY) {
        return (((distanceX <= 0 ? -1 : 1) * BEE_BULLET_SPEED_ABS) * MyAPI.dSin(crlFP32.actTanDegree(Math.abs(distanceX), Math.abs(distanceY)))) / 100;
    }

    private int beeBulletVy(int distanceX, int distanceY) {
        return (((distanceY <= 0 ? -1 : 1) * BEE_BULLET_SPEED_ABS) * MyAPI.dCos(crlFP32.actTanDegree(Math.abs(distanceX), Math.abs(distanceY)))) / 100;
    }

    public void logic() {
        if (!this.dead) {
            this.alert_state = checkPlayerInEnemyAlertRange(this.posX >> 6, this.posY >> 6, this.ALERT_WIDTH, this.ALERT_HEIGHT);
            int preX = this.posX;
            int preY = this.posY;
            switch (this.state) {
                case 0:
                    if (this.velocity > 0) {
                        this.posX += this.velocity;
                        this.drawer.setActionId(0);
                        this.drawer.setTrans(2);
                        this.drawer.setLoop(true);
                        if (this.posX >= this.limitRightX) {
                            this.posX = this.limitRightX;
                            this.velocity = -this.velocity;
                            this.drawer.setActionId(1);
                            this.drawer.setTrans(0);
                            this.drawer.setLoop(false);
                            this.attack_flag = false;
                        }
                    } else {
                        this.posX += this.velocity;
                        this.drawer.setActionId(0);
                        this.drawer.setTrans(0);
                        this.drawer.setLoop(true);
                        if (this.posX <= this.limitLeftX) {
                            this.posX = this.limitLeftX;
                            this.velocity = -this.velocity;
                            this.drawer.setActionId(1);
                            this.drawer.setTrans(0);
                            this.drawer.setLoop(false);
                            this.attack_flag = false;
                        }
                    }
                    if (this.alert_state == 0 && this.posY + 1536 < player.getCheckPositionY() && !this.attack_flag && checkPlayerInEnemyAlertRangeScale(this.posX >> 6, this.posY >> 6, this.ATTACK_MIN_SCALE, this.ATTACK_MAX_SCALE) && IsFacePlayer()) {
                        if (this.posX < player.getCheckPositionX()) {
                            if (this.velocity > 0) {
                                this.drawer.setActionId(2);
                                this.drawer.setTrans(2);
                                this.drawer.setLoop(false);
                                this.state = 1;
                                this.attack_cnt = 0;
                                this.bullset_v_x = beeBulletVx(player.getCheckPositionX() - this.posX, player.getCheckPositionY() - this.posY);
                                this.bullset_v_y = beeBulletVy(player.getCheckPositionX() - this.posX, player.getCheckPositionY() - this.posY);
                            }
                        } else if (this.posX > player.getCheckPositionX() && this.velocity < 0) {
                            this.drawer.setActionId(2);
                            this.drawer.setTrans(0);
                            this.drawer.setLoop(false);
                            this.state = 1;
                            this.attack_cnt = 0;
                            this.bullset_v_x = beeBulletVx(player.getCheckPositionX() - this.posX, player.getCheckPositionY() - this.posY);
                            this.bullset_v_y = beeBulletVy(player.getCheckPositionX() - this.posX, player.getCheckPositionY() - this.posY);
                        }
                    }
                    checkWithPlayer(preX, preY, this.posX, this.posY);
                    return;
                case 1:
                    if (this.drawer.checkEnd()) {
                        BulletObject.addBullet(this.enemyid, this.posX, this.posY + 768, this.bullset_v_x, this.bullset_v_y);
                        this.state = 0;
                        this.attack_flag = true;
                    }
                    checkWithPlayer(preX, preY, this.posX, this.posY);
                    return;
                default:
                    return;
            }
        }
    }

    private boolean IsFacePlayer() {
        if ((this.posX <= player.getFootPositionX() || this.velocity >= 0) && (this.posX >= player.getFootPositionX() || this.velocity <= 0)) {
            return false;
        }
        return true;
    }

    public void draw(MFGraphics g) {
        if (!this.dead) {
            drawInMap(g, this.drawer);
            drawCollisionRect(g);
        }
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 768, y - 768, 1536, 1536);
    }
}
