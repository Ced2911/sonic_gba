package SonicGBA;

import Lib.MyAPI;
import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;

/* compiled from: BackGroundManager */
class BackManagerStageFinal extends BackGroundManager {
    private static MFImage bgImage;
    private int height;

    public BackManagerStageFinal() {
        if (bgImage == null) {
            bgImage = MFImage.createImage("/map/final_bg.png");
        }
        this.height = 20;
    }

    public void close() {
        bgImage = null;
    }

    public void draw(MFGraphics g) {
        g.setColor(0);
        MyAPI.fillRect(g, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
        int cameraX = MapManager.getCamera().f13x;
        int cameraY = MapManager.getCamera().f14y;
        MyAPI.drawImage(g, bgImage, (((-cameraX) / 43 < -12 ? -12 : (-cameraX) / 43) - 284) + SCREEN_WIDTH, ((-cameraY) / 18) + this.height > 0 ? 0 : ((-cameraY) / 18) + this.height, 0);
    }
}
