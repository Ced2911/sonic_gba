package SonicGBA;

import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;

/* compiled from: GimmickObject */
class HariIsland extends GimmickObject {
    private static MFImage image;
    private static MFImage image2;
    private int collisionHeight;
    private int collisionWidth;
    private int damageDirection;
    private boolean isH;
    private MoveCalculator mCalc;
    private MFImage thisImage;

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */

    //protected HariIsland(int r7, int r8, int r9, int r10, int r11, int r12, int r13) {
    protected HariIsland(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        /*
        r6 = this;
        r5 = 0;
        r4 = 1;
        r6.<init>(r7, r8, r9, r10, r11, r12, r13);
        r2 = SonicGBA.StageManager.getCurrentZoneId();	 Catch:{ Exception -> 0x0065 }
        switch(r2) {
            case 3: goto L_0x0051;
            case 4: goto L_0x000c;
            case 5: goto L_0x006b;
            default: goto L_0x000c;
        };
    L_0x000c:
        r2 = SonicGBA.StageManager.getCurrentZoneId();
        switch(r2) {
            case 3: goto L_0x0098;
            case 4: goto L_0x0013;
            case 5: goto L_0x009c;
            default: goto L_0x0013;
        };
    L_0x0013:
        r2 = image;
        r2 = r2.getWidth();
        r2 = Lib.MyAPI.zoomIn(r2);
        r2 = r2 << 6;
        r6.collisionWidth = r2;
        r2 = image;
        r2 = r2.getHeight();
        r2 = Lib.MyAPI.zoomIn(r2);
        r2 = r2 << 6;
        r6.collisionHeight = r2;
        r1 = 0;
        r2 = r6.mWidth;
        r3 = r6.mHeight;
        if (r2 < r3) goto L_0x00b0;
    L_0x0036:
        r6.isH = r4;
        r2 = r6.iLeft;
        if (r2 != 0) goto L_0x00a8;
    L_0x003c:
        r1 = 0;
    L_0x003d:
        r2 = new SonicGBA.MoveCalculator;
        r3 = r6.isH;
        if (r3 == 0) goto L_0x00ba;
    L_0x0043:
        r3 = r6.posX;
    L_0x0045:
        r4 = r6.isH;
        if (r4 == 0) goto L_0x00bd;
    L_0x0049:
        r4 = r6.mWidth;
    L_0x004b:
        r2.<init>(r3, r4, r1);
        r6.mCalc = r2;
        return;
    L_0x0051:
        r2 = image;	 Catch:{ Exception -> 0x0065 }
        if (r2 != 0) goto L_0x005d;
    L_0x0055:
        r2 = "/gimmick/hari_island_up_3.png";
        r2 = com.sega.mobile.framework.device.MFImage.createImage(r2);	 Catch:{ Exception -> 0x0065 }
        image = r2;	 Catch:{ Exception -> 0x0065 }
    L_0x005d:
        r2 = 1;
        r6.damageDirection = r2;	 Catch:{ Exception -> 0x0065 }
        r2 = image;	 Catch:{ Exception -> 0x0065 }
        r6.thisImage = r2;	 Catch:{ Exception -> 0x0065 }
        goto L_0x000c;
    L_0x0065:
        r2 = move-exception;
        r0 = r2;
        r0.printStackTrace();
        goto L_0x000c;
    L_0x006b:
        r2 = r6.iLeft;	 Catch:{ Exception -> 0x0065 }
        if (r2 != 0) goto L_0x0083;
    L_0x006f:
        r2 = image;	 Catch:{ Exception -> 0x0065 }
        if (r2 != 0) goto L_0x007b;
    L_0x0073:
        r2 = "/gimmick/hari_island_up_5.png";
        r2 = com.sega.mobile.framework.device.MFImage.createImage(r2);	 Catch:{ Exception -> 0x0065 }
        image = r2;	 Catch:{ Exception -> 0x0065 }
    L_0x007b:
        r2 = 1;
        r6.damageDirection = r2;	 Catch:{ Exception -> 0x0065 }
        r2 = image;	 Catch:{ Exception -> 0x0065 }
        r6.thisImage = r2;	 Catch:{ Exception -> 0x0065 }
        goto L_0x000c;
    L_0x0083:
        r2 = image2;	 Catch:{ Exception -> 0x0065 }
        if (r2 != 0) goto L_0x008f;
    L_0x0087:
        r2 = "/gimmick/hari_island_down_5.png";
        r2 = com.sega.mobile.framework.device.MFImage.createImage(r2);	 Catch:{ Exception -> 0x0065 }
        image2 = r2;	 Catch:{ Exception -> 0x0065 }
    L_0x008f:
        r2 = 0;
        r6.damageDirection = r2;	 Catch:{ Exception -> 0x0065 }
        r2 = image2;	 Catch:{ Exception -> 0x0065 }
        r6.thisImage = r2;	 Catch:{ Exception -> 0x0065 }
        goto L_0x000c;
    L_0x0098:
        r6.damageDirection = r4;
        goto L_0x0013;
    L_0x009c:
        r2 = r6.iLeft;
        if (r2 != 0) goto L_0x00a4;
    L_0x00a0:
        r6.damageDirection = r4;
        goto L_0x0013;
    L_0x00a4:
        r6.damageDirection = r5;
        goto L_0x0013;
    L_0x00a8:
        r1 = 1;
        r2 = r6.mWidth;
        r2 = r2 + 64;
        r6.mWidth = r2;
        goto L_0x003d;
    L_0x00b0:
        r6.isH = r5;
        r2 = r6.iTop;
        if (r2 != 0) goto L_0x00b8;
    L_0x00b6:
        r1 = 0;
        goto L_0x003d;
    L_0x00b8:
        r1 = 1;
        goto L_0x003d;
    L_0x00ba:
        r3 = r6.posY;
        goto L_0x0045;
    L_0x00bd:
        r4 = r6.mHeight;
        goto L_0x004b;
        */
        throw new UnsupportedOperationException("Method not decompiled: SonicGBA.HariIsland.<init>(int, int, int, int, int, int, int):void");
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - (this.collisionWidth >> 1), y - (this.collisionHeight >> 1), this.collisionWidth, this.collisionHeight);
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        object.beStop(0, direction, this);
        if (direction == this.damageDirection) {
            object.beHurt();
        }
        if (player.isFootOnObject(this) && this.worldInstance.getWorldY(object.collisionRect.x0, object.collisionRect.y0, 1, 0) != -1000 && this.worldInstance.getWorldY(object.collisionRect.x1, object.collisionRect.y0, 1, 0) != -1000) {
            object.setDie(false);
        }
    }

    public void draw(MFGraphics g) {
        drawInMap(g, this.thisImage, 3);
    }

    public void logic() {
        this.mCalc.logic();
        int preX = this.posX;
        int preY = this.posY;
        if (this.isH) {
            this.posX = this.mCalc.getPosition();
        } else {
            this.posY = this.mCalc.getPosition();
        }
        checkWithPlayer(preX, preY, this.posX, this.posY);
    }

    public void close() {
        this.thisImage = null;
        this.mCalc = null;
    }

    public static void releaseAllResource() {
        image = null;
        image2 = null;
    }

    public int getPaintLayer() {
        return 0;
    }
}
