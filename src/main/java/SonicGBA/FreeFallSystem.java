package SonicGBA;

import GameEngine.Key;
import Lib.Coordinate;
import Lib.SoundSystem;
import com.sega.mobile.framework.device.MFGamePad;

/* compiled from: GimmickObject */
class FreeFallSystem extends GimmickObject {
    private static final int MOVE_DISTANCE = 30720;
    private static final int MOVE_VELOCITY = 320;
    private static final int SHOOT_TIME = 80;
    private FreeFallBar bar;
    private int barOriginalPosX;
    private int barOriginalPosY;
    private int frame;
    public boolean initFlag;
    private boolean isActive;
    public boolean moving;
    private int plaOriginalPosX;
    private int plaOriginalPosY;
    private FreeFallPlatform platform;
    private int posYOriginal;
    private Coordinate position;
    public boolean releaseAble;
    private int shootCnt;
    public boolean shootDirection;

    protected FreeFallSystem(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        this.posYOriginal = 0;
        this.posYOriginal = this.posY;
        this.position = new Coordinate();
        this.position.f13x = this.posX;
        this.position.f14y = this.posY;
        this.moving = false;
        this.releaseAble = false;
        this.bar = new FreeFallBar(this, this.posX, this.posY);
        this.platform = new FreeFallPlatform(this, this.posX, this.posY);
        GameObject.addGameObject(this.bar, this.posX, this.posY);
        GameObject.addGameObject(this.platform, this.posX, this.posY);
        this.plaOriginalPosX = this.posX;
        this.plaOriginalPosY = this.posY;
        this.barOriginalPosX = this.posX;
        this.barOriginalPosY = this.posY;
        this.initFlag = false;
        this.frame = 0;
        this.isActive = false;
        this.shootCnt = 0;
        this.shootDirection = false;
    }

    public Coordinate getBarPosition() {
        return this.position;
    }

    public Coordinate getPlatformPosition() {
        return this.position;
    }

    public void logic() {
        if (this.initFlag) {
            refreshCollisionRect(this.posX, this.posY);
            if (!screenRect.collisionChk(this.bar.collisionRect) && !screenRect.collisionChk(this.platform.collisionRect)) {
                this.initFlag = false;
                return;
            }
            return;
        }
        if (this.moving) {
            if (!IsGamePause) {
                this.isActive = true;
            }
            this.posY += MOVE_VELOCITY;
            if (this.posY >= this.posYOriginal + MOVE_DISTANCE) {
                this.posY = this.posYOriginal + MOVE_DISTANCE;
                this.releaseAble = true;
            }
            this.position.f14y = this.posY;
            refreshCollisionRect(this.posX, this.posY);
        }
        this.bar.barLogic();
        this.platform.platformLogic();
        if (this.releaseAble) {
            this.shootCnt++;
            if (this.shootCnt < 80) {
                if (Key.press(Key.gLeft)) {
                    player.changeVisible(true);
                    player.outOfControl = false;
                    player.setVelX(-2000);
                    player.faceDirection = false;
                    this.moving = false;
                    this.frame = 0;
                    this.releaseAble = false;
                    player.setAnimationId(3);
                    player.restartAniDrawer();
                } else if (Key.press(Key.gRight)) {
                    player.changeVisible(true);
                    player.outOfControl = false;
                    player.setVelX(2000);
                    this.moving = false;
                    this.frame = 0;
                    this.releaseAble = false;
                    player.faceDirection = true;
                    player.setAnimationId(3);
                    player.restartAniDrawer();
                }
            } else if (this.shootDirection) {
                player.changeVisible(true);
                player.outOfControl = false;
                player.setVelX(2000);
                this.moving = false;
                this.frame = 0;
                this.releaseAble = false;
                player.faceDirection = true;
                player.setAnimationId(3);
                player.restartAniDrawer();
            } else {
                player.changeVisible(true);
                player.outOfControl = false;
                player.setVelX(-2000);
                player.faceDirection = false;
                this.moving = false;
                this.frame = 0;
                this.releaseAble = false;
                player.setAnimationId(3);
                player.restartAniDrawer();
            }
        }
        if (this.moving && !IsGamePause) {
            this.frame++;
            if (this.frame <= 32) {
                if (this.frame % 7 == 0) {
                    SoundSystem.getInstance().playSe(73);
                }
            } else if (this.frame <= 64) {
                if (this.frame % 4 == 0) {
                    SoundSystem.getInstance().playSe(74);
                }
            } else if (this.frame % 2 == 0) {
                SoundSystem.getInstance().playSe(75);
            }
        }
        if (this.posY != this.posYOriginal + MOVE_DISTANCE) {
            return;
        }
        if (player.posX - this.posX > MFGamePad.KEY_NUM_8 || this.posX - player.posX > MFGamePad.KEY_NUM_8 || player.posY - this.posY > 9216 || this.posY - player.posY > 9216) {
            doInitInCamera();
        }
    }

    public boolean isSystemReady() {
        return this.posY == this.posYOriginal;
    }

    public void doWhileNoCollision() {
        if (this.isActive) {
            this.shootCnt = 0;
            this.isActive = false;
        }
    }

    public void close() {
        this.bar = null;
        this.platform = null;
        this.position = null;
    }

    public void doInitInCamera() {
        this.posX = this.plaOriginalPosX;
        this.posY = this.plaOriginalPosY;
        this.position.f13x = this.posX;
        this.position.f14y = this.posY;
        this.initFlag = false;
        this.bar.init();
        this.platform.init();
    }

    public static void releaseAllResource() {
    }

    public void refreshCollisionRect(int x, int y) {
    }
}
