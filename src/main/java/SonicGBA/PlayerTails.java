package SonicGBA;

import GameEngine.Key;
import Lib.Animation;
import Lib.AnimationDrawer;
import Lib.Coordinate;
import com.sega.engine.lib.CrlFP32;
import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;

public class PlayerTails extends PlayerObject {
    private static final int[] ANIMATION_CONVERT;
    private static final int FLY_GRAVITY = 30;
    private static final int FLY_POWER = -90;
    private static final int FLY_POWER_COUNT = 8;
    private static final int FLY_TIME = 128;
    private static final int LOOP = -1;
    private static final int[] LOOP_INDEX;
    private static final int MAX_FLY_VEL_Y = -176;
    private static final int NO_ANIMATION = -1;
    private static final int NO_LOOP = -2;
    private static final int NO_LOOP_DEPAND = -2;
    public static final int TAILS_ANI_ATTACK = 11;
    public static final int TAILS_ANI_BANK_1 = 31;
    public static final int TAILS_ANI_BANK_2 = 32;
    public static final int TAILS_ANI_BANK_3 = 33;
    public static final int TAILS_ANI_BAR_MOVE = 42;
    public static final int TAILS_ANI_BAR_STAY = 41;
    public static final int TAILS_ANI_BRAKE = 46;
    public static final int TAILS_ANI_BREATHE = 52;
    public static final int TAILS_ANI_CAUGHT = 47;
    public static final int TAILS_ANI_CELEBRATE_1 = 27;
    public static final int TAILS_ANI_CELEBRATE_2 = 28;
    public static final int TAILS_ANI_CELEBRATE_3 = 29;
    public static final int TAILS_ANI_CELEBRATE_4 = 30;
    public static final int TAILS_ANI_CLIFF_1 = 25;
    public static final int TAILS_ANI_CLIFF_2 = 26;
    public static final int TAILS_ANI_DEAD_1 = 17;
    public static final int TAILS_ANI_DEAD_2 = 18;
    public static final int TAILS_ANI_ENTER_SP = 51;
    public static final int TAILS_ANI_FLY_1 = 12;
    public static final int TAILS_ANI_FLY_2 = 13;
    public static final int TAILS_ANI_FLY_3 = 14;
    public static final int TAILS_ANI_HURT_1 = 15;
    public static final int TAILS_ANI_HURT_2 = 16;
    public static final int TAILS_ANI_JUMP_BODY = 5;
    public static final int TAILS_ANI_JUMP_TAIL = 6;
    public static final int TAILS_ANI_LOOK_UP_1 = 7;
    public static final int TAILS_ANI_LOOK_UP_2 = 8;
    public static final int TAILS_ANI_POLE_H = 36;
    public static final int TAILS_ANI_POLE_V = 35;
    public static final int TAILS_ANI_PUSH_WALL = 19;
    public static final int TAILS_ANI_RAIL_BODY = 44;
    public static final int TAILS_ANI_RAIL_TAIL = 45;
    public static final int TAILS_ANI_ROLL_H_1 = 39;
    public static final int TAILS_ANI_ROLL_H_2 = 40;
    public static final int TAILS_ANI_ROLL_V_1 = 37;
    public static final int TAILS_ANI_ROLL_V_2 = 38;
    public static final int TAILS_ANI_RUN = 3;
    public static final int TAILS_ANI_SPIN = 4;
    public static final int TAILS_ANI_SPRING_1 = 20;
    public static final int TAILS_ANI_SPRING_2 = 21;
    public static final int TAILS_ANI_SPRING_3 = 22;
    public static final int TAILS_ANI_SPRING_4 = 23;
    public static final int TAILS_ANI_SPRING_5 = 24;
    public static final int TAILS_ANI_SQUAT_1 = 9;
    public static final int TAILS_ANI_SQUAT_2 = 10;
    public static final int TAILS_ANI_STAND = 0;
    public static final int TAILS_ANI_SWIM_1 = 48;
    public static final int TAILS_ANI_SWIM_2 = 49;
    public static final int TAILS_ANI_UP_ARM = 34;
    public static final int TAILS_ANI_VS_KNUCKLE = 50;
    public static final int TAILS_ANI_WAITING_1 = 53;
    public static final int TAILS_ANI_WAITING_2 = 54;
    public static final int TAILS_ANI_WALK_1 = 1;
    public static final int TAILS_ANI_WALK_2 = 2;
    public static final int TAILS_ANI_WIND = 43;
    public static boolean isInWind = false;
    private PlayerAnimationCollisionRect attackRect;
    public int flyCount = 0;
    private int flyUpCoolCount;
    private AnimationDrawer tailDrawer;
    private AnimationDrawer tailsDrawer1;
    private AnimationDrawer tailsDrawer2;

    static {
        int[] iArr = new int[54];
        iArr[1] = 1;
        iArr[2] = 2;
        iArr[3] = 3;
        iArr[4] = 5;
        iArr[5] = 10;
        iArr[6] = 4;
        iArr[7] = 4;
        iArr[8] = 19;
        iArr[9] = 20;
        iArr[10] = 24;
        iArr[11] = -1;
        iArr[12] = 16;
        iArr[13] = 35;
        iArr[14] = 21;
        iArr[15] = -1;
        iArr[16] = -1;
        iArr[17] = 46;
        iArr[18] = -1;
        iArr[19] = -1;
        iArr[20] = -1;
        iArr[21] = 44;
        iArr[22] = 39;
        iArr[23] = 40;
        iArr[24] = 34;
        iArr[25] = 37;
        iArr[26] = 38;
        iArr[27] = 41;
        iArr[28] = 42;
        iArr[29] = 43;
        iArr[30] = 16;
        iArr[31] = 36;
        iArr[32] = 31;
        iArr[33] = 32;
        iArr[34] = 33;
        iArr[35] = 27;
        iArr[36] = 28;
        iArr[37] = 29;
        iArr[38] = 7;
        iArr[39] = 8;
        iArr[40] = 7;
        iArr[41] = 18;
        iArr[42] = 22;
        iArr[43] = 23;
        iArr[44] = 16;
        iArr[45] = 17;
        iArr[46] = 9;
        iArr[47] = 25;
        iArr[48] = 26;
        iArr[49] = 52;
        iArr[50] = 53;
        iArr[51] = 54;
        iArr[52] = 47;
        iArr[53] = 50;
        ANIMATION_CONVERT = iArr;
        iArr = new int[56];
        iArr[0] = -1;
        iArr[1] = -1;
        iArr[2] = -1;
        iArr[3] = -1;
        iArr[4] = -1;
        iArr[5] = -1;
        iArr[6] = -1;
        iArr[7] = 8;
        iArr[8] = -1;
        iArr[9] = -2;
        iArr[10] = -1;
        iArr[12] = -1;
        iArr[13] = 14;
        iArr[14] = -1;
        iArr[15] = 16;
        iArr[16] = -1;
        iArr[17] = 18;
        iArr[18] = -1;
        iArr[19] = -1;
        iArr[20] = 23;
        iArr[21] = -1;
        iArr[22] = 23;
        iArr[23] = 24;
        iArr[24] = -1;
        iArr[25] = -1;
        iArr[26] = -1;
        iArr[27] = 28;
        iArr[28] = -1;
        iArr[29] = 30;
        iArr[30] = -1;
        iArr[31] = -1;
        iArr[32] = -1;
        iArr[33] = -1;
        iArr[34] = -1;
        iArr[35] = 21;
        iArr[36] = 3;
        iArr[37] = 38;
        iArr[38] = 37;
        iArr[39] = 40;
        iArr[40] = 39;
        iArr[41] = -1;
        iArr[42] = -1;
        iArr[43] = -1;
        iArr[44] = -1;
        iArr[45] = -1;
        iArr[46] = -1;
        iArr[47] = -1;
        iArr[48] = 49;
        iArr[49] = -1;
        iArr[50] = -1;
        iArr[51] = -2;
        iArr[52] = -2;
        iArr[53] = 54;
        iArr[54] = -1;
        LOOP_INDEX = iArr;
    }

    public PlayerTails() {
        MFImage tailsImage = MFImage.createImage("/animation/player/chr_tails.png");
        Animation animation1 = new Animation(tailsImage, "/animation/player/chr_tails_01");
        this.tailsDrawer1 = animation1.getDrawer();
        this.tailDrawer = animation1.getDrawer();
        this.drawer = this.tailsDrawer1;
        this.tailsDrawer2 = new Animation(tailsImage, "/animation/player/chr_tails_02").getDrawer();
        this.attackRect = new PlayerAnimationCollisionRect(this);
    }

    public void closeImpl() {
        Animation.closeAnimationDrawer(this.tailsDrawer1);
        this.tailsDrawer1 = null;
        Animation.closeAnimationDrawer(this.tailDrawer);
        this.tailDrawer = null;
        Animation.closeAnimationDrawer(this.tailsDrawer2);
        this.tailsDrawer2 = null;
    }

    public void drawCharacter(MFGraphics g) {
        Coordinate camera = MapManager.getCamera();
        int preMyAnimationID = this.myAnimationID;
        if (this.animationID != -1) {
            this.myAnimationID = ANIMATION_CONVERT[this.animationID];
        }
        if (preMyAnimationID == 30 && this.myAnimationID == 29) {
            this.myAnimationID = 30;
        }
        if (this.myAnimationID != -1) {
            boolean loop;
            if (LOOP_INDEX[this.myAnimationID] == -1) {
                loop = true;
            } else {
                loop = false;
            }
            if (this.myAnimationID == 50) {
                loop = false;
            }
            this.drawer = this.tailsDrawer1;
            int drawerActionID = this.myAnimationID;
            if (this.myAnimationID >= 48) {
                this.drawer = this.tailsDrawer2;
                drawerActionID = this.myAnimationID - 48;
                if (this.myAnimationID == 53 && this.isResetWaitAni) {
                    this.drawer.restart();
                    this.isResetWaitAni = false;
                }
            }
            if (this.isInWater) {
                this.drawer.setSpeed(1, 2);
                this.tailDrawer.setSpeed(1, 2);
            } else {
                this.drawer.setSpeed(1, 1);
                this.tailDrawer.setSpeed(1, 1);
            }
            if (this.hurtCount % 2 == 0) {
                drawTail(g);
                int bodyCenterX;
                int bodyCenterY;
                int trans;
                if (this.animationID == 4) {
                    bodyCenterX = getNewPointX(this.footPointX, 0, -512, this.faceDegree);
                    bodyCenterY = getNewPointY(this.footPointY, 0, -512, this.faceDegree);
                    int drawX = getNewPointX(bodyCenterX, 0, 512, 0);
                    int drawY = getNewPointY(bodyCenterY, 0, 512, 0);
                    if (this.collisionState == (byte) 0) {
                        if (this.isAntiGravity) {
                            if (this.faceDirection) {
                                trans = this.totalVelocity >= 0 ? 3 : 1;
                                drawY -= 1024;
                            } else {
                                trans = this.totalVelocity > 0 ? 3 : 1;
                                drawY -= 1024;
                            }
                        } else if (this.faceDirection) {
                            trans = this.totalVelocity >= 0 ? 0 : 2;
                        } else {
                            trans = this.totalVelocity > 0 ? 0 : 2;
                        }
                    } else if (this.isAntiGravity) {
                        if (this.faceDirection) {
                            trans = this.velX <= 0 ? 3 : 1;
                            drawY -= 1024;
                        } else {
                            trans = this.velX < 0 ? 3 : 1;
                            drawY -= 1024;
                        }
                    } else if (this.faceDirection) {
                        trans = this.velX >= 0 ? 0 : 2;
                    } else {
                        trans = this.velX > 0 ? 0 : 2;
                    }
                    this.drawer.draw(g, drawerActionID, (drawX >> 6) - camera.f13x, (drawY >> 6) - camera.f14y, loop, trans);
                } else if (this.animationID == 6 || this.animationID == 7) {
                    drawDrawerByDegree(g, this.drawer, drawerActionID, (this.footPointX >> 6) - camera.f13x, (this.footPointY >> 6) - camera.f14y, loop, this.degreeForDraw, !this.faceDirection);
                } else {
                    if (this.myAnimationID == 25 || this.myAnimationID == 26 || this.myAnimationID == 7 || this.myAnimationID == 8) {
                        this.degreeForDraw = this.degreeStable;
                        this.faceDegree = this.degreeStable;
                    }
                    if (this.myAnimationID == 46) {
                        this.degreeForDraw = this.degreeStable;
                    }
                    if (!(this.myAnimationID == 1 || this.myAnimationID == 2 || this.myAnimationID == 3 || this.myAnimationID == 47)) {
                        this.degreeForDraw = this.degreeStable;
                    }
                    if (this.fallinSandSlipState != 0) {
                        if (this.fallinSandSlipState == 1) {
                            this.faceDirection = true;
                        } else if (this.fallinSandSlipState == 2) {
                            this.faceDirection = false;
                        }
                    }
                    if (this.faceDirection) {
                        trans = 0;
                    } else {
                        trans = 2;
                    }
                    if (this.degreeForDraw != this.faceDegree) {
                        bodyCenterX = getNewPointX(this.footPointX, 0, (-this.collisionRect.getHeight()) >> 1, this.faceDegree);
                        bodyCenterY = getNewPointY(this.footPointY, 0, (-this.collisionRect.getHeight()) >> 1, this.faceDegree);
                        g.saveCanvas();
                        g.translateCanvas((bodyCenterX >> 6) - camera.f13x, (bodyCenterY >> 6) - camera.f14y);
                        g.rotateCanvas((float) this.degreeForDraw);
                        this.drawer.draw(g, drawerActionID, 0, (this.collisionRect.getHeight() >> 1) >> 6, loop, trans);
                        g.restoreCanvas();
                    } else {
                        drawDrawerByDegree(g, this.drawer, drawerActionID, (this.footPointX >> 6) - camera.f13x, (this.footPointY >> 6) - camera.f14y, loop, this.degreeForDraw, !this.faceDirection);
                    }
                }
            } else {
                if (drawerActionID != this.drawer.getActionId()) {
                    this.drawer.setActionId(drawerActionID);
                }
                if (!AnimationDrawer.isAllPause()) {
                    this.drawer.moveOn();
                    this.tailDrawer.moveOn();
                }
            }
            this.attackRectVec.removeAllElements();
            byte[] rect = this.drawer.getARect();
            if (this.isAntiGravity) {
                byte[] rectTmp = this.drawer.getARect();
                if (rectTmp != null) {
                    rect[0] = (byte) ((-rectTmp[0]) - rectTmp[2]);
                }
            }
            if (rect != null) {
                if (SonicDebug.showCollisionRect) {
                    g.setColor(65280);
                    g.drawRect(((this.footPointX >> 6) + rect[0]) - camera.f13x, ((this.footPointY >> 6) + (this.isAntiGravity ? (-rect[1]) - rect[3] : rect[1])) - camera.f14y, rect[2], rect[3]);
                }
                this.attackRect.initCollision(rect[0] << 6, rect[1] << 6, rect[2] << 6, rect[3] << 6, this.myAnimationID);
                this.attackRectVec.addElement(this.attackRect);
            } else {
                this.attackRect.reset();
            }
            if (this.myAnimationID == 50 && this.drawer.checkEnd()) {
                this.animationID = 0;
            } else if (this.drawer.checkEnd() && LOOP_INDEX[this.myAnimationID] >= 0) {
                if (this.animationID == -1) {
                    switch (this.myAnimationID) {
                        case 11:
                            this.animationID = 0;
                            this.isAttacking = false;
                            break;
                    }
                }
                this.myAnimationID = LOOP_INDEX[this.myAnimationID];
            }
        }
    }

    public int getGravity() {
        if (this.flyCount == 0) {
            return super.getGravity();
        }
        return 0;
    }

    public int getGravity2() {
        return super.getGravity();
    }

    protected void extraLogicJump() {
        int i;
        int i2;
        if (this.myAnimationID == 13 || this.myAnimationID == 14) {
            this.velY += (this.isAntiGravity ? -1 : 1) * 30;
             i2 = this.velY;
            if (this.isAntiGravity) {
                i = -1;
            } else {
                i = 1;
            }
            this.velY = i2 - (i * getGravity());
        }
        if (this.myAnimationID == 12 || this.myAnimationID == 48 || this.myAnimationID == 49) {
            this.flyCount--;
            if (this.flyUpCoolCount == 1) {
                if (Key.press(16777216) && (((!this.isAntiGravity && this.velY >= MAX_FLY_VEL_Y) || (this.isAntiGravity && this.velY <= 176)) && this.flyCount > 0)) {
                    this.flyUpCoolCount = 2;
                }
                this.velY += (this.isAntiGravity ? -1 : 1) * 30;
            } else if ((this.isAntiGravity || this.velY < MAX_FLY_VEL_Y) && (!this.isAntiGravity || this.velY > 176)) {
                this.flyUpCoolCount = 1;
            } else {
                if (this.myAnimationID == 48 || this.myAnimationID == 49) {
                    i2 = this.velY;
                    if (this.isAntiGravity) {
                        i = -1;
                    } else {
                        i = 1;
                    }
                    this.velY = i2 + (i * -450);
                } else {
                    this.velY += (this.isAntiGravity ? -1 : 1) * FLY_POWER;
                }
                this.flyUpCoolCount++;
                if (this.flyUpCoolCount == 8) {
                    this.flyUpCoolCount = 1;
                }
            }
            i2 = this.velY;
            if (this.isAntiGravity) {
                i = -1;
            } else {
                i = 1;
            }
            this.velY = i2 - (i * getGravity());
            if (this.isInWater && this.myAnimationID == 12) {
                this.myAnimationID = 48;
            } else if (!this.isInWater) {
                this.myAnimationID = 12;
            }
            if (this.flyCount == 0) {
                this.myAnimationID = 13;
                soundInstance.stopLoopSe();
            } else if (!this.isInWater && !isInWind && !this.isCrashFallingSand && !IsGamePause) {
                soundInstance.playLoopSe(15);
            }
        } else if (this.animationID != 4) {
        } else {
            if ((this.doJumpForwardly || this.isCrashFallingSand) && Key.press(16777216)) {
                this.animationID = -1;
                this.myAnimationID = 12;
                if (this.isInWater) {
                    this.myAnimationID = 48;
                } else {
                    soundInstance.playLoopSe(15);
                }
                this.flyCount = 128;
                this.flyUpCoolCount = 1;
                i2 = this.velY;
                if (this.isAntiGravity) {
                    i = -1;
                } else {
                    i = 1;
                }
                this.velY = i2 + (i * getGravity2());
                this.velY = (this.velY * 13) / 16;
                i2 = this.velY;
                if (this.isAntiGravity) {
                    i = -1;
                } else {
                    i = 1;
                }
                this.velY = i2 - (i * getGravity());
                i2 = this.velY;
                if (this.isAntiGravity) {
                    i = -1;
                } else {
                    i = 1;
                }
                this.velY = i2 + (i * 30);
            }
        }
    }

    public void resetFlyCount() {
        this.flyCount = 0;
    }

    public boolean flyState() {
        return this.flyCount > 0;
    }

    protected void extraLogicWalk() {
        if (this.flyCount > 0) {
            soundInstance.stopLoopSe();
            this.flyCount = 0;
        }
        if (Key.press(Key.gSelect) && this.myAnimationID != 11 && this.myAnimationID != 19 && this.collisionState != (byte) 1 && this.animationID != 4) {
            this.animationID = -1;
            this.myAnimationID = 11;
            this.drawer.restart();
            soundInstance.playSe(16);
            this.isAttacking = true;
        }
    }

    protected void extraLogicOnObject() {
        extraLogicWalk();
    }

    public void stopFly() {
        soundInstance.stopLoopSe();
        this.flyCount = 0;
    }

    private void drawTail(MFGraphics g) {
        boolean mirror;
        int tailID = -1;
        int bodyCenterX = getNewPointX(this.footPointX, 0, -512, this.faceDegree);
        int bodyCenterY = getNewPointY(this.footPointY, 0, -512, this.faceDegree);
        if (this.myAnimationID == 5) {
            tailID = 6;
        } else if (this.myAnimationID == 44) {
            tailID = 45;
        }
        int tailDegree = this.faceDegree;
        int trans = getTrans(tailDegree);
        if (this.faceDirection) {
            mirror = false;
        } else {
            mirror = true;
        }
        boolean preFaceDirection = this.faceDirection;
        if (this.animationID == 4) {
            if (this.collisionState == (byte) 0) {
                if (!this.faceDirection) {
                    mirror = this.totalVelocity <= 0;
                } else if (this.totalVelocity < 0) {
                    mirror = true;
                } else {
                    mirror = false;
                }
            } else if (this.faceDirection) {
                mirror = this.velX < 0;
            } else {
                mirror = this.velX <= 0;
            }
        }
        if (this.collisionState == (byte) 1 || this.collisionState == (byte) 3 || ((this.piping && this.pipeState == (byte) 1) || this.myAnimationID == 44)) {
            tailDegree = CrlFP32.actTanDegree(this.velY, this.velX);
            trans = TRANS[getTransId(tailDegree)];
            mirror = false;
            this.faceDirection = true;
        }
        if (tailID != -1) {
            drawDrawerByDegree(g, this.tailDrawer, tailID, (bodyCenterX >> 6) - camera.f13x, (bodyCenterY >> 6) - camera.f14y, true, tailDegree, mirror);
        }
        this.faceDirection = preFaceDirection;
    }

    public boolean doPoalMotion(int x, int y, boolean isLeft) {
        if (this.myAnimationID == 12 || this.myAnimationID == 13 || this.myAnimationID == 14) {
            return false;
        }
        return super.doPoalMotion(x, y, isLeft);
    }

    public boolean needRetPower() {
        if (this.myAnimationID == 11) {
            return true;
        }
        return super.needRetPower();
    }

    public int getRetPower() {
        if (this.myAnimationID == 11) {
            return MOVE_POWER_REVERSE >> 1;
        }
        return super.getRetPower();
    }

    public boolean noRotateDraw() {
        if (this.myAnimationID == 11) {
            return true;
        }
        return super.noRotateDraw();
    }

    public boolean canDoJump() {
        if (this.myAnimationID == 11) {
            return false;
        }
        return super.canDoJump();
    }
}
