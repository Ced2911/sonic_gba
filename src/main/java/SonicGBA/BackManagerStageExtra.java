package SonicGBA;

import Lib.MyAPI;
import Special.SSDef;
import State.StringIndex;
import com.sega.mobile.define.MDPhone;
import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;

/* compiled from: BackGroundManager */
class BackManagerStageExtra extends BackGroundManager {
    private static final int[][] DIVIDE_PARAM = null;
    private static final int[] DIVIDE_VELOCITY = new int[12];
    private static MFImage bgImage;
    private int[] xOffset = new int[DIVIDE_VELOCITY.length];

    /*
    static {
        r0 = new int[12][];
        int[] iArr = new int[4];
        iArr[2] = 284;
        iArr[3] = 91;
        r0[0] = iArr;
        int[] iArr2 = new int[]{91, 256, 10, iArr2};
        iArr2 = new int[]{101, 256, 5, iArr2};
        iArr2 = new int[]{106, 256, 7, iArr2};
        iArr2 = new int[]{StringIndex.STR_RIGHT_ARROW, 256, 15, iArr2};
        iArr = new int[]{128, 256, 2, iArr};
        iArr = new int[]{130, 256, 3, iArr};
        iArr = new int[]{133, 256, 4, iArr};
        iArr = new int[]{137, 256, 5, iArr};
        iArr = new int[]{142, 256, 5, iArr};
        iArr = new int[]{StringIndex.STR_RESET_RECORD_COMFIRM, 256, 6, iArr};
        iArr = new int[]{153, 256, 7, iArr};
        DIVIDE_PARAM = r0;
        int[] iArr3 = new int[12];
        iArr3[1] = 15;
        iArr3[2] = 30;
        iArr3[3] = 60;
        iArr3[4] = 120;
        iArr3[5] = SSDef.PLAYER_MOVE_HEIGHT;
        iArr3[6] = MDPhone.SCREEN_WIDTH;
        iArr3[7] = 480;
        iArr3[8] = 600;
        iArr3[9] = 720;
        iArr3[10] = 840;
        iArr3[11] = 960;
        DIVIDE_VELOCITY = iArr3;
    }
    */

    public BackManagerStageExtra() {
        bgImage = MFImage.createImage("/map/ex_bg_1.png");
    }

    public void close() {
        bgImage = null;
        this.xOffset = null;
    }

    public void draw(MFGraphics g) {
        g.setColor(0);
        MyAPI.fillRect(g, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
        for (int i = 0; i < DIVIDE_VELOCITY.length; i++) {
            for (int j = 0; j < 4; j++) {
                MyAPI.drawImage(g, bgImage, DIVIDE_PARAM[i][0], DIVIDE_PARAM[i][1], DIVIDE_PARAM[i][2], DIVIDE_PARAM[i][3], 0, ((j - 1) * 256) + (((-this.xOffset[i]) / 256) + (SCREEN_WIDTH / 2)), DIVIDE_PARAM[i][1], 17);
            }
            if (!GameObject.IsGamePause) {
                int[] iArr = this.xOffset;
                iArr[i] = iArr[i] + DIVIDE_VELOCITY[i];
                iArr = this.xOffset;
                iArr[i] = iArr[i] % 65536;
            }
        }
    }
}
