package SonicGBA;

import Lib.Animation;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: EnemyObject */
class Monkey extends EnemyObject {
    private static final int ALERT_RANGE = 9216;
    private static final int COLLISION_HEIGHT = 1280;
    private static final int COLLISION_WIDTH = 1536;
    private static final int STATE_ATTACK = 1;
    private static final int STATE_CLIMB = 0;
    private static Animation monkeyAnimation;
    private int ALERT_HEIGHT = 160;
    private int ALERT_WIDTH = 256;
    private int BOMB_MAX_SPEED_X = PlayerSonic.BACK_JUMP_SPEED_X;
    private int alert_state;
    private int attack_cnt = 0;
    private int enemyid;
    private int limitBottomY;
    private int limitTopY;
    private int posXl;
    private int posXr;
    private int state;
    private int velocity = 320;

    public static void releaseAllResource() {
        Animation.closeAnimation(monkeyAnimation);
        monkeyAnimation = null;
    }

    protected Monkey(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        this.posY -= 1024;
        this.posXl = this.posX;
        this.posXr = this.posX - (this.velocity * 2);
        this.limitTopY = this.posY;
        this.limitBottomY = this.posY + this.mHeight;
        if (monkeyAnimation == null) {
            monkeyAnimation = new Animation("/animation/monkey");
        }
        this.drawer = monkeyAnimation.getDrawer(0, true, 0);
        this.enemyid = id;
    }

    public void logic() {
        if (!this.dead) {
            this.alert_state = checkPlayerInEnemyAlertRange(this.posX >> 6, this.posY >> 6, this.ALERT_WIDTH, this.ALERT_HEIGHT);
            int preX = this.posX;
            int preY = this.posY;
            if (this.posX < player.getCheckPositionX()) {
                this.posX = this.posXr;
            } else {
                this.posX = this.posXl;
            }
            preX = this.posX;
            switch (this.state) {
                case 0:
                    if (this.alert_state == 0 || this.alert_state == 1) {
                        if (this.posX < player.getCheckPositionX()) {
                            this.drawer.setTrans(2);
                        } else {
                            this.drawer.setTrans(0);
                        }
                    }
                    if (this.velocity > 0) {
                        this.posY += this.velocity;
                        if (this.posY >= this.limitBottomY) {
                            this.posY = this.limitBottomY;
                            this.velocity = -this.velocity;
                        }
                    } else {
                        this.posY += this.velocity;
                        if (this.posY <= this.limitTopY) {
                            this.posY = this.limitTopY;
                            this.velocity = -this.velocity;
                        }
                    }
                    if (this.posY == this.limitTopY && this.alert_state == 0) {
                        this.attack_cnt++;
                        if (this.attack_cnt == 2) {
                            this.state = 1;
                            this.attack_cnt = 0;
                        }
                    }
                    checkWithPlayer(preX, preY, this.posX, this.posY);
                    return;
                case 1:
                    this.drawer.setActionId(1);
                    this.drawer.setLoop(false);
                    if (this.posX < player.getCheckPositionX()) {
                        this.drawer.setTrans(2);
                    } else {
                        this.drawer.setTrans(0);
                    }
                    if (this.drawer.checkEnd()) {
                        int bomb_speed_x = (-(this.posX - player.getCheckPositionX())) / 12;
                        if (Math.abs(bomb_speed_x) > this.BOMB_MAX_SPEED_X) {
                            if (bomb_speed_x > 0) {
                                bomb_speed_x = this.BOMB_MAX_SPEED_X;
                            } else {
                                bomb_speed_x = -this.BOMB_MAX_SPEED_X;
                            }
                        }
                        BulletObject.addBullet(this.enemyid, this.posX, this.posY, bomb_speed_x, 0);
                        this.state = 0;
                        this.drawer.setActionId(0);
                        this.drawer.setLoop(true);
                    }
                    checkWithPlayer(preX, preY, this.posX, this.posY);
                    return;
                default:
                    return;
            }
        }
    }

    public void draw(MFGraphics g) {
        if (!this.dead) {
            drawInMap(g, this.drawer);
            drawCollisionRect(g);
        }
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 768, y, 1536, COLLISION_HEIGHT);
    }
}
