package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import Lib.MyAPI;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: GimmickObject */
class WaterFall extends GimmickObject {
    private static int frame;
    public static AnimationDrawer waterFallDrawer1;
    public static AnimationDrawer waterFallDrawer2;
    public static AnimationDrawer waterFallDrawer3;
    public static AnimationDrawer waterFallDrawer4;
    private int drawSpace;
    private AnimationDrawer drawer;
    private boolean isActived;
    private boolean isFirstTouchedSandFall = false;
    private boolean isTouchSand = false;

    protected WaterFall(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        player.initWaterFall();
        this.isActived = false;
        player.isCrashFallingSand = false;
        this.isFirstTouchedSandFall = false;
        if (StageManager.getCurrentZoneId() == 1) {
            this.isTouchSand = false;
        } else if (StageManager.getCurrentZoneId() == 5) {
            this.isTouchSand = false;
            frame = 0;
        }
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (object == player && this.collisionRect.collisionChk(player.getCheckPositionX(), player.getCheckPositionY())) {
            if (StageManager.getCurrentZoneId() == 5) {
                this.isActived = true;
                if (player.collisionState == (byte) 1) {
                    player.collisionState = (byte) 3;
                }
                if (!this.isTouchSand) {
                    this.isTouchSand = true;
                }
                player.isCrashFallingSand = true;
                if ((player instanceof PlayerKnuckles) && ((PlayerKnuckles) player).flying) {
                    player.setVelX(0);
                    ((PlayerKnuckles) player).flying = false;
                }
                if (player.getVelX() >= 498) {
                    player.setVelX(498);
                } else if (player.getVelX() <= -498) {
                    player.setVelX(-498);
                }
                if (!this.isFirstTouchedSandFall) {
                    soundInstance.playSe(70);
                    this.isFirstTouchedSandFall = true;
                    frame = 0;
                }
                if (this.isFirstTouchedSandFall) {
                    frame++;
                    if (frame > 4 && !IsGamePause) {
                        soundInstance.playLoopSe(71);
                    }
                }
            } else if (StageManager.getCurrentZoneId() != 1) {
                this.isActived = true;
                frame++;
                frame %= 11;
                if (!this.isTouchSand) {
                    this.isTouchSand = true;
                }
                player.isCrashFallingSand = false;
            } else {
                player.isCrashFallingSand = false;
            }
            player.beWaterFall();
        }
    }

    public void doWhileNoCollision() {
        if (this.isActived) {
            if (this.isFirstTouchedSandFall) {
                if (soundInstance.getPlayingLoopSeIndex() == 71) {
                    soundInstance.stopLoopSe();
                }
                this.isFirstTouchedSandFall = false;
            }
            player.isCrashFallingSand = false;
            this.isActived = false;
        }
    }

    public void draw(MFGraphics g) {
        if (this.drawer != null) {
            MyAPI.setClip(g, (this.collisionRect.x0 >> 6) - camera.f13x, (this.collisionRect.y0 >> 6) - camera.f14y, this.collisionRect.getWidth() >> 6, this.collisionRect.getHeight() >> 6);
            int i = this.collisionRect.y0;
            while (i < this.collisionRect.y1) {
                drawInMap(g, this.drawer, this.posX, i);
                i += this.drawSpace;
            }
            MyAPI.setClip(g, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
        }
    }

    public static void releaseAllResource() {
        Animation.closeAnimationDrawer(waterFallDrawer1);
        Animation.closeAnimationDrawer(waterFallDrawer2);
        Animation.closeAnimationDrawer(waterFallDrawer3);
        Animation.closeAnimationDrawer(waterFallDrawer4);
        waterFallDrawer1 = null;
        waterFallDrawer2 = null;
        waterFallDrawer3 = null;
        waterFallDrawer4 = null;
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x, (this.iTop * 512) + y, this.mWidth, this.mHeight);
    }

    public static void staticLogic() {
        if (waterFallDrawer1 != null) {
            waterFallDrawer1.moveOn();
        }
        if (waterFallDrawer2 != null) {
            waterFallDrawer2.moveOn();
        }
        if (waterFallDrawer3 != null) {
            waterFallDrawer3.moveOn();
        }
        if (waterFallDrawer4 != null) {
            waterFallDrawer4.moveOn();
        }
    }

    public void close() {
        this.drawer = null;
    }

    public int getPaintLayer() {
        return 2;
    }
}
