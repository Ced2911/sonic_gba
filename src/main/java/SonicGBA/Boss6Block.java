package SonicGBA;

import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;

/* compiled from: GimmickObject */
class Boss6Block extends GimmickObject {
    public static final int COLLISION2_HEIGHT = 1664;
    public static final int COLLISION_HEIGHT = 1536;
    public static final int COLLISION_WIDTH = 2176;
    private static MFImage blockImage;
    private boolean IsDisplay;

    protected Boss6Block(int x, int y) {
        super(122, x, y, 0, 0, 0, 0);
        if (blockImage == null) {
            blockImage = MFImage.createImage("/gimmick/boss6_block.png");
        }
        this.IsDisplay = true;
        this.used = false;
        this.posX = x;
        this.posY = y;
    }

    public static void releaseAllResource() {
        blockImage = null;
    }

    public void logic(int x, int y) {
        if (this.IsDisplay) {
            this.posX = x;
            this.posY = y;
            checkWithPlayer(this.posX, this.posY, this.posX, this.posY);
        }
    }

    public boolean getUsedState() {
        return this.used;
    }

    public void setUsedState(boolean state) {
        this.used = state;
    }

    public void setDisplayState(boolean state) {
        this.IsDisplay = state;
    }

    public void draw(MFGraphics g) {
        if (this.IsDisplay) {
            drawInMap(g, blockImage, this.posX, this.posY, 3);
            drawCollisionRect(g);
        }
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (this.IsDisplay) {
            switch (direction) {
                case 1:
                    object.beStop(this.collisionRect.y0, 1, this);
                    this.used = true;
                    return;
                case 4:
                    object.beStop(this.collisionRect.y0, 1, this);
                    this.used = true;
                    return;
                default:
                    object.beStop(this.collisionRect.y0, 1, this);
                    this.used = true;
                    return;
            }
        }
    }

    public void refreshCollisionRect(int x, int y) {
        if (this.IsDisplay) {
            this.collisionRect.setRect(x - 1088, y - 768, COLLISION_WIDTH, 1536);
        }
    }

    public boolean collisionChkWithObject(PlayerObject object) {
        CollisionRect objectRect = object.getCollisionRect();
        CollisionRect thisRect = getCollisionRect();
        rectV.setRect(objectRect.x0 + 192, objectRect.y0, objectRect.getWidth() - PlayerSonic.BACK_JUMP_SPEED_X, objectRect.getHeight());
        return thisRect.collisionChk(rectV);
    }
}
