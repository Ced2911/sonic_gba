package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import Lib.SoundSystem;
import com.sega.mobile.framework.device.MFGamePad;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: EnemyObject */
class Boss2 extends BossObject {
    private static final int BOAT_HURT = 1;
    private static final int BOAT_NORMAL = 0;
    private static int BOSS_DRIP_X = 0;
    private static final int BOSS_DRIP_X_ST2 = 572672;
    private static final int BOSS_DRIP_X_ST5 = 603136;
    private static final int COLLISION_HEIGHT = 1280;
    private static final int COLLISION_WIDTH = 3072;
    private static final int FACE_HURT = 2;
    private static final int FACE_NORMAL = 0;
    private static final int FACE_SMILE = 1;
    private static int SIDE = 0;
    private static int SIDE_DOWN_MIDDLE = 0;
    private static final int SIDE_DOWN_MIDDLE_ST2 = 1416;
    private static final int SIDE_DOWN_MIDDLE_ST5 = 1136;
    private static int SIDE_LEFT = 0;
    private static final int SIDE_LEFT_ST2 = 8708;
    private static final int SIDE_LEFT_ST5 = 9184;
    private static int SIDE_RIGHT = 0;
    private static final int SIDE_RIGHT_ST2 = 9008;
    private static final int SIDE_RIGHT_ST5 = 9484;
    private static final int SIDE_ST2 = 561152;
    private static final int SIDE_ST5 = 591616;
    private static int SIDE_UP = 78848;
    private static final int SPRING_DAMPING = 2;
    private static final int SPRING_FLYING = 0;
    private static final int SPRING_WAITING = 1;
    private static final int STATE_ATTACK_JUMPING = 7;
    private static final int STATE_ATTACK_SPRING_DAMPING = 6;
    private static final int STATE_ATTACK_WAITING = 5;
    private static final int STATE_BROKEN = 9;
    private static final int STATE_ESCAPE = 10;
    private static final int STATE_RELASE_SPRING_DAMPING = 8;
    private static final int STATE_SHOW_DROP = 0;
    private static final int STATE_SHOW_LAUGH = 3;
    private static final int STATE_SHOW_SPRING_DAMPING = 1;
    private static final int STATE_SHOW_WAITING = 2;
    private static final int STATE_SHOW_WAITING_2 = 4;
    private static final int STATE_WAIT = -1;
    private static Animation boatAni = null;
    private static final int cnt_max = 8;
    private static final int display_cnt_max = 35;
    private static final int display_wait_cnt_max = 16;
    private static Animation escapeboatAni = null;
    private static Animation escapefaceAni = null;
    private static Animation faceAni = null;
    private static final int high_jump_wait_cnt_max = 34;
    private static final int show_laugh_cnt_max = 10;
    private boolean IsBroken;
    private boolean IsinHighJump = false;
    private int WaitCnt;
    private int boat_cnt;
    private int boat_state;
    private AnimationDrawer boatdrawer;
    private int boom_offset = 1088;
    private BossBroken bossbroken;
    private int display_cnt = 0;
    private int display_wait_cnt;
    private int drop_cnt;
    private int escape_v = 512;
    private AnimationDrawer escapeboatdrawer;
    private AnimationDrawer escapefacedrawer;
    private int face_cnt;
    private int face_state;
    private AnimationDrawer facedrawer;
    private int fly_end;
    private int fly_top;
    private int fly_top_range = MFGamePad.KEY_NUM_6;
    private int high_jump_cnt = -1;
    private int high_jump_cnt_max = 3;
    private int high_jump_wait_cnt = 0;
    private int limitLeftX;
    private int limitRightX;
    private int lowerHP_jump_startVerlY = -2600;
    private int min_range_x = 0;
    private int normal_jump_startVerlY = (((-GRAVITY) / 2) * 19);
    private int offset_y = 1088;
    private int range = 28800;
    private int show_laugh_cnt = 0;
    private int side_left = 0;
    private int side_right = 0;
    private Boss2Spring spring;
    private int springH;
    private int springHmax;
    private int spring_state;
    private boolean start_cnt = false;
    private int start_pos = 16640;
    private int state;
    private int v_x_frame = 19;
    private int velX = 0;
    private int velY = 0;
    private int velocity = -384;
    private int wait_cnt;
    private int wait_cnt_max = 5;

    public static void releaseAllResource() {
        Animation.closeAnimation(boatAni);
        Animation.closeAnimation(faceAni);
        Animation.closeAnimation(escapeboatAni);
        Animation.closeAnimation(escapefaceAni);
        boatAni = null;
        faceAni = null;
        escapeboatAni = null;
        escapefaceAni = null;
    }

    protected Boss2(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        this.posX -= this.iLeft * 8;
        this.posY -= this.iTop * 8;
        if (StageManager.getCurrentZoneId() == 5) {
            SIDE = SIDE_ST5;
            SIDE_LEFT = SIDE_LEFT_ST5;
            SIDE_RIGHT = SIDE_RIGHT_ST5;
            SIDE_DOWN_MIDDLE = SIDE_DOWN_MIDDLE_ST5;
            BOSS_DRIP_X = BOSS_DRIP_X_ST5;
        } else {
            SIDE = SIDE_ST2;
            SIDE_LEFT = SIDE_LEFT_ST2;
            SIDE_RIGHT = SIDE_RIGHT_ST2;
            SIDE_DOWN_MIDDLE = SIDE_DOWN_MIDDLE_ST2;
            BOSS_DRIP_X = BOSS_DRIP_X_ST2;
        }
        this.limitRightX = SIDE_RIGHT << 6;
        this.limitLeftX = SIDE_LEFT << 6;
        refreshCollisionRect(this.posX >> 6, this.posY >> 6);
        if (boatAni == null) {
            boatAni = new Animation("/animation/boss2_boat");
        }
        this.boatdrawer = boatAni.getDrawer(0, true, 0);
        if (faceAni == null) {
            faceAni = new Animation("/animation/boss2_face");
        }
        this.facedrawer = faceAni.getDrawer(0, true, 0);
        if (escapeboatAni == null) {
            escapeboatAni = new Animation("/animation/pod_boat");
        }
        this.escapeboatdrawer = escapeboatAni.getDrawer(0, true, 0);
        if (escapefaceAni == null) {
            escapefaceAni = new Animation("/animation/pod_face");
        }
        this.escapefacedrawer = escapefaceAni.getDrawer(4, true, 0);
        this.posY -= this.start_pos;
        this.spring = new Boss2Spring(32, x, y, left, top, width, height);
        GameObject.addGameObject(this.spring, x, y);
        this.IsBroken = false;
        this.state = -1;
        this.high_jump_cnt = -1;
        setBossHP();
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (this.dead || object != player || this.state <= 4) {
            return;
        }
        if (player.isAttackingEnemy()) {
            if (this.HP > 0 && this.face_state != 2) {
                this.HP--;
                if (this.HP == 4) {
                    this.high_jump_cnt = 0;
                }
                player.doBossAttackPose(this, direction);
                this.face_state = 2;
                this.boat_state = 1;
                if (this.HP == 0) {
                    SoundSystem.getInstance().playSe(35);
                } else {
                    SoundSystem.getInstance().playSe(34);
                }
            }
        } else if (this.state != 9 && this.state != 10 && this.boat_state != 1 && player.canBeHurt()) {
            player.beHurt();
            this.face_state = 1;
        }
    }

    public void doWhileBeAttack(PlayerObject object, int direction, int animationID) {
        if (this.state > 4 && this.HP > 0 && this.face_state != 2) {
            this.HP--;
            if (this.HP == 4) {
                this.high_jump_cnt = 0;
            }
            player.doBossAttackPose(this, direction);
            this.face_state = 2;
            this.boat_state = 1;
            if (this.HP == 0) {
                SoundSystem.getInstance().playSe(35);
            } else {
                SoundSystem.getInstance().playSe(34);
            }
        }
    }

    public void logic() {
        if (!this.dead) {
            this.springH = this.spring.getSpringHeight();
            int preX = this.posX;
            int preY = this.posY;
            int tmpX = this.posX;
            int tmpY = this.posY;
            if (this.HP == 0 && !this.IsBroken) {
                this.state = 9;
                this.spring.setBossBrokenState(true);
                this.posY = getGroundY(this.posX, this.posY) - this.springHmax;
                this.bossbroken = new BossBroken(23, this.posX >> 6, this.posY >> 6, 0, 0, 0, 0);
                GameObject.addGameObject(this.bossbroken, this.posX >> 6, this.posY >> 6);
                this.IsBroken = true;
                this.velY = 0;
                this.side_left = MapManager.getCamera().f13x;
                this.side_right = this.side_left + MapManager.CAMERA_WIDTH;
                MapManager.setCameraLeftLimit(this.side_left);
                MapManager.setCameraRightLimit(this.side_right);
            }
            if (this.state > 4) {
                this.spring.setAttackable(true);
            }
            if (this.HP >= 5) {
                this.wait_cnt_max = 8;
                this.v_x_frame = 19;
            } else if (this.HP == 4 || this.HP == 3) {
                this.wait_cnt_max = 7;
                this.v_x_frame = 16;
            } else if (this.HP == 2) {
                this.wait_cnt_max = 6;
                this.v_x_frame = 14;
            } else if (this.HP == 1) {
                this.wait_cnt_max = 5;
                this.v_x_frame = 12;
            }
            this.normal_jump_startVerlY = ((-GRAVITY) / 2) * this.v_x_frame;
            if (this.state > -1) {
                isBossEnter = true;
            }
            switch (this.state) {
                case -1:
                    if (player.getFootPositionX() >= SIDE) {
                        MapManager.setCameraRightLimit(SIDE_RIGHT);
                    }
                    if (player.getFootPositionX() >= SIDE && !this.start_cnt && player.getFootPositionY() >= SIDE_UP) {
                        if (!this.IsPlayBossBattleBGM) {
                            bossFighting = true;
                            bossID = 23;
                            SoundSystem.getInstance().playBgm(22, true);
                            MapManager.setCameraLeftLimit(SIDE_LEFT);
                            MapManager.setCameraRightLimit(SIDE_RIGHT);
                            MapManager.setCameraDownLimit(SIDE_DOWN_MIDDLE + ((MapManager.CAMERA_HEIGHT * 1) / 4));
                            MapManager.setCameraUpLimit(SIDE_DOWN_MIDDLE - ((MapManager.CAMERA_HEIGHT * 3) / 4));
                            this.IsPlayBossBattleBGM = true;
                        }
                        this.start_cnt = true;
                    }
                    if (this.start_cnt) {
                        if (this.display_cnt < 35) {
                            this.display_cnt++;
                            break;
                        }
                        this.state = 0;
                        this.posX = BOSS_DRIP_X;
                        break;
                    }
                    break;
                case 0:
                    this.velY += GRAVITY;
                    this.posY += this.velY;
                    if (this.posY + this.velY >= getGroundY(this.posX, this.posY)) {
                        this.posY = getGroundY(this.posX, this.posY);
                        this.state = 1;
                        this.spring_state = 2;
                        this.spring.setSpringAni(2, false);
                        SoundSystem.getInstance().playSe(36);
                        break;
                    }
                    break;
                case 1:
                    if (this.spring.getEndState()) {
                        this.state = 2;
                        this.spring_state = 1;
                        this.spring.setSpringAni(1, true);
                        break;
                    }
                    break;
                case 2:
                    if (this.display_wait_cnt >= 16) {
                        this.state = 3;
                        this.facedrawer.setActionId(1);
                        this.facedrawer.setTrans(0);
                        this.facedrawer.setLoop(true);
                        this.display_wait_cnt = 0;
                        break;
                    }
                    this.display_wait_cnt++;
                    break;
                case 3:
                    if (this.show_laugh_cnt >= 10) {
                        this.state = 4;
                        changeAniState(this.facedrawer, 0, true);
                        break;
                    }
                    this.show_laugh_cnt++;
                    break;
                case 4:
                    if (this.display_wait_cnt < 16) {
                        this.display_wait_cnt++;
                    } else {
                        this.state = 6;
                        this.spring_state = 2;
                        this.spring.setSpringAni(2, false);
                        this.display_wait_cnt = 0;
                    }
                    changeAniState(this.boatdrawer, this.boat_state, true);
                    changeAniState(this.facedrawer, this.face_state, true);
                    break;
                case 5:
                    resetBossDisplayState();
                    if (this.wait_cnt < this.wait_cnt_max) {
                        this.wait_cnt++;
                    } else {
                        this.state = 6;
                        this.spring_state = 2;
                        this.spring.setSpringAni(2, false);
                        this.wait_cnt = 0;
                    }
                    changeAniStateNoTran(this.boatdrawer, this.boat_state, true);
                    changeAniStateNoTran(this.facedrawer, this.face_state, true);
                    break;
                case 6:
                    resetBossDisplayState();
                    if (this.spring.getEndState()) {
                        this.state = 7;
                        this.spring_state = 0;
                        this.spring.setSpringAni(0, true);
                        if (HighJump()) {
                            this.velX = 0;
                            this.velY = this.lowerHP_jump_startVerlY;
                            this.IsinHighJump = false;
                        } else {
                            if (player.getFootPositionX() - this.posX > this.min_range_x || player.getFootPositionX() - this.posX < (-this.min_range_x)) {
                                this.velX = (player.getFootPositionX() - this.posX) / this.v_x_frame;
                            } else if (player.getFootPositionX() - this.posX <= this.min_range_x && player.getFootPositionX() - this.posX > 0) {
                                this.velX = this.min_range_x / this.v_x_frame;
                            } else if (player.getFootPositionX() - this.posX >= (-this.min_range_x) && player.getFootPositionX() - this.posX < 0) {
                                this.velX = (-this.min_range_x) / this.v_x_frame;
                            }
                            this.velY = this.normal_jump_startVerlY;
                        }
                    }
                    changeAniState(this.boatdrawer, this.boat_state, true);
                    changeAniState(this.facedrawer, this.face_state, true);
                    break;
                case 7:
                    resetBossDisplayState();
                    if (this.posY + this.velY > getGroundY(this.posX, this.posY)) {
                        this.posY = getGroundY(this.posX, this.posY);
                        this.state = 8;
                        this.spring_state = 2;
                        this.spring.setSpringAni(2, false);
                        if (HighJump()) {
                            MapManager.setShake(8);
                        }
                        if (HighJump() && player.getFootPositionY() == getGroundY(player.getFootPositionX(), player.getFootPositionY())) {
                            player.beHurt();
                            this.face_state = 1;
                        }
                        if (this.HP < 5) {
                            if (this.high_jump_cnt < this.high_jump_cnt_max) {
                                this.high_jump_cnt++;
                            } else {
                                this.high_jump_cnt = 0;
                            }
                        }
                        SoundSystem.getInstance().playSe(36);
                    } else {
                        if (this.posX + this.velX >= this.limitRightX) {
                            this.posX = this.limitRightX;
                        } else if (this.posX + this.velX <= this.limitLeftX) {
                            this.posX = this.limitLeftX;
                        } else {
                            this.posX += this.velX;
                        }
                        if (!IsJumpOutScreen() || this.IsinHighJump) {
                            this.velY += GRAVITY;
                            this.posY += this.velY;
                            this.high_jump_wait_cnt = 0;
                        } else if (this.high_jump_wait_cnt < 34) {
                            this.high_jump_wait_cnt++;
                        } else {
                            this.posX = player.getFootPositionX();
                            this.velY = 0;
                            this.velY += GRAVITY * 2;
                            this.IsinHighJump = true;
                        }
                        this.springHmax = this.spring.getSpringHeight();
                    }
                    changeAniStateNoTran(this.boatdrawer, this.boat_state, true);
                    changeAniStateNoTran(this.facedrawer, this.face_state, true);
                    break;
                case 8:
                    resetBossDisplayState();
                    if (this.spring.getEndState()) {
                        this.state = 5;
                        this.spring_state = 1;
                        this.spring.setSpringAni(1, true);
                    }
                    changeAniStateNoTran(this.boatdrawer, this.boat_state, true);
                    changeAniStateNoTran(this.facedrawer, this.face_state, true);
                    break;
                case 9:
                    resetBossDisplayState();
                    this.bossbroken.logicBoom(this.posX, this.posY - this.boom_offset);
                    this.springH = 0;
                    if (this.posY + this.velY > getGroundY(this.posX, this.posY) && this.drop_cnt == 0) {
                        this.posY = getGroundY(this.posX, this.posY);
                        this.velY = -640;
                        this.drop_cnt = 1;
                    } else if (this.posY + this.velY > getGroundY(this.posX, this.posY) && this.drop_cnt == 1) {
                        this.posY = getGroundY(this.posX, this.posY);
                        this.velY = -320;
                        this.drop_cnt = 2;
                    } else if (this.posY + this.velY > getGroundY(this.posX, this.posY) && this.drop_cnt == 2) {
                        this.posY = getGroundY(this.posX, this.posY);
                        this.drop_cnt = 3;
                    } else if (this.drop_cnt != 3) {
                        this.velY += GRAVITY;
                        this.posY += this.velY;
                    }
                    if (this.bossbroken.getEndState()) {
                        this.state = 10;
                        this.fly_top = this.posY;
                        this.fly_end = this.side_right;
                        this.wait_cnt = 0;
                        bossFighting = false;
                        player.getBossScore();
                        SoundSystem.getInstance().playBgm(StageManager.getBgmId(), true);
                    }
                    changeAniStateNoTran(this.facedrawer, this.face_state, true);
                    break;
                case 10:
                    this.springH = 0;
                    this.wait_cnt++;
                    if (this.wait_cnt >= this.wait_cnt_max && this.posY >= this.fly_top - this.fly_top_range) {
                        this.posY -= this.escape_v;
                    }
                    if (this.posY <= this.fly_top - this.fly_top_range && this.WaitCnt == 0) {
                        this.posY = this.fly_top - this.fly_top_range;
                        this.escapefacedrawer.setActionId(0);
                        this.escapeboatdrawer.setActionId(1);
                        this.escapeboatdrawer.setLoop(false);
                        this.WaitCnt = 1;
                    }
                    if (this.WaitCnt == 1 && this.escapeboatdrawer.checkEnd()) {
                        this.escapefacedrawer.setActionId(0);
                        this.escapefacedrawer.setTrans(2);
                        this.escapefacedrawer.setLoop(true);
                        this.escapeboatdrawer.setActionId(1);
                        this.escapeboatdrawer.setTrans(2);
                        this.escapeboatdrawer.setLoop(false);
                        this.WaitCnt = 2;
                    }
                    if (this.WaitCnt == 2 && this.escapeboatdrawer.checkEnd()) {
                        this.escapeboatdrawer.setActionId(0);
                        this.escapeboatdrawer.setTrans(2);
                        this.escapeboatdrawer.setLoop(true);
                        this.WaitCnt = 3;
                    }
                    if (this.WaitCnt == 3 || this.WaitCnt == 4) {
                        this.posX += this.escape_v;
                    }
                    if (this.posX > (this.side_right << 6) && this.WaitCnt == 3) {
                        GameObject.addGameObject(new Cage((MapManager.getCamera().f13x + (MapManager.CAMERA_WIDTH >> 1)) << 6, MapManager.getCamera().f14y << 6));
                        MapManager.lockCamera(true);
                        this.WaitCnt = 4;
                        break;
                    }
            }
            this.spring.logic(this.posX, this.posY, this.spring_state, this.velocity);
            this.spring.getIsHurt(this.boat_state == 1);
            refreshCollisionRect(this.posX >> 6, this.posY >> 6);
            checkWithPlayer(preX, preY, this.posX, this.posY);
        }
    }

    public boolean HighJump() {
        if (this.HP >= 5 || this.high_jump_cnt != 0) {
            return false;
        }
        return true;
    }

    public boolean IsJumpOutScreen() {
        if (!HighJump() || (this.posY >> 6) >= MapManager.getCamera().f14y) {
            return false;
        }
        return true;
    }

    public void draw(MFGraphics g) {
        if (!this.dead) {
            if (this.state == 0) {
                if (this.posY > ((SIDE_DOWN_MIDDLE - MapManager.CAMERA_HEIGHT) << 6)) {
                    drawInMap(g, this.boatdrawer, this.posX, this.posY - this.springH);
                    drawInMap(g, this.facedrawer, this.posX, (this.posY - Boss6Block.COLLISION2_HEIGHT) - this.springH);
                }
            } else if (this.state != 10 && this.state != -1) {
                drawInMap(g, this.boatdrawer, this.posX, this.posY - this.springH);
                drawInMap(g, this.facedrawer, this.posX, (this.posY - Boss6Block.COLLISION2_HEIGHT) - this.springH);
            } else if (this.state != -1) {
                drawInMap(g, this.escapeboatdrawer, this.posX, this.posY);
                drawInMap(g, this.escapefacedrawer, this.posX, this.posY - Boss6Block.COLLISION2_HEIGHT);
            }
            drawCollisionRect(g);
            if (!(this.state == 9 || this.state == 10 || this.state == -1)) {
                this.spring.draw(g);
            }
            if (this.bossbroken != null) {
                this.bossbroken.draw(g);
            }
        }
    }

    public void resetBossDisplayState() {
        if (this.face_state != 0) {
            if (this.face_cnt < 8) {
                this.face_cnt++;
            } else {
                this.face_state = 0;
                this.face_cnt = 0;
            }
        }
        if (this.boat_state != 1) {
            return;
        }
        if (this.boat_cnt < 8) {
            this.boat_cnt++;
            return;
        }
        this.boat_state = 0;
        this.boat_cnt = 0;
    }

    public void changeAniState(AnimationDrawer AniDrawer, int state, boolean isloop) {
        if (player.getCheckPositionX() > this.posX) {
            AniDrawer.setActionId(state);
            AniDrawer.setTrans(2);
            AniDrawer.setLoop(isloop);
            return;
        }
        AniDrawer.setActionId(state);
        AniDrawer.setTrans(0);
        AniDrawer.setLoop(isloop);
    }

    public void changeAniStateNoTran(AnimationDrawer AniDrawer, int state, boolean isloop) {
        AniDrawer.setActionId(state);
        AniDrawer.setLoop(isloop);
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 1536, (y - (this.offset_y + COLLISION_HEIGHT)) - this.springH, 3072, COLLISION_HEIGHT);
    }

    public void close() {
        this.boatdrawer = null;
        this.facedrawer = null;
        this.escapeboatdrawer = null;
        this.escapefacedrawer = null;
    }
}
