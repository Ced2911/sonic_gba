package SonicGBA;

import Lib.Animation;
import com.sega.mobile.framework.device.MFGamePad;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: EnemyObject */
class Lizard extends EnemyObject {
    private static final int COLLISION_HEIGHT = 2048;
    private static final int COLLISION_WIDTH = 1728;
    private static final int STATE_ATTACK = 1;
    private static final int STATE_MOVE = 0;
    private static Animation lizardAnimation;
    private boolean IsFired;
    private int enemyid;
    private int fire_start_speed;
    private int limitLeftX;
    private int limitRightX;
    private int state;
    private int velocity;

    public static void releaseAllResource() {
        Animation.closeAnimation(lizardAnimation);
        lizardAnimation = null;
    }

    protected Lizard(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        this.velocity = 150;
        this.fire_start_speed = PlayerObject.SONIC_ATTACK_LEVEL_3_V0;
        this.mWidth = MFGamePad.KEY_NUM_6;
        this.limitLeftX = this.posX;
        this.limitRightX = this.posX + this.mWidth;
        this.posX += this.mWidth >> 1;
        if (lizardAnimation == null) {
            lizardAnimation = new Animation("/animation/lizard");
        }
        this.drawer = lizardAnimation.getDrawer(0, true, 0);
        this.IsFired = false;
        this.enemyid = id;
    }

    public void logic() {
        if (!this.dead) {
            int preX = this.posX;
            int preY = this.posY;
            switch (this.state) {
                case 0:
                    int pos = (this.limitLeftX + (this.mWidth >> 1)) / 150;
                    if (this.velocity > 0) {
                        this.posX += this.velocity;
                        this.drawer.setActionId(0);
                        this.drawer.setTrans(2);
                        this.drawer.setLoop(true);
                        if (this.posX >= this.limitRightX) {
                            this.posX = this.limitRightX;
                            this.velocity = -this.velocity;
                            this.IsFired = false;
                        }
                        if (this.posX / 150 == pos && !this.IsFired) {
                            this.state = 1;
                            this.drawer.setActionId(1);
                            this.drawer.setTrans(2);
                            this.drawer.setLoop(false);
                            this.IsFired = true;
                        }
                    } else {
                        this.posX += this.velocity;
                        this.drawer.setActionId(0);
                        this.drawer.setTrans(0);
                        this.drawer.setLoop(true);
                        if (this.posX <= this.limitLeftX) {
                            this.posX = this.limitLeftX;
                            this.velocity = -this.velocity;
                            this.IsFired = false;
                        }
                        if (this.posX / 150 == pos && !this.IsFired) {
                            this.state = 1;
                            this.drawer.setActionId(1);
                            this.drawer.setTrans(0);
                            this.drawer.setLoop(false);
                            this.IsFired = true;
                        }
                    }
                    this.posY = getGroundY(this.posX, this.posY);
                    checkWithPlayer(preX, preY, this.posX, this.posY);
                    return;
                case 1:
                    if (this.drawer.checkEnd()) {
                        BulletObject.addBullet(this.enemyid, this.posX, this.posY - 1024, 0, -this.fire_start_speed);
                        this.state = 0;
                        if (this.velocity > 0) {
                            this.drawer.setActionId(0);
                            this.drawer.setTrans(2);
                        } else {
                            this.drawer.setActionId(0);
                            this.drawer.setTrans(0);
                        }
                    }
                    this.posY = getGroundY(this.posX, this.posY);
                    checkWithPlayer(preX, preY, this.posX, this.posY);
                    return;
                default:
                    return;
            }
        }
    }

    public void draw(MFGraphics g) {
        if (!this.dead) {
            drawInMap(g, this.drawer);
            drawCollisionRect(g);
        }
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 864, y - 2048, COLLISION_WIDTH, 2048);
    }
}
