package SonicGBA;

import Lib.Animation;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: EnemyObject */
class SnowRobotV extends EnemyObject {
    private static final int ALERT_HEIGHT = 10;
    private static final int ALERT_WIDTH = 200;
    private static final int COLLISION_HEIGHT = 1152;
    private static final int COLLISION_WIDTH = 1600;
    private static final int LAUNCH_SPEED = 960;
    private static final int SPEED = 128;
    private static final int STATE_ATTACK = 1;
    private static final int STATE_PATROL = 0;
    private static final int WAIT_MAX = 30;
    private boolean IsFire = false;
    private int alert_state;
    private boolean dir = false;
    private int endPosY;
    private int iLeft;
    private int iTrans;
    private int startPosY;
    private int state;
    private int wait_cn;

    protected SnowRobotV(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        if (snowrobotAnimation == null) {
            snowrobotAnimation = new Animation("/animation/yukimal");
        }
        this.iLeft = left;
        this.iTrans = this.iLeft == 0 ? 5 : 6;
        this.drawer = snowrobotAnimation.getDrawer(0, true, this.iTrans);
        this.startPosY = this.posY;
        this.endPosY = this.posY + this.mHeight;
        this.dir = false;
        this.state = 0;
        this.wait_cn = 0;
        this.IsFire = false;
    }

    public static void releaseAllResource() {
        Animation.closeAnimation(snowrobotAnimation);
        snowrobotAnimation = null;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void logic() {
        /*
        r8 = this;
        r7 = 1;
        r6 = 0;
        r2 = r8.dead;
        if (r2 == 0) goto L_0x0007;
    L_0x0006:
        return;
    L_0x0007:
        r2 = r8.posX;
        r2 = r2 >> 6;
        r3 = r8.posY;
        r3 = r3 >> 6;
        r4 = 200; // 0xc8 float:2.8E-43 double:9.9E-322;
        r5 = 10;
        r2 = r8.checkPlayerInEnemyAlertRange(r2, r3, r4, r5);
        r8.alert_state = r2;
        r0 = r8.posX;
        r1 = r8.posY;
        r2 = r8.state;
        switch(r2) {
            case 0: goto L_0x0023;
            case 1: goto L_0x0093;
            default: goto L_0x0022;
        };
    L_0x0022:
        goto L_0x0006;
    L_0x0023:
        r2 = r8.dir;
        if (r2 != 0) goto L_0x0074;
    L_0x0027:
        r2 = r8.posY;
        r2 = r2 + 128;
        r8.posY = r2;
        r2 = r8.posY;
        r3 = r8.endPosY;
        if (r2 < r3) goto L_0x0039;
    L_0x0033:
        r8.dir = r7;
        r2 = r8.endPosY;
        r8.posY = r2;
    L_0x0039:
        r2 = r8.alert_state;
        if (r2 != 0) goto L_0x006c;
    L_0x003d:
        r2 = r8.iLeft;
        if (r2 != 0) goto L_0x0088;
    L_0x0041:
        r2 = r8.posX;
        r3 = player;
        r3 = r3.getFootPositionX();
        if (r2 >= r3) goto L_0x006c;
    L_0x004b:
        r2 = r8.posY;
        r3 = r8.startPosY;
        if (r2 == r3) goto L_0x006c;
    L_0x0051:
        r2 = r8.posY;
        r3 = r8.endPosY;
        if (r2 == r3) goto L_0x006c;
    L_0x0057:
        r8.state = r7;
        r2 = r8.drawer;
        r2.setActionId(r7);
        r2 = r8.drawer;
        r3 = r8.iTrans;
        r2.setTrans(r3);
        r2 = r8.drawer;
        r2.setLoop(r6);
        r8.IsFire = r7;
    L_0x006c:
        r2 = r8.posX;
        r3 = r8.posY;
        r8.checkWithPlayer(r0, r1, r2, r3);
        goto L_0x0006;
    L_0x0074:
        r2 = r8.posY;
        r3 = 128; // 0x80 float:1.794E-43 double:6.32E-322;
        r2 = r2 - r3;
        r8.posY = r2;
        r2 = r8.posY;
        r3 = r8.startPosY;
        if (r2 > r3) goto L_0x0039;
    L_0x0081:
        r8.dir = r6;
        r2 = r8.startPosY;
        r8.posY = r2;
        goto L_0x0039;
    L_0x0088:
        r2 = r8.posX;
        r3 = player;
        r3 = r3.getFootPositionX();
        if (r2 <= r3) goto L_0x006c;
    L_0x0092:
        goto L_0x004b;
    L_0x0093:
        r2 = r8.wait_cn;
        r3 = 30;
        if (r2 >= r3) goto L_0x00d2;
    L_0x0099:
        r2 = r8.wait_cn;
        r2 = r2 + 1;
        r8.wait_cn = r2;
        r2 = r8.drawer;
        r2 = r2.checkEnd();
        if (r2 == 0) goto L_0x00c3;
    L_0x00a7:
        r2 = r8.IsFire;
        if (r2 == 0) goto L_0x00c3;
    L_0x00ab:
        r8.IsFire = r6;
        r2 = 17;
        r3 = r8.posX;
        r4 = r8.iLeft;
        if (r4 != 0) goto L_0x00cc;
    L_0x00b5:
        r4 = 1600; // 0x640 float:2.242E-42 double:7.905E-321;
    L_0x00b7:
        r3 = r3 + r4;
        r4 = r8.posY;
        r5 = r8.iLeft;
        if (r5 != 0) goto L_0x00cf;
    L_0x00be:
        r5 = 960; // 0x3c0 float:1.345E-42 double:4.743E-321;
    L_0x00c0:
        SonicGBA.BulletObject.addBullet(r2, r3, r4, r5, r6);
    L_0x00c3:
        r2 = r8.posX;
        r3 = r8.posY;
        r8.checkWithPlayer(r0, r1, r2, r3);
        goto L_0x0006;
    L_0x00cc:
        r4 = -1600; // 0xfffffffffffff9c0 float:NaN double:NaN;
        goto L_0x00b7;
    L_0x00cf:
        r5 = -960; // 0xfffffffffffffc40 float:NaN double:NaN;
        goto L_0x00c0;
    L_0x00d2:
        r8.wait_cn = r6;
        r2 = r8.drawer;
        r2.setActionId(r6);
        r2 = r8.drawer;
        r3 = r8.iTrans;
        r2.setTrans(r3);
        r2 = r8.drawer;
        r2.setLoop(r7);
        r8.state = r6;
        goto L_0x00c3;
        */
        throw new UnsupportedOperationException("Method not decompiled: SonicGBA.SnowRobotV.logic():void");
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect((this.iLeft == 0 ? 0 : -1600) + x, y, COLLISION_WIDTH, 1152);
    }

    public void draw(MFGraphics g) {
        if (!this.dead) {
            drawInMap(g, this.drawer);
        }
    }
}
