package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import Lib.MyRandom;
import com.sega.mobile.framework.device.MFGraphics;
import java.lang.reflect.Array;

/* compiled from: EnemyObject */
class BreakingParts extends EnemyObject {
    private static final int COLLISION_HEIGHT = 64;
    private static final int COLLISION_WIDTH = 64;
    private static Animation PartsAni;
    private static int frame;
    private int[] Vix = new int[]{750, SmallAnimal.FLY_VELOCITY_Y, -150, 150, 450, -750, 600, 300, -450};
    private int[] Viy = new int[]{-1200, -1050, PlayerObject.MIN_ATTACK_JUMP, -750, -600};
    private AnimationDrawer partsdrawer;
    private int[][] pos;

    public static void releaseAllResource() {
        Animation.closeAnimation(PartsAni);
        PartsAni = null;
    }

    protected BreakingParts(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        if (PartsAni == null) {
            PartsAni = new Animation("/animation/parts");
        }
        this.partsdrawer = PartsAni.getDrawer(0, false, 0);
        this.pos = (int[][]) Array.newInstance(Integer.TYPE, new int[]{3, 5});
        this.posX = x;
        this.posY = y;
        for (int i = 0; i < this.pos.length; i++) {
            this.pos[i][0] = this.posX;
            this.pos[i][1] = this.posY;
            this.pos[i][2] = this.Vix[MyRandom.nextInt(this.Vix.length)];
            this.pos[i][3] = this.Viy[MyRandom.nextInt(this.Viy.length)];
            this.pos[i][4] = MyRandom.nextInt(5);
        }
        frame = 0;
    }

    public void logic() {
        if (!IsGamePause) {
            frame++;
            for (int i = 0; i < this.pos.length; i++) {
                int[] iArr = this.pos[i];
                iArr[0] = iArr[0] + this.pos[i][2];
                iArr = this.pos[i];
                iArr[3] = iArr[3] + (GRAVITY >> 1);
                iArr = this.pos[i];
                iArr[1] = iArr[1] + this.pos[i][3];
            }
            if (frame >= 50) {
                this.dead = true;
            }
        }
        refreshCollisionRect(this.posX, this.posY);
    }

    public void draw(MFGraphics g) {
        if (!this.dead) {
            for (int i = 0; i < this.pos.length; i++) {
                this.partsdrawer.setActionId(this.pos[i][4]);
                drawInMap(g, this.partsdrawer, this.pos[i][0], this.pos[i][1]);
            }
        }
    }

    public void doWhileCollision(PlayerObject object, int direction) {
    }

    public void doWhileBeAttack(PlayerObject object, int direction, int animationID) {
    }

    public void close() {
        this.partsdrawer = null;
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 32, y - 32, 64, 64);
    }
}
