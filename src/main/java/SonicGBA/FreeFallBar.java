package SonicGBA;

import Lib.Coordinate;
import Lib.SoundSystem;
import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;

/* compiled from: GimmickObject */
class FreeFallBar extends GimmickObject {
    private static final int COLLISION_HEIGHT = 2560;
    private static final int COLLISION_WIDTH = 1024;
    private static final int PLAYER_OFFSET = -640;
    private static MFImage barImage;
    private static int frameCnt = 0;
    private boolean isLeftEnter;
    private int posXorg;
    private int posYorg;
    private FreeFallSystem system;

    protected FreeFallBar(FreeFallSystem system, int x, int y) {
        super(0, x, y, 0, 0, 0, 0);
        this.system = system;
        if (barImage == null) {
            try {
                barImage = MFImage.createImage("/gimmick/freefall_bar.png");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        frameCnt = 1;
        this.isLeftEnter = false;
        Coordinate co = system.getBarPosition();
        this.posXorg = co.f13x;
        this.posYorg = co.f14y;
    }

    public void init() {
        this.used = false;
        frameCnt = 0;
        this.posX = this.posXorg;
        this.posY = this.posYorg;
        refreshCollisionRect(this.posX, this.posY);
    }

    public void barLogic() {
        Coordinate co = this.system.getBarPosition();
        this.posX = co.f13x;
        this.posY = co.f14y;
        refreshCollisionRect(this.posX, this.posY);
        if (this.system.moving) {
            player.setFootPositionY(this.posY - PLAYER_OFFSET);
            player.setFootPositionX(this.posX);
            player.faceDirection = true;
            System.out.println("bar posX:" + (this.posX >> 6));
        }
    }

    public void draw(MFGraphics g) {
        if (!this.system.initFlag) {
            if (this.system.moving) {
                frameCnt++;
                if (player.getAnimationId() == 22) {
                    drawInMap(g, barImage, 33);
                    player.draw(g, true);
                } else if (player.getAnimationId() == 23) {
                    player.draw(g, true);
                    drawInMap(g, barImage, 33);
                } else {
                    player.setAnimationId(23);
                }
                System.out.println("player posX:" + (player.footPointX >> 6));
                return;
            }
            drawInMap(g, barImage, 33);
        }
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (!this.system.isSystemReady()) {
            return;
        }
        if ((!(object instanceof PlayerTails) || object.getCharacterAnimationID() < 12 || object.getCharacterAnimationID() > 14) && !this.used && !this.system.moving) {
            this.used = true;
            this.system.moving = true;
            this.system.shootDirection = player.getVelX() > 0;
            SoundSystem.getInstance().playSe(73);
            player.changeVisible(false);
            player.setOutOfControl(this);
            switch (direction) {
                case 2:
                    player.setAnimationId(22);
                    this.isLeftEnter = true;
                    break;
                case 3:
                    player.setAnimationId(23);
                    this.isLeftEnter = false;
                    break;
            }
            player.cancelFootObject();
            player.collisionChkBreak = true;
            player.faceDirection = true;
            player.collisionState = (byte) 1;
        }
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 512, y - 2560, 1024, 2560);
    }

    public void close() {
        this.system = null;
    }

    public static void releaseAllResource() {
        barImage = null;
    }

    public int getPaintLayer() {
        return 0;
    }

    public void doInitInCamera() {
        this.used = false;
        frameCnt = 0;
    }
}
