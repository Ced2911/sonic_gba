package SonicGBA;

import Lib.MyAPI;
import Lib.crlFP32;
import com.sega.mobile.define.MDPhone;
import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;

/* compiled from: GimmickObject */
class BallHobin extends HexHobin {
    private static final int COLLISION_HEIGHT = 1536;
    private static final int COLLISION_WIDTH = 1536;
    private static final int HOBIN_POWER = 1100;
    private static MFImage ballHobinImage = null;
    private boolean CanAddScore = true;
    private int initPos;
    private boolean isH;
    private int offset_distance;

    protected BallHobin(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        int i;
        if (ballHobinImage == null) {
            try {
                ballHobinImage = MFImage.createImage("/gimmick/ball_hobin.png");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (this.mWidth >= this.mHeight) {
            this.isH = true;
        } else {
            this.isH = false;
        }
        if (this.isH) {
            i = this.posX;
        } else {
            i = this.posY;
        }
        this.initPos = i;
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 768, y - 768, 1536, 1536);
    }

    public void logic() {
        this.moveCal.logic();
        int preX = this.posX;
        int preY = this.posY;
        if (this.isH) {
            if (this.iLeft == 0) {
                this.posX = this.moveCal.getPosition();
            } else {
                this.offset_distance = this.initPos - this.moveCal.getPosition();
                this.posX = this.initPos + this.offset_distance;
            }
        } else if (this.iTop == 0) {
            this.posY = this.moveCal.getPosition();
        } else {
            this.offset_distance = this.initPos - this.moveCal.getPosition();
            this.posY = this.initPos + this.offset_distance;
        }
        checkWithPlayer(preX, preY, this.posX, this.posY);
    }

    public void draw(MFGraphics g) {
        drawInMap(g, ballHobinImage, this.posX + this.hobinCal.getPosOffsetX(), this.posY + this.hobinCal.getPosOffsetY(), 3);
        this.hobinCal.logic();
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (this.firstTouch) {
            player.pipeOut();
            if (player.collisionState == (byte) 1 && object.animationID != 4) {
                object.beStop(this.collisionRect.y0, direction, this);
            }
            if (direction == 3) {
                player.rightStopped = false;
                if (((player instanceof PlayerKnuckles) && (player.getCharacterAnimationID() == 19 || player.getCharacterAnimationID() == 20 || player.getCharacterAnimationID() == 21 || player.getCharacterAnimationID() == 22 || player.getCharacterAnimationID() == 29 || player.getCharacterAnimationID() == 30 || player.getCharacterAnimationID() == 31 || player.getCharacterAnimationID() == 32 || player.getCharacterAnimationID() == 33)) || ((player instanceof PlayerTails) && (player.getCharacterAnimationID() == 12 || player.getCharacterAnimationID() == 13 || player.getCharacterAnimationID() == 14))) {
                    player.animationID = 4;
                }
            } else if (direction == 2) {
                player.leftStopped = false;
                if (((player instanceof PlayerKnuckles) && (player.getCharacterAnimationID() == 19 || player.getCharacterAnimationID() == 20 || player.getCharacterAnimationID() == 21 || player.getCharacterAnimationID() == 22 || player.getCharacterAnimationID() == 29 || player.getCharacterAnimationID() == 30 || player.getCharacterAnimationID() == 31 || player.getCharacterAnimationID() == 32 || player.getCharacterAnimationID() == 33)) || ((player instanceof PlayerTails) && (player.getCharacterAnimationID() == 12 || player.getCharacterAnimationID() == 13 || player.getCharacterAnimationID() == 14))) {
                    player.animationID = 4;
                }
            }
            if (player.collisionState != (byte) 0) {
                player.collisionState = (byte) 1;
            }
            player.dashRolling = false;
            int distanceX = player.getCheckPositionX() - this.collisionRect.getCenterX();
            int distanceY = player.getCheckPositionY() - this.collisionRect.getCenterY();
            if (distanceX != 0 || distanceY != 0) {
                int degree = (crlFP32.actTanDegree(distanceY, distanceX) + MDPhone.SCREEN_WIDTH) % MDPhone.SCREEN_WIDTH;
                this.hobinCal.startHobin(0, degree + RollPlatformSpeedC.DEGREE_VELOCITY, 10);
                if (this.CanAddScore) {
                    player.getBallHobinScore();
                    this.CanAddScore = false;
                }
                int powerY = (MyAPI.dSin(degree) * HOBIN_POWER) / 100;
                player.setVelX((MyAPI.dCos(degree) * HOBIN_POWER) / 100);
                player.setVelY(powerY);
                if (object.animationID != 4) {
                    player.doWalkPoseInAir();
                }
                soundInstance.playSe(54);
            }
        }
    }

    public void doWhileNoCollision() {
        this.CanAddScore = true;
    }

    public void close() {
    }

    public static void releaseAllResource() {
        ballHobinImage = null;
    }

    public int getPaintLayer() {
        return 0;
    }
}
