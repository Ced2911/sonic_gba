package SonicGBA;

import com.sega.mobile.define.MDPhone;
import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;

/* compiled from: GimmickObject */
class Platform extends GimmickObject {
    public static int COLLISION_HEIGHT = 1280;
    public static int COLLISION_OFFSET_Y = 256;
    public static final int COLLISION_WIDTH = 3072;
    public static final int DRAW_OFFSET_Y = 768;
    public static final int STAND_OFFSET = 192;
    public int COLLISION_HEIGHT_OFFSET = 0;
    public boolean IsDisplay = true;
    private int initPosx;
    private int initPosy;
    public boolean isH;
    public MoveCalculator moveCal;
    public int offsetY = 0;
    public int offsetY2 = 0;

    protected Platform(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        boolean moveDirection;
        if (platformImage == null) {
            try {
                if (StageManager.getCurrentZoneId() != 6) {
                    platformImage = MFImage.createImage("/gimmick/platform" + StageManager.getCurrentZoneId() + ".png");
                } else {
                    platformImage = MFImage.createImage("/gimmick/platform" + StageManager.getCurrentZoneId() + (StageManager.getStageID() - 9) + ".png");
                }
            } catch (Exception e) {
                e.printStackTrace();
                try {
                    platformImage = MFImage.createImage("/gimmick/platform0.png");
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        }
        if (StageManager.getStageID() == 5 && left == 5312) {
            this.COLLISION_HEIGHT_OFFSET = -DRAW_OFFSET_Y;
        }
        if (this.mWidth >= this.mHeight) {
            this.isH = true;
            if (this.iLeft == 0) {
                moveDirection = false;
            } else {
                moveDirection = true;
            }
        } else {
            this.isH = false;
            if (this.iTop == 0) {
                moveDirection = false;
            } else {
                moveDirection = true;
            }
        }
        this.moveCal = new MoveCalculator(this.isH ? this.posX : this.posY, this.isH ? this.mWidth : this.mHeight, moveDirection);
        this.initPosx = this.posX;
        this.initPosy = this.posY;
        this.IsDisplay = true;
    }

    public void setDisplay(boolean state) {
        this.IsDisplay = state;
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (this.IsDisplay && !player.isFootOnObject(this)) {
            switch (direction) {
                case 0:
                    if (player.isAntiGravity) {
                        object.beStop(this.collisionRect.y0, 0, this);
                        this.used = true;
                        return;
                    }
                    return;
                case 1:
                    if (!player.isAntiGravity) {
                        object.beStop(this.collisionRect.y0, 1, this);
                        this.used = true;
                        return;
                    }
                    return;
                case 4:
                    if (player.isAntiGravity || object.getMoveDistance().f14y <= 0) {
                        if (player.isAntiGravity && object.getMoveDistance().f14y < 0 && object.getCollisionRect().y0 > this.collisionRect.y0) {
                            object.beStop(this.collisionRect.y0, 0, this);
                            this.used = true;
                            return;
                        }
                        return;
                    } else if (((object.getCollisionRect().y0 + object.getCollisionRect().y1) / 2) - MDPhone.SCREEN_HEIGHT < this.collisionRect.y1) {
                        object.beStop(this.collisionRect.y0, 1, this);
                        this.used = true;
                        return;
                    } else {
                        return;
                    }
                default:
                    return;
            }
        }
    }

    public void draw(MFGraphics g) {
        if (this.IsDisplay) {
            drawInMap(g, platformImage, this.posX, (this.posY + 768) + this.offsetY, 33);
            drawCollisionRect(g);
        }
    }

    public void logic() {
        if (this.IsDisplay) {
            this.moveCal.logic();
            int preX = this.posX;
            int preY = this.posY;
            if (this.isH) {
                if (this.iLeft == 0) {
                    this.posX = this.moveCal.getPosition();
                } else {
                    this.posX = this.initPosx + (this.initPosx - this.moveCal.getPosition());
                }
            } else if (this.iTop == 0) {
                this.posY = this.moveCal.getPosition();
            } else {
                this.posY = this.initPosy + (this.initPosy - this.moveCal.getPosition());
            }
            if (player.isFootOnObject(this)) {
                this.offsetY = 192;
                if (player instanceof PlayerKnuckles) {
                    ((PlayerKnuckles) player).setFloating(false);
                }
            } else {
                this.offsetY = 0;
            }
            if (StageManager.getCurrentZoneId() >= 3 && StageManager.getCurrentZoneId() <= 6) {
                this.offsetY2 = GimmickObject.PLATFORM_OFFSET_Y;
            }
            checkWithPlayer(preX, preY, this.posX, this.posY);
        }
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - (COLLISION_WIDTH/2), (((COLLISION_OFFSET_Y + y) - DRAW_OFFSET_Y) + this.offsetY) + this.offsetY2, COLLISION_WIDTH, COLLISION_HEIGHT + this.COLLISION_HEIGHT_OFFSET);
        if (this.COLLISION_HEIGHT_OFFSET != 0) {
            System.out.println("COLLISION_HEIGHT+COLLISION_HEIGHT_OFFSET=" + (COLLISION_HEIGHT + this.COLLISION_HEIGHT_OFFSET));
        }
    }

    public boolean collisionChkWithObject(PlayerObject object) {
        CollisionRect objectRect = object.getCollisionRect();
        CollisionRect thisRect = getCollisionRect();
        rectV.setRect(objectRect.x0 + 192, objectRect.y0 + this.offsetY2, objectRect.getWidth() - PlayerSonic.BACK_JUMP_SPEED_X, objectRect.getHeight());
        return thisRect.collisionChk(rectV);
    }

    public int getPaintLayer() {
        return 0;
    }
}
