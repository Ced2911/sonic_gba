package SonicGBA;

/* compiled from: GimmickObject */
class AntiGravity extends GimmickObject {
    private boolean activeAfterNoCollison;
    private int enterPlayerX;
    private int iLeft;

    protected AntiGravity(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        this.iLeft = left;
        if (this.iLeft == 0 && this.iTop == 0) {
            this.activeAfterNoCollison = true;
        }
    }

    public void doWhileNoCollision() {
        if (this.activeAfterNoCollison && this.used && (player.getCheckPositionX() > this.enterPlayerX + 1000 || player.getCheckPositionX() < this.enterPlayerX - 1000)) {
            player.setAntiGravity(!player.isAntiGravity);
        }
        this.used = false;
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (object == player) {
            if (this.activeAfterNoCollison) {
                if (this.firstTouch) {
                    this.enterPlayerX = player.getCheckPositionX();
                    this.used = true;
                }
            } else if (!this.collisionRect.collisionChk(player.getCheckPositionX(), player.getCheckPositionY())) {
                this.used = false;
            } else if (!this.used) {
                switch (this.iLeft) {
                    case 0:
                        if (this.iTop != 0) {
                            player.setAntiGravity(false);
                            break;
                        } else {
                            player.setAntiGravity(!player.isAntiGravity);
                            break;
                        }
                    case 1:
                        player.setAntiGravity(false);
                        break;
                    case 2:
                        player.setAntiGravity(true);
                        break;
                }
                this.used = true;
            }
        }
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x, y, this.mWidth, this.mHeight);
    }
}
