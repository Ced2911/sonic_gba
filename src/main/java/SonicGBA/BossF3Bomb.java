package SonicGBA;

import Lib.Animation;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: BulletObject */
class BossF3Bomb extends BulletObject {
    private static final int BOOM_V1 = 720;
    private static final int BOOM_V2 = 360;
    private static final int COLLISION_HEIGHT = 1024;
    private static final int COLLISION_WIDTH = 1024;
    private int vel_x;
    private int vel_y;
    private int velorg_y;

    protected BossF3Bomb(int x, int y, int type, int direct) {
        super(x, y, 0, 0, false);
        if (bossf3bombAnimation == null) {
            bossf3bombAnimation = new Animation("/animation/bossf3_bullet");
        }
        this.drawer = bossf3bombAnimation.getDrawer(0, true, 0);
        if (direct == 1) {
            if (type == 0) {
                this.vel_x = BOOM_V1;
                this.vel_y = -720;
                this.velorg_y = -720;
            } else if (type == 1) {
                this.vel_x = 360;
                this.vel_y = -1080;
                this.velorg_y = -1080;
            }
        } else if (direct == 0) {
            if (type == 0) {
                this.vel_x = -720;
                this.vel_y = -720;
                this.velorg_y = -720;
            } else if (type == 1) {
                this.vel_x = -360;
                this.vel_y = -1080;
                this.velorg_y = -1080;
            }
        }
        this.posX = x;
        this.posY = y;
    }

    public void bulletLogic() {
        this.posX += this.vel_x;
        if (this.posY + this.vel_y >= getGroundY(this.posX, this.posY + this.vel_y)) {
            this.posY = getGroundY(this.posX, this.posY + this.vel_y);
            this.velorg_y = (this.velorg_y * 7) / 8;
            this.vel_y = this.velorg_y;
        } else {
            this.vel_y += GRAVITY;
            this.posY += this.vel_y;
        }
        refreshCollisionRect(this.posX, this.posY);
    }

    public void draw(MFGraphics g) {
        drawInMap(g, this.drawer);
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 512, y - 1024, 1024, 1024);
    }

    public boolean chkDestroy() {
        return !isInCamera() || isFarAwayCamera();
    }
}
