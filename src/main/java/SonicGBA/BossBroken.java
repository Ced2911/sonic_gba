package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import Lib.MyRandom;
import com.sega.mobile.framework.device.MFGraphics;
import java.lang.reflect.Array;

/* compiled from: EnemyObject */
class BossBroken extends EnemyObject {
    private static final int COLLISION_HEIGHT = 1;
    private static final int COLLISION_WIDTH = 1;
    private static final int MAX_BOOM = 8;
    private static Animation PartsAni;
    private static int frame;
    private boolean IsEnd;
    private int[] Vix = new int[]{750, SmallAnimal.FLY_VELOCITY_Y, -150, 150, 450, -750, 600, 300, -450};
    private int[] Viy = new int[]{-1200, -1050, PlayerObject.MIN_ATTACK_JUMP, -750, -600};
    private int[] back_offset;
    private int boomCount;
    private int[][] boomPos;
    private AnimationDrawer[] boomdrawers;
    private int enemyid;
    private int jump_time = 10;
    private int offsety = 0;
    private AnimationDrawer partsdrawer;
    private int[][] pos;
    private int range = 6400;
    private int startx;
    private int starty;
    private int time_cnt;
    private int totalBoom;
    private int total_cnt;
    private int total_cnt_max = 8;

    public static void releaseAllResource() {
        Animation.closeAnimation(BoomAni);
        Animation.closeAnimation(PartsAni);
        BoomAni = null;
        PartsAni = null;
    }

    protected BossBroken(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        int i;
        int[] iArr = new int[7];
        iArr[0] = -15;
        iArr[1] = -10;
        iArr[2] = -5;
        iArr[4] = 5;
        iArr[5] = 10;
        iArr[6] = 15;
        this.back_offset = iArr;
        this.posX -= this.iLeft * 8;
        this.posY -= this.iTop * 8;
        this.enemyid = id;
        switch (id) {
            case 22:
                this.offsety = 1728;
                break;
            case 23:
                this.offsety = 960;
                break;
            case 24:
            case 25:
                this.offsety = 0;
                break;
        }
        this.startx = this.posX;
        this.starty = this.posY - this.offsety;
        if (BoomAni == null) {
            BoomAni = new Animation("/animation/boom");
        }
        this.boomdrawers = new AnimationDrawer[8];
        for (i = 0; i < 8; i++) {
            this.boomdrawers[i] = BoomAni.getDrawer(0, false, 0);
        }
        this.boomPos = (int[][]) Array.newInstance(Integer.TYPE, new int[]{8, 2});
        this.boomCount = 0;
        this.totalBoom = 0;
        if (id != 26) {
            if (id == 27) {
                if (PartsAni == null) {
                    PartsAni = new Animation("/animation/boss6_parts");
                }
            } else if (PartsAni == null) {
                PartsAni = new Animation("/animation/parts");
            }
            this.partsdrawer = PartsAni.getDrawer(0, true, 0);
        }
        this.pos = (int[][]) Array.newInstance(Integer.TYPE, new int[]{3, 5});
        for (i = 0; i < this.pos.length; i++) {
            this.pos[i][0] = this.startx;
            this.pos[i][1] = this.starty;
            this.pos[i][2] = 0;
            this.pos[i][3] = 0;
            this.pos[i][4] = 0;
        }
        this.total_cnt_max = 8;
        this.jump_time = 10;
        frame = 1;
    }

    public void doWhileCollision(PlayerObject object, int direction) {
    }

    public void doWhileBeAttack(PlayerObject object, int direction, int animationID) {
    }

    public void logic() {
    }

    public void logicBoom(int carx, int cary) {
        if (!IsGamePause) {
            if (this.total_cnt <= (this.total_cnt_max >> 1)) {
                this.startx = carx;
                this.starty = cary;
            } else {
                this.startx = (this.back_offset[MyRandom.nextInt(this.back_offset.length)] << 6) + carx;
                this.starty = ((cary - 1344) + (this.back_offset[MyRandom.nextInt(this.back_offset.length)] << 6)) - this.offsety;
            }
            frame++;
            frame %= 11;
            if (frame % 2 == 0) {
                soundInstance.playSe(35);
            }
        }
        AnimationDrawer.setAllPause(IsGamePause);
    }

    public boolean getEndState() {
        return this.IsEnd;
    }

    public void draw(MFGraphics g) {
        if (this.total_cnt < this.total_cnt_max) {
            int i;
            if (!IsGamePause) {
                this.time_cnt++;
                if (this.time_cnt % this.jump_time == 0 || this.time_cnt == 0) {
                    this.total_cnt++;
                    for (i = 0; i < this.pos.length; i++) {
                        this.pos[i][0] = this.startx;
                        this.pos[i][1] = this.starty;
                        this.pos[i][2] = this.Vix[MyRandom.nextInt(this.Vix.length)];
                        this.pos[i][3] = this.Viy[MyRandom.nextInt(this.Viy.length)];
                        this.pos[i][4] = MyRandom.nextInt(6);
                    }
                }
                for (i = 0; i < this.pos.length; i++) {
                    int[] iArr = this.pos[i];
                    iArr[0] = iArr[0] + this.pos[i][2];
                    iArr = this.pos[i];
                    iArr[3] = iArr[3] + (GRAVITY >> 1);
                    iArr = this.pos[i];
                    iArr[1] = iArr[1] + this.pos[i][3];
                }
                if (this.total_cnt >= this.total_cnt_max) {
                    this.IsEnd = true;
                }
                this.boomCount++;
                this.boomCount %= 2;
                if (this.boomCount == 1) {
                    this.boomPos[this.totalBoom][0] = this.startx + ((MyRandom.nextInt(60) - 30) << 6);
                    this.boomPos[this.totalBoom][1] = this.starty + ((MyRandom.nextInt(60) - 30) << 6);
                    this.boomdrawers[this.totalBoom].restart();
                    this.totalBoom++;
                }
                if (this.totalBoom == 8) {
                    this.totalBoom = 0;
                }
            }
            for (i = 0; i <= this.totalBoom; i++) {
                if (!this.boomdrawers[i].checkEnd()) {
                    drawInMap(g, this.boomdrawers[i], this.boomPos[i][0], this.boomPos[i][1]);
                }
            }
            if (this.enemyid != 26) {
                for (i = 0; i < this.pos.length; i++) {
                    this.partsdrawer.setActionId(this.pos[i][4]);
                    drawInMap(g, this.partsdrawer, this.pos[i][0], this.pos[i][1]);
                }
            }
        }
    }

    public void setTotalCntMax(int time) {
        this.total_cnt_max = time;
    }

    public void setJumpTime(int time) {
        this.jump_time = time;
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x, y, 1, 1);
    }

    public void close() {
        for (int i = 0; i < 8; i++) {
            this.boomdrawers[i] = null;
        }
        this.boomdrawers = null;
        this.partsdrawer = null;
        this.pos = null;
    }
}
