package SonicGBA;

import Lib.Animation;
import Lib.AnimationDrawer;
import Lib.Coordinate;
import Lib.MyAPI;
import com.sega.mobile.framework.device.MFDevice;
import com.sega.mobile.framework.device.MFGamePad;
import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;
import java.io.DataInputStream;
import java.io.InputStream;
import java.lang.reflect.Array;

public class MapManager implements SonicDef {
    public static final int CAMERA_HEIGHT = (SCREEN_HEIGHT - 0);
    private static final int CAMERA_MAX_SPEED_X = 48;
    private static final int CAMERA_MAX_SPEED_Y = 48;
    private static final int CAMERA_SPEED = 5;
    public static final int CAMERA_WIDTH = SCREEN_WIDTH;
    public static final int CAMERA_OFFSET_X = ((SCREEN_WIDTH - CAMERA_WIDTH) >> 1);
    public static final int CAMERA_OFFSET_Y = ((SCREEN_HEIGHT - CAMERA_HEIGHT) >> 1);
    private static final int CAM_OFF = 5;
    public static final int COLOR_SPACE = 20;
    public static final int END_COLOR = 16777215;
    private static int IMAGE_TILE_WIDTH = 16;
    private static final int LINE_HEIGHT = 2;
    private static final int LOAD_BACK = 5;
    private static final int LOAD_CAMERA_RESET = 6;
    private static final int LOAD_FRONT = 4;
    private static final int LOAD_MAP_IMAGE = 0;
    private static final int LOAD_MODEL = 3;
    private static final int LOAD_OPEN_FILE = 1;
    private static final int LOAD_OVERALL = 2;
    private static final int WIND_LOOP_WIDTH = 480;
    private static final int LOOP_COUNT = (((SCREEN_WIDTH + 960) - 1) / WIND_LOOP_WIDTH);
    public static final String MAP_EXTEND_NAME = ".pm";
    private static final boolean MODEL_CACHE_DRAW = false;
    private static final int MODEL_HEIGHT = 6;
    private static final int MODEL_WIDTH = 6;
    private static final boolean NO_LINE = true;
    public static final String PNG_NAME = "/stage";
    private static final int RECT_FRAME_WIDTH = 40;
    private static final int SHAKE_RANGE = 6;
    public static final int START_COLOR = 6067452;
    public static final int START_COLOR_2 = 15964672;
    private static final int TILE_HEIGHT = 16;
    private static final int TILE_WIDTH = 16;
    private static final int[][] WIND_POSITION;
    public static int actualDownCameraLimit;
    public static int actualLeftCameraLimit;
    public static int actualRightCameraLimit;
    public static int actualUpCameraLimit;
    private static int brokeOffsetY;
    private static int brokePointY;
    private static Coordinate camera = new Coordinate();
    private static int cameraActionX = 2;
    private static int cameraActionY = 2;
    private static boolean cameraLocked;
    private static boolean cameraUpDownLocked;
    private static DataInputStream ds;
    private static Focusable focusObj;
    public static int gameFrame = 0;
    public static MFImage image;
    private static InputStream is;
    private static int loadStep = 0;
    public static short[][] mapBack;
    public static short[][] mapFront;
    public static int mapHeight;
    private static int mapLoopLeft;
    private static int mapLoopRight;
    public static short[][][] mapModel;
    public static int mapOffsetX;
    public static int mapVelX = -30;
    public static int mapWidth;
    private static int mappaintframe = 0;
    private static MFImage[] modelImageArray;
    private static int[] modelRGB = new int[9216];
    public static int proposeDownCameraLimit;
    public static int proposeLeftCameraLimit;
    public static int proposeRightCameraLimit;
    public static int proposeUpCameraLimit;
    public static Coordinate reCamera = new Coordinate();
    private static int shakeCount;
    private static int shakeMaxCount;
    private static int shakePowerX;
    private static int shakePowerY;
    private static boolean shakingUp;
    private static boolean stageFlag = false;
    private static int stage_id;
    public static MFImage[] tileimage;
    private static AnimationDrawer windDrawer;
    private static MFImage windImage;
    private static int[] zone4TileLoopID;


    static {
        for (int i = 0; i < modelRGB.length; i++) {
            modelRGB[i] = modelRGB[i] | 0xFF000000;
        }
        /*
        r1 = new int[8][];
        int[] iArr = new int[]{24, iArr, new int[]{60, 80, 1}};
        int[] iArr2 = new int[]{100, 1, iArr2};
        iArr = new int[]{120, 44, iArr};
        r1[4] = new int[]{216, 58, 1};
        iArr2 = new int[]{296, 12, iArr2};
        iArr2 = new int[]{352, 72, iArr2};
        r1[7] = new int[]{386, 36, 1};
        WIND_POSITION = r1;
        */
        WIND_POSITION = new int[8][];
        int[] iArr3 = new int[72];
        iArr3[1] = 1;
        iArr3[2] = 2;
        iArr3[3] = 3;
        iArr3[4] = 4;
        iArr3[5] = 4;
        iArr3[6] = 4;
        iArr3[7] = 4;
        iArr3[8] = 4;
        iArr3[9] = 4;
        iArr3[10] = 4;
        iArr3[11] = 4;
        iArr3[12] = 4;
        iArr3[13] = 4;
        iArr3[14] = 4;
        iArr3[15] = 4;
        iArr3[16] = 4;
        iArr3[17] = 4;
        iArr3[18] = 4;
        iArr3[19] = 4;
        iArr3[20] = 4;
        iArr3[21] = 4;
        iArr3[22] = 4;
        iArr3[23] = 4;
        iArr3[24] = 5;
        iArr3[25] = 6;
        iArr3[26] = 7;
        iArr3[27] = 8;
        iArr3[28] = 9;
        iArr3[29] = 9;
        iArr3[30] = 9;
        iArr3[31] = 9;
        iArr3[32] = 9;
        iArr3[33] = 9;
        iArr3[34] = 9;
        iArr3[35] = 9;
        iArr3[36] = 10;
        iArr3[37] = 11;
        iArr3[38] = 12;
        iArr3[39] = 13;
        iArr3[40] = 14;
        iArr3[41] = 14;
        iArr3[42] = 14;
        iArr3[43] = 14;
        iArr3[44] = 14;
        iArr3[45] = 14;
        iArr3[46] = 14;
        iArr3[47] = 14;
        iArr3[48] = 15;
        iArr3[49] = 16;
        iArr3[50] = 17;
        iArr3[51] = 18;
        iArr3[52] = 19;
        iArr3[53] = 19;
        iArr3[54] = 19;
        iArr3[55] = 19;
        iArr3[56] = 19;
        iArr3[57] = 19;
        iArr3[58] = 19;
        iArr3[59] = 19;
        iArr3[60] = 19;
        iArr3[61] = 19;
        iArr3[62] = 19;
        iArr3[63] = 19;
        iArr3[64] = 19;
        iArr3[65] = 19;
        iArr3[66] = 19;
        iArr3[67] = 19;
        iArr3[68] = 19;
        iArr3[69] = 19;
        iArr3[70] = 19;
        iArr3[71] = 19;
        zone4TileLoopID = iArr3;
    }

    public static void cameraLogic() {
        if (focusObj != null && !cameraLocked) {
            if (getPixelWidth() < CAMERA_WIDTH) {
                camera.f13x = (CAMERA_WIDTH - getPixelWidth()) / 2;
            } else {
                cameraActionX();
                if (actualLeftCameraLimit > proposeLeftCameraLimit) {
                    actualLeftCameraLimit -= 5;
                    if (actualLeftCameraLimit < proposeLeftCameraLimit) {
                        actualLeftCameraLimit = proposeLeftCameraLimit;
                    }
                } else if (actualLeftCameraLimit < proposeLeftCameraLimit) {
                    actualLeftCameraLimit += 5;
                    if (actualLeftCameraLimit > proposeLeftCameraLimit) {
                        actualLeftCameraLimit = proposeLeftCameraLimit;
                    }
                }
                if (actualRightCameraLimit < proposeRightCameraLimit) {
                    actualRightCameraLimit += 5;
                    if (actualRightCameraLimit > proposeRightCameraLimit) {
                        actualRightCameraLimit = proposeRightCameraLimit;
                    }
                } else if (actualRightCameraLimit > proposeRightCameraLimit) {
                    actualRightCameraLimit -= 5;
                    if (actualRightCameraLimit < proposeRightCameraLimit) {
                        actualRightCameraLimit = proposeRightCameraLimit;
                    }
                }
                if (camera.f13x < actualLeftCameraLimit - CAMERA_OFFSET_X) {
                    camera.f13x = actualLeftCameraLimit - CAMERA_OFFSET_X;
                    cameraActionX = 0;
                }
                if (actualRightCameraLimit != getPixelWidth() && camera.f13x > (actualRightCameraLimit - CAMERA_WIDTH) - CAMERA_OFFSET_X) {
                    camera.f13x = (actualRightCameraLimit - CAMERA_WIDTH) - CAMERA_OFFSET_X;
                    cameraActionX = 0;
                }
                if (actualRightCameraLimit != getPixelWidth() && Math.abs(actualRightCameraLimit - actualLeftCameraLimit) < CAMERA_WIDTH) {
                    camera.f13x = ((actualRightCameraLimit + actualLeftCameraLimit) - CAMERA_WIDTH) >> 1;
                    cameraActionX = 0;
                }
            }
            if (!cameraUpDownLocked) {
                if (getPixelHeight() < CAMERA_HEIGHT) {
                    camera.f14y = (CAMERA_HEIGHT - getPixelHeight()) / 2;
                } else {
                    cameraActionY();
                    if (actualDownCameraLimit > proposeDownCameraLimit) {
                        actualDownCameraLimit -= 5;
                        if (actualDownCameraLimit < proposeDownCameraLimit) {
                            actualDownCameraLimit = proposeDownCameraLimit;
                        }
                    } else if (actualDownCameraLimit < proposeDownCameraLimit) {
                        actualDownCameraLimit += 5;
                        if (actualDownCameraLimit > proposeDownCameraLimit) {
                            actualDownCameraLimit = proposeDownCameraLimit;
                        }
                    }
                    if (actualUpCameraLimit < proposeUpCameraLimit) {
                        actualUpCameraLimit += 5;
                        if (actualUpCameraLimit > proposeUpCameraLimit) {
                            actualUpCameraLimit = proposeUpCameraLimit;
                        }
                    } else if (actualUpCameraLimit > proposeUpCameraLimit) {
                        actualUpCameraLimit -= 5;
                        if (actualUpCameraLimit < proposeUpCameraLimit) {
                            actualUpCameraLimit = proposeUpCameraLimit;
                        }
                    }
                    if (camera.f14y < actualUpCameraLimit - CAMERA_OFFSET_Y) {
                        camera.f14y = actualUpCameraLimit - CAMERA_OFFSET_Y;
                        cameraActionY = 0;
                    } else if (camera.f14y > (actualDownCameraLimit - CAMERA_HEIGHT) - CAMERA_OFFSET_Y) {
                        camera.f14y = (actualDownCameraLimit - CAMERA_HEIGHT) - CAMERA_OFFSET_Y;
                        cameraActionY = 0;
                    }
                }
            }
            Coordinate coordinate = camera;
            coordinate.f13x += shakePowerX;
            shakePowerX = 0;
            if (shakeCount > 0) {
                boolean z;
                shakeCount--;
                int shakeRange = (shakeCount * shakePowerY) / shakeMaxCount;
                coordinate = camera;
                coordinate.f14y += shakingUp ? shakeRange : -shakeRange;
                if (shakingUp) {
                    z = false;
                } else {
                    z = true;
                }
                shakingUp = z;
                if (camera.f13x < 0) {
                    camera.f13x = 0;
                }
                if (camera.f13x + CAMERA_WIDTH > getPixelWidth()) {
                    camera.f13x = getPixelWidth() - CAMERA_WIDTH;
                }
                if (camera.f14y < 0) {
                    camera.f14y = 0;
                }
                if (camera.f14y + CAMERA_HEIGHT > getPixelHeight()) {
                    camera.f14y = getPixelHeight() - CAMERA_HEIGHT;
                }
            }
            if (StageManager.getCurrentZoneId() == 8) {
                mapOffsetX += mapVelX;
                if ((-mapOffsetX) + camera.f13x > getPixelWidth() - CAMERA_WIDTH && Math.abs(mapOffsetX) < 0) {
                    mapOffsetX += 448;
                }
            }
        }
    }

    private static void cameraActionX() {
        int desCamX = (focusObj.getFocusX() - (CAMERA_WIDTH >> 1)) - CAMERA_OFFSET_X;
        switch (cameraActionX) {
            case 0:
                int preCameraX = camera.f13x;
                camera.f13x = MyAPI.calNextPosition((double) camera.f13x, (double) desCamX, 3, 6, 4.0d);
                if (Math.abs(preCameraX - camera.f13x) > 48) {
                    if (camera.f13x > preCameraX) {
                        camera.f13x = preCameraX + 48;
                    }
                    if (camera.f13x < preCameraX) {
                        camera.f13x = preCameraX - 48;
                        return;
                    }
                    return;
                }
                return;
            case 1:
                int destiny;
                if (desCamX < 0) {
                    destiny = 0;
                } else {
                    destiny = desCamX;
                }
                int move = ((destiny - camera.f13x) * 100) / 5;
                Coordinate coordinate = camera;
                int i = coordinate.f13x;
                int i2 = move / 100;
                int i3 = move == 0 ? 0 : move > 0 ? 5 : -5;
                coordinate.f13x = i + (i2 + i3);
                if (((destiny * 100) - (camera.f13x * 100)) * move <= 0) {
                    cameraActionX = 0;
                    return;
                }
                return;
            case 2:
                camera.f13x = desCamX;
                cameraActionX = 0;
                return;
            default:
                return;
        }
    }

    private static void cameraActionY() {
        int desCamY = (focusObj.getFocusY() - (CAMERA_HEIGHT >> 1)) - CAMERA_OFFSET_Y;
        switch (cameraActionY) {
            case 0:
                int preCameraY = camera.f14y;
                camera.f14y = MyAPI.calNextPosition((double) camera.f14y, (double) desCamY, 3, 6, 4.0d);
                if (Math.abs(preCameraY - camera.f14y) > 48) {
                    if (camera.f14y > preCameraY) {
                        camera.f14y = preCameraY + 48;
                    }
                    if (camera.f14y < preCameraY) {
                        camera.f14y = preCameraY - 48;
                        return;
                    }
                    return;
                }
                return;
            case 1:
                int destiny = focusObj.getFocusY() - (CAMERA_HEIGHT >> 1) < 0 ? 0 : focusObj.getFocusY() - (CAMERA_HEIGHT >> 1);
                int move = ((destiny - camera.f14y) * 100) / 5;
                Coordinate coordinate = camera;
                int i = coordinate.f14y;
                int i2 = move / 100;
                int i3 = move == 0 ? 0 : move > 0 ? 5 : -5;
                coordinate.f14y = i + (i2 + i3);
                if (((destiny * 100) - (camera.f14y * 100)) * move <= 0) {
                    cameraActionY = 0;
                    return;
                }
                return;
            case 2:
                camera.f14y = desCamY;
                cameraActionY = 0;
                return;
            default:
                return;
        }
    }

    public static void setCameraLeftLimit(int limit) {
        proposeLeftCameraLimit = limit;
        if (proposeLeftCameraLimit <= camera.f13x) {
            actualLeftCameraLimit = proposeLeftCameraLimit;
        } else {
            actualLeftCameraLimit = camera.f13x;
        }
    }

    public static void setCameraRightLimit(int limit) {
        proposeRightCameraLimit = limit;
        if (proposeRightCameraLimit >= camera.f13x + CAMERA_WIDTH) {
            actualRightCameraLimit = proposeRightCameraLimit;
        } else {
            actualRightCameraLimit = camera.f13x + CAMERA_WIDTH;
        }
    }

    public static void setCameraDownLimit(int limit) {
        proposeDownCameraLimit = limit;
        actualDownCameraLimit = camera.f14y + CAMERA_HEIGHT;
    }

    public static void setCameraUpLimit(int limit) {
        proposeUpCameraLimit = limit;
        if (camera.f14y < proposeUpCameraLimit) {
            actualUpCameraLimit = camera.f14y;
        } else {
            actualUpCameraLimit = proposeUpCameraLimit;
        }
    }

    public static void calCameraImmidiately() {
        actualUpCameraLimit = proposeUpCameraLimit;
        actualDownCameraLimit = proposeDownCameraLimit;
        actualLeftCameraLimit = proposeLeftCameraLimit;
        actualRightCameraLimit = proposeRightCameraLimit;
    }

    public static void releaseCameraUpLimit() {
        proposeUpCameraLimit = 0;
    }

    public static void releaseCameraLeftLimit(int limit) {
        proposeLeftCameraLimit = 0;
    }

    public static void releaseCameraRightLimit(int limit) {
        proposeRightCameraLimit = getPixelWidth();
    }

    public static int getCameraRightLimit() {
        return proposeRightCameraLimit;
    }

    public static boolean isCameraStop() {
        if (focusObj == null || cameraLocked) {
            return true;
        }
        if (cameraActionX == 0 && cameraActionY == 0) {
            return true;
        }
        return false;
    }

    public static Coordinate getCamera() {
        return camera;
    }

    public static void setFocusObj(Focusable obj) {
        if (focusObj != obj) {
            cameraActionX = 1;
            cameraActionY = 1;
        }
        focusObj = obj;
        lockCamera(false);
    }

    public static void focusQuickLocation() {
        cameraActionX = 0;
        cameraActionY = 0;
        if (focusObj != null) {
            camera.f13x = focusObj.getFocusX() - (CAMERA_WIDTH >> 1);
            camera.f14y = focusObj.getFocusY() - (CAMERA_HEIGHT >> 1);
        }
        cameraLogic();
    }

    public static void setCameraMoving() {
        cameraActionX = 1;
        cameraActionY = 1;
        cameraLogic();
    }

    public static void lockCamera(boolean lock) {
        cameraLocked = lock;
        if (!lock) {
            cameraUpDownLocked = false;
        }
    }

    public static void lockUpDownCamera(boolean lock) {
        cameraUpDownLocked = lock;
    }

    public static boolean loadMapStep_error(int i, String s)
    {
        int i0 = loadStep;
        label9: {
            Exception a = null;
            label0: {
                switch(i0){
                    case 6: {
                        proposeLeftCameraLimit = 0;
                        actualLeftCameraLimit = 0;
                        proposeUpCameraLimit = 0;
                        actualUpCameraLimit = 0;
                        proposeRightCameraLimit = SonicGBA.MapManager.getPixelWidth();
                        actualRightCameraLimit = SonicGBA.MapManager.getPixelWidth();
                        proposeDownCameraLimit = SonicGBA.MapManager.getPixelHeight();
                        actualDownCameraLimit = SonicGBA.MapManager.getPixelHeight();
                        loadStep = 0;
                        mapOffsetX = 0;
                        shakeCount = 0;
                        brokePointY = 0;
                        brokeOffsetY = 0;
                        SonicGBA.MapManager.setMapLoop(mapWidth - 4, mapWidth);
                        if (SonicGBA.StageManager.getCurrentZoneId() == 8)
                        {
                            SonicGBA.MapManager.setCameraRightLimit(480);
                            SonicGBA.MapManager.setMapLoop(mapWidth - 28, mapWidth);
                            SonicGBA.MapManager.calCameraImmidiately();
                        }
                        if (SonicGBA.StageManager.getStageID() == 10)
                        {
                            SonicGBA.MapManager.setCameraRightLimit(SonicGBA.MapManager.getPixelWidth() - 1);
                        }
                        return true;
                    }
                    case 5: {
                        int i1 = 0;
                        try
                        {
                            while(i1 < mapWidth)
                            {
                                int i2 = 0;
                                while(i2 < mapHeight)
                                {
                                    short[] a0 = mapBack[i1];
                                    int i3 = ds.readShort();
                                    a0[i2] = (short)i3;
                                    i2 = i2 + 1;
                                }
                                i1 = i1 + 1;
                            }
                            if (ds == null)
                            {
                                break;
                            }
                            ds.close();
                            break;
                        }
                        catch(Exception ignoredException)
                        {
                            break;
                        }
                    }
                    case 4: {
                        int i4 = 0;
                        while(i4 < mapWidth)
                        {
                            int i5 = 0;
                            while(i5 < mapHeight)
                            {
                                try
                                {
                                    short[] a1 = mapFront[i4];
                                    int i6 = ds.readShort();
                                    a1[i5] = (short)i6;
                                    i5 = i5 + 1;
                                }
                                catch(Exception ignoredException0)
                                {
                                    break label9;
                                }
                            }
                            i4 = i4 + 1;
                        }
                        break;
                    }
                    case 3: {
                        Exception a2 = null;
                        label8: {
                            int i7 = 0;
                            java.io.DataInputStream a3 = null;
                            try
                            {
                                i7 = ds.readShort();
                                a3 = ds;
                            }
                            catch(Exception a4)
                            {
                                a2 = a4;
                                break label8;
                            }
                            try
                            {
                                a3.readShort();
                                int[] a5 = new int[3];
                                a5[0] = i7;
                                a5[1] = 6;
                                a5[2] = 6;
                                mapModel = (short[][][])java.lang.reflect.Array.newInstance(Short.TYPE, a5);
                                int i8 = 0;
                                while(i8 < i7)
                                {
                                    int i9 = 0;
                                    while(i9 < 6)
                                    {
                                        int i10 = 0;
                                        while(i10 < 6)
                                        {
                                            short[] a6 = mapModel[i8][i9];
                                            int i11 = ds.readShort();
                                            a6[i10] = (short)i11;
                                            i10 = i10 + 1;
                                        }
                                        i9 = i9 + 1;
                                    }
                                    i8 = i8 + 1;
                                }
                                break;
                            }
                            catch(Exception a7)
                            {
                                a2 = a7;
                            }
                        }
                        a2.printStackTrace();
                        break;
                    }
                    case 2: {
                        short[][] a8 = null;
                        label7: {
                            {
                                int i12 = 0;
                                try
                                {
                                    int i13 = ds.readByte();
                                    mapWidth = i13;
                                    int i14 = ds.readByte();
                                    mapHeight = i14;
                                    i12 = mapWidth;
                                }
                                catch(Exception ignoredException1)
                                {
                                    break;
                                }
                                if (i12 < 0)
                                {
                                    mapWidth = 256 + mapWidth;
                                }
                                if (mapHeight < 0)
                                {
                                    mapHeight = 256 + mapHeight;
                                }
                                int[] a9 = new int[2];
                                a9[0] = mapWidth;
                                a9[1] = mapHeight;
                                Class a10 = Short.TYPE;
                                try
                                {
                                    mapFront = (short[][])java.lang.reflect.Array.newInstance(a10, a9);
                                    int[] a11 = new int[2];
                                    a11[0] = mapWidth;
                                    a11[1] = mapHeight;
                                    a8 = (short[][])java.lang.reflect.Array.newInstance(Short.TYPE, a11);
                                    break label7;
                                }
                                catch(Exception ignoredException2)
                                {
                                }
                            }
                            break;
                        }
                        mapBack = a8;
                        break;
                    }
                    case 1: {
                        is = com.sega.mobile.framework.device.MFDevice.getResourceAsStream(new StringBuilder("/map/").append(s).append(".pm").toString());
                        ds = new java.io.DataInputStream(is);
                        break;
                    }
                    case 0: {
                        stage_id = i;
                        label6: {
                            label4: {
                                label5: {
                                    if (i == 0)
                                    {
                                        break label5;
                                    }
                                    if (i == 1)
                                    {
                                        break label5;
                                    }
                                    if (i == 4)
                                    {
                                        break label5;
                                    }
                                    if (i == 5)
                                    {
                                        break label5;
                                    }
                                    if (i == 6)
                                    {
                                        break label5;
                                    }
                                    if (i == 7)
                                    {
                                        break label5;
                                    }
                                    if (i == 8)
                                    {
                                        break label5;
                                    }
                                    if (i == 9)
                                    {
                                        break label5;
                                    }
                                    if (i == 10)
                                    {
                                        break label5;
                                    }
                                    if (i != 11)
                                    {
                                        break label4;
                                    }
                                }
                                stageFlag = true;
                                break label6;
                            }
                            try
                            {
                                stageFlag = false;
                                image = com.sega.mobile.framework.device.MFImage.createImage(new StringBuilder("/map/stage").append(s).append(".png").toString());
                            }
                            catch(Exception a12)
                            {
                                a = a12;
                                break label0;
                            }
                        }
                        try
                        {
                            if (stageFlag)
                            {
                                int i15 = 0;
                                switch(i){
                                    case 11: {
                                        i15 = 16;
                                        break;
                                    }
                                    case 8: case 9: {
                                        i15 = 4;
                                        break;
                                    }
                                    case 6: case 7: {
                                        i15 = 20;
                                        break;
                                    }
                                    case 0: case 1: case 4: case 5: case 10: {
                                        i15 = 8;
                                        break;
                                    }
                                    default: {
                                        i15 = 0;
                                    }
                                }
                                tileimage = new com.sega.mobile.framework.device.MFImage[i15];
                                label3: {
                                    label2: {
                                        if (i == 0)
                                        {
                                            break label2;
                                        }
                                        if (i == 1)
                                        {
                                            break label2;
                                        }
                                        if (i == 4)
                                        {
                                            break label2;
                                        }
                                        if (i == 5)
                                        {
                                            break label2;
                                        }
                                        if (i == 10)
                                        {
                                            break label2;
                                        }
                                        label1: {
                                            if (i != 11)
                                            {
                                                break label1;
                                            }
                                            break label2;
                                        }
                                        tileimage[0] = com.sega.mobile.framework.device.MFImage.createImage(new StringBuilder("/map/stage").append(s).append("/#1.png").toString());
                                        int i16 = 1;
                                        while(i16 < i15)
                                        {
                                            tileimage[i16] = com.sega.mobile.framework.device.MFImage.createPaletteImage(new StringBuilder("/map/stage").append(s).append("/#").append(i16 + 1).append(".pal").toString());
                                            i16 = i16 + 1;
                                        }
                                        break label3;
                                    }
                                    int i17 = 0;
                                    while(i17 < i15)
                                    {
                                        tileimage[i17] = com.sega.mobile.framework.device.MFImage.createImage(new StringBuilder("/map/stage").append(s).append("/#").append(i17 + 1).append(".png").toString());
                                        i17 = i17 + 1;
                                    }
                                }
                                IMAGE_TILE_WIDTH = Lib.MyAPI.zoomIn(tileimage[0].getWidth() / 16);
                                break;
                            }
                            else
                            {
                                IMAGE_TILE_WIDTH = Lib.MyAPI.zoomIn(image.getWidth() / 16);
                                break;
                            }
                        }
                        catch(Exception a13)
                        {
                            a = a13;
                            break label0;
                        }
                    }
                }
                break label9;
            }
            a.printStackTrace();
        }
        loadStep = 1 + loadStep;
        return false;
    }


    public static boolean loadMapStep(int stageId, String stageName) {
        int imageNum;int stageId2;
        switch (loadStep) {
            // Load pal/Images
            case LOAD_MAP_IMAGE:
                try {
                    stage_id = stageId;
                    if (stageId == 0 || stageId == 1 || stageId == 4 || stageId == 5 || stageId == 6 || stageId == 7 || stageId == 8 || stageId == 9 || stageId == 10 || stageId == 11) {
                        stageFlag = true;
                    } else {
                        stageFlag = false;
                        image = MFImage.createImage("/map/stage" + stageName + ".png");
                    }
                    if (!stageFlag) {
                        IMAGE_TILE_WIDTH = MyAPI.zoomIn(image.getWidth() / 16);
                        break;
                    }
                    imageNum = 0;
                    switch (stageId) {
                        case 0:
                        case 1:
                        case 4:
                        case 5:
                        case 10:
                            imageNum = 8;
                            break;
                        case 6:
                        case 7:
                            imageNum = 20;
                            break;
                        case 8:
                        case 9:
                            imageNum = 4;
                            break;
                        case 11:
                            imageNum = 16;
                            break;
                    }
                    tileimage = new MFImage[imageNum];
                    if (stageId == 0 || stageId == 1 || stageId == 4 || stageId == 5 || stageId == 10 || stageId == 11) {
                        for (stageId = 0; stageId < imageNum; stageId++) {
                            tileimage[stageId] = MFImage.createImage("/map/stage" + stageName + "/#" + (stageId + 1) + ".png");
                        }
                    } else {
                        tileimage[0] = MFImage.createImage("/map/stage" + stageName + "/#1.png");
                        for (stageId = 1; stageId < imageNum; stageId++) {
                            tileimage[stageId] = MFImage.createPaletteImage("/map/stage" + stageName + "/#" + (stageId + 1) + ".pal");
                        }
                    }
                    //IMAGE_TILE_WIDTH = MyAPI.zoomIn(tileimage[null].getWidth() / 16);
                    IMAGE_TILE_WIDTH = MyAPI.zoomIn(tileimage[0].getWidth() / 16);
                    break;
                } catch (Exception e) {
                    e.printStackTrace();
                    break;
                }
            case LOAD_OPEN_FILE:
                is = MFDevice.getResourceAsStream("/map/" + stageName + MAP_EXTEND_NAME);
                ds = new DataInputStream(is);
                break;
            // Read map dimensions
            case LOAD_OVERALL:
                try {
                    mapWidth = ds.readByte();
                    mapHeight = ds.readByte();
                    if (mapWidth < 0) {
                        mapWidth += 256;
                    }
                    if (mapHeight < 0) {
                        mapHeight += 256;
                    }
                    mapFront = (short[][]) Array.newInstance(Short.TYPE, new int[]{mapWidth, mapHeight});
                    mapBack = (short[][]) Array.newInstance(Short.TYPE, new int[]{mapWidth, mapHeight});

                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case LOAD_MODEL:
                try {
                    imageNum = ds.readShort();

                    if (imageNum <0) {
                        System.err.println("LOAD_MODEL (imageNum <0)");
                    }

                    ds.readShort(); // used ???
                    mapModel = (short[][][]) Array.newInstance(Short.TYPE, new int[]{imageNum, MODEL_WIDTH, MODEL_HEIGHT});
                    for (int n = 0; n < imageNum; n++) {
                        for (int x = 0; x < MODEL_WIDTH; x++) {
                            for (int y  = 0; y < MODEL_HEIGHT; y++) {
                                mapModel[n][x][y] = (short)(ds.readShort() & 0xFFFF);
                            }
                        }
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
                break;
            case LOAD_FRONT:
                for(int x = 0; x < mapWidth; x++) {
                    try {
                        for (int i = 0; i < mapHeight; i++) {
                            mapFront[x][i] = ds.readShort();
                        }
                    } catch (Exception e3) {
                        e3.printStackTrace();
                    }
                }
                break;
            case LOAD_BACK:
                for(int x = 0; x < mapWidth; x++) {
                    try {
                        for (int i = 0; i < mapHeight; i++) {
                            mapBack[x][i] = ds.readShort();
                        }
                    } catch (Exception e4) {
                        e4.printStackTrace();
                    }
                }
                try {
                    if (ds != null) {
                        ds.close();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case LOAD_CAMERA_RESET:
                proposeLeftCameraLimit = 0;
                actualLeftCameraLimit = 0;
                proposeUpCameraLimit = 0;
                actualUpCameraLimit = 0;
                proposeRightCameraLimit = getPixelWidth();
                actualRightCameraLimit = getPixelWidth();
                proposeDownCameraLimit = getPixelHeight();
                actualDownCameraLimit = getPixelHeight();
                loadStep = 0;
                mapOffsetX = 0;
                shakeCount = 0;
                brokePointY = 0;
                brokeOffsetY = 0;
                setMapLoop(mapWidth - 4, mapWidth);
                switch (StageManager.getCurrentZoneId()) {
                    case 8:
                        setCameraRightLimit(WIND_LOOP_WIDTH);
                        setMapLoop(mapWidth - 28, mapWidth);
                        calCameraImmidiately();
                        break;
                }
                switch (StageManager.getStageID()) {
                    case 10:
                        setCameraRightLimit(getPixelWidth() - 1);
                        break;
                }
                // Finished ?
                return true;
        }
        // Soemthing wrong !?
        loadStep++;
        return false;
    }

    public static void closeMap() {
        image = null;
        if (tileimage != null) {
            for (int i = 0; i < tileimage.length; i++) {
                tileimage[i] = null;
            }
        }
        tileimage = null;
        mapModel = null;
        mapFront = null;
        mapBack = null;
        windImage = null;
        windDrawer = null;
    }

    public static void drawBack(MFGraphics g) {
        BackGroundManager.drawBackGround(g);
        drawMap(g, mapBack);
    }

    public static void drawFrontNatural(MFGraphics g) {
        BackGroundManager.drawFrontNatural(g);
    }

    private static void drawWind(MFGraphics g) {
        if (windImage == null) {
            windImage = MFImage.createImage("/animation/bg_cloud_" + StageManager.getCurrentZoneId() + ".png");
            windDrawer = new Animation(windImage, "/animation/bg_cloud").getDrawer();
        }
        if (windImage != null && windDrawer != null) {
            int offsetX = (camera.f13x / 8) % WIND_LOOP_WIDTH;
            for (int i = -1; i < LOOP_COUNT; i++) {
                for (int j = 0; j < WIND_POSITION.length; j++) {
                    if ((WIND_POSITION[j][0] - offsetX) + (i * WIND_LOOP_WIDTH) >= (-(SCREEN_WIDTH >> 1))) {
                        if ((WIND_POSITION[j][0] - offsetX) + (i * WIND_LOOP_WIDTH) > SCREEN_WIDTH) {
                            break;
                        }
                        windDrawer.setActionId(WIND_POSITION[j][2]);
                        windDrawer.draw(g, (WIND_POSITION[j][0] - offsetX) + (i * WIND_LOOP_WIDTH), WIND_POSITION[j][1]);
                    }
                }
            }
        }
    }

    private static void fillChangeColorRect(MFGraphics g, int x, int y, int w, int h, int colorStart, int colorEnd, int coorSpace) {
        y = ((h + coorSpace) - 1) / coorSpace;
        for (int i = 0; i < y; i++) {
            h = ((((((65280 & colorEnd) >> 8) * i) + (((65280 & colorStart) >> 8) * (y - i))) / y) << 8) & 65280;
            x = ((((((colorEnd & 255) >> 0) * i) + (((colorStart & 255) >> 0) * (y - i))) / y) << 0) & 255;
            g.setColor(x | (h | (((((((16711680 & colorEnd) >> 16) * i) + (((16711680 & colorStart) >> 16) * (y - i))) / y) << 16) & 16711680)));
            MyAPI.fillRect(g, 0, coorSpace * i, w, coorSpace);
        }
    }

    public static void drawFront(MFGraphics g) {
        drawMap(g, mapFront);
    }

    public static void drawMapFrame(MFGraphics g) {
        if (CAMERA_OFFSET_X > 0 || CAMERA_OFFSET_Y > 0) {
            if (CAMERA_OFFSET_Y > 0) {
                g.setColor(255);
                MyAPI.fillRect(g, 0, 0, SCREEN_WIDTH, CAMERA_OFFSET_Y);
                MyAPI.fillRect(g, 0, SCREEN_HEIGHT - CAMERA_OFFSET_Y, SCREEN_WIDTH, CAMERA_OFFSET_Y);
            }
            if (CAMERA_OFFSET_X > 0) {
                g.setColor(255);
                MyAPI.fillRect(g, 0, CAMERA_OFFSET_Y + 0, CAMERA_OFFSET_X, SCREEN_HEIGHT - (CAMERA_OFFSET_Y << 1));
                MyAPI.fillRect(g, SCREEN_WIDTH - CAMERA_OFFSET_X, CAMERA_OFFSET_Y + 0, CAMERA_OFFSET_X, SCREEN_HEIGHT - (CAMERA_OFFSET_Y << 1));
            }
        }
    }

    private static void drawMap(MFGraphics g, short[][] mapArray) {
        int startY = (camera.f14y + CAMERA_OFFSET_Y) / 16;
        int endX = (((((camera.f13x + CAMERA_WIDTH) + 16) - 1) + CAMERA_OFFSET_X) - mapOffsetX) / 16;
        int endY = ((((camera.f14y + CAMERA_HEIGHT) + 16) - 1) + CAMERA_OFFSET_Y) / 16;
        int preCheckModelX = -1;
        boolean flipX;
        int x = ((camera.f13x + CAMERA_OFFSET_X) - mapOffsetX) / 16;
        while (x < endX) {
            int modelX = x / 6;
            int my;
            int y;
            int tileId;
            if (modelX != preCheckModelX) {
                preCheckModelX = 1;
                for (my = startY / 6; my < ((endY + 6) - 1) / 6; my++) {
                    if (getModelIdByIndex(mapArray, modelX, my) != 0) {
                        preCheckModelX = 0;
                        break;
                    }
                }
                my = x / 6;
                if (preCheckModelX != 0) {
                    modelX = ((modelX + 1) * 6) - 1;
                    preCheckModelX = my;
                }
                y = startY;
                while (y < endY) {
                    if (getModelId(mapArray, x, y) != 0) {
                        preCheckModelX = (((y / 6) + 1) * 6) - 1;
                    } else {
                        tileId = getTileId(mapArray, x, y);
                        flipX = (0x8000 & tileId) == 0;
                        if ((tileId & 0x4000) == 0) {
                            modelX = 0;
                        } else {
                            modelX = 0 | 2;
                        }
                        if (flipX) {
                            preCheckModelX = modelX;
                        } else {
                            preCheckModelX = modelX | 1;
                        }
                        drawTile(g, tileId & 0x3FFF, x, y, preCheckModelX);
                        preCheckModelX = y;
                    }
                    y = preCheckModelX + 1;
                }
                modelX = x;
                preCheckModelX = my;
            } else {
                my = preCheckModelX;
                y = startY;
                while (y < endY) {
                    if (getModelId(mapArray, x, y) != 0) {
                        tileId = getTileId(mapArray, x, y);
                        flipX = (0x8000 & tileId) == 0;
                        if ((tileId & 0x4000) == 0) {
                            modelX = 0;
                        } else {
                            modelX = 0 | 2;
                        }
                        if (flipX) {
                            preCheckModelX = modelX;
                        } else {
                            preCheckModelX = modelX | 1;
                        }
                        drawTile(g, tileId & 0x3FFF, x, y, preCheckModelX);
                        preCheckModelX = y;
                    } else {
                        preCheckModelX = (((y / 6) + 1) * 6) - 1;
                    }
                    y = preCheckModelX + 1;
                }
                modelX = x;
                preCheckModelX = my;
            }
            x = modelX + 1;
        }
    }

    private static int getTileId(short[][] mapArray, int x, int y) {
        return mapModel[getModelId(mapArray, x, y)][x % MODEL_WIDTH][y % MODEL_HEIGHT];
    }

    private static int getModelId(short[][] mapArray, int x, int y) {
        return getModelIdByIndex(mapArray, x / MODEL_WIDTH, y / MODEL_HEIGHT);
    }

    private static int getModelIdByIndex(short[][] mapArray, int x, int y) {
        x = getConvertX(x);
        if (y >= mapArray[0].length) {
            return 0;
        }
        return mapArray[x][y];
    }

    public static int getConvertX(int x) {
        if (x < mapLoopRight) {
            return x;
        }
        int duration = mapLoopRight - mapLoopLeft;
        switch (StageManager.getCurrentZoneId()) {
            case 8:
                return mapLoopLeft + ((x - mapLoopRight) % duration);
            default:
                return mapLoopLeft + ((x - mapLoopRight) % duration);
        }
    }

    private static void drawTile(MFGraphics g, int sy, int x, int y, int trans) {
        if (sy != 0) {
            int sx = sy % IMAGE_TILE_WIDTH;
            sy /= IMAGE_TILE_WIDTH;
            if (stageFlag) {
                mappaintframe = gameFrame;
                switch (stage_id) {
                    case 0:
                    case 1:
                    case 8:
                    case 9:
                    case 10:
                        mappaintframe %= tileimage.length;
                        break;
                    case 4:
                    case 5:
                    case 11:
                        mappaintframe = (gameFrame % (tileimage.length * 2)) / 2;
                        break;
                }
                if (stage_id == 6 || stage_id == 7) {
                    mappaintframe = zone4TileLoopID[gameFrame % zone4TileLoopID.length];
                }
                MyAPI.drawImage(g, tileimage[mappaintframe], sx * 16, sy * 16, 16, 16, trans, ((x * 16) - camera.f13x) + mapOffsetX, ((y * 16) - camera.f14y) + (y >= brokePointY ? brokeOffsetY : 0), 20);
                return;
            }
            MyAPI.drawImage(g, image, sx * 16, sy * 16, 16, 16, trans, ((x * 16) - camera.f13x) + mapOffsetX, ((y * 16) - camera.f14y) + (y >= brokePointY ? brokeOffsetY : 0), 20);
        }
    }

    public static int getPixelWidth() {
        return (mapWidth * 16) * 6;
    }

    public static int getPixelHeight() {
        return (mapHeight * 16) * 6;
    }

    public static int getMapWidth() {
        return mapWidth;
    }

    public static int getMapHeight() {
        return mapHeight;
    }

    public static void setShake(int count) {
        setShake(count, 6);
    }

    public static void setShake(int count, int power) {
        if (count > 0) {
            shakeCount = count;
            shakeMaxCount = count;
            shakePowerY = power;
        }
    }

    public static void setShakeX(int power) {
        shakePowerX = power;
    }

    public static void setMapBrokeParam(int pointY, int offsetY) {
        brokePointY = pointY / 2;
        brokeOffsetY = offsetY;
    }

    public static void releaseCamera() {
        proposeLeftCameraLimit = 0;
        proposeRightCameraLimit = getPixelWidth();
        proposeUpCameraLimit = 0;
        proposeDownCameraLimit = getPixelHeight();
        calCameraImmidiately();
    }

    public static void releaseCamera2() {
        proposeLeftCameraLimit = 0;
        proposeRightCameraLimit = getPixelWidth();
        proposeUpCameraLimit = 0;
        proposeDownCameraLimit = 2372;
        calCameraImmidiately();
    }

    public static void setMapLoop(int loopLeft, int loopRight) {
        mapLoopLeft = loopLeft;
        mapLoopRight = loopRight;
    }
}
