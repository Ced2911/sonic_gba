package SonicGBA;

import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;

/* compiled from: GimmickObject */
class Boss4Ice extends Platform {
    public static final int COLLISION_HEIGHT = 2112;
    public static final int COLLISION_OFFSET_Y = 192;
    public static final int COLLISION_WIDTH = 1536;
    public static final int DRAW_OFFSET_Y = 2112;
    public static final int STAND_OFFSET = 192;
    private static MFImage iceImage;
    public static boolean isEnd;
    private Boss4 boss4;
    private int drop_vel;
    private int endPos;
    private int iceDownCounter;

    protected Boss4Ice(int x, int y, int vel, int endPosY, Boss4 boss) {
        super(121, x, y, 0, 0, 0, 0);
        if (iceImage == null) {
            iceImage = MFImage.createImage("/gimmick/boss4_ice.png");
        }
        this.iceDownCounter = 0;
        this.drop_vel = vel;
        this.endPos = endPosY;
        this.IsDisplay = true;
        isEnd = false;
        this.boss4 = boss;
    }

    public static void releaseAllResource() {
        iceImage = null;
    }

    public void logic() {
        if (this.IsDisplay) {
            this.iceDownCounter++;
            int preX = this.posX;
            int preY = this.posY;
            if (player.isFootOnObject(this)) {
                this.offsetY = 192;
            } else {
                this.offsetY = 0;
            }
            if (this.boss4.dead || this.posY < (StageManager.getWaterLevel() << 6)) {
                this.posY += this.drop_vel * 6;
            } else {
                this.posY += this.drop_vel;
            }
            checkWithPlayer(preX, preY, this.posX, this.posY);
        }
    }

    public void draw(MFGraphics g) {
        if (this.IsDisplay) {
            drawInMap(g, iceImage, this.posX, (this.posY + 2112) + this.offsetY, 33);
            drawCollisionRect(g);
        }
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (this.IsDisplay && !player.isFootOnObject(this)) {
            switch (direction) {
                case 1:
                case 2:
                case 3:
                    if (object.getMoveDistance().f14y <= 0) {
                        object.beStop(this.collisionRect.y0, direction, this);
                    } else if (object.getCollisionRect().y1 < this.collisionRect.y1) {
                        object.beStop(this.collisionRect.y0, 1, this);
                    }
                    this.used = true;
                    return;
                case 4:
                    if (object.getMoveDistance().f14y > 0 && object.getCollisionRect().y1 < this.collisionRect.y1) {
                        object.beStop(this.collisionRect.y0, 1, this);
                        this.used = true;
                        return;
                    }
                    return;
                default:
                    if (object != player) {
                        return;
                    }
                    if (this.boss4.isNoneIce) {
                        object.beStop(this.collisionRect.y0, 1, this);
                        return;
                    } else {
                        player.beHurt();
                        return;
                    }
            }
        }
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 768, this.offsetY + y, 1536, 2112);
    }

    public boolean chkDestroy() {
        return !isInCamera() || isFarAwayCamera() || this.posY > this.endPos || this.boss4.isNoneIce;
    }
}
