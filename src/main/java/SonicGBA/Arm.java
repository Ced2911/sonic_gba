package SonicGBA;

import Lib.SoundSystem;
import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;

/* compiled from: GimmickObject */
class Arm extends GimmickObject {
    private static final int ARM_DRAW_HEIGHT_1 = 31;
    private static final int ARM_DRAW_HEIGHT_2 = 36;
    private static final int ARM_DRAW_WIDTH = 36;
    private static final int BACK_WAIT_COUNT = 4;
    private static final int BAR_LENGTH = 1536;
    private static final int BAR_WIDTH = 384;
    private static final int COLLISION_HEIGHT = 1024;
    private static final int COLLISION_OFFSET_Y = 2304;
    private static final int COLLISION_WIDTH = 2048;
    private static final int PULL_POINT_OFFSET = -192;
    private static final byte STATE_BACK = (byte) 3;
    private static final byte STATE_BACK_WAIT = (byte) 5;
    private static final byte STATE_DOWN = (byte) 4;
    private static final byte STATE_PULLING = (byte) 2;
    private static final byte STATE_UP = (byte) 1;
    private static final byte STATE_WAIT = (byte) 0;
    private static final int UP_DISTANCE = 3328;
    private static final int VELOCITY = 300;
    public static MFImage armImage;
    public static MFImage armplusImage;
    public static MFImage barImage;
    public static MFImage baseImage;
    private static int frame;
    private int count;
    private int posXOriginal;
    private int posYOriginal;
    private byte state;

    protected Arm(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        this.posYOriginal = this.posY;
        this.posXOriginal = this.posX;
        this.posY = this.posYOriginal + UP_DISTANCE;
        this.state = (byte) 0;
        if (armImage == null) {
            try {
                armImage = MFImage.createImage("/gimmick/arm.png");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (barImage == null) {
            try {
                barImage = MFImage.createImage("/gimmick/arm_bar.png");
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        if (armplusImage == null) {
            try {
                armplusImage = MFImage.createImage("/gimmick/arm_plus.png");
            } catch (Exception e22) {
                e22.printStackTrace();
            }
        }
        if (baseImage == null) {
            try {
                baseImage = MFImage.createImage("/gimmick/part_1_2.png");
            } catch (Exception e222) {
                e222.printStackTrace();
            }
        }
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        switch (this.state) {
            case (byte) 0:
                SoundSystem.getInstance().playSe(51);
                this.state = (byte) 1;
                player.setOutOfControl(this);
                player.doWalkPoseInAir();
                player.degreeForDraw = player.faceDegree;
                isGotRings = false;
                frame = 1;
                return;
            default:
                return;
        }
    }

    public void logic() {
        if (this.count > 0) {
            this.count--;
        }
        switch (this.state) {
            case (byte) 1:
                this.posY -= 300;
                if (this.posY <= this.posYOriginal) {
                    this.posY = this.posYOriginal;
                    this.state = (byte) 2;
                }
                frame++;
                if (frame == 5) {
                    soundInstance.playLoopSe(52);
                    break;
                }
                break;
            case (byte) 2:
                this.posX += 300;
                frame++;
                if (this.posX >= this.posXOriginal + this.mWidth) {
                    this.posX = this.posXOriginal + this.mWidth;
                    player.outOfControl = false;
                    player.collisionState = (byte) 1;
                    player.velY = 0;
                    player.doWalkPoseInAir();
                    this.count = 4;
                    this.state = (byte) 5;
                    soundInstance.stopLoopSe();
                    break;
                }
                break;
            case (byte) 3:
                this.posX -= 300;
                if (this.posX <= this.posXOriginal) {
                    this.posX = this.posXOriginal;
                    this.state = (byte) 4;
                    break;
                }
                break;
            case (byte) 4:
                this.posY += 300;
                if (this.posY >= this.posYOriginal + UP_DISTANCE) {
                    this.posY = this.posYOriginal + UP_DISTANCE;
                    this.state = (byte) 0;
                    break;
                }
                break;
            case (byte) 5:
                if (this.count == 0) {
                    this.state = (byte) 3;
                    break;
                }
                break;
        }
        refreshCollisionRect(this.posX, this.posY);
        if (player.outOfControl && player.outOfControlObject == this) {
            int preX = player.getFootPositionX();
            int preY = player.getFootPositionY();
            player.doPullMotion(this.posX, (this.posY + COLLISION_OFFSET_Y) + PULL_POINT_OFFSET);
            player.checkWithObject(preX, preY, player.getFootPositionX(), player.getFootPositionY());
        }
    }

    public void draw(MFGraphics g) {
        drawInMap(g, baseImage, 0, 0, 8, 8, 0, 17);
        int i = 0;
        while (i < (this.posY - this.posYOriginal) - 1536) {
            drawInMap(g, barImage, this.posX, this.posYOriginal + i, 17);
            i += 1536;
        }
        drawInMap(g, barImage, 0, 0, 6, ((this.posY - this.posYOriginal) - i) >> 6, 0, this.posX, this.posYOriginal + i, 17);
        drawInMap(g, armplusImage, 0, 0, 8, 8, 1, this.posX, this.posYOriginal - 256, 17);
        drawInMap(g, armImage, 0, catching() ? 0 : 31, 36, catching() ? 31 : 36, 0, 17);
        drawCollisionRect(g);
    }

    public boolean catching() {
        return this.state == (byte) 1 || this.state == (byte) 2;
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 1024, (y + COLLISION_OFFSET_Y) - 1024, 2048, 1024);
    }

    public void close() {
    }

    public static void releaseAllResource() {
    }
}
