package SonicGBA;

import GameEngine.Key;
import Lib.MyAPI;
import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;

/* compiled from: GimmickObject */
class Furiko extends GimmickObject {
    private static final int COLLISION_HEIGHT = 1024;
    private static final int COLLISION_OFFSET_Y = 960;
    private static final int COLLISION_WIDTH = 1024;
    private static final int DRAW_HEIGHT = 24;
    private static final int DRAW_WIDTH = 16;
    private static final int LEAVE_COUNT = 10;
    private static final int RADIUS = 4928;
    private static final int RING_NUM = 3;
    private static final int RING_SPACE = 960;
    private static int degree = 2240;
    private static int lineVelocity = 0;
    private int centerX;
    private int centerY;
    private int leaveCount = 0;
    private int thisDegree;
    private boolean touching = false;

    protected Furiko(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
        if (Arm.baseImage == null) {
            try {
                Arm.baseImage = MFImage.createImage("/gimmick/part_1_2.png");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (hookImage == null) {
            try {
                hookImage = MFImage.createImage("/gimmick/hook.png");
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        this.centerX = this.posX;
        this.centerY = this.posY;
    }

    public static void staticLogic() {
        lineVelocity += ((GRAVITY - 60) * MyAPI.dSin((degree >> 6) - 90)) / 100;
        degree -= ((((lineVelocity << 6) / RADIUS) << 6) * RollPlatformSpeedC.DEGREE_VELOCITY) / SonicDef.PI;
    }

    public void logic() {
        if (this.leaveCount > 0) {
            this.leaveCount--;
        }
        this.thisDegree = degree >> 6;
        if (this.iLeft != 0) {
            this.thisDegree = RollPlatformSpeedC.DEGREE_VELOCITY - this.thisDegree;
        }
        this.posX = this.centerX + ((MyAPI.dCos(this.thisDegree) * RADIUS) / 100);
        this.posY = this.centerY + ((MyAPI.dSin(this.thisDegree) * RADIUS) / 100);
        refreshCollisionRect(this.posX, this.posY);
        if (this.touching) {
            player.doPullMotion(this.posX, this.posY);
            if (Key.press(Key.gUp | 16777216)) {
                player.outOfControl = false;
                this.touching = false;
                player.doJump();
                player.setFurikoOutVelX(this.thisDegree);
                this.leaveCount = 10;
            }
        }
    }

    public void draw(MFGraphics g) {
        for (int i = 0; i < 3; i++) {
            MFGraphics mFGraphics = g;
            drawInMap(mFGraphics, Arm.baseImage, 8, 0, 8, 8, 0, this.centerX + ((((i + 1) * 960) * MyAPI.dCos(this.thisDegree)) / 100), this.centerY + ((((i + 1) * 960) * MyAPI.dSin(this.thisDegree)) / 100), 3);
        }
        g.saveCanvas();
        g.translateCanvas((this.posX >> 6) - camera.f13x, (this.posY >> 6) - camera.f14y);
        g.rotateCanvas((float) (this.thisDegree - 90));
        MyAPI.drawRegion(g, hookImage, 16, 0, 16, 24, 0, 0, -22, 17);
        g.restoreCanvas();
        drawCollisionRect(g);
    }

    public void refreshCollisionRect(int x, int y) {
        this.collisionRect.setRect(x - 512, y - 512, 1024, 1024);
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (!this.touching && this.firstTouch && this.leaveCount == 0) {
            this.touching = true;
            object.setOutOfControl(this);
        }
    }

    public void close() {
    }

    public static void releaseAllResource() {
    }
}
