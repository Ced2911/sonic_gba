package SonicGBA;

/* compiled from: GimmickObject */
class Bank extends GimmickObject {
    private boolean touching;

    protected Bank(int id, int x, int y, int left, int top, int width, int height) {
        super(id, x, y, left, top, width, height);
    }

    public void doWhileCollision(PlayerObject object, int direction) {
        if (object.onBank) {
            object.onBank = false;
            object.bankwalking = false;
        } else if (((this.objId == 28 && direction == 3) || (this.objId == 29 && direction == 2)) && object.collisionState == (byte) 0) {
            this.touching = true;
            player.bankwalking = true;
        }
    }

    public void doWhileNoCollision() {
        if (this.touching) {
            if ((this.objId == 28 && player.getVelX() > PlayerObject.BANKING_MIN_SPEED) || (this.objId == 29 && player.getVelX() < -500)) {
                player.setBank();
            }
            this.touching = false;
        }
    }
}
