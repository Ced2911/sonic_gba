package Special;

import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: SpecialObject */
class SSLostRing extends SSRing {
    private static final int GRAVITY = 172;
    private int velX;
    private int velY;
    private int velZ;

    public SSLostRing(int x, int y, int z, int velX, int velY, int velZ) {
        super(x, y, z << 3);
        this.velX = velX;
        this.velY = velY;
        this.velZ = velZ;
    }

    public void close() {
    }

    public void doWhileCollision(SpecialObject collisionObj) {
    }

    public void draw(MFGraphics g) {
        SpecialObject.calDrawPosition(this.posX >> 6, this.posY >> 6, this.posZ);
        drawObj(g, ringDrawer, 0, 8);
    }

    public void logic() {
        this.posZ += this.velZ;
        this.posX += this.velX;
        this.posY += this.velY;
    }

    public void refreshCollision(int x, int y) {
    }

    public boolean chkDestory() {
        return this.posZ < ((player.posZ - 6) - 30) + 1;
    }
}
