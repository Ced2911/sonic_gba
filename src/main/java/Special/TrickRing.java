package Special;

import Lib.SoundSystem;
import com.sega.mobile.framework.device.MFGraphics;

/* compiled from: SpecialObject */
class TrickRing extends SpecialObject {
    private static final int COLLISION_WIDTH = 60;
    private static final int[][] RING_GEN_PARAM;
    private boolean used;
    static {
/*
        r0 = new int[12][];
        int[] iArr = new int[]{60, iArr};
        r0[1] = new int[]{50, -50};
        iArr = new int[]{-60, iArr};
        r0[3] = new int[]{-50, -50};
        int[] iArr2 = new int[]{-60, iArr2};
        r0[5] = new int[]{-50, 50};
        iArr2 = new int[]{60, iArr2};
        r0[7] = new int[]{50, 50};
        iArr2 = new int[]{100, iArr2};
        iArr2 = new int[]{-100, iArr2};
        iArr2 = new int[]{-100, iArr2};
        iArr2 = new int[]{100, iArr2};
        RING_GEN_PARAM = r0;
*/
        RING_GEN_PARAM = new int[12][];;
    }

    public TrickRing(int x, int y, int z) {
        super(1, x, y, z);
        this.drawer = objAnimation.getDrawer(0, true, 0);
        this.used = false;
    }

    public void close() {
        this.drawer = null;
    }

    public void doWhileCollision(SpecialObject collisionObj) {
        if (!this.used) {
            for (int i = 0; i < RING_GEN_PARAM.length; i++) {
                SpecialObject tmpObj = new SSFollowRing(this.posX + RING_GEN_PARAM[i][0], this.posY + RING_GEN_PARAM[i][1], this.posZ << 3, false);
                SpecialObject.addExtraObject(tmpObj);
                decideObjects.addElement(tmpObj);
            }
            player.setTrikCount();
            SoundSystem.getInstance().playSe(39);
            this.drawer.setActionId(1);
            this.drawer.setLoop(false);
            this.used = true;
        }
    }

    public void draw(MFGraphics g) {
        SpecialObject.calDrawPosition(this.posX, this.posY, this.posZ);
        drawObj(g, this.drawer, 0, 0);
    }

    public void logic() {
    }

    public void refreshCollision(int x, int y) {
        this.collisionRect.setRect(x - 30, y - 30, 60, 60);
    }
}
