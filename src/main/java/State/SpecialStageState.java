package State;

import Common.BarWord;
import Common.NumberDrawer;
import Common.WhiteBarDrawer;
import Ending.SpecialEnding;
import GameEngine.Def;
import GameEngine.Key;
import GameEngine.TouchKeyRange;
import Lib.Animation;
import Lib.AnimationDrawer;
import Lib.MyAPI;
import Lib.MyRandom;
import Lib.Record;
import Lib.SoundSystem;
import SonicGBA.GlobalResource;
import SonicGBA.MapManager;
import SonicGBA.PlayerObject;
import SonicGBA.StageManager;
import Special.SSDef;
import Special.SpecialMap;
import Special.SpecialObject;
import com.sega.mobile.framework.device.MFDevice;
import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;
import com.sega.mobile.framework.ui.MFTouchKey;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.util.Vector;

public class SpecialStageState extends State implements SSDef, BarWord {
    private static final int[] BONUS_ID = new int[]{22, 23, 24};
    private static final int BONUS_NUM_X = ((SCREEN_WIDTH >> 1) + 75);
    private static final int BONUS_X = ((SCREEN_WIDTH >> 1) - 79);
    private static final int BONUS_Y_ORIGINAL = (SCREEN_HEIGHT + 40);
    private static final int BONUS_Y_SPACE = 18;
    private static final int BONUS_Y_START = ((SCREEN_HEIGHT >> 1) - 7);
    private static final int EMERALD_SPACE = 32;
    private static final int EMERALD_X_ORIGINAL = (SCREEN_WIDTH + 30);
    private static final int EMERALD_X_START = ((SCREEN_WIDTH >> 1) - 96);
    private static final int EMERALD_Y = ((SCREEN_HEIGHT >> 1) - 32);
    private static final boolean IS_EMERALD_DEBUG_FULL = false;
    private static final int OPTION_MOVING_INTERVAL = 100;
    private static final int OPTION_MOVING_SPEED = 4;
    private static final int PAUSE_OPTION_ITEMS_NUM = 6;
    private static final int SPEED_LIGHT_NUM_PER_FRAME = 2;
    private static final int SPEED_LIGHT_VELOCITY = 60;
    private static final int STATE_END = 3;
    private static final int STATE_GAMING = 2;
    private static final int STATE_GET_EMERALD = 4;
    private static final int STATE_INTERRUPT = 14;
    private static final int STATE_INTRO = 1;
    private static final int STATE_PAUSE = 6;
    private static final int STATE_PAUSE_OPTION = 8;
    private static final int STATE_PAUSE_OPTION_HELP = 13;
    private static final int STATE_PAUSE_OPTION_KEY_CONTROL = 11;
    private static final int STATE_PAUSE_OPTION_SENSOR = 16;
    private static final int STATE_PAUSE_OPTION_SOUND = 9;
    private static final int STATE_PAUSE_OPTION_SOUND_VOLUMN = 15;
    private static final int STATE_PAUSE_OPTION_SP_SET = 12;
    private static final int STATE_PAUSE_OPTION_VIB = 10;
    private static final int STATE_PAUSE_TO_TITLE = 7;
    private static final int STATE_READY = 0;
    private static final int STATE_WAIT_FOR_OVER = 5;
    private static final int VISIBLE_OPTION_ITEMS_NUM = 9;
    private static final int WORD_FINISH_STAGE = 1;
    private static final int WORD_GET_EMERALD = 0;
    public static MFTouchKey aButton;
    public static MFTouchKey bButton;
    private static int[] emeraldStatus = new int[7];
    private WhiteBarDrawer barDrawer;
    private int[] bonusY = new int[3];
    private boolean changingState;
    private AnimationDrawer characterUpDrawer;
    private int characterY = SCREEN_HEIGHT;
    private int clearScore;
    private int count;
    private int[] emeraldX = new int[7];
    private SpecialEnding endingInstance;
    private boolean fadeChangeState;
    private AnimationDrawer fontDrawer;
    private AnimationDrawer interruptDrawer;
    private int interrupt_state;
    private boolean isChanged;
    private boolean isIntroBGMplay = false;
    private boolean isOptionChange;
    private boolean isOptionDisFlag = false;
    private boolean isSelectable;
    private int nextState;
    private int optionDrawOffsetBottomY;
    private int optionDrawOffsetTmpY1;
    private int optionDrawOffsetY;
    private int optionOffsetTmp;
    private int optionOffsetX;
    private int optionOffsetY;
    private int optionOffsetYAim;
    private boolean optionReturnFlag;
    private int optionYDirect;
    private int optionslide_getprey = -1;
    private int optionslide_gety = -1;
    private int optionslide_y;
    private int optionslidefirsty;
    private int pauseOptionCursor;
    private int pause_item_cursor;
    private int pause_item_speed;
    private int pause_item_x;
    private int pause_item_y;
    private boolean pause_optionFlag;
    private int pause_optionframe;
    private boolean pause_returnFlag;
    private int pause_returnframe;
    private int pause_saw_speed;
    private int pause_saw_x;
    private int pause_saw_y;
    private int pausecnt;
    private int preVelZ;
    private MFImage readyBgImage;
    private int ringScore;
    private MFImage speedLight;
    private Vector speedLightVec;
    private int state = 0;
    private int totalScore;
    private MFImage welcomeBgImage;

    public SpecialStageState() {
        fading = false;
        this.barDrawer = WhiteBarDrawer.getInstance();
        if (GlobalResource.spsetConfig == 1) {
            bButton = new MFTouchKey(0, 0, MyAPI.zoomOut(Def.SCREEN_WIDTH) >> 1, MyAPI.zoomOut(Def.SCREEN_HEIGHT), Key.B_SEL);
            MFDevice.addComponent(bButton);
            aButton = new MFTouchKey(MyAPI.zoomOut(Def.SCREEN_WIDTH) >> 1, 0, MyAPI.zoomOut(Def.SCREEN_WIDTH) >> 1, MyAPI.zoomOut(Def.SCREEN_HEIGHT), 16777216);
            MFDevice.addComponent(aButton);
        } else if (GlobalResource.spsetConfig == 0) {
            if (bButton != null) {
                MFDevice.removeComponent(bButton);
            }
            bButton = null;
            if (aButton != null) {
                MFDevice.removeComponent(aButton);
            }
            aButton = null;
            Key.touchkeyboardClose();
            Key.touchSpKeyboardInit();
        }
        Key.touchSPstageInit();
        loadData();
        if (emeraldState(StageManager.getStageID()) != 1) {
            setEmeraldState(StageManager.getStageID(), 2);
            saveData();
        }
    }

    public void logic() {
        fadeStateLogic();
        if (Key.press(Key.B_PO)) {
        }
        int i;
        switch (this.state) {
            case 0:
                if (State.fadeChangeOver()) {
                    if (!this.isIntroBGMplay) {
                        SoundSystem.getInstance().playBgm(34, false);
                        this.isIntroBGMplay = true;
                    }
                    if (this.changingState) {
                        this.state = 1;
                        State.fadeInitAndStart(255, 0);
                        SpecialObject.player.initWelcome();
                        this.changingState = false;
                    } else {
                        this.characterY -= 15;
                        if (this.characterY < -40) {
                            State.fadeInitAndStart(0, 255);
                            this.changingState = true;
                        }
                    }
                }
                i = 0;
                while (i < this.speedLightVec.size()) {
                    int[] position = (int[]) this.speedLightVec.elementAt(i);
                    position[1] = position[1] + 60;
                    if (position[1] > SCREEN_HEIGHT + this.speedLight.getHeight()) {
                        this.speedLightVec.removeElementAt(i);
                        i--;
                    }
                    i++;
                }
                for (i = 0; i < 2; i++) {
                    this.speedLightVec.addElement(new int[]{MyRandom.nextInt(0, SCREEN_WIDTH), 0 - (i * 30)});
                }
                return;
            case 1:
                SpecialObject.player.logicWelcome();
                if (!State.fadeChangeOver()) {
                    return;
                }
                if (this.changingState) {
                    this.state = 2;
                    SoundSystem.getInstance().playBgm(35);
                    fading = false;
                    State.fadeInit(255, 0);
                    this.changingState = false;
                    return;
                } else if (SpecialObject.player.isWelcomeOver()) {
                    State.fadeInitAndStart(0, 255);
                    this.changingState = true;
                    return;
                } else {
                    return;
                }
            case 2:
                SpecialObject.player.logic();
                SpecialObject.objectLogic();
                if (!this.changingState && SpecialObject.player.isOver()) {
                    State.setFadeColor(MapManager.END_COLOR);
                    State.fadeInitAndStart(0, 255);
                    this.changingState = true;
                }
                if (State.fadeChangeOver() && this.changingState) {
                    int i2;
                    this.state = 3;
                    State.setFadeColor(0);
                    fading = false;
                    this.barDrawer.initBar(this, SpecialObject.player.checkSuccess ? 0 : 1);
                    this.barDrawer.setPause(true);
                    this.changingState = false;
                    this.count = 0;
                    for (i = 0; i < this.bonusY.length; i++) {
                        this.bonusY[i] = BONUS_Y_ORIGINAL;
                    }
                    for (i = 0; i < this.emeraldX.length; i++) {
                        this.emeraldX[i] = EMERALD_X_ORIGINAL;
                    }
                    this.ringScore = SpecialObject.player.getRingNum() * 100;
                    if (SpecialObject.player.checkSuccess) {
                        i2 = 10000;
                    } else {
                        i2 = 0;
                    }
                    this.clearScore = i2;
                    this.totalScore = 0;
                    if (SpecialObject.player.checkSuccess) {
                        setEmeraldState(StageManager.getStageID(), 1);
                        saveData();
                        this.endingInstance = new SpecialEnding(PlayerObject.getCharacterID(), STAGE_ID_TO_SPECIAL_ID[StageManager.getStageID()]);
                        this.state = 4;
                        State.setFadeColor(MapManager.END_COLOR);
                        State.fadeInitAndStart(255, 0);
                        SoundSystem.getInstance().stopBgm(false);
                    } else {
                        if (emeraldState(StageManager.getStageID()) != 1) {
                            setEmeraldState(StageManager.getStageID(), 2);
                            saveData();
                        }
                        SoundSystem.getInstance().playBgm(39);
                    }
                }
                if (!SpecialObject.player.isNeedTouchPad()) {
                    return;
                }
                if (Key.touchspstagepause.IsButtonPress() || Key.press(524288)) {
                    SoundSystem.getInstance().playSe(33);
                    pauseInit();
                    return;
                }
                return;
            case 3:
                this.count++;
                if (this.count >= 13) {
                    for (i = 0; i < 3; i++) {
                        if (i <= this.count - 13) {
                            this.bonusY[i] = MyAPI.calNextPosition((double) this.bonusY[i], (double) (BONUS_Y_START + (i * 18)), 1, 4);
                        }
                    }
                }
                if (this.count >= 16) {
                    for (i = 0; i < 7; i++) {
                        if (i <= this.count - 16) {
                            this.emeraldX[i] = MyAPI.calNextPosition((double) this.emeraldX[i], (double) (EMERALD_X_START + (i * 32)), 1, 4);
                        }
                    }
                }
                if (this.count > 46) {
                    int scoreChange;
                    if (this.ringScore > 0) {
                        scoreChange = Math.min(400, this.ringScore);
                        this.ringScore -= scoreChange;
                        this.totalScore += scoreChange;
                    }
                    if (this.clearScore > 0) {
                        scoreChange = Math.min(400, this.clearScore);
                        this.clearScore -= scoreChange;
                        this.totalScore += scoreChange;
                    }
                    if (this.ringScore == 0) {
                        SoundSystem.getInstance().playSe(32);
                    } else {
                        SoundSystem.getInstance().playSe(31);
                    }
                    if (this.ringScore == 0 && this.clearScore == 0) {
                        PlayerObject.setScore(PlayerObject.getScore() + this.totalScore);
                        this.state = 5;
                        this.count = 0;
                        return;
                    }
                    return;
                }
                return;
            case 4:
                this.endingInstance.logic();
                if (this.endingInstance.isOver() && !this.changingState) {
                    State.setFadeColor(MapManager.END_COLOR);
                    State.fadeInitAndStart(0, 255);
                    this.changingState = true;
                }
                if (!State.fadeChangeOver()) {
                    return;
                }
                if (this.changingState) {
                    this.state = 3;
                    SoundSystem.getInstance().playBgm(40);
                    this.changingState = false;
                    State.setFadeColor(0);
                    fading = false;
                    return;
                }
                State.setFadeColor(0);
                return;
            case 5:
                this.count++;
                if (this.count == 128) {
                    this.barDrawer.setPause(false);
                }
                if (this.barDrawer.getState() == 4) {
                    backToGameStage();
                    return;
                }
                return;
            case 6:
                pauseLogic();
                return;
            case 7:
                pausetoTitleLogic();
                return;
            case 8:
                optionLogic();
                return;
            case 9:
                pauseOptionSoundLogic();
                return;
            case 10:
                pauseOptionVibrationLogic();
                return;
            case 12:
                pauseOptionSpSetLogic();
                return;
            case 13:
                helpLogic();
                if ((Key.press(524288) || (Key.touchhelpreturn.IsButtonPress() && this.returnPageCursor == 1)) && State.fadeChangeOver()) {
                    changeStateWithFade(8);
                    AnimationDrawer.setAllPause(true);
                    SoundSystem.getInstance().playSe(2);
                    return;
                }
                return;
            case 14:
                interruptLogic();
                return;
            case 15:
                switch (soundVolumnLogic()) {
                    case 2:
                        State.fadeInit(102, 0);
                        this.state = 8;
                        return;
                    default:
                        return;
                }
            case 16:
                switch (spSenorSetLogic()) {
                    case 1:
                        GlobalResource.sensorConfig = 0;
                        State.fadeInit(102, 0);
                        this.state = 8;
                        return;
                    case 2:
                        GlobalResource.sensorConfig = 1;
                        State.fadeInit(102, 0);
                        this.state = 8;
                        return;
                    case 3:
                        State.fadeInit(102, 0);
                        this.state = 8;
                        return;
                    case 4:
                        GlobalResource.sensorConfig = 2;
                        State.fadeInit(102, 0);
                        this.state = 8;
                        return;
                    default:
                        return;
                }
            default:
                return;
        }
    }

    private void pausetoTitleLogic() {
        switch (secondEnsureLogic()) {
            case 1:
                Key.touchanykeyInit();
                Key.touchMainMenuInit2();
                AnimationDrawer.setAllPause(false);
                StageManager.characterFromGame = PlayerObject.getCharacterID();
                StageManager.stageIDFromGame = StageManager.getStageID();
                State.setState(2);
                return;
            case 2:
                pauseInitFromItems();
                return;
            default:
                return;
        }
    }

    private void pauseOptionSoundLogic() {
        switch (itemsSelect2Logic()) {
            case 1:
                GlobalResource.soundConfig = 5;
                GlobalResource.seConfig = 1;
                State.fadeInit(102, 0);
                this.state = 8;
                SoundSystem.getInstance().setSoundState(GlobalResource.soundConfig);
                SoundSystem.getInstance().setSeState(GlobalResource.seConfig);
                return;
            case 2:
                GlobalResource.soundConfig = 0;
                GlobalResource.seConfig = 0;
                State.fadeInit(102, 0);
                this.state = 8;
                SoundSystem.getInstance().setSoundState(GlobalResource.soundConfig);
                SoundSystem.getInstance().setSeState(GlobalResource.seConfig);
                return;
            case 3:
                State.fadeInit(102, 0);
                this.state = 8;
                SoundSystem.getInstance().setSoundState(GlobalResource.soundConfig);
                SoundSystem.getInstance().setSeState(GlobalResource.seConfig);
                return;
            default:
                return;
        }
    }

    private void pauseOptionVibrationLogic() {
        switch (itemsSelect2Logic()) {
            case 1:
                GlobalResource.vibrationConfig = 1;
                State.fadeInit(102, 0);
                this.state = 8;
                MyAPI.vibrate();
                return;
            case 2:
                GlobalResource.vibrationConfig = 0;
                State.fadeInit(102, 0);
                this.state = 8;
                return;
            case 3:
                State.fadeInit(102, 0);
                this.state = 8;
                return;
            default:
                return;
        }
    }

    private void pauseOptionSpSetLogic() {
        switch (itemsSelect2Logic()) {
            case 1:
                GlobalResource.spsetConfig = 0;
                State.fadeInit(102, 0);
                this.state = 8;
                Key.touchkeyboardClose();
                Key.touchSpKeyboardInit();
                if (bButton != null) {
                    MFDevice.removeComponent(bButton);
                    bButton = null;
                }
                if (aButton != null) {
                    MFDevice.removeComponent(aButton);
                    aButton = null;
                    return;
                }
                return;
            case 2:
                GlobalResource.spsetConfig = 1;
                State.fadeInit(102, 0);
                this.state = 8;
                Key.touchkeyboardClose();
                if (bButton == null) {
                    bButton = new MFTouchKey(0, 0, MyAPI.zoomOut(Def.SCREEN_WIDTH) >> 1, MyAPI.zoomOut(Def.SCREEN_HEIGHT), Key.B_SEL);
                    MFDevice.addComponent(bButton);
                }
                if (aButton == null) {
                    aButton = new MFTouchKey(MyAPI.zoomOut(Def.SCREEN_WIDTH) >> 1, 0, MyAPI.zoomOut(Def.SCREEN_WIDTH) >> 1, MyAPI.zoomOut(Def.SCREEN_HEIGHT), 16777216);
                    MFDevice.addComponent(aButton);
                    return;
                }
                return;
            case 3:
                State.fadeInit(102, 0);
                this.state = 8;
                return;
            default:
                return;
        }
    }

    public void draw(MFGraphics g) {
        switch (this.state) {
            case 13:
                g.setFont(11);
                break;
            default:
                g.setFont(14);
                break;
        }
        int i;
        switch (this.state) {
            case 0:
                MyAPI.drawImage(g, this.readyBgImage, 0, 0, 0);
                for (i = 0; i < this.speedLightVec.size(); i++) {
                    int[] position = (int[]) this.speedLightVec.elementAt(i);
                    MyAPI.drawImage(g, this.speedLight, position[0], position[1], 33);
                }
                this.characterUpDrawer.draw(g, SCREEN_WIDTH >> 1, this.characterY);
                return;
            case 1:
                g.setColor(0);
                MyAPI.fillRect(g, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
                MyAPI.drawImage(g, this.welcomeBgImage, 0, 0, 0);
                SpecialObject.player.setPause(false);
                SpecialObject.player.drawWelcome(g);
                return;
            case 2:
                SpecialMap.drawMap(g);
                SpecialObject.drawObjects(g);
                SpecialObject.player.setPause(false);
                SpecialObject.player.drawInfo(g);
                if (GlobalResource.spsetConfig == 0 && SpecialObject.player.isNeedTouchPad()) {
                    drawTouchKeyDirect(g);
                }
                if (SpecialObject.player.isNeedTouchPad()) {
                    State.drawSoftKeyPause(g);
                    return;
                }
                return;
            case 3:
            case 5:
                g.setColor(MapManager.END_COLOR);
                MyAPI.fillRect(g, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
                this.barDrawer.drawBar(g);
                for (i = 0; i < this.bonusY.length; i++) {
                    this.fontDrawer.draw(g, BONUS_ID[i], this.barDrawer.getBarX() + BONUS_X, this.bonusY[i], false, 0);
                    int drawNum = 0;
                    switch (i) {
                        case 0:
                            drawNum = this.ringScore;
                            break;
                        case 1:
                            drawNum = this.clearScore;
                            break;
                        case 2:
                            drawNum = this.totalScore;
                            break;
                        default:
                            break;
                    }
                    NumberDrawer.drawNum(g, 0, drawNum, this.barDrawer.getBarX() + ((SCREEN_WIDTH >> 1) + 75), this.bonusY[i], 4);
                }
                i = 0;
                while (i < 7) {
                    this.fontDrawer.draw(g, emeraldStatus[i] == 1 ? i + 15 : 14, this.emeraldX[i] + this.barDrawer.getBarX(), EMERALD_Y, false, 0);
                    i++;
                }
                return;
            case 4:
                this.endingInstance.draw(g);
                return;
            case 6:
                SpecialMap.drawMap(g);
                SpecialObject.drawObjects(g);
                SpecialObject.player.setPause(true);
                SpecialObject.player.drawInfo(g);
                pauseDraw(g);
                return;
            case 7:
                SpecialMap.drawMap(g);
                SpecialObject.drawObjects(g);
                SpecialObject.player.setPause(true);
                SpecialObject.player.drawInfo(g);
                muiAniDrawer.setActionId(50);
                muiAniDrawer.draw(g, this.pause_saw_x, this.pause_saw_y);
                State.drawFade(g);
                SecondEnsurePanelDraw(g, 18);
                return;
            case 8:
                optionDraw(g);
                return;
            case 9:
                optionDraw(g);
                itemsSelect2Draw(g, 35, 36);
                return;
            case 10:
                optionDraw(g);
                itemsSelect2Draw(g, 35, 36);
                return;
            case 11:
                optionDraw(g);
                return;
            case 12:
                optionDraw(g);
                itemsSelect2Draw(g, 37, 38);
                return;
            case 13:
                helpDraw(g);
                return;
            case 14:
                interruptDraw(g);
                return;
            case 15:
                optionDraw(g);
                soundVolumnDraw(g);
                return;
            case 16:
                optionDraw(g);
                spSenorSetDraw(g);
                return;
            default:
                return;
        }
    }

    public void init() {
        State.initTouchkeyBoard();
        SpecialMap.loadMap();
        SpecialObject.initObjects();
        this.characterUpDrawer = new Animation("/animation/special/sp_up_chr").getDrawer(PlayerObject.getCharacterID(), true, 0);
        this.characterY = SCREEN_HEIGHT + 40;
        State.fadeInitAndStart(255, 0);
        this.speedLight = MFImage.createImage("/special_res/speed_light.png");
        this.speedLightVec = new Vector();
        this.changingState = false;
        this.readyBgImage = MFImage.createImage("/special_res/sp_up_bg.png");
        this.welcomeBgImage = MFImage.createImage("/special_res/sp_welcome_bg.png");
        this.fontDrawer = Animation.getInstanceFromQi("/special_res/sp_font.dat")[0].getDrawer();
    }

    public void close() {
        State.releaseTouchkeyBoard();
        Animation.closeAnimationDrawer(this.characterUpDrawer);
        this.characterUpDrawer = null;
        this.speedLight = null;
        this.readyBgImage = null;
        this.welcomeBgImage = null;
        Animation.closeAnimationDrawer(this.fontDrawer);
        this.fontDrawer = null;
        Animation.closeAnimationDrawer(this.interruptDrawer);
        this.interruptDrawer = null;
        SpecialMap.releaseMap();
        SpecialObject.closeObjects();
        if (bButton != null) {
            MFDevice.removeComponent(bButton);
        }
        if (aButton != null) {
            MFDevice.removeComponent(aButton);
        }
        if (GlobalResource.spsetConfig == 0) {
            Key.touchkeyboardClose();
        }
        Key.touchSPstageClose();
        System.gc();
        try {
            Thread.sleep(100);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void pauseInit() {
        if (SpecialObject.player.velZ != 0) {
            this.preVelZ = SpecialObject.player.velZ;
        }
        SpecialObject.player.velZ = 0;
        this.state = 6;
        Key.touchGamePauseInit(0);
        this.pausecnt = 0;
        this.pause_saw_x = -50;
        this.pause_saw_y = 0;
        this.pause_saw_speed = 30;
        this.pause_item_x = SCREEN_WIDTH - 26;
        this.pause_item_speed = (-((SCREEN_WIDTH >> 1) + 14)) / 3;
        if (muiAniDrawer == null) {
            muiAniDrawer = new Animation("/animation/mui").getDrawer(0, false, 0);
        }
        this.pause_returnFlag = false;
        this.pause_item_y = (SCREEN_HEIGHT >> 1) - 36;
        State.fadeInit_Modify(0, 102);
        SoundSystem.getInstance().stopBgm(false);
        Key.touchgamekeyClose();
        AnimationDrawer.setAllPause(true);
    }

    private void pauseInitFromItems() {
        if (SpecialObject.player.velZ != 0) {
            this.preVelZ = SpecialObject.player.velZ;
        }
        SpecialObject.player.velZ = 0;
        this.state = 6;
        Key.touchGamePauseInit(0);
        Key.touchgamekeyClose();
        State.fadeInit_Modify(192, 102);
    }

    private void pauseLogic() {
        this.pausecnt++;
        if (this.pausecnt >= 5 && this.pausecnt <= 7) {
            if (this.pause_saw_x + this.pause_saw_speed > 0) {
                this.pause_saw_x = 0;
            } else {
                this.pause_saw_x += this.pause_saw_speed;
            }
            if (this.pause_item_x + this.pause_item_speed < (SCREEN_WIDTH >> 1) - 40) {
                this.pause_item_x = (SCREEN_WIDTH >> 1) - 40;
            } else {
                this.pause_item_x += this.pause_item_speed;
            }
        } else if (this.pausecnt > 7) {
            int i;
            for (i = 0; i < 3; i++) {
                if (Key.touchgamepauseitem[i].Isin() && Key.touchgamepause.IsClick()) {
                    this.pause_item_cursor = i;
                }
            }
            if (Key.touchgamepausereturn.Isin() && Key.touchgamepause.IsClick()) {
                this.pause_item_cursor = -2;
            }
            if ((Key.press(524288) || (Key.touchgamepausereturn.IsButtonPress() && this.pause_item_cursor == -2)) && !this.pause_returnFlag) {
                this.pause_returnFlag = true;
                this.pause_returnframe = this.pausecnt;
                SoundSystem.getInstance().playSe(2);
            }
            if (this.pause_returnFlag) {
                if (this.pausecnt > this.pause_returnframe && this.pausecnt <= this.pause_returnframe + 3) {
                    this.pause_saw_x -= this.pause_saw_speed;
                    this.pause_item_x -= this.pause_item_speed;
                } else if (this.pausecnt > this.pause_returnframe + 3) {
                    BacktoGame();
                    isDrawTouchPad = true;
                    this.pause_returnFlag = false;
                }
            }
            if (this.pause_optionFlag && this.pausecnt > this.pause_optionframe + 3) {
                optionInit();
                this.state = 8;
                this.pause_optionFlag = false;
            }
            if (!State.fadeChangeOver()) {
                for (i = 0; i < 3; i++) {
                    Key.touchgamepauseitem[i].resetKeyState();
                }
            } else if (Key.touchgamepauseitem[0].IsButtonPress() && this.pause_item_cursor == 0 && !this.pause_optionFlag) {
                this.pause_returnFlag = true;
                this.pause_returnframe = this.pausecnt;
                SoundSystem.getInstance().playSe(1);
            } else if (Key.touchgamepauseitem[1].IsButtonPress() && this.pause_item_cursor == 1 && State.fadeChangeOver()) {
                this.state = 7;
                State.fadeInit(102, 220);
                secondEnsureInit();
                SoundSystem.getInstance().playSe(1);
            } else if (Key.touchgamepauseitem[2].IsButtonPress() && this.pause_item_cursor == 2 && !this.pause_optionFlag) {
                changeStateWithFade(8);
                optionInit();
                SoundSystem.getInstance().playSe(1);
            }
        }
    }

    private void pauseDraw(MFGraphics g) {
        State.drawFade(g);
        if (this.pausecnt > 5) {
            muiAniDrawer.setActionId(50);
            muiAniDrawer.draw(g, this.pause_saw_x, this.pause_saw_y);
        }
        if (this.pausecnt > 7) {
            muiAniDrawer.setActionId((Key.touchgamepausereturn.Isin() ? 5 : 0) + 61);
            muiAniDrawer.draw(g, 0, SCREEN_HEIGHT);
        }
        if (this.pausecnt > 5) {
            int i;
            AnimationDrawer animationDrawer = muiAniDrawer;
            if (this.pause_item_cursor == 0 && Key.touchgamepauseitem[0].Isin()) {
                i = 1;
            } else {
                i = 0;
            }
            animationDrawer.setActionId(i + 2);
            muiAniDrawer.draw(g, this.pause_item_x, this.pause_item_y);
            animationDrawer = muiAniDrawer;
            if (this.pause_item_cursor == 1 && Key.touchgamepauseitem[1].Isin()) {
                i = 1;
            } else {
                i = 0;
            }
            animationDrawer.setActionId(i + 10);
            muiAniDrawer.draw(g, this.pause_item_x, this.pause_item_y + 24);
            animationDrawer = muiAniDrawer;
            if (this.pause_item_cursor == 2 && Key.touchgamepauseitem[2].Isin()) {
                i = 1;
            } else {
                i = 0;
            }
            animationDrawer.setActionId(i + 12);
            muiAniDrawer.draw(g, this.pause_item_x, this.pause_item_y + 48);
        }
    }

    private int itemsid(int id) {
        int itemsidoffset = (this.optionOffsetY / 24) * 2;
        if (id + itemsidoffset < 0) {
            return 0;
        }
        if (id + itemsidoffset > 9) {
            return 9;
        }
        return id + itemsidoffset;
    }

    private void optionInit() {
        if (muiAniDrawer == null) {
            muiAniDrawer = new Animation("/animation/mui").getDrawer(0, false, 0);
        }
        this.optionOffsetX = 0;
        Key.touchGamePauseOptionInit();
        this.pauseOptionCursor = 0;
        this.isOptionDisFlag = false;
        Key.touchMenuOptionInit();
        this.optionOffsetYAim = 0;
        this.optionOffsetY = 0;
        this.isChanged = false;
        this.optionslide_getprey = -1;
        this.optionslide_gety = -1;
        this.optionslide_y = 0;
        this.optionDrawOffsetBottomY = 0;
        this.optionYDirect = 0;
    }

    private void optionLogic() {
        int i;
        if (!this.isOptionDisFlag) {
            SoundSystem.getInstance().playBgm(5);
            this.isOptionDisFlag = true;
        }
        if (Key.touchmenuoptionreturn.Isin() && Key.touchmenuoption.IsClick()) {
            this.returnCursor = 1;
        }
        this.optionslide_gety = Key.slidesensormenuoption.getPointerY();
        this.optionslide_y = 0;
        this.optionslidefirsty = 0;
        for (i = 0; i < (Key.touchmenuoptionitems.length >> 1); i++) {
            Key.touchmenuoptionitems[i * 2].setStartY((((i * 24) + 28) + this.optionDrawOffsetY) + this.optionslide_y);
            Key.touchmenuoptionitems[(i * 2) + 1].setStartY((((i * 24) + 28) + this.optionDrawOffsetY) + this.optionslide_y);
        }
        if (this.isSelectable) {
            for (i = 0; i < Key.touchmenuoptionitems.length; i++) {
                if (Key.touchmenuoptionitems[i].Isin() && Key.touchmenuoption.IsClick()) {
                    this.pauseOptionCursor = i / 2;
                    this.returnCursor = 0;
                    break;
                }
            }
        }
        if (Key.touchmenuoptionreturn.Isin() && Key.touchmenuoption.IsClick()) {
            this.returnCursor = 1;
        }
        if ((Key.press(524288) || (Key.touchmenuoptionreturn.IsButtonPress() && this.returnCursor == 1)) && State.fadeChangeOver()) {
            changeStateWithFade(6);
            SoundSystem.getInstance().stopBgm(false);
            SoundSystem.getInstance().playSe(2);
            GlobalResource.saveSystemConfig();
        }
        if (Key.slidesensormenuoption.isSliding()) {
            this.isSelectable = true;
        } else {
            if (this.isOptionChange && this.optionslide_y == 0) {
                this.optionDrawOffsetY = this.optionDrawOffsetTmpY1;
                this.isOptionChange = false;
                this.optionYDirect = 0;
            }
            if (!this.isOptionChange) {
                int speed;
                if (this.optionDrawOffsetY > 0) {
                    this.optionYDirect = 1;
                    speed = (-this.optionDrawOffsetY) >> 1;
                    if (speed > -2) {
                        speed = -2;
                    }
                    if (this.optionDrawOffsetY + speed <= 0) {
                        this.optionDrawOffsetY = 0;
                        this.optionYDirect = 0;
                    } else {
                        this.optionDrawOffsetY += speed;
                    }
                } else if (this.optionDrawOffsetY < this.optionDrawOffsetBottomY) {
                    this.optionYDirect = 2;
                    speed = (this.optionDrawOffsetBottomY - this.optionDrawOffsetY) >> 1;
                    if (speed < 2) {
                        speed = 2;
                    }
                    if (this.optionDrawOffsetY + speed >= this.optionDrawOffsetBottomY) {
                        this.optionDrawOffsetY = this.optionDrawOffsetBottomY;
                        this.optionYDirect = 0;
                    } else {
                        this.optionDrawOffsetY += speed;
                    }
                }
            }
        }
        if (this.isSelectable && this.optionYDirect == 0) {
            if (Key.touchmenuoptionitems[1].IsButtonPress() && this.pauseOptionCursor == 0 && GlobalResource.soundSwitchConfig != 0 && State.fadeChangeOver()) {
                this.state = 15;
                soundVolumnInit();
                SoundSystem.getInstance().playSe(1);
            } else if (Key.touchmenuoptionitems[3].IsButtonPress() && this.pauseOptionCursor == 1 && State.fadeChangeOver()) {
                this.state = 10;
                itemsSelect2Init();
                SoundSystem.getInstance().playSe(1);
            } else if (Key.touchmenuoptionitems[5].IsButtonPress() && this.pauseOptionCursor == 2 && State.fadeChangeOver()) {
                this.state = 12;
                itemsSelect2Init();
                SoundSystem.getInstance().playSe(1);
            } else if (Key.touchmenuoptionitems[7].IsButtonPress() && this.pauseOptionCursor == 3 && GlobalResource.spsetConfig != 0 && State.fadeChangeOver()) {
                this.state = 16;
                spSenorSetInit();
                SoundSystem.getInstance().playSe(1);
            } else if (Key.touchmenuoptionitems[8].IsButtonPress() && this.pauseOptionCursor == 4 && State.fadeChangeOver()) {
                changeStateWithFade(13);
                AnimationDrawer.setAllPause(false);
                helpInit();
                SoundSystem.getInstance().playSe(1);
            }
        }
        this.optionslide_getprey = this.optionslide_gety;
    }

    private void releaseOptionItemsTouchKey() {
        for (TouchKeyRange resetKeyState : Key.touchmenuoptionitems) {
            resetKeyState.resetKeyState();
        }
    }

    private void optionDraw(MFGraphics g) {
        int i;
        g.setColor(0);
        MyAPI.fillRect(g, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
        muiAniDrawer.setActionId(52);
        for (int i2 = 0; i2 < (SCREEN_WIDTH / 48) + 1; i2++) {
            for (int j = 0; j < (SCREEN_HEIGHT / 48) + 1; j++) {
                muiAniDrawer.draw(g, i2 * 48, j * 48);
            }
        }
        if (this.state != 8) {
            this.pauseOptionCursor = -2;
        }
        muiAniDrawer.setActionId(25);
        muiAniDrawer.draw(g, (SCREEN_WIDTH >> 1) - 96, ((this.optionDrawOffsetY + 40) + this.optionslide_y) + 0);
        AnimationDrawer animationDrawer = muiAniDrawer;
        if (GlobalResource.soundSwitchConfig == 0) {
            i = 67;
        } else {
            i = (Key.touchmenuoptionitems[1].Isin() && this.pauseOptionCursor == 0 && this.isSelectable) ? 1 : 0;
            i += 57;
        }
        animationDrawer.setActionId(i);
        muiAniDrawer.draw(g, (SCREEN_WIDTH >> 1) + 56, ((this.optionDrawOffsetY + 40) + this.optionslide_y) + 0);
        muiAniDrawer.setActionId(GlobalResource.soundConfig + 73);
        muiAniDrawer.draw(g, (SCREEN_WIDTH >> 1) + 56, ((this.optionDrawOffsetY + 40) + this.optionslide_y) + 0);
        muiAniDrawer.setActionId(21);
        muiAniDrawer.draw(g, (SCREEN_WIDTH >> 1) - 96, ((this.optionDrawOffsetY + 40) + this.optionslide_y) + 24);
        animationDrawer = muiAniDrawer;
        if (Key.touchmenuoptionitems[3].Isin() && this.pauseOptionCursor == 1 && this.isSelectable) {
            i = 1;
        } else {
            i = 0;
        }
        animationDrawer.setActionId(i + 57);
        muiAniDrawer.draw(g, (SCREEN_WIDTH >> 1) + 56, ((this.optionDrawOffsetY + 40) + this.optionslide_y) + 24);
        animationDrawer = muiAniDrawer;
        if (GlobalResource.vibrationConfig == 0) {
            i = 1;
        } else {
            i = 0;
        }
        animationDrawer.setActionId(i + 35);
        muiAniDrawer.draw(g, (SCREEN_WIDTH >> 1) + 56, ((this.optionDrawOffsetY + 40) + this.optionslide_y) + 24);
        muiAniDrawer.setActionId(23);
        muiAniDrawer.draw(g, (SCREEN_WIDTH >> 1) - 96, ((this.optionDrawOffsetY + 40) + this.optionslide_y) + 48);
        animationDrawer = muiAniDrawer;
        if (Key.touchmenuoptionitems[5].Isin() && this.pauseOptionCursor == 2 && this.isSelectable) {
            i = 1;
        } else {
            i = 0;
        }
        animationDrawer.setActionId(i + 57);
        muiAniDrawer.draw(g, (SCREEN_WIDTH >> 1) + 56, ((this.optionDrawOffsetY + 40) + this.optionslide_y) + 48);
        animationDrawer = muiAniDrawer;
        if (GlobalResource.spsetConfig == 0) {
            i = 0;
        } else {
            i = 1;
        }
        animationDrawer.setActionId(i + 37);
        muiAniDrawer.draw(g, (SCREEN_WIDTH >> 1) + 56, ((this.optionDrawOffsetY + 40) + this.optionslide_y) + 48);
        muiAniDrawer.setActionId(24);
        muiAniDrawer.draw(g, (SCREEN_WIDTH >> 1) - 96, ((this.optionDrawOffsetY + 40) + this.optionslide_y) + 72);
        animationDrawer = muiAniDrawer;
        if (GlobalResource.spsetConfig == 0) {
            i = 67;
        } else {
            i = (Key.touchmenuoptionitems[7].Isin() && this.pauseOptionCursor == 3 && this.isSelectable) ? 1 : 0;
            i += 57;
        }
        animationDrawer.setActionId(i);
        muiAniDrawer.draw(g, (SCREEN_WIDTH >> 1) + 56, ((this.optionDrawOffsetY + 40) + this.optionslide_y) + 72);
        switch (GlobalResource.sensorConfig) {
            case 0:
                muiAniDrawer.setActionId(70);
                break;
            case 1:
                muiAniDrawer.setActionId(69);
                break;
            case 2:
                muiAniDrawer.setActionId(68);
                break;
        }
        muiAniDrawer.draw(g, (SCREEN_WIDTH >> 1) + 56, ((this.optionDrawOffsetY + 40) + this.optionslide_y) + 72);
        animationDrawer = muiAniDrawer;
        if (Key.touchmenuoptionitems[8].Isin() && this.pauseOptionCursor == 4 && this.isSelectable) {
            i = 1;
        } else {
            i = 0;
        }
        animationDrawer.setActionId(i + 27);
        muiAniDrawer.draw(g, (SCREEN_WIDTH >> 1) - 96, ((this.optionDrawOffsetY + 40) + this.optionslide_y) + 96);
        this.optionOffsetX -= 4;
        this.optionOffsetX %= 100;
        muiAniDrawer.setActionId(51);
        for (int x1 = this.optionOffsetX; x1 < SCREEN_WIDTH * 2; x1 += 100) {
            muiAniDrawer.draw(g, x1, 0);
        }
        animationDrawer = muiAniDrawer;
        if (Key.touchmenuoptionreturn.Isin()) {
            i = 5;
        } else {
            i = 0;
        }
        animationDrawer.setActionId(i + 61);
        muiAniDrawer.draw(g, 0, SCREEN_HEIGHT);
        State.drawFade(g);
    }

    private void BacktoGame() {
        this.state = 2;
        State.fadeInit(102, 0);
        SoundSystem instance = SoundSystem.getInstance();
        SoundSystem.getInstance();
        instance.playBgm(35);
        Key.initSonic();
        SpecialObject.player.velZ = this.preVelZ;
        AnimationDrawer.setAllPause(false);
        if (Key.touchspstagepause != null) {
            Key.touchspstagepause.resetKeyState();
        }
    }

    public void pause() {
        if (this.state != 14 && this.state != 6) {
            if (this.state == 2 && SpecialObject.player.isNeedTouchPad()) {
                pauseInit();
            } else {
                this.interrupt_state = this.state;
                this.state = 14;
                interruptInit();
            }
            if (this.interrupt_state == 4) {
                this.endingInstance.pause();
            }
        }
    }

    private void interruptInit() {
        State.fadeInitAndStart(0, 0);
        if (SpecialObject.player.velZ != 0) {
            this.preVelZ = SpecialObject.player.velZ;
        }
        SpecialObject.player.velZ = 0;
        if (this.interruptDrawer == null) {
            this.interruptDrawer = Animation.getInstanceFromQi("/animation/utl_res/suspend_resume.dat")[0].getDrawer(0, true, 0);
        }
        isDrawTouchPad = false;
        Key.touchInterruptInit();
    }

    private void interruptLogic() {
        if (Key.touchinterruptreturn != null && Key.touchinterruptreturn.IsButtonPress()) {
            SoundSystem.getInstance().playSe(2);
            Key.touchInterruptClose();
            if (Key.touchitemsselect2_1 != null) {
                Key.touchitemsselect2_1.reset();
            }
            if (Key.touchitemsselect2_2 != null) {
                Key.touchitemsselect2_2.reset();
            }
            if (Key.touchitemsselect3_1 != null) {
                Key.touchitemsselect3_1.reset();
            }
            if (Key.touchitemsselect3_2 != null) {
                Key.touchitemsselect3_2.reset();
            }
            if (Key.touchitemsselect3_3 != null) {
                Key.touchitemsselect3_3.reset();
            }
            this.state = this.interrupt_state;
            this.interrupt_state = -1;
            Key.clear();
            isDrawTouchPad = true;
            fading = false;
            switch (this.state) {
                case 0:
                case 1:
                    State.fadeInit(0, 0);
                    SpecialObject.player.velZ = this.preVelZ;
                    return;
                case 2:
                    State.fadeInit(192, 192);
                    SpecialObject.player.velZ = this.preVelZ;
                    SoundSystem.getInstance().playBgm(35);
                    return;
                case 4:
                    this.endingInstance.setOverFromInterrupt();
                    return;
                case 7:
                    State.fadeInit(192, 192);
                    isDrawTouchPad = false;
                    return;
                case 8:
                    optionInit();
                    isDrawTouchPad = false;
                    return;
                case 9:
                case 10:
                case 11:
                case 12:
                case 15:
                case 16:
                    State.fadeInit(192, 192);
                    return;
                default:
                    return;
            }
        }
    }

    private void interruptDraw(MFGraphics g) {
        this.interruptDrawer.setActionId((Key.touchinterruptreturn.Isin() ? 1 : 0) + 0);
        this.interruptDrawer.draw(g, SCREEN_WIDTH >> 1, SCREEN_HEIGHT >> 1);
    }

    public void drawWord(MFGraphics g, int wordID, int x, int y) {
        switch (wordID) {
            case 0:
                this.fontDrawer.draw(g, PlayerObject.getCharacterID() + 8, x, y, false, 0);
                this.fontDrawer.draw(g, 12, x, y, false, 0);
                return;
            case 1:
                this.fontDrawer.draw(g, 13, x, y, false, 0);
                return;
            default:
                return;
        }
    }

    public int getWordLength(int wordID) {
        return 320;
    }

    private void backToGameStage() {
        State.setState(7);
    }

    public static void loadData() {
        byte[] record = Record.loadRecord(Record.EMERALD_RECORD);
        int i;
        if (record != null) {
            DataInputStream ds = new DataInputStream(new ByteArrayInputStream(record));
            i = 0;
            while (i < emeraldStatus.length) {
                try {
                    emeraldStatus[i] = ds.readByte();
                    i++;
                } catch (Exception e) {
                    for (i = 0; i < emeraldStatus.length; i++) {
                        emeraldStatus[i] = 0;
                    }
                    return;
                }
            }
            return;
        }
        for (i = 0; i < emeraldStatus.length; i++) {
            emeraldStatus[i] = 0;
        }
    }

    public static void saveData() {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        DataOutputStream ds = new DataOutputStream(os);
        int i = 0;
        while (i < emeraldStatus.length) {
            try {
                ds.writeByte(emeraldStatus[i]);
                i++;
            } catch (Exception e) {
                return;
            }
        }
        Record.saveRecord(Record.EMERALD_RECORD, os.toByteArray());
    }

    public static void emptyEmeraldArray() {
        for (int i = 0; i < emeraldStatus.length; i++) {
            emeraldStatus[i] = 0;
        }
        saveData();
    }

    public static boolean emeraldMissed() {
        for (int i : emeraldStatus) {
            if (i != 1) {
                return true;
            }
        }
        return false;
    }

    public static int emeraldID(int stageId) {
        return STAGE_ID_TO_SPECIAL_ID[stageId];
    }

    public static int emeraldState(int stageId) {
        return emeraldStatus[emeraldID(stageId)];
    }

    public static void setEmeraldState(int stageId, int state) {
        emeraldStatus[emeraldID(stageId)] = state;
    }

    public void changeStateWithFade(int nState) {
        if (!fading) {
            fading = true;
            if (nState == 8) {
                State.fadeInit(102, 255);
            } else {
                State.fadeInit(0, 255);
            }
            this.nextState = nState;
            this.fadeChangeState = true;
        }
    }

    public void fadeStateLogic() {
        if (fading && this.fadeChangeState && State.fadeChangeOver() && this.state != this.nextState) {
            this.state = this.nextState;
            this.fadeChangeState = false;
            if (this.state == 6) {
                State.fadeInit_Modify(255, 102);
            } else {
                State.fadeInit(255, 0);
            }
        }
        if (this.state == this.nextState && State.fadeChangeOver()) {
            fading = false;
        }
    }
}
