package State;

import GameEngine.Key;
import GameEngine.TouchDirectKey;
import Lib.Animation;
import Lib.AnimationDrawer;
import Lib.MyAPI;
import Lib.Record;
import Lib.SoundSystem;
import SonicGBA.GameObject;
import SonicGBA.GlobalResource;
import SonicGBA.SonicDef;
import SonicGBA.StageManager;
import android.os.Message;
import com.sega.MFLib.Main;
import com.sega.mobile.define.MDPhone;
import com.sega.mobile.framework.device.MFDevice;
import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;

public abstract class State implements SonicDef, StringIndex {
    public static final int ARROW_WAIT_FRAME = 4;
    public static final int BACKGROUND_WIDTH = 80;
    private static final int[] BAR_ANIMATION = new int[]{103, 102, 118};
    private static final int[] BAR_COLOR =  new int[]{255, 0, 16777215};
    public static final int BAR_HEIGHT = 20;
    public static final int BAR_ID_BLACK = 1;
    public static final int BAR_ID_BLUE = 0;
    public static final int BAR_ID_WHITE = 2;
    public static final int BG_NUM = (((SCREEN_WIDTH + 80) - 1) / 80);
    public static String[][] BPEffectStrings = null;
    public static String BPPayingTitle = null;
    public static final byte BP_ITEM_BLUE_SHELTER = (byte) 1;
    public static final byte BP_ITEM_GREEN_SHELTER = (byte) 2;
    public static final byte BP_ITEM_INVINCIBILITY = (byte) 0;
    public static final byte BP_ITEM_SPEED = (byte) 3;
    public static boolean BP_enteredPaying = false;
    public static int BP_itemsHeight = 0;
    public static MFImage BP_itemsImg = null;
    public static int BP_itemsWidth = 0;
    public static byte[] BP_items_num = new byte[4];
    public static int BP_wordsHeight = 0;
    public static MFImage BP_wordsImg = null;
    public static int BP_wordsWidth = 0;
    public static String[] BPstrings = null;
    public static final int CASE_HEIGHT = (SCREEN_HEIGHT - 60);
    public static final int CASE_WIDTH = MENU_RECT_WIDTH;
    public static final int CASE_X = ((SCREEN_WIDTH - MENU_RECT_WIDTH) >> 1);
    public static final int CASE_Y = 30;
    public static final int COMFIRM_FRAME_ID = 100;
    public static final int COMFIRM_ID = 10;
    public static final int COMFIRM_X = (SCREEN_WIDTH >> 1);
    public static final int COMFIRM_Y = (SCREEN_HEIGHT >> 1);
    private static final int CONFIRM_FRAME_OFFSET_X = Math.max(FONT_WIDTH * 6, 72);
    private static final int CONFIRM_FRAME_OFFSET_Y = ((MENU_SPACE > 32 ? MENU_SPACE : 32) + 20);
    private static final int CONFIRM_OFFSET_X = 0;
    private static final int CONFIRM_STR_OFFSET_X = 44;
    public static final int DRAW_NUM = 2;
    public static final int EMERALD_STATE_FAILD = 2;
    public static final int EMERALD_STATE_NONENTER = 0;
    public static final int EMERALD_STATE_SUCCESS = 1;
    private static final int FADE_FILL_HEIGHT = 40;
    private static final int FADE_FILL_WIDTH = 40;
    public static final int FONT_MOVE_SPEED = 8;
    private static final int FRAME_DOWN = 117;
    public static final int FRAME_HEIGHT = ((SCREEN_HEIGHT - 30) - 30);
    private static final int FRAME_MIDDLE = 116;
    private static final int FRAME_OFFSET_Y = 24;
    private static final int FRAME_UP = 115;
    public static final int FRAME_WIDTH = MENU_RECT_WIDTH;
    public static final int FRAME_X = ((SCREEN_WIDTH - MENU_RECT_WIDTH) >> 1);
    public static final int FRAME_Y = 30;
    private static final int HELP_PAGE_BACKGROUND_WIDTH = 64;
    public static boolean IsAllClear = false;
    public static boolean IsGameOver = false;
    public static boolean IsInInterrupt = false;
    public static boolean IsPaid = false;
    public static boolean IsTimeOver = false;
    public static final int LOADING_TYPE_BAR = 2;
    public static final int LOADING_TYPE_BLACK = 0;
    public static final int LOADING_TYPE_WHITE = 1;
    private static final int MENU_BG_COLOR_1 = 16777215;
    private static final int MENU_BG_COLOR_2 = 15658734;
    private static final int MENU_BG_OFFSET = 24;
    private static final int MENU_BG_SPEED = MyAPI.zoomIn(1);
    private static final int MENU_BG_WIDTH = 48;
    private static final int MENU_FRAME_BACK_COLOR = 16763904;
    private static final int MENU_FRAME_BAR_COLOR = 16750916;
    public static final int MOVE_DIRECTION_FONT = 80;
    public static final int MOVE_DIRECTION = (MOVE_DIRECTION_FONT > 120 ? MOVE_DIRECTION_FONT : 120);
     public static final int MENU_TITLE_DRAW_OFFSET_Y = MOVING_TITLE_TOP ? 10 : SCREEN_HEIGHT < 220 ? 10 : 30;
    public static final int MENU_TITLE_MOVE_DIRECTION = (MOVE_DIRECTION_FONT > 80 ? MOVE_DIRECTION_FONT : 80);
    public static final int MENU_TITLE_DRAW_NUM = ((((SCREEN_WIDTH + MENU_TITLE_MOVE_DIRECTION) - 1) / MENU_TITLE_MOVE_DIRECTION) + 2);

    public static final int MESSAGE_EXIT = 5;
    public static final int MESSAGE_GAME_TOP = 2;
    public static final int MESSAGE_INIT_SDK = 0;
    public static final int MESSAGE_PAGE = 1;
    public static final int MESSAGE_RANKING = 3;
    public static final int MESSAGE_SAVE = 6;
    public static final int MESSAGE_USER = 4;
    public static final int MORE_GAME_COMFIRM = 3;
    private static final int[] OPTION_SOUND = new int[]{55, 58, 57, 56};
    public static final int PAGE_BACKGROUND_HEIGHT = 56;
    public static final int PAGE_BACKGROUND_SPEED = 1;
    private static final int PAUSE_SOUND_ITEMS = 4;
    public static int PageBackGroundOffsetX = 0;
    public static int PageBackGroundOffsetY = 0;
    public static int PageFrameCnt = 0;
    public static final int QUIT_COMFIRM = 9;
    public static final int RETURN_PRESSED = 400;
    public static int SELECT_BAR_WIDTH = 40;
    private static final int SELECT_BOX_COLOR = 16773039;
    private static final int SELECT_BOX_WIDTH = (FONT_H + 4);
    private static final int SOFT_KEY_OFFSET = 2;
    private static final int SOUND_VOLUMN_WAIT_FRAME = 4;
    public static final int STAGE_MOVE_DIRECTION = 224;
    public static final int STATE_ENDING = 8;
    public static final int STATE_EXTRA_ENDING = 11;
    public static final int STATE_GAME = 1;
    public static final int STATE_NORMAL_ENDING = 10;
    public static final int STATE_RETURN_FROM_GAME = 2;
    public static final int STATE_SCORE_RANKING = 4;
    public static final int STATE_SELECT_CHARACTER = 9;
    public static final int STATE_SELECT_NORMAL_STAGE = 5;
    public static final int STATE_SELECT_RACE_STAGE = 3;
    public static final int STATE_SPECIAL = 6;
    public static final int STATE_SPECIAL_ENDING = 12;
    public static final int STATE_SPECIAL_TO_GMAE = 7;
    public static final int STATE_TITLE = 0;
    private static final int STR_NO = 149;
    private static final int STR_YES = 148;
    public static final int TEXT_DISPLAY_WIDTH = 188;
    public static final byte TXT_BLUE_SHELTER_EFFECT = (byte) 14;
    public static final byte TXT_BUY_TOOL = (byte) 18;
    public static final byte TXT_BUY_TOOLS_TEXT = (byte) 8;
    public static final byte TXT_BUY_TOOLS_TITLE = (byte) 7;
    public static final byte TXT_CONTINUE_TRY = (byte) 2;
    public static final byte TXT_EFFECT = (byte) 12;
    public static final byte TXT_GREEN_SHELTER_EFFECT = (byte) 15;
    public static final byte TXT_HOLD_NUM = (byte) 11;
    public static final byte TXT_INVINCIBILITY_EFFECT = (byte) 16;
    public static final byte TXT_MENU = (byte) 10;
    public static final byte TXT_PAY = (byte) 1;
    public static final byte TXT_PAY_TEXT = (byte) 4;
    public static final byte TXT_PAY_TITLE = (byte) 3;
    public static final byte TXT_REVIVE_TEXT = (byte) 6;
    public static final byte TXT_REVIVE_TITLE = (byte) 5;
    public static final byte TXT_SHOP = (byte) 9;
    public static final byte TXT_SPEED_EFFECT = (byte) 13;
    public static final byte TXT_TOOL = (byte) 17;
    public static final byte TXT_TOOL_MAX = (byte) 19;
    public static final byte TXT_TRY_TITLE = (byte) 0;
    public static final byte TXT_USE_BLUE_SHELTER = (byte) 21;
    public static final byte TXT_USE_GREEN_SHELTER = (byte) 22;
    public static final byte TXT_USE_INVINCIBILITY = (byte) 20;
    public static final byte TXT_USE_SPEED = (byte) 23;
    protected static final int VOL_IMAGE_WIDTH = 8;
    public static int VOLUME_X = ((SCREEN_WIDTH - (((VOL_IMAGE_WIDTH - 1) * 10) + 1)) >> 1);
    protected static final int VOL_SIGN_IMAGE_WIDTH = 10;
    public static final String WARNING_STR_ORIGNAL = "警告：开始新游戏将覆盖您当前的游戏进度";
    public static final int WARNING_X = (SCREEN_WIDTH > SCREEN_HEIGHT ? 50 : 26);
    public static final int WARNING_FONT_WIDTH = (SCREEN_WIDTH - ((WARNING_X + 5) * 2));
    public static final String[] WARNING_STR = MyAPI.getStrings(WARNING_STR_ORIGNAL, WARNING_FONT_WIDTH);
    public static final int WARNING_HEIGHT = ((WARNING_STR.length * LINE_SPACE) + 20);
    public static final int WARNING_WIDTH = (SCREEN_WIDTH - (WARNING_X * 2));
    public static final int WARNING_Y_DES = (SCREEN_HEIGHT - WARNING_HEIGHT);
    public static final int WARNING_Y_DES_2 = (SCREEN_HEIGHT + 40);
    public static final int TOOL_TIP_FONT_WIDTH = WARNING_FONT_WIDTH;
    public static final String[] RESET_STR = MyAPI.getStrings("记录已重置！", WARNING_FONT_WIDTH);
    public static final int RESET_HEIGHT = ((RESET_STR.length * LINE_SPACE) + 20);
    public static final int RESET_Y_DES = (SCREEN_HEIGHT - RESET_HEIGHT);
    public static int TOOL_TIP_HEIGHT = 0;
    public static String[] TOOL_TIP_STR = null;
    public static String TOOL_TIP_STR_ORIGNAL = null;
    public static final int TOOL_TIP_WIDTH = WARNING_WIDTH;
    public static int TOOL_TIP_X = WARNING_X;
    public static int TOOL_TIP_Y_DES = 0;
    public static int TOOL_TIP_Y_DES_2 = WARNING_Y_DES_2;
    public static String[] aboutStrings;
    public static Main activity;
    public static String[] allStrings;
    public static Animation arrowAnimation;
    public static AnimationDrawer downArrowDrawer;
    private static int fadeAlpha = 40;
    private static int fadeFromValue;
    private static int[] fadeRGB = new int[1600];
    private static int fadeToValue;
    public static boolean fading;
    public static String[] helpStrings;
    private static String helpTitleString;
    public static boolean isDrawTouchPad;
    public static boolean isInSPStage = false;
    public static boolean lastFading;
    public static AnimationDrawer leftArrowDrawer;
    public static int loadingType = 0;
    public static MFImage menuBg;
    public static AnimationDrawer menuFontDrawer;
    public static AnimationDrawer muiAniDrawer;
    public static AnimationDrawer muiDownArrowDrawer;
    public static AnimationDrawer muiLeftArrowDrawer;
    public static AnimationDrawer muiRightArrowDrawer;
    public static AnimationDrawer muiUpArrowDrawer;
    private static int pause_x = (SCREEN_WIDTH - 40);
    private static int pause_y = 17;
    private static int preFadeAlpha;
    public static AnimationDrawer rightArrowDrawer;
    protected static MFImage skipImage;
    private static int softKeyHeight = 0;
    private static MFImage softKeyImage;
    private static int softKeyWidth = 0;
    private static State state;
    private static int stateId = -1;
    public static String[] strForShow;
    public static int tool_id;
    public static MFImage touchgamekeyImage;
    public static Animation touchgamekeyboardAnimation;
    public static AnimationDrawer touchgamekeyboardDrawer;
    public static Animation touchkeyboardAnimation;
    public static AnimationDrawer touchkeyboardDrawer;
    public static int trytimes = 3;
    public static AnimationDrawer upArrowDrawer;
    protected static MFImage volumeImage;
    public static int warningY = WARNING_Y_DES_2;
    public boolean IsInBP = false;
    public int MORE_GAME_HEIGHT = ((MENU_SPACE * 4) + 20);
    public int MORE_GAME_START_X = FRAME_X;
    public int MORE_GAME_START_Y = ((SCREEN_HEIGHT - this.MORE_GAME_HEIGHT) >> 1);
    public int MORE_GAME_WIDTH = FRAME_WIDTH;
    public int arrowframecnt;
    public int arrowindex = -1;
    public int confirmcursor = 0;
    public int confirmframe;
    public int[] currentElement;
    public int cursor;
    public int elementNum;
    public int finalitemsselectcursor;
    private int helpIndex;
    public boolean isArrowClicked = false;
    public boolean isConfirm;
    public boolean isItemsSelect;
    private boolean isSoundVolumnClick;
    public int itemsselectcursor;
    public int itemsselectframe;
    public int mainMenuItemCursor;
    public boolean menuMoving = true;
    public int pauseoptionCursor;
    public int returnCursor = 0;
    public int returnPageCursor = 0;
    public int selectMenuOffsetX = 0;
    public MFImage textBGImage;
    private int timeCnt;
    public int titleBgOffsetX;
    public int titleBgOffsetY;
    public int tooltipY = TOOL_TIP_Y_DES_2;

    public abstract void close();

    public abstract void draw(MFGraphics mFGraphics);

    public abstract void init();

    public abstract void logic();

    public abstract void pause();

    /*
    static {
        int[] iArr = new int[3];
        iArr[0] = 255;
        iArr[2] = 16777215;
       // BAR_COLOR = new int[3];
        int i = MOVING_TITLE_TOP ? 10 : SCREEN_HEIGHT < 220 ? 10 : 30;
        MENU_TITLE_DRAW_OFFSET_Y = i;
    }
*/

    public static void setState(int mStateId) {
        if (stateId != mStateId) {
            if (state != null) {
                state.close();
                state = null;
                System.gc();
            }
            stateId = mStateId;
            switch (stateId) {
                case STATE_TITLE:
                    state = new TitleState();
                    break;
                case STATE_GAME:
                    Key.touchCharacterSelectModeClose();
                    state = new GameState();
                    break;
                case STATE_RETURN_FROM_GAME:
                    Key.touchkeyboardClose();
                    break;
                case 3:
                case 4:
                case 5:
                case 9:
                    break;
                case STATE_SPECIAL:
                    state = new SpecialStageState();
                    break;
                case STATE_SPECIAL_TO_GMAE:
                    state = new GameState(stateId);
                    break;
                case STATE_NORMAL_ENDING:
                    state = new EndingState(0);
                    break;
                case STATE_EXTRA_ENDING:
                    state = new EndingState(1);
                    break;
                case STATE_SPECIAL_ENDING:
                    state = new EndingState(2);
                    break;
                default:
                    state = new TitleState(stateId);
                    break;
            }
            //
            state.init();
            isDrawTouchPad = false;
            if (stateId == STATE_SPECIAL) {
                isInSPStage = true;
            } else {
                isInSPStage = false;
            }
        }
    }

    public static void stateLogic() {
        SoundSystem.getInstance().exec();
        if (state != null) {
            try {
                arrowLogic();
                state.logic();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void stateDraw(MFGraphics g) {
        if (state != null) {
            try {
                MyAPI.setClip(g, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
                state.draw(g);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (fading) {
                drawFade(g);
            }
        }
    }

    public static void stateInit() {
        Key.init();
        Record.initRecord();
        initArrowDrawer();
        StageManager.loadStageRecord();
        GlobalResource.loadSystemConfig();
        SpecialStageState.loadData();
        StageManager.loadHighScoreRecord();
    }

    public static void exitGame() {
        if (state != null) {
            state.close();
            state = null;
        }
        MFDevice.notifyExit();
    }

    public static void statePause() {
        if (state != null) {
            state.pause();
        }
        SoundSystem.getInstance().stopBgm(true);
    }

    public static void pauseTrigger() {
        if (SoundSystem.getInstance().bgmPlaying2()) {
            SoundSystem.getInstance().stopBgm(true);
            SoundSystem.getInstance().stopLongSe();
            SoundSystem.getInstance().stopLoopSe();
        }
    }

    private static void initArrowDrawer() {
    }

    public static void arrowLogic() {
    }

    public static void setFadeColor(int color) {
        for (int i = 0; i < fadeRGB.length; i++) {
            fadeRGB[i] = color;
        }
    }

    public static void fadeInit(int from, int to) {
        if (to == 220 || to == 102) {
            to = 192;
        }
        fadeFromValue = from;
        fadeToValue = to;
        fadeAlpha = fadeFromValue;
        preFadeAlpha = -1;
    }

    public static void fadeInit_Modify(int from, int to) {
        fadeFromValue = from;
        fadeToValue = to;
        fadeAlpha = fadeFromValue;
        preFadeAlpha = -1;
    }

    public static int getCurrentFade() {
        return fadeAlpha;
    }

    public static void fadeInitAndStart(int from, int to) {
        fadeFromValue = from;
        fadeToValue = to;
        fadeAlpha = fadeFromValue;
        preFadeAlpha = -1;
        fading = true;
    }

    private static void drawFadeCore(MFGraphics g) {
        if (fadeAlpha != 0) {
            int w;
            int h;
            if (preFadeAlpha != fadeAlpha) {
                for (w = 0; w < 40; w++) {
                    for (h = 0; h < 40; h++) {
                        fadeRGB[(h * 40) + w] = ((fadeAlpha << 24) & -16777216) | (fadeRGB[(h * 40) + w] & 16777215);
                    }
                }
                preFadeAlpha = fadeAlpha;
            }
            for (w = 0; w < MyAPI.zoomOut(SCREEN_WIDTH); w += 40) {
                for (h = 0; h < MyAPI.zoomOut(SCREEN_HEIGHT); h += 40) {
                    g.drawRGB(fadeRGB, 0, 40, w, h, 40, 40, true);
                }
            }
        }
    }

    public static void drawFadeBase(MFGraphics g, int vel2) {
        fadeAlpha = MyAPI.calNextPosition((double) fadeAlpha, (double) fadeToValue, 1, vel2, 3.0d);
        drawFadeCore(g);
    }

    public static void drawFadeInSpeed(MFGraphics g, int vel) {
        if (fadeFromValue > fadeToValue) {
            fadeAlpha -= vel;
            if (fadeAlpha <= fadeToValue) {
                fadeAlpha = fadeToValue;
            }
        } else if (fadeFromValue < fadeToValue) {
            fadeAlpha += vel;
            if (fadeAlpha >= fadeToValue) {
                fadeAlpha = fadeToValue;
            }
        }
        drawFadeCore(g);
    }

    public static void drawFade(MFGraphics g) {
        drawFadeBase(g, 3);
    }

    public static void staticDrawFadeSlow(MFGraphics g) {
        if (state != null) {
            drawFadeSlow(g);
        }
    }

    public static void drawFadeSlow(MFGraphics g) {
        drawFadeBase(g, 6);
    }

    public static boolean fadeChangeOver() {
        return fadeAlpha == fadeToValue;
    }

    public static int getFade() {
        return fadeAlpha;
    }

    public static void setFadeOver() {
        fadeAlpha = fadeToValue;
    }

    public static void drawSoftKey(MFGraphics g, boolean IsLeft, boolean IsRight) {
        if (IsLeft) {
            drawLeftSoftKey(g);
        }
        if (IsRight) {
            drawRightSoftKey(g);
        }
    }

    private static void drawLeftSoftKey(MFGraphics g) {
    }

    private static void drawRightSoftKey(MFGraphics g) {
    }

    public static void drawSoftKeyPause(MFGraphics g) {
        if (!isInSPStage) {
            int offsetx = GameObject.stageModeState == 0 ? 0 : 32;
            if (Key.touchkey_pause == null) {
                return;
            }
            if (Key.touchkey_pause.Isin()) {
                drawTouchKeyBoardById(g, 14, SCREEN_WIDTH + offsetx, 0);
            } else {
                drawTouchKeyBoardById(g, 13, SCREEN_WIDTH + offsetx, 0);
            }
        } else if (Key.touchspstagepause == null) {
        } else {
            if (Key.touchspstagepause.Isin()) {
                drawTouchKeyBoardById(g, 14, SCREEN_WIDTH + 32, 0);
            } else {
                drawTouchKeyBoardById(g, 13, SCREEN_WIDTH + 32, 0);
            }
        }
    }

    public static void drawSkipSoftKey(MFGraphics g) {
    }

    public static void initMenuFont() {
    }

    public static void drawMenuFontById(MFGraphics g, int id, int x, int y) {
        drawMenuFontById(g, id, x, y, 0);
    }

    public static void drawMenuFontById(MFGraphics g, int id, int x, int y, int trans) {
    }

    public static void drawMenuStringById(MFGraphics g, int id, int x, int y, int trans) {
    }

    public static void drawMenuFontByString(MFGraphics g, String string, int x, int y, int anchor, int color1, int color2) {
        MyAPI.drawBoldString(g, string, x, y - FONT_H_HALF, anchor, color1, color2);
    }

    public static void drawMenuFontByString(MFGraphics g, int id, int x, int y) {
        drawMenuFontByString(g, id, x, y, 17);
    }

    public static void drawMenuFontByString(MFGraphics g, int id, int x, int y, int anchor) {
    }

    public static void drawMenuFontByString(MFGraphics g, String string, int x, int y, int anchor) {
        drawMenuFontByString(g, string, x, y, anchor, 16777215, 0);
    }

    public static int getMenuFontWidth(int id) {
        if (menuFontDrawer == null) {
            initMenuFont();
        }
        menuFontDrawer.setActionId(id);
        return menuFontDrawer.getCurrentFrameWidth();
    }

    public static void drawBar(MFGraphics g, int barID, int x, int y) {
        g.setColor(BAR_COLOR[barID]);
        MyAPI.fillRect(g, x, y - 10, SCREEN_WIDTH, 20);
    }

    public static void drawMenuBar(MFGraphics g, int barID, int x, int y) {
        for (int i = 0; i < BG_NUM; i++) {
            drawMenuFontById(g, BAR_ANIMATION[barID], (i * 80) + x, y);
        }
    }

    public static void drawBar(MFGraphics g, int barID, int y) {
        drawBar(g, barID, 0, y);
    }

    public static void fillMenuRect(MFGraphics g, int x, int y, int w, int h) {
        drawMenuFontById(g, FRAME_DOWN, x, y + h);
        drawMenuFontById(g, FRAME_DOWN, x + w, y + h, 2);
        if (h - 48 > 0) {
            MyAPI.setClip(g, x, y + 24, w, h - 48);
            for (int i = 0; i < h - 48; i += 24) {
                drawMenuFontById(g, FRAME_MIDDLE, x, (y + 24) + i);
                drawMenuFontById(g, FRAME_MIDDLE, x + w, (y + 24) + i, 2);
            }
        }
        MyAPI.setClip(g, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
        drawMenuFontById(g, FRAME_UP, x, y);
        drawMenuFontById(g, FRAME_UP, x + w, y, 2);
    }

    public int comfirmLogic() {
        Key.touchConfirmInit();
        if (Key.press(Key.gSelect | Key.B_S1)) {
            Key.touchConfirmClose();
            return this.cursor;
        } else if (Key.touchConfirmYes.Isin() && this.cursor == 1) {
            this.cursor = 0;
            Key.touchConfirmYes.reset();
            return -1;
        } else if (Key.touchConfirmNo.Isin() && this.cursor == 0) {
            this.cursor = 1;
            Key.touchConfirmNo.reset();
            return -1;
        } else if ((Key.touchConfirmYes.Isin() && this.cursor == 0) || (Key.touchConfirmNo.Isin() && this.cursor == 1)) {
            Key.touchConfirmClose();
            return this.cursor;
        } else {
            if (Key.press(2)) {
            }
            if (Key.press(524288)) {
                Key.touchConfirmClose();
                return 400;
            }
            if (Key.press(Key.gLeft)) {
                this.cursor--;
                this.cursor += 2;
                this.cursor %= 2;
            } else if (Key.press(Key.gRight)) {
                this.cursor++;
                this.cursor += 2;
                this.cursor %= 2;
            }
            return -1;
        }
    }

    public void comfirmDraw(MFGraphics g, int id) {
        drawMenuFontById(g, 100, COMFIRM_X, COMFIRM_Y);
        drawMenuFontById(g, id, COMFIRM_X, COMFIRM_Y - (MENU_SPACE >> 1));
        drawMenuFontById(g, 101, ((this.cursor * 40) + COMFIRM_X) - 20, COMFIRM_Y + (MENU_SPACE >> 1));
        drawMenuFontById(g, 10, COMFIRM_X, COMFIRM_Y + (MENU_SPACE >> 1));
    }

    public void confirmDraw(MFGraphics g, String title) {
        drawMenuFontById(g, 100, COMFIRM_X, COMFIRM_Y);
        MyAPI.drawBoldString(g, title, COMFIRM_X, ((COMFIRM_Y - CONFIRM_FRAME_OFFSET_Y) + 10) + LINE_SPACE, 17, 16777215, 4656650);
        drawMenuFontById(g, 101, ((this.cursor * 40) + COMFIRM_X) - 20, COMFIRM_Y + (MENU_SPACE >> 1));
        drawMenuFontById(g, 10, COMFIRM_X, COMFIRM_Y + (MENU_SPACE >> 1));
    }

    public void menuBgDraw(MFGraphics g) {
    }

    public void drawLowQualifyBackGround(MFGraphics g, int color1, int color2, int offset) {
        g.setColor(color1);
        MyAPI.fillRect(g, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
        g.setColor(color2);
        this.titleBgOffsetX += MENU_BG_SPEED;
        this.titleBgOffsetY += MENU_BG_SPEED;
        this.titleBgOffsetX %= offset;
        this.titleBgOffsetY %= offset;
        int x = (-offset) + this.titleBgOffsetX;
        while (x < SCREEN_WIDTH) {
            int y = (-offset) - this.titleBgOffsetY;
            while (y < SCREEN_HEIGHT) {
                MyAPI.fillRect(g, x, y, offset >> 1, offset >> 1);
                MyAPI.fillRect(g, (offset >> 1) + x, (offset >> 1) + y, offset >> 1, offset >> 1);
                y += offset;
            }
            x += offset;
        }
    }

    public void drawMenuTitle(MFGraphics g, int id, int y) {
        drawScrollFont(g, id, MENU_TITLE_DRAW_OFFSET_Y, MENU_TITLE_MOVE_DIRECTION);
    }

    public void drawMenuTitle(MFGraphics g, int id, int y, int width) {
        drawScrollFont(g, id, MENU_TITLE_DRAW_OFFSET_Y, width > 0 ? width : MENU_TITLE_MOVE_DIRECTION);
    }

    public void drawScrollFont(MFGraphics g, int id, int y, int width) {
        drawBar(g, 0, y);
        this.selectMenuOffsetX += 8;
        this.selectMenuOffsetX %= width;
        int x = 0;
        while (x - this.selectMenuOffsetX > 0) {
            x -= width;
        }
        for (int i = 0; i < MENU_TITLE_DRAW_NUM; i++) {
            drawMenuFontById(g, id, (x + (i * width)) - this.selectMenuOffsetX, y);
        }
    }

    public void helpInit() {
        helpStrings = MyAPI.loadText("/help");
        this.helpIndex = 0;
        MyAPI.initString();
        strForShow = MyAPI.getStrings(helpStrings[this.helpIndex], 11, SCREEN_WIDTH);
        helpTitleString = MyAPI.getStringToDraw(strForShow[0]);
        muiLeftArrowDrawer = new Animation("/animation/mui").getDrawer(91, true, 0);
        muiRightArrowDrawer = new Animation("/animation/mui").getDrawer(92, true, 0);
        muiUpArrowDrawer = new Animation("/animation/mui").getDrawer(93, true, 0);
        muiDownArrowDrawer = new Animation("/animation/mui").getDrawer(94, true, 0);
        Key.touchInstructionInit();
        this.arrowframecnt = 0;
        this.isArrowClicked = false;
        PageFrameCnt = 0;
        if (this.textBGImage == null) {
            this.textBGImage = MFImage.createImage("/animation/text_bg.png");
        }
        this.arrowindex = -1;
        this.returnPageCursor = 0;
    }

    public void helpLogic() {
        if (Key.touchhelpleftarrow.Isin() && Key.touchpage.IsClick()) {
            this.arrowindex = 0;
        } else if (Key.touchhelprightarrow.Isin() && Key.touchpage.IsClick()) {
            this.arrowindex = 1;
        }
        if (Key.touchhelpreturn.Isin() && Key.touchpage.IsClick()) {
            this.returnPageCursor = 1;
        }
        if (Key.slidesensorhelp.isSliding()) {
            if (Key.slidesensorhelp.isSlide(Key.DIR_UP)) {
                MyAPI.logicString(true, false);
            } else if (Key.slidesensorhelp.isSlide(Key.DIR_DOWN)) {
                MyAPI.logicString(false, true);
            }
        }
        if (Key.touchhelpuparrow.Isin()) {
            this.arrowframecnt++;
            if (this.arrowframecnt <= 4 && !this.isArrowClicked) {
                MyAPI.logicString(false, true);
                this.isArrowClicked = true;
            } else if (this.arrowframecnt > 4 && this.arrowframecnt % 2 == 0) {
                MyAPI.logicString(false, true);
            }
        } else if (Key.touchhelpdownarrow.Isin()) {
            this.arrowframecnt++;
            if (this.arrowframecnt <= 4 && !this.isArrowClicked) {
                MyAPI.logicString(true, false);
                this.isArrowClicked = true;
            } else if (this.arrowframecnt > 4 && this.arrowframecnt % 2 == 0) {
                MyAPI.logicString(true, false);
            }
        } else {
            this.arrowframecnt = 0;
            this.isArrowClicked = false;
        }
        if (Key.touchhelpleftarrow.IsButtonPress() && this.arrowindex == 0) {
            SoundSystem.getInstance().playSe(3);
            this.helpIndex--;
            this.helpIndex += helpStrings.length;
            this.helpIndex %= helpStrings.length;
            MyAPI.initString();
            strForShow = MyAPI.getStrings(helpStrings[this.helpIndex], 11, SCREEN_WIDTH);
        } else if (Key.touchhelprightarrow.IsButtonPress() && this.arrowindex == 1) {
            SoundSystem.getInstance().playSe(3);
            this.helpIndex++;
            this.helpIndex %= helpStrings.length;
            MyAPI.initString();
            strForShow = MyAPI.getStrings(helpStrings[this.helpIndex], 11, SCREEN_WIDTH);
        }
    }

    public void helpDraw(MFGraphics g) {
        drawFade(g);
        if (muiAniDrawer == null) {
            muiAniDrawer = new Animation("/animation/mui").getDrawer(0, false, 0);
            return;
        }
        muiAniDrawer.setActionId(62);
        PageFrameCnt++;
        PageFrameCnt %= 11;
        if (PageFrameCnt % 2 == 0) {
            PageBackGroundOffsetX++;
            PageBackGroundOffsetX %= 64;
            PageBackGroundOffsetY--;
            PageBackGroundOffsetY %= 56;
        }
        for (int x = PageBackGroundOffsetX - 64; x < (SCREEN_WIDTH * 3) / 2; x += 64) {
            for (int y = PageBackGroundOffsetY - 56; y < (SCREEN_HEIGHT * 3) / 2; y += 56) {
                muiAniDrawer.draw(g, x, y);
            }
        }
        for (int i = 0; i < 8; i++) {
            MyAPI.drawImage(g, this.textBGImage, ((SCREEN_WIDTH >> 1) - 104) + (i * 26), (SCREEN_HEIGHT >> 1) - 72, 0);
        }
        MyAPI.drawStrings(g, strForShow, ((SCREEN_WIDTH >> 1) - 104) + 10, ((SCREEN_HEIGHT >> 1) - 72) + 5, SCREEN_WIDTH, 131, 0, true, 16777215, 4656650, 0, 11);
        muiLeftArrowDrawer.draw(g, 0, (SCREEN_HEIGHT >> 1) - 4);
        muiRightArrowDrawer.draw(g, SCREEN_WIDTH, (SCREEN_HEIGHT >> 1) - 4);
        if (MyAPI.upPermit) {
            muiUpArrowDrawer.draw(g, (SCREEN_WIDTH >> 1) - 25, SCREEN_HEIGHT);
        }
        if (MyAPI.downPermit) {
            muiDownArrowDrawer.draw(g, (SCREEN_WIDTH >> 1) + 24, SCREEN_HEIGHT);
        }
        muiAniDrawer.setActionId(this.helpIndex + 95);
        muiAniDrawer.draw(g, SCREEN_WIDTH, SCREEN_HEIGHT);
        muiAniDrawer.setActionId((Key.touchhelpreturn.Isin() ? 5 : 0) + 61);
        muiAniDrawer.draw(g, 0, SCREEN_HEIGHT);
    }

    public void moregameDraw(MFGraphics g) {
        menuBgDraw(g);
        drawMenuTitle(g, 3, 0);
        fillMenuRect(g, this.MORE_GAME_START_X, this.MORE_GAME_START_Y, this.MORE_GAME_WIDTH, this.MORE_GAME_HEIGHT);
        MFGraphics mFGraphics = g;
        MyAPI.drawBoldString(mFGraphics, "是否退出游戏", SCREEN_WIDTH >> 1, this.MORE_GAME_START_Y + 10, 17, 16777215, 0);
        int i = MENU_SPACE + (this.MORE_GAME_START_Y + 10);
        mFGraphics = g;
        MyAPI.drawBoldString(mFGraphics, "并连接网络", SCREEN_WIDTH >> 1, i, 17, 16777215, 0);
        drawMenuFontById(g, 101, ((this.cursor * 40) + COMFIRM_X) - 20, ((this.MORE_GAME_START_Y + 10) + (MENU_SPACE * 3)) + FONT_H_HALF);
        drawMenuFontById(g, 10, COMFIRM_X, ((this.MORE_GAME_START_Y + 10) + (MENU_SPACE * 3)) + FONT_H_HALF);
    }

    public void pauseoptionLogic() {
        if (Key.press(Key.gSelect)) {
            this.pauseoptionCursor++;
            this.pauseoptionCursor += 4;
            this.pauseoptionCursor %= 4;
        }
    }

    public void pauseoptionDraw(MFGraphics g) {
        drawMenuTitle(g, 5, 0);
        drawMenuFontById(g, 54, (SCREEN_WIDTH >> 1) - 32, SCREEN_HEIGHT >> 1);
        drawMenuFontById(g, StringIndex.STR_RIGHT_ARROW, (SCREEN_WIDTH >> 1) - 76, SCREEN_HEIGHT >> 1);
        drawMenuFontById(g, OPTION_SOUND[this.pauseoptionCursor] + 55, (SCREEN_WIDTH >> 1) + 32, SCREEN_HEIGHT >> 1);
    }

    public void menuInit(int[] menuElement) {
        this.currentElement = menuElement;
        this.cursor = 0;
        this.mainMenuItemCursor = 0;
        this.menuMoving = true;
        this.elementNum = this.currentElement.length;
        this.selectMenuOffsetX = 0;
        this.cursor = this.returnCursor;
        this.mainMenuItemCursor = this.returnCursor;
    }

    public void menuInit(int nElementNum) {
        this.cursor = 0;
        this.mainMenuItemCursor = 0;
        this.menuMoving = false;
        this.elementNum = nElementNum;
        this.selectMenuOffsetX = 0;
        this.cursor = this.returnCursor;
        this.mainMenuItemCursor = this.returnCursor;
    }

    public void menuInit(int nElementNum, int cursorIndex) {
        menuInit(nElementNum);
        this.cursor = cursorIndex;
        this.mainMenuItemCursor = cursorIndex;
    }

    public int getMenuPosY(int i) {
        return ((SCREEN_HEIGHT - ((this.elementNum - 1) * MENU_SPACE)) >> 1) + (MENU_SPACE * i);
    }

    public int getMenuPosY(int i, int num) {
        return ((SCREEN_HEIGHT - ((num - 1) * MENU_SPACE)) >> 1) + (MENU_SPACE * i);
    }

    public int menuLogic() {
        Key.touchMenuInit();
        if (Key.touchmenunew.Isin() && this.cursor == 1) {
            this.cursor = 0;
            Key.touchmenunew.reset();
            return -1;
        } else if (Key.touchmenucon.Isin() && this.cursor == 0) {
            this.cursor = 1;
            Key.touchmenucon.reset();
            return -1;
        } else if ((Key.touchmenunew.Isin() && this.cursor == 0) || (Key.touchmenucon.Isin() && this.cursor == 1)) {
            return this.cursor;
        } else {
            if (this.menuMoving) {
                return -1;
            }
            if (Key.press(Key.gUp)) {
                this.cursor--;
                this.cursor = (this.cursor + this.elementNum) % this.elementNum;
                this.selectMenuOffsetX = 0;
                changeUpSelect();
            } else if (Key.press(Key.gDown)) {
                this.cursor++;
                this.cursor = (this.cursor + this.elementNum) % this.elementNum;
                this.selectMenuOffsetX = 0;
                changeDownSelect();
            } else if (Key.press(Key.gSelect | Key.B_S1)) {
                this.cursor = (this.cursor + this.elementNum) % this.elementNum;
                Key.touchMenuClose();
                return this.cursor;
            } else {
                if (Key.press(2)) {
                }
                if (Key.press(524288)) {
                    Key.touchMenuClose();
                    return 400;
                }
            }
            return -1;
        }
    }

    public void changeUpSelect() {
    }

    public void changeDownSelect() {
    }

    public static void initTouchkeyBoard() {
        if (touchkeyboardAnimation == null) {
            touchkeyboardAnimation = new Animation("/tuch/control_panel");
        }
        touchkeyboardDrawer = touchkeyboardAnimation.getDrawer(0, true, 0);
        touchgamekeyboardDrawer = touchkeyboardAnimation.getDrawer(0, true, 0);
    }

    public static void releaseTouchkeyBoard() {
        Animation.closeAnimation(touchkeyboardAnimation);
        touchkeyboardAnimation = null;
        Animation.closeAnimationDrawer(touchkeyboardDrawer);
        touchkeyboardDrawer = null;
        Animation.closeAnimation(touchgamekeyboardAnimation);
        touchgamekeyboardAnimation = null;
        Animation.closeAnimationDrawer(touchgamekeyboardDrawer);
        touchgamekeyboardDrawer = null;
        touchgamekeyImage = null;
    }

    public static void drawTouchGameKey(MFGraphics g) {
    }

    public static void drawTouchKeyBoardById(MFGraphics g, int id, int x, int y) {
        touchkeyboardDrawer.setActionId(id);
        touchkeyboardDrawer.draw(g, x, y);
    }

    public static void drawTouchGameKeyBoardById(MFGraphics g, int id, int x, int y) {
        touchgamekeyboardDrawer.setActionId(id);
        touchgamekeyboardDrawer.draw(g, x, y);
    }

    public void drawArcLine(MFGraphics g, int centerx, int centery, int pointx, int pointy) {
        g.setColor(16711680);
        MyAPI.drawLine(g, centerx, centery, pointx, pointy);
    }

    private int transTouchPointX(TouchDirectKey directkey, int pointx, int pointy) {
        int reset_x = pointx - 32;
        int reset_y = pointy - 128;
        if ((reset_x * reset_x) + (reset_y * reset_y) > 256) {
            return ((MyAPI.dCos(directkey.getDegree()) * 16) / 100) + 32;
        }
        return pointx;
    }

    private int transTouchPointY(TouchDirectKey directkey, int pointx, int pointy) {
        int reset_x = pointx - 32;
        int reset_y = pointy - 128;
        if ((reset_x * reset_x) + (reset_y * reset_y) > 256) {
            return ((MyAPI.dSin(directkey.getDegree()) * 16) / 100) + 128;
        }
        return pointy;
    }

    private void drawTouchKeyDirectPad(MFGraphics g) {
        int degree = 0;
        if (Key.touchdirectgamekey != null) {
            degree = Key.touchdirectgamekey.getDegree();
        }
        int id = 0;
        if (degree >= 248 && degree < 292) {
            id = 1;
        } else if (degree >= 292 && degree < 337) {
            id = 2;
        } else if ((degree >= 0 && degree < 22) || (degree >= 337 && degree <= MDPhone.SCREEN_WIDTH)) {
            id = 3;
        } else if (degree >= 22 && degree < 67) {
            id = 4;
        } else if (degree >= 67 && degree < StringIndex.STR_RIGHT_ARROW) {
            id = 5;
        } else if (degree >= StringIndex.STR_RIGHT_ARROW && degree < 157) {
            id = 6;
        } else if (degree >= 157 && degree < 202) {
            id = 7;
        } else if (degree >= 202 && degree < 248) {
            id = 8;
        }
        drawTouchGameKeyBoardById(g, id, 32, 128);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void drawTouchKeyDirect(com.sega.mobile.framework.device.MFGraphics r7) {

        //if () {

        //}
        drawTouchGameKeyBoardById(r7, 0, 32, 128);
        drawTouchGameKeyBoardById(r7, 10, SCREEN_WIDTH - 22, 128);
        drawTouchGameKeyBoardById(r7, 12, SCREEN_WIDTH - 67, 128);

       // drawTouchKeyDirectPad(r7);
     //   drawTouchGameKeyBoardById(r7, 9, SCREEN_WIDTH - 22, 128);
     //   drawTouchGameKeyBoardById(r7, 11, SCREEN_WIDTH - 67, 128);

        /*
        r6 = this;
        r5 = 16777216; // 0x1000000 float:2.3509887E-38 double:8.289046E-317;
        r4 = 138; // 0x8a float:1.93E-43 double:6.8E-322;
        r3 = 123; // 0x7b float:1.72E-43 double:6.1E-322;
        r0 = GameEngine.Key.touchdirectgamekey;
        r0 = GameEngine.TouchDirectKey.IsKeyReleased();
        if (r0 == 0) goto L_0x0045;
    L_0x000e:
        r0 = 0;
        r1 = 32;
        r2 = 128; // 0x80 float:1.794E-43 double:6.32E-322;
        drawTouchGameKeyBoardById(r7, r0, r1, r2);
    L_0x0016:
        r0 = GameEngine.Key.press(r5);
        if (r0 != 0) goto L_0x0022;
    L_0x001c:
        r0 = GameEngine.Key.repeat(r5);
        if (r0 == 0) goto L_0x0059;
    L_0x0022:
        r0 = 10;
        r1 = SCREEN_WIDTH;
        r1 = r1 + -22;
        drawTouchGameKeyBoardById(r7, r0, r1, r3);
    L_0x002b:
        r0 = GameEngine.Key.B_SEL;
        r0 = GameEngine.Key.press(r0);
        if (r0 != 0) goto L_0x003b;
    L_0x0033:
        r0 = GameEngine.Key.B_SEL;
        r0 = GameEngine.Key.repeat(r0);
        if (r0 == 0) goto L_0x0063;
    L_0x003b:
        r0 = 12;
        r1 = SCREEN_WIDTH;
        r1 = r1 + -67;
        drawTouchGameKeyBoardById(r7, r0, r1, r4);
    L_0x0044:
        return;
    L_0x0045:
        r0 = GameEngine.Key.touchdirectgamekey;
        r0 = GameEngine.TouchDirectKey.IsKeyDragged();
        if (r0 != 0) goto L_0x0055;
    L_0x004d:
        r0 = GameEngine.Key.touchdirectgamekey;
        r0 = GameEngine.TouchDirectKey.IsKeyPressed();
        if (r0 == 0) goto L_0x0016;
    L_0x0055:
        r6.drawTouchKeyDirectPad(r7);
        goto L_0x0016;
    L_0x0059:
        r0 = 9;
        r1 = SCREEN_WIDTH;
        r1 = r1 + -22;
        drawTouchGameKeyBoardById(r7, r0, r1, r3);
        goto L_0x002b;
    L_0x0063:
        r0 = 11;
        r1 = SCREEN_WIDTH;
        r1 = r1 + -67;
        drawTouchGameKeyBoardById(r7, r0, r1, r4);
        goto L_0x0044;
        */
//        throw new UnsupportedOperationException("Method not decompiled: State.State.drawTouchKeyDirect(com.sega.mobile.framework.device.MFGraphics):void");
        System.out.println("Method not decompiled: State.State.drawTouchKeyDirect(com.sega.mobile.framework.device.MFGraphics):void");
    }

    public static void setTry() {
        if (trytimes > 0) {
            trytimes--;
        } else {
            trytimes = 0;
        }
    }

    public static boolean IsActiveGame() {
        return false;
    }

    public static boolean IsToolsCharge() {
        return false;
    }

    public static void activeGameProcess(boolean state) {
        IsPaid = state;
    }

    public static void load_bp_string() {
        setMenu();
    }

    public static void setMenu() {
        TitleState.setMainMenu();
        GameState.setPauseMenu();
    }

    public static void init_bp() {
    }

    public static void loadBPRecord() {
    }

    public static void saveBPRecord() {
    }

    public static boolean BP_chargeLogic(int index) {
        return false;
    }

    public static boolean BP_IsChargeByIndex(int index) {
        return false;
    }

    public void BP_payingInit(int text_id, int title_id) {
        MyAPI.initString();
        strForShow = MyAPI.getStrings(BPstrings[text_id], MENU_RECT_WIDTH - 20);
        BPPayingTitle = BPstrings[title_id];
        this.IsInBP = true;
    }

    public void BP_payingDraw(MFGraphics g) {
        menuBgDraw(g);
    }

    public static void BP_loadImage() {
    }

    public void secondEnsureInit() {
        this.isConfirm = false;
        this.confirmframe = 0;
        if (muiAniDrawer == null) {
            muiAniDrawer = new Animation("/animation/mui").getDrawer(0, false, 0);
        }
        Key.touchSecondEnsureClose();
        Key.touchSecondEnsureInit();
    }

    public void secondEnsureInit2() {
        this.isConfirm = false;
        this.confirmframe = 0;
        if (muiAniDrawer == null) {
            muiAniDrawer = new Animation("/animation/mui").getDrawer(0, false, 0);
        }
        Key.touchSecondEnsureClose();
        Key.touchSecondEnsureInit();
    }

    public int secondEnsureLogic() {
        if (Key.touchsecondensurereturn.Isin() && Key.touchsecondensure.IsClick()) {
            this.confirmcursor = 2;
        }
        if (Key.touchsecondensureyes.Isin() && Key.touchsecondensure.IsClick()) {
            this.confirmcursor = 0;
        }
        if (Key.touchsecondensureno.Isin() && Key.touchsecondensure.IsClick()) {
            this.confirmcursor = 1;
        }
        if (this.isConfirm) {
            this.confirmframe++;
            if (this.confirmframe > 8) {
                return 1;
            }
        }
        if (Key.touchsecondensureyes.IsButtonPress() && this.confirmcursor == 0 && !this.isConfirm) {
            this.isConfirm = true;
            this.confirmframe = 0;
            SoundSystem.getInstance().playSe(1);
            return 0;
        } else if (Key.touchsecondensureno.IsButtonPress() && this.confirmcursor == 1 && !this.isConfirm) {
            SoundSystem.getInstance().playSe(2);
            return 2;
        } else if ((!Key.press(524288) && !Key.touchsecondensurereturn.IsButtonPress()) || !fadeChangeOver() || this.isConfirm) {
            return 0;
        } else {
            SoundSystem.getInstance().playSe(2);
            return 2;
        }
    }

    public int secondEnsureDirectLogic() {
        if (Key.touchsecondensurereturn.Isin() && Key.touchsecondensure.IsClick()) {
            this.confirmcursor = 2;
        }
        if (Key.touchsecondensureyes.Isin() && Key.touchsecondensure.IsClick()) {
            this.confirmcursor = 0;
        }
        if (Key.touchsecondensureno.Isin() && Key.touchsecondensure.IsClick()) {
            this.confirmcursor = 1;
        }
        if (Key.touchsecondensureyes.IsButtonPress() && this.confirmcursor == 0) {
            this.isConfirm = true;
            this.confirmframe = 0;
            SoundSystem.getInstance().playSe(1);
            return 1;
        } else if (Key.touchsecondensureno.IsButtonPress() && this.confirmcursor == 1) {
            SoundSystem.getInstance().playSe(2);
            return 2;
        } else if ((!Key.press(524288) && !Key.touchsecondensurereturn.IsButtonPress()) || !fadeChangeOver()) {
            return 0;
        } else {
            SoundSystem.getInstance().playSe(2);
            return 2;
        }
    }

    public void SecondEnsurePanelDraw(MFGraphics g, int ani_id) {
        if (muiAniDrawer == null) {
            muiAniDrawer = new Animation("/animation/mui").getDrawer(0, false, 0);
            return;
        }
        muiAniDrawer.setActionId(54);
        muiAniDrawer.draw(g, SCREEN_WIDTH >> 1, (SCREEN_HEIGHT >> 1) - 20);
        AnimationDrawer animationDrawer = muiAniDrawer;
        int i = (Key.touchsecondensureyes.Isin() && this.confirmcursor == 0) ? 1 : 0;
        animationDrawer.setActionId(i + 59);
        muiAniDrawer.draw(g, (SCREEN_WIDTH >> 1) - 40, (SCREEN_HEIGHT >> 1) + 40);
        animationDrawer = muiAniDrawer;
        if (Key.touchsecondensureno.Isin() && this.confirmcursor == 1) {
            i = 1;
        } else {
            i = 0;
        }
        animationDrawer.setActionId(i + 59);
        muiAniDrawer.draw(g, (SCREEN_WIDTH >> 1) + 40, (SCREEN_HEIGHT >> 1) + 40);
        muiAniDrawer.setActionId(46);
        muiAniDrawer.draw(g, (SCREEN_WIDTH >> 1) - 40, (SCREEN_HEIGHT >> 1) + 40);
        muiAniDrawer.setActionId(47);
        muiAniDrawer.draw(g, (SCREEN_WIDTH >> 1) + 40, (SCREEN_HEIGHT >> 1) + 40);
        muiAniDrawer.setActionId(ani_id);
        muiAniDrawer.draw(g, SCREEN_WIDTH >> 1, (SCREEN_HEIGHT >> 1) - 20);
        animationDrawer = muiAniDrawer;
        if (Key.touchsecondensurereturn.Isin()) {
            i = 5;
        } else {
            i = 0;
        }
        animationDrawer.setActionId(i + 61);
        muiAniDrawer.draw(g, 0, SCREEN_HEIGHT);
        if (this.isConfirm && this.confirmframe > 8) {
            g.setColor(0);
            MyAPI.fillRect(g, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
        }
    }

    public void itemsSelect2Init() {
        Key.touchItemsSelect2Init();
        this.itemsselectcursor = 0;
        this.isItemsSelect = false;
        fadeInit(0, 192);
    }

    public int itemsSelect2Logic() {
        if (Key.touchitemsselect2_return.Isin() && Key.touchitemsselect2.IsClick()) {
            this.itemsselectcursor = 2;
        }
        if (Key.touchitemsselect2_1.Isin() && Key.touchitemsselect2.IsClick()) {
            this.itemsselectcursor = 0;
        }
        if (Key.touchitemsselect2_2.Isin() && Key.touchitemsselect2.IsClick()) {
            this.itemsselectcursor = 1;
        }
        if (this.isItemsSelect) {
            this.itemsselectframe++;
            if (this.itemsselectframe > 8) {
                fadeInit(220, 102);
                if (this.finalitemsselectcursor == 0) {
                    return 1;
                }
                return 2;
            }
        }
        if (Key.touchitemsselect2_1.IsButtonPress() && this.itemsselectcursor == 0 && !this.isItemsSelect) {
            this.isItemsSelect = true;
            this.itemsselectframe = 0;
            this.finalitemsselectcursor = 0;
            SoundSystem.getInstance().playSe(1);
            return 0;
        } else if (Key.touchitemsselect2_2.IsButtonPress() && this.itemsselectcursor == 1 && !this.isItemsSelect) {
            this.isItemsSelect = true;
            this.itemsselectframe = 0;
            this.finalitemsselectcursor = 1;
            SoundSystem.getInstance().playSe(1);
            return 0;
        } else if ((!Key.press(524288) && !Key.touchitemsselect2_return.IsButtonPress()) || !fadeChangeOver()) {
            return 0;
        } else {
            SoundSystem.getInstance().playSe(2);
            return 3;
        }
    }

    public void itemsSelect2Draw(MFGraphics g, int items1, int items2) {
        if (muiAniDrawer == null) {
            muiAniDrawer = new Animation("/animation/mui").getDrawer(0, false, 0);
            return;
        }
        AnimationDrawer animationDrawer = muiAniDrawer;
        int i = (Key.touchitemsselect2_1.Isin() && this.itemsselectcursor == 0) ? 1 : 0;
        animationDrawer.setActionId(i + 55);
        muiAniDrawer.draw(g, SCREEN_WIDTH >> 1, (SCREEN_HEIGHT >> 1) - 18);
        animationDrawer = muiAniDrawer;
        if (Key.touchitemsselect2_2.Isin() && this.itemsselectcursor == 1) {
            i = 1;
        } else {
            i = 0;
        }
        animationDrawer.setActionId(i + 55);
        muiAniDrawer.draw(g, SCREEN_WIDTH >> 1, (SCREEN_HEIGHT >> 1) + 18);
        muiAniDrawer.setActionId(items1);
        muiAniDrawer.draw(g, SCREEN_WIDTH >> 1, (SCREEN_HEIGHT >> 1) - 18);
        muiAniDrawer.setActionId(items2);
        muiAniDrawer.draw(g, SCREEN_WIDTH >> 1, (SCREEN_HEIGHT >> 1) + 18);
        animationDrawer = muiAniDrawer;
        if (Key.touchitemsselect2_return.Isin()) {
            i = 5;
        } else {
            i = 0;
        }
        animationDrawer.setActionId(i + 61);
        muiAniDrawer.draw(g, 0, SCREEN_HEIGHT);
    }

    public void soundVolumnInit() {
        Key.touchSecondEnsureClose();
        Key.touchSecondEnsureInit();
        this.timeCnt = 0;
        this.isSoundVolumnClick = false;
        fadeInit(0, 192);
        if (muiAniDrawer == null) {
            muiAniDrawer = new Animation("/animation/mui").getDrawer(0, false, 0);
        }
    }

    public int soundVolumnLogic() {
        if (Key.touchsecondensurereturn.Isin() && Key.touchsecondensure.IsClick()) {
            this.confirmcursor = 2;
        }
        if (Key.touchsecondensureyes.Isin() && Key.touchsecondensure.IsClick()) {
            this.confirmcursor = 0;
        }
        if (Key.touchsecondensureno.Isin() && Key.touchsecondensure.IsClick()) {
            this.confirmcursor = 1;
        }
        if (Key.touchsecondensureyes.Isin()) {
            this.timeCnt++;
            if (this.timeCnt <= 4 && !this.isSoundVolumnClick) {
                if (GlobalResource.soundConfig <= 0) {
                    GlobalResource.soundConfig = 0;
                } else {
                    GlobalResource.soundConfig--;
                }
                if (GlobalResource.soundConfig == 0) {
                    GlobalResource.seConfig = 0;
                }
                SoundSystem.getInstance().setSoundState(GlobalResource.soundConfig);
                SoundSystem.getInstance().setSeState(GlobalResource.seConfig);
                this.isSoundVolumnClick = true;
            } else if (this.timeCnt > 4 && this.timeCnt % 2 == 0) {
                if (GlobalResource.soundConfig <= 0) {
                    GlobalResource.soundConfig = 0;
                } else {
                    GlobalResource.soundConfig--;
                }
                if (GlobalResource.soundConfig == 0) {
                    GlobalResource.seConfig = 0;
                }
                SoundSystem.getInstance().setSoundState(GlobalResource.soundConfig);
                SoundSystem.getInstance().setSeState(GlobalResource.seConfig);
            }
        } else if (Key.touchsecondensureno.Isin()) {
            this.timeCnt++;
            if (this.timeCnt <= 4 && !this.isSoundVolumnClick) {
                if (GlobalResource.soundConfig >= 15) {
                    GlobalResource.soundConfig = 15;
                } else {
                    GlobalResource.soundConfig++;
                }
                if (GlobalResource.soundConfig > 0) {
                    GlobalResource.seConfig = 1;
                }
                this.isSoundVolumnClick = true;
                SoundSystem.getInstance().setSoundState(GlobalResource.soundConfig);
                SoundSystem.getInstance().setSeState(GlobalResource.seConfig);
                if (GlobalResource.soundConfig == 1) {
                    SoundSystem.getInstance().resumeBgm();
                }
            } else if (this.timeCnt > 4 && this.timeCnt % 2 == 0) {
                if (GlobalResource.soundConfig >= 15) {
                    GlobalResource.soundConfig = 15;
                } else {
                    GlobalResource.soundConfig++;
                }
                if (GlobalResource.soundConfig > 0) {
                    GlobalResource.seConfig = 1;
                }
                SoundSystem.getInstance().setSoundState(GlobalResource.soundConfig);
                SoundSystem.getInstance().setSeState(GlobalResource.seConfig);
                if (GlobalResource.soundConfig == 1) {
                    SoundSystem.getInstance().resumeBgm();
                }
            }
        } else {
            this.timeCnt = 0;
            this.isSoundVolumnClick = false;
        }
        if (GlobalResource.soundConfig >= 15) {
            GlobalResource.soundConfig = 15;
        } else if (GlobalResource.soundConfig <= 0) {
            GlobalResource.soundConfig = 0;
        }
        if (GlobalResource.soundConfig > 0) {
            GlobalResource.seConfig = 1;
        }
        if ((!Key.press(524288) && !Key.touchsecondensurereturn.IsButtonPress()) || !fadeChangeOver()) {
            return 0;
        }
        SoundSystem.getInstance().playSe(2);
        return 2;
    }

    public void soundVolumnDraw(MFGraphics g) {
        if (muiAniDrawer == null) {
            muiAniDrawer = new Animation("/animation/mui").getDrawer(0, false, 0);
            return;
        }
        int i;
        muiAniDrawer.setActionId(54);
        muiAniDrawer.draw(g, SCREEN_WIDTH >> 1, (SCREEN_HEIGHT >> 1) - 20);
        AnimationDrawer animationDrawer = muiAniDrawer;
        if (Key.touchsecondensureyes.Isin() && this.confirmcursor == 0) {
            i = 1;
        } else {
            i = 0;
        }
        animationDrawer.setActionId(i + 59);
        muiAniDrawer.draw(g, (SCREEN_WIDTH >> 1) - 40, (SCREEN_HEIGHT >> 1) + 40);
        animationDrawer = muiAniDrawer;
        if (Key.touchsecondensureno.Isin() && this.confirmcursor == 1) {
            i = 1;
        } else {
            i = 0;
        }
        animationDrawer.setActionId(i + 59);
        muiAniDrawer.draw(g, (SCREEN_WIDTH >> 1) + 40, (SCREEN_HEIGHT >> 1) + 40);
        muiAniDrawer.setActionId(89);
        muiAniDrawer.draw(g, (SCREEN_WIDTH >> 1) - 40, (SCREEN_HEIGHT >> 1) + 40);
        muiAniDrawer.setActionId(90);
        muiAniDrawer.draw(g, (SCREEN_WIDTH >> 1) + 40, (SCREEN_HEIGHT >> 1) + 40);
        muiAniDrawer.setActionId(71);
        muiAniDrawer.draw(g, SCREEN_WIDTH >> 1, (SCREEN_HEIGHT >> 1) - 20);
        muiAniDrawer.setActionId(72);
        for (int i2 = 1; i2 <= GlobalResource.soundConfig; i2++) {
            muiAniDrawer.draw(g, ((SCREEN_WIDTH >> 1) - 56) + ((i2 - 1) * 8), (SCREEN_HEIGHT >> 1) - 20);
        }
        muiAniDrawer.setActionId(GlobalResource.soundConfig + 73);
        muiAniDrawer.draw(g, SCREEN_WIDTH >> 1, SCREEN_HEIGHT >> 1);
        animationDrawer = muiAniDrawer;
        if (Key.touchsecondensurereturn.Isin()) {
            i = 5;
        } else {
            i = 0;
        }
        animationDrawer.setActionId(i + 61);
        muiAniDrawer.draw(g, 0, SCREEN_HEIGHT);
    }

    public void spSenorSetInit() {
        Key.touchItemsSelect3Init();
        this.itemsselectcursor = 0;
        this.isItemsSelect = false;
        fadeInit(0, 102);
        if (muiAniDrawer == null) {
            muiAniDrawer = new Animation("/animation/mui").getDrawer(0, false, 0);
        }
    }

    public int spSenorSetLogic() {
        if (Key.touchitemsselect3_return.Isin() && Key.touchitemsselect3.IsClick()) {
            this.itemsselectcursor = 2;
        }
        if (Key.touchitemsselect3_1.Isin() && Key.touchitemsselect3.IsClick()) {
            this.itemsselectcursor = 0;
        }
        if (Key.touchitemsselect3_2.Isin() && Key.touchitemsselect3.IsClick()) {
            this.itemsselectcursor = 1;
        }
        if (Key.touchitemsselect3_3.Isin() && Key.touchitemsselect3.IsClick()) {
            this.itemsselectcursor = 4;
        }
        if (this.isItemsSelect) {
            this.itemsselectframe++;
            if (this.itemsselectframe > 8) {
                fadeInit(220, 102);
                if (this.finalitemsselectcursor == 0) {
                    return 1;
                }
                if (this.finalitemsselectcursor == 1) {
                    return 2;
                }
                return 4;
            }
        }
        if (Key.touchitemsselect3_1.IsButtonPress() && this.itemsselectcursor == 0 && !this.isItemsSelect) {
            this.isItemsSelect = true;
            this.itemsselectframe = 0;
            this.finalitemsselectcursor = 0;
            SoundSystem.getInstance().playSe(1);
            return 0;
        } else if (Key.touchitemsselect3_2.IsButtonPress() && this.itemsselectcursor == 1 && !this.isItemsSelect) {
            this.isItemsSelect = true;
            this.itemsselectframe = 0;
            this.finalitemsselectcursor = 1;
            SoundSystem.getInstance().playSe(1);
            return 0;
        } else if (Key.touchitemsselect3_3.IsButtonPress() && this.itemsselectcursor == 4 && !this.isItemsSelect) {
            this.isItemsSelect = true;
            this.itemsselectframe = 0;
            this.finalitemsselectcursor = 2;
            SoundSystem.getInstance().playSe(1);
            return 0;
        } else if ((!Key.press(524288) && !Key.touchitemsselect2_return.IsButtonPress()) || !fadeChangeOver()) {
            return 0;
        } else {
            SoundSystem.getInstance().playSe(2);
            return 3;
        }
    }

    public void spSenorSetDraw(MFGraphics g) {
        if (muiAniDrawer == null) {
            muiAniDrawer = new Animation("/animation/mui").getDrawer(0, false, 0);
            return;
        }
        AnimationDrawer animationDrawer = muiAniDrawer;
        int i = (Key.touchitemsselect3_1.Isin() && this.itemsselectcursor == 0) ? 1 : 0;
        animationDrawer.setActionId(i + 55);
        muiAniDrawer.draw(g, SCREEN_WIDTH >> 1, (SCREEN_HEIGHT >> 1) - 36);
        animationDrawer = muiAniDrawer;
        if (Key.touchitemsselect3_2.Isin() && this.itemsselectcursor == 1) {
            i = 1;
        } else {
            i = 0;
        }
        animationDrawer.setActionId(i + 55);
        muiAniDrawer.draw(g, SCREEN_WIDTH >> 1, ((SCREEN_HEIGHT >> 1) - 36) + 36);
        animationDrawer = muiAniDrawer;
        if (Key.touchitemsselect3_3.Isin() && this.itemsselectcursor == 4) {
            i = 1;
        } else {
            i = 0;
        }
        animationDrawer.setActionId(i + 55);
        muiAniDrawer.draw(g, SCREEN_WIDTH >> 1, ((SCREEN_HEIGHT >> 1) - 36) + 72);
        muiAniDrawer.setActionId(70);
        muiAniDrawer.draw(g, SCREEN_WIDTH >> 1, (SCREEN_HEIGHT >> 1) - 36);
        muiAniDrawer.setActionId(69);
        muiAniDrawer.draw(g, SCREEN_WIDTH >> 1, ((SCREEN_HEIGHT >> 1) - 36) + 36);
        muiAniDrawer.setActionId(68);
        muiAniDrawer.draw(g, SCREEN_WIDTH >> 1, ((SCREEN_HEIGHT >> 1) - 36) + 72);
        animationDrawer = muiAniDrawer;
        if (Key.touchitemsselect3_return.Isin()) {
            i = 5;
        } else {
            i = 0;
        }
        animationDrawer.setActionId(i + 61);
        muiAniDrawer.draw(g, 0, SCREEN_HEIGHT);
    }

    public static void setSoundVolumnDown() {
        if (GlobalResource.soundConfig <= 0) {
            GlobalResource.soundConfig = 0;
        } else {
            GlobalResource.soundConfig--;
        }
        if (GlobalResource.soundConfig == 0) {
            GlobalResource.seConfig = 0;
        }
        SoundSystem.getInstance().setVolumnState(GlobalResource.soundConfig);
        SoundSystem.getInstance().setSeState(GlobalResource.seConfig);
    }

    public static void setSoundVolumnUp() {
        if (GlobalResource.soundConfig >= 15) {
            GlobalResource.soundConfig = 15;
        } else {
            GlobalResource.soundConfig++;
        }
        if (GlobalResource.soundConfig == 1) {
            SoundSystem.getInstance().resumeBgm();
        }
        if (GlobalResource.soundConfig > 0) {
            GlobalResource.seConfig = 1;
        }
        SoundSystem.getInstance().setVolumnState(GlobalResource.soundConfig);
        SoundSystem.getInstance().setSeState(GlobalResource.seConfig);
    }

    public static void getMain(Main main) {
        activity = main;
    }

    public void setGameScore(int strScore) {
        activity.setScore(strScore);
    }

    public void sendMessage(Message msg, int id) {
        msg.what = id;
        activity.handler.handleMessage(msg);
    }
}
