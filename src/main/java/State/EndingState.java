package State;

import Ending.NormalEnding;
import Ending.SpecialEnding;
import Ending.SuperSonicEnding;
import SonicGBA.PlayerObject;
import com.sega.mobile.framework.device.MFGraphics;

public class EndingState extends State {
    private int endingState;
    private NormalEnding normalEndingInstance;
    private SpecialEnding specialEndingInstance;
    private SuperSonicEnding superSonicEndingInstance;

    public EndingState(int state) {
        this.endingState = state;
        switch (state) {
            case 0:
                PlayerObject.resetGameParam();
                this.normalEndingInstance = new NormalEnding();
                this.normalEndingInstance.init(0, PlayerObject.getCharacterID());
                return;
            case 1:
                PlayerObject.resetGameParam();
                this.superSonicEndingInstance = new SuperSonicEnding();
                return;
            case 2:
                this.specialEndingInstance = new SpecialEnding(3, 5);
                return;
            default:
                return;
        }
    }

    public void close() {
    }

    public void draw(MFGraphics g) {
        switch (this.endingState) {
            case 0:
                this.normalEndingInstance.draw(g);
                return;
            case 1:
                this.superSonicEndingInstance.draw(g);
                return;
            case 2:
                this.specialEndingInstance.draw(g);
                return;
            default:
                return;
        }
    }

    public void init() {
    }

    public void logic() {
        switch (this.endingState) {
            case 0:
                this.normalEndingInstance.logic();
                return;
            case 1:
                this.superSonicEndingInstance.logic();
                return;
            case 2:
                this.specialEndingInstance.logic();
                return;
            default:
                return;
        }
    }

    public void pause() {
        switch (this.endingState) {
            case 0:
                this.normalEndingInstance.pause();
                return;
            case 1:
                this.superSonicEndingInstance.pause();
                return;
            case 2:
                this.specialEndingInstance.pause();
                return;
            default:
                return;
        }
    }
}
