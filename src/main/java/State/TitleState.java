package State;

import GameEngine.Def;
import GameEngine.Key;
import GameEngine.TouchKeyRange;
import Lib.Animation;
import Lib.AnimationDrawer;
import Lib.MyAPI;
import Lib.SoundSystem;
import PlatformStandard.Standard;
import PlatformStandard.Standard2;
import SonicGBA.GameObject;
import SonicGBA.GimmickObject;
import SonicGBA.GlobalResource;
import SonicGBA.MapManager;
import SonicGBA.PlayerObject;
import SonicGBA.SonicDef;
import SonicGBA.StageManager;
import Special.SSDef;
import android.os.Message;
import com.sega.mobile.framework.device.MFDevice;
import com.sega.mobile.framework.device.MFGamePad;
import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;

public class TitleState extends State {
    private static final int ACTION_NUM_OFFSET = 49;
    private static final int ACTION_OFFSET = 26;
    private static final int ARRAW_OFFSET_Y = 16;
    private static final int BACK_LINE_SPACE_NORMAL = 96;
    private static final int BACK_LINE_SPACE_TIME = 96;
    private static final int BALL_INIT_Y = (SCREEN_HEIGHT >> 1);
    private static final int BALL_POSITION_X = 0;
    private static final int BALL_RUN_POSITION_X = -460;
    private static final int CHARACTER_MOVE_OFFSET = 8;
    private static final int CHARACTER_MOVE_OFFSET_SPEED_MAX = 32;
    private static final int CHARACTER_OFFSET_LEFT = 1;
    private static final int CHARACTER_OFFSET_NONE = 0;
    private static final int CHARACTER_OFFSET_RIGHT = 2;
    public static final int CHARACTER_RECORD_BG_HEIGHT = 48;
    public static final int CHARACTER_RECORD_BG_OFFSET = 128;
    public static final int CHARACTER_RECORD_BG_SPEED = 4;
    private static final int CHARACTER_SELECT_ARROW_LEFT = 1;
    private static final int CHARACTER_SELECT_ARROW_NONE = 0;
    private static final int CHARACTER_SELECT_ARROW_RIGHT = 2;
    private static final String[] CHARACTER_STR = new String[]{"索尼克", "塔尔斯", "那克鲁兹", "艾米"};
    private static final int COPY_RIGHT_X = -2;
    private static final int COPY_RIGHT_Y = 4;
    private static final int CREDIT_PAGE_BACKGROUND_WIDTH = 112;
    private static final int DEGREE_GAP = 18;
    private static final int DEGREE_START = -18;
    private static final int ELEMENT_OFFSET = -1;
    private static final int ELEMENT_V_OFFSET = 0;
    private static final int EN_TITLE_SPACE_FOR_STAGE_SELECT = 0;
    private static final int GESTURE_SLIDE_STATE_DOWN = 2;
    private static final int GESTURE_SLIDE_STATE_NONE = 0;
    private static final int GESTURE_SLIDE_STATE_UP = 1;
    private static final int INTERGRADE_RECORD_STAGE_NAME_SPEED = -8;
    private static final int INTERGRADE_RECORD_STAGE_NAME_WIDTH = 200;
    private static final int INTERVAL_ABOVE_RECORD_BAR = (MENU_SPACE >> 1);
    private static final int INTERVAL_FOR_RECORD_BAR = MENU_SPACE;
    private static final int ITEMS_INTERVALS = 20;
    private static final int ITEM_SPACE = 24;
    public static final int ITEM_X = ((SCREEN_WIDTH * 5) / 6);
    private static boolean IsSoundVolSet = false;
    private static int[] MAIN_MENU = null;
    private static final int MAIN_MENU_CENTER_X = 51;
    private static final int MAIN_MENU_CENTER_Y = 159;
    private static int[] MAIN_MENU_FUNCTION = null;
    private static final int[] MAIN_MENU_FUNCTION_MOREGAME = new int[]{4, 6, 7, 8, 9, 10, 11, 12};
    private static final int[] MAIN_MENU_FUNCTION_NO_MOREGAME = new int[]{4, 6, 8, 9, 10, 11, 12};
    private static final int[] MAIN_MENU_FUNCTION_UNACTIVIATE = new int[]{4, 7, 8, 9, 10, 11, 12};
    private static final int[] MAIN_MENU_MOREGAME = new int[]{1, 2, 3, 4, 5, 6, 7, 8};
    private static final int[] MAIN_MENU_NO_MOREGAME = new int[]{1, 2, 4, 5, 6, 7, 8};
    private static final int[] MAIN_MENU_UNACTIVIATE = new int[]{1, 3, 4, 5, 6, 7, 8};
    private static final int MAIN_MENU_V_CENTER_X = ((SCREEN_WIDTH >> 1) + 56);
    private static final int MAIN_MENU_V_CENTER_Y = ((SCREEN_HEIGHT >> 1) + 40);
    private static int MENU_INTERVAL = MENU_SPACE;
    private static int MENU_OFFSET_X = 0;
    private static final int MENU_SPACE_INTERVAL = 0;
    private static final int N5500_OFFSET = -15;
    private static final int N5500_OFFSET_2 = -7;
    private static final int N5500_OFFSET_3 = -12;
    private static final int[] OFFSET_ARRAY;
    private static final byte OPENING_STATE_AMY = (byte) 5;
    private static final byte OPENING_STATE_EMERALD = (byte) 0;
    private static final byte OPENING_STATE_EMERALD_SHINING = (byte) 1;
    private static final byte OPENING_STATE_END = (byte) 6;
    private static final byte OPENING_STATE_KNUCKLES = (byte) 4;
    private static final byte OPENING_STATE_SONIC = (byte) 2;
    private static final byte OPENING_STATE_TAILS = (byte) 3;
    private static final int[] OPTION_DIFFICULTY = new int[]{51, 53};
    private static final int OPTION_MOVING_INTERVAL = 100;
    private static final int OPTION_MOVING_SPEED = 4;
    private static final int[] OPTION_SE = new int[]{55, 144};
    private static final int[] OPTION_TIME = new int[]{60, 61};
    private static final int[] OPTION_SOUND_NO_VOLUME = new int[]{55, 144};
    private static final int[] OPTION_SOUND_VOLUME = new int[]{55, 58, 58, 58, 57, 57, 57, 57, 56, 56, 56};
    private static final int[] OPTION_SOUND = OPTION_SOUND_VOLUME;
    private static final int[][] OPTION_SELECTOR_HAS_SE = new int[][]{OPTION_DIFFICULTY, OPTION_SOUND, OPTION_SE, OPTION_TIME};
    private static final int[][] OPTION_SELECTOR_NO_SE = new int[][]{OPTION_DIFFICULTY, OPTION_SOUND, OPTION_TIME};
    private static final int[] OPTION_TAG_HAS_SE = new int[]{50, 54, StringIndex.STR_SE, 59, StringIndex.STR_RESET_RECORD};
    private static final int[] OPTION_TAG_NO_SE = new int[]{50, 54, 59, StringIndex.STR_RESET_RECORD};
    private static final int[] OPTION_TAG = OPTION_TAG_HAS_SE;
    private static final int OPTION_ELEMENT_NUM = OPTION_TAG.length;
    private static final int[][] OPTION_SELECTOR = OPTION_SELECTOR_HAS_SE;
    private static final int ORIGINAL_LOGO_X = ((SCREEN_WIDTH * 152) / SSDef.PLAYER_MOVE_HEIGHT);
    private static final int ORIGINAL_LOGO_Y_NO_ROTATE = ((SCREEN_HEIGHT * 61) / 320);
    private static final int ORIGINAL_LOGO_Y_ROTATE = ((SCREEN_HEIGHT * 168) / 320);
    private static final int LOGO_POSITION_X = ORIGINAL_LOGO_X;
    private static final int LOGO_POSITION_Y = ORIGINAL_LOGO_Y_ROTATE;
    private static final int LOGO_POSITION_Y_2 = ORIGINAL_LOGO_Y_NO_ROTATE;
    private static final int LINE_START_X = (LOGO_POSITION_X + 0);
    private static final int LINE_START_Y = ((((SCREEN_HEIGHT >> 1) + (MENU_SPACE + 4)) + (SCREEN_HEIGHT == SSDef.PLAYER_MOVE_HEIGHT ? 24 : 0)) + 0);

    private static final int PATCH_OFFSET_X = 66;
    private static final int PATCH_OFFSET_Y = 224;
    private static final int PIC_OFFSET_X = 92;
    private static final int PIC_OFFSET_Y = 12;
    private static final byte PRESS_DELAY = (byte) 5;
    private static final int PRESS_START_Y = ((SCREEN_HEIGHT * 236) / 320);
    private static final int RADIUS = 137;
    public static final int RETURN_PRESSED = 400;
    private static final int SHOW_ELEMENT_NUM = 5;
    private static final int SHOW_ELEMENT_V_NUM = 3;
    private static final int SONIC_BALL_SPACE = 120;
    private static final int SONIC_BIG_Y = ((SCREEN_HEIGHT * 28) / 320);
    private static final int SONIC_RUN_POSITION_X = -580;
    private static final int STAGE_SELECT_ARROW_STATE_DOWN = 2;
    private static final int STAGE_SELECT_ARROW_STATE_NONE = 0;
    private static final int STAGE_SELECT_ARROW_STATE_UP = 1;
    public static final int STAGE_SELECT_KEY_DIRECT_PLAY = 2;
    public static final int STAGE_SELECT_KEY_RECORD_1 = 0;
    public static final int STAGE_SELECT_KEY_RECORD_2 = 1;
    private static final int STAGE_SELECT_PRESS_STATE_ARROW = 2;
    private static final int STAGE_SELECT_PRESS_STATE_NONE = 0;
    private static final int STAGE_SELECT_PRESS_STATE_SLIDING = 1;
    private static final int STAGE_SELECT_SIDE_BAR_WIDTH = 60;
    private static final int STAGE_TYPE_CANT_CHOOSE = 137;
    private static final int STAGE_TYPE_CHOOSE = 19;
    private static final int STAGE_TYPE_UNCHOOSE = 13;
    private static final int[] START_GAME_MENU = new int[]{StringIndex.MENU_NEW_GAME, 12};
    private static final int STATE_ABOUT = 11;
    private static final int STATE_BP_TRY_PAYING = 22;
    private static final int STATE_CHARACTER_RECORD = 25;
    private static final int STATE_CHARACTER_SELECT = 23;
    private static final int STATE_EXIT = 12;
    private static final int STATE_GAMEOVER_RANKING = 15;
    private static final int STATE_GOTO_GAME = 5;
    private static final int STATE_HELP = 10;
    private static final int STATE_INTERGRADE_RECORD = 26;
    private static final int STATE_INTERRUPT = 16;
    private static final int STATE_MAIN_MENU = 2;
    private static final int STATE_MORE_GAME = 7;
    private static final int STATE_MOVING = 2;
    private static final int STATE_OPENING = 3;
    private static final int STATE_OPTION = 9;
    private static final int STATE_OPTION_CREDIT = 35;
    private static final int STATE_OPTION_DIFF = 27;
    private static final int STATE_OPTION_HELP = 34;
    private static final int STATE_OPTION_KEY_SET = 31;
    private static final int STATE_OPTION_LANGUAGE = 33;
    private static final int STATE_OPTION_RESET_RECORD = 36;
    private static final int STATE_OPTION_RESET_RECORD_ENSURE = 37;
    private static final int STATE_OPTION_SENSOR_SET = 40;
    private static final int STATE_OPTION_SOUND = 28;
    private static final int STATE_OPTION_SOUND_VOLUMN = 39;
    private static final int STATE_OPTION_SP_SET = 32;
    private static final int STATE_OPTION_TIME_LIMIT = 30;
    private static final int STATE_OPTION_VIBRATION = 29;
    private static final int STATE_PRESS_START = 1;
    private static final int STATE_PRE_PRESS_START = 38;
    private static final int STATE_PRO_RACE_MODE = 24;
    private static final int STATE_QUIT = 13;
    private static final int STATE_RACE_MODE = 6;
    private static final int STATE_RANKING = 8;
    private static final int STATE_RESET_RECORD_ASK = 17;
    private static final int STATE_RETURN_TO_LOGO_1 = 20;
    private static final int STATE_RETURN_TO_LOGO_2 = 21;
    private static final int STATE_SCORE_UPDATE = 42;
    private static final int STATE_SCORE_UPDATED = 44;
    private static final int STATE_SCORE_UPDATE_ENSURE = 43;
    private static final int STATE_SEGA_LOGO = 0;
    private static final int STATE_SEGA_MORE = 41;
    private static final int STATE_SELECT = 1;
    private static final int STATE_STAGE_SELECT = 14;
    private static final int STATE_START_GAME = 4;
    private static final int STATE_START_TO_MENU_1 = 18;
    private static final int STATE_START_TO_MENU_2 = 19;
    private static final int STATE_UP = 0;
    private static final int TIME_ATTACK_SPEED_X = -2;
    private static final int TIME_ATTACK_WIDTH = 128;
    private static final int TITLE_BG_COLOR_1 = 15530750;
    private static final int TITLE_BG_COLOR_2 = 9886462;
    private static final int TITLE_BG_OFFSET = 31;
    private static final int TITLE_BG_SPEED = 1;
    private static final int TITLE_BG_WIDTH = 62;
    private static final int TITLE_FRAME_HEIGHT = ((SCREEN_HEIGHT * 44) / 320);
    private static final int TOTAL_OPTION_ITEMS_NUM = 10;
    private static final int VISIBLE_OPTION_ITEMS_NUM = 9;
    private static final int ZONE_NUM_OFFSET = 1;
    private static final int ZONE_OFFSET = -22;
    private static AnimationDrawer downCursorDrawer = null;
    private static final int intergradeRecordtoGamecnt_max = 56;
    public static int preStageSelectState;
    private static String[] stageName = null;
    private static int state;
    public static MFImage titleLeftImage;
    public static MFImage titleRightImage;
    public static MFImage titleSegaImage;
    private static AnimationDrawer upCursorDrawer;
    public boolean IsFromOptionItems;
    public boolean IsFromStageSelect;
    private int RESET_INFO_COUNT;
    private int RecordtimeScrollPosY;
    private int STAGE_SEL_ARROW_DOWN_X;
    private int STAGE_SEL_ARROW_DOWN_Y;
    private int STAGE_SEL_ARROW_UP_X;
    private int STAGE_SEL_ARROW_UP_Y;
    private int STAGE_TOTAL_NUM;
    private int arrowPressState;
    private int ballVx;
    private int ballY;
    private int cameraX;
    private Animation[] charSelAni;
    private AnimationDrawer charSelAniDrawer;
    private AnimationDrawer charSelArrowDrawer;
    private AnimationDrawer charSelCaseDrawer;
    private Animation[] charSelFilAni;
    private AnimationDrawer charSelFilAniDrawer;
    private AnimationDrawer charSelRoleDrawer;
    private AnimationDrawer charSelTitleDrawer;
    private int characterRecordBGOffsetX_1;
    private int characterRecordBGOffsetX_2;
    private boolean characterRecordDisFlag;
    private int characterRecordID;
    private int characterRecordScoreUpdateCursor;
    private int characterRecordScoreUpdateIconY;
    private boolean character_arrow_display;
    private boolean character_circleturnright;
    private int character_id;
    private boolean character_idchangeFlag;
    private boolean character_move;
    private int character_move_frame;
    private int character_offset_state;
    private boolean character_outer;
    private int character_preid;
    private boolean character_reback;
    private int character_sel_frame_cnt;
    private int character_sel_offset_x;
    private int copyOffsetX;
    private MFImage copyrightImage;
    private int count;
    private int creditOffsetY;
    private int creditStringLineNum;
    private int currentStageSelectSlidePointY;
    private int debug_bgm_id;
    private int debug_bgm_state;
    private int degree;
    private int degreeDes;
    private boolean fadeChangeState;
    private int firstStageSelectSlidePointY;
    private int gestureSlideSpeed;
    private int gestureSlideState;
    private int intergradeRecordStageNameOffsetX;
    private int intergradeRecordtoGamecnt;
    private AnimationDrawer interruptDrawer;
    public int interrupt_state;
    private boolean isAtMainMenu;
    private boolean isChanged;
    private boolean isDrawDownArrow;
    private boolean isDrawUpArrow;
    private boolean isFromStartGame;
    private boolean isOptionChange;
    private boolean isOptionDisFlag;
    private boolean isRaceModeItemsSelected;
    private boolean isSelectable;
    private boolean isStageSelectChange;
    private boolean isTitleBGMPlay;
    private int itemOffsetX;
    private int logoGravity;
    private MFImage logoImage;
    private int logoVx;
    private int logoVy;
    private int logoX;
    private int logoY;
    private boolean mainMenuBackFlag;
    private int mainMenuCursor;
    private boolean mainMenuEnsureFlag;
    private int menuOptionCursor;
    private int[] multiMainItems;
    private int nextState;
    private int offsetOfVolumeInterface;
    private int[] offsetY;
    int offset_flag;
    private int opengingCursor;
    private Animation[] openingAnimation;
    private int openingCount;
    private AnimationDrawer[] openingDrawer;
    private boolean openingEnding;
    private int openingFrame;
    private int openingOffsetX;
    private int openingOffsetY;
    private byte openingState;
    private boolean openingStateChanging;
    private AnimationDrawer optionArrowDownDrawer;
    private int optionArrowDriveOffsetY;
    private int optionArrowDriveY;
    private boolean optionArrowMoveable;
    private AnimationDrawer optionArrowUpDrawer;
    private int[] optionCursor;
    private boolean optionDownArrowAvailable;
    private int optionDrawOffsetBottomY;
    private int optionDrawOffsetTmpY1;
    private int optionDrawOffsetY;
    private int optionMenuCursor;
    private int optionOffsetTmp;
    private int optionOffsetX;
    private int optionOffsetY;
    private int optionOffsetYAim;
    private boolean optionReturnFlag;
    private boolean optionUpArrowAvailable;
    private int optionYDirect;
    private int optionslide_getprey;
    private int optionslide_gety;
    private int optionslide_y;
    private int optionslidefirsty;
    private int preCharaterSelectState;
    private int preStageSelectSlidePointY;
    private byte pressDelay;
    private byte pressDelay2;
    private int quitFlag;
    private int[] rankingScore;
    private Animation[] recordAni;
    private AnimationDrawer recordAniDrawer;
    private int[] recordArrowOffsetXArray;
    private int recordArrowOffsetXID;
    private int resetInfoCount;
    private int returnCursor;
    private int scoreUpdateCursor;
    private int shakeCount;
    private AnimationDrawer skipDrawer;
    private MFImage sonicBigImage;
    private MFImage sonicBigPatchImage;
    private int sonicBigX;
    private int sonicVx;
    private int sonicX;
    private int sonicY;
    private int stageDrawEndY;
    private int stageDrawKeyAimY;
    private int stageDrawKeyOffsetY;
    private int stageDrawOffsetBottomY;
    private int stageDrawOffsetTmpY1;
    private int stageDrawOffsetTmpY2;
    private int stageDrawOffsetY;
    private int stageDrawStartY;
    private int stageItemNumForShow;
    private Animation[] stageSelAni;
    private AnimationDrawer stageSelAniDrawer;
    private AnimationDrawer stageSelArrowDownDrawer;
    private AnimationDrawer stageSelArrowUpDrawer;
    private AnimationDrawer stageSelEmeraldDrawer;
    private int stageSelectArrowDriveOffsetY;
    private int stageSelectArrowDriveY;
    private boolean stageSelectArrowMoveable;
    private boolean stageSelectDownArrowAvailable;
    private boolean stageSelectReturnFlag;
    private int stageSelectSlideFrame;
    private boolean stageSelectUpArrowAvailable;
    private int stageStartIndex;
    private int stageYDirect;
    private int stage_characterRecord_ID;
    private int stage_sel_key;
    private int stage_select_arrow_state;
    private int stage_select_press_state;
    private int stage_select_state;
    private int stageselectslide_getprey;
    private int stageselectslide_gety;
    private int stageselectslide_y;
    private int stageselectslidefirsty;
    private int startgamecursor;
    private boolean startgameensureFlag;
    private int startgameframe;
    private Animation[] timeAttAni;
    private AnimationDrawer timeAttAniDrawer;
    private int timeAttackOffsetX;
    private int timecount_ranking;
    private Animation[] titleAni;
    private AnimationDrawer titleAniDrawer;
    private int titleDegree;
    private int titleFrame;
    private MFImage titleFrameImage;
    public float titleScale;
    private AnimationDrawer titleSonicDrawer;
    private int title_name_center_x;
    private int title_name_center_y;
    private int[] vY;

    static {
        int[] iArr = new int[6];
        iArr[1] = -1;
        iArr[2] = -1;
        iArr[4] = 1;
        iArr[5] = 1;
        OFFSET_ARRAY = iArr;
    }

    public static void setMainMenu() {
        MAIN_MENU = MAIN_MENU_MOREGAME;
        MAIN_MENU_FUNCTION = MAIN_MENU_FUNCTION_MOREGAME;
    }

    public TitleState() {
        this.STAGE_TOTAL_NUM = 14;
        this.count = 0;
        this.titleScale = 1.0f;
        this.title_name_center_x = 77;
        this.title_name_center_y = -53;
        this.titleFrame = 0;
        this.mainMenuBackFlag = false;
        this.resetInfoCount = 0;
        this.RESET_INFO_COUNT = 30;
        this.optionCursor = new int[OPTION_ELEMENT_NUM];
        this.isOptionDisFlag = false;
        this.optionslide_getprey = -1;
        this.optionslide_gety = -1;
        this.offsetOfVolumeInterface = 0;
        this.pressDelay = (byte) 5;
        this.pressDelay2 = (byte) 1;
        this.rankingScore = new int[5];
        this.timecount_ranking = 0;
        this.offset_flag = 0;
        this.RecordtimeScrollPosY = 0;
        this.logoX = LOGO_POSITION_X;
        this.logoGravity = 12;
        this.copyOffsetX = 0;
        this.opengingCursor = -1;
        this.shakeCount = 0;
        this.multiMainItems = new int[]{3, 5, 7, 12, 9};
        this.isAtMainMenu = true;
        this.arrowPressState = 0;
        this.timeAttackOffsetX = 0;
        this.stageselectslide_getprey = -1;
        this.stageselectslide_gety = -1;
        this.stageSelectReturnFlag = false;
        this.characterRecordDisFlag = false;
        int[] iArr = new int[12];
        iArr[0] = 1;
        iArr[1] = 1;
        iArr[2] = 2;
        iArr[3] = 2;
        iArr[4] = 3;
        iArr[5] = 3;
        iArr[6] = 2;
        iArr[7] = 2;
        iArr[8] = 1;
        iArr[9] = 1;
        this.recordArrowOffsetXArray = iArr;
        this.isTitleBGMPlay = false;
        this.debug_bgm_state = 0;
        this.debug_bgm_id = 0;
        state = 0;
        Key.touchsoftkeyInit();
        this.logoX = LOGO_POSITION_X;
        this.logoY = LOGO_POSITION_Y;
        this.sonicBigX = 0;
        initTitleRes();

        // Hack
        //state = 2; // direct main menu
    }

    public static void drawTitle(MFGraphics g, int layer) {
        if (state == 38 || state == 1 || state == 2 || state == 12 || state == 4 || state == 41) {
            float scale = ((float) MFDevice.getDeviceHeight()) / ((float) titleLeftImage.getHeight());
            g.saveCanvas();
            g.scaleCanvas(scale, scale, 0, 0);
            g.drawImage(titleLeftImage, 0, 0, 20);
            g.restoreCanvas();
            g.saveCanvas();
            g.scaleCanvas(scale, scale, MFDevice.getDeviceWidth(), 0);
            g.drawImage(titleRightImage, MFDevice.getDeviceWidth(), 0, 24);
            g.restoreCanvas();
            g.saveCanvas();
            g.scaleCanvas(scale, scale, MFDevice.getDeviceWidth(), MFDevice.getDeviceHeight());
            g.drawImage(titleSegaImage, MFDevice.getDeviceWidth(), MFDevice.getDeviceHeight(), 40);
            g.restoreCanvas();
        }
    }

    public TitleState(int stateId) {
        this.STAGE_TOTAL_NUM = 14;
        this.count = 0;
        this.titleScale = 1.0f;
        this.title_name_center_x = 77;
        this.title_name_center_y = -53;
        this.titleFrame = 0;
        this.mainMenuBackFlag = false;
        this.resetInfoCount = 0;
        this.RESET_INFO_COUNT = 30;
        this.optionCursor = new int[OPTION_ELEMENT_NUM];
        this.isOptionDisFlag = false;
        this.optionslide_getprey = -1;
        this.optionslide_gety = -1;
        this.offsetOfVolumeInterface = 0;
        this.pressDelay = (byte) 5;
        this.pressDelay2 = (byte) 1;
        this.rankingScore = new int[5];
        this.timecount_ranking = 0;
        this.offset_flag = 0;
        this.RecordtimeScrollPosY = 0;
        this.logoX = LOGO_POSITION_X;
        this.logoGravity = 12;
        this.copyOffsetX = 0;
        this.opengingCursor = -1;
        this.shakeCount = 0;
        this.multiMainItems = new int[]{3, 5, 7, 12, 9};
        this.isAtMainMenu = true;
        this.arrowPressState = 0;
        this.timeAttackOffsetX = 0;
        this.stageselectslide_getprey = -1;
        this.stageselectslide_gety = -1;
        this.stageSelectReturnFlag = false;
        this.characterRecordDisFlag = false;
        int[] iArr = new int[12];
        iArr[0] = 1;
        iArr[1] = 1;
        iArr[2] = 2;
        iArr[3] = 2;
        iArr[4] = 3;
        iArr[5] = 3;
        iArr[6] = 2;
        iArr[7] = 2;
        iArr[8] = 1;
        iArr[9] = 1;
        this.recordArrowOffsetXArray = iArr;
        this.isTitleBGMPlay = false;
        this.debug_bgm_state = 0;
        this.debug_bgm_id = 0;
        state = 0;
        this.logoX = LOGO_POSITION_X;
        this.logoY = LOGO_POSITION_Y_2;
        this.sonicBigX = 0;
        initTitleRes();
        switch (stateId) {
            case 2:
                state = 1;
                this.nextState = 1;
                this.logoY = LOGO_POSITION_Y;
                SoundSystem.getInstance().playBgm(1, false);
                break;
            case 3:
                state = 14;
                this.nextState = 14;
                preStageSelectState = 23;
                this.preCharaterSelectState = 24;
                menuInit(this.STAGE_TOTAL_NUM);
                initStageSelectRes();
                PlayerObject.stageModeState = 1;
                SoundSystem.getInstance().playBgm(3);
                this.stage_sel_key = 1;
                break;
            case 4:
                rankingInit();
                state = 15;
                this.nextState = 15;
                SoundSystem.getInstance().playBgm(4);
                break;
            case 5:
                state = 14;
                this.nextState = 14;
                preStageSelectState = 2;
                menuInit(this.STAGE_TOTAL_NUM);
                initStageSelet();
                PlayerObject.stageModeState = 0;
                SoundSystem.getInstance().playBgm(3);
                break;
            case 9:
                state = 23;
                this.nextState = 23;
                this.preCharaterSelectState = 24;
                this.stage_sel_key = 1;
                initCharacterSelectRes();
                break;
        }
        Key.touchkeypauseClose();
    }

    public void close() {
        MFDevice.disableLayer(-1);
        titleLeftImage = null;
        titleRightImage = null;
        titleSegaImage = null;
        Animation.closeAnimationArray(this.titleAni);
        this.titleAni = null;
        Animation.closeAnimationDrawer(this.titleAniDrawer);
        this.titleAniDrawer = null;
        Animation.closeAnimationArray(this.stageSelAni);
        this.stageSelAni = null;
        Animation.closeAnimationDrawer(this.stageSelAniDrawer);
        this.stageSelAniDrawer = null;
        Animation.closeAnimationDrawer(this.stageSelArrowUpDrawer);
        this.stageSelArrowUpDrawer = null;
        Animation.closeAnimationDrawer(this.stageSelArrowDownDrawer);
        this.stageSelArrowDownDrawer = null;
        Animation.closeAnimationDrawer(this.stageSelEmeraldDrawer);
        this.stageSelEmeraldDrawer = null;
        Animation.closeAnimationDrawer(this.optionArrowUpDrawer);
        this.optionArrowUpDrawer = null;
        Animation.closeAnimationDrawer(this.optionArrowDownDrawer);
        this.optionArrowDownDrawer = null;
        Animation.closeAnimationArray(this.timeAttAni);
        this.timeAttAni = null;
        Animation.closeAnimationDrawer(this.timeAttAniDrawer);
        this.timeAttAniDrawer = null;
        Animation.closeAnimationArray(this.recordAni);
        this.recordAni = null;
        Animation.closeAnimationDrawer(this.recordAniDrawer);
        this.recordAniDrawer = null;
        Animation.closeAnimationArray(this.charSelAni);
        this.charSelAni = null;
        Animation.closeAnimationDrawer(this.charSelAniDrawer);
        this.charSelAniDrawer = null;
        Animation.closeAnimationDrawer(this.charSelCaseDrawer);
        this.charSelCaseDrawer = null;
        Animation.closeAnimationDrawer(this.charSelRoleDrawer);
        this.charSelRoleDrawer = null;
        Animation.closeAnimationDrawer(this.charSelArrowDrawer);
        this.charSelArrowDrawer = null;
        Animation.closeAnimationDrawer(this.charSelTitleDrawer);
        this.charSelTitleDrawer = null;
        Animation.closeAnimationArray(this.charSelFilAni);
        this.charSelFilAni = null;
        Animation.closeAnimationDrawer(this.charSelFilAniDrawer);
        this.charSelFilAniDrawer = null;
        Animation.closeAnimationDrawer(this.titleSonicDrawer);
        this.titleSonicDrawer = null;
        this.logoImage = null;
        this.sonicBigImage = null;
        this.titleFrameImage = null;
        this.copyrightImage = null;
        openingClose();
        Animation.closeAnimationDrawer(this.interruptDrawer);
        this.interruptDrawer = null;
        System.gc();
        try {
            Thread.sleep(100);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void draw(MFGraphics g) {
        switch (state) {
            case 34:
            case 35:
                g.setFont(11);
                break;
            default:
                g.setFont(14);
                break;
        }
        switch (state) {
            case 0:
                Standard.drawSplash(g, MyAPI.zoomOut(SCREEN_WIDTH), MyAPI.zoomOut(SCREEN_HEIGHT));
                break;
            case 1:
                drawTitleBg(g);
                break;
            case 2:
                drawTitleBg(g);
                drawMainMenu(g);
                break;
            case 3:
                openingDraw(g);
                break;
            case 4:
                drawTitleBg(g);
                drawMainMenu(g);
                State.drawFade(g);
                startGameDraw(g);
                break;
            case 6:
                stageSelectDraw(g, 1);
                drawTouchKeySelectStage(g);
                State.drawSoftKey(g, true, true);
                break;
            case 7:
                moregameDraw(g);
                State.drawSoftKey(g, true, true);
                break;
            case 8:
                rankingDraw(g);
                State.drawSoftKey(g, false, true);
                break;
            case 9:
                optionDraw(g);
                break;
            case 10:
                helpDraw(g);
                drawTouchKeyHelp(g);
                State.drawSoftKey(g, false, true);
                break;
            case 11:
                aboutDraw(g);
                drawTouchKeyAbout(g);
                State.drawSoftKey(g, false, true);
                break;
            case 12:
                drawTitleBg(g);
                if (this.quitFlag == 0) {
                    drawMainMenu(g);
                }
                State.drawFade(g);
                SecondEnsurePanelDraw(g, 14);
                break;
            case 13:
                Standard.drawMoreGame(g, MyAPI.zoomOut(SCREEN_WIDTH), MyAPI.zoomOut(SCREEN_HEIGHT));
                break;
            case 14:
                drawStageSelect(g);
                break;
            case 15:
                rankingDraw(g);
                State.drawSoftKey(g, false, true);
                break;
            case 16:
                interruptDraw(g);
                break;
            case 17:
                menuBgDraw(g);
                optionDraw(g);
                State.drawFade(g);
                comfirmDraw(g, StringIndex.STR_RESET_RECORD_COMFIRM);
                State.drawSoftKey(g, true, true);
                break;
            case 18:
            case 19:
            case 20:
            case 21:
                titleBgDraw0(g);
                drawTitle1(g);
                if (state != 1 || (System.currentTimeMillis() / 500) % 2 == 0) {
                    drawTitle2(g);
                    break;
                }
                drawTitle2(g);
                break;
            case 23:
                drawCharacterSelect(g);
                break;
            case 24:
                drawProTimeAttack(g);
                break;
            case 25:
                drawCharacterRecord(g);
                break;
            case 26:
                drawIntergradeRecord(g);
                break;
            case 27:
                optionDraw(g);
                itemsSelect2Draw(g, 33, 34);
                break;
            case 28:
                optionDraw(g);
                itemsSelect2Draw(g, 35, 36);
                break;
            case 29:
                optionDraw(g);
                itemsSelect2Draw(g, 35, 36);
                break;
            case 30:
                optionDraw(g);
                itemsSelect2Draw(g, 35, 36);
                break;
            case 31:
                optionDraw(g);
                State.drawFade(g);
                break;
            case 32:
                optionDraw(g);
                itemsSelect2Draw(g, 37, 38);
                break;
            case 33:
                optionDraw(g);
                menuOptionLanguageDraw(g);
                break;
            case 34:
                optionDraw(g);
                helpDraw(g);
                break;
            case 35:
                optionDraw(g);
                creditDraw(g);
                break;
            case 36:
                optionDraw(g);
                SecondEnsurePanelDraw(g, 44);
                break;
            case 37:
                optionDraw(g);
                SecondEnsurePanelDraw(g, 45);
                break;
            case 38:
                g.setColor(0);
                this.titleAniDrawer.setActionId(0);
                State.drawFade(g);
                break;
            case 39:
                optionDraw(g);
                soundVolumnDraw(g);
                break;
            case 40:
                optionDraw(g);
                spSenorSetDraw(g);
                break;
            case 41:
                drawTitleBg(g);
                drawMainMenu(g);
                State.drawFade(g);
                SecondEnsurePanelDraw(g, 105);
                break;
        }
        if (isDrawTouchPad && state != 0 && state != 1 && state != 13) {
            drawTouchKeyDirect(g);
        }
    }

    public void logic() {
        if (this.count > 0) {
            this.count--;
        }
        fadeStateLogic();
        switch (state) {
            case 0:
                if (Key.press(Key.B_S1 | Key.gSelect)) {
                    Standard.pressConfirm();
                } else if (Key.press(2)) {
                    Standard.pressCancel();
                }
                switch (Standard.execSplash()) {
                    case 1:
                        GlobalResource.soundSwitchConfig = 1;
                        if (GlobalResource.soundConfig == 0) {
                            GlobalResource.soundConfig = 9;
                        }
                        GlobalResource.seConfig = 1;
                        SoundSystem.getInstance().setSoundState(GlobalResource.soundConfig);
                        SoundSystem.getInstance().setSeState(GlobalResource.seConfig);
                        return;
                    case 2:
                        GlobalResource.soundSwitchConfig = 1;
                        GlobalResource.soundConfig = 0;
                        GlobalResource.seConfig = 0;
                        SoundSystem.getInstance().setSoundState(0);
                        SoundSystem.getInstance().setSeState(0);
                        return;
                    case 3:
                        changeStateWithFade(3);
                        Key.touchOpeningInit();
                        if (!SoundSystem.getInstance().bgmPlaying()) {
                            SoundSystem.getInstance().playBgm(0, false);
                        }
                        Key.touchsoftkeyClose();
                        Key.touchanykeyInit();
                        State.load_bp_string();
                        return;
                    default:
                        return;
                }
            case 1:
                this.titleFrame++;
                if (this.titleFrame > 212) {
                    state = 3;
                    openingInit();
                    Key.touchOpeningInit();
                    SoundSystem.getInstance().playBgm(0, false);
                }
                if (this.titleFrame > 4) {
                    if (Key.buttonPress(Key.B_SEL)) {
                        SoundSystem.getInstance().playSe(1);
                        gotoMainmenu();
                        State.setFadeOver();
                        Key.clear();
                    }
                    if (Key.press(524288) && State.fadeChangeOver()) {
                        state = 12;
                        secondEnsureInit();
                        State.fadeInit(0, 220);
                        SoundSystem.getInstance().playSe(1);
                        this.quitFlag = 1;
                        return;
                    }
                    return;
                }
                return;
            case 2:
                mainMenuLogic();
                return;
            case 3:
                if (openingLogic()) {
                    state = 38;
                    this.isTitleBGMPlay = true;
                    State.setFadeColor(MapManager.END_COLOR);
                    State.fadeInit(255, 0);
                    openingClose();
                    initTitleRes();
                    return;
                }
                return;
            case 4:
                startGameLogic();
                return;
            case 5:
                State.setState(1);
                return;
            case 7:
                switch (comfirmLogic()) {
                    case 0:
                        SoundSystem.getInstance().stopBgm(true);
                        Standard.menuMoreGameSelected();
                        State.exitGame();
                        return;
                    case 1:
                    case 400:
                        state = 2;
                        menuInit(MAIN_MENU);
                        mainMenuInit();
                        return;
                    default:
                        return;
                }
            case 8:
                rankingLogic();
                return;
            case 9:
                optionLogic();
                return;
            case 10:
                helpLogic();
                if (Key.press(2)) {
                }
                if (Key.press(524288)) {
                    changeStateWithFade(2);
                    menuInit(MAIN_MENU);
                    mainMenuInit();
                    Key.touchHelpClose();
                    return;
                }
                return;
            case 11:
                aboutLogic();
                return;
            case 12:
                quitLogic();
                return;
            case 13:
                if (Key.press(Key.B_S1 | Key.gSelect)) {
                    Standard.pressConfirm();
                } else if (Key.press(2)) {
                    Standard.pressCancel();
                }
                switch (Standard.execMoreGame(true)) {
                    case 0:
                        Key.touchsoftkeyClose();
                        GlobalResource.saveSystemConfig();
                        State.exitGame();
                        sendMessage(new Message(), 5);
                        return;
                    default:
                        return;
                }
            case 14:
                stageSelectLogic();
                return;
            case 15:
                gameover_rankingLogic();
                return;
            case 16:
                interruptLogic();
                return;
            case 17:
                switch (comfirmLogic()) {
                    case 0:
                        StageManager.resetGameRecord();
                        this.resetInfoCount = this.RESET_INFO_COUNT;
                        state = 9;
                        return;
                    case 1:
                    case 400:
                        state = 9;
                        return;
                    default:
                        return;
                }
            case 18:
                this.logoX = MyAPI.calNextPositionReverse(this.logoX, LOGO_POSITION_X, SCREEN_WIDTH + (SCREEN_WIDTH >> 1), 1, 2);
                if (this.logoX == SCREEN_WIDTH + (SCREEN_WIDTH >> 1)) {
                    this.logoX = -(SCREEN_WIDTH >> 1);
                    state = 2;
                    this.nextState = 2;
                    this.logoY = LOGO_POSITION_Y_2;
                    return;
                }
                return;
            case 19:
                this.logoX = MyAPI.calNextPosition((double) this.logoX, (double) LOGO_POSITION_X, 1, 2);
                if (this.logoX == LOGO_POSITION_X) {
                    state = 2;
                    this.nextState = 2;
                    return;
                }
                return;
            case 20:
                this.logoX = MyAPI.calNextPositionReverse(this.logoX, LOGO_POSITION_X, SCREEN_WIDTH + (SCREEN_WIDTH >> 1), 1, 2);
                if (this.logoX == SCREEN_WIDTH + (SCREEN_WIDTH >> 1)) {
                    this.logoX = -(SCREEN_WIDTH >> 1);
                    state = 21;
                    this.nextState = 21;
                    this.logoY = LOGO_POSITION_Y;
                }
                if (this.mainMenuBackFlag && !this.menuMoving) {
                    this.mainMenuBackFlag = false;
                    return;
                }
                return;
            case 21:
                this.logoX = MyAPI.calNextPosition((double) this.logoX, (double) LOGO_POSITION_X, 1, 2);
                if (this.logoX == LOGO_POSITION_X) {
                    state = 1;
                    this.nextState = 1;
                }
                if (this.mainMenuBackFlag && !this.menuMoving) {
                    this.mainMenuBackFlag = false;
                    return;
                }
                return;
            case 23:
                characterSelectLogic();
                return;
            case 24:
                proRaceModeLogic();
                return;
            case 25:
                characterRecordLogic();
                return;
            case 26:
                intergradeRecordLogic();
                return;
            case 27:
                menuOptionDiffLogic();
                return;
            case 28:
                menuOptionSoundLogic();
                return;
            case 29:
                menuOptionVibLogic();
                return;
            case 30:
                menuOptionTimeLimitLogic();
                return;
            case 32:
                menuOptionSpSetLogic();
                return;
            case 33:
                menuOptionLanguageLogic();
                return;
            case 34:
                helpLogic();
                if ((Key.press(524288) || (Key.touchhelpreturn.IsButtonPress() && this.returnPageCursor == 1)) && State.fadeChangeOver()) {
                    changeStateWithFade(9);
                    this.isOptionDisFlag = false;
                    SoundSystem.getInstance().playSe(2);
                    return;
                }
                return;
            case 35:
                creditLogic();
                return;
            case 36:
                menuOptionResetRecordLogic();
                return;
            case 37:
                menuOptionResetRecordEnsureLogic();
                return;
            case 38:
                if (State.fadeChangeOver()) {
                    state = 1;
                    State.setFadeColor(0);
                    Key.touchOpeningClose();
                    return;
                }
                return;
            case 39:
                switch (soundVolumnLogic()) {
                    case 2:
                        State.fadeInit(220, 0);
                        state = 9;
                        return;
                    default:
                        return;
                }
            case 40:
                switch (spSenorSetLogic()) {
                    case 1:
                        GlobalResource.sensorConfig = 0;
                        State.fadeInit(220, 0);
                        state = 9;
                        return;
                    case 2:
                        GlobalResource.sensorConfig = 1;
                        State.fadeInit(220, 0);
                        state = 9;
                        return;
                    case 3:
                        State.fadeInit(220, 0);
                        state = 9;
                        return;
                    case 4:
                        GlobalResource.sensorConfig = 2;
                        State.fadeInit(220, 0);
                        state = 9;
                        return;
                    default:
                        return;
                }
            case 41:
                segaMoreLogic();
                return;
            default:
                return;
        }
    }

    public void init() {
        try {
            helpStrings = MyAPI.loadText("/help");
            aboutStrings = MyAPI.loadText("/about");
            this.openingOffsetX = (SCREEN_WIDTH - 284) >> 1;
            this.openingOffsetY = (SCREEN_HEIGHT - 160) >> 1;
        } catch (Exception e) {
        }
        this.count = 50;
        State.initMenuFont();
        if (state == 0) {
            openingInit();
        }
        this.titleScale = 4.0f;
    }

    private void drawTitleName(MFGraphics g) {
        if (this.titleFrame == 0) {
            this.titleScale = 4.0f;
            this.title_name_center_x = 216;
            this.title_name_center_y = 152;
        } else if (this.titleFrame == 1) {
            this.titleScale = 3.25f;
            this.title_name_center_x = 162;
            this.title_name_center_y = 114;
        } else if (this.titleFrame == 2) {
            this.titleScale = 2.5f;
            this.title_name_center_x = Def.TOUCH_SEL_1_W;
            this.title_name_center_y = 76;
        } else if (this.titleFrame == 3) {
            this.titleScale = 1.75f;
            this.title_name_center_x = 54;
            this.title_name_center_y = 38;
        } else if (this.titleFrame >= 4) {
            this.titleScale = 1.0f;
            this.title_name_center_x = 0;
            this.title_name_center_y = 0;
        }
        g.saveCanvas();
        g.translateCanvas(SCREEN_WIDTH + this.title_name_center_x, (SCREEN_HEIGHT >> 1) + this.title_name_center_y);
        g.scaleCanvas(this.titleScale, this.titleScale);
        this.titleAniDrawer.setActionId(1);
        g.restoreCanvas();
        if (this.titleFrame >= 4) {
            this.titleAniDrawer.setActionId(11);
        }
    }

    private void drawSegaLogo(MFGraphics g) {
    }

    private void menuDraw(MFGraphics g) {
        this.menuMoving = false;
        if (this.currentElement != null) {
            int posStartY = (SCREEN_HEIGHT - ((this.currentElement.length - 1) * MENU_INTERVAL)) >> 1;
            for (int i = 0; i < this.currentElement.length; i++) {
            }
        }
    }

    private void mainMenuInit() {
        this.menuMoving = true;
        this.degree = (this.currentElement.length - 1) * 18;
        this.degreeDes = 0;
    }

    private void mainMenuBackInit() {
        this.menuMoving = true;
        this.mainMenuBackFlag = true;
        this.degree = 0;
        this.degreeDes = (this.currentElement.length - 1) * 18;
    }

    private void mainMenuDraw(MFGraphics g) {
        if (this.mainMenuBackFlag) {
            this.degree = MyAPI.calNextPositionReverse(this.degree, 0, this.degreeDes, 1, 3);
        } else {
            this.degree = MyAPI.calNextPosition((double) this.degree, (double) this.degreeDes, 1, 3);
        }
        if (this.degree == this.degreeDes) {
            this.menuMoving = false;
        }
        if (this.currentElement != null) {
            int startCursor = (((this.cursor - 1) - 1) + this.currentElement.length) % this.currentElement.length;
            for (int i = 0; i < 6; i++) {
                int elementId = (startCursor + i) % this.currentElement.length;
                int currentDegree = (this.degree + DEGREE_START) + ((i - 1) * 18);
                if (currentDegree >= -27 && currentDegree <= 63) {
                    State.drawMenuFontById(g, this.currentElement[elementId], ((MyAPI.dCos(currentDegree) * 137) + 5100) / 100, ((MyAPI.dSin(currentDegree) * 137) + 15900) / 100);
                }
            }
        }
    }

    private void mainMenuDrawV(MFGraphics g) {
        this.menuMoving = false;
        this.mainMenuBackFlag = false;
        int startCursor = (((this.cursor + 0) - 1) + this.elementNum) % this.elementNum;
        if (this.currentElement != null) {
            for (int i = 0; i < 3; i++) {
                int currentPos = MAIN_MENU_V_CENTER_Y + ((i - 1) * 20);
                State.drawMenuFontById(g, this.currentElement[(startCursor + i) % this.currentElement.length], MAIN_MENU_V_CENTER_X, currentPos);
            }
            State.drawMenuFontById(g, 95, MAIN_MENU_V_CENTER_X, (SCREEN_HEIGHT >> 1) + 10);
            State.drawMenuFontById(g, 96, MAIN_MENU_V_CENTER_X, (SCREEN_HEIGHT >> 1) + 70);
        }
    }

    private void mainMenuDraw2(MFGraphics g) {
        MENU_OFFSET_X = (SCREEN_WIDTH >> 1) + 16;
        this.degree = MyAPI.calNextPosition((double) this.degree, 0.0d, 1, 3);
        if (this.degree == 0) {
            this.menuMoving = false;
        }
        int startCursor = (((this.cursor - 1) - 1) + this.currentElement.length) % this.currentElement.length;
        for (int i = 0; i < 4; i++) {
            int elementId = (startCursor + i) % this.currentElement.length;
            int x = MENU_OFFSET_X;
            int y = (this.degree + LINE_START_Y) + ((MENU_SPACE + MENU_SPACE_INTERVAL) * (i - 1));
            if (y >= LINE_START_Y - ((MENU_SPACE + MENU_SPACE_INTERVAL) >> 1) && y <= (LINE_START_Y + (MENU_OFFSET_X * 2)) + ((MENU_SPACE + MENU_SPACE_INTERVAL) >> 1)) {
                State.drawMenuFontById(g, this.currentElement[elementId], x, y);
            }
        }
    }

    public void changeUpSelect() {
        this.degree = DEGREE_START;
        this.menuMoving = true;
    }

    public void changeDownSelect() {
        this.degree = 18;
        this.menuMoving = true;
    }

    private void titleBgDraw0(MFGraphics g) {
    }

    private void drawMenuSelection1(MFGraphics g, int id, int x, int y) {
        State.drawMenuBar(g, 1, 0, y);
        this.selectMenuOffsetX += 8;
        this.selectMenuOffsetX %= MOVE_DIRECTION;
        while (x - this.selectMenuOffsetX > 0) {
            x -= MOVE_DIRECTION;
        }
        for (int i = 0; i < 2; i++) {
            State.drawMenuFontById(g, id, (x + (MOVE_DIRECTION * i)) - this.selectMenuOffsetX, y);
        }
    }

    public void changeStateWithFade(int nState) {
        if (!fading) {
            fading = true;
            State.fadeInit(0, 255);
            this.nextState = nState;
            this.fadeChangeState = true;
        }
    }

    public void fadeStateLogic() {
        if (fading && this.fadeChangeState && State.fadeChangeOver() && state != this.nextState) {
            state = this.nextState;
            this.fadeChangeState = false;
            if (this.IsFromStageSelect) {
                State.fadeInit(255, 102);
                this.IsFromStageSelect = false;
            } else if (this.IsFromOptionItems) {
                State.fadeInit(255, 220);
                this.IsFromOptionItems = false;
            } else {
                State.fadeInit(255, 0);
            }
        }
        if (state == this.nextState && State.fadeChangeOver()) {
            fading = false;
        }
    }

    private void optionInit() {
        this.optionMenuCursor = 0;
        this.optionCursor[0] = GlobalResource.difficultyConfig;
        this.optionCursor[1] = GlobalResource.soundConfig;
        this.optionCursor[2] = GlobalResource.seConfig;
        this.optionCursor[3] = GlobalResource.timeLimit;
        this.resetInfoCount = 0;
        warningY = WARNING_Y_DES_2;
        this.offsetOfVolumeInterface = 0;
        this.optionOffsetX = 0;
        if (muiAniDrawer == null) {
            muiAniDrawer = new Animation("/animation/mui").getDrawer(0, false, 0);
        }
        if (this.optionArrowUpDrawer == null) {
            this.optionArrowUpDrawer = new Animation("/animation/mui").getDrawer(64, true, 0);
            this.optionArrowDownDrawer = new Animation("/animation/mui").getDrawer(65, true, 0);
        }
        Key.touchMenuOptionInit();
        this.menuOptionCursor = 0;
        this.optionOffsetYAim = 0;
        this.optionOffsetY = 0;
        this.isChanged = false;
        this.isOptionDisFlag = false;
        this.optionslide_getprey = -1;
        this.optionslide_gety = -1;
        this.optionslide_y = 0;
        this.optionDrawOffsetBottomY = -96;
        this.optionYDirect = 0;
    }

    private void optionLogic() {
        int i;
        if (!this.isOptionDisFlag) {
            SoundSystem.getInstance().playBgm(5);
            this.isOptionDisFlag = true;
        }
        this.optionslide_gety = Key.slidesensormenuoption.getPointerY();
        if (this.optionslide_gety == -1 && this.optionslide_getprey == -1) {
            this.optionslide_y = 0;
            this.optionslidefirsty = 0;
        } else if (this.optionslide_gety != -1 && this.optionslide_getprey == -1) {
            this.optionslidefirsty = this.optionslide_gety;
        } else if (this.optionslide_gety != -1 && this.optionslide_getprey != -1) {
            this.optionslide_y = this.optionslide_gety - this.optionslidefirsty;
        } else if (this.optionslide_gety == -1 && this.optionslide_getprey != -1) {
            this.optionDrawOffsetTmpY1 = this.optionslide_y + this.optionDrawOffsetY;
        }
        for (i = 0; i < (Key.touchmenuoptionitems.length >> 1); i++) {
            Key.touchmenuoptionitems[i * 2].setStartY((((i * 24) + 28) + this.optionDrawOffsetY) + this.optionslide_y);
            Key.touchmenuoptionitems[(i * 2) + 1].setStartY((((i * 24) + 28) + this.optionDrawOffsetY) + this.optionslide_y);
        }
        if (this.isSelectable) {
            for (i = 0; i < Key.touchmenuoptionitems.length; i++) {
                if (Key.touchmenuoptionitems[i].Isin() && Key.touchmenuoption.IsClick()) {
                    this.menuOptionCursor = i / 2;
                    this.returnCursor = 0;
                    break;
                }
            }
        }
        if (Key.touchmenuoptionreturn.Isin() && Key.touchmenuoption.IsClick()) {
            this.returnCursor = 1;
        }
        if ((Key.press(524288) || (Key.touchmenuoptionreturn.IsButtonPress() && this.returnCursor == 1)) && State.fadeChangeOver()) {
            changeStateWithFade(2);
            this.isTitleBGMPlay = false;
            Key.touchMainMenuInit2();
            SoundSystem.getInstance().stopBgm(false);
            SoundSystem.getInstance().playSe(2);
            GlobalResource.saveSystemConfig();
            this.returnCursor = 0;
            menuInit(this.multiMainItems);
        }
        if (this.optionDrawOffsetY + this.optionslide_y < 0) {
            this.optionUpArrowAvailable = true;
        } else {
            this.optionUpArrowAvailable = false;
        }
        if (this.optionDrawOffsetY + this.optionslide_y > this.optionDrawOffsetBottomY) {
            this.optionDownArrowAvailable = true;
        } else {
            this.optionDownArrowAvailable = false;
        }
        if (Key.touchmenuoptionuparrow.Isin() && this.optionUpArrowAvailable) {
            this.optionArrowDriveOffsetY = 12;
            this.optionArrowMoveable = true;
        }
        if (Key.touchmenuoptiondownarrow.Isin() && this.optionDownArrowAvailable) {
            this.optionArrowDriveOffsetY = -12;
            this.optionArrowMoveable = true;
        }
        if (this.optionArrowMoveable) {
            this.optionArrowDriveOffsetY /= 2;
            if (this.optionArrowDriveOffsetY > 0 && this.optionArrowDriveOffsetY < 2) {
                this.optionArrowDriveOffsetY = 2;
            }
            if (this.optionArrowDriveOffsetY < 0 && this.optionArrowDriveOffsetY > -2) {
                this.optionArrowDriveOffsetY = -2;
            }
            this.optionArrowDriveY += this.optionArrowDriveOffsetY;
            if (this.optionArrowDriveOffsetY > 0) {
                if (this.optionArrowDriveY >= 24) {
                    this.optionArrowDriveY = 24;
                    this.optionDrawOffsetY += this.optionArrowDriveY;
                    this.optionArrowDriveY = 0;
                    this.optionArrowMoveable = false;
                }
            } else if (this.optionArrowDriveOffsetY < 0 && this.optionArrowDriveY <= -24) {
                this.optionArrowDriveY = -24;
                this.optionDrawOffsetY += this.optionArrowDriveY;
                this.optionArrowDriveY = 0;
                this.optionArrowMoveable = false;
            }
        }
        if (Key.slidesensormenuoption.isSliding()) {
            if (this.optionslide_y > 4 || this.optionslide_y < -4) {
                this.isOptionChange = true;
                this.isSelectable = false;
                releaseOptionItemsTouchKey();
                this.optionReturnFlag = false;
            } else {
                this.isSelectable = true;
            }
            if (Key.slidesensormenuoption.isSlide(Key.DIR_UP)) {
                this.isOptionChange = true;
                this.isSelectable = false;
            } else if (Key.slidesensormenuoption.isSlide(Key.DIR_DOWN)) {
                this.isOptionChange = true;
                this.isSelectable = false;
            }
        } else {
            if (this.isOptionChange && this.optionslide_y == 0) {
                this.optionDrawOffsetY = this.optionDrawOffsetTmpY1;
                this.isOptionChange = false;
                this.optionYDirect = 0;
            }
            if (!this.isOptionChange) {
                int speed;
                if (this.optionDrawOffsetY > 0) {
                    this.optionYDirect = 1;
                    speed = (-this.optionDrawOffsetY) >> 1;
                    if (speed > -2) {
                        speed = -2;
                    }
                    if (this.optionDrawOffsetY + speed <= 0) {
                        this.optionDrawOffsetY = 0;
                        this.optionYDirect = 0;
                    } else {
                        this.optionDrawOffsetY += speed;
                    }
                } else if (this.optionDrawOffsetY < this.optionDrawOffsetBottomY) {
                    this.optionYDirect = 2;
                    speed = (this.optionDrawOffsetBottomY - this.optionDrawOffsetY) >> 1;
                    if (speed < 2) {
                        speed = 2;
                    }
                    if (this.optionDrawOffsetY + speed >= this.optionDrawOffsetBottomY) {
                        this.optionDrawOffsetY = this.optionDrawOffsetBottomY;
                        this.optionYDirect = 0;
                    } else {
                        this.optionDrawOffsetY += speed;
                    }
                }
            }
        }
        if (this.isSelectable && this.optionYDirect == 0) {
            if (Key.touchmenuoptionitems[1].IsButtonPress() && this.menuOptionCursor == 0 && State.fadeChangeOver()) {
                state = 27;
                itemsSelect2Init();
                SoundSystem.getInstance().playSe(1);
            } else if (Key.touchmenuoptionitems[3].IsButtonPress() && this.menuOptionCursor == 1 && GlobalResource.soundSwitchConfig != 0 && State.fadeChangeOver()) {
                state = 39;
                soundVolumnInit();
                SoundSystem.getInstance().playSe(1);
            } else if (Key.touchmenuoptionitems[5].IsButtonPress() && this.menuOptionCursor == 2 && State.fadeChangeOver()) {
                state = 29;
                itemsSelect2Init();
                SoundSystem.getInstance().playSe(1);
            } else if (Key.touchmenuoptionitems[7].IsButtonPress() && this.menuOptionCursor == 3 && State.fadeChangeOver()) {
                state = 30;
                itemsSelect2Init();
                SoundSystem.getInstance().playSe(1);
            } else if (Key.touchmenuoptionitems[9].IsButtonPress() && this.menuOptionCursor == 4 && State.fadeChangeOver()) {
                state = 32;
                itemsSelect2Init();
                SoundSystem.getInstance().playSe(1);
            } else if (Key.touchmenuoptionitems[11].IsButtonPress() && this.menuOptionCursor == 5 && State.fadeChangeOver()) {
                if (GlobalResource.spsetConfig != 0) {
                    state = 40;
                    spSenorSetInit();
                    SoundSystem.getInstance().playSe(1);
                } else {
                    SoundSystem.getInstance().playSe(2);
                }
            } else if (Key.touchmenuoptionitems[12].IsButtonPress() && this.menuOptionCursor == 6 && State.fadeChangeOver()) {
                changeStateWithFade(34);
                helpInit();
                SoundSystem.getInstance().playSe(1);
            } else if (Key.touchmenuoptionitems[14].IsButtonPress() && this.menuOptionCursor == 7 && State.fadeChangeOver()) {
                changeStateWithFade(35);
                creditInit();
                SoundSystem.getInstance().playSe(1);
            } else if (Key.touchmenuoptionitems[16].IsButtonPress() && this.menuOptionCursor == 8 && State.fadeChangeOver()) {
                state = 36;
                secondEnsureInit();
                State.fadeInit(102, 220);
                SoundSystem.getInstance().playSe(1);
            }
        }
        this.optionslide_getprey = this.optionslide_gety;
    }

    private void releaseOptionItemsTouchKey() {
        for (TouchKeyRange resetKeyState : Key.touchmenuoptionitems) {
            resetKeyState.resetKeyState();
        }
    }

    private void optionDraw(MFGraphics g) {
        int i;
        g.setColor(0);
        MyAPI.fillRect(g, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
        muiAniDrawer.setActionId(52);
        for (int i2 = 0; i2 < (SCREEN_WIDTH / 48) + 1; i2++) {
            for (int j = 0; j < (SCREEN_HEIGHT / 48) + 1; j++) {
                muiAniDrawer.draw(g, i2 * 48, j * 48);
            }
        }
        if (state != 9) {
            this.menuOptionCursor = -2;
        }
        muiAniDrawer.setActionId(19);
        muiAniDrawer.draw(g, (SCREEN_WIDTH >> 1) - 96, ((this.optionDrawOffsetY + 40) + this.optionslide_y) + this.optionArrowDriveY);
        AnimationDrawer animationDrawer = muiAniDrawer;
        if (Key.touchmenuoptionitems[1].Isin() && this.menuOptionCursor == 0 && this.isSelectable) {
            i = 1;
        } else {
            i = 0;
        }
        animationDrawer.setActionId(i + 57);
        muiAniDrawer.draw(g, (SCREEN_WIDTH >> 1) + 56, ((this.optionDrawOffsetY + 40) + this.optionslide_y) + this.optionArrowDriveY);
        animationDrawer = muiAniDrawer;
        if (GlobalResource.difficultyConfig == 0) {
            i = 1;
        } else {
            i = 0;
        }
        animationDrawer.setActionId(i + 33);
        muiAniDrawer.draw(g, (SCREEN_WIDTH >> 1) + 56, ((this.optionDrawOffsetY + 40) + this.optionslide_y) + this.optionArrowDriveY);
        muiAniDrawer.setActionId(25);
        muiAniDrawer.draw(g, (SCREEN_WIDTH >> 1) - 96, (((this.optionDrawOffsetY + 40) + this.optionslide_y) + 24) + this.optionArrowDriveY);
        animationDrawer = muiAniDrawer;
        if (GlobalResource.soundSwitchConfig == 0) {
            i = 67;
        } else {
            i = (Key.touchmenuoptionitems[3].Isin() && this.menuOptionCursor == 1 && this.isSelectable) ? 1 : 0;
            i += 57;
        }
        animationDrawer.setActionId(i);
        muiAniDrawer.draw(g, (SCREEN_WIDTH >> 1) + 56, (((this.optionDrawOffsetY + 40) + this.optionslide_y) + 24) + this.optionArrowDriveY);
        muiAniDrawer.setActionId(GlobalResource.soundConfig + 73);
        muiAniDrawer.draw(g, (SCREEN_WIDTH >> 1) + 56, (((this.optionDrawOffsetY + 40) + this.optionslide_y) + 24) + this.optionArrowDriveY);
        muiAniDrawer.setActionId(21);
        muiAniDrawer.draw(g, (SCREEN_WIDTH >> 1) - 96, (((this.optionDrawOffsetY + 40) + this.optionslide_y) + 48) + this.optionArrowDriveY);
        animationDrawer = muiAniDrawer;
        if (Key.touchmenuoptionitems[5].Isin() && this.menuOptionCursor == 2 && this.isSelectable) {
            i = 1;
        } else {
            i = 0;
        }
        animationDrawer.setActionId(i + 57);
        muiAniDrawer.draw(g, (SCREEN_WIDTH >> 1) + 56, (((this.optionDrawOffsetY + 40) + this.optionslide_y) + 48) + this.optionArrowDriveY);
        animationDrawer = muiAniDrawer;
        if (GlobalResource.vibrationConfig == 0) {
            i = 1;
        } else {
            i = 0;
        }
        animationDrawer.setActionId(i + 35);
        muiAniDrawer.draw(g, (SCREEN_WIDTH >> 1) + 56, (((this.optionDrawOffsetY + 40) + this.optionslide_y) + 48) + this.optionArrowDriveY);
        muiAniDrawer.setActionId(22);
        muiAniDrawer.draw(g, (SCREEN_WIDTH >> 1) - 96, (((this.optionDrawOffsetY + 40) + this.optionslide_y) + 72) + this.optionArrowDriveY);
        animationDrawer = muiAniDrawer;
        if (Key.touchmenuoptionitems[7].Isin() && this.menuOptionCursor == 3 && this.isSelectable) {
            i = 1;
        } else {
            i = 0;
        }
        animationDrawer.setActionId(i + 57);
        muiAniDrawer.draw(g, (SCREEN_WIDTH >> 1) + 56, (((this.optionDrawOffsetY + 40) + this.optionslide_y) + 72) + this.optionArrowDriveY);
        muiAniDrawer.setActionId(GlobalResource.timeLimit + 35);
        muiAniDrawer.draw(g, (SCREEN_WIDTH >> 1) + 56, (((this.optionDrawOffsetY + 40) + this.optionslide_y) + 72) + this.optionArrowDriveY);
        muiAniDrawer.setActionId(23);
        muiAniDrawer.draw(g, (SCREEN_WIDTH >> 1) - 96, (((this.optionDrawOffsetY + 40) + this.optionslide_y) + 96) + this.optionArrowDriveY);
        animationDrawer = muiAniDrawer;
        i = (Key.touchmenuoptionitems[9].Isin() && this.menuOptionCursor == 4 && this.isSelectable) ? 1 : 0;
        animationDrawer.setActionId(i + 57);
        muiAniDrawer.draw(g, (SCREEN_WIDTH >> 1) + 56, (((this.optionDrawOffsetY + 40) + this.optionslide_y) + 96) + this.optionArrowDriveY);
        animationDrawer = muiAniDrawer;
        if (GlobalResource.spsetConfig == 0) {
            i = 0;
        } else {
            i = 1;
        }
        animationDrawer.setActionId(i + 37);
        muiAniDrawer.draw(g, (SCREEN_WIDTH >> 1) + 56, (((this.optionDrawOffsetY + 40) + this.optionslide_y) + 96) + this.optionArrowDriveY);
        muiAniDrawer.setActionId(24);
        muiAniDrawer.draw(g, (SCREEN_WIDTH >> 1) - 96, (((this.optionDrawOffsetY + 40) + this.optionslide_y) + SONIC_BALL_SPACE) + this.optionArrowDriveY);
        animationDrawer = muiAniDrawer;
        if (GlobalResource.spsetConfig == 0) {
            i = 67;
        } else {
            i = (Key.touchmenuoptionitems[11].Isin() && this.menuOptionCursor == 5 && this.isSelectable) ? 1 : 0;
            i += 57;
        }
        animationDrawer.setActionId(i);
        muiAniDrawer.draw(g, (SCREEN_WIDTH >> 1) + 56, (((this.optionDrawOffsetY + 40) + this.optionslide_y) + SONIC_BALL_SPACE) + this.optionArrowDriveY);
        switch (GlobalResource.sensorConfig) {
            case 0:
                muiAniDrawer.setActionId(70);
                break;
            case 1:
                muiAniDrawer.setActionId(69);
                break;
            case 2:
                muiAniDrawer.setActionId(68);
                break;
        }
        muiAniDrawer.draw(g, (SCREEN_WIDTH >> 1) + 56, (((this.optionDrawOffsetY + 40) + this.optionslide_y) + SONIC_BALL_SPACE) + this.optionArrowDriveY);
        animationDrawer = muiAniDrawer;
        if (Key.touchmenuoptionitems[12].Isin() && this.menuOptionCursor == 6 && this.isSelectable) {
            i = 1;
        } else {
            i = 0;
        }
        animationDrawer.setActionId(i + 27);
        muiAniDrawer.draw(g, (SCREEN_WIDTH >> 1) - 96, (((this.optionDrawOffsetY + 40) + this.optionslide_y) + 144) + this.optionArrowDriveY);
        animationDrawer = muiAniDrawer;
        if (Key.touchmenuoptionitems[14].Isin() && this.menuOptionCursor == 7 && this.isSelectable) {
            i = 1;
        } else {
            i = 0;
        }
        animationDrawer.setActionId(i + 29);
        muiAniDrawer.draw(g, (SCREEN_WIDTH >> 1) - 96, (((this.optionDrawOffsetY + 40) + this.optionslide_y) + 168) + this.optionArrowDriveY);
        animationDrawer = muiAniDrawer;
        if (Key.touchmenuoptionitems[16].Isin() && this.menuOptionCursor == 8 && this.isSelectable) {
            i = 1;
        } else {
            i = 0;
        }
        animationDrawer.setActionId(i + 31);
        muiAniDrawer.draw(g, (SCREEN_WIDTH >> 1) - 96, (((this.optionDrawOffsetY + 40) + this.optionslide_y) + 192) + this.optionArrowDriveY);
        if (this.optionUpArrowAvailable) {
            this.optionArrowUpDrawer.draw(g, (SCREEN_WIDTH >> 1) + Def.TOUCH_OPTION_ARROW_OFFSET_X, (SCREEN_HEIGHT >> 1) - 19);
        }
        if (this.optionDownArrowAvailable) {
            this.optionArrowDownDrawer.draw(g, (SCREEN_WIDTH >> 1) + Def.TOUCH_OPTION_ARROW_OFFSET_X, (SCREEN_HEIGHT >> 1) + 25);
        }
        this.optionOffsetX -= 4;
        this.optionOffsetX %= 100;
        muiAniDrawer.setActionId(51);
        for (int x1 = this.optionOffsetX; x1 < SCREEN_WIDTH * 2; x1 += 100) {
            muiAniDrawer.draw(g, x1, 0);
        }
        animationDrawer = muiAniDrawer;
        if (Key.touchmenuoptionreturn.Isin()) {
            i = 5;
        } else {
            i = 0;
        }
        animationDrawer.setActionId(i + 61);
        muiAniDrawer.draw(g, 0, SCREEN_HEIGHT);
        State.drawFade(g);
    }

    private int itemsid(int id) {
        int itemsidoffset = (this.optionOffsetY / 24) * 2;
        if (id + itemsidoffset < 0) {
            return 0;
        }
        if (id + itemsidoffset > 9) {
            return 9;
        }
        return id + itemsidoffset;
    }

    public void aboutInit() {
        MyAPI.initString();
        strForShow = MyAPI.getStrings(aboutStrings[0], MENU_RECT_WIDTH - 20);
    }

    public void aboutLogic() {
        boolean IsUp;
        boolean IsDown;
        Key.touchAboutInit();
        if (Key.touchhelpup.Isin() || Key.repeat(Key.gUp)) {
            IsUp = true;
        } else {
            IsUp = false;
        }
        if (Key.touchhelpdown.Isin() || Key.repeat(Key.gDown)) {
            IsDown = true;
        } else {
            IsDown = false;
        }
        MyAPI.logicString(IsDown, IsUp);
        if (Key.press(2)) {
        }
        if (Key.press(524288)) {
            changeStateWithFade(2);
            menuInit(MAIN_MENU);
            mainMenuInit();
            Key.touchAboutClose();
        }
    }

    public void aboutDraw(MFGraphics g) {
        menuBgDraw(g);
        drawMenuTitle(g, 7, 0);
        State.fillMenuRect(g, FRAME_X, 30, FRAME_WIDTH, FRAME_HEIGHT);
        g.setColor(0);
        MyAPI.drawBoldStrings(g, strForShow, FRAME_X + 10, 38, MENU_RECT_WIDTH - 20, FRAME_HEIGHT - 16, MapManager.END_COLOR, 4656650, 0);
    }

    private void rankingInit() {
        StageManager.normalHighScoreInit();
    }

    private void rankingLogic() {
        switch (comfirmLogic()) {
            case 400:
                changeStateWithFade(2);
                menuInit(MAIN_MENU);
                mainMenuInit();
                StageManager.drawHighScoreEnd();
                return;
            default:
                return;
        }
    }

    private void rankingDraw(MFGraphics g) {
        menuBgDraw(g);
        for (int i = 0; i < (SCREEN_WIDTH / 32) + 1; i++) {
            State.drawMenuFontById(g, 111, i * 32, 0);
            State.drawMenuFontById(g, 112, (i * 32) - 1, SCREEN_HEIGHT);
        }
        drawMenuTitle(g, 4, 0);
        StageManager.drawNormalHighScore(g);
    }

    private void gameover_rankingLogic() {
        this.timecount_ranking++;
        switch (comfirmLogic()) {
            case 400:
                changeStateWithFade(2);
                menuInit(MAIN_MENU);
                mainMenuInit();
                this.returnCursor = 0;
                StageManager.drawHighScoreEnd();
                return;
            default:
                return;
        }
    }

    private void gameover_rankingDraw(MFGraphics g) {
        menuBgDraw(g);
        for (int i = 0; i < (SCREEN_WIDTH / 32) + 1; i++) {
            State.drawMenuFontById(g, 111, i * 32, 0);
            State.drawMenuFontById(g, 112, i * 32, SCREEN_HEIGHT);
        }
        drawMenuTitle(g, 4, 0);
        StageManager.drawNormalHighScore(g);
    }

    private void initStageSelet() {
        this.stageItemNumForShow = getAvailableItemNum();
        this.stageDrawEndY = (this.stageDrawStartY + (this.stageItemNumForShow * ITEM_SPACE)) - (ITEM_SPACE >> 1);
        this.stageStartIndex = 0;
        this.stageDrawOffsetY = 0;
        this.offsetY = new int[this.STAGE_TOTAL_NUM];
        this.vY = new int[this.STAGE_TOTAL_NUM];
        this.offsetY[0] = (SCREEN_HEIGHT >> 1) - 72;
        this.vY[0] = 4;
        for (int i = 1; i < this.STAGE_TOTAL_NUM; i++) {
            this.offsetY[i] = this.offsetY[i - 1] * 2;
            this.vY[i] = this.vY[i - 1] * 2;
        }
        this.stage_select_state = 0;
        this.optionMenuCursor = 0;
    }

    private int getAvailableItemNum() {
        int num = (((SCREEN_HEIGHT - INTERVAL_FOR_RECORD_BAR) - MENU_TITLE_DRAW_OFFSET_Y) - INTERVAL_ABOVE_RECORD_BAR) / ITEM_SPACE;
        if (num % 2 != 0) {
            if (num < 2) {
                num++;
            } else {
                num--;
            }
        }
        if (num > StageManager.STAGE_NUM) {
            if (StageManager.STAGE_NUM < 8) {
                this.stageDrawStartY = (MENU_TITLE_DRAW_OFFSET_Y + (MENU_SPACE >> 1)) + (((((SCREEN_HEIGHT - INTERVAL_FOR_RECORD_BAR) - MENU_TITLE_DRAW_OFFSET_Y) - INTERVAL_ABOVE_RECORD_BAR) - (StageManager.STAGE_NUM * ITEM_SPACE)) >> 1);
            } else {
                this.stageDrawStartY = 72;
            }
            return StageManager.STAGE_NUM;
        }
        this.stageDrawStartY = (MENU_TITLE_DRAW_OFFSET_Y + (MENU_SPACE >> 1)) + (ITEM_SPACE >> 1);
        return num;
    }

    public void stageSelectDraw(MFGraphics g, int type) {
        int i;
        menuBgDraw(g);
        this.stageDrawOffsetY = MyAPI.calNextPosition((double) this.stageDrawOffsetY, (double) ((-this.stageStartIndex) * ITEM_SPACE), 1, 2);
        if (this.stageItemNumForShow != StageManager.STAGE_NUM) {
            MyAPI.setClip(g, 0, this.stageDrawStartY - (ITEM_SPACE >> 1), SCREEN_WIDTH, this.stageItemNumForShow * ITEM_SPACE);
        }
        for (i = 0; i < StageManager.STAGE_NAME.length; i++) {
            if (i == this.optionMenuCursor && this.stage_select_state == 1) {
                g.setColor(16711680);
            } else {
                g.setColor(0);
            }
            MyAPI.drawString(g, "stage" + StageManager.STAGE_NAME[i], SCREEN_WIDTH >> 1, (this.stageDrawOffsetY + (i * 20)) + 20, 17);
        }
        MyAPI.setClip(g, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
        if (type == 0) {
            for (i = 0; i < (SCREEN_HEIGHT / 96) + 1; i++) {
                State.drawMenuFontById(g, 104, 0, i * 96);
            }
            drawMenuTitle(g, 1, 5, 0);
        } else if (type == 1) {
            for (i = 0; i < (SCREEN_HEIGHT / 96) + 2; i++) {
                State.drawMenuFontById(g, GimmickObject.GIMMICK_NUM, 0, i * 96);
            }
            drawMenuTitle(g, 2, 5, 0);
        }
        this.STAGE_SEL_ARROW_UP_X = (SCREEN_WIDTH >> 1) - 64;
        this.STAGE_SEL_ARROW_UP_Y = (SCREEN_HEIGHT >> 1) - 48;
        this.STAGE_SEL_ARROW_DOWN_X = (SCREEN_WIDTH >> 1) - 64;
        this.STAGE_SEL_ARROW_DOWN_Y = (SCREEN_HEIGHT >> 1) + 48;
    }

    private void drawStageName(MFGraphics g, int stageId, int type, int offsetX, int mOffsetY) {
        State.drawMenuFontById(g, 109, COMFIRM_X, ((this.stageDrawStartY + (ITEM_SPACE * stageId)) + this.offsetY[stageId]) + mOffsetY);
        State.drawMenuFontById(g, (type + 2) + (stageId % 2), (COMFIRM_X + 49) + offsetX, ((this.stageDrawStartY + (ITEM_SPACE * stageId)) + this.offsetY[stageId]) + mOffsetY);
        State.drawMenuFontById(g, type + 1, (COMFIRM_X + 26) + offsetX, ((this.stageDrawStartY + (ITEM_SPACE * stageId)) + this.offsetY[stageId]) + mOffsetY);
        State.drawMenuFontById(g, ((stageId >> 1) + type) + 2, (COMFIRM_X + 1) + offsetX, ((this.stageDrawStartY + (ITEM_SPACE * stageId)) + this.offsetY[stageId]) + mOffsetY);
        State.drawMenuFontById(g, type, (COMFIRM_X - 22) + offsetX, ((this.stageDrawStartY + (ITEM_SPACE * stageId)) + this.offsetY[stageId]) + mOffsetY);
    }

    private void drawRecordtimeScroll(MFGraphics g, int id, int y, int timeCount, int speed, int space) {
        State.drawBar(g, 0, y);
        this.itemOffsetX += speed;
        this.itemOffsetX %= space;
        int x = 0;
        while (x - this.itemOffsetX > 0) {
            x -= space;
        }
        int drawNum = (((SCREEN_WIDTH + space) - 1) / space) + 2;
        for (int i = 0; i < drawNum; i++) {
            int x2 = x + (i * space);
            State.drawMenuFontById(g, id, x2 - this.itemOffsetX, y);
            drawRecordtime(g, timeCount, (x2 - this.itemOffsetX) + FONT_WIDTH, y);
        }
    }

    private void drawRecordtime(MFGraphics g, int timeCount, int x, int y) {
        PlayerObject.drawRecordTimeLeft(g, timeCount, x, y);
    }

    private void drawTimeNum(MFGraphics g, int num, int x, int y, int blockNum) {
        int i;
        int divideNum = 1;
        for (i = 1; i < blockNum; i++) {
            divideNum *= 10;
        }
        for (i = 0; i < blockNum; i++) {
            divideNum /= 10;
            State.drawMenuFontById(g, (Math.abs(num / divideNum) % 10) + 37, (i * 8) + x, y);
        }
    }

    public void pause() {
        if (state != 44) {
            if (state == 16) {
                state = 16;
                this.nextState = 16;
                interruptInit();
                return;
            }
            if (this.fadeChangeState && this.nextState != state) {
                state = this.nextState;
                this.fadeChangeState = false;
                if (this.IsFromStageSelect) {
                    State.fadeInit(255, 102);
                    this.IsFromStageSelect = false;
                } else if (this.IsFromOptionItems) {
                    State.fadeInit(255, 220);
                    this.IsFromOptionItems = false;
                } else {
                    State.fadeInit(255, 0);
                }
            }
            this.interrupt_state = state;
            state = 16;
            this.nextState = 16;
            interruptInit();
            Key.touchInterruptInit();
            Standard.pause();
            Key.touchkeyboardInit();
        }
    }

    private void interruptInit() {
        if (this.interruptDrawer == null) {
            this.interruptDrawer = Animation.getInstanceFromQi("/animation/utl_res/suspend_resume.dat")[0].getDrawer(0, true, 0);
        }
        IsInInterrupt = true;
        lastFading = fading;
        fading = false;
    }

    public void interruptLogic() {
        SoundSystem.getInstance().stopBgm(false);
        if (Key.press(2)) {
        }
        if (Key.press(524288) || (Key.touchinterruptreturn != null && Key.touchinterruptreturn.IsButtonPress())) {
            SoundSystem.getInstance().playSe(2);
            Key.touchInterruptClose();
            Key.touchkeyboardClose();
            MFGamePad.resetKeys();
            if (Key.touchitemsselect2_1 != null) {
                Key.touchitemsselect2_1.reset();
            }
            if (Key.touchitemsselect2_2 != null) {
                Key.touchitemsselect2_2.reset();
            }
            if (Key.touchitemsselect3_1 != null) {
                Key.touchitemsselect3_1.reset();
            }
            if (Key.touchitemsselect3_2 != null) {
                Key.touchitemsselect3_2.reset();
            }
            if (Key.touchitemsselect3_3 != null) {
                Key.touchitemsselect3_3.reset();
            }
            Standard.resume();
            state = this.interrupt_state;
            switch (this.interrupt_state) {
                case 2:
                    State.fadeInit(0, 0);
                    break;
                case 3:
                    this.interrupt_state = 38;
                    state = 38;
                    this.isTitleBGMPlay = true;
                    State.setFadeColor(MapManager.END_COLOR);
                    State.fadeInit(255, 0);
                    initTitleRes();
                    SoundSystem.getInstance().playBgm(1, false);
                    break;
                case 6:
                case 8:
                    SoundSystem.getInstance().playBgm(4);
                    break;
                case 9:
                    SoundSystem.getInstance().playBgm(5);
                    break;
                case 14:
                    State.fadeInit(0, 0);
                    SoundSystem.getInstance().playBgm(3);
                    break;
                case 23:
                    SoundSystem.getInstance().playBgm(2);
                    break;
                case 24:
                    State.fadeInit(0, 0);
                    initTimeStageRes();
                    break;
                case 25:
                    State.fadeInit(0, 0);
                    this.characterRecordDisFlag = false;
                    break;
                case 27:
                case 28:
                case 29:
                case 30:
                case 31:
                case 32:
                case 33:
                case 36:
                case 37:
                case 39:
                case 40:
                    SoundSystem.getInstance().playBgm(5);
                    this.IsFromOptionItems = true;
                    break;
                case 34:
                    SoundSystem.getInstance().playBgm(5);
                    break;
                case 35:
                    SoundSystem.getInstance().playBgm(33);
                    break;
            }
            switch (this.interrupt_state) {
                case 0:
                    Key.touchsoftkeyInit();
                    break;
                case 1:
                    Key.touchanykeyInit();
                    break;
                default:
                    Key.touchkeyboardInit();
                    break;
            }
            IsInInterrupt = false;
            this.IsFromStageSelect = false;
            this.IsFromOptionItems = false;
            Key.clear();
        }
    }

    public void interruptDraw(MFGraphics g) {
        this.interruptDrawer.setActionId((Key.touchinterruptreturn.Isin() ? 1 : 0) + 0);
        this.interruptDrawer.draw(g, SCREEN_WIDTH >> 1, SCREEN_HEIGHT >> 1);
    }

    private void openingInit() {
        close();
        this.openingFrame = 0;
        this.openingState = (byte) 0;
        int i;
        if (this.openingAnimation == null) {
            this.openingAnimation = Animation.getInstanceFromQi("/animation/opening/opening.dat");
            this.openingDrawer = new AnimationDrawer[this.openingAnimation.length];
            for (i = 0; i < this.openingDrawer.length; i++) {
                this.openingDrawer[i] = this.openingAnimation[i].getDrawer(0, false, 0);
                this.openingDrawer[i].mustKeepFrameTime(63);
            }
        } else {
            for (i = 0; i < this.openingDrawer.length; i++) {
                this.openingDrawer[i].setActionId(0);
                this.openingDrawer[i].restart();
            }
        }
        if (this.skipDrawer == null) {
            this.skipDrawer = new Animation("/animation/skip").getDrawer(0, false, 0);
        }
        this.openingStateChanging = false;
    }

    private void openingClose() {
        Animation.closeAnimationArray(this.openingAnimation);
        this.openingAnimation = null;
        Animation.closeAnimationDrawerArray(this.openingDrawer);
        this.openingDrawer = null;
        Animation.closeAnimationDrawer(this.skipDrawer);
        this.skipDrawer = null;
        System.gc();
        try {
            Thread.sleep(100);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean openingLogic() {
        this.openingFrame++;
        if (Key.touchopeningskip.Isin() && Key.touchopening.IsClick()) {
            this.opengingCursor = 0;
        }
        if ((Key.touchopeningskip.IsButtonPress() || Key.press(Key.B_S1)) && !this.openingDrawer[4].checkEnd()) {
            SoundSystem.getInstance().playSe(1);
            SoundSystem.getInstance().stopBgm(false);
            SoundSystem.getInstance().playBgm(1, false);
            return true;
        }
        if (this.openingCount > 0) {
            this.openingCount--;
        }
        switch (this.openingState) {
            case OPENING_STATE_EMERALD:
                if (this.openingDrawer[0].checkEnd()) {
                    this.openingDrawer[0].setActionId(1);
                    this.openingState = OPENING_STATE_EMERALD_SHINING;
                    SoundSystem.getInstance().playSequenceSe(80);
                }
                break;
            case OPENING_STATE_EMERALD_SHINING:
                if (this.openingDrawer[0].checkEnd()) {
                    this.openingDrawer[1].setActionId(0);
                    this.openingState = OPENING_STATE_SONIC;
                }
                break;
            case OPENING_STATE_SONIC:
                if (this.openingDrawer[1].checkEnd()) {
                    this.openingDrawer[2].setActionId(0);
                    if (this.openingStateChanging) {
                        if (State.fadeChangeOver()) {
                            this.openingState = OPENING_STATE_TAILS;
                            this.openingStateChanging = false;
                        }
                    }
                    this.openingStateChanging = true;
                    State.setFadeColor(MapManager.END_COLOR);
                    State.fadeInit(0, 255);
                }
                break;
            case OPENING_STATE_TAILS:
                if (this.openingDrawer[2].checkEnd()) {
                    this.openingDrawer[3].setActionId(0);
                    if (this.openingStateChanging) {
                        if (State.fadeChangeOver()) {
                            this.openingState = OPENING_STATE_KNUCKLES;
                            this.openingStateChanging = false;
                        }
                    }
                    this.openingStateChanging = true;
                    State.setFadeColor(MapManager.END_COLOR);
                    State.fadeInit(0, 255);
                }
                break;
            case OPENING_STATE_KNUCKLES:
                if (this.openingDrawer[3].checkEnd()) {
                    this.openingDrawer[4].setActionId(0);
                    this.openingDrawer[4].restart();
                    this.openingEnding = false;
                    if (this.openingStateChanging) {
                        if (State.fadeChangeOver()) {
                            this.openingState = OPENING_STATE_AMY;
                            this.openingStateChanging = false;
                        }
                    }
                    this.openingStateChanging = true;
                    State.setFadeColor(MapManager.END_COLOR);
                    State.fadeInit(0, 255);
                }
                break;
            case OPENING_STATE_AMY:
                if (this.openingDrawer[4].checkEnd() && this.openingEnding && State.fadeChangeOver()) {
                    this.openingState = OPENING_STATE_END;
                }
                break;
            case OPENING_STATE_END:
                this.openingFrame = 0;
                return true;
        }
        return false;
    }

    private void openingDraw(MFGraphics g) {
        switch (this.openingState) {
            case (byte) 0:
            case (byte) 1:
                this.openingDrawer[0].draw(g, this.openingOffsetX, this.openingOffsetY);
                break;
            case (byte) 2:
                this.openingDrawer[1].draw(g, this.openingOffsetX, this.openingOffsetY);
                break;
            case (byte) 3:
                this.openingDrawer[2].draw(g, this.openingOffsetX, this.openingOffsetY);
                break;
            case (byte) 4:
                this.openingDrawer[3].draw(g, this.openingOffsetX, this.openingOffsetY);
                break;
            case (byte) 5:
                this.openingDrawer[4].draw(g, this.openingOffsetX, this.openingOffsetY);
                if (!this.openingEnding && this.openingDrawer[4].checkEnd()) {
                    this.openingEnding = true;
                    State.setFadeColor(MapManager.END_COLOR);
                    State.fadeInit(0, 255);
                }
                if (this.openingEnding) {
                    State.drawFadeBase(g, 2);
                }
                break;
        }
        if (this.openingStateChanging) {
            State.drawFadeBase(g, 2);
        }
        if (!this.openingDrawer[4].checkEnd()) {
            int i;
            AnimationDrawer animationDrawer = this.skipDrawer;
            if (Key.touchopeningskip.Isin() && this.opengingCursor == 0) {
                i = 1;
            } else {
                i = 0;
            }
            animationDrawer.setActionId(i + 0);
            this.skipDrawer.draw(g, 0, SCREEN_HEIGHT);
        }
    }

    private void initTitleRes() {
        if (titleLeftImage == null) {
            MFDevice.enableLayer(-1);
            titleLeftImage = MFImage.createImage("/title/title_left.png");
            titleRightImage = MFImage.createImage("/title/title_right.png");
            titleSegaImage = MFImage.createImage("/title/title_sega.png");
            if (this.titleAni == null) {
                this.titleAni = Animation.getInstanceFromQi("/animation/utl_res/title.dat");
                this.titleAniDrawer = this.titleAni[0].getDrawer(0, true, 0);
            }
            this.titleFrame = 0;
            Key.touchMainMenuInit2();
        }
    }

    private void initTitleRes2() {
        if (this.titleAni == null) {
            this.titleAni = Animation.getInstanceFromQi("/animation/utl_res/title.dat");
            this.titleAniDrawer = this.titleAni[0].getDrawer(0, true, 0);
        }
        this.titleFrame = 5;
        Key.touchMainMenuInit2();
    }

    private void drawTitleBg(MFGraphics g) {
        g.setColor(0);
        this.titleAniDrawer.setActionId(0);
        if (state == 1) {
            if ((System.currentTimeMillis() / 500) % 2 == 0) {
                this.titleAniDrawer.setActionId(2);
                this.titleAniDrawer.draw(g, SCREEN_WIDTH >> 1, (SCREEN_HEIGHT >> 1) + 33);
            }
        } else if (state == 12 && this.quitFlag == 1) {
            this.titleAniDrawer.setActionId(2);
            this.titleAniDrawer.draw(g, SCREEN_WIDTH >> 1, (SCREEN_HEIGHT >> 1) + 33);
        }
        drawSegaLogo(g);
        drawTitleName(g);
    }

    private void drawMainMenuNormal(MFGraphics g) {
        if (state == 2) {
            this.isAtMainMenu = true;
        } else {
            this.isAtMainMenu = false;
        }
        AnimationDrawer animationDrawer = this.titleAniDrawer;
        int i = this.isAtMainMenu ? (Key.touchmainmenustart.Isin() && this.cursor == 0) ? 1 : 0 : 0;
        animationDrawer.setActionId(i + 3);
        this.titleAniDrawer.draw(g, SCREEN_WIDTH >> 1, (SCREEN_HEIGHT >> 1) + 12);
        animationDrawer = this.titleAniDrawer;
        i = this.isAtMainMenu ? (Key.touchmainmenurace.Isin() && this.cursor == 1) ? 1 : 0 : 0;
        animationDrawer.setActionId(i + 5);
        this.titleAniDrawer.draw(g, SCREEN_WIDTH >> 1, (SCREEN_HEIGHT >> 1) + 32);
        animationDrawer = this.titleAniDrawer;
        i = this.isAtMainMenu ? (Key.touchmainmenuoption.Isin() && this.cursor == 2) ? 1 : 0 : 0;
        animationDrawer.setActionId(i + 7);
        this.titleAniDrawer.draw(g, SCREEN_WIDTH >> 1, (SCREEN_HEIGHT >> 1) + 52);
        animationDrawer = this.titleAniDrawer;
        i = this.isAtMainMenu ? (Key.touchmainmenuend.Isin() && this.cursor == 3) ? 1 : 0 : 0;
        animationDrawer.setActionId(i + 9);
        this.titleAniDrawer.draw(g, SCREEN_WIDTH >> 1, (SCREEN_HEIGHT >> 1) + 72);
        if (muiAniDrawer == null) {
            muiAniDrawer = new Animation("/animation/mui").getDrawer(0, false, 0);
            return;
        }
        animationDrawer = muiAniDrawer;
        if (Key.touchmainmenureturn.Isin()) {
            i = 5;
        } else {
            i = 0;
        }
        animationDrawer.setActionId(i + 61);
        muiAniDrawer.draw(g, 0, SCREEN_HEIGHT);
    }

    private void drawMainMenuMultiItems(MFGraphics g) {
        if (state == 2) {
            this.isAtMainMenu = true;
        } else {
            this.isAtMainMenu = false;
        }
        this.degree = MyAPI.calNextPosition((double) this.degree, 0.0d, 1, 3);
        if (this.degree == 0) {
            this.menuMoving = false;
        }
        this.titleAniDrawer.setActionId(14);
        this.titleAniDrawer.draw(g, SCREEN_WIDTH >> 1, (SCREEN_HEIGHT >> 1) + 40);
        int startCursor = (((this.mainMenuItemCursor - 1) - 1) + this.currentElement.length) % this.currentElement.length;
        for (int i = 0; i < 4; i++) {
            int elementId = (startCursor + i) % this.currentElement.length;
            int y = (this.degree + 20) + ((i - 1) * 20);
            if (y >= 10 && y <= 70) {
                int i2;
                AnimationDrawer animationDrawer = this.titleAniDrawer;
                int i3 = this.currentElement[elementId];
                if (this.mainMenuEnsureFlag && this.mainMenuItemCursor == elementId) {
                    i2 = 1;
                } else {
                    i2 = 0;
                }
                animationDrawer.setActionId(i3 + i2);
                this.titleAniDrawer.draw(g, SCREEN_WIDTH >> 1, (SCREEN_HEIGHT >> 1) + y);
            }
        }
        this.titleAniDrawer.setActionId(15);
        this.titleAniDrawer.draw(g, 0, SCREEN_HEIGHT >> 1);
        drawSegaLogo(g);
        drawTitleName(g);
        this.titleAniDrawer.setActionId(16);
        this.titleAniDrawer.draw(g, SCREEN_WIDTH >> 1, (SCREEN_HEIGHT >> 1) + 40);
        if (muiAniDrawer == null) {
            muiAniDrawer = new Animation("/animation/mui").getDrawer(0, false, 0);
            return;
        }
        AnimationDrawer animationDrawer = muiAniDrawer;
        int i3;
        if (Key.touchmainmenureturn.Isin()) {
            i3 = 5;
        } else {
            i3 = 0;
        }
        animationDrawer.setActionId(i3 + 61);
        muiAniDrawer.draw(g, 0, SCREEN_HEIGHT);
    }

    private void drawMainMenu(MFGraphics g) {
        drawMainMenuNormal(g);
    }

    private void initCharacterSelectRes() {
        if (this.charSelAni == null || this.charSelFilAni == null) {
            this.charSelAni = Animation.getInstanceFromQi("/animation/utl_res/character_select.dat");
            this.charSelAniDrawer = this.charSelAni[0].getDrawer(0, false, 0);
            this.charSelCaseDrawer = this.charSelAni[0].getDrawer(3, false, 0);
            this.charSelRoleDrawer = this.charSelAni[0].getDrawer(4, false, 0);
            this.charSelArrowDrawer = this.charSelAni[0].getDrawer(13, true, 0);
            this.charSelTitleDrawer = this.charSelAni[0].getDrawer(14, true, 0);
            this.charSelFilAni = Animation.getInstanceFromQi("/animation/utl_res/character_select_filter.dat");
            this.charSelFilAniDrawer = this.charSelFilAni[0].getDrawer(0, false, 0);
        }
        this.charSelAniDrawer.setActionId(0);
        this.charSelCaseDrawer.setActionId(3);
        this.charSelRoleDrawer.setActionId(4);
        this.charSelArrowDrawer.setActionId(13);
        this.charSelTitleDrawer.setActionId(14);
        this.charSelFilAniDrawer.setActionId(0);
        this.charSelAniDrawer.restart();
        this.charSelCaseDrawer.restart();
        this.charSelRoleDrawer.restart();
        this.charSelArrowDrawer.restart();
        this.charSelTitleDrawer.restart();
        this.charSelFilAniDrawer.restart();
        this.character_sel_frame_cnt = 0;
        this.character_id = 0;
        this.character_move = false;
        this.character_arrow_display = true;
        this.character_outer = false;
        this.cursor = 0;
        this.character_sel_offset_x = 0;
        this.character_idchangeFlag = false;
        this.character_offset_state = 0;
        Key.touchCharacterSelectModeInit();
        this.returnCursor = 0;
    }

    private void characterSelectLogic() {
        // Select sonic ...
        PlayerObject.setCharacter(PlayerObject.CHARACTER_SONIC);
        if (StageManager.getStageID() == 0 && StageManager.getOpenedStageId() == 0 && GameObject.stageModeState == 0) {
            StageManager.setStageID(0);
            StageManager.setStartStageID(0);
            State.setState(1);
            PlayerObject.resetGameParam();
            StageManager.saveStageRecord();
        } else {
            changeStateWithFade(14);
            preStageSelectState = 23;
            menuInit(this.STAGE_TOTAL_NUM);
            initStageSelectRes();
        }
        Key.touchCharacterSelectModeClose();
    }

    private void buggedCharacterSelectLogic() {
        this.character_sel_frame_cnt++;
        if (this.character_sel_frame_cnt == 1) {
            this.charSelAniDrawer.setActionId(0);
            this.arrowPressState = 0;
            SoundSystem.getInstance().playBgm(2);
        }
        if (this.character_sel_frame_cnt == 4) {
            this.charSelCaseDrawer.setLoop(false);
        }
        if (this.character_sel_frame_cnt == 9) {
            this.charSelRoleDrawer.setLoop(false);
        }
        if (this.character_sel_frame_cnt == 10) {
            this.charSelFilAniDrawer.setLoop(false);
        }
        if (this.character_sel_frame_cnt == 16) {
            this.charSelCaseDrawer.setActionId(15);
            this.charSelRoleDrawer.setActionId(28);
        }
        if (this.character_sel_frame_cnt <= 16) {
            this.returnCursor = 0;
        } else if (!this.character_move) {
            if (Key.touchcharsel != null && Key.touchcharselselect.Isin() && Key.touchcharsel.IsClick()) {
                this.cursor = 2;
                this.returnCursor = 0;
            }
            if (Key.touchcharsel != null && Key.touchcharselleftarrow.Isin() && Key.touchcharsel.IsClick()) {
                this.cursor = 3;
                this.returnCursor = 0;
            }
            if (Key.touchcharsel != null && Key.touchcharselrightarrow.Isin() && Key.touchcharsel.IsClick()) {
                this.cursor = 4;
                this.returnCursor = 0;
            }
            if (Key.touchcharselreturn != null && Key.touchcharsel.IsClick() && Key.touchcharselreturn.Isin()) {
                this.returnCursor = 1;
            }
            if (!this.character_outer) {
                if (Key.touchcharselleftarrow.IsButtonPress() && this.cursor == 3) {
                    this.arrowPressState = 1;
                    this.character_offset_state = 1;
                    Key.touchcharselleftarrow.resetKeyState();
                }
                if (Key.touchcharselrightarrow.IsButtonPress() && this.cursor == 4) {
                    this.arrowPressState = 2;
                    this.character_offset_state = 2;
                    Key.touchcharselrightarrow.resetKeyState();
                }
                if (Key.slidesensorcharsel.isSliding()) {
                    boolean idChanged = false;
                    if (this.arrowPressState == 0) {
                        if (!Key.slidesensorcharsel.isSlide(Key.DIR_LEFT)) {
                            if (Key.slidesensorcharsel.isSlide(Key.DIR_RIGHT)) {
                                switch (this.character_offset_state) {
                                    case 0:
                                        this.character_id--;
                                        this.character_circleturnright = false;
                                        idChanged = true;
                                        this.character_offset_state = 2;
                                        break;
                                    case 1:
                                        this.character_id--;
                                        this.character_circleturnright = false;
                                        idChanged = true;
                                        this.character_offset_state = 0;
                                        break;
                                    case 2:
                                        this.character_offset_state = 0;
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                        switch (this.character_offset_state) {
                            case 0:
                                this.character_id++;
                                this.character_circleturnright = true;
                                idChanged = true;
                                this.character_offset_state = 1;
                                break;
                            case 1:
                                this.character_offset_state = 0;
                                break;
                            case 2:
                                this.character_id++;
                                this.character_circleturnright = true;
                                idChanged = true;
                                this.character_offset_state = 0;
                                break;
                        }
                        switch (this.character_offset_state) {
                            case 0:
                                this.character_sel_offset_x = Key.slidesensorcharsel.getOffsetX();
                                break;
                            case 1:
                                this.character_sel_offset_x = Key.slidesensorcharsel.getOffsetX() + 64;
                                break;
                            case 2:
                                this.character_sel_offset_x = Key.slidesensorcharsel.getOffsetX() - 64;
                                break;
                        }
                        if (idChanged) {
                            this.character_id += PlayerObject.CHARACTER_LIST.length;
                            this.character_id %= PlayerObject.CHARACTER_LIST.length;
                            this.charSelRoleDrawer.setLoop(false);
                            this.charSelRoleDrawer.setActionId(this.character_id + 28);
                            this.character_idchangeFlag = true;
                            if (this.character_id != this.character_preid) {
                                this.character_reback = true;
                            } else {
                                this.character_reback = false;
                            }
                        }
                    }
                } else {
                    this.character_offset_state = 0;
                    if (this.arrowPressState == 0) {
                        this.character_sel_offset_x = MyAPI.calNextPosition((double) this.character_sel_offset_x, 0.0d, 1, 2);
                    } else {
                        if (this.arrowPressState == 1) {
                            this.character_sel_offset_x = MyAPI.calNextPosition((double) this.character_sel_offset_x, 128.0d, 1, 2);
                            if (this.character_sel_offset_x == 128) {
                                this.character_id--;
                                this.character_circleturnright = false;
                                this.character_id += PlayerObject.CHARACTER_LIST.length;
                                this.character_id %= PlayerObject.CHARACTER_LIST.length;
                                this.charSelRoleDrawer.setLoop(false);
                                this.charSelRoleDrawer.setActionId(this.character_id + 28);
                                this.character_idchangeFlag = true;
                                this.arrowPressState = 0;
                                this.character_reback = true;
                                this.character_sel_offset_x = 0;
                            }
                        }
                        if (this.arrowPressState == 2) {
                            this.character_sel_offset_x = MyAPI.calNextPosition((double) this.character_sel_offset_x, -128.0d, 1, 2);
                            if (this.character_sel_offset_x == Def.TOUCH_HELP_LEFT_X) {
                                this.character_id++;
                                this.character_circleturnright = true;
                                this.character_id += PlayerObject.CHARACTER_LIST.length;
                                this.character_id %= PlayerObject.CHARACTER_LIST.length;
                                this.charSelRoleDrawer.setLoop(false);
                                this.charSelRoleDrawer.setActionId(this.character_id + 28);
                                this.character_idchangeFlag = true;
                                this.arrowPressState = 0;
                                this.character_reback = true;
                                this.character_sel_offset_x = 0;
                            }
                        }
                    }
                    if (this.character_idchangeFlag && this.character_sel_offset_x == 0) {
                        if (this.character_reback) {
                            int i;
                            this.character_move = true;
                            AnimationDrawer animationDrawer = this.charSelAniDrawer;
                            if (this.character_circleturnright) {
                                i = 1;
                            } else {
                                i = 2;
                            }
                            animationDrawer.setActionId(i);
                            this.charSelAniDrawer.restart();
                        }
                        this.charSelFilAniDrawer.setActionId(this.character_id + 1);
                        this.character_idchangeFlag = false;
                        Key.touchcharselselect.reset();
                        SoundSystem.getInstance().playSe(0);
                    }
                    boolean charselKey = Key.touchcharselselect.IsButtonPress();

                    if (!this.character_idchangeFlag && this.character_sel_offset_x < 8 && this.character_sel_offset_x > INTERGRADE_RECORD_STAGE_NAME_SPEED && charselKey && this.cursor == 2) {
                        SoundSystem.getInstance().playSe(1);
                        this.charSelTitleDrawer.setActionId(27);
                        this.character_arrow_display = false;
                        this.charSelRoleDrawer.setActionId(this.character_id + 9);
                        this.character_outer = true;
                        this.character_sel_offset_x = 0;
                    }
                    this.character_preid = this.character_id;
                }
                if (this.character_sel_offset_x == 0 && ((Key.press(524288) || (Key.touchcharselreturn.IsButtonPress() && this.returnCursor == 1)) && State.fadeChangeOver())) {
                    changeStateWithFade(this.preCharaterSelectState);
                    switch (this.preCharaterSelectState) {
                        case 2:
                            this.isTitleBGMPlay = false;
                            Key.touchMainMenuInit2();
                            initTitleRes2();
                            SoundSystem.getInstance().stopBgm(false);
                            break;
                        case 24:
                            Key.touchProRaceModeInit();
                            initTimeStageRes();
                            break;
                    }
                    SoundSystem.getInstance().playSe(2);
                }
            }
            if (this.character_outer && this.charSelRoleDrawer.checkEnd()) {
                PlayerObject.setCharacter(this.character_id);
                if (StageManager.getStageID() == 0 && StageManager.getOpenedStageId() == 0 && GameObject.stageModeState == 0) {
                    StageManager.setStageID(0);
                    StageManager.setStartStageID(0);
                    State.setState(1);
                    PlayerObject.resetGameParam();
                    StageManager.saveStageRecord();
                } else {
                    changeStateWithFade(14);
                    preStageSelectState = 23;
                    menuInit(this.STAGE_TOTAL_NUM);
                    initStageSelectRes();
                }
                Key.touchCharacterSelectModeClose();
            }
        }
    }

    private void drawCharacterSelect(MFGraphics g) {
        g.setColor(MapManager.END_COLOR);
        MyAPI.fillRect(g, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
        this.charSelAniDrawer.draw(g, SCREEN_WIDTH >> 1, SCREEN_HEIGHT >> 1);
        if (this.character_sel_frame_cnt > 4 && this.character_sel_frame_cnt <= 16) {
            this.charSelCaseDrawer.draw(g, SCREEN_WIDTH >> 1, SCREEN_HEIGHT >> 1);
            if (this.character_sel_frame_cnt == 16) {
                this.charSelCaseDrawer.setLoop(true);
            }
        }
        if (this.character_sel_frame_cnt > 9) {
            this.charSelRoleDrawer.draw(g, (SCREEN_WIDTH >> 1) + this.character_sel_offset_x, SCREEN_HEIGHT >> 1);
        }
        if (this.character_sel_frame_cnt > 16) {
            if (this.character_arrow_display) {
                this.charSelArrowDrawer.draw(g, SCREEN_WIDTH >> 1, SCREEN_HEIGHT >> 1);
            }
            this.charSelTitleDrawer.draw(g, SCREEN_WIDTH >> 1, SCREEN_HEIGHT >> 1);
            this.charSelCaseDrawer.draw(g, SCREEN_WIDTH >> 1, SCREEN_HEIGHT >> 1);
            if (this.character_move && this.charSelCaseDrawer.checkEnd()) {
                this.charSelCaseDrawer.setLoop(false);
                this.charSelCaseDrawer.setActionId((this.character_id * 3) + 17);
                this.character_move = false;
            }
        }
        if (muiAniDrawer == null) {
            muiAniDrawer = new Animation("/animation/mui").getDrawer(0, false, 0);
        } else {
            if (Key.touchcharselreturn != null) {
                int i;
                AnimationDrawer animationDrawer = muiAniDrawer;
                if (Key.touchcharselreturn.Isin()) {
                    i = 5;
                } else {
                    i = 0;
                }
                animationDrawer.setActionId(i + 61);
            }
            muiAniDrawer.draw(g, 0, SCREEN_HEIGHT);
        }
        State.drawFade(g);
    }

    private void initTimeStageRes() {
        if (this.timeAttAni == null) {
            this.timeAttAni = Animation.getInstanceFromQi("/animation/utl_res/time_attack.dat");
            this.timeAttAniDrawer = this.timeAttAni[0].getDrawer(0, true, 0);
        }
        Key.touchProRaceModeInit();
        SoundSystem.getInstance().playBgm(2);
        this.isRaceModeItemsSelected = false;
    }

    private void proRaceModeLogic() {
        if (State.fadeChangeOver()) {
            if (Key.touchproracemodestart.Isin() && Key.touchproracemode.IsClick()) {
                this.cursor = 0;
            } else if (Key.touchproracemoderecord.Isin() && Key.touchproracemode.IsClick()) {
                this.cursor = 1;
            } else if (Key.touchproracemodereturn.Isin() && Key.touchproracemode.IsClick()) {
                this.cursor = 2;
            }
            if (Key.touchproracemodestart.IsButtonPress() && this.cursor == 0 && !this.isRaceModeItemsSelected) {
                this.isRaceModeItemsSelected = true;
                this.stage_sel_key = 1;
                changeStateWithFade(23);
                this.preCharaterSelectState = 24;
                initCharacterSelectRes();
                SoundSystem.getInstance().playSe(1);
            } else if (Key.touchproracemoderecord.IsButtonPress() && this.cursor == 1 && !this.isRaceModeItemsSelected) {
                this.isRaceModeItemsSelected = true;
                this.stage_sel_key = 0;
                changeStateWithFade(14);
                preStageSelectState = 24;
                initStageSelectRes();
                SoundSystem.getInstance().playSe(1);
            }
            if ((Key.press(524288) || (Key.touchproracemodereturn.IsButtonPress() && this.cursor == 2)) && State.fadeChangeOver()) {
                changeStateWithFade(2);
                this.isTitleBGMPlay = false;
                Key.touchMainMenuInit2();
                initTitleRes2();
                this.returnCursor = 0;
                SoundSystem.getInstance().playSe(2);
                SoundSystem.getInstance().stopBgm(false);
                menuInit(this.multiMainItems);
                return;
            }
            return;
        }
        Key.touchproracemodestart.reset();
        Key.touchproracemoderecord.reset();
    }

    private void drawProTimeAttack(MFGraphics g) {
        g.setColor(MapManager.END_COLOR);
        MyAPI.fillRect(g, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
        this.timeAttAniDrawer.setActionId(0);
        this.timeAttAniDrawer.draw(g, 0, 0);
        this.timeAttAniDrawer.setActionId(1);
        this.timeAttAniDrawer.draw(g, SCREEN_WIDTH, SCREEN_HEIGHT >> 1);
        int x;
        if ((Key.touchproracemodestart.Isin() || this.isRaceModeItemsSelected) && this.cursor == 0) {
            this.timeAttackOffsetX -= 2;
            this.timeAttackOffsetX %= 128;
            this.timeAttAniDrawer.setActionId(2);
            for (x = this.timeAttackOffsetX; x < (SCREEN_WIDTH * 3) / 2; x += 128) {
                this.timeAttAniDrawer.draw(g, ((SCREEN_WIDTH >> 1) + x) - 8, SCREEN_HEIGHT >> 1);
            }
            this.timeAttAniDrawer.draw(g, ((this.timeAttackOffsetX - 128) + (SCREEN_WIDTH >> 1)) - 8, SCREEN_HEIGHT >> 1);
        } else if ((Key.touchproracemoderecord.Isin() || this.isRaceModeItemsSelected) && this.cursor == 1) {
            this.timeAttackOffsetX -= 2;
            this.timeAttackOffsetX %= 128;
            this.timeAttAniDrawer.setActionId(3);
            for (x = this.timeAttackOffsetX; x < (SCREEN_WIDTH * 3) / 2; x += 128) {
                this.timeAttAniDrawer.draw(g, ((SCREEN_WIDTH >> 1) + x) - 8, SCREEN_HEIGHT >> 1);
            }
            this.timeAttAniDrawer.draw(g, ((this.timeAttackOffsetX - 128) + (SCREEN_WIDTH >> 1)) - 8, SCREEN_HEIGHT >> 1);
        } else {
            this.timeAttackOffsetX = 0;
        }
        if (muiAniDrawer == null) {
            muiAniDrawer = new Animation("/animation/mui").getDrawer(0, false, 0);
            return;
        }
        int i;
        AnimationDrawer animationDrawer = muiAniDrawer;
        if (Key.touchproracemodereturn.Isin()) {
            i = 5;
        } else {
            i = 0;
        }
        animationDrawer.setActionId(i + 61);
        muiAniDrawer.draw(g, 0, SCREEN_HEIGHT);
    }

    private void initStageSelectRes() {
        if (this.stageSelAni == null) {
            this.stageSelAni = Animation.getInstanceFromQi("/animation/utl_res/stage_select.dat");
            this.stageSelAniDrawer = this.stageSelAni[0].getDrawer(0, true, 0);
            this.stageSelArrowUpDrawer = this.stageSelAni[0].getDrawer(51, true, 0);
            this.stageSelArrowDownDrawer = this.stageSelAni[0].getDrawer(52, true, 0);
        }
        if (this.stageSelEmeraldDrawer == null) {
            this.stageSelEmeraldDrawer = new Animation("/animation/stage_select_emerald").getDrawer(0, false, 0);
        }
        Key.touchStageSelectModeInit();
        this.stageItemNumForShow = 6;
        this.stageDrawEndY = (this.stageDrawStartY + (this.stageItemNumForShow * ITEM_SPACE)) - (ITEM_SPACE >> 1);
        this.stageStartIndex = 0;
        this.stageDrawOffsetY = 0;
        this.offsetY = new int[this.STAGE_TOTAL_NUM];
        this.vY = new int[this.STAGE_TOTAL_NUM];
        this.offsetY[0] = SCREEN_HEIGHT >> 1;
        this.vY[0] = 6;
        for (int i = 1; i < this.STAGE_TOTAL_NUM; i++) {
            this.offsetY[i] = this.offsetY[i - 1] * 2;
            this.vY[i] = this.vY[i - 1] * 2;
        }
        this.stage_select_state = 0;
        this.stageSelectReturnFlag = false;
        this.optionMenuCursor = -1;
    }

    private void stageSelectLogic() {
        int i;
        if (this.stage_select_state == 0) {
            Key.touchstageselectreturn.resetKeyState();
            Key.touchCharacterSelectModeClose();
            Key.setKeyFunction(true);
            if (this.offsetY[0] == (SCREEN_HEIGHT >> 1)) {
                SoundSystem.getInstance().playBgm(3);
            }
            if (this.offsetY[0] - this.vY[0] <= 0) {
                for (i = 0; i < this.STAGE_TOTAL_NUM; i++) {
                    this.offsetY[i] = 0;
                }
                this.stage_select_state = 1;
            } else {
                for (i = 0; i < this.STAGE_TOTAL_NUM; i++) {
                    int[] iArr = this.offsetY;
                    iArr[i] = iArr[i] - this.vY[i];
                }
            }
            this.isStageSelectChange = false;
            this.stageselectslide_getprey = -1;
            this.stageselectslide_gety = -1;
            this.stageselectslide_y = 0;
            this.stageDrawOffsetBottomY = (-(StageManager.getMaxStageID() - 5)) * 24;
            this.stageYDirect = 0;
            this.isSelectable = false;
            this.stageSelectReturnFlag = false;
            this.stage_select_press_state = 0;
            this.stage_select_arrow_state = 0;
        } else if (this.stage_select_state == 1) {
            if (this.stageStartIndex > 0) {
                this.stageSelectUpArrowAvailable = true;
                this.isDrawUpArrow = true;
            } else {
                this.stageSelectUpArrowAvailable = false;
                this.isDrawUpArrow = false;
            }
            if (this.stageStartIndex < StageManager.getMaxStageID() - 5) {
                this.stageSelectDownArrowAvailable = true;
                this.isDrawDownArrow = true;
            } else {
                this.stageSelectDownArrowAvailable = false;
                this.isDrawDownArrow = false;
            }
            if (Key.touchstageselectuparrow.Isin() && this.stageSelectUpArrowAvailable) {
                this.stageSelectArrowDriveOffsetY = 12;
                this.stageSelectArrowMoveable = true;
            }
            if (Key.touchstageselectdownarrow.Isin() && this.stageSelectDownArrowAvailable) {
                this.stageSelectArrowDriveOffsetY = -12;
                this.stageSelectArrowMoveable = true;
            }
            if (this.stageSelectArrowMoveable) {
                this.stageSelectArrowDriveOffsetY /= 2;
                if (this.stageSelectArrowDriveOffsetY > 0 && this.stageSelectArrowDriveOffsetY < 2) {
                    this.stageSelectArrowDriveOffsetY = 2;
                }
                if (this.stageSelectArrowDriveOffsetY < 0 && this.stageSelectArrowDriveOffsetY > -2) {
                    this.stageSelectArrowDriveOffsetY = -2;
                }
                this.stageSelectArrowDriveY += this.stageSelectArrowDriveOffsetY;
                if (this.stageSelectArrowDriveOffsetY > 0) {
                    if (this.stageSelectArrowDriveY >= 24) {
                        this.stageSelectArrowDriveY = 24;
                        this.stageDrawOffsetY += this.stageSelectArrowDriveY;
                        this.stageSelectArrowDriveY = 0;
                        this.stageSelectArrowMoveable = false;
                    }
                } else if (this.stageSelectArrowDriveOffsetY < 0 && this.stageSelectArrowDriveY <= -24) {
                    this.stageSelectArrowDriveY = -24;
                    this.stageDrawOffsetY += this.stageSelectArrowDriveY;
                    this.stageSelectArrowDriveY = 0;
                    this.stageSelectArrowMoveable = false;
                }
            }
            this.stageselectslide_gety = Key.slidesensorstagesel.getPointerY();
            if (this.stageselectslide_gety == -1 && this.stageselectslide_getprey == -1) {
                this.stageselectslide_y = 0;
                this.stageselectslidefirsty = 0;
            } else if (this.stageselectslide_gety != -1 && this.stageselectslide_getprey == -1) {
                this.stageselectslidefirsty = this.stageselectslide_gety;
                this.firstStageSelectSlidePointY = this.stageselectslidefirsty;
                this.stageSelectSlideFrame = 0;
            } else if (this.stageselectslide_gety != -1 && this.stageselectslide_getprey != -1) {
                this.stageselectslide_y = this.stageselectslide_gety - this.stageselectslidefirsty;
            } else if (this.stageselectslide_gety == -1 && this.stageselectslide_getprey != -1) {
                this.stageDrawOffsetTmpY1 = this.stageselectslide_y + this.stageDrawOffsetY;
            }
            if (Key.slidesensorstagesel.isSliding()) {
                this.currentStageSelectSlidePointY = this.stageselectslide_y;
                if (this.currentStageSelectSlidePointY - this.preStageSelectSlidePointY >= 2 || this.currentStageSelectSlidePointY - this.preStageSelectSlidePointY <= -2) {
                    this.stageSelectSlideFrame++;
                } else {
                    this.stageSelectSlideFrame = 0;
                    this.firstStageSelectSlidePointY = this.currentStageSelectSlidePointY;
                }
                this.preStageSelectSlidePointY = this.currentStageSelectSlidePointY;
                if (this.stageSelectSlideFrame == 0) {
                    this.gestureSlideSpeed = 0;
                } else {
                    this.gestureSlideSpeed = ((this.currentStageSelectSlidePointY - this.firstStageSelectSlidePointY) / this.stageSelectSlideFrame) * 2;
                }
            } else if (this.stageYDirect == 0) {
                if (this.gestureSlideSpeed > 0) {
                    this.gestureSlideSpeed -= 3;
                    if (this.gestureSlideSpeed < 2) {
                        this.gestureSlideSpeed = 0;
                    }
                }
                if (this.gestureSlideSpeed < 0) {
                    this.gestureSlideSpeed -= -3;
                    if (this.gestureSlideSpeed > -2) {
                        this.gestureSlideSpeed = 0;
                    }
                }
                this.stageDrawOffsetY += this.gestureSlideSpeed;
                if (this.stageDrawOffsetY + this.gestureSlideSpeed > 0) {
                    this.stageDrawOffsetY = 0;
                    this.gestureSlideSpeed = 0;
                } else if (this.stageDrawOffsetY + this.gestureSlideSpeed < this.stageDrawOffsetBottomY) {
                    this.stageDrawOffsetY = this.stageDrawOffsetBottomY;
                    this.gestureSlideSpeed = 0;
                }
            }
            for (i = 0; i < Key.touchstageselectitem.length; i++) {
                Key.touchstageselectitem[i].setStartY((((this.stageDrawStartY + ((i + 1) * 24)) + this.offsetY[i]) + this.stageDrawOffsetY) + this.stageselectslide_y);
            }
            if (this.isSelectable && this.stageYDirect == 0) {
                i = 0;
                while (i < Key.touchstageselectitem.length) {
                    if (Key.touchstageselectitem[i].IsButtonPress() && this.optionMenuCursor == i && State.fadeChangeOver() && !Key.touchstageselect.Isin()) {
                        switch (this.stage_sel_key) {
                            case 0:
                                changeStateWithFade(25);
                                this.stage_characterRecord_ID = this.optionMenuCursor;
                                initRecordRes();
                                break;
                            case 1:
                                changeStateWithFade(26);
                                this.stage_characterRecord_ID = this.optionMenuCursor;
                                initIntergradeRecordRes();
                                break;
                            case 2:
                                StageManager.setStageID(this.optionMenuCursor);
                                StageManager.setStartStageID(this.optionMenuCursor);
                                State.setState(1);
                                break;
                        }
                        SoundSystem.getInstance().playSe(1);
                    } else {
                        i++;
                    }
                }
            }
            if (Key.slidesensorstagesel.isSliding()) {
                this.stage_select_press_state = 1;
                if (this.stageselectslide_y > 4 || this.stageselectslide_y < -4) {
                    this.isStageSelectChange = true;
                    this.isSelectable = false;
                    releaseAllStageSelectItemsTouchKey();
                    this.optionMenuCursor = -1;
                    this.stageSelectReturnFlag = false;
                } else {
                    this.isSelectable = true;
                }
                if (Key.slidesensorstagesel.isSlide(Key.DIR_UP)) {
                    this.isStageSelectChange = true;
                    this.isSelectable = false;
                } else if (Key.slidesensorstagesel.isSlide(Key.DIR_DOWN)) {
                    this.isStageSelectChange = true;
                    this.isSelectable = false;
                }
            } else {
                if (this.isStageSelectChange && this.stageselectslide_y == 0) {
                    this.stageDrawOffsetY = this.stageDrawOffsetTmpY1;
                    this.isStageSelectChange = false;
                    this.stageYDirect = 0;
                }
                if (!this.isStageSelectChange) {
                    int speed;
                    if (this.stageDrawOffsetY > 0) {
                        this.stageYDirect = 1;
                        speed = (-this.stageDrawOffsetY) >> 1;
                        if (speed > -2) {
                            speed = -2;
                        }
                        if (this.stageDrawOffsetY + speed <= 0) {
                            this.stageDrawOffsetY = 0;
                            this.stage_select_press_state = 0;
                            this.stageYDirect = 0;
                        } else {
                            this.stageDrawOffsetY += speed;
                        }
                    } else if (this.stageDrawOffsetY < this.stageDrawOffsetBottomY) {
                        this.stageYDirect = 2;
                        speed = (this.stageDrawOffsetBottomY - this.stageDrawOffsetY) >> 1;
                        if (speed < 2) {
                            speed = 2;
                        }
                        if (this.stageDrawOffsetY + speed >= this.stageDrawOffsetBottomY) {
                            this.stageDrawOffsetY = this.stageDrawOffsetBottomY;
                            this.stage_select_press_state = 0;
                            this.stageYDirect = 0;
                        } else {
                            this.stageDrawOffsetY += speed;
                        }
                    }
                }
            }
            this.stageStartIndex = (-(this.stageDrawOffsetY + this.stageselectslide_y)) / 24;
            if (this.stageYDirect == 0) {
                if (Key.touchstageselectreturn.Isin() && Key.touchstageselect.IsClick()) {
                    this.returnCursor = 1;
                }
                if (this.isSelectable) {
                    i = 0;
                    while (i < Key.touchstageselectitem.length) {
                        if (Key.touchstageselectitem[i].Isin() && Key.touchstageselect.IsClick() && i <= StageManager.getOpenedStageId()) {
                            this.optionMenuCursor = i;
                            this.returnCursor = 0;
                        } else {
                            i++;
                        }
                    }
                }
            }
            if ((Key.press(524288) || (Key.touchstageselectreturn.IsButtonPress() && this.returnCursor == 1)) && State.fadeChangeOver()) {
                SoundSystem.getInstance().stopBgm(false);
                changeStateWithFade(preStageSelectState);
                switch (preStageSelectState) {
                    case 2:
                        this.isTitleBGMPlay = false;
                        Key.touchMainMenuInit2();
                        break;
                    case 23:
                        Key.touchCharacterSelectModeInit();
                        initCharacterSelectRes();
                        break;
                    case 24:
                        Key.touchProRaceModeInit();
                        initTimeStageRes();
                        break;
                }
                SoundSystem.getInstance().playSe(2);
            }
            this.stageselectslide_getprey = this.stageselectslide_gety;
        }
    }

    private void releaseAllStageSelectItemsTouchKey() {
        for (TouchKeyRange resetKeyState : Key.touchstageselectitem) {
            resetKeyState.resetKeyState();
        }
    }

    private void drawStageSelect(MFGraphics g) {
        g.setColor(MapManager.END_COLOR);
        MyAPI.fillRect(g, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
        if (this.stageItemNumForShow != StageManager.STAGE_NUM) {
            MyAPI.setClip(g, 0, this.stageDrawStartY - 12, SCREEN_WIDTH, 200);
        }
        int i = 0;
        while (i < StageManager.STAGE_NAME.length) {
            if (i == this.optionMenuCursor && this.stage_select_state == 1 && this.isSelectable && this.stageYDirect == 0) {
                drawStageName(g, 1, i, (this.stageDrawOffsetY + this.stageselectslide_y) + this.stageSelectArrowDriveY);
                drawStageName(g, 29, i, (this.stageDrawOffsetY + this.stageselectslide_y) + this.stageSelectArrowDriveY);
            } else if (i <= StageManager.getOpenedStageId()) {
                drawStageName(g, 15, i, (this.stageDrawOffsetY + this.stageselectslide_y) + this.stageSelectArrowDriveY);
            } else if ((GameObject.stageModeState == 0 && i < 13) || (GameObject.stageModeState == 1 && i < 12)) {
                drawStageName(g, 1, i, (this.stageDrawOffsetY + this.stageselectslide_y) + this.stageSelectArrowDriveY);
            }
            i++;
        }
        i = 0;
        while (i < (StageManager.STAGE_NAME.length >> 1) - 1 && i * 2 <= StageManager.getOpenedStageId()) {
            this.stageSelAniDrawer.draw(g, i + 43, SCREEN_WIDTH, this.stageSelectArrowDriveY + ((((this.stageDrawStartY + this.offsetY[(i * 2) + 1]) + ((i + 1) * 48)) + this.stageDrawOffsetY) + this.stageselectslide_y), false, 0);
            i++;
        }
        int maxUnlockStage = ((StageManager.getOpenedStageId() / 2) * 2) + 1 > 12 ? 12 : ((StageManager.getOpenedStageId() / 2) * 2) + 1;
        if (maxUnlockStage == 7) {
            maxUnlockStage = 8;
        }
        if (GameObject.stageModeState == 0) {
            for (i = 0; i < maxUnlockStage; i++) {
                drawStageEmerald(g, i);
            }
        }
        if (12 <= StageManager.getOpenedStageId()) {
            this.stageSelAniDrawer.draw(g, 49, SCREEN_WIDTH, this.stageSelectArrowDriveY + (((((this.stageDrawStartY + this.offsetY[12]) + 336) - 24) + this.stageDrawOffsetY) + this.stageselectslide_y), false, 0);
        }
        if (13 <= StageManager.getOpenedStageId()) {
            this.stageSelAniDrawer.draw(g, 50, SCREEN_WIDTH, this.stageSelectArrowDriveY + ((((this.stageDrawStartY + this.offsetY[13]) + 336) + this.stageDrawOffsetY) + this.stageselectslide_y), false, 0);
        }
        this.stageSelAniDrawer.setActionId(0);
        this.stageSelAniDrawer.draw(g, 0, 0);
        if (this.isDrawUpArrow) {
            this.stageSelArrowUpDrawer.draw(g, 44, (SCREEN_HEIGHT >> 1) - 49);
        }
        if (this.isDrawDownArrow) {
            this.stageSelArrowDownDrawer.draw(g, 44, (SCREEN_HEIGHT >> 1) + 49);
        }
        if (muiAniDrawer == null) {
            muiAniDrawer = new Animation("/animation/mui").getDrawer(0, false, 0);
            return;
        }
        int i2;
        AnimationDrawer animationDrawer = muiAniDrawer;
        if (Key.touchstageselectreturn.Isin()) {
            i2 = 5;
        } else {
            i2 = 0;
        }
        animationDrawer.setActionId(i2 + 61);
        muiAniDrawer.draw(g, 0, SCREEN_HEIGHT);
    }

    private void drawStageName(MFGraphics g, int preid, int stageId, int mOffsetY) {
        this.stageSelAniDrawer.draw(g, preid + stageId, SCREEN_WIDTH >> 1, ((this.stageDrawStartY + ((stageId + 1) * 24)) + this.offsetY[stageId]) + mOffsetY, false, 0);
    }

    private void drawStageEmerald(MFGraphics g, int stageId) {
        if (stageId != 7) {
            switch (SpecialStageState.emeraldState(stageId)) {
                case 1:
                    this.stageSelEmeraldDrawer.draw(g, stageId < 7 ? (stageId >> 1) + 2 : ((stageId >> 1) + 2) + 2, SCREEN_WIDTH, this.stageSelectArrowDriveY + ((((this.stageDrawStartY + this.offsetY[((stageId >> 1) * 2) + 1]) + (((stageId >> 1) + 1) * 48)) + this.stageDrawOffsetY) + this.stageselectslide_y), false, 0);
                    return;
                case 2:
                    this.stageSelEmeraldDrawer.draw(g, 0, SCREEN_WIDTH, ((((this.stageDrawStartY + this.offsetY[((stageId >> 1) * 2) + 1]) + (((stageId >> 1) + 1) * 48)) + this.stageDrawOffsetY) + this.stageselectslide_y) + this.stageSelectArrowDriveY, false, 0);
                    return;
                default:
                    return;
            }
        } else if (SpecialStageState.emeraldState(stageId - 1) == 0) {
            switch (SpecialStageState.emeraldState(stageId)) {
                case 1:
                    this.stageSelEmeraldDrawer.draw(g, 6, SCREEN_WIDTH, this.stageSelectArrowDriveY + ((((this.stageDrawStartY + this.offsetY[((stageId >> 1) * 2) + 1]) + (((stageId >> 1) + 1) * 48)) + this.stageDrawOffsetY) + this.stageselectslide_y), false, 0);
                    return;
                case 2:
                    this.stageSelEmeraldDrawer.draw(g, 0, SCREEN_WIDTH, ((((this.stageDrawStartY + this.offsetY[((stageId >> 1) * 2) + 1]) + (((stageId >> 1) + 1) * 48)) + this.stageDrawOffsetY) + this.stageselectslide_y) + this.stageSelectArrowDriveY, false, 0);
                    return;
                default:
                    return;
            }
        } else {
            switch (SpecialStageState.emeraldState(stageId)) {
                case 1:
                    this.stageSelEmeraldDrawer.draw(g, 7, SCREEN_WIDTH, this.stageSelectArrowDriveY + ((((this.stageDrawStartY + this.offsetY[((stageId >> 1) * 2) + 1]) + (((stageId >> 1) + 1) * 48)) + this.stageDrawOffsetY) + this.stageselectslide_y), false, 0);
                    return;
                case 2:
                    this.stageSelEmeraldDrawer.draw(g, 1, SCREEN_WIDTH, ((((this.stageDrawStartY + this.offsetY[((stageId >> 1) * 2) + 1]) + (((stageId >> 1) + 1) * 48)) + this.stageDrawOffsetY) + this.stageselectslide_y) + this.stageSelectArrowDriveY, false, 0);
                    return;
                default:
                    return;
            }
        }
    }

    private void initRecordRes() {
        if (this.recordAni == null) {
            this.recordAni = Animation.getInstanceFromQi("/animation/utl_res/record.dat");
            this.recordAniDrawer = this.recordAni[0].getDrawer(0, true, 0);
        }
        Key.touchCharacterRecordInit();
        this.characterRecordID = 0;
        this.characterRecordDisFlag = false;
        this.characterRecordScoreUpdateIconY = -32;
        this.characterRecordScoreUpdateCursor = 0;
    }

    private void characterRecordLogic() {
        if (!this.characterRecordDisFlag) {
            SoundSystem.getInstance().playBgm(4);
            this.characterRecordDisFlag = true;
        }
        if (Key.slidesensorcharacterrecord.isSliding()) {
            if (Key.slidesensorcharacterrecord.isSlide(Key.DIR_LEFT)) {
                this.characterRecordID--;
                this.characterRecordID += PlayerObject.CHARACTER_LIST.length;
                this.characterRecordID %= PlayerObject.CHARACTER_LIST.length;
                SoundSystem.getInstance().playSe(3);
            } else if (Key.slidesensorcharacterrecord.isSlide(Key.DIR_RIGHT)) {
                this.characterRecordID++;
                this.characterRecordID += PlayerObject.CHARACTER_LIST.length;
                this.characterRecordID %= PlayerObject.CHARACTER_LIST.length;
                SoundSystem.getInstance().playSe(3);
            }
        }
        if (Key.touchintergraderecordleftarrow.IsButtonPress() && State.fadeChangeOver()) {
            this.characterRecordID--;
            this.characterRecordID += PlayerObject.CHARACTER_LIST.length;
            this.characterRecordID %= PlayerObject.CHARACTER_LIST.length;
            SoundSystem.getInstance().playSe(3);
        } else if (Key.touchintergraderecordrightarrow.IsButtonPress() && State.fadeChangeOver()) {
            this.characterRecordID++;
            this.characterRecordID += PlayerObject.CHARACTER_LIST.length;
            this.characterRecordID %= PlayerObject.CHARACTER_LIST.length;
            SoundSystem.getInstance().playSe(3);
        }
        if ((Key.press(524288) || Key.touchintergraderecordreturn.IsButtonPress()) && State.fadeChangeOver()) {
            this.stage_sel_key = 0;
            changeStateWithFade(14);
            preStageSelectState = 24;
            initStageSelectRes();
            SoundSystem.getInstance().playSe(2);
        }
    }

    private void drawCharacterRecord(MFGraphics g) {
        drawCharacterMovingBG(g, this.characterRecordID);
        this.recordAniDrawer.draw(g, this.characterRecordID + 4, SCREEN_WIDTH >> 1, SCREEN_HEIGHT >> 1, false, 0);
        this.recordAniDrawer.draw(g, this.stage_characterRecord_ID + 14, SCREEN_WIDTH >> 1, SCREEN_HEIGHT >> 1, false, 0);
        drawRecordArrow(g);
        drawRecordTime(g, StageManager.getTimeModeScore(this.characterRecordID, this.stage_characterRecord_ID, 0), (SCREEN_HEIGHT >> 1) + 12);
        drawRecordTime(g, StageManager.getTimeModeScore(this.characterRecordID, this.stage_characterRecord_ID, 1), ((SCREEN_HEIGHT >> 1) + 12) + 16);
        drawRecordTime(g, StageManager.getTimeModeScore(this.characterRecordID, this.stage_characterRecord_ID, 2), ((SCREEN_HEIGHT >> 1) + 12) + 32);
        if (muiAniDrawer == null) {
            muiAniDrawer = new Animation("/animation/mui").getDrawer(0, false, 0);
            return;
        }
        int i;
        AnimationDrawer animationDrawer = muiAniDrawer;
        if (Key.touchintergraderecordreturn.Isin()) {
            i = 5;
        } else {
            i = 0;
        }
        animationDrawer.setActionId(i + 61);
        muiAniDrawer.draw(g, 0, SCREEN_HEIGHT);
    }

    private void drawRecordArrow(MFGraphics g) {
        this.recordArrowOffsetXID++;
        this.recordArrowOffsetXID %= this.recordArrowOffsetXArray.length;
        this.recordAniDrawer.draw(g, 12, this.recordArrowOffsetXArray[this.recordArrowOffsetXID] + SCREEN_WIDTH, SCREEN_HEIGHT >> 1, false, 0);
        this.recordAniDrawer.draw(g, 13, 0 - this.recordArrowOffsetXArray[this.recordArrowOffsetXID], SCREEN_HEIGHT >> 1, false, 0);
    }

    private void drawRecordTime(MFGraphics g, int num, int y) {
        int timenum = num == 0 ? SonicDef.OVER_TIME : num;
        int sec = (timenum % 60000) / 1000;
        int sec10 = sec / 10;
        int sec01 = sec % 10;
        int msec = ((timenum % 60000) % 1000) / 10;
        int msec10 = msec / 10;
        int msec01 = msec % 10;
        this.recordAniDrawer.draw(g, (timenum / 60000) + 34, (SCREEN_WIDTH >> 1) - 38, y, false, 0);
        this.recordAniDrawer.draw(g, sec10 + 34, (SCREEN_WIDTH >> 1) - 6, y, false, 0);
        this.recordAniDrawer.draw(g, sec01 + 34, (SCREEN_WIDTH >> 1) + 10, y, false, 0);
        this.recordAniDrawer.draw(g, msec10 + 34, (SCREEN_WIDTH >> 1) + 42, y, false, 0);
        this.recordAniDrawer.draw(g, msec01 + 34, (SCREEN_WIDTH >> 1) + 58, y, false, 0);
    }

    private void drawCharacterMovingBG(MFGraphics g, int character_id) {
        int x;
        int y;
        this.recordAniDrawer.setActionId(character_id);
        this.characterRecordBGOffsetX_1 += 4;
        this.characterRecordBGOffsetX_1 %= 128;
        for (x = this.characterRecordBGOffsetX_1; x < SCREEN_WIDTH * 2; x += 128) {
            for (y = 0; y < SCREEN_HEIGHT; y += 48) {
                this.recordAniDrawer.draw(g, x - 128, y);
            }
        }
        this.characterRecordBGOffsetX_2 -= 4;
        this.characterRecordBGOffsetX_2 %= 128;
        for (x = this.characterRecordBGOffsetX_2 - 64; x < (SCREEN_WIDTH * 3) / 2; x += 128) {
            for (y = 0; y < SCREEN_HEIGHT; y += 48) {
                this.recordAniDrawer.draw(g, x, y + 24);
            }
        }
    }

    private void initIntergradeRecordRes() {
        if (this.recordAni == null) {
            this.recordAni = Animation.getInstanceFromQi("/animation/utl_res/record.dat");
            this.recordAniDrawer = this.recordAni[0].getDrawer(0, true, 0);
        }
        this.characterRecordID = PlayerObject.getCharacterID();
        Key.touchCharacterRecordInit();
        Key.touchanykeyInit();
        this.intergradeRecordtoGamecnt = 0;
    }

    private void intergradeRecordLogic() {
        this.intergradeRecordtoGamecnt++;
        if (Key.press(Key.B_SEL) && this.intergradeRecordtoGamecnt != 56) {
            this.intergradeRecordtoGamecnt = 56;
            SoundSystem.getInstance().playSe(1);
        }
        if (this.intergradeRecordtoGamecnt == 56) {
            StageManager.setStageID(this.stage_characterRecord_ID);
            SoundSystem.getInstance().stopBgm(false);
            State.setState(1);
            Key.touchCharacterRecordClose();
            Key.touchanykeyClose();
        }
        if (Key.press(524288) && State.fadeChangeOver()) {
            StageManager.setStageID(this.stage_characterRecord_ID);
            SoundSystem.getInstance().stopBgm(false);
            State.setState(1);
            Key.touchCharacterRecordClose();
            SoundSystem.getInstance().playSe(1);
        }
    }

    private void drawIntergradeRecord(MFGraphics g) {
        drawCharacterMovingBG(g, this.characterRecordID);
        this.recordAniDrawer.draw(g, this.characterRecordID + 8, SCREEN_WIDTH >> 1, SCREEN_HEIGHT >> 1, false, 0);
        this.recordAniDrawer.draw(g, this.stage_characterRecord_ID + 14, SCREEN_WIDTH >> 1, SCREEN_HEIGHT >> 1, false, 0);
        drawStageNameinIntergradeRecord(g);
        drawRecordTime(g, StageManager.getTimeModeScore(this.characterRecordID, this.stage_characterRecord_ID, 0), (SCREEN_HEIGHT >> 1) + 12);
        drawRecordTime(g, StageManager.getTimeModeScore(this.characterRecordID, this.stage_characterRecord_ID, 1), ((SCREEN_HEIGHT >> 1) + 12) + 16);
        drawRecordTime(g, StageManager.getTimeModeScore(this.characterRecordID, this.stage_characterRecord_ID, 2), ((SCREEN_HEIGHT >> 1) + 12) + 32);
    }

    private void drawStageNameinIntergradeRecord(MFGraphics g) {
        int aniID;
        this.recordAniDrawer.draw(g, 26, SCREEN_WIDTH >> 1, SCREEN_HEIGHT >> 1, false, 0);
        this.intergradeRecordStageNameOffsetX += INTERGRADE_RECORD_STAGE_NAME_SPEED;
        this.intergradeRecordStageNameOffsetX %= 200;
        if (this.stage_characterRecord_ID < 10) {
            aniID = (this.stage_characterRecord_ID >> 1) + 27;
        } else {
            aniID = (this.stage_characterRecord_ID - 5) + 27;
        }
        for (int x = this.intergradeRecordStageNameOffsetX - 100; x < (SCREEN_WIDTH * 3) / 2; x += 200) {
            this.recordAniDrawer.draw(g, aniID, x, SCREEN_HEIGHT >> 1, false, 0);
        }
    }

    private void drawScrollString(MFGraphics g, String string, int y, int speed, int space, int color1, int color2, int color3, int anchor) {
        this.itemOffsetX += speed;
        this.itemOffsetX %= space;
        int x = 0;
        while (x - this.itemOffsetX > 0) {
            x -= space;
        }
        int drawNum = (((SCREEN_WIDTH + space) - 1) / space) + 2;
        for (int i = 0; i < drawNum; i++) {
            MyAPI.drawBoldString(g, string, (x + (i * space)) - this.itemOffsetX, y, 17, color1, color2, color3);
        }
    }

    private void drawTitle1(MFGraphics g) {
        this.copyOffsetX = MyAPI.calNextPosition((double) this.copyOffsetX, 0.0d, 1, 2);
        MyAPI.drawImage(g, this.sonicBigImage, this.sonicBigX - this.cameraX, SONIC_BIG_Y, 20);
    }

    private void drawTitle2(MFGraphics g) {
        if (this.logoX == LOGO_POSITION_X) {
            degreeLogic();
        }
    }

    private void degreeLogic() {
        this.titleDegree++;
        this.titleDegree %= OFFSET_ARRAY.length;
    }

    private int getOffsetY(int degreeOffset) {
        return OFFSET_ARRAY[this.titleDegree];
    }

    private static void drawTouchKeyMainMenu(MFGraphics g) {
    }

    private static void drawTouchKeyConfirm(MFGraphics g) {
    }

    private static void drawTouchKeyMenu(MFGraphics g) {
    }

    private static void drawTouchKeyHelp(MFGraphics g) {
    }

    private static void drawTouchKeyAbout(MFGraphics g) {
    }

    private static void drawTouchKeySelectStage(MFGraphics g) {
    }

    private static void drawTouchKeyOption(MFGraphics g) {
    }

    public void gotoMainmenu() {
        state = 2;
        this.nextState = 2;
        menuInit(this.multiMainItems);
        mainMenuInit();
        Key.touchMainMenuInit2();
        this.returnCursor = 0;
        Key.touchanykeyClose();
        Key.touchkeyboardInit();
        Key.clear();
    }

    public void gotoStageSelect() {
        if (StageManager.characterFromGame == -1 || StageManager.stageIDFromGame == -1) {
            changeStateWithFade(23);
            this.preCharaterSelectState = 2;
            this.stage_sel_key = 2;
            initCharacterSelectRes();
            GameState.isThroughGame = false;
            StageManager.isContinueGame = false;
            return;
        }
        startGameInit();
        State.fadeInit(0, 102);
        state = 4;
    }

    public boolean BP_gotoPaying() {
        return false;
    }

    public void BP_payingLogic() {
        if (!BP_enteredPaying) {
            if (State.BP_chargeLogic(0)) {
                BP_enteredPaying = true;
                State.activeGameProcess(true);
                State.setMenu();
                State.setTry();
                State.saveBPRecord();
                gotoStageSelect();
                return;
            }
            BP_enteredPaying = true;
            State.setTry();
            StageManager.resetStageIdforTry();
            State.saveBPRecord();
            gotoMainmenu();
        }
    }

    public void characterSelectInit() {
    }

    public void characterSelectDraw(MFGraphics g) {
        menuBgDraw(g);
        for (int i = 0; i < CHARACTER_STR.length; i++) {
            MyAPI.drawBoldString(g, CHARACTER_STR[i], SCREEN_WIDTH >> 1, (i * 30) + 20, 17, 16776960, 0);
            if (i == PlayerObject.getCharacterID()) {
                MFGraphics mFGraphics = g;
                MyAPI.drawBoldString(mFGraphics, "*", ((SCREEN_WIDTH - MFGraphics.stringWidth(14, CHARACTER_STR[i])) >> 1) - 10, (i * 30) + 20, 24, 16776960, 0);
            }
        }
    }

    private void mainMenuNormalLogic() {
        if (!this.isTitleBGMPlay) {
            this.isTitleBGMPlay = true;
        }
        Key.touchanykeyClose();
        if (Key.touchmainmenustart.Isin() && Key.touchmainmenu.IsClick()) {
            this.cursor = 0;
        } else if (Key.touchmainmenurace.Isin() && Key.touchmainmenu.IsClick()) {
            this.cursor = 1;
        } else if (Key.touchmainmenuoption.Isin() && Key.touchmainmenu.IsClick()) {
            this.cursor = 2;
        } else if (Key.touchmainmenuend.Isin() && Key.touchmainmenu.IsClick()) {
            this.cursor = 3;
        }
        if (Key.touchmainmenureturn.Isin() && Key.touchmainmenu.IsClick()) {
            this.returnCursor = 1;
        }
        if (Key.touchmainmenustart.IsButtonPress() && this.cursor == 0 && (State.fadeChangeOver() || this.isFromStartGame)) {
            PlayerObject.stageModeState = 0;
            if (!BP_gotoPaying()) {
                gotoStageSelect();
            }
            SoundSystem.getInstance().playSe(1);
            if (this.isFromStartGame) {
                this.isFromStartGame = false;
            }
        } else if (Key.touchmainmenurace.IsButtonPress() && this.cursor == 1 && (State.fadeChangeOver() || this.isFromStartGame)) {
            PlayerObject.stageModeState = 1;
            changeStateWithFade(24);
            initTimeStageRes();
            this.cursor = 0;
            SoundSystem.getInstance().playSe(1);
            if (this.isFromStartGame) {
                this.isFromStartGame = false;
            }
            this.isRaceModeItemsSelected = false;
        } else if (Key.touchmainmenuoption.IsButtonPress() && this.cursor == 2 && (State.fadeChangeOver() || this.isFromStartGame)) {
            optionInit();
            changeStateWithFade(9);
            SoundSystem.getInstance().playSe(1);
            if (this.isFromStartGame) {
                this.isFromStartGame = false;
            }
        } else if (Key.touchmainmenuend.IsButtonPress() && this.cursor == 3 && (State.fadeChangeOver() || this.isFromStartGame)) {
            state = 12;
            secondEnsureInit();
            State.fadeInit(0, 220);
            SoundSystem.getInstance().playSe(1);
            if (this.isFromStartGame) {
                this.isFromStartGame = false;
            }
            this.quitFlag = 0;
        }
        if (!Key.press(524288) && (!Key.touchmainmenureturn.IsButtonPress() || this.returnCursor != 1)) {
            return;
        }
        if (State.fadeChangeOver() || this.isFromStartGame) {
            state = 1;
            initTitleRes2();
            SoundSystem.getInstance().playSe(2);
            if (this.isFromStartGame) {
                this.isFromStartGame = false;
            }
            Key.touchanykeyInit();
            fading = false;
        }
    }

    private void mainMenuMultiItemsLogic() {
        if (!this.isTitleBGMPlay) {
            this.isTitleBGMPlay = true;
        }
        Key.touchanykeyClose();
        if (this.menuMoving) {
            this.mainMenuCursor = 0;
            this.returnCursor = 0;
            this.mainMenuEnsureFlag = false;
            return;
        }
        if (Key.touchmainmenuup.Isin() && Key.touchmainmenu.IsClick()) {
            this.mainMenuCursor = 1;
        } else if (Key.touchmainmenudown.Isin() && Key.touchmainmenu.IsClick()) {
            this.mainMenuCursor = 2;
        } else if (Key.touchmainmenuitem.Isin() && Key.touchmainmenu.IsClick()) {
            this.mainMenuCursor = 3;
            this.mainMenuEnsureFlag = true;
        }
        if (Key.touchmainmenureturn.Isin() && Key.touchmainmenu.IsClick()) {
            this.returnCursor = 1;
        }
        if (Key.touchmainmenuup.IsButtonPress() && this.mainMenuCursor == 1) {
            this.mainMenuItemCursor--;
            this.mainMenuItemCursor = (this.mainMenuItemCursor + this.elementNum) % this.elementNum;
            changeUpSelect();
            SoundSystem.getInstance().playSe(3);
        } else if (Key.touchmainmenudown.IsButtonPress() && this.mainMenuCursor == 2) {
            this.mainMenuItemCursor++;
            this.mainMenuItemCursor = (this.mainMenuItemCursor + this.elementNum) % this.elementNum;
            changeDownSelect();
            SoundSystem.getInstance().playSe(3);
        } else if (Key.touchmainmenuitem.IsButtonPress() && this.mainMenuCursor == 3) {
            switch (this.mainMenuItemCursor) {
                case 1:
                    PlayerObject.stageModeState = 1;
                    changeStateWithFade(24);
                    initTimeStageRes();
                    this.cursor = 0;
                    if (this.isFromStartGame) {
                        this.isFromStartGame = false;
                    }
                    this.isRaceModeItemsSelected = false;
                    break;
                case 2:
                    optionInit();
                    changeStateWithFade(9);
                    if (this.isFromStartGame) {
                        this.isFromStartGame = false;
                        break;
                    }
                    break;
                case 3:
                    state = 41;
                    secondEnsureInit();
                    State.fadeInit(0, 220);
                    if (this.isFromStartGame) {
                        this.isFromStartGame = false;
                    }
                    segaMoreInit();
                    break;
                case 4:
                    state = 12;
                    secondEnsureInit();
                    State.fadeInit(0, 220);
                    if (this.isFromStartGame) {
                        this.isFromStartGame = false;
                    }
                    this.quitFlag = 0;
                    break;
            }
            SoundSystem.getInstance().playSe(1);
        }
        if (!Key.press(524288) && (!Key.touchmainmenureturn.IsButtonPress() || this.returnCursor != 1)) {
            return;
        }
        if (State.fadeChangeOver() || this.isFromStartGame) {
            state = 1;
            initTitleRes2();
            SoundSystem.getInstance().playSe(2);
            if (this.isFromStartGame) {
                this.isFromStartGame = false;
            }
            Key.touchanykeyInit();
            fading = false;
        }
    }

    private void mainMenuLogic() {
        mainMenuNormalLogic();
    }

    private void startGameInit() {
        if (muiAniDrawer == null) {
            muiAniDrawer = new Animation("/animation/mui").getDrawer(0, false, 0);
        }
        Key.touchStartGameInit();
        this.startgamecursor = -1;
        this.startgameframe = 0;
        this.startgameensureFlag = false;
        State.fadeInit(102, 220);
        StageManager.isContinueGame = false;
    }

    private void startGameLogic() {
        Key.touchMainMenuReset2();
        if (Key.touchstartgamecontinue.Isin() && Key.touchstartgame.IsClick()) {
            this.startgamecursor = 0;
        } else if (Key.touchstartgamenew.Isin() && Key.touchstartgame.IsClick()) {
            this.startgamecursor = 1;
        } else if (Key.touchstartgamereturn.Isin() && Key.touchstartgame.IsClick() && !this.startgameensureFlag) {
            this.startgamecursor = 2;
        }
        if (this.startgameensureFlag) {
            this.startgameframe++;
            if (this.startgameframe != 12) {
                return;
            }
            if (this.startgamecursor == 0) {
                StageManager.isContinueGame = true;
                State.fadeInit(255, 0);
                State.setState(1);
                System.out.println("continue");
                return;
            } else if (this.startgamecursor == 1) {
                StageManager.isContinueGame = false;
                changeStateWithFade(23);
                initCharacterSelectRes();
                this.preCharaterSelectState = 2;
                this.stage_sel_key = 2;
                PlayerObject.stageModeState = 0;
                System.out.println("new game");
                return;
            } else {
                return;
            }
        }
        if (Key.touchstartgamecontinue.IsButtonPress() && this.startgamecursor == 0) {
            this.startgameensureFlag = true;
            this.startgameframe = 0;
            StageManager.loadStageRecord();
            PlayerObject.setCharacter(StageManager.characterFromGame);
            StageManager.setStageID(StageManager.stageIDFromGame);
            State.fadeInit(102, 255);
            SoundSystem.getInstance().playSe(1);
        } else if (Key.touchstartgamenew.IsButtonPress() && this.startgamecursor == 1) {
            this.startgameensureFlag = true;
            this.startgameframe = 0;
            PlayerObject.setScore(0);
            PlayerObject.setLife(2);
            State.fadeInit(102, 255);
            SoundSystem.getInstance().playSe(1);
            GameState.isThroughGame = false;
            StageManager.isContinueGame = false;
        }
        if ((Key.press(524288) || (Key.touchstartgamereturn.IsButtonPress() && this.startgamecursor == 2)) && State.fadeChangeOver()) {
            State.fadeInit(102, 0);
            state = 2;
            this.nextState = 2;
            State.setFadeOver();
            this.isTitleBGMPlay = false;
            Key.touchMainMenuInit2();
            Key.touchanykeyClose();
            Key.touchkeyboardInit();
            Key.clear();
            SoundSystem.getInstance().playSe(2);
            initTitleRes2();
            this.returnCursor = 0;
        }
    }

    private void startGameDraw(MFGraphics g) {
        AnimationDrawer animationDrawer = muiAniDrawer;
        int i = (Key.touchstartgamecontinue.Isin() && this.startgamecursor == 0) ? 1 : 0;
        animationDrawer.setActionId(i + 55);
        muiAniDrawer.draw(g, SCREEN_WIDTH >> 1, (SCREEN_HEIGHT >> 1) - 18);
        animationDrawer = muiAniDrawer;
        if (Key.touchstartgamenew.Isin() && this.startgamecursor == 1) {
            i = 1;
        } else {
            i = 0;
        }
        animationDrawer.setActionId(i + 55);
        muiAniDrawer.draw(g, SCREEN_WIDTH >> 1, (SCREEN_HEIGHT >> 1) + 18);
        muiAniDrawer.setActionId(0);
        muiAniDrawer.draw(g, SCREEN_WIDTH >> 1, (SCREEN_HEIGHT >> 1) - 18);
        muiAniDrawer.setActionId(1);
        muiAniDrawer.draw(g, SCREEN_WIDTH >> 1, (SCREEN_HEIGHT >> 1) + 18);
        animationDrawer = muiAniDrawer;
        if (Key.touchstartgamereturn.Isin()) {
            i = 5;
        } else {
            i = 0;
        }
        animationDrawer.setActionId(i + 61);
        muiAniDrawer.draw(g, 0, SCREEN_HEIGHT);
        if (this.startgameensureFlag) {
            State.drawFadeSlow(g);
            if (this.startgameframe >= 12) {
                g.setColor(0);
                MyAPI.fillRect(g, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
            }
        }
    }

    private void quitLogic() {
        Key.touchMainMenuReset2();
        switch (secondEnsureLogic()) {
            case 1:
                close();
                System.gc();
                state = 13;
                SoundSystem.getInstance().stopBgm(true);
                Key.touchkeyboardClose();
                Key.touchsoftkeyInit();
                return;
            case 2:
                if (this.quitFlag == 1) {
                    state = 1;
                    Key.touchanykeyInit();
                    return;
                } else if (this.quitFlag == 0) {
                    gotoMainmenu();
                    Key.clear();
                    State.setFadeOver();
                    return;
                } else {
                    return;
                }
            default:
                return;
        }
    }

    private void menuOptionDiffLogic() {
        switch (itemsSelect2Logic()) {
            case 1:
                GlobalResource.difficultyConfig = 1;
                State.fadeInit(220, 0);
                state = 9;
                return;
            case 2:
                GlobalResource.difficultyConfig = 0;
                State.fadeInit(220, 0);
                state = 9;
                return;
            case 3:
                State.fadeInit(220, 0);
                state = 9;
                return;
            default:
                return;
        }
    }

    private void menuOptionSoundLogic() {
        switch (itemsSelect2Logic()) {
            case 1:
                GlobalResource.soundSwitchConfig = 1;
                State.fadeInit(220, 0);
                state = 9;
                SoundSystem.getInstance().setSoundState(GlobalResource.soundConfig);
                SoundSystem.getInstance().setSeState(GlobalResource.seConfig);
                return;
            case 2:
                GlobalResource.soundSwitchConfig = 0;
                State.fadeInit(220, 0);
                state = 9;
                SoundSystem.getInstance().setSoundState(0);
                SoundSystem.getInstance().setSeState(0);
                return;
            case 3:
                State.fadeInit(220, 0);
                state = 9;
                return;
            default:
                return;
        }
    }

    private void menuOptionVibLogic() {
        switch (itemsSelect2Logic()) {
            case 1:
                GlobalResource.vibrationConfig = 1;
                State.fadeInit(220, 0);
                state = 9;
                MyAPI.vibrate();
                return;
            case 2:
                GlobalResource.vibrationConfig = 0;
                State.fadeInit(220, 0);
                state = 9;
                return;
            case 3:
                State.fadeInit(220, 0);
                state = 9;
                return;
            default:
                return;
        }
    }

    private void menuOptionTimeLimitLogic() {
        switch (itemsSelect2Logic()) {
            case 1:
                GlobalResource.timeLimit = 0;
                State.fadeInit(220, 0);
                state = 9;
                return;
            case 2:
                GlobalResource.timeLimit = 1;
                State.fadeInit(220, 0);
                state = 9;
                return;
            case 3:
                State.fadeInit(220, 0);
                state = 9;
                return;
            default:
                return;
        }
    }

    private void menuOptionSpSetLogic() {
        switch (itemsSelect2Logic()) {
            case 1:
                GlobalResource.spsetConfig = 0;
                State.fadeInit(220, 0);
                state = 9;
                return;
            case 2:
                GlobalResource.spsetConfig = 1;
                State.fadeInit(220, 0);
                state = 9;
                return;
            case 3:
                State.fadeInit(220, 0);
                state = 9;
                return;
            default:
                return;
        }
    }

    private void menuOptionResetRecordLogic() {
        switch (secondEnsureDirectLogic()) {
            case 1:
                state = 37;
                secondEnsureInit();
                return;
            case 2:
                State.fadeInit(220, 0);
                state = 9;
                return;
            default:
                return;
        }
    }

    private void menuOptionResetRecordEnsureLogic() {
        switch (secondEnsureLogic()) {
            case 1:
                StageManager.resetGameRecord();
                SoundSystem.getInstance().setSoundState(GlobalResource.soundConfig);
                SoundSystem.getInstance().setSeState(GlobalResource.seConfig);
                this.resetInfoCount = this.RESET_INFO_COUNT;
                state = 9;
                State.fadeInit(220, 0);
                return;
            case 2:
                state = 36;
                secondEnsureInit();
                State.fadeInit(220, 220);
                return;
            default:
                return;
        }
    }

    private void menuOptionLanguageInit() {
        this.itemsselectcursor = 0;
        this.isItemsSelect = false;
        State.fadeInit(102, 220);
        Key.touchMenuOptionLanguageInit();
    }

    private int menuOptionLanguageCheck() {
        int i;
        for (i = 0; i < 5; i++) {
            if (Key.touchmenuoptionlanguageitems[i].Isin() && Key.touchmenuoptionlanguage.IsClick()) {
                this.itemsselectcursor = i;
            }
        }
        if (Key.touchmenuoptionlanguagereturn.Isin() && Key.touchmenuoptionlanguage.IsClick()) {
            this.itemsselectcursor = -2;
        }
        if (this.isItemsSelect) {
            this.itemsselectframe++;
            if (this.itemsselectframe > 8) {
                State.fadeInit(220, 102);
                return this.finalitemsselectcursor + 2;
            }
        }
        i = 0;
        while (i < 5) {
            if (this.itemsselectcursor == i && Key.touchmenuoptionlanguageitems[i].IsClick()) {
                this.isItemsSelect = true;
                this.itemsselectframe = 0;
                this.finalitemsselectcursor = i;
                return 0;
            }
            i++;
        }
        if ((!Key.press(524288) && (!Key.touchmenuoptionlanguagereturn.IsButtonPress() || this.itemsselectcursor != -2)) || !State.fadeChangeOver()) {
            return 0;
        }
        SoundSystem.getInstance().playSe(2);
        return 1;
    }

    private void menuOptionLanguageLogic() {
        int language = menuOptionLanguageCheck();
        if (language == 1) {
            State.fadeInit(220, 102);
            state = 9;
        } else if (language >= 2) {
            GlobalResource.languageConfig = language - 2;
            State.fadeInit(220, 102);
            state = 9;
        }
    }

    private void menuOptionLanguageDraw(MFGraphics g) {
        if (muiAniDrawer == null) {
            muiAniDrawer = new Animation("/animation/mui").getDrawer(0, false, 0);
            return;
        }
        AnimationDrawer animationDrawer;
        int i;
        int i2 = 0;
        while (i2 < 5) {
            animationDrawer = muiAniDrawer;
            if (Key.touchmenuoptionlanguageitems[i2].Isin() && this.itemsselectcursor == i2) {
                i = 1;
            } else {
                i = 0;
            }
            animationDrawer.setActionId(i + 55);
            muiAniDrawer.draw(g, SCREEN_WIDTH >> 1, ((SCREEN_HEIGHT >> 1) - 48) + (i2 * 24));
            muiAniDrawer.setActionId(i2 + 39);
            muiAniDrawer.draw(g, SCREEN_WIDTH >> 1, ((SCREEN_HEIGHT >> 1) - 48) + (i2 * 24));
            i2++;
        }
        animationDrawer = muiAniDrawer;
        if (Key.touchmenuoptionlanguagereturn.Isin()) {
            i = 5;
        } else {
            i = 0;
        }
        animationDrawer.setActionId(i + 61);
        muiAniDrawer.draw(g, 0, SCREEN_HEIGHT);
    }

    private void creditInit() {
        MyAPI.initString();
        strForShow = MyAPI.getStrings(aboutStrings[0], 11, SCREEN_WIDTH);
        this.creditStringLineNum = strForShow.length;
        this.creditOffsetY = 0;
        muiUpArrowDrawer = new Animation("/animation/mui").getDrawer(93, true, 0);
        muiDownArrowDrawer = new Animation("/animation/mui").getDrawer(94, true, 0);
        Key.touchInstructionInit();
        SoundSystem.getInstance().playBgm(33);
        this.arrowframecnt = 0;
        PageFrameCnt = 0;
        if (this.textBGImage == null) {
            this.textBGImage = MFImage.createImage("/animation/text_bg.png");
        }
        this.returnPageCursor = 0;
    }

    private void creditLogic() {
        if (Key.slidesensorhelp.isSliding()) {
            if (Key.slidesensorhelp.isSlide(Key.DIR_UP)) {
                MyAPI.logicString(true, false);
            } else if (Key.slidesensorhelp.isSlide(Key.DIR_DOWN)) {
                MyAPI.logicString(false, true);
            }
        }
        this.creditOffsetY %= (this.creditStringLineNum * LINE_SPACE) + SCREEN_HEIGHT;
        if (Key.touchhelpreturn.Isin() && Key.touchpage.IsClick()) {
            this.returnPageCursor = 1;
        }
        if ((Key.press(524288) || (Key.touchhelpreturn.IsButtonPress() && this.returnPageCursor == 1)) && State.fadeChangeOver()) {
            changeStateWithFade(9);
            this.isOptionDisFlag = false;
            SoundSystem.getInstance().playSe(2);
        }
        if (Key.touchhelpuparrow.Isin()) {
            this.arrowframecnt++;
            if (this.arrowframecnt <= 4 && !this.isArrowClicked) {
                MyAPI.logicString(false, true);
                this.isArrowClicked = true;
            } else if (this.arrowframecnt > 4 && this.arrowframecnt % 2 == 0) {
                MyAPI.logicString(false, true);
            }
        } else if (Key.touchhelpdownarrow.Isin()) {
            this.arrowframecnt++;
            if (this.arrowframecnt <= 4 && !this.isArrowClicked) {
                MyAPI.logicString(true, false);
                this.isArrowClicked = true;
            } else if (this.arrowframecnt > 4 && this.arrowframecnt % 2 == 0) {
                MyAPI.logicString(true, false);
            }
        } else {
            this.arrowframecnt = 0;
            this.isArrowClicked = false;
        }
    }

    private void creditDraw(MFGraphics g) {
        if (muiAniDrawer == null) {
            muiAniDrawer = new Animation("/animation/mui").getDrawer(0, false, 0);
        } else {
            muiAniDrawer.setActionId(63);
            PageFrameCnt++;
            PageFrameCnt %= 11;
            if (PageFrameCnt % 2 == 0) {
                PageBackGroundOffsetX++;
                PageBackGroundOffsetX %= 112;
                PageBackGroundOffsetY--;
                PageBackGroundOffsetY %= 56;
            }
            for (int x = PageBackGroundOffsetX - 112; x < (SCREEN_WIDTH * 3) / 2; x += 112) {
                for (int y = PageBackGroundOffsetY - 56; y < (SCREEN_HEIGHT * 3) / 2; y += 56) {
                    muiAniDrawer.draw(g, x, y);
                }
            }
            for (int i = 0; i < 8; i++) {
                MyAPI.drawImage(g, this.textBGImage, ((SCREEN_WIDTH >> 1) - 104) + (i * 26), (SCREEN_HEIGHT >> 1) - 72, 0);
            }
            MyAPI.drawStrings(g, strForShow, (SCREEN_WIDTH >> 1) - 104, ((SCREEN_HEIGHT >> 1) - 72) + 5, (int) Def.TOUCH_HELP_WIDTH, 131, 0, true, (int) MapManager.END_COLOR, 4656650, 0, 11);
            if (MyAPI.upPermit) {
                muiUpArrowDrawer.draw(g, (SCREEN_WIDTH >> 1) - 25, SCREEN_HEIGHT);
            }
            if (MyAPI.downPermit) {
                muiDownArrowDrawer.draw(g, (SCREEN_WIDTH >> 1) + 24, SCREEN_HEIGHT);
            }
            muiAniDrawer.setActionId((Key.touchhelpreturn.Isin() ? 5 : 0) + 61);
            muiAniDrawer.draw(g, 0, SCREEN_HEIGHT);
        }
        State.drawFade(g);
    }

    private void segaMoreLogic() {
        Key.touchMainMenuReset2();
        switch (secondEnsureLogic()) {
            case 1:
                MFDevice.openUrl(activity.segaMoreUrl, true);
                State.exitGame();
                sendMessage(new Message(), 5);
                return;
            case 2:
                gotoMainmenu();
                Key.clear();
                State.setFadeOver();
                return;
            default:
                return;
        }
    }

    private void scoreUpdateInit() {
        Key.touchgamekeyClose();
        Key.touchkeygameboardClose();
        Key.touchkeyboardInit();
        Key.touchscoreupdatekeyInit();
        changeStateWithFade(42);
        if (muiAniDrawer == null) {
            muiAniDrawer = new Animation("/animation/mui").getDrawer(0, false, 0);
        }
        if (GameState.guiAnimation == null) {
            GameState.guiAnimation = new Animation("/animation/gui");
        }
        GameState.guiAniDrawer = GameState.guiAnimation.getDrawer(0, false, 0);
        this.returnCursor = 0;
        this.scoreUpdateCursor = 0;
        this.characterRecordScoreUpdateIconY = -32;
        this.characterRecordScoreUpdateCursor = 0;
    }

    private void scoreUpdateLogic() {
        if (State.fadeChangeOver()) {
            if (Key.touchscoreupdatereturn.Isin() && Key.touchscoreupdate.IsClick()) {
                this.returnCursor = 1;
            }
            if (Key.touchscoreupdateyes.Isin() && Key.touchscoreupdate.IsClick()) {
                this.scoreUpdateCursor = 1;
            } else if (Key.touchscoreupdateno.Isin() && Key.touchscoreupdate.IsClick()) {
                this.scoreUpdateCursor = 2;
            }
            if (Key.touchscoreupdateyes.IsButtonPress() && this.scoreUpdateCursor == 1) {
                state = 43;
                secondEnsureInit();
                State.fadeInit(0, GimmickObject.GIMMICK_NUM);
                SoundSystem.getInstance().playSe(1);
            } else if (Key.touchscoreupdateno.IsButtonPress() && this.scoreUpdateCursor == 2) {
                state = 25;
                State.setFadeOver();
                SoundSystem.getInstance().playSe(1);
            }
            if ((Key.press(524288) || (Key.touchscoreupdatereturn.IsButtonPress() && this.returnCursor == 1)) && State.fadeChangeOver()) {
                state = 25;
                State.setFadeOver();
                SoundSystem.getInstance().playSe(2);
            }
        }
    }

    private void scoreUpdateDraw(MFGraphics g) {
        int i;
        g.setColor(0);
        MyAPI.fillRect(g, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
        muiAniDrawer.setActionId(52);
        for (int i2 = 0; i2 < (SCREEN_WIDTH / 48) + 1; i2++) {
            for (int j = 0; j < (SCREEN_HEIGHT / 48) + 1; j++) {
                muiAniDrawer.draw(g, i2 * 48, j * 48);
            }
        }
        this.optionOffsetX -= 4;
        this.optionOffsetX %= 128;
        muiAniDrawer.setActionId(102);
        for (int x1 = this.optionOffsetX; x1 < SCREEN_WIDTH * 2; x1 += 128) {
            muiAniDrawer.draw(g, x1, 0);
        }
        GameState.guiAniDrawer.setActionId(17);
        GameState.guiAniDrawer.draw(g, SCREEN_WIDTH >> 1, (SCREEN_HEIGHT >> 1) - 37);
        PlayerObject.drawRecordTime(g, StageManager.getTimeModeScore(PlayerObject.getCharacterID()), (Def.SCREEN_WIDTH >> 1) + 54, (Def.SCREEN_HEIGHT >> 1) - 10, 2, 2);
        muiAniDrawer.setActionId((Key.touchscoreupdateyes.Isin() ? 1 : 0) + 55);
        muiAniDrawer.draw(g, (Def.SCREEN_WIDTH >> 1) - 60, (Def.SCREEN_HEIGHT >> 1) + 28);
        muiAniDrawer.setActionId(103);
        muiAniDrawer.draw(g, (Def.SCREEN_WIDTH >> 1) - 60, (Def.SCREEN_HEIGHT >> 1) + 28);
        AnimationDrawer animationDrawer = muiAniDrawer;
        if (Key.touchscoreupdateno.Isin()) {
            i = 1;
        } else {
            i = 0;
        }
        animationDrawer.setActionId(i + 55);
        muiAniDrawer.draw(g, (Def.SCREEN_WIDTH >> 1) + 60, (Def.SCREEN_HEIGHT >> 1) + 28);
        muiAniDrawer.setActionId(104);
        muiAniDrawer.draw(g, (Def.SCREEN_WIDTH >> 1) + 60, (Def.SCREEN_HEIGHT >> 1) + 28);
        animationDrawer = muiAniDrawer;
        if (Key.touchscoreupdatereturn.Isin()) {
            i = 5;
        } else {
            i = 0;
        }
        animationDrawer.setActionId(i + 61);
        muiAniDrawer.draw(g, 0, SCREEN_HEIGHT);
        State.drawFade(g);
    }

    private void scoreUpdateEnsureLogic() {
        switch (secondEnsureLogic()) {
            case 1:
                Message msg = new Message();
                setGameScore(StageManager.getTimeModeScore(PlayerObject.getCharacterID()));
                sendMessage(msg, 6);
                state = 44;
                return;
            case 2:
                state = 42;
                State.fadeInit(GimmickObject.GIMMICK_NUM, 0);
                State.setFadeOver();
                this.returnCursor = 0;
                this.scoreUpdateCursor = 0;
                return;
            default:
                return;
        }
    }

    private void scoreUpdateEnsureDraw(MFGraphics g) {
        scoreUpdateDraw(g);
        State.drawFade(g);
        SecondEnsurePanelDraw(g, 106);
    }

    private void scoreUpdatedLogic() {
        if (activity.isResumeFromOtherActivity) {
            Standard2.splashinit(true);
            changeStateWithFade(0);
            activity.isResumeFromOtherActivity = false;
        }
    }

    private void scoreUpdatedDraw(MFGraphics g) {
    }

    private void segaMoreInit() {
        activity.buildSegaMoreUrl();
    }
}
