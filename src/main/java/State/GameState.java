package State;

import GameEngine.Def;
import GameEngine.Key;
import GameEngine.TouchKeyRange;
import Lib.Animation;
import Lib.AnimationDrawer;
import Lib.MyAPI;
import Lib.MyRandom;
import Lib.SoundSystem;
import PlatformStandard.Standard2;
import SonicGBA.BackGroundManager;
import SonicGBA.Effect;
import SonicGBA.EnemyObject;
import SonicGBA.GameObject;
import SonicGBA.GimmickObject;
import SonicGBA.GlobalResource;
import SonicGBA.MapManager;
import SonicGBA.PlayerObject;
import SonicGBA.RocketSeparateEffect;
import SonicGBA.StageManager;
import android.os.Message;
import com.sega.mobile.define.MDPhone;
import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;
import java.lang.reflect.Array;

public class GameState extends State {
    private static final byte ACTION = (byte) 4;
    private static final int BAR_COLOR = 2;
    private static final int BIRD_NUM = 10;
    private static final int BIRD_OFFSET = 2;
    private static final int BIRD_SPACE_1 = 10;
    private static final int BIRD_SPACE_2 = 14;
    private static final int BIRD_X = 0;
    private static final int BIRD_Y = 1;
    private static final int[] BP_ITEMS_HIGH = new int[]{5, 3, 5};
    private static final int[] BP_ITEMS_HIGH_NORMAL_NUM = new int[]{5, 3, 5};
    private static final int[] BP_ITEMS_LOW;
    private static final int[] BP_ITEMS_LOW_NORMAL_NUM = new int[]{5, 5, 5};
    private static final int CLOUD_NUM = 10;
    private static final int CLOUD_TYPE = 0;
    private static final int[] CLOUD_VELOCITY = new int[]{5, 4, 3};
    private static final int CLOUD_X = 1;
    private static final int CLOUD_Y = 2;
    private static final int[] COLOR_SEQ = new int[]{16777024, 16756912, 11599680, 16756800};
    private static final int DEGREE_VELOCITY = 10;
    private static final byte ED_END = (byte) 5;
    private static final byte ED_STATE_BIRD_APPEAR = (byte) 2;
    private static final byte ED_STATE_CONGRATULATION = (byte) 3;
    private static final byte ED_STATE_NO_PLANE = (byte) 0;
    private static final byte ED_STATE_PLANE_APPEAR = (byte) 1;
    private static final byte ED_STATE_STAFF = (byte) 4;
    private static final String ENDING_ANIMATION_PATH = "/animation/ending";
    private static final int FLOAT_RANGE = 10;
    public static boolean IsSingleDown = false;
    public static boolean IsSingleUp = false;
    private static boolean IsSoundVolSet = false;
    private static final int LOADING_TIME_LIMIT = 10;
    private static final byte NUM = (byte) 6;
    private static final int OPTION_MOVING_INTERVAL = 100;
    private static final int OPTION_MOVING_SPEED = 4;
    private static final int[] OPTION_SE = new int[]{55, 144};
    private static final int[] OPTION_SOUND_NO_VOLUME = new int[]{55, 144};
    private static final int[] OPTION_SOUND_VOLUME = new int[]{55, 58, 58, 58, 57, 57, 57, 57, 56, 56, 56};
    private static final int[] OPTION_SOUND = OPTION_SOUND_VOLUME;
    private static final int[] OPTION_TAG_HAS_SE = new int[]{54, StringIndex.STR_SE};
    private static final int[] OPTION_TAG = OPTION_TAG_HAS_SE;
    private static final int OPTION_ELEMENT_NUM = OPTION_TAG.length;
    private static final int[] OPTION_TAG_NO_SE = new int[]{54};
    private static final int[][] OPTION_SELECTOR_HAS_SE = new int[][]{OPTION_SOUND, OPTION_SE};
    private static final int[][] OPTION_SELECTOR_NO_SE = new int[][]{OPTION_SOUND};
    private static final int[][] OPTION_SELECTOR = OPTION_SELECTOR_HAS_SE;
    private static final int PAUSE_NORMAL_INSTRUCTION = 4;
    private static int[] PAUSE_NORMAL_MODE = null;
    private static final int[] PAUSE_NORMAL_MODE_NOSHOP;
    private static final int[] PAUSE_NORMAL_MODE_SHOP;
    private static final int PAUSE_NORMAL_OPTION = 3;
    private static final int PAUSE_NORMAL_RESUME = 0;
    private static final int PAUSE_NORMAL_SHOP = 2;
    private static final int PAUSE_NORMAL_TO_TITLE = 1;
    private static final int PAUSE_OPTION_ITEMS_NUM = 6;
    private static final int PAUSE_RACE_INSTRUCTION = 5;
    private static final int PAUSE_RACE_OPTION = 4;
    private static final int PAUSE_RACE_RESUME = 0;
    private static final int PAUSE_RACE_RETRY = 1;
    private static final int PAUSE_RACE_SELECT_STAGE = 2;
    private static final int PAUSE_RACE_TO_TITLE = 3;
    private static final int PLANE_VELOCITY = 2;
    private static final byte POSX = (byte) 0;
    private static final byte POSY = (byte) 1;
    private static final byte PRESS_DELAY = (byte) 5;
    public static boolean PreGame = false;
    private static final byte SAW = (byte) 0;
    private static final byte SONIC = (byte) 3;
    private static final int STAFF_CENTER_Y = ((SCREEN_HEIGHT * 2) / 7);
    private static final int STAFF_SHOW_TIME = 45;
    private static String[] STAFF_STR = null;
    private static final byte STAGE_NAME = (byte) 2;
    private static final byte STAGE_NAME_S = (byte) 5;
    private static final byte STATE_ALL_CLEAR = (byte) 14;
    private static final byte STATE_BP_BUY = (byte) 23;
    private static final byte STATE_BP_CONTINUE_TRY = (byte) 19;
    private static final byte STATE_BP_REVIVE = (byte) 21;
    private static final byte STATE_BP_SHOP = (byte) 22;
    private static final byte STATE_BP_TOOLS_MAX = (byte) 24;
    private static final byte STATE_BP_TOOLS_USE = (byte) 25;
    private static final byte STATE_BP_TOOLS_USE_ENSURE = (byte) 26;
    private static final byte STATE_BP_TRY_PAYING = (byte) 20;
    private static final byte STATE_CONTINUE_0 = (byte) 41;
    private static final byte STATE_CONTINUE_1 = (byte) 42;
    private static final byte STATE_GAME = (byte) 0;
    private static final byte STATE_GAME_OVER_1 = (byte) 12;
    private static final byte STATE_GAME_OVER_2 = (byte) 13;
    private static final byte STATE_GAME_OVER_PRE = (byte) 28;
    private static final byte STATE_INTERRUPT = (byte) 18;
    private static final byte STATE_PAUSE = (byte) 1;
    private static final byte STATE_PAUSE_INSTRUCTION = (byte) 6;
    private static final byte STATE_PAUSE_OPTION = (byte) 7;
    private static final byte STATE_PAUSE_OPTION_HELP = (byte) 34;
    private static final byte STATE_PAUSE_OPTION_KEY_CONTROL = (byte) 32;
    private static final byte STATE_PAUSE_OPTION_SENSOR = (byte) 39;
    private static final byte STATE_PAUSE_OPTION_SOUND = (byte) 30;
    private static final byte STATE_PAUSE_OPTION_SOUND_VOLUMN = (byte) 38;
    private static final byte STATE_PAUSE_OPTION_SP_SET = (byte) 33;
    private static final byte STATE_PAUSE_OPTION_VIB = (byte) 31;
    private static final byte STATE_PAUSE_RETRY = (byte) 8;
    private static final byte STATE_PAUSE_SELECT_CHARACTER = (byte) 29;
    private static final byte STATE_PAUSE_SELECT_STAGE = (byte) 9;
    private static final byte STATE_PAUSE_TO_TITLE = (byte) 10;
    private static final byte STATE_PRE_ALL_CLEAR = (byte) 40;
    private static final byte STATE_PRE_GAME_0 = (byte) 36;
    private static final byte STATE_PRE_GAME_0_TYPE2 = (byte) 37;
    private static final byte STATE_PRE_GAME_1 = (byte) 15;
    private static final byte STATE_PRE_GAME_1_TYPE2 = (byte) 27;
    private static final byte STATE_PRE_GAME_2 = (byte) 16;
    private static final byte STATE_PRE_GAME_3 = (byte) 17;
    private static final byte STATE_SCORE_UPDATE = (byte) 43;
    private static final byte STATE_SCORE_UPDATED = (byte) 45;
    private static final byte STATE_SCORE_UPDATE_ENSURE = (byte) 44;
    private static final byte STATE_SET_PARAM = (byte) 3;
    private static final byte STATE_STAGE_LOADING = (byte) 5;
    private static final byte STATE_STAGE_LOADING_TURN = (byte) 35;
    public static final byte STATE_STAGE_PASS = (byte) 4;
    private static final byte STATE_STAGE_SELECT = (byte) 2;
    private static final byte STATE_TIME_OVER_0 = (byte) 11;
    private static final boolean USE_NARRAW_TEXT_TIPS = false;
    private static final int VISIBLE_OPTION_ITEMS_NUM = 9;
    private static final byte VX = (byte) 2;
    private static final byte VY = (byte) 3;
    private static final byte WHITE_BAR = (byte) 1;
    private static int[] currentBPItems = BP_ITEMS_HIGH;
    private static int[] currentBPItemsNormalNum = BP_ITEMS_HIGH_NORMAL_NUM;
    public static AnimationDrawer guiAniDrawer;
    public static Animation guiAnimation;
    public static boolean isBackFromSpStage;
    public static boolean isThroughGame = false;
    private static AnimationDrawer numberDrawer;
    private static int spCheckPointID;
    private static int spReserveRingNum;
    private static int spTimeCount;
    private static String[][] staffStringForShow = null;
    public static AnimationDrawer stageInfoAniDrawer;
    private static String[] tipsForShow;
    private static String[] tipsString;
    private static int tool_x = 40;
    private static int tool_y = 17;
    int BP_CONTINUETRY_MENU_HEIGHT;
    int BP_CONTINUETRY_MENU_START_X;
    int BP_CONTINUETRY_MENU_START_Y;
    int BP_CONTINUETRY_MENU_WIDTH;
    private boolean BP_IsFromContinueTry;
    private boolean IsActNumDrawable;
    private boolean IsPlayerNameDrawable;
    private String[] TIPS;
    private int TIPS_OFFSET_X;
    private int TIPS_TEXT_INTERVAL_Y;
    private int TIPS_TEXT_OFFSET_Y;
    private int TIPS_TITLE_OFFSET_Y;
    private int allclearFrame;
    private AnimationDrawer[] birdDrawer;
    private int[][] birdInfo;
    private int birdX;
    private boolean changing;
    private int cloudCount;
    private AnimationDrawer cloudDrawer;
    private int[][] cloudInfo;
    private int cnt;
    private int colorCursor;
    private int continueCursor;
    private int continueFrame;
    private int continueMoveBlackBarX;
    private int continueMoveNumberX;
    private int continueNumber;
    private float continueNumberScale;
    private int continueNumberState;
    private float continueScale;
    private int continueStartEndFrame;
    private int count;
    private int degree;
    private int[][] display;
    private byte endingState;
    private MFImage exendBg1Image;
    private MFImage exendBgImage;
    private boolean fadeChangeState;
    private int frameCount;
    private int gameoverCnt;
    private AnimationDrawer interruptDrawer;
    private int interrupt_state;
    private boolean isChanged;
    private boolean isOptionChange;
    private boolean isOptionDisFlag;
    private boolean isSelectable;
    private boolean isStateClassSwitch;
    private int itemOffsetX;
    private AnimationDrawer loadingDrawer;
    private long loadingStartTime;
    private AnimationDrawer loadingWordsDrawer;
    private int movingTitleSpeedX;
    private int movingTitleX;
    private int nextState;
    private int offsetOfVolumeInterface;
    private int[] optionCursor;
    private int optionDrawOffsetBottomY;
    private int optionDrawOffsetTmpY1;
    private int optionDrawOffsetY;
    private int optionMenuCursor;
    private int optionOffsetTmp;
    private int optionOffsetX;
    private int optionOffsetY;
    private int optionOffsetYAim;
    private boolean optionReturnFlag;
    private int optionYDirect;
    private int optionslide_getprey;
    private int optionslide_gety;
    private int optionslide_y;
    private int optionslidefirsty;
    private boolean outing;
    public int overcnt;
    private int overtitleID;
    private int pauseOptionCursor;
    private int pause_item_cursor;
    private int pause_item_speed;
    private int pause_item_x;
    private int pause_item_y;
    private boolean pause_optionFlag;
    private int pause_optionframe;
    private boolean pause_returnFlag;
    private int pause_returnframe;
    private int pause_saw_speed;
    private int pause_saw_x;
    private int pause_saw_y;
    private int pausecnt;
    private AnimationDrawer planeDrawer;
    private int planeX;
    private int planeY;
    private int position;
    private int preScore;
    private byte pressDelay;
    private int racemode_cnt;
    private int scoreUpdateCursor;
    private int showCount;
    private AnimationDrawer stageInfoActNumDrawer;
    private Animation[] stageInfoClearAni;
    private AnimationDrawer stageInfoPlayerNameDrawer;
    public int state;
    private int stateForSet;
    private int stringCursor;

    private void initTips() {
        if (this.loadingWordsDrawer == null || this.loadingDrawer == null) {
            Animation animation = new Animation("/animation/loading");
            this.loadingWordsDrawer = animation.getDrawer(0, true, 0);
            this.loadingDrawer = animation.getDrawer(0, false, 0);
        }
        this.TIPS = null;
        switch (PlayerObject.getCharacterID()) {
            case 0:
                if (StageManager.getCurrentZoneId() != 8) {
                    this.TIPS = MyAPI.loadText("/tip/tips_sonic");
                    break;
                } else {
                    this.TIPS = MyAPI.loadText("/tip/tips_ssonic");
                    break;
                }
            case 1:
                this.TIPS = MyAPI.loadText("/tip/tips_tails");
                break;
            case 2:
                this.TIPS = MyAPI.loadText("/tip/tips_knuckles");
                break;
            case 3:
                this.TIPS = MyAPI.loadText("/tip/tips_amy");
                break;
        }
        MyAPI.initString();
        this.loadingStartTime = System.currentTimeMillis();
        tipsForShow = null;
        State.fadeInit(255, 0);
    }

    public GameState() {
        this.state = 2;
        this.overtitleID = 0;
        this.movingTitleX = 0;
        this.movingTitleSpeedX = 0;
        this.display = (int[][]) Array.newInstance(Integer.TYPE, new int[]{6, 4});
        this.TIPS = null;
        this.loadingStartTime = 0;
        this.TIPS_OFFSET_X = 16;
        this.TIPS_TITLE_OFFSET_Y = 0;
        this.TIPS_TEXT_OFFSET_Y = (FONT_H - 0) - 0;
        this.TIPS_TEXT_INTERVAL_Y = (FONT_H - 0) - 0;
        this.racemode_cnt = 60;
        this.optionCursor = new int[OPTION_ELEMENT_NUM];
        this.isOptionDisFlag = false;
        this.optionslide_getprey = -1;
        this.optionslide_gety = -1;
        this.offsetOfVolumeInterface = 0;
        this.pressDelay = (byte) 5;
        this.cloudInfo = (int[][]) Array.newInstance(Integer.TYPE, new int[]{10, 3});
        this.cloudCount = 0;
        this.birdInfo = (int[][]) Array.newInstance(Integer.TYPE, new int[]{10, 3});
        this.BP_IsFromContinueTry = false;
        this.BP_CONTINUETRY_MENU_START_X = FRAME_X;
        this.BP_CONTINUETRY_MENU_START_Y = this.MORE_GAME_START_Y;
        this.BP_CONTINUETRY_MENU_WIDTH = FRAME_WIDTH;
        this.BP_CONTINUETRY_MENU_HEIGHT = this.MORE_GAME_HEIGHT;
        this.overcnt = 0;
        this.IsPlayerNameDrawable = false;
        this.IsActNumDrawable = false;
        this.state = 5;
        loadingType = 0;
        State.fadeInit(255, 0);
        fading = false;
        isBackFromSpStage = false;
        initTips();
    }

    public GameState(int stateID) {
        this();
        switch (stateID) {
            case 7:
                isBackFromSpStage = true;
                isThroughGame = true;
                return;
            default:
                return;
        }
    }

    private boolean loadingEnd() {
        return System.currentTimeMillis() - this.loadingStartTime >= 10000 && StageManager.loadStageStep();
    }

    public void logic() {
        if (this.state != 18) {
            fadeStateLogic();
        }
        int[] iArr;
        switch (this.state) {
            case STATE_GAME:
                if (!(StageManager.isStagePassTimePause() || GameObject.IsGamePause)) {
                    PlayerObject.timeLogic();
                }
                GameObject.logicObjects();
                StageManager.stageLogic();
                if (Key.buttonPress(2)) {
                }
                if ((Key.touchkey_pause.IsButtonPress() || Key.press(524288)) && State.fadeChangeOver() && !GameObject.player.isDead) {
                    if (!StageManager.isStagePassTimePause()) {
                        PlayerObject.gamepauseInit();
                        this.state = 1;
                        gamePauseInit();
                        isDrawTouchPad = false;
                        SoundSystem.getInstance().playSe(33);
                        Key.init();
                        GameObject.IsGamePause = true;
                        State.fadeInit_Modify(0, 102);
                        SoundSystem.getInstance().stopBgm(false);
                        SoundSystem.getInstance().stopLongSe();
                        SoundSystem.getInstance().stopLoopSe();
                        this.pauseoptionCursor = GlobalResource.soundConfig;
                        Key.touchgamekeyClose();
                        Key.touchkeygameboardClose();
                        Key.touchkeyboardInit();
                    }
                } else if (!(Key.press(Key.B_0) || Key.press(Key.B_PO) || !Key.press(Key.B_S1) || !State.IsToolsCharge() || GameObject.stageModeState != 0 || GameObject.player.isDead || StageManager.isStagePassTimePause())) {
                    PlayerObject.gamepauseInit();
                    this.state = 25;
                    this.tooltipY = TOOL_TIP_Y_DES_2;
                    Key.init();
                    GameObject.IsGamePause = true;
                    State.fadeInit(0, 102);
                    SoundSystem.getInstance().stopBgm(false);
                    this.pauseoptionCursor = GlobalResource.soundConfig;
                    Key.touchgamekeyClose();
                    Key.touchkeygameboardClose();
                    Key.touchkeyboardInit();
                }
                if (StageManager.isStagePass()) {
                    this.state = 4;
                    isDrawTouchPad = false;
                    if (StageManager.IsStageEnd()) {
                        PlayerObject.isbarOut = true;
                        this.state = 40;
                        State.setFadeColor(MapManager.END_COLOR);
                        State.fadeInit(0, 255);
                    }
                } else {
                    PlayerObject.initMovingBar();
                    this.cnt = 0;
                    PlayerObject.IsStarttoCnt = false;
                    StageManager.IsCalculateScore = true;
                    PlayerObject.IsDisplayRaceModeNewRecord = false;
                }
                if (StageManager.isStageRestart()) {
                    this.state = 5;
                    loadingType = 0;
                    Key.touchgamekeyClose();
                    Key.touchkeygameboardClose();
                    Key.touchkeyboardInit();
                    initTips();
                    State.fadeInit(255, 0);
                    return;
                }
                if (StageManager.isStageGameover()) {
                    if (State.IsToolsCharge() && PlayerObject.stageModeState == 0) {
                        BP_gotoRevive();
                    } else {
                        gotoGameOver();
                    }
                }
                if (StageManager.isStageTimeover()) {
                    IsTimeOver = true;
                    this.overcnt = 0;
                    this.state = 28;
                    this.movingTitleX = SCREEN_WIDTH + 56;
                    if (StageManager.isStageGameover()) {
                        IsTimeOver = false;
                        IsGameOver = true;
                        SoundSystem.getInstance().stopBgm(true);
                        SoundSystem.getInstance().playBgm(30, false);
                    } else {
                        this.overtitleID = Def.TOUCH_HELP_HEIGHT;
                        SoundSystem.getInstance().playSe(40);
                    }
                    Key.touchgamekeyClose();
                    Key.touchkeygameboardClose();
                    Key.touchkeyboardInit();
                }
                if (IsGameOver) {
                    this.overtitleID = 78;
                }
                if (this.overtitleID == 78) {
                    this.movingTitleSpeedX = 24;
                    return;
                } else if (this.overtitleID == Def.TOUCH_HELP_HEIGHT) {
                    this.movingTitleSpeedX = 8;
                    return;
                } else {
                    return;
                }
            case 1:
                gamePauseLogic();
                return;
            case 4:
                stagePassLogic();
                if (StageManager.getStageID() != 12 || StageManager.isGoingToExtraStage()) {
                    GameObject.logicObjects();
                }
                StageManager.stageLogic();
                if (StageManager.isStagePassTimePause() && StageManager.IsCalculateScore) {
                    StageManager.IsCalculateScore = false;
                    PlayerObject.calculateScore();
                    return;
                }
                return;
            case 5:
                PlayerObject.isTerminal = false;
                if (SoundSystem.getInstance().bgmPlaying()) {
                    SoundSystem.getInstance().stopBgm(false);
                }
                PreGame = true;
                if (loadingEnd()) {
                    Animation.closeAnimationDrawer(this.loadingWordsDrawer);
                    this.loadingWordsDrawer = null;
                    Animation.closeAnimationDrawer(this.loadingDrawer);
                    this.loadingDrawer = null;
                    System.gc();
                    if (isThroughGame) {
                        isThroughGame = false;
                    }
                    if (!(isBackFromSpStage || GameObject.stageModeState == 1)) {
                        StageManager.characterFromGame = PlayerObject.getCharacterID();
                        StageManager.stageIDFromGame = StageManager.getStageID();
                        StageManager.saveStageRecord();
                    }
                    StageManager.isSaveTimeModeScore = false;
                    MapManager.setFocusObj(GameObject.player);
                    GameObject.player.headInit();
                    GameObject.bossFighting = false;
                    GameObject.isUnlockCage = false;
                    EnemyObject.isBossEnter = false;
                    if (isBackFromSpStage) {
                        PlayerObject.initSpParam(spReserveRingNum, spCheckPointID, spTimeCount);
                        isBackFromSpStage = false;
                    }
                    if (!(StageManager.getStageID() == 10 && PlayerObject.isDeadLineEffect)) {
                        PlayerObject.isDeadLineEffect = false;
                    }
                    Key.initSonic();
                    IsGameOver = false;
                    if (StageManager.isNextGameStageDirectedly) {
                        this.state = 36;
                        initStageIntroType1Conf();
                    } else {
                        this.state = 36;
                        initStageIntroType1Conf();
                    }
                    initStageInfoClearRes();
                    PlayerObject.clipMoveInit(0, (SCREEN_HEIGHT >> 1) - 10, 20, SCREEN_WIDTH, 60);
                    Key.touchkeypauseInit();
                    Key.touchkeyboardClose();
                    Key.touchkeygameboardInit();
                    StageManager.stagePassInit();
                    return;
                }
                return;
            case 6:
                helpLogic();
                if (Key.press(2)) {
                }
                if (Key.press(524288)) {
                    this.state = 1;
                    GameObject.IsGamePause = true;
                    Key.clear();
                    this.isOptionDisFlag = false;
                    return;
                }
                return;
            case 7:
                optionLogic();
                if (Key.press(2)) {
                }
                if (Key.press(524288)) {
                    this.state = 1;
                    GameObject.IsGamePause = true;
                    Key.clear();
                    SoundSystem.getInstance().stopBgm(false);
                    State.fadeInit_Modify(192, 102);
                    return;
                }
                return;
            case 8:
                retryStageLogic();
                return;
            case 9:
                pausetoSelectStageLogic();
                return;
            case 10:
                pausetoTitleLogic();
                return;
            case 11:
                if (this.movingTitleX - this.movingTitleSpeedX < (SCREEN_WIDTH >> 1)) {
                    this.movingTitleX = SCREEN_WIDTH;
                    State.fadeInit(0, 255);
                    this.state = 12;
                    return;
                }
                this.movingTitleX -= this.movingTitleSpeedX;
                return;
            case 12:
                if (State.fadeChangeOver()) {
                    this.state = 13;
                    this.gameoverCnt = 0;
                    return;
                }
                return;
            case 13:
                if (State.fadeChangeOver()) {
                    if (IsGameOver) {
                        if ((SoundSystem.getInstance().bgmPlaying() || GlobalResource.soundSwitchConfig == 0) && !(GlobalResource.soundSwitchConfig == 0 && this.gameoverCnt == 128)) {
                            this.gameoverCnt++;
                        } else {
                            if (PlayerObject.stageModeState == 0) {
                                Standard2.splashinit(true);
                                setStateWithFade(0);
                            } else if (PlayerObject.stageModeState == 1) {
                                setStateWithFade(3);
                            }
                            Key.touchkeyboardClose();
                            Key.touchkeyboardInit();
                        }
                    }
                    if (!IsTimeOver) {
                        return;
                    }
                    if (this.gameoverCnt == 128) {
                        this.state = 5;
                        loadingType = 0;
                        StageManager.setStageRestart();
                        StageManager.checkPointTime = 0;
                        State.fadeInit(255, 0);
                        initTips();
                        return;
                    }
                    this.gameoverCnt++;
                    return;
                }
                return;
            case 14:
                if (State.fadeChangeOver()) {
                    this.allclearFrame++;
                    if (this.allclearFrame > 20) {
                        stagePassLogic();
                        return;
                    }
                    return;
                }
                return;
            case 15:
                if (this.frameCount < 10) {
                    this.frameCount++;
                } else {
                    releaseTips();
                    this.state = 16;
                    Key.touchanykeyInit();
                    State.fadeInit(204, 0);
                }
                if (this.frameCount < 1 || this.frameCount > 3) {
                    this.display[0][2] = 0;
                } else {
                    this.display[0][2] = 30;
                }
                if (this.frameCount < 4 || this.frameCount > 7) {
                    this.display[1][2] = 0;
                } else {
                    this.display[1][2] = -96;
                }
                if (this.frameCount < 8 || this.frameCount > 10) {
                    this.display[2][2] = 0;
                } else {
                    this.display[2][2] = -104;
                }
                if (this.frameCount < 6 || this.frameCount > 7) {
                    this.display[5][2] = 0;
                } else {
                    this.display[5][2] = -104;
                }
                if (this.frameCount == 7) {
                    this.IsPlayerNameDrawable = true;
                    this.stageInfoPlayerNameDrawer.restart();
                }
                if (this.frameCount == 9) {
                    this.IsActNumDrawable = true;
                    this.stageInfoActNumDrawer.restart();
                }
                if (this.display[0][0] + this.display[0][2] > 0) {
                    this.display[0][0] = 0;
                } else {
                    iArr = this.display[0];
                    iArr[0] = iArr[0] + this.display[0][2];
                }
                iArr = this.display[0];
                iArr[1] = iArr[1] + this.display[0][3];
                iArr = this.display[1];
                iArr[0] = iArr[0] + this.display[1][2];
                if (this.display[1][0] < 0) {
                    this.display[1][0] = 0;
                }
                iArr = this.display[1];
                iArr[1] = iArr[1] + this.display[1][3];
                iArr = this.display[2];
                iArr[0] = iArr[0] + this.display[2][2];
                while (this.display[2][0] < 0) {
                    iArr = this.display[2];
                    iArr[0] = iArr[0] + 224;
                }
                iArr = this.display[2];
                iArr[0] = iArr[0] % 224;
                iArr = this.display[2];
                iArr[1] = iArr[1] + this.display[2][3];
                iArr = this.display[4];
                iArr[0] = iArr[0] + this.display[4][2];
                if (this.display[5][0] + this.display[5][2] < SCREEN_WIDTH - 112) {
                    this.display[5][0] = SCREEN_WIDTH - 112;
                } else {
                    iArr = this.display[5];
                    iArr[0] = iArr[0] + this.display[5][2];
                }
                if (this.state == 16) {
                    this.frameCount = 0;
                }
                GameObject.setNoInput();
                GameObject.logicObjects();
                return;
            case 16:
                if (this.frameCount < 48) {
                    this.frameCount++;
                } else {
                    this.state = 17;
                    Key.clear();
                    Key.touchanykeyClose();
                }
                this.display[2][2] = -7;
                this.display[4][3] = 0;
                iArr = this.display[0];
                iArr[0] = iArr[0] + this.display[0][2];
                iArr = this.display[0];
                iArr[1] = iArr[1] + this.display[0][3];
                iArr = this.display[1];
                iArr[0] = iArr[0] + this.display[1][2];
                iArr = this.display[1];
                iArr[1] = iArr[1] + this.display[1][3];
                iArr = this.display[2];
                iArr[0] = iArr[0] + this.display[2][2];
                while (this.display[2][0] < 0) {
                    iArr = this.display[2];
                    iArr[0] = iArr[0] + 224;
                }
                iArr = this.display[2];
                iArr[0] = iArr[0] % 224;
                iArr = this.display[2];
                iArr[1] = iArr[1] + this.display[2][3];
                iArr = this.display[3];
                iArr[0] = iArr[0] + this.display[3][2];
                iArr = this.display[3];
                iArr[1] = iArr[1] + this.display[3][3];
                iArr = this.display[4];
                iArr[0] = iArr[0] + this.display[4][2];
                iArr = this.display[4];
                iArr[1] = iArr[1] + this.display[4][3];
                iArr = this.display[5];
                iArr[0] = iArr[0] + this.display[5][2];
                if (Key.press(Key.B_SEL)) {
                    this.state = 17;
                    Key.clear();
                    Key.touchanykeyClose();
                    State.fadeInit(State.getCurrentFade(), 0);
                }
                if (this.state == 17) {
                    this.frameCount = 0;
                }
                GameObject.setNoInput();
                GameObject.logicObjects();
                return;
            case 17:
                if (this.frameCount < 3) {
                    this.frameCount++;
                } else {
                    PlayerObject.isNeedPlayWaterSE = true;
                    this.state = 0;
                    PreGame = false;
                }
                this.display[0][2] = -30;
                this.display[1][2] = -97;
                this.display[2][2] = -105;
                this.display[5][2] = -105;
                this.display[3][3] = -75;
                this.display[4][3] = 45;
                iArr = this.display[0];
                iArr[0] = iArr[0] + this.display[0][2];
                iArr = this.display[0];
                iArr[1] = iArr[1] + this.display[0][3];
                iArr = this.display[1];
                iArr[0] = iArr[0] + this.display[1][2];
                iArr = this.display[1];
                iArr[1] = iArr[1] + this.display[1][3];
                iArr = this.display[2];
                iArr[0] = iArr[0] + this.display[2][2];
                iArr = this.display[5];
                iArr[0] = iArr[0] + this.display[5][2];
                iArr = this.display[2];
                iArr[1] = iArr[1] + this.display[2][3];
                iArr = this.display[3];
                iArr[0] = iArr[0] + this.display[3][2];
                iArr = this.display[3];
                iArr[1] = iArr[1] + this.display[3][3];
                iArr = this.display[4];
                iArr[0] = iArr[0] + this.display[4][2];
                iArr = this.display[4];
                iArr[1] = iArr[1] + this.display[4][3];
                if (this.state == 0) {
                    isDrawTouchPad = true;
                    this.frameCount = 0;
                    State.fadeInit(102, 0);
                    State.setFadeOver();
                }
                GameObject.setNoInput();
                GameObject.logicObjects();
                return;
            case 18:
                interruptLogic();
                return;
            case 27:
                if (this.frameCount < 18) {
                    this.frameCount++;
                } else {
                    releaseTips();
                    this.state = 16;
                    Key.touchanykeyInit();
                    State.fadeInit(204, 0);
                }
                if (this.frameCount < 9 || this.frameCount > 11) {
                    this.display[0][2] = 0;
                } else {
                    this.display[0][2] = 30;
                }
                if (this.frameCount < 12 || this.frameCount > 15) {
                    this.display[1][3] = 0;
                } else {
                    this.display[1][3] = 28;
                }
                if (this.frameCount < 16 || this.frameCount > 18) {
                    this.display[2][2] = 0;
                } else {
                    this.display[2][2] = -104;
                }
                if (this.frameCount < 14 || this.frameCount > 15) {
                    this.display[5][2] = 0;
                } else {
                    this.display[5][2] = -104;
                }
                if (this.frameCount == 15) {
                    this.IsPlayerNameDrawable = true;
                    this.stageInfoPlayerNameDrawer.restart();
                }
                if (this.frameCount == 17) {
                    this.IsActNumDrawable = true;
                    this.stageInfoActNumDrawer.restart();
                }
                if (this.display[0][0] + this.display[0][2] > 0) {
                    this.display[0][0] = 0;
                } else {
                    iArr = this.display[0];
                    iArr[0] = iArr[0] + this.display[0][2];
                }
                iArr = this.display[0];
                iArr[1] = iArr[1] + this.display[0][3];
                iArr = this.display[1];
                iArr[0] = iArr[0] + this.display[1][2];
                if (this.display[1][0] < 0) {
                    this.display[1][0] = 0;
                }
                if (this.display[1][1] + this.display[1][3] > (SCREEN_HEIGHT >> 1) + 48) {
                    this.display[1][1] = (SCREEN_HEIGHT >> 1) + 48;
                } else {
                    iArr = this.display[1];
                    iArr[1] = iArr[1] + this.display[1][3];
                }
                iArr = this.display[2];
                iArr[0] = iArr[0] + this.display[2][2];
                while (this.display[2][0] < 0) {
                    iArr = this.display[2];
                    iArr[0] = iArr[0] + 224;
                }
                iArr = this.display[2];
                iArr[0] = iArr[0] % 224;
                iArr = this.display[2];
                iArr[1] = iArr[1] + this.display[2][3];
                iArr = this.display[4];
                iArr[0] = iArr[0] + this.display[4][2];
                if (this.display[5][0] + this.display[5][2] < SCREEN_WIDTH - 112) {
                    this.display[5][0] = SCREEN_WIDTH - 112;
                } else {
                    iArr = this.display[5];
                    iArr[0] = iArr[0] + this.display[5][2];
                }
                if (this.state == 16) {
                    this.frameCount = 0;
                }
                GameObject.setNoInput();
                GameObject.logicObjects();
                return;
            case 28:
                this.overcnt++;
                if (this.overcnt == 20) {
                    isDrawTouchPad = false;
                }
                if (this.overtitleID == 78) {
                    if (this.overcnt == 36) {
                        this.state = 41;
                        return;
                    }
                    return;
                } else if (this.overtitleID == Def.TOUCH_HELP_HEIGHT && this.overcnt == 28) {
                    this.state = 11;
                    return;
                } else {
                    return;
                }
            case 29:
                pausetoSelectCharacterLogic();
                return;
            case 30:
                pauseOptionSoundLogic();
                return;
            case 31:
                pauseOptionVibrationLogic();
                return;
            case 33:
                pauseOptionSpSetLogic();
                return;
            case 34:
                helpLogic();
                if ((Key.press(524288) || (Key.touchhelpreturn.IsButtonPress() && this.returnPageCursor == 1)) && State.fadeChangeOver()) {
                    changeStateWithFade(7);
                    GameObject.IsGamePause = true;
                    this.isOptionDisFlag = false;
                    SoundSystem.getInstance().playSe(2);
                    this.returnCursor = 0;
                    return;
                }
                return;
            case 35:
                if (State.fadeChangeOver()) {
                    this.state = 5;
                    MapManager.closeMap();
                    initTips();
                    return;
                }
                return;
            case 36:
                if (State.fadeChangeOver()) {
                    this.state = 15;
                    State.fadeInit(255, 204);
                    return;
                }
                return;
            case 37:
                if (State.fadeChangeOver()) {
                    this.state = 27;
                    State.fadeInit(255, 204);
                    return;
                }
                return;
            case 38:
                switch (soundVolumnLogic()) {
                    case 2:
                        State.fadeInit(190, 0);
                        this.state = 7;
                        this.returnCursor = 0;
                        return;
                    default:
                        return;
                }
            case 39:
                switch (spSenorSetLogic()) {
                    case 1:
                        GlobalResource.sensorConfig = 0;
                        State.fadeInit(190, 0);
                        this.state = 7;
                        this.returnCursor = 0;
                        return;
                    case 2:
                        GlobalResource.sensorConfig = 1;
                        State.fadeInit(190, 0);
                        this.state = 7;
                        this.returnCursor = 0;
                        return;
                    case 3:
                        State.fadeInit(190, 0);
                        this.state = 7;
                        this.returnCursor = 0;
                        return;
                    case 4:
                        GlobalResource.sensorConfig = 2;
                        State.fadeInit(190, 0);
                        this.state = 7;
                        this.returnCursor = 0;
                        return;
                    default:
                        return;
                }
            case 40:
                if (State.fadeChangeOver()) {
                    PlayerObject.calculateScore();
                    this.state = 14;
                    State.fadeInit(255, 0);
                    this.exendBgImage = null;
                    this.exendBg1Image = null;
                    this.exendBgImage = MFImage.createImage("/animation/ending/ed_ex_moon_bg.png");
                    this.exendBg1Image = MFImage.createImage("/animation/ending/ed_ex_forest.png");
                    SoundSystem.getInstance().stopBgm(false);
                    this.allclearFrame = 0;
                    return;
                }
                return;
            case 41:
                if (this.movingTitleX - this.movingTitleSpeedX < (SCREEN_WIDTH >> 1)) {
                    this.movingTitleX = SCREEN_WIDTH;
                    State.fadeInit(0, 102);
                    continueInit();
                    return;
                }
                this.movingTitleX -= this.movingTitleSpeedX;
                return;
            case 42:
                this.continueFrame++;
                if (this.continueFrame <= 5) {
                    this.continueScale -= 0.2f;
                    return;
                } else if (this.continueFrame <= 10) {
                    this.continueScale += 0.2f;
                    if (this.continueScale > 1.0f) {
                        this.continueScale = 1.0f;
                        return;
                    }
                    return;
                } else if (this.continueFrame > 10) {
                    if (this.continueMoveBlackBarX + (SCREEN_WIDTH / 6) > 0) {
                        this.continueMoveBlackBarX = 0;
                    } else {
                        this.continueMoveBlackBarX += SCREEN_WIDTH / 6;
                    }
                    if (this.continueMoveBlackBarX == 0) {
                        if (this.continueNumberState == 0) {
                            this.continueMoveNumberX += SCREEN_WIDTH / 12;
                            if (this.continueMoveNumberX >= (SCREEN_WIDTH >> 1)) {
                                this.continueMoveNumberX = SCREEN_WIDTH >> 1;
                                this.continueNumberState = 1;
                                this.continueNumberScale = 2.0f;
                            }
                        } else if (this.continueNumberState == 1) {
                            this.continueNumberScale -= 0.125f;
                            if (this.continueNumberScale <= 1.0f) {
                                this.continueNumberScale = 1.0f;
                                this.continueNumberState = 2;
                            }
                        } else if (this.continueNumberState == 2) {
                            this.continueMoveNumberX += SCREEN_WIDTH / 12;
                            if (this.continueMoveNumberX >= SCREEN_WIDTH + 30) {
                                this.continueMoveNumberX = -30;
                                this.continueNumberState = 0;
                                this.continueNumber--;
                                if (this.continueNumber == -1) {
                                    continueEnd();
                                }
                            }
                        } else if (this.continueNumberState == 3) {
                            if (this.continueFrame == this.continueStartEndFrame + 3) {
                                State.fadeInit(102, 255);
                            } else if (this.continueFrame > this.continueStartEndFrame + 3 && State.fadeChangeOver()) {
                                if (this.continueScale - 0.2f > 0.0f) {
                                    this.continueScale -= 0.2f;
                                } else {
                                    this.continueScale = 0.0f;
                                }
                                if (this.continueScale == 0.0f) {
                                    Standard2.splashinit(true);
                                    State.setState(0);
                                }
                            }
                        } else if (this.continueNumberState == 4) {
                            this.continueMoveNumberX += SCREEN_WIDTH / 12;
                            if (this.continueMoveNumberX >= (SCREEN_WIDTH >> 1)) {
                                this.continueMoveNumberX = SCREEN_WIDTH >> 1;
                                this.continueNumberState = 5;
                                this.continueNumberScale = 2.0f;
                            }
                        } else if (this.continueNumberState == 5) {
                            this.continueNumberScale -= 0.125f;
                            if (this.continueNumberScale <= 1.0f) {
                                this.continueNumberScale = 1.0f;
                                this.continueNumberState = 6;
                            }
                        } else if (this.continueNumberState == 6) {
                            this.continueMoveNumberX += SCREEN_WIDTH / 12;
                            if (this.continueMoveNumberX >= SCREEN_WIDTH + 30) {
                                this.state = 5;
                                loadingType = 0;
                                initTips();
                                PlayerObject.resetGameParam();
                            }
                        }
                        if (this.continueNumberState < 3) {
                            if (Key.touchgameoveryres.Isin() && Key.touchgameover.IsClick()) {
                                this.continueCursor = 0;
                            }
                            if (Key.touchgameoverno.Isin() && Key.touchgameover.IsClick()) {
                                this.continueCursor = 1;
                            }
                            if (Key.touchgameoveryres.IsButtonPress() && this.continueCursor == 0) {
                                this.continueNumberState = 4;
                                this.continueMoveNumberX = -30;
                                this.continueNumberScale = 1.0f;
                                SoundSystem.getInstance().playSe(1);
                            }
                            if (Key.touchgameoverno.IsButtonPress() && this.continueCursor == 1) {
                                continueEnd();
                                SoundSystem.getInstance().playSe(2);
                                return;
                            }
                            return;
                        }
                        return;
                    }
                    return;
                } else {
                    return;
                }
            default:
                return;
        }
    }

    public void draw(MFGraphics g) {
        switch (this.state) {
            case 34:
                g.setFont(11);
                break;
            default:
                g.setFont(14);
                break;
        }
        switch (this.state) {
            case 2:
                StageManager.draw(g);
                break;
            case 3:
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
            case 24:
            case 43:
            case 44:
            case 45:
                break;
            case 5:
                State.drawFadeSlow(g);
                drawLoading(g);
                break;
            case 6:
                helpDraw(g);
                break;
            case 7:
                optionDraw(g);
                break;
            case 13:
                g.setColor(0);
                MyAPI.fillRect(g, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
                drawTimeOver(g, SCREEN_WIDTH >> 1, (SCREEN_HEIGHT >> 1) - 28);
                break;
            case 14:
                MyAPI.drawImage(g, this.exendBgImage, SCREEN_WIDTH >> 1, 0, 17);
                MyAPI.drawImage(g, this.exendBg1Image, SCREEN_WIDTH >> 1, 80, 17);
                State.drawFade(g);
                if (State.fadeChangeOver() && this.allclearFrame > 20) {
                    PlayerObject.stagePassDraw(g);
                    break;
                }
            case 18:
                interruptDraw(g);
                break;
            case 30:
                optionDraw(g);
                itemsSelect2Draw(g, 35, 36);
                break;
            case 31:
                optionDraw(g);
                itemsSelect2Draw(g, 35, 36);
                break;
            case 32:
                optionDraw(g);
                break;
            case 33:
                optionDraw(g);
                itemsSelect2Draw(g, 37, 38);
                break;
            case 34:
                helpDraw(g);
                break;
            case 38:
                optionDraw(g);
                soundVolumnDraw(g);
                break;
            case 39:
                optionDraw(g);
                spSenorSetDraw(g);
                break;
            default:
                if (GameObject.IsGamePause) {
                    AnimationDrawer.setAllPause(true);
                }
                if (!(this.interrupt_state == 5 || this.interrupt_state == 14 || this.interrupt_state == 4)) {
                    drawGame(g);
                }
                if (GameObject.IsGamePause) {
                    AnimationDrawer.setAllPause(false);
                }
                if (!(this.state == 15 || this.state == 16 || this.state == 17 || this.state == 27)) {
                    PlayerObject.drawGameUI(g);
                    drawGameSoftKey(g);
                }
                if (this.state == 1) {
                    State.drawFade(g);
                    drawGamePause(g);
                }
                if (this.state == 25) {
                    State.drawFade(g);
                    BP_toolsuseDraw(g);
                }
                if (this.state == 26) {
                    State.drawFade(g);
                    BP_ensureToolsUseDraw(g);
                }
                if (this.state == 15 || this.state == 27 || this.state == 16 || this.state == 17) {
                    if (this.state == 15 || this.state == 27) {
                        State.drawFadeInSpeed(g, 4);
                    } else if (this.state == 16) {
                        State.drawFadeInSpeed(g, 12);
                    } else {
                        State.drawFade(g);
                    }
                    drawSaw(g, this.display[0][0], this.display[0][1]);
                    drawWhiteBar(g, this.display[1][0], this.display[1][1]);
                    drawTinyStageName(g, StageManager.getStageID(), this.display[5][0], this.display[5][1]);
                    g.setClip(this.display[1][0], this.display[1][1] - 10, SCREEN_WIDTH, 20);
                    drawHugeStageName(g, StageManager.getStageID(), this.display[2][0], this.display[2][1]);
                    g.setClip(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
                    drawSonic(g, this.display[3][0], this.display[3][1]);
                    drawAction(g, StageManager.getStageID(), this.display[4][0], this.display[4][1]);
                }
                if (this.state == 36 || this.state == 37) {
                    g.setColor(0);
                    MyAPI.fillRect(g, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
                    if (loadingType == 2) {
                        g.setColor(MapManager.END_COLOR);
                        MyAPI.fillRect(g, 0, ((SCREEN_HEIGHT >> 1) - 36) - 10, SCREEN_WIDTH, 20);
                    }
                }
                if (this.state == 8) {
                    drawGamePause(g);
                    State.drawFade(g);
                    SecondEnsurePanelDraw(g, 15);
                }
                if (this.state == 9) {
                    drawGamePause(g);
                    State.drawFade(g);
                    SecondEnsurePanelDraw(g, 17);
                }
                if (this.state == 29) {
                    drawGamePause(g);
                    State.drawFade(g);
                    SecondEnsurePanelDraw(g, 16);
                }
                if (this.state == 10) {
                    drawGamePause(g);
                    State.drawFade(g);
                    SecondEnsurePanelDraw(g, 18);
                }
                if (this.state == 11 || this.state == 41) {
                    drawTimeOver(g, this.movingTitleX, (SCREEN_HEIGHT >> 1) - 28);
                }
                if (this.state == 42) {
                    State.drawFade(g);
                    drawGameOver(g);
                }
                if (this.state == 12) {
                    State.drawFade(g);
                    drawTimeOver(g, SCREEN_WIDTH >> 1, (SCREEN_HEIGHT >> 1) - 28);
                }
                if (this.state == 40) {
                    State.drawFade(g);
                }
                if (this.state == 4) {
                    PlayerObject.stagePassDraw(g);
                }
                if (this.state == 35) {
                    State.drawFade(g);
                    if (loadingType == 2) {
                        g.setColor(MapManager.END_COLOR);
                        MyAPI.fillRect(g, 0, ((SCREEN_HEIGHT >> 1) - 36) - 10, SCREEN_WIDTH, 20);
                        break;
                    }
                }
                break;
        }
        if (isDrawTouchPad) {
            if (!(this.state == 5 || this.state == 14)) {
                drawTouchKeyDirect(g);
            }
            if (this.state == 14 && this.endingState > (byte) 3) {
                drawTouchKeyDirect(g);
            }
        }
    }

    public void drawGame(MFGraphics g) {
        g.setColor(MapManager.END_COLOR);
        MyAPI.fillRect(g, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
        if (!GameObject.IsGamePause) {
            MapManager.gameFrame++;
        }
        BackGroundManager.frame = MapManager.gameFrame;
        MapManager.drawBack(g);
        GameObject.drawObjectBeforeSonic(g);
        GameObject.drawPlayer(g);
        Effect.draw(g, 1);
        GameObject.drawObjects(g);
        GameObject.player.drawSheild1(g);
        MapManager.drawFront(g);
        GameObject.player.draw2(g);
        GameObject.player.drawSheild2(g);
        GameObject.drawObjectAfterEveryThing(g);
        MapManager.drawFrontNatural(g);
        Effect.draw(g, 0);
        MapManager.drawMapFrame(g);
        RocketSeparateEffect.getInstance().draw(g);
    }

    public void drawGameSoftKey(MFGraphics g) {
        if (this.state != 1 && this.state != 10 && this.state != 8 && this.state != 9 && this.state != 29 && this.state != 25 && this.state != 26 && this.state != 41 && this.state != 42 && this.state != 11 && this.state != 12 && this.state != 13 && !StageManager.isStagePassTimePause()) {
            if (!GameObject.player.isDead) {
                State.drawSoftKeyPause(g);
            }
            if (this.state != 25 && this.state != 26 && this.state != 1 && this.state != 6 && this.state != 7 && this.state != 8 && this.state != 9 && this.state != 29 && this.state != 10 && State.IsToolsCharge() && GameObject.stageModeState == 0 && !GameObject.player.isDead) {
                MyAPI.drawImage(g, BP_wordsImg, 0, BP_wordsHeight, BP_wordsWidth, BP_wordsHeight, 0, tool_x, tool_y, 36);
            }
        }
    }

    public void close() {
        MapManager.closeMap();
        State.releaseTouchkeyBoard();
        Animation.closeAnimationArray(this.stageInfoClearAni);
        this.stageInfoClearAni = null;
        Animation.closeAnimationDrawer(stageInfoAniDrawer);
        stageInfoAniDrawer = null;
        Animation.closeAnimationDrawer(this.stageInfoPlayerNameDrawer);
        this.stageInfoPlayerNameDrawer = null;
        Animation.closeAnimationDrawer(this.stageInfoActNumDrawer);
        this.stageInfoActNumDrawer = null;
        Animation.closeAnimation(guiAnimation);
        guiAnimation = null;
        Animation.closeAnimationDrawer(guiAniDrawer);
        guiAniDrawer = null;
        Animation.closeAnimationDrawer(numberDrawer);
        numberDrawer = null;
        Animation.closeAnimationDrawer(this.planeDrawer);
        this.planeDrawer = null;
        Animation.closeAnimationDrawer(this.cloudDrawer);
        this.cloudDrawer = null;
        Animation.closeAnimationDrawerArray(this.birdDrawer);
        this.birdDrawer = null;
        this.exendBgImage = null;
        this.exendBg1Image = null;
        Animation.closeAnimationDrawer(this.interruptDrawer);
        this.interruptDrawer = null;
        SoundSystem.getInstance().setSoundSpeed(1.0f);
        Key.init();
        GameObject.quitGameState();
        System.gc();
        try {
            Thread.sleep(100);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void init() {
        State.initTouchkeyBoard();
        Key.initSonic();
        tipsForShow = null;
    }

    static {
        int[] iArr = new int[5];
        iArr[1] = 1;
        iArr[2] = 2;
        iArr[3] = 3;
        iArr[4] = 4;
        PAUSE_NORMAL_MODE_SHOP = iArr;
        iArr = new int[4];
        iArr[1] = 1;
        iArr[2] = 3;
        iArr[3] = 4;
        PAUSE_NORMAL_MODE_NOSHOP = iArr;
        iArr = new int[3];
        iArr[1] = 1;
        iArr[2] = 3;
        //BP_ITEMS_HIGH = iArr;
        iArr = new int[3];
        iArr[1] = 2;
        iArr[2] = 3;
        BP_ITEMS_LOW = iArr;
    }

    public static void setPauseMenu() {
        PAUSE_NORMAL_MODE = State.IsToolsCharge() ? PAUSE_NORMAL_MODE_SHOP : PAUSE_NORMAL_MODE_NOSHOP;
    }

    public void gamepauseLogic() {
        boolean IsUp;
        boolean IsDown;
        Key.touchkeyboardInit();
        if (PlayerObject.stageModeState == 0) {
            PlayerObject.cursorMax = State.IsToolsCharge() ? 5 : 4;
            Key.touchPauseInit(false);
        } else if (PlayerObject.stageModeState == 1) {
            PlayerObject.cursorMax = 6;
            Key.touchPauseInit(true);
        }
        if (PlayerObject.stageModeState == 1) {
            if ((!Key.touchpausearrowup.Isin() || IsSingleUp || IsSingleDown) && !((Key.touchpausearrowupsingle.Isin() && IsSingleUp && !IsSingleDown) || Key.press(Key.gUp))) {
                IsUp = false;
            } else {
                IsUp = true;
            }
            if ((!Key.touchpausearrowdown.Isin() || IsSingleUp || IsSingleDown) && !((Key.touchpausearrowdownsingle.Isin() && !IsSingleUp && IsSingleDown) || Key.press(Key.gDown))) {
                IsDown = false;
            } else {
                IsDown = true;
            }
        } else {
            if (Key.press(Key.gUp)) {
                IsUp = true;
            } else {
                IsUp = false;
            }
            if (Key.press(Key.gDown)) {
                IsDown = true;
            } else {
                IsDown = false;
            }
        }
        if (PlayerObject.stageModeState == 0) {
            if ((Key.touchpause1.IsClick() && PlayerObject.cursor == PlayerObject.cursorIndex + 0) || ((Key.touchpause2.IsClick() && PlayerObject.cursor == PlayerObject.cursorIndex + 1) || ((Key.touchpause3.IsClick() && PlayerObject.cursor == PlayerObject.cursorIndex + 2) || (Key.touchpause4.IsClick() && PlayerObject.cursor == PlayerObject.cursorIndex + 3)))) {
                switch (PAUSE_NORMAL_MODE[PlayerObject.cursor]) {
                    case 0:
                        BacktoGame();
                        break;
                    case 1:
                        this.state = 10;
                        this.cursor = 1;
                        break;
                    case 2:
                        shopInit(true);
                        break;
                    case 3:
                        optionInit();
                        this.state = 7;
                        break;
                    case 4:
                        helpInit();
                        this.state = 6;
                        break;
                }
                Key.touchPauseClose(false);
            } else if (Key.touchpause1.IsClick()) {
                PlayerObject.cursor = PlayerObject.cursorIndex + 0;
                Key.touchpause1.reset();
            } else if (Key.touchpause2.IsClick()) {
                PlayerObject.cursor = PlayerObject.cursorIndex + 1;
                Key.touchpause2.reset();
            } else if (Key.touchpause3.IsClick()) {
                PlayerObject.cursor = PlayerObject.cursorIndex + 2;
                Key.touchpause3.reset();
            } else if (Key.touchpause4.IsClick()) {
                PlayerObject.cursor = PlayerObject.cursorIndex + 3;
                Key.touchpause4.reset();
            }
        } else if (PlayerObject.stageModeState == 1) {
            if ((Key.touchpause1.IsClick() && PlayerObject.cursor == PlayerObject.cursorIndex + 0) || ((Key.touchpause2.IsClick() && PlayerObject.cursor == PlayerObject.cursorIndex + 1) || ((Key.touchpause3.IsClick() && PlayerObject.cursor == PlayerObject.cursorIndex + 2) || (Key.touchpause4.IsClick() && PlayerObject.cursor == PlayerObject.cursorIndex + 3)))) {
                switch (PlayerObject.cursor) {
                    case 0:
                        BacktoGame();
                        break;
                    case 1:
                        this.state = 8;
                        this.cursor = 1;
                        break;
                    case 2:
                        this.state = 9;
                        this.cursor = 0;
                        break;
                    case 3:
                        this.state = 10;
                        this.cursor = 1;
                        break;
                    case 4:
                        optionInit();
                        this.state = 7;
                        this.cursor = 0;
                        break;
                    case 5:
                        helpInit();
                        this.state = 6;
                        this.cursor = 0;
                        break;
                    case 29:
                        this.state = 29;
                        this.cursor = 0;
                        break;
                }
                Key.touchPauseClose(true);
            } else if (Key.touchpause1.IsClick()) {
                PlayerObject.cursor = PlayerObject.cursorIndex + 0;
                Key.touchpause1.reset();
            } else if (Key.touchpause2.IsClick()) {
                PlayerObject.cursor = PlayerObject.cursorIndex + 1;
                Key.touchpause2.reset();
            } else if (Key.touchpause3.IsClick()) {
                PlayerObject.cursor = PlayerObject.cursorIndex + 2;
                Key.touchpause3.reset();
            } else if (Key.touchpause4.IsClick()) {
                PlayerObject.cursor = PlayerObject.cursorIndex + 3;
                Key.touchpause4.reset();
            }
        }
        if (IsUp) {
            PlayerObject.cursor--;
            PlayerObject.cursor += PlayerObject.cursorMax;
            PlayerObject.cursor %= PlayerObject.cursorMax;
        } else if (IsDown) {
            PlayerObject.cursor++;
            PlayerObject.cursor %= PlayerObject.cursorMax;
        } else if (Key.press(Key.gSelect | Key.B_S1)) {
            if (PlayerObject.stageModeState != 0) {
                if (PlayerObject.stageModeState == 1) {
                    switch (PlayerObject.cursor) {
                        case 0:
                            BacktoGame();
                            break;
                        case 1:
                            this.state = 8;
                            this.cursor = 1;
                            break;
                        case 2:
                            this.state = 9;
                            this.cursor = 0;
                            break;
                        case 3:
                            this.state = 10;
                            this.cursor = 1;
                            break;
                        case 4:
                            optionInit();
                            this.state = 7;
                            this.cursor = 0;
                            break;
                        case 5:
                            helpInit();
                            this.state = 6;
                            this.cursor = 0;
                            break;
                        case 29:
                            this.state = 29;
                            this.cursor = 0;
                            break;
                        default:
                            break;
                    }
                }
            }
            switch (PAUSE_NORMAL_MODE[PlayerObject.cursor]) {
                case 0:
                    BacktoGame();
                    break;
                case 1:
                    this.state = 10;
                    this.cursor = 1;
                    break;
                case 2:
                    shopInit(true);
                    break;
                case 3:
                    optionInit();
                    this.state = 7;
                    break;
                case 4:
                    helpInit();
                    this.state = 6;
                    break;
            }
            Key.clear();
        }
        if (Key.press(2)) {
        }
        if (Key.press(524288)) {
            doReturnGameStuff();
            if (PlayerObject.stageModeState == 0) {
                Key.touchPauseClose(false);
            } else if (PlayerObject.stageModeState == 1) {
                Key.touchPauseClose(true);
            }
            Key.touchgamekeyInit();
            Key.touchkeyboardClose();
            Key.touchkeygameboardInit();
            Key.clear();
        }
    }

    private void doReturnGameStuff() {
        this.state = 0;
        GameObject.IsGamePause = false;
        State.fadeInit(102, 0);
        if (!StageManager.isStagePass()) {
            if (PlayerObject.IsInvincibility()) {
                SoundSystem.getInstance().playBgm(44);
            } else if (GameObject.bossFighting) {
                switch (GameObject.bossID) {
                    case 21:
                    case 26:
                        if (!GameObject.isBossHalf) {
                            SoundSystem.getInstance().playBgm(23, true);
                            break;
                        } else {
                            SoundSystem.getInstance().playBgm(24, true);
                            break;
                        }
                    case 22:
                    case 23:
                    case 24:
                    case 25:
                        SoundSystem.getInstance().playBgm(22);
                        break;
                    case 27:
                        SoundSystem.getInstance().playBgm(25);
                        break;
                    case 28:
                        SoundSystem.getInstance().playBgm(46);
                        break;
                    case 29:
                        SoundSystem.getInstance().playBgm(47);
                        break;
                    case 30:
                        SoundSystem.getInstance().playBgm(19, true);
                        break;
                    default:
                        break;
                }
            } else {
                SoundSystem.getInstance().playBgm(StageManager.getBgmId(), true);
            }
        }
        Key.initSonic();
    }

    private void drawLoadingBar(MFGraphics g, int y) {
        drawTips(g, y);
        State.drawBar(g, 0, y);
        this.selectMenuOffsetX += 8;
        this.selectMenuOffsetX %= MENU_TITLE_MOVE_DIRECTION;
        int x = 0;
        while (x - this.selectMenuOffsetX > 0) {
            x -= MENU_TITLE_MOVE_DIRECTION;
        }
        for (int i = 0; i < MENU_TITLE_DRAW_NUM; i++) {
            int i2 = (MENU_TITLE_MOVE_DIRECTION * i) + x;
        }
    }

    private void drawTips(MFGraphics g, int endy) {
        State.fillMenuRect(g, ((SCREEN_WIDTH - MENU_RECT_WIDTH) - 0) >> 1, (SCREEN_HEIGHT >> 2) - 16, MENU_RECT_WIDTH + 0, (SCREEN_HEIGHT >> 1) + 16);
        if (tipsForShow == null) {
            tipsForShow = MyAPI.getStrings(tipsString[MyRandom.nextInt(0, tipsString.length - 1)], MENU_RECT_WIDTH - this.TIPS_OFFSET_X);
            MyAPI.initString();
            return;
        }
        g.setColor(0);
        int i = this.TIPS_TITLE_OFFSET_Y + ((SCREEN_HEIGHT >> 2) - 8);
        MFGraphics mFGraphics = g;
        MyAPI.drawBoldString(mFGraphics, "小提示", ((SCREEN_WIDTH - MENU_RECT_WIDTH) + this.TIPS_OFFSET_X) >> 1, i, 0, MapManager.END_COLOR, 4656650, 0);
        MyAPI.drawBoldStrings(g, tipsForShow, ((SCREEN_WIDTH - MENU_RECT_WIDTH) + this.TIPS_OFFSET_X) >> 1, LINE_SPACE + ((SCREEN_HEIGHT >> 2) - 8), MENU_RECT_WIDTH - this.TIPS_OFFSET_X, (SCREEN_HEIGHT >> 1) - LINE_SPACE, MapManager.END_COLOR, 4656650, 0);
    }

    private void releaseTips() {
        tipsForShow = null;
    }

    private void stagePassLogic() {
        PlayerObject.isNeedPlayWaterSE = false;
        if (StageManager.isOnlyStagePass) {
            State.fadeInit(0, 255);
            this.state = 35;
            isThroughGame = true;
            loadingType = 0;
            StageManager.addStageID();
            SoundSystem.getInstance().stopBgm(false);
        } else if (PlayerObject.IsStarttoCnt) {
            this.cnt++;
            if (this.cnt == 1) {
                if (PlayerObject.stageModeState == 1) {
                    GameObject.ObjectClear();
                }
                PlayerObject.isbarOut = true;
            }
            if (this.cnt == 2) {
                SoundSystem.getInstance().playSe(32, false);
            }
            if (this.cnt > 20) {
                if (this.cnt == 21) {
                    if (PlayerObject.stageModeState == 0) {
                        GameObject.player.setStagePassRunOutofScreen();
                    } else if (PlayerObject.stageModeState == 1) {
                        if (PlayerObject.IsDisplayRaceModeNewRecord) {
                            this.racemode_cnt = 128;
                        } else {
                            this.racemode_cnt = 60;
                        }
                    }
                } else if (this.cnt > 21 && PlayerObject.stageModeState == 0 && GameObject.player.stagePassRunOutofScreenLogic()) {
                    if (StageManager.IsStageEnd()) {
                        StageManager.characterFromGame = -1;
                        StageManager.stageIDFromGame = -1;
                        StageManager.saveStageRecord();
                        State.setState(11);
                    } else if (StageManager.getStageID() != 12) {
                        State.fadeInit(0, 255);
                        this.state = 35;
                        isThroughGame = true;
                        loadingType = 0;
                        StageManager.addStageID();
                        StageManager.characterFromGame = PlayerObject.getCharacterID();
                        StageManager.stageIDFromGame = StageManager.getStageID();
                        StageManager.saveStageRecord();
                        SoundSystem.getInstance().stopBgm(false);
                    } else if (StageManager.isGoingToExtraStage()) {
                        State.fadeInit(0, 255);
                        this.state = 35;
                        isThroughGame = true;
                        loadingType = 0;
                        StageManager.addStageID();
                        SoundSystem.getInstance().stopBgm(false);
                    } else {
                        StageManager.characterFromGame = -1;
                        StageManager.stageIDFromGame = -1;
                        StageManager.saveStageRecord();
                        State.setState(10);
                    }
                }
                if (this.cnt == this.racemode_cnt) {
                    if (PlayerObject.stageModeState == 1) {
                        setStateWithFade(3);
                        Key.touchgamekeyClose();
                        Key.touchkeygameboardClose();
                        Key.touchkeyboardInit();
                    }
                    StageManager.saveHighScoreRecord();
                }
            }
        }
    }

    private void drawSaw(MFGraphics g, int x, int y) {
        stageInfoAniDrawer.draw(g, PlayerObject.getCharacterID(), x, y, false, 0);
    }

    private void drawWhiteBar(MFGraphics g, int x, int y) {
        g.setColor(MapManager.END_COLOR);
        MyAPI.fillRect(g, x, y - 10, SCREEN_WIDTH, 20);
    }

    private void drawHugeStageName(MFGraphics g, int stageid, int x, int y) {
        int stage_id;
        this.selectMenuOffsetX -= 8;
        this.selectMenuOffsetX %= 224;
        if (stageid < 12) {
            stage_id = stageid >> 1;
        } else {
            stage_id = stageid - 5;
        }
        if (stageid == 11) {
            stage_id = 6;
        }
        for (int x1 = this.selectMenuOffsetX - 294; x1 < SCREEN_WIDTH * 2; x1 += 224) {
            MFGraphics mFGraphics = g;
            stageInfoAniDrawer.draw(mFGraphics, stage_id + 5, x1, (y - 10) + 2, false, 0);
        }
    }

    private void drawTinyStageName(MFGraphics g, int stageid, int x, int y) {
        int stage_id;
        if (stageid < 11) {
            stage_id = stageid >> 1;
        } else {
            stage_id = stageid - 5;
        }
        stageInfoAniDrawer.draw(g, stage_id + 14, x, y, false, 0);
    }

    private void drawSonic(MFGraphics g, int x, int y) {
        if (this.IsPlayerNameDrawable) {
            this.stageInfoPlayerNameDrawer.draw(g, PlayerObject.getCharacterID() + 23, x, y, false, 0);
        }
    }

    private void drawAction(MFGraphics g, int id, int x, int y) {
        if (this.IsActNumDrawable && StageManager.getStageID() < 12) {
            this.stageInfoActNumDrawer.draw(g, (StageManager.getStageID() % 2) + 27, x, y, false, 0);
        }
    }

    public void pause() {
        if (this.state != 45 && this.state != 18 && this.state != 1 && this.state != 20 && this.state != 23 && this.state != 21) {
            if (this.state == 0 && GameObject.player.isDead) {
                this.interrupt_state = this.state;
                this.state = 18;
                interruptInit();
            } else if (this.state == 5 || this.state == 35 || this.state == 14 || this.state == 4 || this.state == 7 || this.state == 8 || this.state == 9 || this.state == 29 || this.state == 10 || this.state == 6 || this.state == 29 || this.state == 41 || this.state == 42 || this.state == 28 || this.state == 11 || this.state == 12 || this.state == 13 || this.state == 30 || this.state == 31 || this.state == 32 || this.state == 33 || this.state == 34 || this.state == 38 || this.state == 39 || this.state == 43 || this.state == 44 || this.state == 45) {
                this.interrupt_state = this.state;
                this.state = 18;
                interruptInit();
                System.out.println("interrupt");
            } else {
                PlayerObject.gamepauseInit();
                this.state = 1;
                isDrawTouchPad = false;
                gamePauseInit();
                Key.init();
                GameObject.IsGamePause = true;
                State.fadeInit_Modify(0, 102);
                SoundSystem.getInstance().stopBgm(false);
                SoundSystem.getInstance().stopLongSe();
                SoundSystem.getInstance().stopLoopSe();
                this.pauseoptionCursor = GlobalResource.soundConfig;
                Key.touchgamekeyClose();
            }
        }
    }

    private void interruptInit() {
        if (this.interruptDrawer == null) {
            this.interruptDrawer = Animation.getInstanceFromQi("/animation/utl_res/suspend_resume.dat")[0].getDrawer(0, true, 0);
        }
        isDrawTouchPad = false;
        IsInInterrupt = true;
        lastFading = fading;
        fading = false;
        Key.touchkeygameboardClose();
        Key.touchkeyboardInit();
        Key.touchInterruptInit();
    }

    public void interruptLogic() {
        SoundSystem.getInstance().stopBgm(false);
        if (Key.press(2)) {
        }
        if (Key.press(524288) || (Key.touchinterruptreturn != null && Key.touchinterruptreturn.IsButtonPress())) {
            fading = lastFading;
            SoundSystem.getInstance().playSe(2);
            Key.touchInterruptClose();
            this.state = this.interrupt_state;
            this.interrupt_state = -1;
            Key.clear();
            if (Key.touchitemsselect2_1 != null) {
                Key.touchitemsselect2_1.reset();
            }
            if (Key.touchitemsselect2_2 != null) {
                Key.touchitemsselect2_2.reset();
            }
            if (Key.touchitemsselect3_1 != null) {
                Key.touchitemsselect3_1.reset();
            }
            if (Key.touchitemsselect3_2 != null) {
                Key.touchitemsselect3_2.reset();
            }
            if (Key.touchitemsselect3_3 != null) {
                Key.touchitemsselect3_3.reset();
            }
            isDrawTouchPad = true;
            switch (this.state) {
                case 4:
                case 35:
                    isDrawTouchPad = false;
                    break;
                case 7:
                    optionInit();
                    State.fadeInit(0, 0);
                    Key.touchkeyboardClose();
                    Key.touchkeyboardInit();
                    isDrawTouchPad = false;
                    break;
                case 8:
                case 9:
                case 10:
                case 29:
                    isDrawTouchPad = false;
                    break;
                case 11:
                case 12:
                case 13:
                    break;
                case 14:
                case 41:
                case 42:
                    isDrawTouchPad = false;
                    break;
                case 30:
                case 31:
                case 32:
                case 33:
                case 34:
                case 38:
                case 39:
                    isDrawTouchPad = false;
                    SoundSystem.getInstance().playBgm(5);
                    break;
                case 43:
                case 45:
                    isDrawTouchPad = false;
                    break;
                case 44:
                    isDrawTouchPad = false;
                    break;
            }
            isDrawTouchPad = false;
            IsInInterrupt = false;
        }
    }

    public void interruptDraw(MFGraphics g) {
        this.interruptDrawer.setActionId((Key.touchinterruptreturn.Isin() ? 1 : 0) + 0);
        this.interruptDrawer.draw(g, SCREEN_WIDTH >> 1, SCREEN_HEIGHT >> 1);
    }

    private void optionInit() {
        this.optionMenuCursor = 0;
        menuInit(OPTION_ELEMENT_NUM);
        this.optionCursor[0] = GlobalResource.soundConfig;
        this.optionCursor[1] = GlobalResource.seConfig;
        this.offsetOfVolumeInterface = MENU_SPACE;
        if (muiAniDrawer == null) {
            muiAniDrawer = new Animation("/animation/mui").getDrawer(0, false, 0);
        }
        this.optionOffsetX = 0;
        this.pauseOptionCursor = 0;
        this.isOptionDisFlag = false;
        State.fadeInit(102, 102);
        Key.touchMenuOptionInit();
        this.optionOffsetYAim = 0;
        this.optionOffsetY = 0;
        this.isChanged = false;
        this.optionslide_getprey = -1;
        this.optionslide_gety = -1;
        this.optionslide_y = 0;
        this.optionDrawOffsetBottomY = 0;
        this.optionYDirect = 0;
    }

    private int itemsid(int id) {
        int itemsidoffset = (this.optionOffsetY / 24) * 2;
        if (id + itemsidoffset < 0) {
            return 0;
        }
        if (id + itemsidoffset > 9) {
            return 9;
        }
        return id + itemsidoffset;
    }

    private void optionLogic() {
        if (!this.isOptionDisFlag) {
            SoundSystem.getInstance().playBgm(5);
            this.isOptionDisFlag = true;
        }
        int i;
        if (State.fadeChangeOver()) {
            if (Key.touchmenuoptionreturn.Isin() && Key.touchmenuoption.IsClick()) {
                this.returnCursor = 1;
            }
            this.optionslide_gety = Key.slidesensormenuoption.getPointerY();
            this.optionslide_y = 0;
            this.optionslidefirsty = 0;
            for (i = 0; i < (Key.touchmenuoptionitems.length >> 1); i++) {
                Key.touchmenuoptionitems[i * 2].setStartY((((i * 24) + 28) + this.optionDrawOffsetY) + this.optionslide_y);
                Key.touchmenuoptionitems[(i * 2) + 1].setStartY((((i * 24) + 28) + this.optionDrawOffsetY) + this.optionslide_y);
            }
            if (this.isSelectable) {
                for (i = 0; i < Key.touchmenuoptionitems.length; i++) {
                    if (Key.touchmenuoptionitems[i].Isin() && Key.touchmenuoption.IsClick()) {
                        this.pauseOptionCursor = i / 2;
                        this.returnCursor = 0;
                        break;
                    }
                }
            }
            if (Key.touchmenuoptionreturn.Isin() && Key.touchmenuoption.IsClick()) {
                this.returnCursor = 1;
            }
            if ((Key.press(524288) || (Key.touchmenuoptionreturn.IsButtonPress() && this.returnCursor == 1)) && State.fadeChangeOver()) {
                changeStateWithFade(1);
                SoundSystem.getInstance().stopBgm(false);
                SoundSystem.getInstance().playSe(2);
                GlobalResource.saveSystemConfig();
            }
            if (Key.slidesensormenuoption.isSliding()) {
                this.isSelectable = true;
            } else {
                if (this.isOptionChange && this.optionslide_y == 0) {
                    this.optionDrawOffsetY = this.optionDrawOffsetTmpY1;
                    this.isOptionChange = false;
                    this.optionYDirect = 0;
                }
                if (!this.isOptionChange) {
                    int speed;
                    if (this.optionDrawOffsetY > 0) {
                        this.optionYDirect = 1;
                        speed = (-this.optionDrawOffsetY) >> 1;
                        if (speed > -2) {
                            speed = -2;
                        }
                        if (this.optionDrawOffsetY + speed <= 0) {
                            this.optionDrawOffsetY = 0;
                            this.optionYDirect = 0;
                        } else {
                            this.optionDrawOffsetY += speed;
                        }
                    } else if (this.optionDrawOffsetY < this.optionDrawOffsetBottomY) {
                        this.optionYDirect = 2;
                        speed = (this.optionDrawOffsetBottomY - this.optionDrawOffsetY) >> 1;
                        if (speed < 2) {
                            speed = 2;
                        }
                        if (this.optionDrawOffsetY + speed >= this.optionDrawOffsetBottomY) {
                            this.optionDrawOffsetY = this.optionDrawOffsetBottomY;
                            this.optionYDirect = 0;
                        } else {
                            this.optionDrawOffsetY += speed;
                        }
                    }
                }
            }
            if (this.isSelectable && this.optionYDirect == 0) {
                if (Key.touchmenuoptionitems[1].IsButtonPress() && this.pauseOptionCursor == 0 && GlobalResource.soundSwitchConfig != 0 && State.fadeChangeOver()) {
                    this.state = 38;
                    soundVolumnInit();
                    SoundSystem.getInstance().playSe(1);
                } else if (Key.touchmenuoptionitems[3].IsButtonPress() && this.pauseOptionCursor == 1 && State.fadeChangeOver()) {
                    this.state = 31;
                    itemsSelect2Init();
                    SoundSystem.getInstance().playSe(1);
                } else if (Key.touchmenuoptionitems[5].IsButtonPress() && this.pauseOptionCursor == 2 && State.fadeChangeOver()) {
                    this.state = 33;
                    itemsSelect2Init();
                    SoundSystem.getInstance().playSe(1);
                } else if (Key.touchmenuoptionitems[7].IsButtonPress() && this.pauseOptionCursor == 3 && State.fadeChangeOver()) {
                    if (GlobalResource.spsetConfig != 0) {
                        this.state = 39;
                        spSenorSetInit();
                        SoundSystem.getInstance().playSe(1);
                    } else {
                        SoundSystem.getInstance().playSe(2);
                    }
                } else if (Key.touchmenuoptionitems[8].IsButtonPress() && this.pauseOptionCursor == 4 && State.fadeChangeOver()) {
                    changeStateWithFade(34);
                    helpInit();
                    SoundSystem.getInstance().playSe(1);
                }
            }
            this.optionslide_getprey = this.optionslide_gety;
            return;
        }
        for (TouchKeyRange resetKeyState : Key.touchmenuoptionitems) {
            resetKeyState.resetKeyState();
        }
    }

    private void releaseOptionItemsTouchKey() {
        for (TouchKeyRange resetKeyState : Key.touchmenuoptionitems) {
            resetKeyState.resetKeyState();
        }
    }

    private void optionDraw(MFGraphics g) {
        int i;
        g.setColor(0);
        MyAPI.fillRect(g, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
        muiAniDrawer.setActionId(52);
        for (int i2 = 0; i2 < (SCREEN_WIDTH / 48) + 1; i2++) {
            for (int j = 0; j < (SCREEN_HEIGHT / 48) + 1; j++) {
                muiAniDrawer.draw(g, i2 * 48, j * 48);
            }
        }
        if (this.state != 7) {
            this.pauseOptionCursor = -2;
        }
        muiAniDrawer.setActionId(25);
        muiAniDrawer.draw(g, (SCREEN_WIDTH >> 1) - 96, ((this.optionDrawOffsetY + 40) + this.optionslide_y) + 0);
        AnimationDrawer animationDrawer = muiAniDrawer;
        if (GlobalResource.soundSwitchConfig == 0) {
            i = 67;
        } else {
            i = (Key.touchmenuoptionitems[1].Isin() && this.pauseOptionCursor == 0 && this.isSelectable) ? 1 : 0;
            i += 57;
        }
        animationDrawer.setActionId(i);
        muiAniDrawer.draw(g, (SCREEN_WIDTH >> 1) + 56, ((this.optionDrawOffsetY + 40) + this.optionslide_y) + 0);
        muiAniDrawer.setActionId(GlobalResource.soundConfig + 73);
        muiAniDrawer.draw(g, (SCREEN_WIDTH >> 1) + 56, ((this.optionDrawOffsetY + 40) + this.optionslide_y) + 0);
        muiAniDrawer.setActionId(21);
        muiAniDrawer.draw(g, (SCREEN_WIDTH >> 1) - 96, ((this.optionDrawOffsetY + 40) + this.optionslide_y) + 24);
        animationDrawer = muiAniDrawer;
        if (Key.touchmenuoptionitems[3].Isin() && this.pauseOptionCursor == 1 && this.isSelectable) {
            i = 1;
        } else {
            i = 0;
        }
        animationDrawer.setActionId(i + 57);
        muiAniDrawer.draw(g, (SCREEN_WIDTH >> 1) + 56, ((this.optionDrawOffsetY + 40) + this.optionslide_y) + 24);
        animationDrawer = muiAniDrawer;
        if (GlobalResource.vibrationConfig == 0) {
            i = 1;
        } else {
            i = 0;
        }
        animationDrawer.setActionId(i + 35);
        muiAniDrawer.draw(g, (SCREEN_WIDTH >> 1) + 56, ((this.optionDrawOffsetY + 40) + this.optionslide_y) + 24);
        muiAniDrawer.setActionId(23);
        muiAniDrawer.draw(g, (SCREEN_WIDTH >> 1) - 96, ((this.optionDrawOffsetY + 40) + this.optionslide_y) + 48);
        animationDrawer = muiAniDrawer;
        if (Key.touchmenuoptionitems[5].Isin() && this.pauseOptionCursor == 2 && this.isSelectable) {
            i = 1;
        } else {
            i = 0;
        }
        animationDrawer.setActionId(i + 57);
        muiAniDrawer.draw(g, (SCREEN_WIDTH >> 1) + 56, ((this.optionDrawOffsetY + 40) + this.optionslide_y) + 48);
        animationDrawer = muiAniDrawer;
        if (GlobalResource.spsetConfig == 0) {
            i = 0;
        } else {
            i = 1;
        }
        animationDrawer.setActionId(i + 37);
        muiAniDrawer.draw(g, (SCREEN_WIDTH >> 1) + 56, ((this.optionDrawOffsetY + 40) + this.optionslide_y) + 48);
        muiAniDrawer.setActionId(24);
        muiAniDrawer.draw(g, (SCREEN_WIDTH >> 1) - 96, ((this.optionDrawOffsetY + 40) + this.optionslide_y) + 72);
        animationDrawer = muiAniDrawer;
        if (GlobalResource.spsetConfig == 0) {
            i = 67;
        } else {
            i = (Key.touchmenuoptionitems[7].Isin() && this.pauseOptionCursor == 3 && this.isSelectable) ? 1 : 0;
            i += 57;
        }
        animationDrawer.setActionId(i);
        muiAniDrawer.draw(g, (SCREEN_WIDTH >> 1) + 56, ((this.optionDrawOffsetY + 40) + this.optionslide_y) + 72);
        switch (GlobalResource.sensorConfig) {
            case 0:
                muiAniDrawer.setActionId(70);
                break;
            case 1:
                muiAniDrawer.setActionId(69);
                break;
            case 2:
                muiAniDrawer.setActionId(68);
                break;
        }
        muiAniDrawer.draw(g, (SCREEN_WIDTH >> 1) + 56, ((this.optionDrawOffsetY + 40) + this.optionslide_y) + 72);
        animationDrawer = muiAniDrawer;
        if (Key.touchmenuoptionitems[8].Isin() && this.pauseOptionCursor == 4 && this.isSelectable) {
            i = 1;
        } else {
            i = 0;
        }
        animationDrawer.setActionId(i + 27);
        muiAniDrawer.draw(g, (SCREEN_WIDTH >> 1) - 96, ((this.optionDrawOffsetY + 40) + this.optionslide_y) + 96);
        this.optionOffsetX -= 4;
        this.optionOffsetX %= 100;
        muiAniDrawer.setActionId(51);
        for (int x1 = this.optionOffsetX; x1 < SCREEN_WIDTH * 2; x1 += 100) {
            muiAniDrawer.draw(g, x1, 0);
        }
        animationDrawer = muiAniDrawer;
        if (Key.touchmenuoptionreturn.Isin()) {
            i = 5;
        } else {
            i = 0;
        }
        animationDrawer.setActionId(i + 61);
        muiAniDrawer.draw(g, 0, SCREEN_HEIGHT);
        State.drawFade(g);
    }

    private void setStateWithFade(int nState) {
        this.isStateClassSwitch = true;
        if (!fading) {
            fading = true;
            State.fadeInit(0, 255);
            this.stateForSet = nState;
        }
    }

    public void changeStateWithFade(int nState) {
        this.isStateClassSwitch = false;
        if (!fading) {
            fading = true;
            if (nState == 7) {
                State.fadeInit(102, 255);
            } else {
                State.fadeInit(0, 255);
            }
            this.nextState = nState;
            this.fadeChangeState = true;
        }
    }

    public void fadeStateLogic() {
        if (!this.isStateClassSwitch) {
            if (fading && this.fadeChangeState && State.fadeChangeOver() && this.state != this.nextState) {
                this.state = this.nextState;
                this.fadeChangeState = false;
                if (this.state == 1) {
                    State.fadeInit_Modify(255, 102);
                } else {
                    State.fadeInit(255, 0);
                }
            }
            if (this.state == this.nextState && State.fadeChangeOver()) {
                fading = false;
            }
        } else if (fading && State.fadeChangeOver()) {
            State.setState(this.stateForSet);
            State.fadeInit(255, 0);
        }
    }

    private void endingInit() {
        this.planeDrawer = new Animation("/animation/ending/ending_plane").getDrawer();
        this.cloudDrawer = new Animation("/animation/ending/ending_cloud").getDrawer();
        Animation birdAnimation = new Animation("/animation/ending/ending_bird");
        this.birdDrawer = new AnimationDrawer[10];
        for (int i = 0; i < 10; i++) {
            this.birdDrawer[i] = birdAnimation.getDrawer();
        }
        this.endingState = (byte) 0;
        this.cloudCount = 0;
        this.count = 20;
        planeInit();
        birdInit();
        staffInit();
        SoundSystem.getInstance().playBgm(33, false);
    }

    private void endingLogic() {
        if (this.count > 0) {
            this.count--;
        }
        degreeLogic();
        cloudLogic();
        switch (this.endingState) {
            case (byte) 0:
                if (this.count == 0) {
                    this.endingState = (byte) 1;
                    return;
                }
                return;
            case (byte) 1:
                planeLogic();
                if (this.planeX == (SCREEN_WIDTH >> 1)) {
                    this.endingState = (byte) 2;
                    return;
                }
                return;
            case (byte) 2:
                if (birdLogic()) {
                    this.endingState = (byte) 3;
                    return;
                }
                return;
            case (byte) 3:
                if (this.showCount > 0) {
                    this.showCount--;
                }
                if (this.showCount == 0) {
                    this.changing = true;
                }
                if (!this.changing) {
                    return;
                }
                if (this.outing) {
                    this.position = MyAPI.calNextPositionReverse(this.position, SCREEN_WIDTH >> 1, (SCREEN_WIDTH * 3) >> 1, 1, 3);
                    if (this.position == ((SCREEN_WIDTH * 3) >> 1)) {
                        this.outing = false;
                        this.position = (-SCREEN_WIDTH) >> 1;
                        this.endingState = (byte) 4;
                        Key.touchkeyboardClose();
                        Key.touchkeyboardInit();
                        return;
                    }
                    return;
                }
                this.position = MyAPI.calNextPosition((double) this.position, (double) (SCREEN_WIDTH >> 1), 1, 3);
                if (this.position == (SCREEN_WIDTH >> 1)) {
                    this.outing = true;
                    this.showCount = 45;
                    this.changing = false;
                    return;
                }
                return;
            case (byte) 4:
                staffLogic();
                if (!this.changing && this.showCount == 0 && this.stringCursor >= STAFF_STR.length - 1) {
                    this.endingState = (byte) 5;
                    return;
                }
                return;
            case (byte) 5:
                if (Key.press(Key.B_S1 | Key.gSelect)) {
                    SoundSystem.getInstance().stopBgm(true);
                    setStateWithFade(4);
                    return;
                }
                return;
            default:
                return;
        }
    }

    private void endingDraw(MFGraphics g) {
        g.setColor(35064);
        MyAPI.fillRect(g, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
        cloudDraw(g);
        birdDraw1(g);
        planeDraw(g);
        birdDraw2(g);
        switch (this.endingState) {
            case (byte) 3:
                State.drawMenuFontById(g, 79, this.position, STAFF_CENTER_Y);
                return;
            case (byte) 4:
                staffDraw(g);
                return;
            case (byte) 5:
                staffDraw(g);
                State.drawSoftKey(g, true, false);
                return;
            default:
                return;
        }
    }

    private void cloudLogic() {
        if (this.cloudCount > 0) {
            this.cloudCount--;
        }
        for (int i = 0; i < 10; i++) {
            if (this.cloudInfo[i][0] != 0) {
                int[] iArr = this.cloudInfo[i];
                iArr[1] = iArr[1] + CLOUD_VELOCITY[this.cloudInfo[i][0] - 1];
                if (this.cloudInfo[i][1] >= SCREEN_WIDTH + 75) {
                    this.cloudInfo[i][0] = 0;
                }
            }
            if (this.cloudInfo[i][0] == 0 && this.cloudCount == 0) {
                this.cloudInfo[i][0] = MyRandom.nextInt(1, 3);
                this.cloudInfo[i][1] = 0;
                this.cloudInfo[i][2] = MyRandom.nextInt(20, SCREEN_HEIGHT - 40);
                this.cloudCount = MyRandom.nextInt(8, 20);
            }
        }
    }

    private void cloudDraw(MFGraphics g) {
        for (int i = 0; i < 10; i++) {
            if (this.cloudInfo[i][0] != 0) {
                this.cloudDrawer.setActionId(this.cloudInfo[i][0] - 1);
                this.cloudDrawer.draw(g, this.cloudInfo[i][1], this.cloudInfo[i][2]);
            }
        }
    }

    private void planeInit() {
        this.planeX = SCREEN_WIDTH + 60;
        this.planeY = (SCREEN_HEIGHT * 4) / 5;
    }

    private void planeLogic() {
        this.planeX -= 2;
        if (this.planeX < (SCREEN_WIDTH >> 1)) {
            this.planeX = SCREEN_WIDTH >> 1;
        }
    }

    private void planeDraw(MFGraphics g) {
        this.planeDrawer.draw(g, this.planeX, this.planeY + getOffsetY(0));
    }

    private void birdInit() {
        int i;
        for (i = 4; i >= 0; i--) {
            this.birdInfo[i][1] = (this.planeY - ((5 - i) * 10)) - 30;
            this.birdInfo[i][0] = (5 - i) * 10;
            this.birdInfo[i][2] = MyRandom.nextInt(MDPhone.SCREEN_WIDTH);
        }
        for (i = 5; i < 10; i++) {
            this.birdInfo[i][1] = (this.planeY - ((4 - i) * 14)) - 15;
            this.birdInfo[i][0] = (4 - i) * -14;
            this.birdInfo[i][2] = MyRandom.nextInt(MDPhone.SCREEN_WIDTH);
        }
        this.birdX = SCREEN_WIDTH + 30;
    }

    private boolean birdLogic() {
        this.birdX -= 2;
        if (this.birdX >= (SCREEN_WIDTH >> 1) + 30) {
            return false;
        }
        this.birdX = (SCREEN_WIDTH >> 1) + 30;
        return true;
    }

    private void birdDraw1(MFGraphics g) {
        for (int i = 0; i < 5; i++) {
            this.birdDrawer[i].draw(g, this.birdX + this.birdInfo[i][0], this.birdInfo[i][1] + getOffsetY(this.birdInfo[i][2]));
        }
    }

    private void birdDraw2(MFGraphics g) {
        for (int i = 5; i < 10; i++) {
            this.birdDrawer[i].draw(g, this.birdX + this.birdInfo[i][0], this.birdInfo[i][1] + getOffsetY(this.birdInfo[i][2]));
        }
    }

    private void degreeLogic() {
        this.degree += 10;
        this.degree %= MDPhone.SCREEN_WIDTH;
    }

    private int getOffsetY(int degreeOffset) {
        return (MyAPI.dSin(this.degree + degreeOffset) * 10) / 100;
    }

    private void staffInit() {
        this.colorCursor = MyRandom.nextInt(COLOR_SEQ.length);
        this.stringCursor = 0;
        this.position = (-SCREEN_WIDTH) >> 1;
        this.outing = false;
    }

    private void staffLogic() {
        if (this.showCount > 0) {
            this.showCount--;
        }
        if (this.showCount > 0 && Key.repeatAnyKey()) {
            this.showCount = 0;
        }
        if (this.showCount == 0 && this.stringCursor < STAFF_STR.length - 1) {
            this.changing = true;
        }
        if (!this.changing) {
            return;
        }
        if (this.outing) {
            this.position = MyAPI.calNextPositionReverse(this.position, SCREEN_WIDTH >> 1, (SCREEN_WIDTH * 3) >> 1, 1, 3);
            if (this.position == ((SCREEN_WIDTH * 3) >> 1)) {
                this.outing = false;
                this.position = (-SCREEN_WIDTH) >> 1;
                this.stringCursor++;
                this.colorCursor += MyRandom.nextInt(1, COLOR_SEQ.length - 1);
                this.colorCursor %= COLOR_SEQ.length;
                return;
            }
            return;
        }
        this.position = MyAPI.calNextPosition((double) this.position, (double) (SCREEN_WIDTH >> 1), 1, 3);
        if (this.position == (SCREEN_WIDTH >> 1)) {
            this.changing = false;
            this.outing = true;
            this.showCount = 45;
        }
    }

    private void staffDraw(MFGraphics g) {
    }

    private static void drawTouchKeyPause(MFGraphics g) {
    }

    private static void drawTouchKeyPauseOption(MFGraphics g) {
    }

    public void BacktoGame() {
        doReturnGameStuff();
        isDrawTouchPad = true;
        Key.touchgamekeyInit();
        Key.touchkeyboardClose();
        Key.touchkeygameboardInit();
        if (Key.touchkey_pause != null) {
            Key.touchkey_pause.resetKeyState();
        }
        Key.touchkeyboardReset();
        Key.touchanykeyClose();
    }

    private void drawScrollString(MFGraphics g, String string, int y, int speed, int space, int color1, int color2, int color3, int anchor) {
        this.itemOffsetX += speed;
        this.itemOffsetX %= space;
        int x = 0;
        while (x - this.itemOffsetX > 0) {
            x -= space;
        }
        int drawNum = (((SCREEN_WIDTH + space) - 1) / space) + 2;
        for (int i = 0; i < drawNum; i++) {
            MyAPI.drawBoldString(g, string, (x + (i * space)) - this.itemOffsetX, y, 17, color1, color2, color3);
        }
    }

    public void BP_GotoTryPaying() {
        if (StageManager.getStageID() == 1 && !IsPaid) {
            BP_continueTryInit();
        }
        if (StageManager.getStageID() == 2 && !IsPaid) {
            this.state = 20;
            BP_enteredPaying = false;
            this.BP_IsFromContinueTry = false;
            BP_payingInit(4, 3);
            State.fadeInit(255, 0);
        }
        if (IsPaid) {
            this.state = 5;
        }
    }

    public void BP_continueTryInit() {
        this.state = 19;
        State.fadeInit(255, 0);
        MyAPI.initString();
        strForShow = MyAPI.getStrings(BPstrings[0], this.BP_CONTINUETRY_MENU_WIDTH - 20);
        this.BP_CONTINUETRY_MENU_HEIGHT = this.MORE_GAME_HEIGHT + ((strForShow.length - 1) * LINE_SPACE);
        this.BP_CONTINUETRY_MENU_START_Y = (SCREEN_HEIGHT - this.BP_CONTINUETRY_MENU_HEIGHT) >> 1;
        PlayerObject.cursor = 0;
    }

    public void BP_continueTryLogic() {
        boolean IsUp;
        boolean IsDown;
        Key.touchkeyboardInit();
        PlayerObject.cursorMax = 2;
        if (Key.press(Key.gUp)) {
            IsUp = true;
        } else {
            IsUp = false;
        }
        if (Key.press(Key.gDown)) {
            IsDown = true;
        } else {
            IsDown = false;
        }
        if (IsUp) {
            PlayerObject.cursor--;
            PlayerObject.cursor += PlayerObject.cursorMax;
            PlayerObject.cursor %= PlayerObject.cursorMax;
        } else if (IsDown) {
            PlayerObject.cursor++;
            PlayerObject.cursor %= PlayerObject.cursorMax;
        } else if (!Key.press(Key.B_S1 | Key.gSelect)) {
        } else {
            if (PlayerObject.cursor == 0) {
                this.state = 20;
                BP_enteredPaying = false;
                BP_payingInit(4, 3);
                this.BP_IsFromContinueTry = true;
                State.fadeInit(255, 0);
            } else if (PlayerObject.cursor == 1) {
                this.state = 5;
                State.fadeInit(255, 0);
            }
        }
    }

    public void BP_continueTryDraw(MFGraphics g) {
        menuBgDraw(g);
        State.fillMenuRect(g, this.BP_CONTINUETRY_MENU_START_X, this.BP_CONTINUETRY_MENU_START_Y, this.BP_CONTINUETRY_MENU_WIDTH, this.BP_CONTINUETRY_MENU_HEIGHT);
        g.setColor(0);
        MyAPI.drawBoldStrings(g, strForShow, this.BP_CONTINUETRY_MENU_START_X + 20, this.BP_CONTINUETRY_MENU_START_Y + 10, this.BP_CONTINUETRY_MENU_WIDTH - 20, this.BP_CONTINUETRY_MENU_HEIGHT - 20, MapManager.END_COLOR, 4656650, 0);
        State.drawMenuFontById(g, 119, SCREEN_WIDTH >> 1, ((this.BP_CONTINUETRY_MENU_START_Y + 15) + ((MENU_SPACE * 3) / 2)) + (MENU_SPACE * ((PlayerObject.cursor + strForShow.length) - 1)));
        State.drawMenuFontById(g, StringIndex.STR_RIGHT_ARROW, (SCREEN_WIDTH >> 1) - 56, ((this.BP_CONTINUETRY_MENU_START_Y + 15) + ((MENU_SPACE * 3) / 2)) + (MENU_SPACE * ((PlayerObject.cursor + strForShow.length) - 1)));
        MyAPI.drawBoldString(g, BPstrings[1], SCREEN_WIDTH >> 1, (this.BP_CONTINUETRY_MENU_START_Y + 15) + (MENU_SPACE * ((strForShow.length - 1) + 1)), 17, MapManager.END_COLOR, 0);
        MyAPI.drawBoldString(g, BPstrings[2], SCREEN_WIDTH >> 1, (this.BP_CONTINUETRY_MENU_START_Y + 15) + (MENU_SPACE * ((strForShow.length - 1) + 2)), 17, MapManager.END_COLOR, 0);
        State.drawSoftKey(g, true, false);
    }

    public void gotoGameOver() {
        IsGameOver = true;
        this.overcnt = 0;
        this.state = 28;
        this.overtitleID = 78;
        SoundSystem.getInstance().stopBgm(true);
        SoundSystem.getInstance().playBgm(30, false);
        this.movingTitleX = SCREEN_WIDTH + 30;
        Key.touchgamekeyClose();
        Key.touchkeygameboardClose();
        Key.touchkeyboardInit();
    }

    public void BP_gotoRanking() {
        PlayerObject.doPauseLeaveGame();
        setStateWithFade(4);
        if (IsPaid) {
            StageManager.addNewNormalScore(IsGameOver ? PlayerObject.getScore() : this.preScore);
        } else {
            StageManager.addNewNormalScore(0);
        }
        StageManager.resetOpenedStageIdforTry(IsGameOver ? StageManager.getStageID() : StageManager.getStageID() - 1);
        Key.touchanykeyInit();
    }

    public void BP_payingLogic() {
        if (!BP_enteredPaying) {
            if (State.BP_chargeLogic(0)) {
                BP_enteredPaying = true;
                if (!this.BP_IsFromContinueTry) {
                    State.setTry();
                }
                State.activeGameProcess(true);
                State.setMenu();
                State.saveBPRecord();
                if (IsGameOver) {
                    BP_gotoRanking();
                    return;
                }
                this.state = 5;
                State.fadeInit(255, 0);
                return;
            }
            BP_enteredPaying = true;
            if (!this.BP_IsFromContinueTry) {
                State.setTry();
            }
            StageManager.resetStageIdforTry();
            State.saveBPRecord();
            State.setState(2);
        }
    }

    public void BP_gotoRevive() {
        this.state = 21;
        BP_payingInit(6, 5);
        State.fadeInit(255, 0);
    }

    public void BP_reviveLogic() {
        if (State.BP_chargeLogic(1)) {
            GameObject.player.resetPlayer();
            this.state = 0;
            State.fadeInit(255, 0);
            return;
        }
        gotoGameOver();
    }

    public void shopInit(boolean IsClearCursor) {
        if (IsClearCursor) {
            PlayerObject.cursor = 0;
        }
        this.state = 22;
        MyAPI.initString();
        Key.clear();
    }

    public void BP_shopLogic() {
        boolean IsUp;
        boolean IsDown;
        this.IsInBP = false;
        Key.touchkeyboardInit();
        PlayerObject.cursorMax = 3;
        if (Key.press(Key.gUp)) {
            IsUp = true;
        } else {
            IsUp = false;
        }
        if (Key.press(Key.gDown)) {
            IsDown = true;
        } else {
            IsDown = false;
        }
        if (IsUp) {
            PlayerObject.cursor--;
            PlayerObject.cursor += PlayerObject.cursorMax;
            PlayerObject.cursor %= PlayerObject.cursorMax;
        } else if (IsDown) {
            PlayerObject.cursor++;
            PlayerObject.cursor %= PlayerObject.cursorMax;
        } else if (Key.press(Key.B_S1 | Key.gSelect)) {
            if (!BP_IsToolsNumMax()) {
                this.state = 23;
                tool_id = PlayerObject.cursor;
                BP_payingInit(8, 7);
            }
        } else if (Key.press(2)) {
            this.state = 1;
            GameObject.IsGamePause = true;
            PlayerObject.cursor = 2;
            Key.clear();
        }
    }

    private boolean BP_IsToolsNumMax() {
        if (BP_items_num[currentBPItems[PlayerObject.cursor]] + currentBPItemsNormalNum[PlayerObject.cursor] <= 99) {
            return false;
        }
        this.state = 24;
        return true;
    }

    public void BP_shopDraw(MFGraphics g) {
        menuBgDraw(g);
        State.fillMenuRect(g, CASE_X, 30, CASE_WIDTH, CASE_HEIGHT);
        MyAPI.drawImage(g, BP_wordsImg, 0, 0, BP_wordsWidth, BP_wordsHeight, 0, SCREEN_WIDTH >> 1, 40, 17);
        MyAPI.drawBoldString(g, BPstrings[10], (CASE_WIDTH >> 2) + CASE_X, LINE_SPACE + 40, 17, MapManager.END_COLOR, 4656650);
        MyAPI.drawBoldString(g, BPstrings[11], ((CASE_WIDTH * 3) >> 2) + CASE_X, LINE_SPACE + 40, 17, MapManager.END_COLOR, 4656650);
        for (int i = 0; i < currentBPItems.length; i++) {
            MyAPI.drawImage(g, BP_itemsImg, BP_itemsWidth * currentBPItems[i], 0, BP_itemsWidth, BP_itemsHeight, 0, (CASE_X + (CASE_WIDTH >> 2)) - (LINE_SPACE >> 1), (LINE_SPACE * (i + 3)) + 30, 3);
            MyAPI.drawBoldString(g, "× " + currentBPItemsNormalNum[i], BP_itemsWidth + ((CASE_X + (CASE_WIDTH >> 2)) - (LINE_SPACE >> 1)), ((LINE_SPACE * (i + 3)) + 30) - Def.FONT_H_HALF, 20, MapManager.END_COLOR, 4656650);
            MyAPI.drawBoldString(g, ((Byte)BP_items_num[currentBPItems[i]]).toString(), (Def.FONT_WIDTH_NUM * 2) + (CASE_X + ((CASE_WIDTH * 3) >> 2)), ((LINE_SPACE * (i + 3)) + 30) - Def.FONT_H_HALF, 24, MapManager.END_COLOR, 4656650);
        }
        State.drawMenuFontById(g, StringIndex.STR_RIGHT_ARROW, ((CASE_X + (CASE_WIDTH >> 2)) - (LINE_SPACE >> 1)) - (BP_itemsWidth * 2), (LINE_SPACE * (PlayerObject.cursor + 3)) + 30);
        MyAPI.drawBoldString(g, BPstrings[12], BP_itemsWidth + CASE_X, (LINE_SPACE * 7) + 30, 20, MapManager.END_COLOR, 4656650);
        g.setColor(0);
        MyAPI.drawBoldStrings(g, BPEffectStrings[PlayerObject.cursor], BP_itemsWidth + CASE_X, (LINE_SPACE * 8) + 30, MENU_RECT_WIDTH - 20, CASE_HEIGHT - 20, MapManager.END_COLOR, 4656650, 0);
        State.drawSoftKey(g, true, true);
    }

    public static void BP_toolsAdd() {
        byte[] bArr = BP_items_num;
        int i = currentBPItems[tool_id];
        bArr[i] = (byte) (bArr[i] + currentBPItemsNormalNum[tool_id]);
        State.saveBPRecord();
    }

    public void BP_buyLogic() {
        if (State.BP_chargeLogic(2)) {
            BP_toolsAdd();
            shopInit(false);
            return;
        }
        shopInit(false);
    }

    public void BP_toolsmaxLogic() {
        if (Key.press(2)) {
            shopInit(false);
        }
    }

    public void BP_toolsmaxDraw(MFGraphics g) {
        menuBgDraw(g);
        State.fillMenuRect(g, FRAME_X, (SCREEN_HEIGHT >> 1) - MENU_SPACE, FRAME_WIDTH, MENU_SPACE << 1);
        g.setColor(0);
        MyAPI.drawBoldString(g, BPstrings[19], SCREEN_WIDTH >> 1, ((SCREEN_HEIGHT >> 1) - MENU_SPACE) + 10, 17, MapManager.END_COLOR, 4656650);
        State.drawSoftKey(g, false, true);
    }

    public boolean IsToolsUsed(int id) {
        switch (id) {
            case 0:
                return PlayerObject.IsInvincibility();
            case 1:
            case 2:
                return PlayerObject.IsUnderSheild();
            case 3:
                return PlayerObject.IsSpeedUp();
            default:
                return false;
        }
    }

    public void BP_toolsuseLogic() {
        boolean IsUp;
        boolean IsDown;
        Key.touchkeyboardInit();
        PlayerObject.cursorMax = 3;
        if (Key.press(Key.gUp)) {
            IsUp = true;
        } else {
            IsUp = false;
        }
        if (Key.press(Key.gDown)) {
            IsDown = true;
        } else {
            IsDown = false;
        }
        if (IsUp) {
            PlayerObject.cursor--;
            PlayerObject.cursor += PlayerObject.cursorMax;
            PlayerObject.cursor %= PlayerObject.cursorMax;
        } else if (IsDown) {
            PlayerObject.cursor++;
            PlayerObject.cursor %= PlayerObject.cursorMax;
        }
        if (Key.press(Key.B_S1 | Key.gSelect) && BP_items_num[currentBPItems[PlayerObject.cursor]] > (byte) 0 && !IsToolsUsed(currentBPItems[PlayerObject.cursor])) {
            this.state = 26;
            this.cursor = 0;
        }
        if (Key.press(2)) {
            BacktoGame();
        }
    }

    public void BP_ensureToolsUseLogic() {
        switch (comfirmLogic()) {
            case 0:
                switch (currentBPItems[PlayerObject.cursor]) {
                    case 0:
                        GameObject.player.getItem(3);
                        break;
                    case 1:
                        GameObject.player.getItem(2);
                        break;
                    case 2:
                        GameObject.player.getItem(1);
                        break;
                    case 3:
                        GameObject.player.getItem(4);
                        break;
                }
                byte[] bArr = BP_items_num;
                int i = currentBPItems[PlayerObject.cursor];
                bArr[i] = (byte) (bArr[i] - 1);
                State.saveBPRecord();
                BacktoGame();
                return;
            case 1:
            case 400:
                this.state = 25;
                return;
            default:
                return;
        }
    }

    public void BP_ensureToolsUseDraw(MFGraphics g) {
        confirmDraw(g, BPstrings[currentBPItems[PlayerObject.cursor] + 20]);
        State.drawSoftKey(g, true, true);
    }

    public void BP_toolsuseDraw(MFGraphics g) {
        State.fillMenuRect(g, (SCREEN_WIDTH >> 1) + PlayerObject.PAUSE_FRAME_OFFSET_X, (SCREEN_HEIGHT >> 1) + PlayerObject.PAUSE_FRAME_OFFSET_Y, PlayerObject.PAUSE_FRAME_WIDTH, PlayerObject.PAUSE_FRAME_HEIGHT);
        MyAPI.drawImage(g, BP_wordsImg, 0, BP_wordsHeight, BP_wordsWidth, BP_wordsHeight, 0, SCREEN_WIDTH >> 1, LINE_SPACE + ((SCREEN_HEIGHT >> 1) + PlayerObject.PAUSE_FRAME_OFFSET_Y), 3);
        State.drawMenuFontById(g, 119, SCREEN_WIDTH >> 1, (((((SCREEN_HEIGHT >> 1) + PlayerObject.PAUSE_FRAME_OFFSET_Y) + 10) + (MENU_SPACE >> 1)) + MENU_SPACE) + (MENU_SPACE * PlayerObject.cursor));
        State.drawMenuFontById(g, StringIndex.STR_RIGHT_ARROW, (SCREEN_WIDTH >> 1) - 56, (((((SCREEN_HEIGHT >> 1) + PlayerObject.PAUSE_FRAME_OFFSET_Y) + 10) + (MENU_SPACE >> 1)) + MENU_SPACE) + (MENU_SPACE * PlayerObject.cursor));
        int i = 0;
        while (i < currentBPItems.length) {
            int i2;
            MFImage mFImage = BP_itemsImg;
            int i3 = BP_itemsWidth * currentBPItems[i];
            if (BP_items_num[currentBPItems[i]] == (byte) 0 || IsToolsUsed(currentBPItems[i])) {
                i2 = BP_itemsHeight;
            } else {
                i2 = 0;
            }
            MyAPI.drawImage(g, mFImage, i3, i2, BP_itemsWidth, BP_itemsHeight, 0, (SCREEN_WIDTH >> 1) - (BP_itemsWidth * 2), (MENU_SPACE * i) + (((((SCREEN_HEIGHT >> 1) + PlayerObject.PAUSE_FRAME_OFFSET_Y) + 10) + (MENU_SPACE >> 1)) + MENU_SPACE), 6);
            g.setColor(0);
            MFGraphics mFGraphics = g;
            MyAPI.drawBoldString(mFGraphics, "×", SCREEN_WIDTH >> 1, ((((((SCREEN_HEIGHT >> 1) + PlayerObject.PAUSE_FRAME_OFFSET_Y) + 10) + (MENU_SPACE >> 1)) + MENU_SPACE) + (MENU_SPACE * i)) - Def.FONT_H_HALF, 17, MapManager.END_COLOR, 4656650);
            MyAPI.drawBoldString(g, ((Byte)BP_items_num[currentBPItems[i]]).toString(), (BP_itemsWidth * 2) + (SCREEN_WIDTH >> 1), ((((((SCREEN_HEIGHT >> 1) + PlayerObject.PAUSE_FRAME_OFFSET_Y) + 10) + (MENU_SPACE >> 1)) + MENU_SPACE) + (MENU_SPACE * i)) - Def.FONT_H_HALF, 24, MapManager.END_COLOR, 4656650);
            i++;
        }
        if (BP_items_num[currentBPItems[PlayerObject.cursor]] == (byte) 0) {
            this.tooltipY = MyAPI.calNextPosition((double) this.tooltipY, (double) TOOL_TIP_Y_DES, 1, 3);
        } else {
            this.tooltipY = MyAPI.calNextPositionReverse(this.tooltipY, TOOL_TIP_Y_DES, TOOL_TIP_Y_DES_2, 1, 3);
        }
        State.fillMenuRect(g, TOOL_TIP_X, this.tooltipY, TOOL_TIP_WIDTH, TOOL_TIP_HEIGHT);
        for (i = 0; i < TOOL_TIP_STR.length; i++) {
            MyAPI.drawBoldString(g, TOOL_TIP_STR[i], SCREEN_WIDTH >> 1, (LINE_SPACE * i) + (this.tooltipY + 10), 17, MapManager.END_COLOR, 4656650, 0);
        }
        State.drawSoftKey(g, true, true);
    }

    private void initStageInfoClearRes() {
        if (this.stageInfoClearAni == null) {
            this.stageInfoClearAni = Animation.getInstanceFromQi("/animation/utl_res/stage_intro_clear.dat");
            stageInfoAniDrawer = this.stageInfoClearAni[0].getDrawer(0, false, 0);
            this.stageInfoPlayerNameDrawer = this.stageInfoClearAni[0].getDrawer(PlayerObject.getCharacterID() + 23, false, 0);
            this.stageInfoActNumDrawer = this.stageInfoClearAni[0].getDrawer(27, false, 0);
            if (guiAnimation == null) {
                guiAnimation = new Animation("/animation/gui");
            }
            guiAniDrawer = guiAnimation.getDrawer(0, false, 0);
        }
        this.IsPlayerNameDrawable = false;
        this.IsActNumDrawable = false;
    }

    private void drawLoading(MFGraphics g) {
        switch (loadingType) {
            case 0:
                g.setColor(0);
                MyAPI.fillRect(g, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
                break;
            case 1:
                g.setColor(MapManager.END_COLOR);
                MyAPI.fillRect(g, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
                break;
            case 2:
                g.setColor(0);
                MyAPI.fillRect(g, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
                g.setColor(MapManager.END_COLOR);
                MyAPI.fillRect(g, 0, ((SCREEN_HEIGHT >> 1) - 36) - 10, SCREEN_WIDTH, 20);
                break;
        }
        this.loadingWordsDrawer.draw(g, SCREEN_WIDTH, SCREEN_HEIGHT);
        this.loadingDrawer.setActionId(1);
        this.loadingDrawer.draw(g, SCREEN_WIDTH >> 1, SCREEN_HEIGHT >> 1);
        if (tipsForShow == null) {
            tipsForShow = MyAPI.getStrings(this.TIPS[MyRandom.nextInt(0, this.TIPS.length - 1)], PlayerObject.SONIC_ATTACK_LEVEL_3_V0);
            MyAPI.initString();
            return;
        }
        MyAPI.drawStrings(g, tipsForShow, (SCREEN_WIDTH >> 1) - 72, ((SCREEN_HEIGHT >> 1) - 50) - 2, 132, 100, 20, MapManager.END_COLOR, 4656650, 0);
    }

    private void initStageIntroType1Conf() {
        this.frameCount = 0;
        this.display[0][0] = -50;
        this.display[0][1] = 0;
        this.display[0][2] = 0;
        this.display[0][3] = 0;
        this.display[1][0] = SCREEN_WIDTH;
        this.display[1][1] = (SCREEN_HEIGHT >> 1) + 48;
        this.display[1][2] = 0;
        this.display[1][3] = 0;
        this.display[2][0] = 48;
        this.display[2][1] = (SCREEN_HEIGHT >> 1) + 48;
        this.display[2][2] = 0;
        this.display[2][3] = 0;
        this.display[3][0] = 0;
        this.display[3][1] = 0;
        this.display[3][2] = 0;
        this.display[3][3] = 0;
        this.display[4][0] = SCREEN_WIDTH;
        this.display[4][1] = SCREEN_HEIGHT;
        this.display[4][2] = 0;
        this.display[4][3] = 0;
        this.display[5][0] = SCREEN_WIDTH;
        this.display[5][1] = (SCREEN_HEIGHT >> 1) - 10;
        this.display[5][2] = 0;
        this.display[5][3] = 0;
    }

    private void initStageIntroType2Conf() {
        this.frameCount = 0;
        this.display[0][0] = -50;
        this.display[0][1] = 0;
        this.display[0][2] = 0;
        this.display[0][3] = 0;
        this.display[1][0] = 0;
        this.display[1][1] = (SCREEN_HEIGHT >> 1) - 36;
        this.display[1][2] = 0;
        this.display[1][3] = 0;
        this.display[2][0] = 48;
        this.display[2][1] = (SCREEN_HEIGHT >> 1) + 48;
        this.display[2][2] = 0;
        this.display[2][3] = 0;
        this.display[3][0] = 0;
        this.display[3][1] = 0;
        this.display[3][2] = 0;
        this.display[3][3] = 0;
        this.display[4][0] = SCREEN_WIDTH;
        this.display[4][1] = SCREEN_HEIGHT;
        this.display[4][2] = 0;
        this.display[4][3] = 0;
        this.display[5][0] = SCREEN_WIDTH;
        this.display[5][1] = (SCREEN_HEIGHT >> 1) - 10;
        this.display[5][2] = 0;
        this.display[5][3] = 0;
    }

    private void drawTimeOver(MFGraphics g, int x, int y) {
        int id = 11;
        if (this.overtitleID == 78) {
            id = 11;
        } else if (this.overtitleID == Def.TOUCH_HELP_HEIGHT) {
            id = 10;
        }
        if (guiAnimation == null) {
            guiAnimation = new Animation("/animation/gui");
        }
        guiAniDrawer = guiAnimation.getDrawer(0, false, 0);
        guiAniDrawer.setActionId(id);
        guiAniDrawer.draw(g, x, y);
    }

    private void continueInit() {
        this.state = 42;
        this.continueFrame = 0;
        this.continueScale = 1.0f;
        this.continueMoveBlackBarX = -SCREEN_WIDTH;
        this.continueMoveNumberX = -30;
        this.continueNumber = 9;
        this.continueNumberState = 0;
        this.continueNumberScale = 1.0f;
        this.continueCursor = -1;
        Key.touchgameoverensurekeyInit();
        if (guiAnimation == null) {
            guiAnimation = new Animation("/animation/gui");
        }
        guiAniDrawer = guiAnimation.getDrawer(0, false, 0);
        if (numberDrawer == null) {
            numberDrawer = new Animation("/animation/number").getDrawer(0, false, 0);
        }
        isThroughGame = false;
    }

    private void continueEnd() {
        this.continueNumberState = 3;
        this.continueStartEndFrame = this.continueFrame;
        StageManager.resetStageIdforContinueEnd();
        Key.touchgameoverensurekeyClose();
        isThroughGame = false;
    }

    private void drawGameOver(MFGraphics g) {
        if (this.continueFrame <= 5) {
            MyAPI.drawScaleAni(g, guiAniDrawer, 11, SCREEN_WIDTH >> 1, (SCREEN_HEIGHT >> 1) - 28, this.continueScale, 1.0f, 0.0f, 0.0f);
        } else if (this.continueFrame <= 10) {
            MyAPI.drawScaleAni(g, guiAniDrawer, 12, SCREEN_WIDTH >> 1, (SCREEN_HEIGHT >> 1) - 28, this.continueScale, 1.0f, 0.0f, 0.0f);
        }
        if (this.continueFrame > 10) {
            MyAPI.drawScaleAni(g, guiAniDrawer, 12, SCREEN_WIDTH >> 1, (SCREEN_HEIGHT >> 1) - 28, this.continueScale, this.continueScale, 0.0f, 8.0f);
            g.setColor(0);
            MyAPI.fillRect(g, this.continueMoveBlackBarX, (SCREEN_HEIGHT >> 1) + 10, SCREEN_WIDTH, 20);
            if (this.continueMoveBlackBarX == 0) {
                if (this.continueNumberState < 3) {
                    MyAPI.drawScaleAni(g, numberDrawer, this.continueNumber + 32, this.continueMoveNumberX - 6, (SCREEN_HEIGHT >> 1) + 12, this.continueNumberScale, this.continueNumberScale, 6.0f, 8.0f);
                } else if (this.continueNumberState > 3) {
                    MyAPI.drawScaleAni(g, guiAniDrawer, 13, this.continueMoveNumberX, (SCREEN_HEIGHT >> 1) + 20, this.continueNumberScale, this.continueNumberScale, 0.0f, 0.0f);
                }
                if (this.continueNumberState < 3 && Key.touchgameover != null) {
                    drawGameOverTouchKey(g);
                }
            }
        }
    }

    private void drawGameOverSingle(MFGraphics g) {
        this.continueFrame++;
        if (this.continueFrame >= 65) {
            if (this.continueScale > 0.0f) {
                this.continueScale -= 0.2f;
            } else {
                this.continueScale = 0.0f;
            }
            MyAPI.drawScaleAni(g, guiAniDrawer, 11, SCREEN_WIDTH >> 1, (SCREEN_HEIGHT >> 1) - 28, this.continueScale, 1.0f, 0.0f, 0.0f);
            return;
        }
        MyAPI.drawScaleAni(g, guiAniDrawer, 11, SCREEN_WIDTH >> 1, (SCREEN_HEIGHT >> 1) - 28, 1.0f, 1.0f, 0.0f, 0.0f);
    }

    private void drawGameOverTouchKey(MFGraphics g) {
        if (Key.touchgameoveryres.Isin()) {
            State.drawTouchGameKeyBoardById(g, 10, SCREEN_WIDTH - 22, Def.TOUCH_A_Y);
        } else {
            State.drawTouchGameKeyBoardById(g, 9, SCREEN_WIDTH - 22, Def.TOUCH_A_Y);
        }
        if (Key.touchgameoverno.Isin()) {
            State.drawTouchGameKeyBoardById(g, 12, SCREEN_WIDTH - 67, Def.TOUCH_B_Y);
        } else {
            State.drawTouchGameKeyBoardById(g, 11, SCREEN_WIDTH - 67, Def.TOUCH_B_Y);
        }
        guiAniDrawer.draw(g, 14, SCREEN_WIDTH - 22, Def.TOUCH_A_Y, false, 0);
        guiAniDrawer.draw(g, 15, SCREEN_WIDTH - 67, Def.TOUCH_B_Y, false, 0);
    }

    private void gamePauseInit() {
        this.pausecnt = 0;
        this.pause_saw_x = -50;
        this.pause_saw_y = 0;
        this.pause_saw_speed = 30;
        this.pause_item_x = SCREEN_WIDTH - 26;
        if (PlayerObject.stageModeState == 0) {
            this.pause_item_y = (SCREEN_HEIGHT >> 1) - 36;
            Key.touchGamePauseInit(0);
        } else if (PlayerObject.stageModeState == 1) {
            this.pause_item_y = (SCREEN_HEIGHT >> 1) - 60;
            Key.touchGamePauseInit(1);
        }
        this.pause_item_speed = (-((SCREEN_WIDTH >> 1) + 14)) / 3;
        if (muiAniDrawer == null) {
            muiAniDrawer = new Animation("/animation/mui").getDrawer(0, false, 0);
        }
        this.pause_returnFlag = false;
    }

    private void gamePauseLogic() {
        this.pausecnt++;
        if (this.pausecnt >= 5 && this.pausecnt <= 7) {
            if (this.pause_saw_x + this.pause_saw_speed > 0) {
                this.pause_saw_x = 0;
            } else {
                this.pause_saw_x += this.pause_saw_speed;
            }
            if (this.pause_item_x + this.pause_item_speed < (SCREEN_WIDTH >> 1) - 40) {
                this.pause_item_x = (SCREEN_WIDTH >> 1) - 40;
            } else {
                this.pause_item_x += this.pause_item_speed;
            }
        } else if (this.pausecnt > 7) {
            int i;
            int items = PlayerObject.stageModeState == 0 ? 3 : 6;
            for (i = 0; i < items; i++) {
                if (Key.touchgamepauseitem[i].Isin() && Key.touchgamepause.IsClick()) {
                    this.pause_item_cursor = i;
                }
            }
            if (Key.touchgamepausereturn.Isin() && Key.touchgamepause.IsClick()) {
                this.pause_item_cursor = -2;
            }
            if ((Key.press(524288) || (Key.touchgamepausereturn.IsButtonPress() && this.pause_item_cursor == -2)) && !this.pause_returnFlag) {
                this.pause_returnFlag = true;
                this.pause_returnframe = this.pausecnt;
                SoundSystem.getInstance().playSe(2);
            }
            if (this.pause_returnFlag) {
                if (this.pausecnt > this.pause_returnframe && this.pausecnt <= this.pause_returnframe + 3) {
                    this.pause_saw_x -= this.pause_saw_speed;
                    this.pause_item_x -= this.pause_item_speed;
                } else if (this.pausecnt > this.pause_returnframe + 3) {
                    BacktoGame();
                    isDrawTouchPad = true;
                    this.pause_returnFlag = false;
                    State.setFadeOver();
                }
            }
            if (this.pause_optionFlag && this.pausecnt > this.pause_optionframe + 3) {
                optionInit();
                this.state = 7;
                this.pause_optionFlag = false;
            }
            if (!State.fadeChangeOver()) {
                for (i = 0; i < items; i++) {
                    Key.touchgamepauseitem[i].resetKeyState();
                }
            } else if (PlayerObject.stageModeState == 0) {
                if (Key.touchgamepauseitem[0].IsButtonPress() && this.pause_item_cursor == 0 && !this.pause_returnFlag) {
                    this.pause_returnFlag = true;
                    this.pause_returnframe = this.pausecnt;
                    SoundSystem.getInstance().playSe(1);
                } else if (Key.touchgamepauseitem[1].IsButtonPress() && this.pause_item_cursor == 1 && State.fadeChangeOver()) {
                    this.state = 10;
                    State.fadeInit(102, 220);
                    secondEnsureInit();
                    SoundSystem.getInstance().playSe(1);
                } else if (Key.touchgamepauseitem[2].IsButtonPress() && this.pause_item_cursor == 2 && !this.pause_returnFlag) {
                    changeStateWithFade(7);
                    optionInit();
                    SoundSystem.getInstance().playSe(1);
                }
            } else if (PlayerObject.stageModeState != 1) {
            } else {
                if (Key.touchgamepauseitem[0].IsButtonPress() && this.pause_item_cursor == 0 && !this.pause_returnFlag) {
                    this.pause_returnFlag = true;
                    this.pause_returnframe = this.pausecnt;
                    SoundSystem.getInstance().playSe(1);
                } else if (Key.touchgamepauseitem[1].IsButtonPress() && this.pause_item_cursor == 1 && State.fadeChangeOver()) {
                    this.state = 8;
                    State.fadeInit(102, 220);
                    secondEnsureInit();
                    SoundSystem.getInstance().playSe(1);
                } else if (Key.touchgamepauseitem[2].IsButtonPress() && this.pause_item_cursor == 2 && State.fadeChangeOver()) {
                    this.state = 29;
                    State.fadeInit(102, 220);
                    secondEnsureInit();
                    SoundSystem.getInstance().playSe(1);
                } else if (Key.touchgamepauseitem[3].IsButtonPress() && this.pause_item_cursor == 3 && State.fadeChangeOver()) {
                    this.state = 9;
                    State.fadeInit(102, 220);
                    secondEnsureInit();
                    SoundSystem.getInstance().playSe(1);
                } else if (Key.touchgamepauseitem[4].IsButtonPress() && this.pause_item_cursor == 4 && State.fadeChangeOver()) {
                    this.state = 10;
                    State.fadeInit(102, 220);
                    secondEnsureInit();
                    SoundSystem.getInstance().playSe(1);
                } else if (Key.touchgamepauseitem[5].IsButtonPress() && this.pause_item_cursor == 5 && !this.pause_returnFlag) {
                    changeStateWithFade(7);
                    optionInit();
                    SoundSystem.getInstance().playSe(1);
                }
            }
        }
    }

    private void drawGamePause(MFGraphics g) {
        if (this.pausecnt > 5) {
            muiAniDrawer.setActionId(50);
            muiAniDrawer.draw(g, this.pause_saw_x, this.pause_saw_y);
        }
        if (this.pausecnt > 7) {
            muiAniDrawer.setActionId((Key.touchgamepausereturn.Isin() ? 5 : 0) + 61);
            muiAniDrawer.draw(g, 0, SCREEN_HEIGHT);
        }
        if (this.pausecnt <= 5) {
            return;
        }
        AnimationDrawer animationDrawer;
        int i;
        if (PlayerObject.stageModeState == 0) {
            animationDrawer = muiAniDrawer;
            if (this.pause_item_cursor == 0 && Key.touchgamepauseitem[0].Isin()) {
                i = 1;
            } else {
                i = 0;
            }
            animationDrawer.setActionId(i + 2);
            muiAniDrawer.draw(g, this.pause_item_x, this.pause_item_y);
            animationDrawer = muiAniDrawer;
            if (this.pause_item_cursor == 1 && Key.touchgamepauseitem[1].Isin()) {
                i = 1;
            } else {
                i = 0;
            }
            animationDrawer.setActionId(i + 10);
            muiAniDrawer.draw(g, this.pause_item_x, this.pause_item_y + 24);
            animationDrawer = muiAniDrawer;
            if (this.pause_item_cursor == 2 && Key.touchgamepauseitem[2].Isin()) {
                i = 1;
            } else {
                i = 0;
            }
            animationDrawer.setActionId(i + 12);
            muiAniDrawer.draw(g, this.pause_item_x, this.pause_item_y + 48);
        } else if (PlayerObject.stageModeState == 1) {
            int i2 = 0;
            while (i2 < 6) {
                int i3;
                animationDrawer = muiAniDrawer;
                i = (i2 + 1) * 2;
                if (this.pause_item_cursor == i2 && Key.touchgamepauseitem[i2].Isin()) {
                    i3 = 1;
                } else {
                    i3 = 0;
                }
                animationDrawer.setActionId(i + i3);
                muiAniDrawer.draw(g, this.pause_item_x, this.pause_item_y + (i2 * 24));
                i2++;
            }
        }
    }

    private void pausetoTitleLogic() {
        switch (secondEnsureLogic()) {
            case 1:
                if (GameObject.stageModeState == 1) {
                    StageManager.doWhileLeaveRace();
                }
                PlayerObject.doPauseLeaveGame();
                if (GameObject.stageModeState != 1) {
                    StageManager.characterFromGame = PlayerObject.getCharacterID();
                }
                StageManager.stageIDFromGame = StageManager.getStageID();
                setStateWithFade(2);
                Key.touchanykeyInit();
                Key.touchMainMenuInit2();
                return;
            case 2:
                State.fadeInit_Modify(192, 102);
                this.state = 1;
                GameObject.IsGamePause = true;
                return;
            default:
                return;
        }
    }

    private void retryStageLogic() {
        switch (secondEnsureLogic()) {
            case 1:
                this.state = 5;
                loadingType = 0;
                initTips();
                return;
            case 2:
                State.fadeInit_Modify(192, 102);
                this.state = 1;
                GameObject.IsGamePause = true;
                return;
            default:
                return;
        }
    }

    private void pausetoSelectStageLogic() {
        switch (secondEnsureLogic()) {
            case 1:
                setStateWithFade(3);
                return;
            case 2:
                State.fadeInit(220, 102);
                State.fadeInit_Modify(192, 102);
                this.state = 1;
                GameObject.IsGamePause = true;
                return;
            default:
                return;
        }
    }

    private void pausetoSelectCharacterLogic() {
        switch (secondEnsureLogic()) {
            case 1:
                setStateWithFade(9);
                return;
            case 2:
                State.fadeInit(220, 102);
                State.fadeInit_Modify(192, 102);
                this.state = 1;
                GameObject.IsGamePause = true;
                return;
            default:
                return;
        }
    }

    private void pauseOptionSoundLogic() {
        switch (itemsSelect2Logic()) {
            case 1:
                GlobalResource.soundSwitchConfig = 1;
                State.fadeInit(102, 0);
                this.state = 7;
                SoundSystem.getInstance().setSoundState(GlobalResource.soundConfig);
                SoundSystem.getInstance().setSeState(GlobalResource.seConfig);
                this.returnCursor = 0;
                return;
            case 2:
                GlobalResource.soundSwitchConfig = 0;
                State.fadeInit(102, 0);
                this.state = 7;
                SoundSystem.getInstance().setSoundState(0);
                SoundSystem.getInstance().setSeState(0);
                this.returnCursor = 0;
                return;
            case 3:
                State.fadeInit(102, 0);
                this.state = 7;
                this.returnCursor = 0;
                return;
            default:
                return;
        }
    }

    private void pauseOptionVibrationLogic() {
        switch (itemsSelect2Logic()) {
            case 1:
                GlobalResource.vibrationConfig = 1;
                State.fadeInit(102, 0);
                this.state = 7;
                this.returnCursor = 0;
                MyAPI.vibrate();
                return;
            case 2:
                GlobalResource.vibrationConfig = 0;
                State.fadeInit(102, 0);
                this.state = 7;
                this.returnCursor = 0;
                return;
            case 3:
                State.fadeInit(102, 0);
                this.state = 7;
                this.returnCursor = 0;
                return;
            default:
                return;
        }
    }

    private void pauseOptionSpSetLogic() {
        switch (itemsSelect2Logic()) {
            case 1:
                GlobalResource.spsetConfig = 0;
                State.fadeInit(102, 0);
                this.state = 7;
                this.returnCursor = 0;
                return;
            case 2:
                GlobalResource.spsetConfig = 1;
                State.fadeInit(102, 0);
                this.state = 7;
                this.returnCursor = 0;
                return;
            case 3:
                State.fadeInit(102, 0);
                this.state = 7;
                this.returnCursor = 0;
                return;
            default:
                return;
        }
    }

    public static void enterSpStage(int ringNum, int chekPointID, int timeCount) {
        spReserveRingNum = ringNum;
        spCheckPointID = chekPointID;
        spTimeCount = timeCount;
        State.setState(6);
    }

    private void scoreUpdateInit() {
        Key.touchgamekeyClose();
        Key.touchkeygameboardClose();
        Key.touchkeyboardInit();
        Key.touchscoreupdatekeyInit();
        changeStateWithFade(43);
        if (muiAniDrawer == null) {
            muiAniDrawer = new Animation("/animation/mui").getDrawer(0, false, 0);
        }
        if (guiAnimation == null) {
            guiAnimation = new Animation("/animation/gui");
        }
        guiAniDrawer = guiAnimation.getDrawer(0, false, 0);
        this.returnCursor = 0;
        this.scoreUpdateCursor = 0;
    }

    private void scoreUpdateLogic() {
        if (State.fadeChangeOver()) {
            if (Key.touchscoreupdatereturn.Isin() && Key.touchscoreupdate.IsClick()) {
                this.returnCursor = 1;
            }
            if (Key.touchscoreupdateyes.Isin() && Key.touchscoreupdate.IsClick()) {
                this.scoreUpdateCursor = 1;
            } else if (Key.touchscoreupdateno.Isin() && Key.touchscoreupdate.IsClick()) {
                this.scoreUpdateCursor = 2;
            }
            if (Key.touchscoreupdateyes.IsButtonPress() && this.scoreUpdateCursor == 1) {
                this.state = 44;
                secondEnsureInit();
                State.fadeInit(0, GimmickObject.GIMMICK_NUM);
                SoundSystem.getInstance().playSe(1);
            } else if (Key.touchscoreupdateno.IsButtonPress() && this.scoreUpdateCursor == 2) {
                Standard2.splashinit(true);
                setStateWithFade(0);
                SoundSystem.getInstance().playSe(1);
            }
            if ((Key.press(524288) || (Key.touchscoreupdatereturn.IsButtonPress() && this.returnCursor == 1)) && State.fadeChangeOver()) {
                Standard2.splashinit(true);
                setStateWithFade(0);
                SoundSystem.getInstance().playSe(2);
            }
        }
    }

    private void scoreUpdateDraw(MFGraphics g) {
        int i;
        g.setColor(0);
        MyAPI.fillRect(g, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
        muiAniDrawer.setActionId(52);
        for (int i2 = 0; i2 < (SCREEN_WIDTH / 48) + 1; i2++) {
            for (int j = 0; j < (SCREEN_HEIGHT / 48) + 1; j++) {
                muiAniDrawer.draw(g, i2 * 48, j * 48);
            }
        }
        this.optionOffsetX -= 4;
        this.optionOffsetX %= 128;
        muiAniDrawer.setActionId(102);
        for (int x1 = this.optionOffsetX; x1 < SCREEN_WIDTH * 2; x1 += 128) {
            muiAniDrawer.draw(g, x1, 0);
        }
        guiAniDrawer.setActionId(17);
        guiAniDrawer.draw(g, SCREEN_WIDTH >> 1, (SCREEN_HEIGHT >> 1) - 37);
        PlayerObject.drawRecordTime(g, StageManager.getTimeModeScore(PlayerObject.getCharacterID()), (Def.SCREEN_WIDTH >> 1) + 54, (Def.SCREEN_HEIGHT >> 1) - 10, 2, 2);
        muiAniDrawer.setActionId((Key.touchscoreupdateyes.Isin() ? 1 : 0) + 55);
        muiAniDrawer.draw(g, (Def.SCREEN_WIDTH >> 1) - 60, (Def.SCREEN_HEIGHT >> 1) + 28);
        muiAniDrawer.setActionId(103);
        muiAniDrawer.draw(g, (Def.SCREEN_WIDTH >> 1) - 60, (Def.SCREEN_HEIGHT >> 1) + 28);
        AnimationDrawer animationDrawer = muiAniDrawer;
        if (Key.touchscoreupdateno.Isin()) {
            i = 1;
        } else {
            i = 0;
        }
        animationDrawer.setActionId(i + 55);
        muiAniDrawer.draw(g, (Def.SCREEN_WIDTH >> 1) + 60, (Def.SCREEN_HEIGHT >> 1) + 28);
        muiAniDrawer.setActionId(104);
        muiAniDrawer.draw(g, (Def.SCREEN_WIDTH >> 1) + 60, (Def.SCREEN_HEIGHT >> 1) + 28);
        animationDrawer = muiAniDrawer;
        if (Key.touchscoreupdatereturn.Isin()) {
            i = 5;
        } else {
            i = 0;
        }
        animationDrawer.setActionId(i + 61);
        muiAniDrawer.draw(g, 0, SCREEN_HEIGHT);
        State.drawFade(g);
    }

    private void scoreUpdateEnsureLogic() {
        switch (secondEnsureLogic()) {
            case 1:
                Message msg = new Message();
                setGameScore(StageManager.getTimeModeScore(PlayerObject.getCharacterID()));
                sendMessage(msg, 6);
                this.state = 45;
                return;
            case 2:
                this.state = 43;
                State.fadeInit(GimmickObject.GIMMICK_NUM, 0);
                State.setFadeOver();
                this.returnCursor = 0;
                this.scoreUpdateCursor = 0;
                return;
            default:
                return;
        }
    }

    private void scoreUpdateEnsureDraw(MFGraphics g) {
        scoreUpdateDraw(g);
        State.drawFade(g);
        SecondEnsurePanelDraw(g, 106);
    }

    private void scoreUpdatedLogic() {
        if (activity.isResumeFromOtherActivity) {
            Standard2.splashinit(true);
            setStateWithFade(0);
            activity.isResumeFromOtherActivity = false;
        }
    }

    private void scoreUpdatedDraw(MFGraphics g) {
    }
}
