package Lib;

import java.util.Random;

public class MyRandom {
    private static Random rnd;

    public static int nextInt(int min, int max) {
        checkInstance();
        return (Math.abs(rnd.nextInt()) % (Math.abs(max - min) + 1)) + Math.min(min, max);
    }

    public static int nextInt(int range) {
        checkInstance();
        return Math.abs(rnd.nextInt()) % range;
    }

    public static int nextInt() {
        checkInstance();
        return rnd.nextInt();
    }

    private static void checkInstance() {
        if (rnd == null) {
            rnd = new Random(System.currentTimeMillis());
        }
    }
}
