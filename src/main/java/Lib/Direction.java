package Lib;

public class Direction {
    public static final int DOWN = 1;
    public static final int LEFT = -1;
    public static final int NONE = 0;
    public static final int RIGHT = 1;
    public static final int UP = -1;
    private int directionX;
    private int directionY;

    public Direction(int x, int y) {
        this.directionX = x;
        this.directionY = y;
    }

    public Direction(int srcX, int srcY, int desX, int desY) {
        if (srcX > desX) {
            this.directionX = -1;
        } else if (srcX < desX) {
            this.directionX = 1;
        } else {
            this.directionX = 0;
        }
        if (srcY > desY) {
            this.directionY = -1;
        } else if (srcY < desY) {
            this.directionY = 1;
        } else {
            this.directionY = 0;
        }
    }

    public Direction getReverse() {
        return new Direction(-this.directionX, -this.directionY);
    }

    public int getValueX(int x) {
        return this.directionX * Math.abs(x);
    }

    public int getValueY(int y) {
        return this.directionY * Math.abs(y);
    }

    public boolean sameDirectionX(int v) {
        if (this.directionX * v < 0) {
            return false;
        }
        return true;
    }

    public boolean sameDirectionY(int v) {
        if (this.directionY * v < 0) {
            return false;
        }
        return true;
    }
}
