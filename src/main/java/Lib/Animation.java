package Lib;

import SonicGBA.GimmickObject;
import com.sega.mobile.framework.device.MFDevice;
import com.sega.mobile.framework.device.MFGamePad;
import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;

public class Animation {
    private static final String[] DOUBLE_ANIMATION_NAME = new String[]{"chr_sonic", "chr_tails_01", "chr_tails_02", "chr_knuckles_01", "chr_knuckles_02", "chr_amy_01", "chr_amy_02"};
    private static final boolean SEPERATE_PNG = false;
    private static final int TRANS_OFFSET = 1;
    private static Animation[] animationInstance;
    private static int[] imageIdArray = new int[10];
    public static boolean isFrameWanted;
    public static boolean isImageWanted;
    private static String tmpPath = "";
    protected String fileName;
    private ImageInfo[] imageInfo;
    private boolean isAnimationQi;
    public boolean isDoubleScale;
    private Action[] m_Actions;
    private int m_CurAni;
    private Frame[] m_Frames;
    private int m_OldAni;
    private int m_nActions;
    private int m_nFrames;
    private Animation[] qiAnimationArray;
    protected int refCount;

    class Action {
        private MFImage img_clip;
        protected Animation m_Ani;
        private short m_CurFrame = (short) 0;
        private byte[][] m_FrameInfo;
        private byte m_OldFrame = (byte) 0;
        private short m_Timer = (short) 0;
        private boolean m_bLoop = true;
        private boolean m_bPause;
        private short m_nFrames;

        public Action(Animation ani) {
            this.m_Ani = ani;
        }

        public void SetLoop(boolean loop) {
            this.m_bLoop = loop;
        }

        public boolean GetLoop() {
            return this.m_bLoop;
        }

        public byte[] GetARect() {
            if (this.m_nFrames == (short) 0) {
                return null;
            }
            int tmpFrame = this.m_FrameInfo[this.m_CurFrame][0];
            if (tmpFrame < 0) {
                tmpFrame += 256;
            }
            return Animation.this.m_Frames[tmpFrame].GetARect();
        }

        public byte[] GetCRect() {
            byte[] rect = null;
            try {
                int tmpFrame = this.m_FrameInfo[this.m_CurFrame][0];
                if (tmpFrame < 0) {
                    tmpFrame += 256;
                }
                rect = Animation.this.m_Frames[tmpFrame].GetCRect();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return rect;
        }

        public void JumpFrame(byte frame) {
            if (frame < this.m_nFrames) {
                this.m_Timer = (short) 0;
                this.m_CurFrame = frame;
            }
        }

        public short GetFrameNo() {
            return this.m_CurFrame;
        }

        public void LoadAction(InputStream in) {
            try {
                this.m_nFrames = (byte) in.read();
                this.m_FrameInfo = (byte[][]) Array.newInstance(Byte.TYPE, new int[]{this.m_nFrames, 2});
                for (short i = (short) 0; i < this.m_nFrames; i++) {
                    for (int j = 0; j < 2; j++) {
                        this.m_FrameInfo[i][j] = (byte) in.read();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void loadActionG2(DataInputStream ds) throws IOException {
            this.m_nFrames = ds.readByte();
            if (this.m_nFrames < (short) 0) {
                this.m_nFrames = (short) (this.m_nFrames + 256);
            }
            this.m_FrameInfo = (byte[][]) Array.newInstance(Byte.TYPE, new int[]{this.m_nFrames, 2});
            for (short i = (short) 0; i < this.m_nFrames; i++) {
                for (int j = 0; j < 2; j++) {
                    this.m_FrameInfo[i][j] = ds.readByte();
                }
                ds.readShort();
                ds.readShort();
            }
        }

        public void LoadAction() {
            this.m_nFrames = (short) 1;
            this.m_FrameInfo = (byte[][]) Array.newInstance(Byte.TYPE, new int[]{this.m_nFrames, 2});
            for (short i = (short) 0; i < this.m_nFrames; i++) {
                this.m_FrameInfo[i][0] = (byte) 0;
                this.m_FrameInfo[i][1] = 100;
            }
        }

        public boolean IsEnd() {
            if (this.m_bLoop) {
                return false;
            }
            if (this.m_Timer < this.m_FrameInfo[this.m_CurFrame][1] || this.m_CurFrame != this.m_nFrames - 1) {
                return false;
            }
            return true;
        }

        public void SetFrames(Frame[] frames) {
            Animation.this.m_Frames = frames;
        }

        public void SetPause(boolean pause) {
            this.m_bPause = pause;
        }

        public void Draw(MFGraphics g, int x, int y, short attr) {
            if (this.m_nFrames != (short) 0) {
                int tmpFrame = this.m_FrameInfo[this.m_CurFrame][0];
                if (tmpFrame < 0) {
                    tmpFrame += 256;
                }
                Animation.this.m_Frames[tmpFrame].Draw(g, x, y, attr);
                if (!this.m_bPause) {
                    this.m_Timer = (short) (this.m_Timer + 1);
                    if (this.m_Timer >= this.m_FrameInfo[this.m_CurFrame][1]) {
                        this.m_CurFrame = (short) (this.m_CurFrame + 1);
                        if (this.m_CurFrame < this.m_nFrames) {
                            this.m_Timer = (short) 0;
                        } else if (this.m_bLoop) {
                            this.m_Timer = (short) 0;
                            this.m_CurFrame = (short) 0;
                        } else {
                            this.m_CurFrame = (byte) (this.m_nFrames - 1);
                        }
                    }
                }
            }
        }

        public void Draw(MFGraphics g, short frame, int x, int y, short attr) {
            try {
                if (frame < this.m_nFrames) {
                    this.m_CurFrame = frame;
                    int tmpFrame = this.m_FrameInfo[frame][0];
                    if (tmpFrame < 0) {
                        tmpFrame += 256;
                    }
                    Animation.this.m_Frames[tmpFrame].Draw(g, x, y, attr);
                }
            } catch (Exception e) {
                System.out.println("frame:" + frame);
                System.out.println("m_FrameInfo.length:" + this.m_FrameInfo.length);
                System.out.println("m_FrameInfo[frame][0]:" + this.m_FrameInfo[frame][0]);
                System.out.println("m_Frames.length:" + Animation.this.m_Frames.length);
            }
        }

        public void SetFrame(short frame) {
            if (frame < this.m_nFrames) {
                this.m_CurFrame = frame;
            }
        }

        public int getFrameNum() {
            return this.m_nFrames;
        }

        public boolean isTimeOver(int time) {
            if (this.m_nFrames == (short) 0) {
                return true;
            }
            return time >= this.m_FrameInfo[this.m_CurFrame][1];
        }
    }

    private class Frame {
        private int color;
        private short frameHeight = (short) -1;
        private short frameWidth = (short) -1;
        private int[] functionID;
        private Animation m_Ani;
        private short[][] m_ClipInfo;
        private short m_img_cx;
        private short m_img_cy;
        private byte m_nClips;
        private byte[] rect1 = new byte[4];
        private byte[] rect2 = new byte[4];
        private byte[] tmp_rect1 = new byte[4];
        private byte[] tmp_rect2 = new byte[4];

        public Frame(Animation ani) {
            this.m_Ani = ani;
        }

        public byte[] GetARect() {
            if (this.rect1[0] == (byte) 0 && this.rect1[1] == (byte) 0 && this.rect1[2] == (byte) 1 && this.rect1[3] == (byte) 1) {
                return null;
            }
            for (int i = 0; i < 4; i++) {
                this.tmp_rect1[i] = this.rect1[i];
            }
            return this.tmp_rect1;
        }

        public byte[] GetCRect() {
            if (this.rect2[0] == (byte) 0 && this.rect2[1] == (byte) 0 && this.rect2[2] == (byte) 1 && this.rect2[3] == (byte) 1) {
                return null;
            }
            for (int i = 0; i < 4; i++) {
                this.tmp_rect2[i] = this.rect2[i];
            }
            return this.tmp_rect2;
        }

        public void LoadFrame(InputStream in) {
            if (in != null) {
                try {
                    in.read(this.rect1, 0, 4);
                    in.read(this.rect2, 0, 4);
                    this.m_nClips = (byte) in.read();
                    this.m_ClipInfo = (short[][]) Array.newInstance(Short.TYPE, new int[]{this.m_nClips, 5});
                    this.functionID = new int[this.m_nClips];
                    byte i = (byte) 0;
                    while (i < this.m_nClips) {
                        short[] sArr;
                        int j = 0;
                        while (j < 4) {
                            this.m_ClipInfo[i][j] = (byte) in.read();
                            if (j > 1 && this.m_ClipInfo[i][j] < (short) 0) {
                                sArr = this.m_ClipInfo[i];
                                sArr[j] = (short) (sArr[j] + 256);
                            }
                            j++;
                        }
                        this.m_ClipInfo[i][4] = (short) 0;
                        int tmp_attr = (short) (this.m_ClipInfo[i][3] << 8);
                        if (tmp_attr == 8192 || tmp_attr == 28672) {
                            sArr = this.m_ClipInfo[i];
                            sArr[0] = (short) (sArr[0] - 1);
                        } else if (tmp_attr == 16384 || tmp_attr == 4096) {
                            sArr = this.m_ClipInfo[i];
                            sArr[1] = (short) (sArr[1] - 1);
                        } else if (tmp_attr == 24576) {
                            sArr = this.m_ClipInfo[i];
                            sArr[0] = (short) (sArr[0] - 1);
                            sArr = this.m_ClipInfo[i];
                            sArr[1] = (short) (sArr[1] - 1);
                        } else if (tmp_attr == 12288 || tmp_attr == 12288) {
                            sArr = this.m_ClipInfo[i];
                            sArr[0] = (short) (sArr[0] - 1);
                            sArr = this.m_ClipInfo[i];
                            sArr[1] = (short) (sArr[1] - 1);
                        }
                        if (Animation.this.isDoubleScale) {
                            this.m_ClipInfo[i][0] = (short) (this.m_ClipInfo[i][0] << 1);
                            this.m_ClipInfo[i][1] = (short) (this.m_ClipInfo[i][1] << 1);
                        } else {
                            this.m_ClipInfo[i][0] = this.m_ClipInfo[i][0];
                            this.m_ClipInfo[i][1] = this.m_ClipInfo[i][1];
                        }
                        i++;
                    }
                    return;
                } catch (Exception e) {
                    e.printStackTrace();
                    return;
                }
            }
            this.m_nClips = (byte) 1;
            this.m_ClipInfo = (short[][]) Array.newInstance(Short.TYPE, new int[]{1, 4});
            this.m_ClipInfo[0][0] = (short) 0;
            this.m_ClipInfo[0][1] = (short) 0;
            this.m_ClipInfo[0][2] = (short) 0;
            this.m_ClipInfo[0][3] = (short) 0;
        }

        public void loadFrameG2(DataInputStream ds) throws IOException {
            this.m_nClips = ds.readByte();
            this.m_ClipInfo = (short[][]) Array.newInstance(Short.TYPE, new int[]{this.m_nClips, 5});
            this.functionID = new int[this.m_nClips];
            for (byte i = (byte) 0; i < this.m_nClips; i++) {
                this.functionID[i] = ds.readByte();
                short[] sArr;
                switch (this.functionID[i]) {
                    case 0:
                        this.m_ClipInfo[i][4] = ds.readByte();
                        this.m_ClipInfo[i][2] = ds.readByte();
                        this.m_ClipInfo[i][3] = ds.readByte();
                        this.m_ClipInfo[i][3] = (short) (this.m_ClipInfo[i][3] << 4);
                        if (this.m_ClipInfo[i][4] < (short) 0) {
                            sArr = this.m_ClipInfo[i];
                            sArr[4] = (short) (sArr[4] + 256);
                        }
                        if (this.m_ClipInfo[i][2] < (short) 0) {
                            sArr = this.m_ClipInfo[i];
                            sArr[2] = (short) (sArr[2] + 256);
                        }
                        this.m_ClipInfo[i][0] = ds.readShort();
                        this.m_ClipInfo[i][1] = ds.readShort();
                        if (!Animation.isFrameWanted) {
                            break;
                        }
                        int j = 0;
                        while (j < Animation.imageIdArray.length) {
                            if (Animation.imageIdArray[j] != -1) {
                                if (Animation.imageIdArray[j] == this.m_ClipInfo[i][4]) {
                                    break;
                                }
                                j++;
                            } else {
                                Animation.imageIdArray[j] = this.m_ClipInfo[i][4];
                                break;
                            }
                        }
                        break;
                    case 1:
                        this.m_ClipInfo[i][2] = ds.readShort();
                        this.m_ClipInfo[i][3] = ds.readShort();
                        this.color = 0;
                        this.color |= (ds.readByte() << 16) & 0xFF0000;
                        this.color |= (ds.readByte() << 8) & 0xFF00;
                        this.color |= (ds.readByte() << 0) & 0xFF;
                        this.m_ClipInfo[i][0] = ds.readShort();
                        this.m_ClipInfo[i][1] = ds.readShort();
                        break;
                    case 2:
                        this.m_ClipInfo[i][2] = ds.readShort();
                        this.m_ClipInfo[i][3] = ds.readShort();
                        this.color = 0;
                        this.color |= (ds.readByte() << 16) & 0xFF0000;
                        this.color |= (ds.readByte() << 8) & 0xFF00;
                        this.color |= (ds.readByte() << 0) & 0xFF;
                        this.m_ClipInfo[i][0] = ds.readShort();
                        this.m_ClipInfo[i][1] = ds.readShort();
                        break;
                    case 6:
                        this.m_ClipInfo[i][2] = ds.readByte();
                        this.m_ClipInfo[i][3] = ds.readByte();
                        if (this.m_ClipInfo[i][3] < (short) 0) {
                            sArr = this.m_ClipInfo[i];
                            sArr[3] = (short) (sArr[3] + 256);
                        }
                        this.m_ClipInfo[i][0] = ds.readShort();
                        this.m_ClipInfo[i][1] = ds.readShort();
                        break;
                    default:
                        break;
                }
            }
        }

        public int getWidth() {
            if (this.m_nClips <= (byte) 0) {
                return 0;
            }
            int xLeft = Integer.MAX_VALUE;
            int xRight = Integer.MIN_VALUE;
            for (byte i = (byte) 0; i < this.m_nClips; i++) {
                if (this.m_ClipInfo[i][0] < xLeft) {
                    xLeft = this.m_ClipInfo[i][0];
                }
                short[][] clipArray = Animation.this.imageInfo[this.m_ClipInfo[i][4]].getClips();
                int widthId = 2;
                if ((((short) (this.m_ClipInfo[i][3] << 8)) & MFGamePad.KEY_NUM_6) != 0) {
                    widthId = 3;
                }
                if (clipArray[this.m_ClipInfo[i][2]][widthId] + this.m_ClipInfo[i][0] > xRight) {
                    xRight = clipArray[this.m_ClipInfo[i][2]][widthId] + this.m_ClipInfo[i][0];
                }
            }
            this.frameWidth = (short) Math.abs(xRight - xLeft);
            if (Animation.this.isDoubleScale) {
                this.frameWidth = (short) (this.frameWidth << 1);
            }
            return this.frameWidth;
        }

        public int getHeight() {
            if (this.m_nClips <= (byte) 0) {
                return 0;
            }
            int yTop = Integer.MAX_VALUE;
            int yBottom = Integer.MIN_VALUE;
            for (byte i = (byte) 0; i < this.m_nClips; i++) {
                if (this.m_ClipInfo[i][1] < yTop) {
                    yTop = this.m_ClipInfo[i][1];
                }
                short[][] clipArray = Animation.this.imageInfo[this.m_ClipInfo[i][4]].getClips();
                int heightId = 3;
                if ((((short) (this.m_ClipInfo[i][3] << 8)) & MFGamePad.KEY_NUM_6) != 0) {
                    heightId = 2;
                }
                if (clipArray[this.m_ClipInfo[i][2]][heightId] + this.m_ClipInfo[i][1] > yBottom) {
                    yBottom = clipArray[this.m_ClipInfo[i][2]][heightId] + this.m_ClipInfo[i][1];
                }
            }
            this.frameHeight = (short) Math.abs(yBottom - yTop);
            if (Animation.this.isDoubleScale) {
                this.frameHeight = (short) (this.frameHeight << 1);
            }
            return this.frameHeight;
        }

        public void SetClips(short[][] clip) {
        }

        public void Draw(MFGraphics g, int x, int y, short attr) {
            if (this.m_nClips != (byte) 0) {
                for (byte i = (byte) 0; i < this.m_nClips; i++) {
                    switch (this.functionID[i]) {
                        case 0:
                            if (!Animation.this.isDoubleScale) {
                                DrawImage(g, i, x, y, attr);
                                break;
                            }
                            g.saveCanvas();
                            g.translateCanvas(x, y);
                            g.scaleCanvas(0.5f, 0.5f);
                            DrawImage(g, i, 0, 0, attr);
                            g.restoreCanvas();
                            break;
                        case 1:
                            fillRect(g, i, x, y, attr);
                            break;
                        case 2:
                            drawRect(g, i, x, y, attr);
                            break;
                        case 6:
                            try {
                                int tmp_x = this.m_ClipInfo[i][0];
                                int tmp_y = this.m_ClipInfo[i][1];
                                if (!Animation.this.isDoubleScale) {
                                    Animation.this.qiAnimationArray[this.m_ClipInfo[i][2]].m_Frames[this.m_ClipInfo[i][3]].Draw(g, x + tmp_x, y + tmp_y, attr);
                                    break;
                                }
                                g.saveCanvas();
                                g.translateCanvas(x + tmp_x, y + tmp_y);
                                g.scaleCanvas(0.5f, 0.5f);
                                Animation.this.qiAnimationArray[this.m_ClipInfo[i][2]].m_Frames[this.m_ClipInfo[i][3]].Draw(g, 0, 0, attr);
                                g.restoreCanvas();
                                break;
                            } catch (Exception e) {
                                System.out.println(new StringBuilder(String.valueOf(this.m_ClipInfo[i][3])).append("is out of bounds").toString());
                                break;
                            }
                        default:
                            break;
                    }
                }
            }
        }

        public void DrawImage(MFGraphics g, int i, int x, int y, short attr) {
            if (this.m_nClips != (byte) 0) {
                MFImage image = Animation.this.imageInfo[this.m_ClipInfo[i][4]].getImage();
                short[] m_Clips_2 = Animation.this.imageInfo[this.m_ClipInfo[i][4]].getClips()[this.m_ClipInfo[i][2]];
                short draw_attr = attr;
                int tmp_attr = (short) (this.m_ClipInfo[i][3] << 8);
                int tmp_x = this.m_ClipInfo[i][0];
                int tmp_y = this.m_ClipInfo[i][1];
                int tmp;
                switch (getMIDPTransId(attr)) {
                    case 1:
                        tmp_y = (-tmp_y) - m_Clips_2[3];
                        break;
                    case 2:
                        tmp_x = (-tmp_x) - m_Clips_2[2];
                        break;
                    case 3:
                        tmp_x = (-tmp_x) - m_Clips_2[2];
                        tmp_y = (-tmp_y) - m_Clips_2[3];
                        break;
                    case 4:
                        tmp = tmp_x;
                        tmp_x = tmp_y;
                        tmp_y = tmp;
                        break;
                    case 5:
                        tmp = tmp_x;
                        tmp_x = (-tmp_y) - m_Clips_2[3];
                        tmp_y = tmp;
                        break;
                    case 6:
                        tmp = tmp_x;
                        tmp_x = tmp_y;
                        tmp_y = (-tmp) - m_Clips_2[2];
                        break;
                    case 7:
                        tmp = tmp_x;
                        tmp_x = (-tmp_y) - m_Clips_2[3];
                        tmp_y = (-tmp) - m_Clips_2[2];
                        break;
                }
                int original_attr = tmp_attr;
                tmp_attr ^= draw_attr;
                if ((tmp_attr & MFGamePad.KEY_NUM_6) != 0) {
                    if ((((original_attr & 8192) != 0 ? 1 : 0) ^ ((original_attr & MFGamePad.KEY_NUM_8) != 0 ? 1 : 0)) != 0) {
                        tmp_attr ^= 24576;
                    }
                } else if (!((original_attr & MFGamePad.KEY_NUM_6) == 0 || (draw_attr & MFGamePad.KEY_NUM_6) == 0)) {
                    tmp_attr ^= 24576;
                }
                Const.DrawImage(g, tmp_x + x, tmp_y + y, image, m_Clips_2[0], m_Clips_2[1], m_Clips_2[2], m_Clips_2[3], tmp_attr);
            }
        }

        private void fillRect(MFGraphics g, int i, int iX, int iY, int attr) {
            int x = this.m_ClipInfo[i][0];
            int y = this.m_ClipInfo[i][1];
            if ((attr & 8192) > 0) {
                x = ((-x) - this.m_ClipInfo[i][2]) + 1;
            }
            if ((attr & MFGamePad.KEY_NUM_8) > 0) {
                y = ((-y) - this.m_ClipInfo[i][3]) + 1;
            }
            int colorBack = g.getColor();
            g.setColor(this.color);
            g.fillRect(iX + x, iY + y, this.m_ClipInfo[i][2], this.m_ClipInfo[i][3]);
            g.setColor(colorBack);
        }

        private void drawRect(MFGraphics g, int i, int iX, int iY, int attr) {
            int x = this.m_ClipInfo[i][0];
            int y = this.m_ClipInfo[i][1];
            if ((attr & 8192) > 0) {
                x = ((-x) - this.m_ClipInfo[i][2]) + 1;
            }
            if ((attr & MFGamePad.KEY_NUM_8) > 0) {
                y = ((-y) - this.m_ClipInfo[i][3]) + 1;
            }
            int colorBack = g.getColor();
            g.setColor(this.color);
            g.fillRect(iX + x, iY + y, this.m_ClipInfo[i][2], this.m_ClipInfo[i][3]);
            g.setColor(colorBack);
        }

        private int getMIDPTransId(int tmp_attr) {
            int attr = 0;
            if ((tmp_attr & MFGamePad.KEY_NUM_6) != 0) {
                attr = 0 | 6;
                if ((tmp_attr & 8192) != 0) {
                    attr ^= 1;
                }
                if ((tmp_attr & MFGamePad.KEY_NUM_8) != 0) {
                    return attr ^ 2;
                }
                return attr;
            }
            if ((tmp_attr & 8192) != 0) {
                attr = 0 | 2;
            }
            if ((tmp_attr & MFGamePad.KEY_NUM_8) != 0) {
                return attr | 1;
            }
            return attr;
        }
    }

    static class ImageInfo {
        private MFImage[] imageSeperate;
        protected MFImage img_clip;
        private short[][] m_Clips;
        private short m_nClips;

        private ImageInfo() {
        }

        private ImageInfo(MFImage image) {
            this.img_clip = image;
        }

        private ImageInfo(String imageFileName) {
            try {
                this.img_clip = MFImage.createImage(imageFileName);
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }

        public void close() {
            this.img_clip = null;
            if (this.imageSeperate != null) {
                for (int i = 0; i < this.imageSeperate.length; i++) {
                    this.imageSeperate[i] = null;
                }
            }
            this.imageSeperate = null;
        }

        public void loadInfo(DataInputStream ds) throws IOException {
            this.m_nClips = ds.readByte();
            if (this.m_nClips < (short) 0) {
                this.m_nClips = (short) (this.m_nClips + 256);
            }
            this.m_Clips = (short[][]) Array.newInstance(Short.TYPE, new int[]{this.m_nClips, 4});
            for (short i = (short) 0; i < this.m_nClips; i++) {
                for (int j = 0; j < 4; j++) {
                    this.m_Clips[i][j] = ds.readShort();
                }
            }
            String fileName = ds.readUTF();
            if (Animation.isImageWanted) {
                String tmpFileName = MyAPI.getFileName(fileName);
                this.img_clip = MFImage.createImage(Animation.tmpPath + tmpFileName);
                System.out.println("image fileName:" + tmpFileName);
            }
        }

        public void loadInfo(InputStream ds) throws IOException {
            short i;
            if (ds == null) {
                this.m_nClips = (short) 1;
                this.m_Clips = (short[][]) Array.newInstance(Short.TYPE, new int[]{this.m_nClips, 4});
                for (i = (short) 0; i < this.m_nClips; i++) {
                    this.m_Clips[i][0] = (short) 0;
                    this.m_Clips[i][1] = (short) 0;
                    this.m_Clips[i][2] = (short) (this.img_clip.getWidth() & 0xFFFF);
                    this.m_Clips[i][3] = (short) (this.img_clip.getHeight() & 0xFFFF);
                }
                return;
            }
            this.m_nClips = (byte) ds.read();
            if (this.m_nClips < (short) 0) {
                this.m_nClips = (short) (this.m_nClips + 256);
            }
            this.m_Clips = (short[][]) Array.newInstance(Short.TYPE, new int[]{this.m_nClips, 4});
            for (i = (short) 0; i < this.m_nClips; i++) {
                for (int j = 0; j < 4; j++) {
                    this.m_Clips[i][j] = Const.ReadShort(ds);
                }
            }
        }

        public MFImage getImage() {
            return this.img_clip;
        }

        public short[][] getClips() {
            return this.m_Clips;
        }

        public void doubleParam() {
            for (short i = (short) 0; i < this.m_nClips; i++) {
                for (int j = 0; j < 4; j++) {
                    this.m_Clips[i][j] = (short) (this.m_Clips[i][j] << 1);
                }
            }
        }

        public void separateImage() {
            this.imageSeperate = new MFImage[this.m_nClips];
            for (short i = (short) 0; i < this.m_nClips; i++) {
                this.imageSeperate[i] = MFImage.createImage(this.img_clip, this.m_Clips[i][0], this.m_Clips[i][1], this.m_Clips[i][2], this.m_Clips[i][3], 0);
                this.m_Clips[i][0] = (short) 0;
                this.m_Clips[i][1] = (short) 0;
            }
            this.img_clip = null;
        }

        public MFImage getSeparateImage(int id) {
            return this.imageSeperate[id];
        }
    }

    private Animation() {
        this.isDoubleScale = false;
        this.fileName = null;
        this.isAnimationQi = false;
        this.m_CurAni = 0;
        this.m_OldAni = 0;
        this.refCount = 0;
    }

    public Animation(String fileName) {
        this.isDoubleScale = false;
        this.fileName = null;
        this.isAnimationQi = false;
        for (String endsWith : DOUBLE_ANIMATION_NAME) {
            if (fileName.endsWith(endsWith)) {
                this.isDoubleScale = true;
                break;
            }
        }
        this.m_CurAni = 0;
        this.m_OldAni = 0;
        this.fileName = fileName;
        SetClipImg(new StringBuilder(String.valueOf(fileName)).append(".png").toString());
        LoadAnimation(new StringBuilder(String.valueOf(fileName)).append(".dat").toString());
    }

    public Animation(MFImage image, String fileName) {
        this.isDoubleScale = false;
        this.fileName = null;
        this.isAnimationQi = false;
        for (String endsWith : DOUBLE_ANIMATION_NAME) {
            if (fileName.endsWith(endsWith)) {
                this.isDoubleScale = true;
                break;
            }
        }
        this.m_CurAni = 0;
        this.m_OldAni = 0;
        this.fileName = fileName;
        this.imageInfo = new ImageInfo[1];
        this.imageInfo[0] = new ImageInfo(image);
        LoadAnimation(new StringBuilder(String.valueOf(fileName)).append(".dat").toString());
    }

    protected void close() {
        if (this.imageInfo != null) {
            for (int i = 0; i < this.imageInfo.length; i++) {
                if (this.imageInfo[i] != null) {
                    this.imageInfo[i].close();
                }
                this.imageInfo[i] = null;
            }
        }
        this.imageInfo = null;
    }

    public void setImage(MFImage image, int id) {
        this.imageInfo[id].img_clip = image;
    }

    public void SetCurAni(int no) {
        if (no >= this.m_nActions) {
            no = 0;
        }
        this.m_CurAni = no;
        if (this.m_OldAni != this.m_CurAni) {
            this.m_OldAni = this.m_CurAni;
            this.m_Actions[this.m_CurAni].JumpFrame((byte) 0);
            this.m_Actions[this.m_CurAni].SetPause(false);
        }
    }

    public void SetCurFrame(short frameId) {
        this.m_Actions[this.m_CurAni].SetFrame(frameId);
    }

    public int GetCurAni() {
        return this.m_CurAni;
    }

    public void JumpFrame(byte no) {
        this.m_Actions[this.m_CurAni].JumpFrame(no);
    }

    public short GetFrameNo() {
        return this.m_Actions[this.m_CurAni].GetFrameNo();
    }

    public byte[] GetARect() {
        return this.m_Actions[this.m_CurAni].GetARect();
    }

    public byte[] GetCRect() {
        return this.m_Actions[this.m_CurAni].GetCRect();
    }

    public boolean IsEnd() {
        return this.m_Actions[this.m_CurAni].IsEnd();
    }

    public void SetLoop(boolean loop) {
        this.m_Actions[this.m_CurAni].SetLoop(loop);
    }

    public void SetPause(boolean pause) {
        this.m_Actions[this.m_CurAni].SetPause(pause);
    }

    public void SetClipImg(String fn) {
        try {
            this.imageInfo = new ImageInfo[1];
            this.imageInfo[0] = new ImageInfo(fn);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void SetClipImg(MFImage img) {
        this.imageInfo = new ImageInfo[1];
        this.imageInfo[0] = new ImageInfo(img);
    }

    public void LoadAnimation(String fileName) {
        InputStream in = MFDevice.getResourceAsStream(fileName);
        LoadAnimation(in);
        if (in != null) {
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void LoadAnimation(InputStream in) {
        int i;
        if (in != null) {
            try {
                this.imageInfo[0].loadInfo(in);
                this.m_nFrames = (byte) in.read();
                if (this.m_nFrames < 0) {
                    this.m_nFrames += 256;
                }
                this.m_Frames = new Frame[this.m_nFrames];
                for (i = 0; i < this.m_nFrames; i++) {
                    this.m_Frames[i] = new Frame(this);
                    this.m_Frames[i].LoadFrame(in);
                    this.m_Frames[i].SetClips(this.imageInfo[0].m_Clips);
                }
                this.m_nActions = (byte) in.read();
                if (this.m_nActions < 0) {
                    this.m_nActions += 256;
                }
                this.m_Actions = new Action[this.m_nActions];
                for (i = 0; i < this.m_nActions; i++) {
                    this.m_Actions[i] = new Action(this);
                    this.m_Actions[i].LoadAction(in);
                    this.m_Actions[i].SetFrames(this.m_Frames);
                }
                if (this.isDoubleScale) {
                    this.imageInfo[0].doubleParam();
                    return;
                }
                return;
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }
        }
        try {
            this.m_nFrames = 1;
            this.m_Frames = new Frame[this.m_nFrames];
            for (i = 0; i < this.m_nFrames; i++) {
                this.m_Frames[i] = new Frame(this);
                this.m_Frames[i].LoadFrame(in);
                this.m_Frames[i].SetClips(this.imageInfo[0].m_Clips);
            }
            this.m_nActions = 1;
            this.m_Actions = new Action[this.m_nActions];
            for (i = 0; i < this.m_nActions; i++) {
                this.m_Actions[i] = new Action(this);
                this.m_Actions[i].LoadAction();
                this.m_Actions[i].SetFrames(this.m_Frames);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private void LoadAnimationG2(DataInputStream ds) {
        this.isAnimationQi = true;
        try {
            int i;
            ds.readInt();
            this.m_nFrames = ds.readByte() & 0xFF;
            if (this.m_nFrames < 0) {
                this.m_nFrames += 256;
            }
            this.m_Frames = new Frame[this.m_nFrames];
            for (i = 0; i < this.m_nFrames; i++) {
                int k;
                int l;
                this.m_Frames[i] = new Frame(this);
                this.m_Frames[i].loadFrameG2(ds);
                int rectarrays = ds.readByte() & 0xFF;
                for (k = 0; k < rectarrays; k++) {
                    int rectsNum = ds.readByte() & 0xFF;
                    for (l = 0; l < rectsNum; l++) {
                        ds.readShort();
                        ds.readShort();
                        ds.readShort();
                        ds.readShort();
                    }
                }
                int crossarrays = ds.readByte() & 0xFF;
                for (k = 0; k < crossarrays; k++) {
                    int crossesNum = ds.readByte() & 0xFF;
                    for (l = 0; l < crossesNum; l++) {
                        ds.readShort();
                        ds.readShort();
                    }
                }
            }
            this.m_nActions = ds.readByte()& 0xFF;
            this.m_Actions = new Action[this.m_nActions];
            for (i = 0; i < this.m_nActions; i++) {
                this.m_Actions[i] = new Action(this);
                this.m_Actions[i].loadActionG2(ds);
                this.m_Actions[i].SetFrames(this.m_Frames);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Animation[] getInstanceFromQi(String fileName) {
        tmpPath = MyAPI.getPath(fileName);
        isFrameWanted = false;
        DataInputStream ds = null;
        try {
            ds = new DataInputStream(MFDevice.getResourceAsStream(fileName));
            try {
                int i;
                int animationNum = ds.readByte() & 0xFF;
                animationInstance = new Animation[animationNum];
                for (i = 0; i < animationNum; i++) {
                    animationInstance[i] = new Animation();
                    animationInstance[i].LoadAnimationG2(ds);
                }
                int imageNum = ds.readByte() & 0xFF;
                ImageInfo[] imageInfo = new ImageInfo[imageNum];
                for (i = 0; i < imageNum; i++) {
                    isImageWanted = true;
                    imageInfo[i] = new ImageInfo();
                    imageInfo[i].loadInfo(ds);
                }
                for (i = 0; i < animationNum; i++) {
                    animationInstance[i].imageInfo = imageInfo;
                    animationInstance[i].qiAnimationArray = animationInstance;
                }
                imageInfo = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (ds != null) {
            try {
                ds.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return animationInstance;
    }

    public static Animation[] getInstanceFromQi(String fileName, boolean[] index) {
        int i;
        Exception e;
        Throwable th;
        for (i = 0; i < imageIdArray.length; i++) {
            imageIdArray[i] = -1;
        }
        DataInputStream dataInputStream = null;
        try {
            DataInputStream ds = new DataInputStream(MFDevice.getResourceAsStream(fileName));
            try {
                int animationNum = ds.readByte();
                animationInstance = new Animation[animationNum];
                for (i = 0; i < animationNum; i++) {
                    isFrameWanted = index[i];
                    animationInstance[i] = new Animation();
                    animationInstance[i].LoadAnimationG2(ds);
                }
                int imageNum = ds.readByte();
                ImageInfo[] imageInfo = new ImageInfo[imageNum];
                for (i = 0; i < imageNum; i++) {
                    isImageWanted = false;
                    int j = 0;
                    while (j < imageIdArray.length) {
                        if (imageIdArray[j] != i) {
                            if (imageIdArray[j] == -1) {
                                break;
                            }
                            j++;
                        } else {
                            isImageWanted = true;
                            break;
                        }
                    }
                    imageInfo[i] = new ImageInfo();
                    imageInfo[i].loadInfo(ds);
                }
                for (i = 0; i < animationNum; i++) {
                    animationInstance[i].imageInfo = imageInfo;
                    animationInstance[i].qiAnimationArray = animationInstance;
                }
                imageInfo = null;
                if (ds != null) {
                    try {
                        ds.close();
                        dataInputStream = ds;
                    } catch (IOException e2) {
                        e2.printStackTrace();
                    }
                    return animationInstance;
                }
                dataInputStream = ds;
            } catch (Exception e3) {
                e = e3;
                dataInputStream = ds;
            } catch (Throwable th2) {
                th = th2;
                dataInputStream = ds;
            }
        } catch (Exception e32) {
            e = e32;
            try {
                e.printStackTrace();
                if (dataInputStream != null) {
                    try {
                        dataInputStream.close();
                    } catch (IOException e22) {
                        e22.printStackTrace();
                    }
                }
                return animationInstance;
            } catch (Throwable th3) {
                th = th3;
                if (dataInputStream != null) {
                    try {
                        dataInputStream.close();
                    } catch (IOException e222) {
                        e222.printStackTrace();
                    }
                }
                //throw th; // TODO ?
            }
        }
        return animationInstance;
    }

    public void DrawAni(MFGraphics g, int x, int y, short attr) {
        this.m_Actions[this.m_CurAni].Draw(g, x, y, attr);
    }

    public void DrawAni(MFGraphics g, short no, int x, int y, boolean loop, short attr) {
        SetCurAni(no);
        SetLoop(loop);
        DrawAni(g, x, y, attr);
    }

    public void DrawAni(MFGraphics g, byte frame, int x, int y, short attr) {
        this.m_Actions[this.m_CurAni].Draw(g, frame, x, y, attr);
    }

    public void DrawAni(MFGraphics g, short ani, short frame, int x, int y, short attr) {
        this.m_Actions[ani].Draw(g, frame, x, y, attr);
    }

    public AnimationDrawer getDrawer(int actionId, boolean loop, int trans) {
        this.refCount++;
        return new AnimationDrawer(this, actionId, loop, trans);
    }

    public AnimationDrawer getDrawer() {
        this.refCount++;
        return new AnimationDrawer(this);
    }

    public int getActionNum() {
        return this.m_nActions;
    }

    public int getFrameNum() {
        return this.m_Actions[this.m_CurAni].getFrameNum();
    }

    public int getFrameNum(int actionId) {
        return this.m_Actions[actionId].getFrameNum();
    }

    public boolean isTimeOver(int time) {
        return this.m_Actions[this.m_CurAni].isTimeOver(time);
    }

    public boolean isTimeOver(int actionId, int frameId, int time) {
        return this.m_Actions[this.m_CurAni].isTimeOver(time);
    }

    public int getWidthWithFrameId(int actionId, int currentFrame) {
        return this.m_Frames[this.m_Actions[actionId].m_FrameInfo[currentFrame][0]].getWidth();
    }

    public int getHeightWithFrameId(int actionId, int currentFrame) {
        return this.m_Frames[this.m_Actions[actionId].m_FrameInfo[currentFrame][0]].getHeight();
    }

    public static void closeAnimation(Animation animation) {
        if (animation != null) {
            animation.close();
        }
    }

    public static void closeAnimationArray(Animation[] animation) {
        if (animation != null) {
            for (int i = 0; i < animation.length; i++) {
                closeAnimation(animation[i]);
                animation[i] = null;
            }
        }
    }

    public static void closeAnimationDrawer(AnimationDrawer drawer) {
        if (drawer != null) {
            drawer.close();
        }
    }

    public static void closeAnimationDrawerArray(AnimationDrawer[] drawer) {
        if (drawer != null) {
            for (int i = 0; i < drawer.length; i++) {
                closeAnimationDrawer(drawer[i]);
                drawer[i] = null;
            }
        }
        drawer = (AnimationDrawer[]) null;
    }
}
