package Lib;

import com.sega.mobile.framework.device.MFGamePad;
import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;
import java.io.InputStream;

public class Const {
    public static final short FLIP_X = (short) 8192;
    public static final short FLIP_Y = (short) 16384;
    public static final short ROTATE_180 = (short) 24576;
    public static final short ROTATE_270 = (short) 4096;
    public static final short[] TRANS;

    static {
        short[] sArr = new short[8];
        sArr[1] = FLIP_Y;
        sArr[2] = FLIP_X;
        sArr[3] = ROTATE_180;
        sArr[4] = (short) 20480;
        sArr[5] = (short) 28672;
        sArr[6] = ROTATE_270;
        sArr[7] = (short) 12288;
        TRANS = sArr;
    }

    public static short ReadShort(InputStream in) {
        short[] tmp = new short[2];
        try {
            tmp[0] = (short) in.read();
            tmp[1] = (short) in.read();
            return (short) ((tmp[1] << 8) + tmp[0]);
        } catch (Exception e) {
            e.printStackTrace();
            return (short) 0;
        }
    }

    public static int ReadInt(InputStream in) {
        int rt = 0;
        int[] tmp = new int[4];
        int i = 0;
        while (i < 4) {
            try {
                tmp[i] = (short) in.read();
                rt += tmp[i] << (i * 8);
                i++;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return rt;
    }

    public static void DrawImage(MFGraphics g, MFImage img, int dx, int dy, int anchor, int tmp_attr) {
        int cx = img.getWidth();
        int cy = img.getHeight();
        int attr = 0;
        if ((tmp_attr & MFGamePad.KEY_NUM_6) != 0) {
            attr = 0 | 6;
            if ((tmp_attr & 8192) != 0) {
                attr ^= 1;
            }
            if ((tmp_attr & MFGamePad.KEY_NUM_8) != 0) {
                attr ^= 2;
            }
        } else {
            if ((tmp_attr & 8192) != 0) {
                attr = 0 | 2;
            }
            if ((tmp_attr & MFGamePad.KEY_NUM_8) != 0) {
                attr |= 1;
            }
        }
        try {
            MyAPI.drawImageWithoutZoom(g, img, 0, 0, cx, cy, attr, dx, dy, anchor);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void DrawImage(MFGraphics g, int dx, int dy, MFImage img, int sx, int sy, int cx, int cy, int tmp_attr) {
        if (img != null) {
            int img_cx = img.getWidth();
            int img_cy = img.getHeight();
            if (sx < img_cx && sy < img_cy) {
                int attr = 0;
                if ((tmp_attr & MFGamePad.KEY_NUM_6) != 0) {
                    attr = 0 | 6;
                    if ((tmp_attr & 8192) != 0) {
                        attr ^= 1;
                    }
                    if ((tmp_attr & MFGamePad.KEY_NUM_8) != 0) {
                        attr ^= 2;
                    }
                } else {
                    if ((tmp_attr & 8192) != 0) {
                        attr = 0 | 2;
                    }
                    if ((tmp_attr & MFGamePad.KEY_NUM_8) != 0) {
                        attr |= 1;
                    }
                }
                if (sx + cx > img_cx) {
                    cx = img_cx - sx;
                }
                if (sy + cy > img_cy) {
                    cy = img_cy - sy;
                }
                MyAPI.drawImageWithoutZoom(g, img, sx, sy, cx, cy, attr, dx, dy, 0);
            }
        }
    }

    boolean RectIntersect(int[] rect1, int[] rect2) {
        if (rect2[0] >= rect1[0] + rect1[2] || rect2[0] + rect2[2] <= rect1[0] || rect2[1] >= rect1[1] + rect1[3] || rect2[1] + rect2[3] <= rect1[1]) {
            return false;
        }
        return true;
    }
}
