package Lib;

import com.sega.mobile.framework.device.MFDevice;
import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;

public class AnimationData {
    public static final byte ANI_SIZE = (byte) 2;
    private static final boolean DEBUG = false;
    public static final byte FLIP_X = (byte) 2;
    public static final byte FLIP_Y = (byte) 4;
    public static final byte ROTATE_90 = (byte) 1;
    private byte[] TRANMODIF;
    Drawer drawer;
    MFImage[] images;
    Object[][] imagesmanager;
    private boolean isLoad;
    Object[][] object;
    String[][] stringmanager;

    public AnimationData() {
        byte[] bArr = new byte[8];
        bArr[1] = (byte) 6;
        bArr[2] = (byte) 2;
        bArr[3] = (byte) 4;
        bArr[4] = (byte) 1;
        bArr[5] = (byte) 7;
        bArr[6] = (byte) 3;
        bArr[7] = (byte) 5;
        this.TRANMODIF = bArr;
    }

    public void close() {
        this.object = null;
        this.imagesmanager = null;
        this.images = null;
        this.stringmanager = null;
        this.drawer = null;
        System.gc();
    }

    public boolean isLoad() {
        return this.isLoad;
    }

    public void newObjectArray(int num) {
        this.object = null;
        System.gc();
        this.object = new Object[num][];
    }

    public void setMFImage(int imageid, MFImage img) {
        this.images[imageid] = img;
    }

    public void setDrawer(Drawer drawer) {
        this.drawer = drawer;
    }

    public void releaseMFImageAll() {
        for (int i = 0; i < this.images.length; i++) {
            this.images[i] = null;
        }
        System.gc();
    }

    public int getMFImageNum() {
        return this.images.length;
    }

    public void releaseMFImage(int iObject) {
        if (this.object != null && this.object[iObject] != null) {
            Object[][] frame = (Object[][])this.object[iObject][0];
            for (Object[][][] objArr : (Object[][][][])frame) {
                Object[][] layer = objArr[0];
                for (Object[] objArr2 : layer) {
                    this.images[((byte[]) objArr2[1])[0] & 0xFF] = null;
                }
            }
            System.gc();
        }
    }

    public void releasePictureNameAll() {
    }

    public void readMFImage(int iObject) {
        if (this.object != null && this.object[iObject] != null) {
            Object[][] frame = (Object[][])this.object[iObject][0];
            for (Object[][][] objArr : (Object[][][][])frame) {
                Object[][] layer = objArr[0];
                for (Object[] objArr2 : layer) {
                    getMFImage(((byte[]) objArr2[1])[0] & 0xFF);
                }
            }
            System.gc();
        }
    }

    public int getObjectNum() {
        if (this.object != null) {
            return this.object.length;
        }
        return -1;
    }

    public int getActionNum(int objectIndex) {
        if (objectIndex < 0 || objectIndex >= this.object.length) {
            return -1;
        }
        return ((Object[][][])(this.object))[objectIndex][1].length;
    }

    public int getPageNum(int objectIndex, int actionIndex) {
        if (objectIndex < 0 || objectIndex >= this.object.length) {
            return -1;
        }
        Object[] action = (Object[])this.object[objectIndex][1];
        if (actionIndex < 0 || actionIndex >= action.length) {
            return -1;
        }
        return ((Object[][])action)[actionIndex].length;
    }

    public int getDelayNum(int objectIndex, int actionIndex, int pageIndex) {
        if (objectIndex < 0 || objectIndex >= this.object.length) {
            return -1;
        }
        Object[] action = (Object[])this.object[objectIndex][1];
        if (actionIndex < 0 || actionIndex >= action.length) {
            return -1;
        }
        Object[][] page = (Object[][])action[actionIndex];
        if (pageIndex < 0 || pageIndex >= page.length) {
            return -1;
        }
        return ((byte[]) page[pageIndex][0])[1] & 0xFF;
    }

    public int getActionTime(int objectIndex, int actionIndex) {
        int time = 0;
        if (objectIndex < 0 || objectIndex >= this.object.length) {
            return -1;
        }
        Object[] action = (Object[])this.object[objectIndex][1];
        if (actionIndex < 0 || actionIndex >= action.length) {
            return -1;
        }
        for (Object[] objArr : (Object[][])action[actionIndex]) {
            time += (((byte[]) objArr[0])[1] & 0xFF) + 1;
        }
        return time;
    }

    public void printlnMFImagePath(boolean bAll) {
        int i = 0;
        while (i < this.imagesmanager.length) {
            if ((!bAll && this.images[i] == null) || this.imagesmanager[i] != null) {
                System.out.println((String) this.imagesmanager[i][0]);
            }
            i++;
        }
    }
/* // JD-GUI decompiled
    private void loadObject(DataInputStream paramDataInputStream, int paramInt)
    {
        int i5;
        int i7;
        int m;
        Object[] arrayOfObject2;
        Object[] arrayOfObject5;
        Object[] arrayOfObject6;
        short[] arrayOfShort4 = new short[4];
        byte[] arrayOfByte3 = new byte[3];
        byte[] arrayOfByte2;
        for (;;)
        {
            try
            {
                this.object[paramInt] = new Object[3];
                int i = 0xFF & paramDataInputStream.readByte();
                System.out.println("m_nFrames:" + i);
                Object[][] arrayOfObject1 = (Object[][])Array.newInstance(Object.class, new int[] { i, 3 });
                int j = 0;
                if (j >= i)
                {
                    this.object[paramInt][0] = arrayOfObject1;
                    int k = 0xFF & paramDataInputStream.readByte();
                    arrayOfObject2 = new Object[k];
                    m = 0;
                    if (m < k) {
                        break label1037;
                    }
                    this.object[paramInt][1] = arrayOfObject2;
                    return;
                }
                System.out.println("Frame:" + j);
                int i2 = 0xFF & paramDataInputStream.readByte();
                System.out.println("layers:" + i2);
                Object[][] arrayOfObject4 = (Object[][])Array.newInstance(Object.class, new int[] { i2, 3 });
                int i3 = 0;
                if (i3 >= i2)
                {
                    arrayOfObject1[j][0] = arrayOfObject4;
                    int i4 = 0xFF & paramDataInputStream.readByte();
                    System.out.println("rectarrays:" + i4);
                    arrayOfObject5 = new Object[i4];
                    i5 = 0;
                    if (i5 < i4) {
                        break;
                    }
                    arrayOfObject1[j][1] = arrayOfObject5;
                    int i6 = 0xFF & paramDataInputStream.readByte();
                    System.out.println("crossarrays:" + i6);
                    arrayOfObject6 = new Object[i6];
                    i7 = 0;
                    if (i7 < i6) {
                        break label963;
                    }
                    arrayOfObject1[j][2] = arrayOfObject6;
                    j++;
                    continue;
                }
                arrayOfByte2 = new byte[1];
                arrayOfByte2[0] = paramDataInputStream.readByte();
                System.out.println("type[0]:" + arrayOfByte2[0]);
                arrayOfObject4[i3][0] = arrayOfByte2;
                arrayOfByte3 = (byte[])null;
                arrayOfShort4 = (short[])null;
                if (arrayOfByte2[0] == 0)
                {
                    arrayOfByte3 = new byte[3];
                    arrayOfShort4 = new short[2];
                    arrayOfByte3[0] = paramDataInputStream.readByte();
                    arrayOfByte3[1] = paramDataInputStream.readByte();
                    arrayOfByte3[2] = paramDataInputStream.readByte();
                    arrayOfShort4[0] = paramDataInputStream.readShort();
                    arrayOfShort4[1] = paramDataInputStream.readShort();
                    arrayOfObject4[i3][1] = arrayOfByte3;
                    arrayOfObject4[i3][2] = arrayOfShort4;
                    i3++;
                    continue;
                }
                if (arrayOfByte2[0] == 1)
                {
                    arrayOfShort4 = new short[4];
                    arrayOfByte3 = new byte[3];
                    arrayOfShort4[0] = paramDataInputStream.readShort();
                    arrayOfShort4[1] = paramDataInputStream.readShort();
                    arrayOfByte3[0] = paramDataInputStream.readByte();
                    arrayOfByte3[1] = paramDataInputStream.readByte();
                    arrayOfByte3[2] = paramDataInputStream.readByte();
                    arrayOfShort4[2] = paramDataInputStream.readShort();
                    arrayOfShort4[3] = paramDataInputStream.readShort();
                    continue;
                }
                if (arrayOfByte2[0] != 2) {
                    break label644;
                }
            }
            catch (Exception localException)
            {
                localException.printStackTrace();
                return;
            }
            arrayOfShort4[0] = paramDataInputStream.readShort();
            arrayOfShort4[1] = paramDataInputStream.readShort();
            arrayOfByte3[0] = paramDataInputStream.readByte();
            arrayOfByte3[1] = paramDataInputStream.readByte();
            arrayOfByte3[2] = paramDataInputStream.readByte();
            arrayOfShort4[2] = paramDataInputStream.readShort();
            arrayOfShort4[3] = paramDataInputStream.readShort();
            continue;
            label644:
            if (arrayOfByte2[0] == 3)
            {
                arrayOfShort4 = new short[2];
                arrayOfByte3 = new byte[7];
                arrayOfByte3[0] = paramDataInputStream.readByte();
                arrayOfByte3[1] = paramDataInputStream.readByte();
                arrayOfByte3[2] = paramDataInputStream.readByte();
                arrayOfByte3[3] = paramDataInputStream.readByte();
                arrayOfByte3[4] = paramDataInputStream.readByte();
                arrayOfByte3[5] = paramDataInputStream.readByte();
                arrayOfByte3[6] = paramDataInputStream.readByte();
                arrayOfShort4[0] = paramDataInputStream.readShort();
                arrayOfShort4[1] = paramDataInputStream.readShort();
            }
            else if (arrayOfByte2[0] == 4)
            {
                arrayOfShort4 = new short[4];
                arrayOfShort4[0] = paramDataInputStream.readShort();
                arrayOfShort4[1] = paramDataInputStream.readShort();
                arrayOfShort4[2] = paramDataInputStream.readShort();
                arrayOfShort4[3] = paramDataInputStream.readShort();
            }
            else if (arrayOfByte2[0] == 5)
            {
                arrayOfShort4 = new short[2];
                arrayOfShort4[0] = paramDataInputStream.readShort();
                arrayOfShort4[1] = paramDataInputStream.readShort();
            }
            else if (arrayOfByte2[0] == 6)
            {
                arrayOfShort4 = new short[2];
                arrayOfByte3 = new byte[2];
                arrayOfByte3[0] = paramDataInputStream.readByte();
                arrayOfByte3[1] = paramDataInputStream.readByte();
                arrayOfShort4[0] = paramDataInputStream.readShort();
                arrayOfShort4[1] = paramDataInputStream.readShort();
            }
        }
        int i10 = 0xFF & paramDataInputStream.readByte();
        Object[] arrayOfObject8 = new Object[i10];
        for (int i11 = 0;; i11++)
        {
            if (i11 >= i10)
            {
                arrayOfObject5[i5] = arrayOfObject8;
                i5++;
                break;
            }
            short[] arrayOfShort3 = new short[4];
            arrayOfShort3[0] = paramDataInputStream.readShort();
            arrayOfShort3[1] = paramDataInputStream.readShort();
            arrayOfShort3[2] = paramDataInputStream.readShort();
            arrayOfShort3[3] = paramDataInputStream.readShort();
            arrayOfObject8[i11] = arrayOfShort3;
        }
        label963:
        int i8 = 0xFF & paramDataInputStream.readByte();
        Object[] arrayOfObject7 = new Object[i8];
        for (int i9 = 0;; i9++)
        {
            if (i9 >= i8)
            {
                arrayOfObject6[i7] = arrayOfObject7;
                i7++;
                break;
            }
            short[] arrayOfShort2 = new short[2];
            arrayOfShort2[0] = paramDataInputStream.readShort();
            arrayOfShort2[1] = paramDataInputStream.readShort();
            arrayOfObject7[i9] = arrayOfShort2;
        }
        label1037:
        int n = 0xFF & paramDataInputStream.readByte();
        Object[][] arrayOfObject3 = (Object[][])Array.newInstance(Object.class, new int[] { n, 2 });
        for (int i1 = 0;; i1++)
        {
            if (i1 >= n)
            {
                arrayOfObject2[m] = arrayOfObject3;
                m++;
                break;
            }
            byte[] arrayOfByte1 = new byte[2];
            arrayOfByte1[0] = paramDataInputStream.readByte();
            arrayOfByte1[1] = paramDataInputStream.readByte();
            arrayOfObject3[i1][0] = arrayOfByte1;
            short[] arrayOfShort1 = new short[2];
            arrayOfShort1[0] = paramDataInputStream.readShort();
            arrayOfShort1[1] = paramDataInputStream.readShort();
            arrayOfObject3[i1][1] = arrayOfShort1;
        }
    }
*/

    private void loadObject(DataInputStream dis, int i) {
        try {
            int j;
            int k;
            this.object[i] = new Object[3];
            int frames = dis.readByte() & 0xFF;
            System.out.println("m_nFrames:" + frames);
            Object[][] frame = (Object[][]) Array.newInstance(Object.class, new int[]{frames, 3});
            for (j = 0; j < frames; j++) {
                int l;
                System.out.println("Frame:" + j);
                int layers = dis.readByte() & 0xFF;
                System.out.println("layers:" + layers);
                Object[][] layer = (Object[][]) Array.newInstance(Object.class, new int[]{layers, 3});
                for (k = 0; k < layers; k++) {
                    byte[] type = new byte[]{dis.readByte()};
                    System.out.println("type[0]:" + type[0]);
                    layer[k][0] = type;
                    byte[] b = null;
                    short[] s = null;
                    if (type[0] == (byte) 0) {
                        b = new byte[3];
                        s = new short[2];
                        b[0] = dis.readByte();
                        b[1] = dis.readByte();
                        b[2] = dis.readByte();
                        s[0] = dis.readShort();
                        s[1] = dis.readShort();
                    } else if (type[0] == (byte) 1) {
                        s = new short[4];
                        b = new byte[3];
                        s[0] = dis.readShort();
                        s[1] = dis.readShort();
                        b[0] = dis.readByte();
                        b[1] = dis.readByte();
                        b[2] = dis.readByte();
                        s[2] = dis.readShort();
                        s[3] = dis.readShort();
                    } else if (type[0] == (byte) 2) {
                        s = new short[4];
                        b = new byte[3];
                        s[0] = dis.readShort();
                        s[1] = dis.readShort();
                        b[0] = dis.readByte();
                        b[1] = dis.readByte();
                        b[2] = dis.readByte();
                        s[2] = dis.readShort();
                        s[3] = dis.readShort();
                    } else if (type[0] == (byte) 3) {
                        s = new short[2];
                        b = new byte[7];
                        b[0] = dis.readByte();
                        b[1] = dis.readByte();
                        b[2] = dis.readByte();
                        b[3] = dis.readByte();
                        b[4] = dis.readByte();
                        b[5] = dis.readByte();
                        b[6] = dis.readByte();
                        s[0] = dis.readShort();
                        s[1] = dis.readShort();

                    } else if (type[0] == (byte) 4) {
                        s = new short[]{dis.readShort(), dis.readShort(), dis.readShort(), dis.readShort()};
                    } else if (type[0] == (byte) 5) {
                        s = new short[]{dis.readShort(), dis.readShort()};
                    } else if (type[0] == (byte) 6) {
                        s = new short[2];
                        b = new byte[2];
                        b[0] = dis.readByte();
                        b[1] = dis.readByte();
                        s[0] = dis.readShort();
                        s[1] = dis.readShort();
                    }
                    layer[k][1] = b;
                    layer[k][2] = s;
                }
                frame[j][0] = layer;
                int rectarrays = dis.readByte() & 0xFF;
                System.out.println("rectarrays:" + rectarrays);
                Object[] rectarray = new Object[rectarrays];
                for (k = 0; k < rectarrays; k++) {
                    int rects = dis.readByte() & 0xFF;
                    Object[] rect = new Object[rects];
                    for (l = 0; l < rects; l++) {
                        rect[l] = new short[]{dis.readShort(), dis.readShort(), dis.readShort(), dis.readShort()};
                    }
                    rectarray[k] = rect;
                }
                frame[j][1] = rectarray;
                int crossarrays = dis.readByte() & 0xFF;
                System.out.println("crossarrays:" + crossarrays);
                Object[] crossarray = new Object[crossarrays];
                for (k = 0; k < crossarrays; k++) {
                    int crosses = dis.readByte() & 0xFF;
                    Object[] cross = new Object[crosses];
                    for (l = 0; l < crosses; l++) {
                        cross[l] = new short[]{dis.readShort(), dis.readShort()};
                    }
                    crossarray[k] = cross;
                }
                frame[j][2] = crossarray;
            }
            this.object[i][0] = frame;
            int actions = dis.readByte() & 0xFF;
            Object[] action = new Object[actions];
            for (j = 0; j < actions; j++) {
                int pages = dis.readByte() & 0xFF;
                Object[][] page = (Object[][]) Array.newInstance(Object.class, new int[]{pages, 2});
                for (k = 0; k < pages; k++) {
                    page[k][0] = new byte[]{dis.readByte(), dis.readByte()};
                    page[k][1] = new short[]{dis.readShort(), dis.readShort()};
                }
                action[j] = page;
            }
            this.object[i][1] = action;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean load(String datapath, int[] objectIndex, int[] imageIndex) {
        try {
            this.isLoad = false;
            InputStream in = MFDevice.getResourceAsStream(datapath);
            if (in == null) {
                return false;
            }
            DataInputStream dis = new DataInputStream(in);
            int objects = dis.readByte() & 0xFF;
            newObjectArray(objects);
            int i = 0;
            int j = 0;
            while (i < objects) {
                try {
                    int len = dis.readInt();
                    if (objectIndex == null || (j < objectIndex.length && objectIndex[j] == i)) {
                        loadObject(dis, i);
                        j++;
                    } else {
                        dis.skip((long) len);
                    }
                    i++;
                } catch (Exception e) {
                    e.printStackTrace();
                    return false;
                }
            }
            this.imagesmanager = (Object[][]) Array.newInstance(Object.class, new int[]{dis.readByte() & 0xFF, 2});
            this.images = new MFImage[this.imagesmanager.length];
            for (j = 0; j < this.imagesmanager.length; j++) {
                short[][] clip = (short[][]) Array.newInstance(Short.TYPE, new int[]{dis.readByte() & 0xFF, 4});
                for (int k = 0; k < clip.length; k++) {
                    for (int l = 0; l < clip[k].length; l++) {
                        clip[k][l] = dis.readShort();
                    }
                }
                this.imagesmanager[j][0] = dis.readUTF();
                this.imagesmanager[j][1] = clip;
            }
            this.stringmanager = new String[(dis.readByte() & 0xFF)][];
            for (i = 0; i < this.stringmanager.length; i++) {
                this.stringmanager[i] = new String[(dis.readByte() & 0xFF)];
                for (j = 0; j < this.stringmanager[i].length; j++) {
                    this.stringmanager[i][j] = dis.readUTF();
                }
            }
            if (imageIndex == null) {
                for (i = 0; i < this.imagesmanager.length; i++) {
                    getMFImage(i);
                }
            } else {
                for (int mFImage : imageIndex) {
                    getMFImage(mFImage);
                }
            }
            dis.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        this.isLoad = true;
        return this.isLoad;
    }

    public String getString(int iArray, int i) {
        return this.stringmanager[iArray][i];
    }

    public int getStringNum(int iArray) {
        return this.stringmanager[iArray].length;
    }

    public MFImage getMFImage(int imageid) {
        if (imageid >= this.images.length || imageid < 0) {
            return null;
        }
        if (this.images[imageid] == null) {
            try {
                this.images[imageid] = MFImage.createImage((String) this.imagesmanager[imageid][0]);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return this.images[imageid];
    }

    public String getImageName(int imageid) {
        if (imageid >= this.images.length || imageid < 0) {
            return null;
        }
        return (String) this.imagesmanager[imageid][0];
    }

    public short[] getCross(int objectid, int frameid, int i, int j, int iX, int iY, int attr, int px, int py) {
        Object[] crossarray = ((Object[][][][][])this.object)[objectid][0][frameid][2];
        if (i >= crossarray.length) {
            return null;
        }
        Object[] cross = (Object[])crossarray[i];
        if (j >= cross.length) {
            return null;
        }
        int x = px + ((short[]) cross[j])[0];
        int y = py + ((short[]) cross[j])[1];
        if ((attr & 2) > 0) {
            x = -x;
        }
        if ((attr & 4) > 0) {
            y = -y;
        }
        return new short[]{(short) (x + iX), (short) (y + iY)};
    }

    public short[][] getCrossArray(int objectid, int frameid, int i, int iX, int iY, int attr, int px, int py) {
        Object[] crossarray = ((Object[][][][][])this.object)[objectid][0][frameid][2];
        if (i >= crossarray.length) {
            return null;
        }
        short[][] temp = new short[((Object[])crossarray[i]).length][];
        for (int j = 0; j < temp.length; j++) {
            temp[j] = getCross(objectid, frameid, i, j, iX, iY, attr, px, py);
        }
        return temp;
    }

    public int getCrossNum(int objectid, int frameid, int i) {
        return ((Object[][][][][][])this.object)[objectid][0][frameid][2][i].length;
    }

    public int getCrossArrayNum(int objectid, int frameid) {
        return ((Object[][][][][][])this.object)[objectid][0][frameid][2].length;
    }

    public short[] getRect(int objectid, int frameid, int i, int j, int iX, int iY, int attr, int px, int py) {
        Object[] rectarray = ((Object[][][][][])this.object)[objectid][0][frameid][1];
        if (i >= rectarray.length) {
            return null;
        }
        Object[] rect = (Object[])rectarray[i];
        if (j >= rect.length) {
            return null;
        }
        int x = px + ((short[]) rect[j])[0];
        int y = py + ((short[]) rect[j])[1];
        if ((attr & 2) > 0) {
            x = ((-x) - ((short[]) rect[j])[2]) + 1;
        }
        if ((attr & 4) > 0) {
            y = ((-y) - ((short[]) rect[j])[3]) + 1;
        }
        return new short[]{(short) (x + iX), (short) (y + iY), ((short[]) rect[j])[2], ((short[]) rect[j])[3]};
    }

    public short[][] getRectArray(int objectid, int frameid, int i, int iX, int iY, int attr, int px, int py) {
        Object[] rectarray = ((Object[][][][][])this.object)[objectid][0][frameid][1];
        if (i >= rectarray.length) {
            return null;
        }
        short[][] temp = new short[((Object[])rectarray[i]).length][];
        for (int j = 0; j < temp.length; j++) {
            temp[j] = getRect(objectid, frameid, i, j, iX, iY, attr, px, py);
        }
        return temp;
    }

    public int getRectNum(int objectid, int frameid, int i) {
        return ((Object[][][][][][])this.object)[objectid][0][frameid][1][i].length;
    }

    public int getRectArrayNum(int objectid, int frameid) {
        return ((Object[][][][][][])this.object)[objectid][0][frameid][1].length;
    }

    private void fillRect(MFGraphics g, int iX, int iY, Object[][] layer, int layerid, int attr, int px, int py) {
        byte[] rgb = (byte[])layer[layerid][1];
        short[] rect = (short[])layer[layerid][2];
        int x = px + rect[2];
        int y = py + rect[3];
        if ((attr & 2) > 0) {
            x = ((-x) - rect[0]) + 1;
        }
        if ((attr & 4) > 0) {
            y = ((-y) - rect[1]) + 1;
        }
        int color = g.getColor();
        g.setColor(rgb[0] & 255, rgb[1] & 255, rgb[2] & 255);
        g.fillRect(iX + x, iY + y, rect[0], rect[1]);
        g.setColor(color);
    }

    private void drawRect(MFGraphics g, int iX, int iY, Object[][] layer, int layerid, int attr, int px, int py) {
        byte[] rgb = (byte[])layer[layerid][1];
        short[] rect = (short[])layer[layerid][2];
        int x = px + rect[2];
        int y = py + rect[3];
        if ((attr & 2) > 0) {
            x = ((-x) - rect[0]) + 1;
        }
        if ((attr & 4) > 0) {
            y = ((-y) - rect[1]) + 1;
        }
        int color = g.getColor();
        g.setColor(rgb[0] & 255, rgb[1] & 255, rgb[2] & 255);
        g.drawRect(iX + x, iY + y, rect[0], rect[1]);
        g.setColor(color);
    }

    private void drawString(MFGraphics g, int iX, int iY, Object[][] layer, int layerid, int px, int py) {
        byte[] linergb = (byte[])layer[layerid][1];
        if (this.stringmanager != null && this.stringmanager[linergb[0]] != null && this.stringmanager[linergb[0]][linergb[1]] != null) {
            short[] xy = (short[])layer[layerid][2];
            int color = g.getColor();
            g.setColor(linergb[2] & 0xFF, linergb[3] & 0xFF, linergb[4] & 0xFF);
            int font = g.getFont();
            if (linergb[5] == (byte) 0) {
                g.setFont(-1);
            } else if (linergb[5] == (byte) 1) {
                g.setFont(-2);
            } else if (linergb[5] == (byte) 2) {
                g.setFont(-3);
            }
            if (linergb[6] == (byte) 0) {
                g.drawString(this.stringmanager[linergb[0]][linergb[1]], (iX + px) + xy[0], (iY + py) + xy[1], 20);
            } else if (linergb[6] == (byte) 1) {
                g.drawString(this.stringmanager[linergb[0]][linergb[1]], (iX + px) + xy[0], (iY + py) + xy[1], 17);
            } else if (linergb[6] == (byte) 2) {
                g.drawString(this.stringmanager[linergb[0]][linergb[1]], (iX + px) + xy[0], (iY + py) + xy[1], 24);
            }
            g.setFont(font);
            g.setColor(color);
        }
    }

    private void callbackRect(MFGraphics g, int iX, int iY, Object[][] layer, int objectid, int frameid, int layerid, int attr, int px, int py) {
        short[] rect = (short[])layer[layerid][2];
        int x = px + rect[2];
        int y = py + rect[3];
        if ((attr & 2) > 0) {
            x = ((-x) - rect[0]) + 1;
        }
        if ((attr & 4) > 0) {
            y = ((-y) - rect[1]) + 1;
        }
        if (this.drawer != null) {
            this.drawer.callbackRect(g, this, objectid, frameid, layerid, iX, iY, rect[0], rect[1], attr);
        }
    }

    private void callbackCross(MFGraphics g, int iX, int iY, Object[][] layer, int objectid, int frameid, int layerid, int attr, int px, int py) {
        short[] xy = (short[])layer[layerid][2];
        int x = px + xy[0];
        int y = py + xy[1];
        if ((attr & 2) > 0) {
            x = -x;
        }
        if ((attr & 4) > 0) {
            y = -y;
        }
        if (this.drawer != null) {
            this.drawer.callbackCross(g, this, objectid, frameid, layerid, iX, iY, attr);
        }
    }

    private void drawIndex(MFGraphics g, int iX, int iY, MFImage[] imgs, Object[][] layer, int layerid, int attr, int px, int py) {
        byte[] index = (byte[])layer[layerid][1];
        short[] xy = (short[])layer[layerid][2];
        draw(g, iX, iY, imgs, index[0], index[1], attr, px + xy[0], py + xy[1]);
    }

    private void drawMFImage(MFGraphics g, int iX, int iY, MFImage[] imgs, Object[][] layer, int layerid, int attr, int px, int py) {
        MFImage image;
        int imageid = ((byte[]) layer[layerid][1])[0] & 255;
        if (imgs == null || imgs.length <= imageid || imgs[imageid] == null) {
            image = getMFImage(imageid);
        } else {
            image = imgs[imageid];
        }
        if (image != null) {
            short[] rect = (short[]) ((Object[][][])this.imagesmanager)[imageid][1][((byte[]) layer[layerid][1])[1] & 255];
            int tran = ((byte[]) layer[layerid][1])[2];
            int x = px + ((short[]) layer[layerid][2])[0];
            int y = py + ((short[]) layer[layerid][2])[1];
            if ((tran & 1) <= 0) {
                if ((attr & 2) > 0) {
                    x = ((-x) - rect[2]) + 1;
                }
                if ((attr & 4) > 0) {
                    y = ((-y) - rect[3]) + 1;
                }
                if ((attr & 1) > 0) {
                    int tmp = y;
                    y = ((-x) - rect[2]) + 1;
                    x = tmp;
                }
            } else if ((attr & 2) > 0 && (attr & 4) > 0) {
                x = ((-x) - rect[3]) + 1;
                y = ((-y) - rect[2]) + 1;
            } else if ((attr & 2) > 0) {
                x = ((-x) - rect[3]) + 1;
                attr = (attr & -3) | 4;
            } else if ((attr & 4) > 0) {
                y = ((-y) - rect[2]) + 1;
                attr = (attr & -5) | 2;
            }
            g.drawRegion(image, rect[0], rect[1], rect[2], rect[3], this.TRANMODIF[tran ^ attr], iX + x, iY + y, 0);
        }
    }

    public void draw(MFGraphics g, int iX, int iY, MFImage[] imgs, int objectid, int frameid, int attr, int px, int py) {
        Object[][] layer = (Object[][])((Object[][][][])this.object)[objectid][0][frameid][0];
        for (int layerid = 0; layerid < layer.length; layerid++) {
            switch (((byte[]) layer[layerid][0])[0]) {
                case (byte) 0:
                    drawMFImage(g, iX, iY, imgs, layer, layerid, attr, px, py);
                    break;
                case (byte) 1:
                    fillRect(g, iX, iY, layer, layerid, attr, px, py);
                    break;
                case (byte) 2:
                    drawRect(g, iX, iY, layer, layerid, attr, px, py);
                    break;
                case (byte) 3:
                    drawString(g, iX, iY, layer, layerid, px, py);
                    break;
                case (byte) 4:
                    if (this.drawer == null) {
                        break;
                    }
                    callbackRect(g, iX, iY, layer, objectid, frameid, layerid, attr, px, py);
                    break;
                case (byte) 5:
                    if (this.drawer == null) {
                        break;
                    }
                    callbackCross(g, iX, iY, layer, objectid, frameid, layerid, attr, px, py);
                    break;
                case (byte) 6:
                    drawIndex(g, iX, iY, imgs, layer, layerid, attr, px, py);
                    break;
                default:
                    break;
            }
        }
    }

    public void draw(MFGraphics g, int iX, int iY, MFImage[] imgs, int objectid, int actionid, int pageid, int attr) {
        if (objectid < this.object.length) {
            Object[] action = (Object[]) this.object[objectid][1];
            if (actionid < action.length) {
                Object[][] page = (Object[][]) action[actionid];
                if (pageid < page.length) {
                    draw(g, iX, iY, imgs, objectid, ((byte[]) page[pageid][0])[0] & 255, attr, ((short[]) page[pageid][1])[0], ((short[]) page[pageid][1])[1]);
                }
            }
        }
    }
}
