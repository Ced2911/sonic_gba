package Lib;

public class Coordinate {
    private static Coordinate returnInstance;
    public int f13x;
    public int f14y;

    public Coordinate() {
        setValue(0, 0);
    }

    public Coordinate(int mx, int my) {
        setValue(mx, my);
    }

    public void setValue(int mx, int my) {
        this.f13x = mx;
        this.f14y = my;
    }

    public boolean equals(Object a) {
        if ((a instanceof Coordinate) && this.f13x == ((Coordinate) a).f13x && this.f14y == ((Coordinate) a).f14y) {
            return true;
        }
        return false;
    }

    public boolean inTheArea(Coordinate[] area) {
        for (Object equals : area) {
            if (equals(equals)) {
                return true;
            }
        }
        return false;
    }

    public static Coordinate returnCoordinate(int x, int y) {
        if (returnInstance == null) {
            returnInstance = new Coordinate(x, y);
        } else {
            returnInstance.setValue(x, y);
        }
        return returnInstance;
    }
}
