package Lib;

import com.sega.mobile.framework.device.MFGraphics;

public interface Drawer {
    void callbackCross(MFGraphics mFGraphics, AnimationData animationData, int i, int i2, int i3, int i4, int i5, int i6);

    void callbackRect(MFGraphics mFGraphics, AnimationData animationData, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8);
}
