package Lib;

public interface Def {
    public static final boolean BOLD_STRING = true;
    public static final boolean EN_VERSION = false;
    public static final int FONT = 14;
    public static final int FONT_H = GameEngine.Def.FONT_H;
    public static final int FONT_H_HALF = (FONT_H >> 1);
    public static final int LINE_SPACE = (FONT_H + 2);
    public static final boolean USE_BMF = false;
}
