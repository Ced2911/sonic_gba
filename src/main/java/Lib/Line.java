package Lib;

public class Line {
    private static final int[] DIVISOR = new int[]{2, 3, 5, 7, 11, 13, 17, 19, 23};
    private static Line line1 = new Line();
    private static Line line2 = new Line();
    public int f17A;
    public int f18B;
    public int f19C;

    public static class CrossPoint {
        public boolean hasPoint;
        public int f15x;
        public int f16y;

        public void reset() {
            this.hasPoint = false;
        }
    }

    public Line() {}

    public Line(int x0, int y0, int x1, int y1) {
        setProperty(x0, y0, x1, y1);
    }

    public Line(int A, int B, int C) {
        this.f17A = A;
        this.f18B = B;
        this.f19C = C;
    }

    public void setProperty(int x0, int y0, int x1, int y1) {
        this.f17A = y1 - y0;
        this.f18B = x0 - x1;
        this.f19C = (this.f17A * x0) + (this.f18B * y0);
        dealDivisor();
    }

    public int getX(int y) {
        if (isHorizontal()) {
            return 0;
        }
        return (this.f19C - (this.f18B * y)) / this.f17A;
    }

    public int getY(int x) {
        if (isVertical()) {
            return 0;
        }
        return (this.f19C - (this.f17A * x)) / this.f18B;
    }

    public boolean isHorizontal() {
        if (this.f17A == 0) {
            return true;
        }
        return false;
    }

    public boolean isVertical() {
        if (this.f18B == 0) {
            return true;
        }
        return false;
    }

    public boolean isLegal() {
        if (isHorizontal() && isVertical()) {
            return false;
        }
        return true;
    }

    private void dealDivisor() {
        if (this.f17A != 0 || this.f18B != 0) {
            for (int d : DIVISOR) {
                while (this.f17A % d == 0 && this.f18B % d == 0 && this.f19C % d == 0) {
                    this.f17A /= d;
                    this.f18B /= d;
                    this.f19C /= d;
                }
            }
        }
    }

    public void getCrossPoint(CrossPoint re, int x0, int y0, int x1, int y1) {
        re.reset();
        line2.setProperty(x0, y0, x1, y1);
        getCrossPoint(re, line2);
        if (!re.hasPoint) {
            return;
        }
        if (re.f15x < Math.min(x0, x1) || re.f15x > Math.max(x0, x1) || re.f16y < Math.min(y0, y1) || re.f16y > Math.max(y0, y1)) {
            re.hasPoint = false;
        }
    }

    private void getCrossPoint(CrossPoint re, Line l) {
        re.reset();
        if (l != null && l.isLegal()) {
            if (!isHorizontal() || !l.isHorizontal()) {
                if ((!isVertical() || !l.isVertical()) && (this.f17A * l.f18B) - (this.f18B * l.f17A) != 0) {
                    int x = ((this.f19C * l.f18B) - (l.f19C * this.f18B)) / ((this.f17A * l.f18B) - (this.f18B * l.f17A));
                    int y = ((l.f19C * this.f17A) - (this.f19C * l.f17A)) / ((this.f17A * l.f18B) - (this.f18B * l.f17A));
                    re.hasPoint = true;
                    re.f15x = x;
                    re.f16y = y;
                }
            }
        }
    }

    public static void getCrossPoint(CrossPoint re, int x0, int y0, int x1, int y1, int x2, int y2, int x3, int y3) {
        re.reset();
        line1.setProperty(x0, y0, x1, y1);
        line2.setProperty(x2, y2, x3, y3);
        line1.getCrossPoint(re, line2);
        if (!re.hasPoint) {
            return;
        }
        if (re.f15x < Math.min(x0, x1) || re.f15x > Math.max(x0, x1) || re.f15x < Math.min(x2, x3) || re.f15x > Math.max(x2, x3) || re.f16y < Math.min(y0, y1) || re.f16y > Math.max(y0, y1) || re.f16y < Math.min(y2, y3) || re.f16y > Math.max(y2, y3)) {
            re.hasPoint = false;
        }
    }

    public Line getPlumbLine() {
        return new Line(-this.f18B, this.f17A, 0);
    }

    public int cos(int length) {
        int re = 0;
        try {
            re = Math.abs(((this.f18B * length) << 3) / crlFP32.sqrt((this.f17A * this.f17A) + (this.f18B * this.f18B)));
        } catch (Exception e) {
            e.toString();
        }
        return re;
    }

    public int sin(int length) {
        int re = 0;
        try {
            re = Math.abs(((this.f17A * length) << 3) / crlFP32.sqrt((this.f17A * this.f17A) + (this.f18B * this.f18B)));
        } catch (Exception e) {
            e.toString();
        }
        return re;
    }

    public boolean directRatio() {
        if (this.f18B * this.f17A < 0) {
            return true;
        }
        return false;
    }

    public Direction getOneDirection() {
        if (isHorizontal()) {
            return new Direction(-1, 0);
        }
        if (isVertical()) {
            return new Direction(0, -1);
        }
        if (directRatio()) {
            return new Direction(1, 1);
        }
        if (directRatio()) {
            return new Direction(0, 0);
        }
        return new Direction(-1, 1);
    }
}
