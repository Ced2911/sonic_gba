package Lib;

import SonicGBA.GlobalResource;
import SonicGBA.PlayerObject;
import com.sega.mobile.framework.device.MFPlayer;
import com.sega.mobile.framework.device.MFSound;

public class SoundSystem {
    public static final byte BGM_1UP = (byte) 43;
    public static final byte BGM_ASPHYXY = (byte) 21;
    public static final byte BGM_BOSS_01 = (byte) 22;
    public static final byte BGM_BOSS_02 = (byte) 23;
    public static final byte BGM_BOSS_03 = (byte) 24;
    public static final byte BGM_BOSS_04 = (byte) 25;
    public static final byte BGM_BOSS_F1 = (byte) 46;
    public static final byte BGM_BOSS_F2 = (byte) 47;
    public static final byte BGM_BOSS_FINAL3 = (byte) 19;
    public static final byte BGM_CLEAR_ACT1 = (byte) 26;
    public static final byte BGM_CLEAR_ACT2 = (byte) 27;
    public static final byte BGM_CLEAR_EX = (byte) 29;
    public static final byte BGM_CLEAR_FINAL = (byte) 28;
    public static final byte BGM_CONTINUE = (byte) 5;
    public static final byte BGM_CREDIT = (byte) 33;
    public static final byte BGM_ENDING_EX_01 = (byte) 32;
    public static final byte BGM_ENDING_EX_02 = (byte) 45;
    public static final byte BGM_ENDING_FINAL = (byte) 31;
    public static final byte BGM_GAMEOVER = (byte) 30;
    public static final byte BGM_INVINCIBILITY = (byte) 44;
    public static final byte BGM_NEWRECORD = (byte) 41;
    public static final byte BGM_OPENING = (byte) 0;
    public static final byte BGM_PLAYER_SELECT = (byte) 2;
    public static final byte BGM_RANKING = (byte) 4;
    public static final byte BGM_SP = (byte) 35;
    public static final byte BGM_SP_CLEAR = (byte) 37;
    public static final byte BGM_SP_EMERALD = (byte) 38;
    public static final byte BGM_SP_INTRO = (byte) 34;
    public static final byte BGM_SP_TOTAL_CLEAR = (byte) 40;
    public static final byte BGM_SP_TOTAL_MISS = (byte) 39;
    public static final byte BGM_SP_TRICK = (byte) 36;
    public static final byte BGM_ST1_1 = (byte) 6;
    public static final byte BGM_ST1_2 = (byte) 7;
    public static final byte BGM_ST2_1 = (byte) 8;
    public static final byte BGM_ST2_2 = (byte) 9;
    public static final byte BGM_ST3_1 = (byte) 10;
    public static final byte BGM_ST3_2 = (byte) 11;
    public static final byte BGM_ST4_1 = (byte) 12;
    public static final byte BGM_ST4_2 = (byte) 13;
    public static final byte BGM_ST5_1 = (byte) 14;
    public static final byte BGM_ST5_2 = (byte) 15;
    public static final byte BGM_ST6_1 = (byte) 16;
    public static final byte BGM_ST6_2 = (byte) 17;
    public static final byte BGM_ST_EX = (byte) 20;
    public static final byte BGM_ST_FINAL = (byte) 18;
    public static final byte BGM_TIMEATTACKGOAL = (byte) 42;
    public static final byte BGM_TITLE = (byte) 1;
    public static final byte BGM_ZONESELECT = (byte) 3;
    public static final boolean HAS_SE = false;
    public static final byte OP_PATCH = (byte) 80;
    public static final boolean PRE_LOAD_SE = false;
    public static final byte SE_103 = (byte) 0;
    public static final byte SE_106 = (byte) 1;
    public static final byte SE_107 = (byte) 2;
    public static final byte SE_108 = (byte) 3;
    public static final byte SE_109 = (byte) 4;
    public static final byte SE_110 = (byte) 5;
    public static final byte SE_111 = (byte) 6;
    public static final byte SE_112 = (byte) 7;
    public static final byte SE_113 = (byte) 79;
    public static final byte SE_114_01 = (byte) 8;
    public static final byte SE_114_02 = (byte) 9;
    public static final byte SE_115 = (byte) 10;
    public static final byte SE_116 = (byte) 11;
    public static final byte SE_117 = (byte) 12;
    public static final byte SE_118 = (byte) 13;
    public static final byte SE_119 = (byte) 14;
    public static final byte SE_120 = (byte) 15;
    public static final byte SE_121 = (byte) 16;
    public static final byte SE_123 = (byte) 17;
    public static final byte SE_125 = (byte) 18;
    public static final byte SE_126 = (byte) 19;
    public static final byte SE_127 = (byte) 20;
    public static final byte SE_128 = (byte) 21;
    public static final byte SE_130 = (byte) 22;
    public static final byte SE_131 = (byte) 23;
    public static final byte SE_132 = (byte) 24;
    public static final byte SE_133 = (byte) 25;
    public static final byte SE_135 = (byte) 26;
    public static final byte SE_136 = (byte) 27;
    public static final byte SE_137 = (byte) 28;
    public static final byte SE_138 = (byte) 29;
    public static final byte SE_139 = (byte) 30;
    public static final byte SE_140 = (byte) 31;
    public static final byte SE_141 = (byte) 32;
    public static final byte SE_142 = (byte) 33;
    public static final byte SE_143 = (byte) 34;
    public static final byte SE_144 = (byte) 35;
    public static final byte SE_145 = (byte) 36;
    public static final byte SE_148 = (byte) 37;
    public static final byte SE_149 = (byte) 38;
    public static final byte SE_150 = (byte) 39;
    public static final byte SE_162 = (byte) 81;
    public static final byte SE_166 = (byte) 40;
    public static final byte SE_168 = (byte) 41;
    public static final byte SE_169 = (byte) 42;
    public static final byte SE_171 = (byte) 43;
    public static final byte SE_172 = (byte) 44;
    public static final byte SE_173 = (byte) 45;
    public static final byte SE_174 = (byte) 46;
    public static final byte SE_175 = (byte) 47;
    public static final byte SE_176 = (byte) 48;
    public static final byte SE_177 = (byte) 82;
    public static final byte SE_178 = (byte) 49;
    public static final byte SE_179 = (byte) 83;
    public static final byte SE_180 = (byte) 50;
    public static final byte SE_181_01 = (byte) 51;
    public static final byte SE_181_02 = (byte) 52;
    public static final byte SE_182 = (byte) 53;
    public static final byte SE_183 = (byte) 54;
    public static final byte SE_184 = (byte) 55;
    public static final byte SE_185 = (byte) 56;
    public static final byte SE_189 = (byte) 57;
    public static final byte SE_190 = (byte) 58;
    public static final byte SE_191 = (byte) 59;
    public static final byte SE_192 = (byte) 60;
    public static final byte SE_193 = (byte) 61;
    public static final byte SE_194 = (byte) 62;
    public static final byte SE_195 = (byte) 63;
    public static final byte SE_198_01 = (byte) 64;
    public static final byte SE_198_02 = (byte) 65;
    public static final byte SE_199_01 = (byte) 66;
    public static final byte SE_199_02 = (byte) 67;
    public static final byte SE_200_01 = (byte) 68;
    public static final byte SE_200_02 = (byte) 69;
    public static final byte SE_201_01 = (byte) 70;
    public static final byte SE_201_02 = (byte) 71;
    public static final byte SE_206 = (byte) 72;
    public static final byte SE_209 = (byte) 73;
    public static final byte SE_210 = (byte) 74;
    public static final byte SE_211 = (byte) 75;
    public static final byte SE_212 = (byte) 76;
    public static final byte SE_213 = (byte) 77;
    public static final byte SE_214 = (byte) 78;
    private static final String SE_PATH = "/se/";
    private static SoundSystem instance;
    private static int volume = 46;
    private final String[] BgmName = new String[]{"bgm_01_opening", "bgm_02_title", "bgm_03_player_select", "bgm_04_zoneselect", "bgm_10_ranking", "bgm_155_continue", "bgm_12_st1_1", "bgm_13_st1_2", "bgm_14_st2_1", "bgm_15_st2_2", "bgm_16_st3_1", "bgm_17_st3_2", "bgm_18_st4_1", "bgm_19_st4_2", "bgm_20_st5_1", "bgm_21_st5_2", "bgm_22_st6_1", "bgm_23_st6_2", "bgm_24_st_final", "bgm_25_boss_final3", "bgm_26_st_ex", "bgm_27_asphyxy", "bgm_29_boss_01", "bgm_30_boss_02", "bgm_31_boss_03", "bgm_32_boss_04", "bgm_33_clear_act1", "bgm_34_clear_act2", "bgm_35_clear_final", "bgm_36_clear_ex", "bgm_37_gameover", "bgm_38_ending_final", "bgm_39_ending_ex_01", "bgm_40_credit", "bgm_41_sp_intro", "bgm_42_sp", "bgm_43_sp_trick", "bgm_44_sp_clear", "bgm_45_sp_emerald", "bgm_46_sp_total_miss", "bgm_47_sp_total_clear", "bgm_305_NewRecord", "bgm_306_TimeAttackGoal", "bgm_307_1up", "bgm_28_invincibility", "bgm_39_ending_ex_02", "bgm_49_boss_f1", "bgm_50_boss_f2"};
    private final String[] BgmNameSpeeding = new String[]{"bgm_12_st1_1_speed", "bgm_13_st1_2_speed", "bgm_14_st2_1_speed", "bgm_15_st2_2_speed", "bgm_16_st3_1_speed", "bgm_17_st3_2_speed", "bgm_18_st4_1_speed", "bgm_19_st4_2_speed", "bgm_20_st5_1_speed", "bgm_21_st5_2_speed", "bgm_22_st6_1_speed", "bgm_23_st6_2_speed", "bgm_28_invincibility_speed"};
    private String BgmPrefix;
    private final String BgmType;
    private final int[][] INTRO_MSEC;
    private String Path;
    private final String[] SE_NAME;
    private final int[] VOLUME_OPTION;
    private int bgmIndex;
    private int currentIntroMsec;
    private int currentLoopMsec;
    private MFPlayer longSeplayer;
    private long mediaTime;
    private int nextBgmIndex;
    private boolean nextBgmLoop;
    private boolean nextBgmWaiting;
    private float preSpeed;
    private int seIndex;
    private MFPlayer seplayer;
    private float speed;


    public SoundSystem() {
        /*
        int[][] r0 = new int[48][];
        int[] iArr = new int[]{32000, iArr};
        r0[3] = new int[]{9433, 25433};
        r0[4] = new int[]{1432, 18132};
        r0[5] = new int[2];
        r0[6] = new int[]{1800, 73800};
        int[] iArr2 = new int[]{78800, iArr2};
        r0[8] = new int[]{7066, 91566};
        r0[9] = new int[]{7500, 70666};
        r0[10] = new int[]{4432, 90832};
        iArr2 = new int[]{72000, iArr2};
        r0[12] = new int[]{17000, 68432};
        iArr2 = new int[]{64066, iArr2};
        iArr2 = new int[]{71032, iArr2};
        iArr2 = new int[]{71032, iArr2};
        r0[16] = new int[]{12600, 70200};
        r0[17] = new int[]{5566, 78966};
        iArr2 = new int[]{88432, iArr2};
        iArr2 = new int[]{67200, iArr2};
        iArr2 = new int[]{78732, iArr2};
        r0[21] = new int[2];
        r0[22] = new int[]{3800, 48366};
        r0[23] = new int[]{3132, 57500};
        r0[24] = new int[]{6200, 56266};
        r0[25] = new int[]{31866, 123066};
        r0[26] = new int[2];
        r0[27] = new int[2];
        r0[28] = new int[2];
        r0[29] = new int[2];
        r0[30] = new int[2];
        r0[31] = new int[2];
        r0[32] = new int[2];
        r0[33] = new int[2];
        r0[34] = new int[2];
        r0[35] = new int[]{6167, 68133};
        r0[36] = new int[2];
        r0[37] = new int[2];
        r0[38] = new int[2];
        r0[39] = new int[2];
        r0[40] = new int[2];
        r0[41] = new int[2];
        r0[42] = new int[2];
        r0[43] = new int[2];
        r0[44] = new int[2];
        r0[45] = new int[2];
        iArr2 = new int[]{69833, iArr2};
        r0[47] = new int[]{17067, 51333};
        this.INTRO_MSEC = r0;
        **/
        int[] arrayOfInt1 = new int[2];
        int[] arrayOfInt2 = new int[2];
        int[] arrayOfInt3 = new int[2];
        arrayOfInt3[1] = 32000;
        int[] arrayOfInt4 = new int[2];
        int[] arrayOfInt5 = new int[2];
        arrayOfInt5[1] = 78800;
        int[] arrayOfInt6 = new int[2];
        arrayOfInt6[1] = 72000;
        int[] arrayOfInt7 = new int[2];
        arrayOfInt7[1] = 64066;
        int[] arrayOfInt8 = new int[2];
        arrayOfInt8[1] = 71032;
        int[] arrayOfInt9 = new int[2];
        arrayOfInt9[1] = 71032;
        int[] arrayOfInt10 = new int[2];
        arrayOfInt10[1] = 88432;
        int[] arrayOfInt11 = new int[2];
        arrayOfInt11[1] = 67200;
        int[] arrayOfInt12 = new int[2];
        arrayOfInt12[1] = 78732;
        int[] arrayOfInt13 = new int[2];
        int[] arrayOfInt14 = new int[2];
        int[] arrayOfInt15 = new int[2];
        int[] arrayOfInt16 = new int[2];
        int[] arrayOfInt17 = new int[2];
        int[] arrayOfInt18 = new int[2];
        int[] arrayOfInt19 = new int[2];
        int[] arrayOfInt20 = new int[2];
        int[] arrayOfInt21 = new int[2];
        int[] arrayOfInt22 = new int[2];
        int[] arrayOfInt23 = new int[2];
        int[] arrayOfInt24 = new int[2];
        int[] arrayOfInt25 = new int[2];
        int[] arrayOfInt26 = new int[2];
        int[] arrayOfInt27 = new int[2];
        int[] arrayOfInt28 = new int[2];
        int[] arrayOfInt29 = new int[2];
        int[] arrayOfInt30 = new int[2];
        int[] arrayOfInt31 = new int[2];
        int[] arrayOfInt32 = new int[2];
        int[] arrayOfInt33 = new int[2];
        arrayOfInt33[1] = 69833;
        int[] arrayOfInt34 = { 17067, 51333 };

        this.INTRO_MSEC = new int[][] { arrayOfInt1, arrayOfInt2, arrayOfInt3, { 9433, 25433 }, { 1432, 18132 }, arrayOfInt4, { 1800, 73800 }, arrayOfInt5, { 7066, 91566 }, { 7500, 70666 }, { 4432, 90832 }, arrayOfInt6, { 17000, 68432 }, arrayOfInt7, arrayOfInt8, arrayOfInt9, { 12600, 70200 }, { 5566, 78966 }, arrayOfInt10, arrayOfInt11, arrayOfInt12, arrayOfInt13, { 3800, 48366 }, { 3132, 57500 }, { 6200, 56266 }, { 31866, 123066 }, arrayOfInt14, arrayOfInt15, arrayOfInt16, arrayOfInt17, arrayOfInt18, arrayOfInt19, arrayOfInt20, arrayOfInt21, arrayOfInt22, { 6167, 68133 }, arrayOfInt23, arrayOfInt24, arrayOfInt25, arrayOfInt26, arrayOfInt27, arrayOfInt28, arrayOfInt29, arrayOfInt30, arrayOfInt31, arrayOfInt32, arrayOfInt33, arrayOfInt34 };


        this.BgmType = ".mid";
        this.SE_NAME = new String[]{"se_103.ogg", "se_106.wav", "se_107.wav", "se_108.wav", "se_109.ogg", "se_110.ogg", "se_111.ogg", "se_112.ogg", "se_114_01.ogg", "se_114_02.ogg", "se_115.ogg", "se_116.ogg", "se_117.ogg", "se_118.ogg", "se_119.ogg", "se_120.ogg", "se_121.ogg", "se_123.ogg", "se_125.ogg", "se_126.ogg", "se_127.ogg", "se_128.ogg", "se_130.ogg", "se_131.ogg", "se_132.ogg", "se_133.ogg", "se_135.ogg", "se_136.ogg", "se_137.ogg", "se_138.ogg", "se_139.ogg", "se_140.ogg", "se_141.wav", "se_142.ogg", "se_143.ogg", "se_144.ogg", "se_145.ogg", "se_148.ogg", "se_149.ogg", "se_150.ogg", "se_166.ogg", "se_168.ogg", "se_169.ogg", "se_171.ogg", "se_172.ogg", "se_173.ogg", "se_174.ogg", "se_175.ogg", "se_176.ogg", "se_178.ogg", "se_180.ogg", "se_181_01.ogg", "se_181_02.ogg", "se_182.ogg", "se_183.ogg", "se_184.ogg", "se_185.ogg", "se_189.ogg", "se_190.ogg", "se_191.ogg", "se_192.ogg", "se_193.ogg", "se_194.ogg", "se_195.ogg", "se_198_01.ogg", "se_198_02.ogg", "se_199_01.ogg", "se_199_02.ogg", "se_200_01.ogg", "se_200_02.ogg", "se_201_01.ogg", "se_201_02.ogg", "se_206.ogg", "se_209.ogg", "se_210.ogg", "se_211.ogg", "se_212.ogg", "se_213.ogg", "se_214.ogg", "se_113.ogg", "op_patch.ogg", "se_162.ogg", "se_177.ogg", "se_179.ogg"};
        this.Path = "/mid/";
        this.BgmPrefix = "";
        this.nextBgmWaiting = false;
        this.nextBgmIndex = 0;
        this.speed = 1.0f;
        this.preSpeed = 1.0f;
        this.mediaTime = 0;
        int[] iArr3 = new int[4];
        iArr3[1] = 25;
        iArr3[2] = 60;
        iArr3[3] = 95;
        this.VOLUME_OPTION = iArr3;
    }

    private int getSpeedyVersionBgm(int bgmID) {
        if (bgmID >= 6 && bgmID < 18) {
            return bgmID - 6;
        }
        if (bgmID == 44) {
            return 12;
        }
        return -1;
    }

    public static void open() {
        instance = new SoundSystem();
        instance.openImpl();
    }

    public static SoundSystem getInstance() {
        if (instance == null) {
            open();
        }
        return instance;
    }

    private void openImpl() {
        this.nextBgmWaiting = false;
    }

    public void exec() {
        if (this.nextBgmWaiting && !bgmPlaying()) {
            this.nextBgmWaiting = false;
            if (!PlayerObject.isTerminal) {
                playBgm(this.nextBgmIndex, this.nextBgmLoop);
            }
        }
        if (!this.nextBgmWaiting && this.currentLoopMsec > 0) {
            MFPlayer currentBGM = MFSound.getCurrentBgm();
            if (currentBGM != null && currentBGM.getState() == 3) {
                if (PlayerObject.IsSpeedUp()) {
                    if (currentBGM.getMediaTime() >= ((long) (this.currentLoopMsec >> 1))) {
                        currentBGM.setMediaTime((int) (((long) (this.currentIntroMsec >> 1)) + (currentBGM.getMediaTime() - ((long) (this.currentLoopMsec >> 1)))));
                    }
                } else if (currentBGM.getMediaTime() >= ((long) this.currentLoopMsec)) {
                    currentBGM.setMediaTime((int) (((long) this.currentIntroMsec) + (currentBGM.getMediaTime() - ((long) this.currentLoopMsec))));
                }
            }
        }
    }

    public void playBgmSequence(int index0, int index1) {
        playBgm(index0, false);
        this.nextBgmWaiting = true;
        this.nextBgmIndex = index1;
        this.nextBgmLoop = true;
    }

    public void playBgmFromTime(long startTime, int index0) {
        this.bgmIndex = index0;
        String fileName = this.Path.concat(this.BgmPrefix).concat(new StringBuilder(String.valueOf(this.BgmName[index0])).append(".mid").toString());
        System.out.println(fileName);
        MFSound.playBgm(fileName, false);
        MFSound.getCurrentBgm().setMediaTime((int) startTime);
    }

    public void playBgmSequenceNoLoop(int index0, int index1) {
        playBgm(index0, false);
        this.nextBgmWaiting = true;
        this.nextBgmIndex = index1;
        this.nextBgmLoop = false;
    }

    public void playBgm(int index) {
        playBgm(index, true);
    }

    public void playNextBgm(int index) {
        this.nextBgmWaiting = true;
        this.nextBgmIndex = index;
        this.nextBgmLoop = true;
    }

    public int getPlayingBGMIndex() {
        return this.bgmIndex;
    }

    public String getBgmName(int index) {
        return this.BgmName[index];
    }

    public void setSoundSpeed(float speed) {
        this.preSpeed = this.speed;
        this.speed = speed;
    }

    public void playBgm(int index, boolean loop) {
        playBgmInSpeed(index, loop, this.speed);
    }

    public void restartBgm() {
        this.mediaTime = MFSound.getBgmMediaTime();
        System.out.println("mediaTime get:" + this.mediaTime);
        this.mediaTime = (long) ((((float) this.mediaTime) * this.preSpeed) / this.speed);
        MFSound.stopBgm();
        playBgm(this.bgmIndex, true);
    }

    public void playBgmInSpeed(int index, boolean loop, float speed) {
        this.currentIntroMsec = this.INTRO_MSEC[index][0];
        this.currentLoopMsec = this.INTRO_MSEC[index][1];
        if (this.currentLoopMsec > 0) {
            loop = false;
        }
        if (speed == 1.0f || getSpeedyVersionBgm(index) < 0) {
            playBgmInNormalSpeed(index, loop);
        } else {
            playBgmInInnormalSpeed(index, loop, speed);
        }
    }

    private void playBgmInNormalSpeed(int index, boolean loop) {
        this.bgmIndex = index;
        String fileName = this.Path.concat(this.BgmPrefix).concat(new StringBuilder(String.valueOf(this.BgmName[index])).append(".mid").toString());
        System.out.println(fileName);
        MFSound.playBgm(fileName, loop);
        System.out.println("play bgm");
        if (this.mediaTime > 0) {
            MFSound.getCurrentBgm().setMediaTime((int) this.mediaTime);
            System.out.println("mediaTime:" + this.mediaTime);
            this.mediaTime = 0;
        }
    }

    private void playBgmInInnormalSpeed(int index, boolean loop, float speed) {
        this.bgmIndex = index;
        String fileName = this.Path.concat(this.BgmPrefix).concat(new StringBuilder(String.valueOf(this.BgmName[index])).append(".mid").toString());
        if (getSpeedyVersionBgm(this.bgmIndex) >= 0) {
            MFPlayer a = MFPlayer.createMFPlayer(this.Path.concat(this.BgmPrefix).concat(new StringBuilder(String.valueOf(this.BgmNameSpeeding[getSpeedyVersionBgm(this.bgmIndex)])).append(".mid").toString()));
            MFSound.setBgm(a, loop);
            if (this.mediaTime > 0) {
                a.setMediaTime((int) this.mediaTime);
                System.out.println("~~mediaTime:" + this.mediaTime);
                this.mediaTime = 0;
            }
        }
    }

    public void stopBgm(boolean isDel) {
        MFSound.stopBgm();
        this.nextBgmWaiting = false;
    }

    public void resumeBgm() {
        MFSound.resumeBgm();
    }

    public void playSe(int index, boolean loop) {
        stopLoopSe();
        MFSound.playSe(new StringBuilder(SE_PATH).append(this.SE_NAME[index]).toString(), 1);
    }

    public void playSe(int index) {
        playSe(index, false);
    }

    public void playLongSe(int index) {
        if (MFSound.getSeFlag()) {
            this.longSeplayer = MFPlayer.createMFPlayer(new StringBuilder(SE_PATH).append(this.SE_NAME[index]).toString());
            this.longSeplayer.realize();
            this.longSeplayer.prefetch();
            this.longSeplayer.setLoop(false);
            if (volume == 0) {
                this.longSeplayer.setVolume(0);
            } else {
                this.longSeplayer.setVolume(100);
            }
            this.longSeplayer.start();
        }
    }

    public void stopLongSe() {
        if (this.longSeplayer != null) {
            this.longSeplayer.stop();
            this.longSeplayer.deallocate();
            this.longSeplayer.close();
            this.longSeplayer = null;
        }
    }

    public void playLoopSe(int index) {
        if (MFSound.getSeFlag() && !isLoopSePlaying()) {
            this.seplayer = null;
            this.seplayer = MFPlayer.createMFPlayer(new StringBuilder(SE_PATH).append(this.SE_NAME[index]).toString());
            this.seIndex = index;
            this.seplayer.realize();
            this.seplayer.prefetch();
            this.seplayer.setLoop(true);
            if (volume == 0) {
                this.seplayer.setVolume(0);
            } else {
                this.seplayer.setVolume(100);
            }
            this.seplayer.start();
        }
    }

    public void playSequenceSe(int index) {
        if (MFSound.getSeFlag() && !isLoopSePlaying()) {
            if (this.seplayer != null) {
                this.seplayer.stop();
                this.seplayer.close();
            }
            this.seplayer = null;
            this.seplayer = MFPlayer.createMFPlayer(new StringBuilder(SE_PATH).append(this.SE_NAME[index]).toString());
            this.seIndex = index;
            this.seplayer.realize();
            this.seplayer.prefetch();
            this.seplayer.setLoop(false);
            if (volume == 0) {
                this.seplayer.setVolume(0);
            } else {
                this.seplayer.setVolume(100);
            }
            this.seplayer.start();
        }
    }

    public void preLoadSequenceSe(int index) {
        if (MFSound.getSeFlag() && !isLoopSePlaying()) {
            this.seplayer = null;
            this.seplayer = MFPlayer.createMFPlayer(new StringBuilder(SE_PATH).append(this.SE_NAME[index]).toString());
            this.seIndex = index;
            this.seplayer.realize();
            this.seplayer.prefetch();
            this.seplayer.setLoop(false);
            if (volume == 0) {
                this.seplayer.setVolume(0);
            } else {
                this.seplayer.setVolume(100);
            }
        }
    }

    public void playSequenceSeSingle() {
        if (MFSound.getSeFlag() && !isLoopSePlaying()) {
            this.seplayer.start();
        }
    }

    public void stopLoopSe() {
        if (MFSound.getSeFlag() && isLoopSePlaying()) {
            this.seplayer.stop();
            this.seplayer.deallocate();
            this.seplayer.close();
            this.seplayer = null;
        }
    }

    public void resumeLoopSe() {
        if (MFSound.getSeFlag()) {
            this.seplayer = null;
            this.seplayer = MFPlayer.createMFPlayer(new StringBuilder(SE_PATH).append(this.SE_NAME[this.seIndex]).toString());
            this.seplayer.realize();
            this.seplayer.prefetch();
            this.seplayer.setLoop(true);
            if (volume == 0) {
                this.seplayer.setVolume(0);
            } else {
                this.seplayer.setVolume(100);
            }
            this.seplayer.start();
        }
    }

    public int getPlayingLoopSeIndex() {
        return this.seIndex;
    }

    public boolean isLoopSePlaying() {
        if (!MFSound.getSeFlag()) {
            return false;
        }
        if (this.seplayer == null) {
            return false;
        }
        if (this.seplayer.getState() == 3) {
            return true;
        }
        return false;
    }

    public void setVolume(int vol) {
        MFSound.setLevel(vol);
    }

    public boolean getBgmFlag() {
        return MFSound.getBgmFlag();
    }

    public void setBgmFlag(boolean flag) {
        MFSound.setBgmFlag(flag);
    }

    public boolean getSeFlag() {
        return MFSound.getSeFlag();
    }

    public void setSeFlag(boolean flag) {
        MFSound.setSeFlag(flag);
    }

    public boolean bgmPlaying() {
        return MFSound.isBgmPlaying();
    }

    public boolean bgmPlaying2() {
        return MFSound.isBgmPlaying();
    }

    public void setVolumnState(int state) {
        if (state == 0) {
            setSeFlag(false);
        } else {
            setSeFlag(true);
        }
        if (state >= 0 && state <= 15) {
            setBgmFlag(true);
        }
        volume = state;
    }

    public void updateVolumeState() {
        GlobalResource.soundConfig = MFSound.getLevel();
        if (GlobalResource.soundConfig == 0) {
            GlobalResource.seConfig = 0;
        }
        getInstance().setSoundState(GlobalResource.soundConfig);
        getInstance().setSeState(GlobalResource.seConfig);
    }

    public void setSoundState(int state) {
        if (state == 0) {
            setSeFlag(false);
        } else {
            setSeFlag(true);
        }
        if (state >= 0 && state <= 15) {
            setBgmFlag(true);
            setVolume(state);
        }
        volume = state;
    }

    public void setSeState(int state) {
        switch (state) {
            case 0:
                setSeFlag(false);
                return;
            case 1:
                setSeFlag(true);
                return;
            default:
                return;
        }
    }

    public void preLoadAllSe() {
        MFSound.releaseAllSound();
        for (String append : this.SE_NAME) {
            MFSound.preloadSound(new StringBuilder(SE_PATH).append(append).toString());
        }
    }
}
