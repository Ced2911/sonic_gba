package Lib;

import SonicGBA.GlobalResource;
import SonicGBA.MapManager;
import SonicGBA.RollPlatformSpeedC;
import Special.SSDef;
import com.sega.mobile.define.MDPhone;
import com.sega.mobile.framework.android.Graphics;
import com.sega.mobile.framework.device.MFDevice;
import com.sega.mobile.framework.device.MFGraphics;
import com.sega.mobile.framework.device.MFImage;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.util.Vector;

public class MyAPI implements Def {
    private static final int[] ARROW = new int[]{1, 3, 5, 7, 7};
    public static final int BMF_COLOR_GRAY = 3;
    public static final int BMF_COLOR_GREEN = 2;
    public static final int BMF_COLOR_WHITE = 0;
    public static final int BMF_COLOR_YELLOW = 1;
    public static final int FIXED_TWO_BASE = 7;
    private static final int FLIP_X = 2;
    private static final int FLIP_Y = 4;
    private static final int[] GRAY_PALETTE_PARAM = new int[]{16711935, 15263976, 15263976, 14211288};
    private static final int[] GREEN_PALETTE_PARAM;
    public static final boolean NOKIA_DRAW = false;
    private static final int[][] OFFSET;
    private static final int[][] OFFSET2;
    private static final int PAGE_WAIT = 5;
    private static final int RAY_HEIGHT = 3;
    private static final int RAY_WIDTH = 288;
    public static final boolean RGB_DRAW = false;
    private static final int ROTATE_90 = 1;
    private static final char[] Symbol_CH = new char[]{'。', '，', '！', ':', '；'};
    private static final char[] Symbol = Symbol_CH;
    private static final char[] Symbol_EN = new char[]{'.', ',', '!', ':', ';'};
    private static final short[] TRANMODIF;
    private static final byte[] TRANMODIF_2;
    private static final int[] YELLOW_PALETTE_PARAM;
    public static final int ZOOM_OUT_MOVE = 0;
    public static String anchor = "";
    public static int backColor = 0;
    public static BitmapFont bmFont;
    public static BitmapFont bmFontGray;
    public static BitmapFont bmFontGreen;
    public static BitmapFont bmFontYellow;
    public static int borderColor = 0;
    private static BitmapFont currentBmFont;
    public static boolean downPermit;
    private static int[] rayRGB = new int[864];
    public static int scrollPageWait;
    public static final int[] sinData2;
    public static int stringCursol = 0;
    public static boolean upPermit;

    static {
        int[] iArr = new int[91];
        iArr[1] = 224;
        iArr[2] = 446;
        iArr[3] = 669;
        iArr[4] = 893;
        iArr[5] = 1116;
        iArr[6] = 1337;
        iArr[7] = 1560;
        iArr[8] = 1781;
        iArr[9] = 2001;
        iArr[10] = 2222;
        iArr[11] = 2442;
        iArr[12] = 2661;
        iArr[13] = 2878;
        iArr[14] = 3096;
        iArr[15] = 3312;
        iArr[16] = 3527;
        iArr[17] = 3742;
        iArr[18] = 3955;
        iArr[19] = 4167;
        iArr[20] = 4377;
        iArr[21] = 4587;
        iArr[22] = 4794;
        iArr[23] = 5000;
        iArr[24] = 5205;
        iArr[25] = 5409;
        iArr[26] = 5611;
        iArr[27] = 5811;
        iArr[28] = 6009;
        iArr[29] = 6205;
        iArr[30] = 6400;
        iArr[31] = 6592;
        iArr[32] = 6782;
        iArr[33] = 6970;
        iArr[34] = 7157;
        iArr[35] = 7342;
        iArr[36] = 7523;
        iArr[37] = 7703;
        iArr[38] = 7879;
        iArr[39] = 8055;
        iArr[40] = 8227;
        iArr[41] = 8396;
        iArr[42] = 8564;
        iArr[43] = 8729;
        iArr[44] = 8890;
        iArr[45] = 9050;
        iArr[46] = 9207;
        iArr[47] = 9360;
        iArr[48] = 9511;
        iArr[49] = 9660;
        iArr[50] = 9804;
        iArr[51] = 9946;
        iArr[52] = 10086;
        iArr[53] = 10222;
        iArr[54] = 10355;
        iArr[55] = 10484;
        iArr[56] = 10611;
        iArr[57] = 10735;
        iArr[58] = 10854;
        iArr[59] = 10972;
        iArr[60] = 11084;
        iArr[61] = 11194;
        iArr[62] = 11301;
        iArr[63] = 11404;
        iArr[64] = 11504;
        iArr[65] = 11600;
        iArr[66] = 11692;
        iArr[67] = 11782;
        iArr[68] = 11868;
        iArr[69] = 11950;
        iArr[70] = 12028;
        iArr[71] = 12102;
        iArr[72] = 12172;
        iArr[73] = 12240;
        iArr[74] = 12304;
        iArr[75] = 12363;
        iArr[76] = 12419;
        iArr[77] = 12472;
        iArr[78] = 12519;
        iArr[79] = 12564;
        iArr[80] = 12605;
        iArr[81] = 12642;
        iArr[82] = 12675;
        iArr[83] = 12704;
        iArr[84] = 12729;
        iArr[85] = 12751;
        iArr[86] = 12769;
        iArr[87] = 12782;
        iArr[88] = 12792;
        iArr[89] = 12797;
        iArr[90] = 12800;
        sinData2 = iArr;
        /*
        int[] iArr2 = new int[]{-1, iArr2};
        iArr2 = new int[]{1, iArr2};
        iArr2 = new int[]{1, iArr2};
        int[] iArr3 = new int[]{-1, iArr3};
        r0 = new int[8][];
        iArr2 = new int[]{-1, iArr2};
        iArr2 = new int[]{1, iArr2};
        iArr2 = new int[]{1, iArr2};
        iArr3 = new int[]{-1, iArr3};
        r0[4] = new int[]{-1, 1};
        r0[5] = new int[]{1, 1};
        r0[6] = new int[]{1, -1};
        r0[7] = new int[]{-1, -1};
        OFFSET2 = r0;
        */

        int[] localObject = new int[2];
        localObject[0] = -1;
        int[] arrayOfInt1 = new int[2];
        arrayOfInt1[1] = 1;
        int[] arrayOfInt2 = new int[2];
        arrayOfInt2[0] = 1;
        int[] arrayOfInt3 = new int[2];
        arrayOfInt3[1] = -1;
        OFFSET = new int[][] { localObject, arrayOfInt1, arrayOfInt2, arrayOfInt3 };

        localObject = new int[2];
        localObject[0] = -1;
        arrayOfInt1 = new int[2];
        arrayOfInt1[1] = 1;
        arrayOfInt2 = new int[2];
        arrayOfInt2[0] = 1;
        arrayOfInt3 = new int[2];
        arrayOfInt3[1] = -1;
        int[] arrayOfInt4 = { -1, 1 };
        int[] arrayOfInt5 = { -1, -1 };
        OFFSET2 = new int[][] { localObject, arrayOfInt1, arrayOfInt2, arrayOfInt3, arrayOfInt4, { 1, 1 }, { 1, -1 }, arrayOfInt5 };


        System.out.println("OFFSET2 = r0;");
        iArr = new int[4];
        iArr[0] = MapManager.END_COLOR;
        iArr[1] = 16763904;
        iArr[2] = 16776960;
        YELLOW_PALETTE_PARAM = iArr;
        iArr = new int[4];
        iArr[0] = MapManager.END_COLOR;
        iArr[1] = 776448;
        iArr[2] = 65280;
        GREEN_PALETTE_PARAM = iArr;
        short[] sArr = new short[8];
        sArr[1] = (short) 90;
        sArr[2] = Const.FLIP_X;
        sArr[3] = (short) 16474;
        sArr[4] = Const.FLIP_Y;
        sArr[5] = (short) 16654;
        sArr[6] = (short) 180;
        sArr[7] = (short) 270;
        TRANMODIF = sArr;
        byte[] bArr = new byte[8];
        bArr[1] = (byte) 4;
        bArr[2] = (byte) 2;
        bArr[3] = (byte) 6;
        bArr[4] = (byte) 3;
        bArr[5] = (byte) 7;
        bArr[6] = (byte) 1;
        bArr[7] = (byte) 5;
        TRANMODIF_2 = bArr;
    }

    public static final void drawRegion(MFGraphics g2, MFImage img, int sx, int sy, int sw, int sh, int trans, int dx, int dy, int anchor) {
        drawImage(g2, img, sx, sy, sw, sh, trans, dx, dy, anchor);
    }

    public static final void drawImage(MFGraphics g2, MFImage img, int sx, int sy, int sw, int sh, int trans, int dx, int dy, int anchor) {
        drawRegionPrivate(g2, img, zoomOut(sx), zoomOut(sy), zoomOut(sw), zoomOut(sh), trans, zoomOut(dx), zoomOut(dy), anchor);
    }

    public static final void drawImageWithoutZoom(MFGraphics g2, MFImage img, int sx, int sy, int sw, int sh, int trans, int dx, int dy, int anchor) {
        drawRegionPrivate(g2, img, sx, sy, sw, sh, trans, dx, dy, anchor);
    }

    public static final void drawImageWithoutZoom(MFGraphics g2, MFImage img, int x, int y, int anchor) {
        g2.drawImage(img, x, y, anchor);
    }

    private static final void drawRegionPrivate(MFGraphics g2, MFImage img, int sx, int sy, int sw, int sh, int trans, int dx, int dy, int anchor) {
        g2.drawRegion(img, sx, sy, sw, sh, trans, dx, dy, anchor);
    }

    private static final Coordinate getRoteCoordinate(int x, int y, int trans) {
        switch (trans) {
            case 3:
                return Coordinate.returnCoordinate(-x, -y);
            case 5:
                return Coordinate.returnCoordinate(y, -x);
            case 6:
                return Coordinate.returnCoordinate(-y, x);
            default:
                return Coordinate.returnCoordinate(x, y);
        }
    }

    public static final void drawImage(MFGraphics g2, MFImage img, int x, int y, int anchor) {
        g2.drawImage(img, zoomOut(x), zoomOut(y), anchor);
    }

    public static final String getStringToDraw(String string) {
        String stringToDraw = "";
        if (string.indexOf("<") == -1 || string.indexOf(">") == -1) {
            return string;
        }
        anchor = string.substring(string.indexOf("<") + 2, string.indexOf(">"));
        return string.substring(string.indexOf(">") + 1);
    }

    public static final void fillRectBold(MFGraphics g2, int x, int y, int w, int h) {
        g2.setColor(backColor);
        g2.fillRect(x, y, w, h);
        g2.setColor(borderColor);
        g2.drawRect(x, y, w, h);
        g2.drawRect(x - 1, y - 1, w + 2, h + 2);
    }

    public static final void fillRoundRectBold(MFGraphics g2, int x, int y, int w, int h, int color1, int color2) {
        x = zoomOut(x);
        y = zoomOut(y);
        w = zoomOut(w);
        h = zoomOut(h);
        g2.setColor(color1);
        g2.fillRoundRect(x, y, w, h, 10, 10);
        g2.setColor(color2);
        g2.drawRoundRect(x, y, w - 1, h, 10, 10);
    }

    public static final void drawRectBold(MFGraphics g2, int x, int y, int w, int h) {
        g2.setColor(backColor);
        g2.drawRect(x, y, w, h);
        g2.drawRect(x - 1, y - 1, w + 2, h + 2);
        g2.drawRect(x + 1, y + 1, w - 2, h - 2);
        g2.setColor(borderColor);
        g2.drawRect(x - 2, y - 2, w + 4, h + 4);
        g2.drawRect(x + 2, y + 2, w - 4, h - 4);
    }

    public static final void drawRect(MFGraphics g2, int x, int y, int w, int h) {
        g2.drawRect(zoomOut(x), zoomOut(y), zoomOut(w), zoomOut(h));
    }

    public static final void fillRect(MFGraphics g2, int x, int y, int w, int h) {
        g2.fillRect(zoomOut(x), zoomOut(y), zoomOut(w), zoomOut(h));
    }

    public static final void drawString(MFGraphics g2, String a, int x, int y, int anchor) {
        g2.drawString(a, zoomOut(x), zoomOut(y), anchor);
    }

    public static final void drawSubstring(MFGraphics g2, String a, int start, int end, int x, int y, int anchor) {
        x = zoomOut(x);
        y = zoomOut(y);
    }

    public static final void fillArc(MFGraphics g2, int x, int y, int w, int h, int start, int thita) {
        g2.fillArc(zoomOut(x), zoomOut(y), zoomOut(w), zoomOut(h), start, thita);
    }

    public static final void drawArc(MFGraphics g2, int x, int y, int w, int h, int start, int thita) {
        g2.fillArc(zoomOut(x), zoomOut(y), zoomOut(w), zoomOut(h), start, thita);
    }

    public static final void setClip(MFGraphics g2, int x, int y, int w, int h) {
        g2.setClip(zoomOut(x), zoomOut(y), zoomOut(w), zoomOut(h));
    }

    public static final void drawLine(MFGraphics g2, int x, int y, int x2, int y2) {
        g2.drawLine(zoomOut(x), zoomOut(y), zoomOut(x2), zoomOut(y2));
    }

    public static void setBackColor(int color) {
        backColor = color;
    }

    public static void setBorderColor(int color) {
        borderColor = color;
    }

    public static void drawSelectBox(MFGraphics g2, Object[] selector, int x, int y, int w, int maxLine, int cursol) {
        fillRectBold(g2, x, y, w, LINE_SPACE * maxLine);
        if (selector.length == 0) {
            g2.drawString("无可选项", x + (w / 2), y + ((LINE_SPACE * maxLine) / 2), 17);
        } else if (selector.length <= maxLine) {
            for (w = 0; w < selector.length; w++) {
                if (cursol == w) {
                    g2.setColor(16711680);
                } else {
                    g2.setColor(MapManager.END_COLOR);
                }
                g2.drawString(selector[w].toString(), x + 8, (LINE_SPACE * w) + y, 20);
            }
        } else {
            int startFrom;
            int scrollBarHeight = ((LINE_SPACE * maxLine) * maxLine) / selector.length;
            if (cursol < maxLine / 2) {
                startFrom = 0;
            } else if ((maxLine / 2) + cursol < selector.length) {
                startFrom = cursol - (maxLine / 2);
            } else {
                startFrom = selector.length - maxLine;
            }
            for (int i = 0; i < maxLine; i++) {
                if (cursol == i + startFrom) {
                    g2.setColor(16711680);
                } else {
                    g2.setColor(MapManager.END_COLOR);
                }
                g2.drawString(selector[i + startFrom].toString(), x + 8, (LINE_SPACE * i) + y, 20);
            }
            g2.setColor(MapManager.END_COLOR);
            g2.fillRect(w - 8, y, 8, LINE_SPACE * maxLine);
            g2.setColor(8421504);
            g2.fillRect(w - 7, (((LINE_SPACE * maxLine) * startFrom) / selector.length) + y, 6, scrollBarHeight);
            g2.setColor(MapManager.END_COLOR);
            g2.drawRect(w - 8, y, 7, (LINE_SPACE * maxLine) - 1);
        }
    }

    public static String[] getStrings(String s, int lineLength) {
        lineLength = zoomOut(lineLength);
        if (lineLength > 8) {
            lineLength -= 8;
        }
        Vector strings = new Vector();
        String answerWord = "";
        boolean function = false;
        int currentPosition = 0;
        int endOfCurrentWord = 0;
        int ConcealEnterPosition = 0;
        int startOfLine = 0;
        int startOfLine2;
        boolean isSpace = false;
        while (endOfCurrentWord >= 0 && endOfCurrentWord < s.length()) {
            endOfCurrentWord++;
            String nextWord = s.substring(currentPosition, endOfCurrentWord);
            if (nextWord.equals("^")) {
                ConcealEnterPosition = currentPosition - startOfLine;
                isSpace = false;
            } else if (nextWord.equals("|") || nextWord.equals("\n")) {
                ConcealEnterPosition = 0;
                startOfLine2 = endOfCurrentWord;
                strings.addElement(answerWord);
                answerWord = "";
                startOfLine = startOfLine2;
                function = false;
            } else {
                answerWord = new StringBuilder(String.valueOf(answerWord)).append(nextWord).toString();
                if (nextWord.equals("<")) {
                    function = true;
                    currentPosition = endOfCurrentWord;
                } else {
                    currentPosition = 0;
                    while (currentPosition < Symbol.length) {
                        if (nextWord.charAt(0) == Symbol[currentPosition]) {
                            currentPosition = endOfCurrentWord;
                            if (endOfCurrentWord == s.length()) {
                                strings.addElement(answerWord);
                            }
                        } else {
                            currentPosition++;
                        }
                    }
                }
            }
            currentPosition = getStringWidth(14, answerWord);
            if (function) {
                currentPosition -= getStringWidth(14, "<H>");
            }
            if (currentPosition >= lineLength && endOfCurrentWord < s.length()) {
                if (ConcealEnterPosition == 0) {
                    try {
                        strings.addElement(answerWord.substring(0, answerWord.length() - 1));
                        answerWord = answerWord.substring(answerWord.length() - 1);
                        startOfLine2 = endOfCurrentWord - 1;
                    } catch (Exception e) {
                        SystemOut("answerWord" + answerWord);
                        startOfLine2 = startOfLine;
                    }
                } else {
                    try {
                        strings.addElement(answerWord.substring(0, ConcealEnterPosition));
                        answerWord = answerWord.substring((isSpace ? 1 : 0) + ConcealEnterPosition);
                        currentPosition = (ConcealEnterPosition + (isSpace ? 1 : 0)) + startOfLine;
                    } catch (Exception e2) {
                        SystemOut("answerWord" + answerWord);
                        SystemOut("ConcealEnterPosition" + ConcealEnterPosition);
                        currentPosition = startOfLine;
                    }
                    ConcealEnterPosition = 0;
                    startOfLine2 = currentPosition;
                }
                startOfLine = startOfLine2;
                function = false;
            } else if (endOfCurrentWord == s.length()) {
                strings.addElement(answerWord);
            }
            currentPosition = endOfCurrentWord;
        }
        /*
        s = new String[strings.size()];
        strings.copyInto(s);
        return s;
        */
        String[] rs = new String[strings.size()];
        strings.copyInto(rs);
        return rs;
    }

    public static String[] getStrings(String s, int fontid, int lineLength) {
        lineLength = zoomOut(lineLength);
        if (lineLength > 8) {
            lineLength -= 8;
        }
        Vector strings = new Vector();
        String answerWord = "";
        boolean function = false;
        int currentPosition = 0;
        int endOfCurrentWord = 0;
        int ConcealEnterPosition = 0;
        int startOfLine = 0;
        int startOfLine2;
        boolean isSpace = false;
        while (endOfCurrentWord >= 0 && endOfCurrentWord < s.length()) {
            endOfCurrentWord++;
            String nextWord = s.substring(currentPosition, endOfCurrentWord);
            if (nextWord.equals("^")) {
                ConcealEnterPosition = currentPosition - startOfLine;
                isSpace = false;
            } else if (nextWord.equals("|") || nextWord.equals("\n")) {
                ConcealEnterPosition = 0;
                startOfLine2 = endOfCurrentWord;
                strings.addElement(answerWord);
                answerWord = "";
                startOfLine = startOfLine2;
                function = false;
            } else {
                answerWord = new StringBuilder(String.valueOf(answerWord)).append(nextWord).toString();
                if (nextWord.equals("<")) {
                    function = true;
                    currentPosition = endOfCurrentWord;
                } else {
                    currentPosition = 0;
                    while (currentPosition < Symbol.length) {
                        if (nextWord.charAt(0) == Symbol[currentPosition]) {
                            currentPosition = endOfCurrentWord;
                            if (endOfCurrentWord == s.length()) {
                                strings.addElement(answerWord);
                            }
                        } else {
                            currentPosition++;
                        }
                    }
                }
            }
            currentPosition = getStringWidth(fontid, answerWord);
            if (function) {
                currentPosition -= getStringWidth(fontid, "<H>");
            }
            if (currentPosition >= lineLength && endOfCurrentWord < s.length()) {
                if (ConcealEnterPosition == 0) {
                    try {
                        strings.addElement(answerWord.substring(0, answerWord.length() - 1));
                        answerWord = answerWord.substring(answerWord.length() - 1);
                        startOfLine2 = endOfCurrentWord - 1;
                    } catch (Exception e) {
                        SystemOut("answerWord" + answerWord);
                        startOfLine2 = startOfLine;
                    }
                } else {
                    try {
                        strings.addElement(answerWord.substring(0, ConcealEnterPosition));
                        answerWord = answerWord.substring((isSpace ? 1 : 0) + ConcealEnterPosition);
                        currentPosition = (ConcealEnterPosition + (isSpace ? 1 : 0)) + startOfLine;
                    } catch (Exception e2) {
                        SystemOut("answerWord" + answerWord);
                        SystemOut("ConcealEnterPosition" + ConcealEnterPosition);
                        currentPosition = startOfLine;
                    }
                    ConcealEnterPosition = 0;
                    startOfLine2 = currentPosition;
                }
                startOfLine = startOfLine2;
                function = false;
            } else if (endOfCurrentWord == s.length()) {
                strings.addElement(answerWord);
            }
            currentPosition = endOfCurrentWord;
        }
        String[] rs = new String[strings.size()];
        strings.copyInto(rs);
        return rs;
    }

    public static String[] getEnStrings(String s, int lineLength) {
        Vector strings = new Vector();
        String answerWord = "";
        int currentPosition = 0;
        int endOfCurrentWord = 0;
        int startOfLine = 0;
        while (endOfCurrentWord >= 0 && endOfCurrentWord < s.length()) {
            endOfCurrentWord++;
            String nextWord = s.substring(currentPosition, endOfCurrentWord);
            if (nextWord.equals("^") || nextWord.equals(" ")) {
                int ConcealEnterPosition = currentPosition - startOfLine;
            } else if (nextWord.equals("|") || nextWord.equals("\n")) {
                startOfLine = endOfCurrentWord;
                strings.addElement(answerWord);
                answerWord = "";
            } else {
                answerWord = new StringBuilder(String.valueOf(answerWord)).append(nextWord).toString();
                if (!nextWord.equals("<")) {
                    int i = 0;
                    while (i < Symbol.length) {
                        if (nextWord.charAt(0) == Symbol[i]) {
                            currentPosition = endOfCurrentWord;
                            if (endOfCurrentWord == s.length()) {
                                strings.addElement(answerWord);
                            }
                        } else {
                            i++;
                        }
                    }
                }
            }
        }
        String[] stringsToDraw = new String[strings.size()];
        strings.copyInto(stringsToDraw);
        return stringsToDraw;
    }

    public static int getStringWidth(int fontID, String answerWord) {
        return MFGraphics.stringWidth(fontID, answerWord);
    }

    public static Object[] getArray(Vector vec) {
        if (vec.size() == 0) {
            return null;
        }
        Object[] re = new Object[vec.size()];
        vec.copyInto(re);
        return re;
    }

    public static void SystemOut(String a) {
    }

    public static void FillQua(MFGraphics g2, int x0, int y0, int x1, int y1, int x2, int y2, int x3, int y3) {
        g2.fillTriangle(x0, y0, x1, y1, x2, y2);
        g2.fillTriangle(x0, y0, x2, y2, x3, y3);
        g2.fillTriangle(x0, y0, x1, y1, x3, y3);
    }

    public static int dSin(int tDeg) {
        while (tDeg < 0) {
            tDeg += MDPhone.SCREEN_WIDTH;
        }
        int tsh = tDeg % MDPhone.SCREEN_WIDTH;
        if (tsh >= 0 && tsh <= 90) {
            return sinData2[tsh] >>> 7;
        }
        if (tsh > 90 && tsh <= RollPlatformSpeedC.DEGREE_VELOCITY) {
            return sinData2[90 - (tsh - 90)] >>> 7;
        }
        if (tsh > RollPlatformSpeedC.DEGREE_VELOCITY && tsh <= 270) {
            return (sinData2[tsh - RollPlatformSpeedC.DEGREE_VELOCITY] >>> 7) * -1;
        }
        if (tsh <= 270 || tsh > 359) {
            return 0;
        }
        return (sinData2[90 - (tsh - 270)] >>> 7) * -1;
    }

    public static int dCos(int tDeg) {
        return dSin(90 - tDeg);
    }

    public static String getTypeName(String fileName, String type) {
        String re = "";
        if (fileName.indexOf(".") != -1) {
            return fileName.substring(0, fileName.indexOf(".")) + type;
        }
        return new StringBuilder(String.valueOf(fileName)).append(type).toString();
    }

    public static String[] loadString(String filename) {
        Throwable th;
        DataInputStream in = null;
        String[] outdata = null;
        try {
            DataInputStream in2 = new DataInputStream(MFDevice.getResourceAsStream(filename));
            try {
                outdata = new String[in2.readInt()];
                for (int i = 0; i < outdata.length; i++) {
                    outdata[i] = in2.readUTF();
                }
                if (in2 != null) {
                    try {
                        in2.close();
                    } catch (Exception e) {
                    }
                }
            } catch (Exception e2) {
                in = in2;
            } catch (Throwable th2) {
                th = th2;
                in = in2;
            }
        } catch (Exception e3) {
            if (in != null) {
                try {
                    in.close();
                } catch (Exception e4) {
                }
            }
            return outdata;
        } catch (Throwable th3) {
            th = th3;
            if (in != null) {
                try {
                    in.close();
                } catch (Exception e5) {
                }
            }
            //throw th;
            System.out.print("throw th");
        }
        return outdata;
    }

    public static void drawStrings(MFGraphics g2, String[] drawString, int x, int y, int width, int height, int beginPosition, boolean bold, int color1, int color2, int color3) {
        if (drawString != null) {
            int x2 = x;
            String stringToDraw = "";
            downPermit = false;
            if (stringCursol > 0) {
                upPermit = true;
            } else {
                upPermit = false;
            }
            int i = beginPosition;
            while (i < drawString.length) {
                if ((((i - stringCursol) - beginPosition) * LINE_SPACE) + FONT_H > height) {
                    downPermit = true;
                    return;
                }
                if (drawString[i].indexOf("<") == -1 || drawString[i].indexOf(">") == -1) {
                    stringToDraw = drawString[i];
                } else {
                    anchor = drawString[i].substring(drawString[i].indexOf("<") + 1, drawString[i].indexOf(">"));
                    stringToDraw = drawString[i].substring(drawString[i].indexOf(">") + 1);
                }
                if (i - beginPosition >= stringCursol) {
                    if (anchor.indexOf("H") != -1) {
                        x2 = x + ((width - zoomIn(getStringWidth(14, stringToDraw))) / 2);
                    } else if (anchor.indexOf("L") != -1) {
                        x2 = x;
                    } else if (anchor.indexOf("R") != -1) {
                        x2 = (x + width) - zoomIn(getStringWidth(14, stringToDraw));
                    }
                    if (bold) {
                        drawBoldString2(g2, stringToDraw, x2, y + (((i - stringCursol) - beginPosition) * LINE_SPACE), 20, color1, color2, color3);
                    } else {
                        drawString(g2, stringToDraw, x2, (((i - stringCursol) - beginPosition) * LINE_SPACE) + y, 20);
                    }
                }
                i++;
            }
        }
    }

    public static void drawStrings(MFGraphics g2, String[] drawString, int x, int y, int width, int height, int beginPosition, boolean bold, int color1, int color2, int color3, int font) {
        if (drawString != null) {
            int x2 = x;
            String stringToDraw = "";
            downPermit = false;
            if (stringCursol > 0) {
                upPermit = true;
            } else {
                upPermit = false;
            }
            int i = beginPosition;
            while (i < drawString.length) {
                if ((((i - stringCursol) - beginPosition) * LINE_SPACE) + FONT_H > height) {
                    downPermit = true;
                    return;
                }
                if (drawString[i].indexOf("<") == -1 || drawString[i].indexOf(">") == -1) {
                    stringToDraw = drawString[i];
                } else {
                    anchor = drawString[i].substring(drawString[i].indexOf("<") + 1, drawString[i].indexOf(">"));
                    stringToDraw = drawString[i].substring(drawString[i].indexOf(">") + 1);
                }
                if (i - beginPosition >= stringCursol) {
                    if (anchor.indexOf("H") != -1) {
                        x2 = x + ((width - zoomIn(getStringWidth(font, stringToDraw))) / 2);
                    } else if (anchor.indexOf("L") != -1) {
                        x2 = x;
                    } else if (anchor.indexOf("R") != -1) {
                        x2 = (x + width) - zoomIn(getStringWidth(font, stringToDraw));
                    }
                    if (bold) {
                        drawBoldString2(g2, stringToDraw, x2, y + (((i - stringCursol) - beginPosition) * LINE_SPACE), 20, color1, color2, color3);
                    } else {
                        drawString(g2, stringToDraw, x2, (((i - stringCursol) - beginPosition) * LINE_SPACE) + y, 20);
                    }
                }
                i++;
            }
        }
    }

    public static void drawStrings(MFGraphics g2, String[] drawString, int x, int y, int width, int height, int fontH, int beginPosition, boolean bold, int color1, int color2, int color3) {
        if (drawString != null) {
            int x2 = x;
            String stringToDraw = "";
            downPermit = false;
            if (stringCursol > 0) {
                upPermit = true;
            } else {
                upPermit = false;
            }
            int i = beginPosition;
            while (i < drawString.length) {
                if ((((i - stringCursol) - beginPosition) * LINE_SPACE) + FONT_H > height) {
                    downPermit = true;
                    return;
                }
                if (drawString[i].indexOf("<") == -1 || drawString[i].indexOf(">") == -1) {
                    stringToDraw = drawString[i];
                } else {
                    anchor = drawString[i].substring(drawString[i].indexOf("<") + 1, drawString[i].indexOf(">"));
                    stringToDraw = drawString[i].substring(drawString[i].indexOf(">") + 1);
                }
                if (i - beginPosition >= stringCursol) {
                    if (anchor.indexOf("H") != -1) {
                        x2 = x + ((width - zoomIn(getStringWidth(14, stringToDraw))) / 2);
                    } else if (anchor.indexOf("L") != -1) {
                        x2 = x;
                    } else if (anchor.indexOf("R") != -1) {
                        x2 = (x + width) - zoomIn(getStringWidth(14, stringToDraw));
                    }
                    if (bold) {
                        drawBoldString2(g2, stringToDraw, x2, y + (((i - stringCursol) - beginPosition) * fontH), 20, color1, color2, color3);
                    } else {
                        drawString(g2, stringToDraw, x2, (((i - stringCursol) - beginPosition) * fontH) + y, 20);
                    }
                }
                i++;
            }
        }
    }

    public static void drawStringsContinue(MFGraphics g2, String[] drawString, int x, int y, int width, int height, int beginPosition, boolean bold, int color1, int color2, int color3) {
        if (drawString != null) {
            int x2 = x;
            String stringToDraw = "";
            downPermit = false;
            if (stringCursol > 0) {
                upPermit = true;
            } else {
                upPermit = false;
            }
            height = beginPosition;
            while (height < drawString.length) {
                if (drawString[height].indexOf("<") == -1 || drawString[height].indexOf(">") == -1) {
                    stringToDraw = drawString[height];
                } else {
                    anchor = drawString[height].substring(drawString[height].indexOf("<") + 1, drawString[height].indexOf(">"));
                    stringToDraw = drawString[height].substring(drawString[height].indexOf(">") + 1);
                }
                if (anchor.indexOf("H") != -1) {
                    x2 = x + ((width - zoomIn(getStringWidth(14, stringToDraw))) / 2);
                } else if (anchor.indexOf("L") != -1) {
                    x2 = x;
                } else if (anchor.indexOf("R") != -1) {
                    x2 = (x + width) - zoomIn(getStringWidth(14, stringToDraw));
                }
                if (bold) {
                    drawBoldString(g2, stringToDraw, x2, y + (((height - stringCursol) - beginPosition) * LINE_SPACE), 20, color1, color2, color3);
                } else {
                    drawString(g2, stringToDraw, x2, (((height - stringCursol) - beginPosition) * LINE_SPACE) + y, 20);
                }
                height++;
            }
        }
    }

    public static void drawStringsNarrow(MFGraphics g2, String[] drawString, int x, int y, int width, int height, int interval, int beginPosition, boolean bold, int color1, int color2, int color3) {
        if (drawString != null) {
            int x2 = x;
            String stringToDraw = "";
            downPermit = false;
            if (stringCursol > 0) {
                upPermit = true;
            } else {
                upPermit = false;
            }
            int i = beginPosition;
            while (i < drawString.length) {
                if ((((i - stringCursol) - beginPosition) * interval) + FONT_H > height) {
                    downPermit = true;
                    return;
                }
                if (drawString[i].indexOf("<") == -1 || drawString[i].indexOf(">") == -1) {
                    stringToDraw = drawString[i];
                } else {
                    anchor = drawString[i].substring(drawString[i].indexOf("<") + 1, drawString[i].indexOf(">"));
                    stringToDraw = drawString[i].substring(drawString[i].indexOf(">") + 1);
                }
                if (i - beginPosition >= stringCursol) {
                    if (anchor.indexOf("H") != -1) {
                        x2 = x + ((width - zoomIn(getStringWidth(14, stringToDraw))) / 2);
                    } else if (anchor.indexOf("L") != -1) {
                        x2 = x;
                    } else if (anchor.indexOf("R") != -1) {
                        x2 = (x + width) - zoomIn(getStringWidth(14, stringToDraw));
                    }
                    if (bold) {
                        drawBoldString(g2, stringToDraw, x2, y + (((i - stringCursol) - beginPosition) * interval), 20, color1, color2, color3);
                    } else {
                        drawString(g2, stringToDraw, x2, (((i - stringCursol) - beginPosition) * interval) + y, 20);
                    }
                }
                i++;
            }
        }
    }

    public static void drawStrings(MFGraphics g2, String[] drawString, int x, int y, int width, int height) {
        drawStrings(g2, drawString, x, y, width, height, 0, false, 0, 0, 0);
    }

    public static void drawBoldStrings(MFGraphics g2, String[] drawString, int x, int y, int width, int height, int color1, int color2, int color3) {
        drawStrings(g2, drawString, x, y, width, height, 0, true, color1, color2, color3);
    }

    public static void drawBoldStringsNarrow(MFGraphics g2, String[] drawString, int x, int y, int width, int height, int interval, int color1, int color2, int color3) {
        drawStringsNarrow(g2, drawString, x, y, width, height, interval, 0, true, color1, color2, color3);
    }

    public static void drawStrings(MFGraphics g2, String[] drawString, int x, int y, int width, int height, int fontH, int color1, int color2, int color3) {
        drawStrings(g2, drawString, x, y, width, height, fontH, 0, true, color1, color2, color3);
    }

    public static void logicString(boolean keyDown, boolean keyUp) {
        if (keyDown) {
            if (downPermit && (scrollPageWait == 5 || scrollPageWait == 0)) {
                stringCursol++;
            }
            if (scrollPageWait > 0) {
                scrollPageWait--;
            }
        } else if (keyUp) {
            if (upPermit && (scrollPageWait == 5 || scrollPageWait == 0)) {
                stringCursol--;
            }
            if (scrollPageWait > 0) {
                scrollPageWait--;
            }
        } else {
            scrollPageWait = 5;
        }
    }

    public static void initString() {
        stringCursol = 0;
        anchor = "";
    }

    public static void drawTxtArrows(MFGraphics g, int x, int y) {
        if (downPermit) {
            drawArrow(g, x + 10, y, false, false);
        }
        if (stringCursol > 0) {
            drawArrow(g, x - 10, y, true, false);
        }
    }

    public static void drawArrow(MFGraphics g, int x, int y, boolean isLeft, boolean isHorizontal) {
        x = zoomOut(x);
        y = zoomOut(y);
        for (int i = 0; i < ARROW.length; i++) {
            int arrowHeight;
            if (isLeft) {
                arrowHeight = ARROW[i];
            } else {
                arrowHeight = ARROW[(ARROW.length - i) - 1];
            }
            if (isHorizontal) {
                g.drawLine((x - (ARROW.length / 2)) + i, (y - (arrowHeight / 2)) - 1, (x - (ARROW.length / 2)) + i, ((y - (arrowHeight / 2)) - 1) + arrowHeight);
            } else {
                g.drawLine((x - (arrowHeight / 2)) - 1, (y - (ARROW.length / 2)) + i, ((x - (arrowHeight / 2)) - 1) + arrowHeight, (y - (ARROW.length / 2)) + i);
            }
        }
    }

    public static void drawBoldString(MFGraphics g2, String a, int x, int y, int anchor, int color) {
        drawBoldString(g2, a, x, y, anchor, color, 0, color);
    }

    public static void drawBoldString(MFGraphics g2, String a, int x, int y, int anchor, int color1, int color2) {
        drawBoldString(g2, a, x, y, anchor, color1, color2, color1);
    }

    public static void setBmfColor(int colorID) {
        switch (colorID) {
            case 0:
                currentBmFont = bmFont;
                return;
            case 1:
                currentBmFont = bmFontYellow;
                return;
            case 2:
                currentBmFont = bmFontGreen;
                return;
            case 3:
                currentBmFont = bmFontGray;
                return;
            default:
                return;
        }
    }

    public static void drawBoldString(MFGraphics g2, String a, int x, int y, int anchor, int color, int color2, int color3) {
        if (a != null) {
            x = zoomOut(x);
            y = zoomOut(y);
            g2.setColor(color2);
            for (int i = 0; i < OFFSET.length; i++) {
                g2.drawString(a, OFFSET[i][0] + x, OFFSET[i][1] + y, anchor);
            }
            g2.setColor(color);
            g2.drawString(a, x, y, anchor);
        }
    }

    public static void drawBoldString2(MFGraphics g2, String a, int x, int y, int anchor, int color, int color2, int color3) {
        if (a != null) {
            x = zoomOut(x);
            y = zoomOut(y);
            g2.setColor(color2);
            for (int i = 0; i < OFFSET2.length; i++) {
                g2.drawString(a, OFFSET2[i][0] + x, OFFSET2[i][1] + y, anchor);
            }
            g2.setColor(color);
            g2.drawString(a, x, y, anchor);
        }
    }

    public static int zoomOut(int x) {
        return x >> 0;
    }

    public static int zoomIn(int x) {
        return x << 0;
    }

    public static int zoomIn(int x, boolean bFlag) {
        if (bFlag) {
            return x << 0;
        }
        return zoomIn(x);
    }

    public static void drawRegionNokia(MFGraphics g, MFImage image, int sx, int sy, int cx, int cy, int attr, int dx, int dy) {
    }

    public static void drawImageSetClip(MFGraphics g, MFImage image, int sx, int sy, int sw, int sh, int dx, int dy, int anchor) {
        if ((anchor & 1) != 0) {
            dx -= sw / 2;
        } else if ((anchor & 8) != 0) {
            dx -= sw;
        }
        if ((anchor & 32) != 0) {
            dy -= sh;
        } else if ((anchor & 2) != 0) {
            dy -= sh / 2;
        }
        g.setClip(Math.max(dx, 0), Math.max(dy, 0), Math.min(sw + dx, 0 + SSDef.PLAYER_MOVE_HEIGHT) - Math.max(dx, 0), Math.min(sh + dy, 0 + 320) - Math.max(dy, 0));
        g.drawImage(image, dx - sx, dy - sy, 0);
        g.setClip(0, 0, SSDef.PLAYER_MOVE_HEIGHT, 320);
    }

    public static void drawRegionDebug(MFGraphics g, MFImage img, int sx, int sy, int w, int he, int rot, int x, int y, int anchor) {
        if ((anchor & 1) != 0) {
            x -= w / 2;
        } else if ((anchor & 8) != 0) {
            x -= w;
        }
        if ((anchor & 32) != 0) {
            y -= he;
        } else if ((anchor & 2) != 0) {
            y -= he / 2;
        }
        if (rot == 0) {
            g.drawRegion(img, sx, sy, w, he, rot, x, y, 20);
            return;
        }
        int[] RGB = new int[(w * he)];
        int[] mRGB = new int[(w * he)];
        img.getRGB(RGB, 0, w, sx, sy, w, he);
        int i;
        if (rot == 2) {
            for (i = 0; i < w; i++) {
                for (sy = 0; sy < he; sy++) {
                    mRGB[((w - i) - 1) + (sy * w)] = RGB[(sy * w) + i];
                }
            }
        } else if (rot == 5) {
            for (i = 0; i < w; i++) {
                for (sy = 0; sy < he; sy++) {
                    mRGB[((he - sy) - 1) + (i * he)] = RGB[(sy * w) + i];
                }
            }
        } else if (rot == 3) {
            for (i = 0; i < w; i++) {
                for (sy = 0; sy < he; sy++) {
                    mRGB[((w - i) - 1) + (((he - sy) - 1) * w)] = RGB[(sy * w) + i];
                }
            }
        } else if (rot == 6) {
            for (i = 0; i < w; i++) {
                for (sy = 0; sy < he; sy++) {
                    mRGB[sy + (((w - i) - 1) * he)] = RGB[(sy * w) + i];
                }
            }
        } else if (rot == 7) {
            for (i = 0; i < w; i++) {
                for (sy = 0; sy < he; sy++) {
                    mRGB[((he - sy) - 1) + (((w - i) - 1) * he)] = RGB[(sy * w) + i];
                }
            }
        } else if (rot == 1) {
            for (i = 0; i < w; i++) {
                for (sy = 0; sy < he; sy++) {
                    mRGB[i + (((he - sy) - 1) * w)] = RGB[(sy * w) + i];
                }
            }
        } else if (rot == 4) {
            for (i = 0; i < w; i++) {
                for (sy = 0; sy < he; sy++) {
                    mRGB[sy + (i * he)] = RGB[(sy * w) + i];
                }
            }
        }
        switch (rot) {
            case 4:
            case 5:
            case 6:
            case 7:
                g.drawRGB(mRGB, 0, he, x, y, he, w, true);
                return;
            default:
                g.drawRGB(mRGB, 0, w, x, y, w, he, true);
                return;
        }
    }

    public static final String[] loadText(String fileName) {
        try {
            return divideText(new String(getResource(fileName), "UTF-8"));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static byte[] getResource(String fileName) {
        InputStream is = null;
        byte[] re = null;
        try {
            is = MFDevice.getResourceAsStream(fileName);
            if (is != null) {
                ByteArrayOutputStream bs = new ByteArrayOutputStream();
                DataOutputStream ds = new DataOutputStream(bs);
                for (int readByte = is.read(); readByte >= 0; readByte = is.read()) {
                    ds.writeByte(readByte);
                }
                re = bs.toByteArray();
                bs.close();
            }
            if (is != null) {
                try {
                    is.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            if (is != null) {
                try {
                    is.close();
                } catch (Exception e3) {
                    e3.printStackTrace();
                }
            }
        } catch (Throwable th) {
            if (is != null) {
                try {
                    is.close();
                } catch (Exception e32) {
                    e32.printStackTrace();
                }
            }
        }
        return re;
    }

    private static int getTextLineNum(String org) {
        int ret = 0;
        int j = 0;
        while (true) {
            int x = org.indexOf("\n", j);
            if (x == -1) {
                return ret;
            }
            j = x + 1;
            ret++;
        }
    }

    private static String[] divideText(String string) {
        if (string == null) {
            return null;
        }
        String[] re = (String[]) null;
        re = new String[getTextLineNum(string)];
        int k = 0;
        int j = 0;
        while (true) {
            int x = string.indexOf(13, j);
            if (x == -1) {
                break;
            }
            int k2 = k + 1;
            re[k] = string.substring(j, x);
            j = x + 2;
            k = k2;
        }
        if (re[0] != null && re[0].length() > 0) {
            StringBuffer b = new StringBuffer(new String(re[0].toCharArray()));
            Integer ascii = new Integer(b.charAt(0));
            if (ascii.hashCode() == 63 || ascii.hashCode() == 65279 || ascii.hashCode() == -257) {
                b.deleteCharAt(0);
            }
            re[0] = b.toString();
        }
        return re;
    }

    public static double calNextPositionD(double current, double destiny, int velocity1, int velocity2) {
        return (double) calNextPosition(current, destiny, velocity1, velocity2, 1.0d);
    }

    public static float calNextPositionF(double current, double destiny, int velocity1, int velocity2, double deviation) {
        return (float) calNextPositionD(current, destiny, velocity1, velocity2, deviation);
    }

    public static int calNextPosition(double current, double destiny, int velocity1, int velocity2) {
        return calNextPosition(current, destiny, velocity1, velocity2, 1.0d);
    }

    public static int calNextPosition(double current, double destiny, int velocity1, int velocity2, double deviation) {
        return (int) calNextPositionD(current, destiny, velocity1, velocity2, deviation);
    }

    public static double calNextPositionD(double current, double destiny, int velocity1, int velocity2, double deviation) {
        double re = current;
        if (velocity2 <= velocity1) {
            return re;
        }
        double distance = (((destiny - current) * 100.0d) * ((double) velocity1)) / ((double) velocity2);
        current += distance / 100.0d;
        if (distance == 0.0d) {
            deviation = 0.0d;
        } else if (distance <= 0.0d) {
            deviation = -deviation;
        }
        current += deviation;
        if (((long) distance) * ((((long) destiny) * 100) - (((long) current) * 100)) <= 0) {
            current = destiny;
        }
        return current;
    }

    public static int calNextPositionReverse(int current, int start, int destiny, int velocity1, int velocity2) {
        return calNextPositionReverse(current, start, destiny, velocity1, velocity2, 1);
    }

    public static int calNextPositionReverse(int current, int start, int destiny, int velocity1, int velocity2, int deviation) {
        int re = current;
        if (velocity2 <= velocity1) {
            return re;
        }
        if (current == destiny) {
            return re;
        }
        int moveChange = ((Math.abs(current - start) * velocity2) / (velocity2 - velocity1)) >> 1;
        if (moveChange == 0) {
            moveChange = deviation;
        }
        if (re < destiny) {
            re += moveChange;
            if (re > destiny) {
                re = destiny;
            }
        } else {
            re -= moveChange;
            if (re < destiny) {
                re = destiny;
            }
        }
        return re;
    }

    public static void drawFadeRange(MFGraphics g, int fadevalue, int x, int y, int type) {
        int i;
        for (i = 0; i < 864; i++) {
            rayRGB[i] = MapManager.END_COLOR;
        }
        for (int w = 0; w < RAY_WIDTH; w++) {
            for (i = 0; i < 3; i++) {
                rayRGB[(i * RAY_WIDTH) + w] = ((fadevalue << 24) & -16777216) | (rayRGB[(i * RAY_WIDTH) + w] & MapManager.END_COLOR);
            }
        }
        switch (type) {
            case 0:
            case 7:
                g.drawRGB(rayRGB, 0, RAY_WIDTH, x, y, RAY_WIDTH, 3, true);
                return;
            case 1:
            case 6:
                g.drawRGB(rayRGB, 0, RAY_WIDTH, x, y, RAY_WIDTH, 3, true);
                g.drawRGB(rayRGB, 0, RAY_WIDTH, x, y - 3, RAY_WIDTH, 3, true);
                g.drawRGB(rayRGB, 0, RAY_WIDTH, x, y + 3, RAY_WIDTH, 3, true);
                return;
            case 2:
            case 5:
                g.drawRGB(rayRGB, 0, RAY_WIDTH, x, y, RAY_WIDTH, 3, true);
                g.drawRGB(rayRGB, 0, RAY_WIDTH, x, y - 3, RAY_WIDTH, 3, true);
                g.drawRGB(rayRGB, 0, RAY_WIDTH, x, y + 3, RAY_WIDTH, 3, true);
                g.drawRGB(rayRGB, 0, RAY_WIDTH, x, y - 6, RAY_WIDTH, 3, true);
                g.drawRGB(rayRGB, 0, RAY_WIDTH, x, y + 6, RAY_WIDTH, 3, true);
                return;
            case 3:
            case 4:
                g.drawRGB(rayRGB, 0, RAY_WIDTH, x, y, RAY_WIDTH, 3, true);
                g.drawRGB(rayRGB, 0, RAY_WIDTH, x, y - 3, RAY_WIDTH, 3, true);
                g.drawRGB(rayRGB, 0, RAY_WIDTH, x, y + 3, RAY_WIDTH, 3, true);
                g.drawRGB(rayRGB, 0, RAY_WIDTH, x, y - 6, RAY_WIDTH, 3, true);
                g.drawRGB(rayRGB, 0, RAY_WIDTH, x, y + 6, RAY_WIDTH, 3, true);
                g.drawRGB(rayRGB, 0, RAY_WIDTH, x, y - 9, RAY_WIDTH, 3, true);
                g.drawRGB(rayRGB, 0, RAY_WIDTH, x, y + 9, RAY_WIDTH, 3, true);
                return;
            default:
                return;
        }
    }

    public static int getRelativePointX(int originalX, int offsetX, int offsetY, int degree) {
        return (((dCos(degree) * offsetX) / 100) + originalX) - ((dSin(degree) * offsetY) / 100);
    }

    public static int getRelativePointY(int originalY, int offsetX, int offsetY, int degree) {
        return (((dSin(degree) * offsetX) / 100) + originalY) + ((dCos(degree) * offsetY) / 100);
    }

    public static String getPath(String path) {
        String path2 = "";
        while (path.indexOf("/") != -1) {
            path2 = new StringBuilder(String.valueOf(path2)).append(path.substring(0, path.indexOf("/") + 1)).toString();
            path = path.substring(path.indexOf("/") + 1);
        }
        return path2;
    }

    public static String getFileName(String path) {
        String path2 = "";
        while (path.indexOf("/") != -1) {
            path2 = new StringBuilder(String.valueOf(path2)).append(path.substring(0, path.indexOf("/") + 1)).toString();
            path = path.substring(path.indexOf("/") + 1);
        }
        return path;
    }

    public static void vibrate() {
        if (GlobalResource.vibrationConfig == 1) {
            MFDevice.vibrateByTime(100);
        }
    }

    public static void drawScaleAni(MFGraphics g, AnimationDrawer drawer, int id, int x, int y, float scalex, float scaley, float pointx, float pointy) {
        drawer.setActionId(id);
        Graphics g2 = (Graphics) g.getSystemGraphics();
        g2.save();
        g2.translate((float) x, (float) y);
        g2.scale(scalex, scaley, pointx, pointy);
        drawer.draw(g, 0, 0);
        g2.scale(1.0f / scalex, 1.0f / scaley);
        g2.translate((float) (-x), (float) (-y));
        g2.restore();
    }
}
