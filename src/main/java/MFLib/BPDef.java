package MFLib;

public interface BPDef {
    public static final String CPID = "";
    public static final int POINT_ACTIVE_GAME = 0;
    public static final int POINT_BUY_TOOLS = 2;
    public static final int POINT_REVIVE = 1;
    public static final int PRICE_ACTIVE_GAME = 400;
    public static final int PRICE_BUY_TOOLS = 200;
    public static final int PRICE_REVIVE = 200;
    public static final String STR_ACTIVE_GAME = "";
    public static final String STR_BUY_TOOLS = "";
    public static final String STR_REVIVE = "";
    public static final String gameID = "";
}
