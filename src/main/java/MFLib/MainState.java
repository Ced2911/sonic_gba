package MFLib;

import GameEngine.Def;
import GameEngine.Key;
import Lib.SoundSystem;
import PlatformStandard.Standard2;
import SonicGBA.PlayerObject;
import SonicGBA.StageManager;
import State.State;
import State.TitleState;
import android.os.Message;
import com.sega.MFLib.Main;
import com.sega.mobile.framework.MFGameState;
import com.sega.mobile.framework.device.MFDevice;
import com.sega.mobile.framework.device.MFGraphics;
import month.MonthCertificationState;

public class MainState implements MFGameState, Def {
    public static final int FRAME_SKIP = 63;
    private static final String MF_VERSION = "";
    private static String gameVersion = "";
    private Main main;
    private boolean pauseFlag = false;

    public MainState(Main main) {
        this.main = main;
    }

    public int getFrameTime() {
        if (Main.BULLET_TIME) {
            return 1008;
        }
        return 63;
    }

    public void onEnter() {
        init_bp();
        State.stateInit();
        PlayerObject.setCharacter(0);
        StageManager.setStageID(0);
        State.setState(0);
        String versionStr = this.main.getAppProperty("MIDlet-Version");
        if (!versionStr.startsWith("1.0")) {
            gameVersion = "ver:" + versionStr;
        }
        MFDevice.setUseMultitouch(true);
        MFDevice.setAntiAlias(false);
        MFDevice.setFilterBitmap(false);
        MFDevice.setVibrationFlag(true);
        this.main.isResumeFromOtherActivity = false;
        Message msg = new Message();
        msg.what = 0;
        this.main.handler.handleMessage(msg);
    }

    private static void init_bp() {
    }

    public void onExit() {
    }

    public void onPause() {
        State.pauseTrigger();
        this.pauseFlag = true;
    }

    private void pauseCheck() {
        if (this.pauseFlag) {
            Key.clear();
            State.statePause();
            this.pauseFlag = false;
        }
    }

    public void onRender(MFGraphics g) {
        State.stateDraw(g);
    }

    public void onTick() {
        if (!MonthCertificationState.logic()) {
            pauseCheck();
            State.stateLogic();
            State.getMain(this.main);
            Standard2.getMain(this.main);
            pauseCheck();
        }
    }

    public void onResume() {
        SoundSystem.getInstance().updateVolumeState();
    }

    public void onVolumeDown() {
    }

    public void onVolumeUp() {
    }

    public void onRender(MFGraphics g, int layer) {
        TitleState.drawTitle(g, layer);
    }
}
