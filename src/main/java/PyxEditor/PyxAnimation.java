package PyxEditor;

import Lib.Animation;
import Lib.AnimationDrawer;
import Lib.MyAPI;
import SonicGBA.RollPlatformSpeedC;

import com.sega.mobile.define.MDPhone;
import com.sega.mobile.framework.android.Graphics;
import com.sega.mobile.framework.device.MFDevice;
import com.sega.mobile.framework.device.MFGraphics;
import java.io.DataInputStream;
import java.util.Stack;

public class PyxAnimation {
    public static final int ZOOM = 6;
    private static boolean pauseFlag = false;
    private Action[] actionArray;
    private Animation[] animationArray;
    private KeyFrame[] changeStratKeyArray;
    private int currentAction;
    private boolean loop = false;
    private Node[] nodeArray;
    private Stack nodeStack;
    private int rootNodeID;
    private int speed = 64;

    class Action {
        private int frame;
        private String label;
        private int timeLimit;
        private ActionTrack[] trackArray;

        public Action(DataInputStream ds) {
            loadStream(ds);
        }

        private void loadStream(DataInputStream ds) {
            try {
                int trackNum = ds.readByte();
                this.trackArray = new ActionTrack[trackNum];
                for (int i = 0; i < trackNum; i++) {
                    this.trackArray[i] = new ActionTrack(ds);
                }
                byte[] strByte = new byte[ds.readShort()];
                ds.read(strByte);
                this.label = new String(strByte, "UTF-8");
                this.timeLimit = ds.readShort();
            } catch (Exception e) {
            }
        }

        public void close() {
            for (ActionTrack close : this.trackArray) {
                close.close();
            }
            this.trackArray = null;
            this.label = null;
        }

        public void reset() {
            this.frame = 0;
            setNodeProperty();
        }

        public void reset(int frame) {
            this.frame = frame << 6;
            setNodeProperty();
        }

        public void moveOn() {
            this.frame += PyxAnimation.this.speed;
            if (this.frame >= (this.timeLimit << 6)) {
                if (PyxAnimation.this.loop) {
                    this.frame -= this.timeLimit << 6;
                } else {
                    this.frame = (this.timeLimit << 6) - 1;
                }
            }
            setNodeProperty();
        }

        public void setNodeProperty() {
            for (ActionTrack nodeProperty : this.trackArray) {
                nodeProperty.setNodeProperty(this.frame);
            }
        }

        public void saveKeyFrame(KeyFrame[] frameArray) {
            for (int i = 0; i < this.trackArray.length; i++) {
                this.trackArray[i].setNodePropertyToKeyFrame(frameArray[i], this.frame);
            }
        }

        public boolean chkEnd() {
            return !PyxAnimation.this.loop && this.frame == (this.timeLimit << 6) - 1;
        }
    }

    class ActionTrack {
        private KeyFrame[] keyFrameArray;
        private int nodeID;

        public ActionTrack(DataInputStream ds) {
            loadStream(ds);
        }

        private void loadStream(DataInputStream ds) {
            try {
                this.nodeID = ds.readByte();
                int frameNum = ds.readByte();
                this.keyFrameArray = new KeyFrame[frameNum];
                for (int i = 0; i < frameNum; i++) {
                    this.keyFrameArray[i] = new KeyFrame(ds);
                }
            } catch (Exception e) {
            }
        }

        public void close() {
            this.keyFrameArray = null;
        }

        public void setNodeProperty(int frame) {
            PyxAnimation.this.nodeArray[this.nodeID].degree = getDegree(frame);
        }

        public void setNodePropertyToKeyFrame(KeyFrame frameObj, int frame) {
            frameObj.degree = getDegree(frame);
        }

        public int getDegree(int frame) {
            frame >>= 6;
            KeyFrame frameBefore = null;
            KeyFrame frameBehind = null;
            if (frame >= 0) {
                for (int i = 0; i < this.keyFrameArray.length; i++) {
                    if (this.keyFrameArray[i].framePosition > frame) {
                        frameBehind = this.keyFrameArray[i];
                        break;
                    }
                    frameBefore = this.keyFrameArray[i];
                }
            } else {
                frameBefore = PyxAnimation.this.changeStratKeyArray[this.nodeID];
                frameBehind = this.keyFrameArray[0];
            }
            if (frameBehind == null) {
                return frameBefore.degree;
            }
            int duration = frameBehind.framePosition - frameBefore.framePosition;
            int degreeDiff = frameBehind.degree - frameBefore.degree;
            if (degreeDiff > RollPlatformSpeedC.DEGREE_VELOCITY) {
                degreeDiff -= 360;
            }
            if (degreeDiff < -180) {
                degreeDiff += MDPhone.SCREEN_WIDTH;
            }
            return frameBefore.degree + (((frame - frameBefore.framePosition) * degreeDiff) / duration);
        }
    }

    class ConnectPoint {
        public int linkNodeId;
        private ConnectPoint linkPoint = null;
        public int linkPointId;
        public Node node;
        private int showX;
        private int showY;
        public int f21x;
        public int f22y;

        public ConnectPoint(Node node, DataInputStream ds) {
            this.node = node;
            loadStream(ds);
        }

        private void loadStream(DataInputStream ds) {
            try {
                this.f21x = ds.readShort();
                this.f22y = ds.readShort();
                this.linkNodeId = ds.readByte();
                this.linkPointId = ds.readByte();
            } catch (Exception e) {
            }
        }

        public void linkByID() {
            if (this.linkNodeId >= 0 && this.linkPointId >= 0) {
                this.linkPoint = PyxAnimation.this.nodeArray[this.linkNodeId].connectPointArray[this.linkPointId];
            }
        }

        public void close() {
            this.node = null;
            this.linkPoint = null;
        }

        public Node getLinkNode() {
            if (this.linkPoint == null) {
                return null;
            }
            return this.linkPoint.node;
        }

        public ConnectPoint getLinkConnectPoint() {
            return this.linkPoint;
        }

        public void setLinkPointPosition() {
            if (this.linkPoint != null) {
                this.linkPoint.showX = this.showX;
                this.linkPoint.showY = this.showY;
            }
        }
    }

    class KeyFrame {
        private int degree;
        private int framePosition;

        public KeyFrame() {
            this.framePosition = 0;
        }

        public KeyFrame(DataInputStream ds) {
            loadStream(ds);
        }

        private void loadStream(DataInputStream ds) {
            try {
                this.framePosition = ds.readShort();
                this.degree = ds.readShort();
            } catch (Exception e) {
            }
        }
    }

    class Node {
        private int animationID;
        private int animationX;
        private int animationY;
        private int calDegree;
        public ConnectPoint centerPoint;
        private ConnectPoint[] connectPointArray;
        private int degree;
        private AnimationDrawer drawer;
        private String label;
        private PyxAnimation pyx;
        private int returnId;
        private Node superNode;

        public Node(PyxAnimation pyx, DataInputStream ds) {
            this.pyx = pyx;
            loadStream(ds);
        }

        private void loadStream(DataInputStream ds) {
            try {
                ds.readShort();
                ds.readShort();
                ds.readShort();
                ds.readShort();
                int connectNum = ds.readByte();
                this.connectPointArray = new ConnectPoint[connectNum];
                for (int i = 0; i < connectNum; i++) {
                    this.connectPointArray[i] = new ConnectPoint(this, ds);
                }
                byte[] strByte = new byte[ds.readShort()];
                ds.read(strByte);
                this.label = new String(strByte, "UTF-8");
                this.animationID = ds.readByte();
                int actionID = ds.readByte();
                this.animationX = ds.readByte();
                this.animationY = ds.readByte();
                this.drawer = PyxAnimation.this.animationArray[this.animationID].getDrawer(actionID, true, 0);
            } catch (Exception e) {
            }
        }

        public void linkByID() {
            for (ConnectPoint linkByID : this.connectPointArray) {
                linkByID.linkByID();
            }
        }

        public void setRootConnectPoint(int rootPoint) {
            this.centerPoint = this.connectPointArray[rootPoint];
        }

        public void close() {
            for (ConnectPoint close : this.connectPointArray) {
                close.close();
            }
            this.connectPointArray = null;
            this.label = null;
            this.drawer = null;
        }

        public void resetForDraw() {
            this.returnId = 0;
            this.superNode = null;
        }

        public void doCalBeforeDraw() {
            if (this.centerPoint != null) {
                this.calDegree = getDrawDegree();
                for (ConnectPoint element : this.connectPointArray) {
                    if (element != this.centerPoint) {
                        int offsetX = (element.f21x - this.centerPoint.f21x) << 6;
                        int offsetY = (element.f22y - this.centerPoint.f22y) << 6;
                        element.showX = MyAPI.getRelativePointX(this.centerPoint.showX, offsetX, offsetY, this.calDegree);
                        element.showY = MyAPI.getRelativePointY(this.centerPoint.showY, offsetX, offsetY, this.calDegree);
                        element.setLinkPointPosition();
                    }
                }
            }
        }

        public Node getSubNode() {
            /*
            Node re = null;
            ConnectPoint centerPoint = null;
            while (true) {
                if ((re == null || re == this.superNode) && this.returnId < this.connectPointArray.length) {
                    re = this.connectPointArray[this.returnId].getLinkNode();
                    centerPoint = this.connectPointArray[this.returnId].getLinkConnectPoint();
                    this.returnId++;
                }
            }
            if (re == null || re == this.superNode) {
                return null;
            }
            re.setSuperNode(this);
            re.setCenterPoint(centerPoint);
            return re;
            */
            Node re = null;
            ConnectPoint centerPoint = null;
            while (this.returnId < this.connectPointArray.length) {
                if ((re == null || re == this.superNode)) {
                    re = this.connectPointArray[this.returnId].getLinkNode();
                    centerPoint = this.connectPointArray[this.returnId].getLinkConnectPoint();
                    this.returnId++;
                }
            }
            if (re == null || re == this.superNode) {
                return null;
            }
            re.setSuperNode(this);
            re.setCenterPoint(centerPoint);
            return re;
        }

        public void setSuperNode(Node node) {
            this.superNode = node;
        }

        public void setCenterPoint(ConnectPoint centerPoint) {
            this.centerPoint = centerPoint;
        }

        public int getDrawDegree() {
            int re = this.degree;
            if (this.superNode != null) {
                re += this.superNode.getDrawDegree();
            }
            while (re < 0) {
                re += MDPhone.SCREEN_WIDTH;
            }
            return re % MDPhone.SCREEN_WIDTH;
        }

        public void draw(MFGraphics g, int x, int y) {
            x <<= 6;
            y <<= 6;
            Graphics g2 = (Graphics) g.getSystemGraphics();
            g2.save();
            g2.translate((float) ((this.centerPoint.showX + x) >> 6), (float) ((this.centerPoint.showY + y) >> 6));
            g2.rotate((float) this.calDegree);
            g2.translate((float) (-this.centerPoint.f21x), (float) (-this.centerPoint.f22y));
            if (this.drawer != null) {
                this.drawer.draw(g, this.animationX, this.animationY);
            }
            g2.restore();
        }

        public void setAnimation(int animationID, int actionID) {
            if (animationID >= 0 && animationID < PyxAnimation.this.animationArray.length) {
                if (this.animationID != animationID) {
                    this.drawer = PyxAnimation.this.animationArray[animationID].getDrawer(0, false, 0);
                    this.animationID = animationID;
                }
                this.drawer.setActionId(actionID);
            }
        }

        public int getAnimationPosX() {
            int centerX = 0;
            int offsetX = this.animationX;
            int offsetY = this.animationY;
            if (this.centerPoint != null) {
                centerX = this.centerPoint.showX;
                int centerY = this.centerPoint.showY;
                offsetX -= this.centerPoint.f21x;
                offsetY -= this.centerPoint.f22y;
            }
            return MyAPI.getRelativePointX(centerX, offsetX << 6, offsetY << 6, this.calDegree) >> 6;
        }

        public int getAnimationPosY() {
            int centerY = 0;
            int offsetX = this.animationX;
            int offsetY = this.animationY;
            if (this.centerPoint != null) {
                int centerX = this.centerPoint.showX;
                centerY = this.centerPoint.showY;
                offsetX -= this.centerPoint.f21x;
                offsetY -= this.centerPoint.f22y;
            }
            return MyAPI.getRelativePointY(centerY, offsetX << 6, offsetY << 6, this.calDegree) >> 6;
        }
    }

    public static class NodeInfo {
        public int animationX;
        public int animationY;
        public int degree;
        public AnimationDrawer drawer;
        private boolean got;
        public int rotateX;
        public int rotateY;

        public void reset() {
            this.got = false;
        }

        public boolean hasNode() {
            return this.got;
        }
    }

    public static void setPause(boolean flag) {
        pauseFlag = flag;
    }

    public PyxAnimation(String fileName, Animation[] animation) {
        this.animationArray = animation;
        this.currentAction = -1;
        this.nodeStack = new Stack();
        loadFile(fileName);
        calBeforeDraw();
    }

    private void loadFile(String fileName) {
        try {
            int i;
            DataInputStream ds = new DataInputStream(MFDevice.getResourceAsStream(fileName));
            int nodeNum = ds.readByte();
            this.nodeArray = new Node[nodeNum];
            this.changeStratKeyArray = new KeyFrame[nodeNum];
            for (i = 0; i < nodeNum; i++) {
                this.nodeArray[i] = new Node(this, ds);
                this.changeStratKeyArray[i] = new KeyFrame();
            }
            for (i = 0; i < nodeNum; i++) {
                this.nodeArray[i].linkByID();
            }
            this.rootNodeID = ds.readByte();
            int rootPointId = ds.readByte();
            if (this.rootNodeID >= 0) {
                this.nodeArray[this.rootNodeID].setRootConnectPoint(rootPointId);
            }
            int actionNum = ds.readByte();
            this.actionArray = new Action[actionNum];
            for (i = 0; i < actionNum; i++) {
                this.actionArray[i] = new Action(ds);
            }
        } catch (Exception e) {
        }
    }

    public void close() {
        for (Node close : this.nodeArray) {
            close.close();
        }
        this.nodeArray = null;
        for (Action close2 : this.actionArray) {
            close2.close();
        }
        this.actionArray = null;
    }

    public void drawAction(MFGraphics g, int actionID, int x, int y) {
        if (this.currentAction != actionID) {
            this.actionArray[actionID].reset();
            this.currentAction = actionID;
        }
        calBeforeDraw();
        for (Node draw : this.nodeArray) {
            draw.draw(g, x, y);
        }
        if (!pauseFlag) {
            this.actionArray[this.currentAction].moveOn();
        }
    }

    public void drawAction(MFGraphics g, int x, int y) {
        if (this.currentAction >= 0) {
            calBeforeDraw();
            for (Node draw : this.nodeArray) {
                draw.draw(g, x, y);
            }
            if (!pauseFlag) {
                this.actionArray[this.currentAction].moveOn();
            }
        }
    }

    private void calBeforeDraw() {
        for (Node resetForDraw : this.nodeArray) {
            resetForDraw.resetForDraw();
        }
        this.nodeStack.removeAllElements();
        Node currentNode = this.nodeArray[this.rootNodeID];
        currentNode.doCalBeforeDraw();
        while (true) {
            if (currentNode != null) {
                Node superNode = currentNode;
                currentNode = currentNode.getSubNode();
                if (currentNode != null) {
                    this.nodeStack.push(superNode);
                    currentNode.doCalBeforeDraw();
                }
            } else if (!this.nodeStack.empty()) {
                currentNode = (Node) this.nodeStack.pop();
            } else {
                return;
            }
        }
    }

    public void setAction(int actionID) {
        this.actionArray[actionID].reset();
        this.currentAction = actionID;
    }

    public void changeToAction(int actionID, int duration) {
        this.actionArray[this.currentAction].saveKeyFrame(this.changeStratKeyArray);
        for (KeyFrame access$3 : this.changeStratKeyArray) {
            access$3.framePosition = -duration;
        }
        this.actionArray[actionID].reset(-duration);
        this.currentAction = actionID;
    }

    public boolean chkEnd() {
        if (this.currentAction >= 0) {
            return this.actionArray[this.currentAction].chkEnd();
        }
        return false;
    }

    public void setLoop(boolean loop) {
        this.loop = loop;
    }

    public int getNodeXByAnimationNamed(String name, int xOffset, int yOffset) {
        for (int i = 0; i < this.nodeArray.length; i++) {
            if (this.nodeArray[i].label.equals(name)) {
                return MyAPI.getRelativePointX(this.nodeArray[i].centerPoint.showX, ((this.nodeArray[i].animationX - this.nodeArray[i].centerPoint.f21x) + xOffset) << 6, ((this.nodeArray[i].animationY - this.nodeArray[i].centerPoint.f22y) + yOffset) << 6, this.nodeArray[i].calDegree) >> 6;
            }
        }
        return 0;
    }

    public int getNodeYByAnimationNamed(String name, int xOffset, int yOffset) {
        for (int i = 0; i < this.nodeArray.length; i++) {
            if (this.nodeArray[i].label.equals(name)) {
                return MyAPI.getRelativePointY(this.nodeArray[i].centerPoint.showY, ((this.nodeArray[i].animationX - this.nodeArray[i].centerPoint.f21x) + xOffset) << 6, ((this.nodeArray[i].animationY - this.nodeArray[i].centerPoint.f22y) + yOffset) << 6, this.nodeArray[i].calDegree) >> 6;
            }
        }
        return 0;
    }

    public void setSpeed(int speed) {
        if (speed > 0) {
            this.speed = speed;
        }
    }

    public Node getNodeByName(String nodeName) {
        for (int i = 0; i < this.nodeArray.length; i++) {
            if (this.nodeArray[i].label.equals(nodeName)) {
                return this.nodeArray[i];
            }
        }
        return null;
    }

    public void changeAnimation(String nodeName, int animationID, int actionID) {
        Node node = getNodeByName(nodeName);
        if (node != null) {
            node.setAnimation(animationID, actionID);
        }
    }

    public void getNodeInfo(NodeInfo info, String nodeName) {
        info.reset();
        Node node = getNodeByName(nodeName);
        if (node != null) {
            info.got = true;
            info.drawer = node.drawer;
            info.animationX = node.getAnimationPosX();
            info.animationY = node.getAnimationPosY();
            info.rotateX = 0;
            info.rotateY = 0;
            if (node.centerPoint != null) {
                info.rotateX = node.centerPoint.showX >> 6;
                info.rotateY = node.centerPoint.showY >> 6;
            }
            info.degree = node.calDegree;
        }
    }
}
