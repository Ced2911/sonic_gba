package com.sega.engine.ext;

import com.sega.engine.action.ACCollision;
import com.sega.engine.action.ACObject;

public class ACRoundCollision extends ACCollision {
    private int centerX;
    private int centerY;
    private int radius;

    public ACRoundCollision(ACObject acObj, int centerX, int centerY, int radius) {
        super(acObj, acObj.getWorld());
        setProperty(centerX, centerY, radius);
    }

    public void setProperty(int centerX, int centerY, int radius) {
        this.centerX = centerX;
        this.centerY = centerY;
        this.radius = radius;
    }

    public void update() {
    }

    public int getCollisionXFromLeft(int y) {
        y -= this.radius;
        int doubleY = y * y;
        int doubleR = this.radius * this.radius;
        int x = -this.radius;
        while (x < 0) {
            if ((x * x) + doubleY <= doubleR) {
                return this.radius + x;
            }
            x += 1 << this.worldZoom;
        }
        return -1000;
    }

    public int getCollisionXFromRight(int y) {
        y -= this.radius;
        int doubleY = y * y;
        int doubleR = this.radius * this.radius;
        int x = this.radius;
        while (x > 0) {
            if ((x * x) + doubleY <= doubleR) {
                return this.radius + x;
            }
            x -= 1 << this.worldZoom;
        }
        return -1000;
    }

    public int getCollisionYFromDown(int x) {
        x -= this.radius;
        int doubleX = x * x;
        int doubleR = this.radius * this.radius;
        int y = this.radius;
        while (y > 0) {
            if ((y * y) + doubleX <= doubleR) {
                return this.radius + y;
            }
            y -= 1 << this.worldZoom;
        }
        return -1000;
    }

    public int getCollisionYFromUp(int x) {
        x -= this.radius;
        int doubleX = x * x;
        int doubleR = this.radius * this.radius;
        int y = -this.radius;
        while (y < 0) {
            if ((y * y) + doubleX <= doubleR) {
                return this.radius + y;
            }
            y += 1 << this.worldZoom;
        }
        return -1000;
    }

    public int getLeftX() {
        return this.centerX - this.radius;
    }

    public int getTopY() {
        return this.centerY - this.radius;
    }

    public int getWidth() {
        return this.radius << 1;
    }

    public int getHeight() {
        return this.radius << 1;
    }
}
