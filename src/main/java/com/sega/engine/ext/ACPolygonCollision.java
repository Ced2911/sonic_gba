package com.sega.engine.ext;

import com.sega.engine.action.ACCollision;
import com.sega.engine.action.ACObject;
import com.sega.engine.lib.Line;
import com.sega.engine.lib.Line.CrossPoint;

public class ACPolygonCollision extends ACCollision {
    protected int VERTEX_NUM;
    protected int height;
    protected int posX;
    protected int posY;
    protected CrossPoint rePoint = new CrossPoint();
    protected int[] useId;
    protected int[][] vertex;
    protected int width;

    public ACPolygonCollision(ACObject acObj, int[][] vertex, int[] useId, int x, int y) {
        super(acObj, acObj.getWorld());
        setProperty(vertex, useId, x, y);
    }

    public void setProperty(int[][] vertex, int[] useId, int x, int y) {
        this.vertex = vertex;
        this.useId = useId;
        this.VERTEX_NUM = this.useId.length;
        this.posX = x;
        this.posY = y;
    }

    public void update() {
        int i;
        int leftX = getLeftX() - this.posX;
        int topY = getTopY() - this.posY;
        int width = 0;
        int height = 0;
        for (i = 0; i < this.VERTEX_NUM; i++) {
            if (this.vertex[this.useId[i]][0] - leftX > width) {
                width = this.vertex[this.useId[i]][0] - leftX;
            }
        }
        for (i = 0; i < this.VERTEX_NUM; i++) {
            if (this.vertex[this.useId[i]][1] - topY > height) {
                height = this.vertex[this.useId[i]][1] - topY;
            }
        }
        this.width = width;
        this.height = height;
    }

    public int getLeftX() {
        int re = this.vertex[this.useId[0]][0];
        for (int i = 0; i < this.VERTEX_NUM; i++) {
            if (this.vertex[this.useId[i]][0] < re) {
                re = this.vertex[this.useId[i]][0];
            }
        }
        return this.posX + re;
    }

    public int getTopY() {
        int re = this.vertex[this.useId[0]][1];
        for (int i = 0; i < this.VERTEX_NUM; i++) {
            if (this.vertex[this.useId[i]][1] < re) {
                re = this.vertex[this.useId[i]][1];
            }
        }
        return this.posY + re;
    }

    public int getHeight() {
        return this.height;
    }

    public int getWidth() {
        return this.width;
    }

    public int getCollisionXFromLeft(int y) {
        int reX = -1000;
        int leftX = getLeftX();
        int topY = getTopY();
        for (int i = 0; i < this.VERTEX_NUM; i++) {
            Line.getCrossPoint(this.rePoint, (this.vertex[this.useId[i]][0] + this.posX) - leftX, (this.vertex[this.useId[i]][1] + this.posY) - topY, (this.vertex[this.useId[(i + 1) % this.VERTEX_NUM]][0] + this.posX) - leftX, (this.vertex[this.useId[(i + 1) % this.VERTEX_NUM]][1] + this.posY) - topY, 0, y, getWidth(), y);
            if (this.rePoint.hasPoint && (reX == -1000 || this.rePoint.f2x < reX)) {
                reX = this.rePoint.f2x;
            }
        }
        return reX;
    }

    public int getCollisionXFromRight(int y) {
        int reX = -1000;
        int leftX = getLeftX();
        int topY = getTopY();
        for (int i = 0; i < this.VERTEX_NUM; i++) {
            Line.getCrossPoint(this.rePoint, (this.vertex[this.useId[i]][0] + this.posX) - leftX, (this.vertex[this.useId[i]][1] + this.posY) - topY, (this.vertex[this.useId[(i + 1) % this.VERTEX_NUM]][0] + this.posX) - leftX, (this.vertex[this.useId[(i + 1) % this.VERTEX_NUM]][1] + this.posY) - topY, 0, y, getWidth(), y);
            if (this.rePoint.hasPoint && (reX == -1000 || this.rePoint.f2x > reX)) {
                reX = this.rePoint.f2x;
            }
        }
        return reX;
    }

    public int getCollisionYFromDown(int x) {
        int reY = -1000;
        int leftX = getLeftX();
        int topY = getTopY();
        for (int i = 0; i < this.VERTEX_NUM; i++) {
            Line.getCrossPoint(this.rePoint, (this.vertex[this.useId[i]][0] + this.posX) - leftX, (this.vertex[this.useId[i]][1] + this.posY) - topY, (this.vertex[this.useId[(i + 1) % this.VERTEX_NUM]][0] + this.posX) - leftX, (this.vertex[this.useId[(i + 1) % this.VERTEX_NUM]][1] + this.posY) - topY, x, 0, x, getHeight());
            if (this.rePoint.hasPoint && (reY == -1000 || this.rePoint.f3y > reY)) {
                reY = this.rePoint.f3y;
            }
        }
        return reY;
    }

    public int getCollisionYFromUp(int x) {
        int reY = -1000;
        int leftX = getLeftX();
        int topY = getTopY();
        for (int i = 0; i < this.VERTEX_NUM; i++) {
            Line.getCrossPoint(this.rePoint, (this.vertex[this.useId[i]][0] + this.posX) - leftX, (this.vertex[this.useId[i]][1] + this.posY) - topY, (this.vertex[this.useId[(i + 1) % this.VERTEX_NUM]][0] + this.posX) - leftX, (this.vertex[this.useId[(i + 1) % this.VERTEX_NUM]][1] + this.posY) - topY, x, 0, x, getHeight());
            if (this.rePoint.hasPoint && (reY == -1000 || this.rePoint.f3y < reY)) {
                reY = this.rePoint.f3y;
            }
        }
        return reY;
    }
}
