package com.sega.engine.lib;

public class Line {
    private static final int[] DIVISOR = new int[]{2, 3, 5, 7, 11, 13, 17, 19, 23};
    private static Line line1 = new Line();
    private static Line line2 = new Line();
    public int f4A;
    public int f5B;
    public int f6C;

    public static class CrossPoint {
        public boolean hasPoint;
        public int f2x;
        public int f3y;

        public void reset() {
            this.hasPoint = false;
        }
    }

    public Line() {}

    public Line(int x0, int y0, int x1, int y1) {
        setProperty(x0, y0, x1, y1);
    }

    public Line(int A, int B, int C) {
        this.f4A = A;
        this.f5B = B;
        this.f6C = C;
    }

    public void setProperty(int x0, int y0, int x1, int y1) {
        this.f4A = y1 - y0;
        this.f5B = x0 - x1;
        this.f6C = (this.f4A * x0) + (this.f5B * y0);
        dealDivisor();
    }

    public int getX(int y) {
        if (isHorizontal()) {
            return 0;
        }
        return (this.f6C - (this.f5B * y)) / this.f4A;
    }

    public int getY(int x) {
        if (isVertical()) {
            return 0;
        }
        return (this.f6C - (this.f4A * x)) / this.f5B;
    }

    public boolean isHorizontal() {
        if (this.f4A == 0) {
            return true;
        }
        return false;
    }

    public boolean isVertical() {
        if (this.f5B == 0) {
            return true;
        }
        return false;
    }

    public boolean isLegal() {
        if (isHorizontal() && isVertical()) {
            return false;
        }
        return true;
    }

    private void dealDivisor() {
        if (this.f4A != 0 || this.f5B != 0) {
            for (int d : DIVISOR) {
                while (this.f4A % d == 0 && this.f5B % d == 0 && this.f6C % d == 0) {
                    this.f4A /= d;
                    this.f5B /= d;
                    this.f6C /= d;
                }
            }
        }
    }

    public void getCrossPoint(CrossPoint re, int x0, int y0, int x1, int y1) {
        re.reset();
        line2.setProperty(x0, y0, x1, y1);
        getCrossPoint(re, line2);
        if (!re.hasPoint) {
            return;
        }
        if (re.f2x < Math.min(x0, x1) || re.f2x > Math.max(x0, x1) || re.f3y < Math.min(y0, y1) || re.f3y > Math.max(y0, y1)) {
            re.hasPoint = false;
        }
    }

    private void getCrossPoint(CrossPoint re, Line l) {
        re.reset();
        if (l != null && l.isLegal()) {
            if (!isHorizontal() || !l.isHorizontal()) {
                if ((!isVertical() || !l.isVertical()) && (this.f4A * l.f5B) - (this.f5B * l.f4A) != 0) {
                    int x = ((this.f6C * l.f5B) - (l.f6C * this.f5B)) / ((this.f4A * l.f5B) - (this.f5B * l.f4A));
                    int y = ((l.f6C * this.f4A) - (this.f6C * l.f4A)) / ((this.f4A * l.f5B) - (this.f5B * l.f4A));
                    re.hasPoint = true;
                    re.f2x = x;
                    re.f3y = y;
                }
            }
        }
    }

    public static void getCrossPoint(CrossPoint re, int x0, int y0, int x1, int y1, int x2, int y2, int x3, int y3) {
        re.reset();
        line1.setProperty(x0, y0, x1, y1);
        line2.setProperty(x2, y2, x3, y3);
        line1.getCrossPoint(re, line2);
        if (!re.hasPoint) {
            return;
        }
        if (re.f2x < Math.min(x0, x1) || re.f2x > Math.max(x0, x1) || re.f2x < Math.min(x2, x3) || re.f2x > Math.max(x2, x3) || re.f3y < Math.min(y0, y1) || re.f3y > Math.max(y0, y1) || re.f3y < Math.min(y2, y3) || re.f3y > Math.max(y2, y3)) {
            re.hasPoint = false;
        }
    }

    public Line getPlumbLine() {
        return new Line(-this.f5B, this.f4A, 0);
    }
}
