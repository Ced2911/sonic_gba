package com.sega.engine.action;

public interface ACWorldCollisionLimit {
    boolean noDownCollision();

    boolean noSideCollision();

    boolean noTopCollision();
}
