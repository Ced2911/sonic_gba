package com.sega.engine.action;

public abstract class ACCollision implements ACParam {
    protected ACObject acObj;
    private boolean horizontalCollision;
    private int horizontalDistance;
    private boolean isLeftCollision;
    private boolean isUpCollision;
    private int objDownHighPointY = -1000;
    private int objLeftHighPointX = -1000;
    private int objRightHighPointX = -1000;
    private int objUpHighPointY = -1000;
    private int thisDownHighPointX = -1000;
    private int thisDownHighPointY = -1000;
    private int thisLeftHighPointX = -1000;
    private int thisLeftHighPointY = -1000;
    private int thisRightHighPointX = -1000;
    private int thisRightHighPointY = -1000;
    private int thisUpHighPointX = -1000;
    private int thisUpHighPointY = -1000;
    private boolean verticalCollision;
    private int verticalDistance;
    protected int worldZoom;

    public abstract int getCollisionXFromLeft(int i);

    public abstract int getCollisionXFromRight(int i);

    public abstract int getCollisionYFromDown(int i);

    public abstract int getCollisionYFromUp(int i);

    public abstract int getHeight();

    public abstract int getLeftX();

    public abstract int getTopY();

    public abstract int getWidth();

    public abstract void update();

    public ACCollision(ACObject acObj, ACWorld worldInstance) {
        setObject(acObj);
        this.worldZoom = worldInstance.getZoom();
    }

    public void setObject(ACObject acObj) {
        this.acObj = acObj;
    }

    public ACObject getObject() {
        return this.acObj;
    }

    private void doObjCollision(ACCollision collision, int direction, int touchX, int touchY, int objTouchX, int objTouchY) {
        if (this.acObj != null && collision.getObject() != null) {
            this.acObj.doWhileCollision(collision.getObject(), this, direction, touchX, touchY, objTouchX, objTouchY);
        }
    }

    public void doCheckCollisionWithCollision(ACCollision obj, int moveDistanceX, int moveDistanceY) {
        this.acObj.doBeforeCollisionCheck();
        obj.getObject().doBeforeCollisionCheck();
        update();
        obj.update();
        this.horizontalDistance = Integer.MAX_VALUE;
        this.verticalDistance = Integer.MAX_VALUE;
        doCheckCollisionX(obj, moveDistanceX);
        doCheckCollisionY(obj, moveDistanceY);
        if (this.horizontalCollision && this.verticalCollision) {
            if (this.horizontalDistance <= this.verticalDistance) {
                doHorizontalCollision(obj);
                this.acObj.doBeforeCollisionCheck();
                obj.getObject().doBeforeCollisionCheck();
                update();
                obj.update();
                doCheckCollisionY(obj, moveDistanceY);
                if (this.verticalCollision) {
                    doVerticalCollision(obj);
                    return;
                }
                return;
            }
            doVerticalCollision(obj);
            this.acObj.doBeforeCollisionCheck();
            obj.getObject().doBeforeCollisionCheck();
            update();
            obj.update();
            doCheckCollisionX(obj, moveDistanceX);
            if (this.horizontalCollision) {
                doHorizontalCollision(obj);
            }
        } else if (this.horizontalCollision) {
            doHorizontalCollision(obj);
        } else if (this.verticalCollision) {
            doVerticalCollision(obj);
        }
    }

    private void doHorizontalCollision(ACCollision obj) {
        if (this.isLeftCollision) {
            doObjCollision(obj, 3, this.thisLeftHighPointX, this.thisLeftHighPointY, this.objLeftHighPointX, this.thisLeftHighPointY);
            obj.doObjCollision(this, 1, this.objLeftHighPointX, this.thisLeftHighPointY, this.thisLeftHighPointX, this.thisLeftHighPointY);
            return;
        }
        doObjCollision(obj, 1, this.thisRightHighPointX, this.thisRightHighPointY, this.objRightHighPointX, this.thisRightHighPointY);
        obj.doObjCollision(this, 3, this.objRightHighPointX, this.thisRightHighPointY, this.thisRightHighPointX, this.thisRightHighPointY);
    }

    private void doVerticalCollision(ACCollision obj) {
        if (this.isUpCollision) {
            doObjCollision(obj, 0, this.thisUpHighPointX, this.thisUpHighPointY, this.thisUpHighPointX, this.objUpHighPointY);
            obj.doObjCollision(this, 2, this.thisUpHighPointX, this.objUpHighPointY, this.thisUpHighPointX, this.thisUpHighPointY);
            return;
        }
        doObjCollision(obj, 2, this.thisDownHighPointX, this.thisDownHighPointY, this.thisDownHighPointX, this.objDownHighPointY);
        obj.doObjCollision(this, 0, this.thisDownHighPointX, this.objDownHighPointY, this.thisDownHighPointX, this.thisDownHighPointY);
    }

    private void doCheckCollisionX(ACCollision obj, int moveDistanceX) {
        this.horizontalCollision = false;
        if (getLeftX() + getWidth() >= obj.getLeftX() && getLeftX() <= obj.getLeftX() + obj.getWidth()) {
            int topY = Math.max(getTopY(), obj.getTopY());
            int bottomY = Math.min(getTopY() + getHeight(), obj.getTopY() + obj.getHeight());
            this.thisLeftHighPointX = -1000;
            this.thisRightHighPointX = -1000;
            this.thisLeftHighPointY = -1000;
            this.objLeftHighPointX = -1000;
            this.objRightHighPointX = -1000;
            this.thisRightHighPointY = -1000;
            int leftMaxDistance = 0;
            int rightMaxDistance = 0;
            int i = topY;
            while (i < bottomY) {
                int thisLeftX = getCollisionX(i, 3);
                int objRightX = obj.getCollisionX(i, (3 + 2) % 4);
                int thisRightX = getCollisionX(i, (3 + 2) % 4);
                int objLeftX = obj.getCollisionX(i, 3);
                if (thisLeftX != -1000 && objRightX != -1000 && thisLeftX <= objRightX && (this.thisLeftHighPointX == -1000 || Math.abs(thisLeftX - objRightX) > leftMaxDistance)) {
                    this.thisLeftHighPointX = thisLeftX;
                    this.objLeftHighPointX = objRightX;
                    leftMaxDistance = Math.abs(thisLeftX - objRightX);
                    this.thisLeftHighPointY = i;
                }
                if (thisRightX != -1000 && objLeftX != -1000 && thisRightX >= objLeftX && (this.thisRightHighPointX == -1000 || Math.abs(thisRightX - objLeftX) > rightMaxDistance)) {
                    this.thisRightHighPointX = thisRightX;
                    this.objRightHighPointX = objLeftX;
                    rightMaxDistance = Math.abs(thisRightX - objLeftX);
                    this.thisRightHighPointY = i;
                }
                i += 1 << this.worldZoom;
            }
            boolean hasCollision = true;
            boolean isLeftCollision = true;
            if (this.thisLeftHighPointX == -1000 || this.thisRightHighPointX == -1000) {
                hasCollision = false;
            } else if (leftMaxDistance == rightMaxDistance) {
                if (moveDistanceX >= 0) {
                    isLeftCollision = false;
                } else {
                    isLeftCollision = true;
                }
            } else if (leftMaxDistance < rightMaxDistance) {
                isLeftCollision = true;
            } else {
                isLeftCollision = false;
            }
            if (hasCollision) {
                this.horizontalCollision = true;
                this.isLeftCollision = isLeftCollision;
                if (isLeftCollision) {
                    this.horizontalDistance = leftMaxDistance;
                } else {
                    this.horizontalDistance = rightMaxDistance;
                }
            }
        }
    }

    private void doCheckCollisionY(ACCollision obj, int moveDistanceY) {
        this.verticalCollision = false;
        if (getTopY() + getHeight() >= obj.getTopY() && getTopY() <= obj.getTopY() + obj.getHeight()) {
            int rightX;
            int leftX = getLeftX() > obj.getLeftX() ? getLeftX() : obj.getLeftX();
            if (getLeftX() + getWidth() < obj.getLeftX() + obj.getWidth()) {
                rightX = getLeftX() + getWidth();
            } else {
                rightX = obj.getLeftX() + obj.getWidth();
            }
            this.thisUpHighPointY = -1000;
            this.objUpHighPointY = -1000;
            this.thisUpHighPointX = -1000;
            this.thisDownHighPointY = -1000;
            this.objDownHighPointY = -1000;
            this.thisDownHighPointX = -1000;
            int upMaxDistance = 0;
            int downMaxDistance = 0;
            int i = leftX;
            while (i < rightX) {
                int thisUpY = getCollisionY(i, 0);
                int thisDownY = getCollisionY(i, (0 + 2) % 4);
                int objUpY = obj.getCollisionY(i, 0);
                int objDownY = obj.getCollisionY(i, (0 + 2) % 4);
                if (thisUpY != -1000 && objDownY != -1000 && thisUpY <= objDownY && (this.thisUpHighPointY == -1000 || Math.abs(thisUpY - objDownY) > upMaxDistance)) {
                    this.thisUpHighPointY = thisUpY;
                    this.objUpHighPointY = objDownY;
                    upMaxDistance = Math.abs(thisUpY - objDownY);
                    this.thisUpHighPointX = i;
                }
                if (thisDownY != -1000 && objUpY != -1000 && thisDownY >= objUpY && (this.thisDownHighPointY == -1000 || Math.abs(thisDownY - objUpY) > downMaxDistance)) {
                    this.thisDownHighPointY = thisDownY;
                    this.objDownHighPointY = objUpY;
                    downMaxDistance = Math.abs(thisDownY - objUpY);
                    this.thisDownHighPointX = i;
                }
                i += 1 << this.worldZoom;
            }
            if (this.thisUpHighPointY != -1000 && this.thisDownHighPointY != -1000) {
                boolean isUpCollision;
                if (upMaxDistance == downMaxDistance) {
                    if (moveDistanceY >= 0) {
                        isUpCollision = false;
                    } else {
                        isUpCollision = true;
                    }
                } else if (upMaxDistance < downMaxDistance) {
                    isUpCollision = true;
                } else {
                    isUpCollision = false;
                }
                this.verticalCollision = true;
                this.isUpCollision = isUpCollision;
                if (isUpCollision) {
                    this.verticalDistance = upMaxDistance;
                } else {
                    this.verticalDistance = downMaxDistance;
                }
            }
        }
    }

    public int getCollisionX(int y, int direction) {
        if (y < getTopY() || y >= getTopY() + getHeight()) {
            return -1000;
        }
        y -= getTopY();
        int refX = -1000;
        switch (direction) {
            case 1:
                refX = getCollisionXFromRight(y);
                break;
            case 3:
                refX = getCollisionXFromLeft(y);
                break;
        }
        if (refX != -1000) {
            return getLeftX() + refX;
        }
        return -1000;
    }

    public int getCollisionY(int x, int direction) {
        if (x < getLeftX() || x >= getLeftX() + getWidth()) {
            return -1000;
        }
        x -= getLeftX();
        int refY = -1000;
        switch (direction) {
            case 0:
                refY = getCollisionYFromUp(x);
                break;
            case 2:
                refY = getCollisionYFromDown(x);
                break;
        }
        if (refY != -1000) {
            return getTopY() + refY;
        }
        return -1000;
    }
}
