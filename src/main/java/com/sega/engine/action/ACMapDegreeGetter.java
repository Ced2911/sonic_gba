package com.sega.engine.action;

import State.StringIndex;
import com.sega.engine.action.ACDegreeGetter.DegreeReturner;
import com.sega.engine.lib.CrlFP32;
import com.sega.mobile.define.MDPhone;
import SonicGBA.RollPlatformSpeedC;

public class ACMapDegreeGetter extends ACDegreeGetter implements ACParam {
    private int searchRange;
    private ACWorld worldInstance;
    private int zoom;

    public ACMapDegreeGetter(ACWorld worldInstance, int searchRange) {
        this.worldInstance = worldInstance;
        this.searchRange = searchRange;
        this.zoom = worldInstance.getZoom();
    }

    public void getDegreeFromWorldByPosition(DegreeReturner re, int currentDegree, int x, int y, int z) {
        int groundDirection = getDirectionByDegree(currentDegree);
        boolean upDownFirst = groundDirection == 0 || groundDirection == 2;
        if (upDownFirst) {
            getDegreeByPositionY(re, null, currentDegree, x, y, z);
            if (re.degreeCalSuccess) {
                return;
            }
        }
        getDegreeByPositionX(re, null, currentDegree, x, y, z);
        if (!re.degreeCalSuccess && !upDownFirst) {
            getDegreeByPositionY(re, null, currentDegree, x, y, z);
            if (!re.degreeCalSuccess) {
            }
        }
    }

    public void getDegreeFromCollisionByPosition(DegreeReturner re, ACCollision obj, int currentDegree, int x, int y, int z) {
        int groundDirection = getDirectionByDegree(currentDegree);
        boolean upDownFirst = groundDirection == 0 || groundDirection == 2;
        if (upDownFirst) {
            getDegreeByPositionY(re, obj, currentDegree, x, y, z);
            if (re.degreeCalSuccess) {
                return;
            }
        }
        getDegreeByPositionX(re, obj, currentDegree, x, y, z);
        if (!re.degreeCalSuccess && !upDownFirst) {
            getDegreeByPositionY(re, obj, currentDegree, x, y, z);
            if (!re.degreeCalSuccess) {
            }
        }
    }

    private int getDirectionByDegree(int degree) {
        while (degree < 0) {
            degree += MDPhone.SCREEN_WIDTH;
        }
        degree %= MDPhone.SCREEN_WIDTH;
        if (degree >= 315 || degree <= 45) {
            return 0;
        }
        if (degree > 225 && degree < 315) {
            return 3;
        }
        if (degree < StringIndex.FONT_COLON_RED || degree > 225) {
            return 1;
        }
        return 2;
    }

    private int getDegreeByTwoPoint(int px1, int py1, int px2, int py2, int degree) {
        if (px1 == px2 && py1 == py2) {
            return degree;
        }
        int re = CrlFP32.actTanDegree(py2 - py1, px2 - px1);
        int degreeDiff = Math.abs(degree - re);
        if (degreeDiff > RollPlatformSpeedC.DEGREE_VELOCITY) {
            degreeDiff = MDPhone.SCREEN_WIDTH - degreeDiff;
        }
        if (degreeDiff > 90) {
            re = (re + RollPlatformSpeedC.DEGREE_VELOCITY) % MDPhone.SCREEN_WIDTH;
        }
        return re;
    }

    private int getDegreeDiff(int degree1, int degree2) {
        int degreeDiff = Math.abs(degree1 - degree2);
        if (degreeDiff > RollPlatformSpeedC.DEGREE_VELOCITY) {
            return MDPhone.SCREEN_WIDTH - degreeDiff;
        }
        return degreeDiff;
    }

    private void getDegreeByPositionY(DegreeReturner re, ACCollision obj, int currentDegree, int x, int y, int z) {
        int groundDirection = 0;
        if (currentDegree > 90 && currentDegree < 270) {
            groundDirection = 2;
        }
        getDegreeByPositionAndDirection(re, obj, currentDegree, x, y, z, groundDirection);
    }

    private void getDegreeByPositionX(DegreeReturner re, ACCollision obj, int currentDegree, int x, int y, int z) {
        int groundDirection = 3;
        if (currentDegree > 0 && currentDegree < RollPlatformSpeedC.DEGREE_VELOCITY) {
            groundDirection = 1;
        }
        getDegreeByPositionAndDirection(re, obj, currentDegree, x, y, z, groundDirection);
    }

    private void getDegreeByPositionAndDirection(DegreeReturner re, ACCollision obj, int currentDegree, int x, int y, int z, int direction) {
        if (obj != null) {
        }
        re.reset(x, y, currentDegree);
        int searchLeftY;
        boolean findSearchPointLeft;
        int searchLeftX;
        int calOffset;
        int searchRightY;
        boolean findSearchPointRight;
        int searchRightX;
        switch (direction) {
            case 0:
            case 2:
                int directionV;
                if (direction == 0) {
                    directionV = 1;
                } else {
                    directionV = -1;
                }
                int newY = getNewY(x, y, z, direction, obj);
                if (newY != -1000) {
                    int searchRightLimitY;
                    y = newY;
                    searchLeftY = y + (this.searchRange * directionV);
                    findSearchPointLeft = false;
                    searchLeftX = x - this.searchRange;
                    while (searchLeftX < x) {
                        calOffset = (this.searchRange * (x - searchLeftX)) / this.searchRange;
                        searchLeftY = y + (directionV * calOffset);
                        int searchLeftLimitY = y - (directionV * calOffset);
                        newY = getNewY(searchLeftX, searchLeftY, z, direction, obj);
                        if (newY == -1000 || ((direction != 0 || newY < searchLeftLimitY) && (direction != 2 || newY > searchLeftLimitY))) {
                            searchLeftX += 1 << this.zoom;
                        } else {
                            findSearchPointLeft = true;
                            searchLeftY = newY;
                            searchRightY = y + (this.searchRange * directionV);
                            findSearchPointRight = false;
                            searchRightX = x + this.searchRange;
                            while (searchRightX > x) {
                                calOffset = (this.searchRange * (searchRightX - x)) / this.searchRange;
                                searchRightY = y + (directionV * calOffset);
                                searchRightLimitY = y - (directionV * calOffset);
                                newY = getNewY(searchRightX, searchRightY, z, direction, obj);
                                if (newY != -1000 || ((direction != 0 || newY < searchRightLimitY) && (direction != 2 || newY > searchRightLimitY))) {
                                    searchRightX -= 1 << this.zoom;
                                } else {
                                    findSearchPointRight = true;
                                    searchRightY = newY;
                                    if (!findSearchPointLeft || findSearchPointRight) {
                                        re.degreeCalSuccess = true;
                                        re.newX = x;
                                        re.newY = y;
                                        re.degree = calDegreeByAllPoints(searchLeftX, searchLeftY, searchRightX, searchRightY, x, y, findSearchPointLeft, findSearchPointRight, currentDegree);
                                        return;
                                    }
                                    return;
                                }
                            }
                            if (findSearchPointLeft) {
                            }
                            re.degreeCalSuccess = true;
                            re.newX = x;
                            re.newY = y;
                            re.degree = calDegreeByAllPoints(searchLeftX, searchLeftY, searchRightX, searchRightY, x, y, findSearchPointLeft, findSearchPointRight, currentDegree);
                            return;
                        }
                    }
                    searchRightY = y + (this.searchRange * directionV);
                    findSearchPointRight = false;
                    searchRightX = x + this.searchRange;
                    while (searchRightX > x) {
                        calOffset = (this.searchRange * (searchRightX - x)) / this.searchRange;
                        searchRightY = y + (directionV * calOffset);
                        searchRightLimitY = y - (directionV * calOffset);
                        newY = getNewY(searchRightX, searchRightY, z, direction, obj);
                        if (newY != -1000) {
                            break;
                        }
                        searchRightX -= 1 << this.zoom;
                    }
                    if (findSearchPointLeft) {
                    }
                    re.degreeCalSuccess = true;
                    re.newX = x;
                    re.newY = y;
                    re.degree = calDegreeByAllPoints(searchLeftX, searchLeftY, searchRightX, searchRightY, x, y, findSearchPointLeft, findSearchPointRight, currentDegree);
                    return;
                }
                return;
            case 1:
            case 3:
                int directionH;
                if (direction == 3) {
                    directionH = 1;
                } else {
                    directionH = -1;
                }
                int newX = getNewX(x, y, z, direction, obj);
                if (newX != -1000) {
                    int searchRightLimitX;
                    x = newX;
                    searchLeftX = 0;
                    findSearchPointLeft = false;
                    searchLeftY = y - this.searchRange;
                    while (searchLeftY < y) {
                        calOffset = (this.searchRange * (y - searchLeftY)) / this.searchRange;
                        searchLeftX = x + (directionH * calOffset);
                        int searchLeftLimitX = x - (directionH * calOffset);
                        newX = getNewX(searchLeftX, searchLeftY, z, direction, obj);
                        if (newX == -1000 || ((direction != 3 || newX < searchLeftLimitX) && (direction != 1 || newX > searchLeftLimitX))) {
                            searchLeftY += 1 << this.zoom;
                        } else {
                            findSearchPointLeft = true;
                            searchLeftX = newX;
                            searchRightX = 0;
                            findSearchPointRight = false;
                            searchRightY = y + this.searchRange;
                            while (searchRightY > y) {
                                calOffset = (this.searchRange * (searchRightY - y)) / this.searchRange;
                                searchRightX = x + (directionH * calOffset);
                                searchRightLimitX = x - (directionH * calOffset);
                                newX = getNewX(searchRightX, searchRightY, z, direction, obj);
                                if (newX != -1000 || ((direction != 3 || newX < searchRightLimitX) && (direction != 1 || newX > searchRightLimitX))) {
                                    searchRightY -= 1 << this.zoom;
                                } else {
                                    findSearchPointRight = true;
                                    searchRightX = newX;
                                    if (!findSearchPointLeft || findSearchPointRight) {
                                        re.degreeCalSuccess = true;
                                        re.newX = x;
                                        re.newY = y;
                                        re.degree = calDegreeByAllPoints(searchLeftX, searchLeftY, searchRightX, searchRightY, x, y, findSearchPointLeft, findSearchPointRight, currentDegree);
                                        return;
                                    }
                                    return;
                                }
                            }
                            if (findSearchPointLeft) {
                            }
                            re.degreeCalSuccess = true;
                            re.newX = x;
                            re.newY = y;
                            re.degree = calDegreeByAllPoints(searchLeftX, searchLeftY, searchRightX, searchRightY, x, y, findSearchPointLeft, findSearchPointRight, currentDegree);
                            return;
                        }
                    }
                    searchRightX = 0;
                    findSearchPointRight = false;
                    searchRightY = y + this.searchRange;
                    while (searchRightY > y) {
                        calOffset = (this.searchRange * (searchRightY - y)) / this.searchRange;
                        searchRightX = x + (directionH * calOffset);
                        searchRightLimitX = x - (directionH * calOffset);
                        newX = getNewX(searchRightX, searchRightY, z, direction, obj);
                        if (newX != -1000) {
                            break;
                        }
                        searchRightY -= 1 << this.zoom;
                    }
                    if (findSearchPointLeft) {
                    }
                    re.degreeCalSuccess = true;
                    re.newX = x;
                    re.newY = y;
                    re.degree = calDegreeByAllPoints(searchLeftX, searchLeftY, searchRightX, searchRightY, x, y, findSearchPointLeft, findSearchPointRight, currentDegree);
                    return;
                }
                return;
            default:
                return;
        }
    }

    private int calDegreeByAllPoints(int searchLeftX, int searchLeftY, int searchRightX, int searchRightY, int x, int y, boolean findSearchPointLeft, boolean findSearchPointRight, int currentDegree) {
        int re = currentDegree;
        int leftDegree = currentDegree;
        if (findSearchPointLeft) {
            leftDegree = getDegreeByTwoPoint(searchLeftX, searchLeftY, x, y, currentDegree);
        }
        int rightDegree = currentDegree;
        if (findSearchPointRight) {
            rightDegree = getDegreeByTwoPoint(searchRightX, searchRightY, x, y, currentDegree);
        }
        if (findSearchPointLeft && findSearchPointRight) {
            if (getDegreeDiff(leftDegree, currentDegree) <= getDegreeDiff(rightDegree, currentDegree)) {
                re = leftDegree;
            } else {
                re = rightDegree;
            }
        } else if (findSearchPointLeft) {
            re = leftDegree;
        } else if (findSearchPointRight) {
            re = rightDegree;
        }
        while (re < 0) {
            re += MDPhone.SCREEN_WIDTH;
        }
        return re % MDPhone.SCREEN_WIDTH;
    }

    private int getNewY(int x, int y, int z, int direction, ACCollision obj) {
        if (obj != null) {
            return obj.getCollisionY(x, direction);
        }
        return this.worldInstance.getWorldY(x, y, z, direction);
    }

    private int getNewX(int x, int y, int z, int direction, ACCollision obj) {
        if (obj != null) {
            return obj.getCollisionX(x, direction);
        }
        return this.worldInstance.getWorldX(x, y, z, direction);
    }
}
