package com.sega.engine.action;

public interface ACParam {
    public static final int DIRECTION_DOWN = 2;
    public static final int DIRECTION_LEFT = 3;
    public static final int DIRECTION_NUM = 4;
    public static final int DIRECTION_OPPOSITE_OFFSET = 2;
    public static final int DIRECTION_RIGHT = 1;
    public static final int DIRECTION_UP = 0;
    public static final int NO_COLLISION = -1000;
}
