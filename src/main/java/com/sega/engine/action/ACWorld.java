package com.sega.engine.action;

public abstract class ACWorld implements ACParam {
    private static final int MAX_SEARCH_BLOCK = 20;
    private ACBlock getBlock;
    private ACBlock nextBlock;

    public abstract void getCollisionBlock(ACBlock aCBlock, int i, int i2, int i3);

    public abstract ACDegreeGetter getDegreeGetterForObject();

    public abstract ACBlock getNewCollisionBlock();

    public abstract int getTileHeight();

    public abstract int getTileWidth();

    public abstract int getWorldHeight();

    public abstract int getWorldWidth();

    public abstract int getZoom();

    public int getWorldY(int x, int y, int z, int direction) {
        if (direction == 1 || direction == 3) {
            return -1000;
        }
        if (this.getBlock == null) {
            this.getBlock = getNewCollisionBlock();
        }
        if (this.nextBlock == null) {
            this.nextBlock = getNewCollisionBlock();
        }
        int fullCollisionParam = (ACUtilities.getQuaParam(y, getTileHeight()) * getTileHeight()) + (direction == 0 ? 0 : ACBlock.downSide);
        int originalY = y;
        getCollisionBlock(this.getBlock, x, y, z);
        int blockPixOppositeY = this.getBlock.getCollisionY(y, (direction + 2) % 4);
        if (blockPixOppositeY != -1000 && ((direction == 0 && blockPixOppositeY < originalY) || (direction == 2 && blockPixOppositeY > originalY))) {
            return -1000;
        }
        int blockPixY = this.getBlock.getCollisionY(x, direction);
        int nextBlockPixY = blockPixY;
        int searchCount = 0;
        fullCollisionParam = (ACUtilities.getQuaParam(y, getTileHeight()) * getTileHeight()) + (direction == 0 ? 0 : ACBlock.downSide);
        while (nextBlockPixY != -1000 && searchCount < MAX_SEARCH_BLOCK) {
            y += (direction == 0 ? -1 : 1) * getTileHeight();
            getCollisionBlock(this.getBlock, x, y, z);
            blockPixY = this.getBlock.getCollisionY(x, direction);
            searchCount++;
            fullCollisionParam = (ACUtilities.getQuaParam(y, getTileHeight()) * getTileHeight()) + (direction == 0 ? 0 : ACBlock.downSide);
            getCollisionBlock(this.nextBlock, x, ((direction == 0 ? -1 : 1) * getTileHeight()) + y, z);
            nextBlockPixY = this.nextBlock.getCollisionY(x, direction);
        }
        if (blockPixY == -1000) {
            for (int i = 0; i < 1; i++) {
                y -= (direction == 0 ? -1 : 1) * getTileHeight();
                getCollisionBlock(this.getBlock, x, y, z);
                blockPixY = this.getBlock.getCollisionY(x, direction);
                if (blockPixY != -1000) {
                    break;
                }
            }
        }
        if (blockPixY != -1000) {
            int reY = blockPixY;
            if ((direction == 0 && reY <= originalY) || (direction == 2 && reY >= originalY)) {
                return reY;
            }
        }
        return -1000;
    }

    public int getWorldX(int x, int y, int z, int direction) {
        if (direction == 0 || direction == 2) {
            return -1000;
        }
        if (this.getBlock == null) {
            this.getBlock = getNewCollisionBlock();
        }
        if (this.nextBlock == null) {
            this.nextBlock = getNewCollisionBlock();
        }
        int fullCollisionParam = (ACUtilities.getQuaParam(x, getTileWidth()) * getTileWidth()) + (direction == 3 ? 0 : ACBlock.rightSide);
        int originalX = x;
        getCollisionBlock(this.getBlock, x, y, z);
        int blockPixOppositeX = this.getBlock.getCollisionX(y, (direction + 2) % 4);
        if (blockPixOppositeX != -1000 && ((direction == 3 && blockPixOppositeX < originalX) || (direction == 1 && blockPixOppositeX > originalX))) {
            return -1000;
        }
        int blockPixX = this.getBlock.getCollisionX(y, direction);
        int nextBlockPixX = blockPixX;
        int searchCount = 0;
        fullCollisionParam = (ACUtilities.getQuaParam(x, getTileWidth()) * getTileWidth()) + (direction == 3 ? 0 : ACBlock.rightSide);
        while (nextBlockPixX != -1000 && searchCount < MAX_SEARCH_BLOCK) {
            x += (direction == 3 ? -1 : 1) * getTileWidth();
            getCollisionBlock(this.getBlock, x, y, z);
            blockPixX = this.getBlock.getCollisionX(y, direction);
            searchCount++;
            fullCollisionParam = (ACUtilities.getQuaParam(x, getTileWidth()) * getTileWidth()) + (direction == 3 ? 0 : ACBlock.rightSide);
            getCollisionBlock(this.nextBlock, ((direction == 3 ? -1 : 1) * getTileWidth()) + x, y, z);
            nextBlockPixX = this.nextBlock.getCollisionX(y, direction);
        }
        if (blockPixX == -1000) {
            for (int i = 0; i < 1; i++) {
                x -= (direction == 3 ? -1 : 1) * getTileWidth();
                getCollisionBlock(this.getBlock, x, y, z);
                blockPixX = this.getBlock.getCollisionX(y, direction);
                if (blockPixX != -1000) {
                    break;
                }
            }
        }
        if (blockPixX != -1000) {
            int reX = blockPixX;
            if ((direction == 3 && reX <= originalX) || (direction == 1 && reX >= originalX)) {
                return reX;
            }
        }
        return -1000;
    }
}
