package com.sega.engine.action;

import java.util.Vector;

public abstract class ACObject implements ACParam {
    private Vector collisionVec = new Vector();
    protected int height;
    public int posX;
    public int posY;
    protected int posZ;
    public int velX;
    public int velY;
    protected int width;
    protected ACWorld worldInstance;
    protected final int worldZoom;

    public abstract void doBeforeCollisionCheck();

    public abstract void doWhileCollision(ACObject aCObject, ACCollision aCCollision, int i, int i2, int i3, int i4, int i5);

    public ACObject(ACWorld worldInstance) {
        this.worldInstance = worldInstance;
        this.worldZoom = worldInstance.getZoom();
    }

    public void addCollision(ACCollision collision) {
        this.collisionVec.addElement(collision);
    }

    public void removeCollision(int index) {
        if (index >= 0 && index < this.collisionVec.size()) {
            this.collisionVec.removeElementAt(index);
        }
    }

    public Vector getCollisionVec() {
        return this.collisionVec;
    }

    public void setPosition(int x, int y) {
        this.posX = x;
        this.posY = y;
    }

    public void setRect(int w, int h) {
        this.width = w;
        this.height = h;
    }

    public int getObjWidth() {
        return this.width;
    }

    public int getObjHeight() {
        return this.height;
    }

    public ACWorld getWorld() {
        return this.worldInstance;
    }

    public int getX() {
        return this.posX;
    }

    public int getY() {
        return this.posY;
    }

    public void doCheckCollisionWithObj(ACObject obj, int moveDistanceX, int moveDistanceY) {
        for (int i = 0; i < this.collisionVec.size(); i++) {
            ACCollision element = (ACCollision) this.collisionVec.elementAt(i);
            Vector objCollisionVec = obj.getCollisionVec();
            for (int j = 0; j < objCollisionVec.size(); j++) {
                element.doCheckCollisionWithCollision((ACCollision) objCollisionVec.elementAt(j), moveDistanceX, moveDistanceY);
            }
        }
    }
}
