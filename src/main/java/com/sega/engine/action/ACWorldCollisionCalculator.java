package com.sega.engine.action;

import SonicGBA.RollPlatformSpeedC;
import State.StringIndex;
import com.sega.engine.action.ACDegreeGetter.DegreeReturner;
import com.sega.engine.lib.CrlFP32;
import com.sega.engine.lib.MyAPI;
import com.sega.mobile.define.MDPhone;

public class ACWorldCollisionCalculator extends ACMoveCalculator implements ACParam {
    private static final int BLOCK_CHECK_RANGE = 1;
    private static final int DIRECTION_OFFSET_DOWN = 0;
    private static final int DIRECTION_OFFSET_LEFT = 1;
    private static final int DIRECTION_OFFSET_RIGHT = 3;
    private static final int DIRECTION_OFFSET_UP = 2;
    private static final int DOWN_SEARCH_BLOCK = 8;
    public static final byte JUMP_ACTION_STATE = (byte) 1;
    public static final byte WALK_ACTION_STATE = (byte) 0;
    public byte actionState;
    private int bodyCollisionPointOffsetX;
    private int[] bodyCollisionPointOffsetY;
    private int[] checkArrayForShowX;
    private int[] checkArrayForShowY;
    private int chkOffsetX;
    private int chkOffsetY;
    private int chkPointDegree;
    private int chkPointId;
    private int chkPointX;
    private int chkPointY;
    private ACCollisionData collisionData;
    protected ACDegreeGetter degreeGetter;
    private DegreeReturner degreeRe = new DegreeReturner();
    private int[] footCollisionPointOffsetX;
    private int footCollisionPointOffsetY;
    private int[] footCollisionPointResaultX;
    private int[] footCollisionPointResaultY;
    public int footDegree;
    private int footOffsetX;
    private int footOffsetY;
    private int footX;
    private int footY;
    protected ACBlock getBlock;
    private int[] headCollisionPointOffsetX;
    private int headCollisionPointOffsetY;
    private boolean isMoved;
    private int lastMoveDistanceX = -9999;
    private int lastMoveDistanceY = -9999;
    private ACWorldCollisionLimit limit;
    private int movePassiveX;
    private int movePassiveY;
    private int preBodyOffset = -1;
    private int preFootOffset = -1;
    private int preHeight = -1;
    private int preWidth = -1;
    private int priorityChkId;
    private int totalDistance;
    private ACWorldCalUser user;

    public ACWorldCollisionCalculator(ACObject acObj, ACWorldCalUser user) {
        super(acObj, user);
        this.user = user;
        this.degreeGetter = this.worldInstance.getDegreeGetterForObject();
        this.actionState = (byte) 1;
        calPosition(acObj.getObjWidth(), acObj.getObjHeight(), user.getFootOffset(), user.getBodyOffset());
        if (this.getBlock == null) {
            this.getBlock = this.worldInstance.getNewCollisionBlock();
        }
        this.collisionData = new ACCollisionData();
    }

    public void setLimit(ACWorldCollisionLimit limit) {
        this.limit = limit;
    }

    private void calPosition(int collisionWidth, int collisionHeight, int footOffset, int bodyOffset) {
        int i;
        this.footCollisionPointOffsetX = new int[3];
        this.footCollisionPointResaultX = new int[3];
        this.footCollisionPointResaultY = new int[3];
        this.footCollisionPointOffsetX[0] = -((collisionWidth - (footOffset * 2)) >> 1);
        this.footCollisionPointOffsetX[3 - 1] = (collisionWidth - (footOffset * 2)) >> 1;
        this.priorityChkId = 1;
        if (3 > 2) {
            int footSpace = (collisionWidth - (footOffset * 2)) / (3 - 1);
            for (i = 1; i < 3 - 1; i++) {
                this.footCollisionPointOffsetX[i] = this.footCollisionPointOffsetX[i - 1] + footSpace;
            }
        }
        this.footCollisionPointOffsetY = 0;
        this.headCollisionPointOffsetX = this.footCollisionPointOffsetX;
        this.headCollisionPointOffsetY = -collisionHeight;
        this.bodyCollisionPointOffsetX = collisionWidth >> 1;
        int bodyNum = (((collisionHeight - (bodyOffset * 2)) - 1) / this.worldInstance.getTileHeight()) + 2;
        this.bodyCollisionPointOffsetY = new int[bodyNum];
        this.bodyCollisionPointOffsetY[0] = -bodyOffset;
        this.bodyCollisionPointOffsetY[bodyNum - 1] = (-collisionHeight) + bodyOffset;
        if (bodyNum > 2) {
            int bodySpace = (collisionHeight - (bodyOffset * 2)) / (bodyNum - 1);
            for (i = 1; i < bodyNum - 1; i++) {
                this.bodyCollisionPointOffsetY[i] = this.bodyCollisionPointOffsetY[i - 1] - bodySpace;
            }
        }
    }

    public void changeSize(int collisionWidth, int collisionHeight, int footOffset, int bodyOffset) {
        if (bodyOffset < this.worldInstance.getTileHeight()) {
            bodyOffset = this.worldInstance.getTileHeight();
        }
        if (collisionWidth != this.preWidth || collisionHeight != this.preHeight || footOffset != this.preFootOffset || bodyOffset != this.preBodyOffset) {
            calPosition(collisionWidth, collisionHeight, footOffset, bodyOffset);
            this.preWidth = collisionWidth;
            this.preHeight = collisionHeight;
            this.preFootOffset = footOffset;
            this.preBodyOffset = bodyOffset;
        }
    }

    public void actionLogic(int moveDistanceX, int moveDistanceY) {
        actionLogic(moveDistanceX, moveDistanceY, ((MyAPI.dCos(this.footDegree) * moveDistanceX) + (MyAPI.dSin(this.footDegree) * moveDistanceY)) / 100);
    }

    public void actionLogic(int moveDistanceX, int moveDistanceY, int totalVelocity) {
        changeSize(this.acObj.getObjWidth(), this.acObj.getObjHeight(), this.user.getFootOffset(), this.user.getBodyOffset());
        this.footX = this.user.getFootX();
        this.footY = this.user.getFootY();
        this.moveDistanceX = moveDistanceX;
        this.moveDistanceY = moveDistanceY;
        switch (this.actionState) {
            case (byte) 0:
                this.totalDistance = totalVelocity;
                break;
        }
        try {
            checkInMap();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setMovedState(boolean state) {
        this.isMoved = state;
    }

    public boolean getMovedState() {
        return this.isMoved;
    }

    private void checkInMap() {
        if (this.moveDistanceX == 0 && this.moveDistanceY == 0 && !this.isMoved) {
            findTheFootPoint();
            this.user.didAfterEveryMove(0, 0);
        }
        do {
            if (this.moveDistanceX != 0 || this.moveDistanceY != 0 || this.isMoved) {
                this.footX = this.user.getFootX();
                this.footY = this.user.getFootY();
                this.footOffsetX = this.footX - this.acObj.posX;
                this.footOffsetY = this.footY - this.acObj.posY;
                findTheFootPoint();
                int preFootX = this.footX;
                int preFootY = this.footY;
                switch (this.actionState) {
                    case (byte) 0:
                        checkInGround();
                        break;
                    case (byte) 1:
                        checkInSky();
                        break;
                }
                this.acObj.posX = this.footX - this.footOffsetX;
                this.acObj.posY = this.footY - this.footOffsetY;
                this.user.didAfterEveryMove(this.footX - preFootX, this.footY - preFootY);
            } else {
                return;
            }
        } while (!this.isMoved);
    }

    private void checkInGround() {
        moveToNextPosition();
    }

    private void checkInSky() {
        int newX;
        int degree;
        int plumbDegree;
        int landDegree = 0;
        int tangentDistance;
        int tangentVel;
        boolean xFirst = Math.abs(this.moveDistanceX) > Math.abs(this.moveDistanceY);
        int startPointX = this.chkPointX;
        int startPointY = this.chkPointY;
        if (xFirst) {
            if (Math.abs(this.moveDistanceX) > this.worldInstance.getTileWidth() - (1 << this.worldInstance.getZoom())) {
                this.chkPointX += (this.moveDistanceX > 0 ? 1 : -1) * (this.worldInstance.getTileWidth() - (1 << this.worldInstance.getZoom()));
                this.chkPointY += (this.moveDistanceY * (this.worldInstance.getTileWidth() - (1 << this.worldInstance.getZoom()))) / Math.abs(this.moveDistanceX);
            } else {
                this.chkPointX += this.moveDistanceX;
                this.chkPointY += this.moveDistanceY;
            }
        } else {
            if (Math.abs(this.moveDistanceY) > this.worldInstance.getTileHeight() - (1 << this.worldInstance.getZoom())) {
                this.chkPointY += (this.moveDistanceY > 0 ? 1 : -1) * (this.worldInstance.getTileHeight() - (1 << this.worldInstance.getZoom()));
                this.chkPointX += (this.moveDistanceX * (this.worldInstance.getTileHeight() - (1 << this.worldInstance.getZoom()))) / Math.abs(this.moveDistanceY);
            } else {
                this.chkPointX += this.moveDistanceX;
                this.chkPointY += this.moveDistanceY;
            }
        }
        calObjPositionFromFoot();
        boolean sideCollision = false;
        int sideOffset = 0;
        int sideCollisionDirection = 1;
        int sideCollisionDegree = 0;
        int sideNewX = 0;
        boolean headCollision = false;
        int bodyOffset = 0;
        int bodyNewY = 0;
        int collisionDegree = 0;
        int footChkPointID = 0;
        int footChkDegree = 0;
        boolean footCollision = false;
        int skyDirection = getDirectionByDegree(this.footDegree);
        boolean isVertical = skyDirection == 1 || skyDirection == 3;
        rightSideCollisionChk(this.footX, this.footY, skyDirection, this.collisionData);
        if (isVertical) {
            newX = this.collisionData.newPosY;
        } else {
            newX = this.collisionData.newPosX;
        }
        if (newX != -1000) {
            if (isVertical) {
                sideOffset = Math.abs(newX - this.footY);
            } else {
                sideOffset = Math.abs(newX - this.footX);
            }
            sideCollision = true;
            sideCollisionDirection = 1;
            sideCollisionDegree = getDegreeFromWorld((this.footDegree + 270) % MDPhone.SCREEN_WIDTH, this.collisionData.collisionX, this.collisionData.collisionY, this.acObj.posZ);
            sideNewX = newX;
        }
        leftSideCollisionChk(this.footX, this.footY, skyDirection, this.collisionData);
        if (isVertical) {
            newX = this.collisionData.newPosY;
        } else {
            newX = this.collisionData.newPosX;
        }
        if (newX != -1000) {
            if (sideCollision) {
                int sideOffset2;
                if (isVertical) {
                    sideOffset2 = Math.abs(newX - this.footY);
                } else {
                    sideOffset2 = Math.abs(newX - this.footX);
                }
                int totalVelocity = ACUtilities.getTotalFromDegree(this.acObj.velX, this.acObj.velY, this.footDegree + RollPlatformSpeedC.DEGREE_VELOCITY);
                if (totalVelocity > 0 || (totalVelocity == 0 && sideOffset2 < sideOffset)) {
                    sideOffset = sideOffset2;
                    sideCollisionDirection = 3;
                    sideCollisionDegree = getDegreeFromWorld((this.footDegree + 90) % MDPhone.SCREEN_WIDTH, this.collisionData.collisionX, this.collisionData.collisionY, this.acObj.posZ);
                    sideNewX = newX;
                }
            } else {
                if (isVertical) {
                    sideOffset = Math.abs(newX - this.footY);
                } else {
                    sideOffset = Math.abs(newX - this.footX);
                }
                sideCollision = true;
                sideCollisionDirection = 3;
                sideCollisionDegree = getDegreeFromWorld((this.footDegree + 90) % MDPhone.SCREEN_WIDTH, this.collisionData.collisionX, this.collisionData.collisionY, this.acObj.posZ);
                sideNewX = newX;
            }
        }
        upSideCollisionChk(this.footX, this.footY, skyDirection, this.collisionData);
        if (!this.collisionData.isNoCollision()) {
            degree = getDegreeFromWorld((this.footDegree + RollPlatformSpeedC.DEGREE_VELOCITY) % MDPhone.SCREEN_WIDTH, this.collisionData.collisionX, this.collisionData.collisionY, this.acObj.posZ);
            plumbDegree = (degree + 90) % MDPhone.SCREEN_WIDTH;
            if (((this.moveDistanceX * MyAPI.dCos(plumbDegree)) + (this.moveDistanceY * MyAPI.dSin(plumbDegree))) / 100 > 0) {
                if (sideCollision) {
                    if (isVertical) {
                        bodyOffset = Math.abs(this.collisionData.newPosX - this.footX);
                        bodyNewY = this.collisionData.newPosX;
                    } else {
                        bodyOffset = Math.abs(this.collisionData.newPosY - this.footY);
                        bodyNewY = this.collisionData.newPosY;
                    }
                    collisionDegree = degree;
                    headCollision = true;
                } else {
                    if (isVertical) {
                        this.footX = this.collisionData.newPosX;
                    } else {
                        this.footY = this.collisionData.newPosY;
                    }
                    calChkPointFromPos();
                    this.user.doWhileTouchWorld(0, degree);
                    tangentDistance = ((this.moveDistanceX * MyAPI.dCos(degree)) + (this.moveDistanceY * MyAPI.dSin(degree))) / 100;
                    this.moveDistanceX = (MyAPI.dCos(degree) * tangentDistance) / 100;
                    this.moveDistanceY = (MyAPI.dSin(degree) * tangentDistance) / 100;
                    tangentVel = ((this.acObj.velX * MyAPI.dCos(degree)) + (this.acObj.velY * MyAPI.dSin(degree))) / 100;
                    this.acObj.velX = (MyAPI.dCos(degree) * tangentVel) / 100;
                    this.acObj.velY = (MyAPI.dSin(degree) * tangentVel) / 100;
                }
            }
        }
        downSideCollisionChk(this.footX, this.footY, skyDirection, this.collisionData);
        if (!this.collisionData.isNoCollision()) {
            degree = getDegreeFromWorld(this.footDegree, this.collisionData.collisionX, this.collisionData.collisionY, this.acObj.posZ);
            plumbDegree = (degree + 90) % MDPhone.SCREEN_WIDTH;
            if (((this.moveDistanceX * MyAPI.dCos(plumbDegree)) + (this.moveDistanceY * MyAPI.dSin(plumbDegree))) / 100 > 0) {
                if (sideCollision) {
                    if (isVertical) {
                        bodyOffset = Math.abs(this.collisionData.newPosX - this.footX);
                        bodyNewY = this.collisionData.newPosX;
                    } else {
                        bodyOffset = Math.abs(this.collisionData.newPosY - this.footY);
                        bodyNewY = this.collisionData.newPosY;
                    }
                    collisionDegree = degree;
                    footCollision = true;
                    footChkPointID = this.collisionData.chkPointID;
                    footChkDegree = degree;
                } else {
                    this.actionState = (byte) 0;
                    this.footX = ACUtilities.getRelativePointX(this.collisionData.collisionX, -this.footCollisionPointOffsetX[this.collisionData.chkPointID], -this.footCollisionPointOffsetY, degree);
                    this.footY = ACUtilities.getRelativePointY(this.collisionData.collisionY, -this.footCollisionPointOffsetX[this.collisionData.chkPointID], -this.footCollisionPointOffsetY, degree);
                    calChkPointFromPos();
                    this.user.doWhileTouchWorld(2, degree);
                    landDegree = degree;
                }
            }
        }
        if ((footCollision || headCollision) && sideCollision) {
            if (sideOffset < bodyOffset) {
                this.user.doWhileTouchWorld(sideCollisionDirection, sideCollisionDegree);
                if (isVertical) {
                    this.footY = sideNewX;
                    calChkPointFromPos();
                    this.moveDistanceY = 0;
                    this.acObj.velY = 0;
                } else if (canBeSideStop(sideCollisionDirection)) {
                    this.footX = sideNewX;
                    calChkPointFromPos();
                    this.moveDistanceX = 0;
                    this.acObj.velX = 0;
                }
                upSideCollisionChk(this.footX, this.footY, skyDirection, this.collisionData);
                if (!this.collisionData.isNoCollision()) {
                    degree = getDegreeFromWorld(this.footDegree + RollPlatformSpeedC.DEGREE_VELOCITY, this.collisionData.collisionX, this.collisionData.collisionY, this.acObj.posZ);
                    plumbDegree = (degree + 90) % MDPhone.SCREEN_WIDTH;
                    if (((this.moveDistanceX * MyAPI.dCos(plumbDegree)) + (this.moveDistanceY * MyAPI.dSin(plumbDegree))) / 100 > 0) {
                        if (isVertical) {
                            this.footX = this.collisionData.newPosX;
                        } else {
                            this.footY = this.collisionData.newPosY;
                        }
                        calChkPointFromPos();
                        this.user.doWhileTouchWorld(0, degree);
                        tangentDistance = ((this.moveDistanceX * MyAPI.dCos(degree)) + (this.moveDistanceY * MyAPI.dSin(degree))) / 100;
                        this.moveDistanceX = (MyAPI.dCos(degree) * tangentDistance) / 100;
                        this.moveDistanceY = (MyAPI.dSin(degree) * tangentDistance) / 100;
                        tangentVel = ((this.acObj.velX * MyAPI.dCos(degree)) + (this.acObj.velY * MyAPI.dSin(degree))) / 100;
                        this.acObj.velX = (MyAPI.dCos(degree) * tangentVel) / 100;
                        this.acObj.velY = (MyAPI.dSin(degree) * tangentVel) / 100;
                    }
                }
                downSideCollisionChk(this.footX, this.footY, skyDirection, this.collisionData);
                if (!this.collisionData.isNoCollision()) {
                    degree = getDegreeFromWorld(this.footDegree, this.collisionData.collisionX, this.collisionData.collisionY, this.acObj.posZ);
                    plumbDegree = (degree + 90) % MDPhone.SCREEN_WIDTH;
                    if (((this.moveDistanceX * MyAPI.dCos(plumbDegree)) + (this.moveDistanceY * MyAPI.dSin(plumbDegree))) / 100 > 0) {
                        this.actionState = (byte) 0;
                        this.footX = ACUtilities.getRelativePointX(this.collisionData.collisionX, -this.footCollisionPointOffsetX[this.collisionData.chkPointID], -this.footCollisionPointOffsetY, degree);
                        this.footY = ACUtilities.getRelativePointY(this.collisionData.collisionY, -this.footCollisionPointOffsetX[this.collisionData.chkPointID], -this.footCollisionPointOffsetY, degree);
                        calChkPointFromPos();
                        this.user.doWhileTouchWorld(2, degree);
                        landDegree = degree;
                    }
                }
            } else {
                if (footCollision) {
                    this.actionState = (byte) 0;
                    this.footX = ACUtilities.getRelativePointX(this.collisionData.collisionX, -this.footCollisionPointOffsetX[footChkPointID], -this.footCollisionPointOffsetY, footChkDegree);
                    this.footY = ACUtilities.getRelativePointY(this.collisionData.collisionY, -this.footCollisionPointOffsetX[footChkPointID], -this.footCollisionPointOffsetY, footChkDegree);
                    calChkPointFromPos();
                    this.user.doWhileTouchWorld(2, footChkDegree);
                    landDegree = footChkDegree;
                } else {
                    if (isVertical) {
                        this.footX = bodyNewY;
                    } else {
                        this.footY = bodyNewY;
                    }
                    calChkPointFromPos();
                    this.user.doWhileTouchWorld(0, collisionDegree);
                    tangentDistance = ((this.moveDistanceX * MyAPI.dCos(collisionDegree)) + (this.moveDistanceY * MyAPI.dSin(collisionDegree))) / 100;
                    this.moveDistanceX = (MyAPI.dCos(collisionDegree) * tangentDistance) / 100;
                    this.moveDistanceY = (MyAPI.dSin(collisionDegree) * tangentDistance) / 100;
                    tangentVel = ((this.acObj.velX * MyAPI.dCos(collisionDegree)) + (this.acObj.velY * MyAPI.dSin(collisionDegree))) / 100;
                    this.acObj.velX = (MyAPI.dCos(collisionDegree) * tangentVel) / 100;
                    this.acObj.velY = (MyAPI.dSin(collisionDegree) * tangentVel) / 100;
                }
                rightSideCollisionChk(this.footX, this.footY, skyDirection, this.collisionData);
                if (isVertical) {
                    newX = this.collisionData.newPosY;
                } else {
                    newX = this.collisionData.newPosX;
                }
                if (newX != -1000) {
                    this.user.doWhileTouchWorld(1, getDegreeFromWorld((this.footDegree + 270) % MDPhone.SCREEN_WIDTH, this.collisionData.collisionX, this.collisionData.collisionY, this.acObj.posZ));
                    if (isVertical) {
                        this.footY = newX;
                        this.moveDistanceY = 0;
                        this.acObj.velY = 0;
                    } else if (canBeSideStop(sideCollisionDirection)) {
                        this.footX = newX;
                        this.moveDistanceX = 0;
                        this.acObj.velX = 0;
                    }
                    calChkPointFromPos();
                }
                leftSideCollisionChk(this.footX, this.footY, skyDirection, this.collisionData);
                if (isVertical) {
                    newX = this.collisionData.newPosY;
                } else {
                    newX = this.collisionData.newPosX;
                }
                if (newX != -1000) {
                    this.user.doWhileTouchWorld(3, getDegreeFromWorld((this.footDegree + 90) % MDPhone.SCREEN_WIDTH, this.collisionData.collisionX, this.collisionData.collisionY, this.acObj.posZ));
                    if (isVertical) {
                        this.footY = newX;
                        this.moveDistanceY = 0;
                        this.acObj.velY = 0;
                    } else if (canBeSideStop(sideCollisionDirection)) {
                        this.footX = newX;
                        this.moveDistanceX = 0;
                        this.acObj.velX = 0;
                    }
                    calChkPointFromPos();
                }
            }
        } else if (sideCollision) {
            this.user.doWhileTouchWorld(sideCollisionDirection, sideCollisionDegree);
            if (isVertical) {
                this.footY = sideNewX;
                this.moveDistanceY = 0;
                this.acObj.velY = 0;
            } else if (canBeSideStop(sideCollisionDirection)) {
                this.footX = sideNewX;
                this.moveDistanceX = 0;
                this.acObj.velX = 0;
            }
            calChkPointFromPos();
            upSideCollisionChk(this.footX, this.footY, skyDirection, this.collisionData);
            if (!this.collisionData.isNoCollision()) {
                degree = getDegreeFromWorld(this.footDegree + RollPlatformSpeedC.DEGREE_VELOCITY, this.collisionData.collisionX, this.collisionData.collisionY, this.acObj.posZ);
                plumbDegree = (degree + 90) % MDPhone.SCREEN_WIDTH;
                if (((this.moveDistanceX * MyAPI.dCos(plumbDegree)) + (this.moveDistanceY * MyAPI.dSin(plumbDegree))) / 100 > 0) {
                    if (isVertical) {
                        this.footX = this.collisionData.newPosX;
                    } else {
                        this.footY = this.collisionData.newPosY;
                    }
                    calChkPointFromPos();
                    this.user.doWhileTouchWorld(0, degree);
                    tangentDistance = ((this.moveDistanceX * MyAPI.dCos(degree)) + (this.moveDistanceY * MyAPI.dSin(degree))) / 100;
                    this.moveDistanceX = (MyAPI.dCos(degree) * tangentDistance) / 100;
                    this.moveDistanceY = (MyAPI.dSin(degree) * tangentDistance) / 100;
                    tangentVel = ((this.acObj.velX * MyAPI.dCos(degree)) + (this.acObj.velY * MyAPI.dSin(degree))) / 100;
                    this.acObj.velX = (MyAPI.dCos(degree) * tangentVel) / 100;
                    this.acObj.velY = (MyAPI.dSin(degree) * tangentVel) / 100;
                }
            }
            downSideCollisionChk(this.footX, this.footY, skyDirection, this.collisionData);
            if (!this.collisionData.isNoCollision()) {
                degree = getDegreeFromWorld(this.footDegree, this.collisionData.collisionX, this.collisionData.collisionY, this.acObj.posZ);
                plumbDegree = (degree + 90) % MDPhone.SCREEN_WIDTH;
                if (((this.moveDistanceX * MyAPI.dCos(plumbDegree)) + (this.moveDistanceY * MyAPI.dSin(plumbDegree))) / 100 > 0) {
                    this.actionState = (byte) 0;
                    this.footX = ACUtilities.getRelativePointX(this.collisionData.collisionX, -this.footCollisionPointOffsetX[this.collisionData.chkPointID], -this.footCollisionPointOffsetY, degree);
                    this.footY = ACUtilities.getRelativePointY(this.collisionData.collisionY, -this.footCollisionPointOffsetX[this.collisionData.chkPointID], -this.footCollisionPointOffsetY, degree);
                    calChkPointFromPos();
                    this.user.doWhileTouchWorld(2, degree);
                    landDegree = degree;
                }
            }
        }
        int preMoveDistanceX = this.moveDistanceX;
        int preMoveDistanceY = this.moveDistanceY;
        if (this.lastMoveDistanceX == 0 && this.chkPointX - startPointX == 0 && this.lastMoveDistanceY == 0 && this.chkPointY - startPointY == 0) {
            this.lastMoveDistanceX = -9999;
            this.lastMoveDistanceY = -9999;
            this.moveDistanceX = 0;
            this.moveDistanceY = 0;
        } else {
            this.lastMoveDistanceX = this.chkPointX - startPointX;
            this.lastMoveDistanceY = this.chkPointY - startPointY;
            this.moveDistanceX -= this.chkPointX - startPointX;
            this.moveDistanceY -= this.chkPointY - startPointY;
        }
        if (this.moveDistanceX * preMoveDistanceX <= 0) {
            this.moveDistanceX = 0;
        }
        if (this.moveDistanceY * preMoveDistanceY <= 0) {
            this.moveDistanceY = 0;
        }
        if (this.actionState == (byte) 0) {
            this.totalDistance = ACUtilities.getTotalFromDegree(this.moveDistanceX, this.moveDistanceY, landDegree);
            this.footDegree = landDegree;
            this.user.doWhileLand(this.footDegree);
        }
        calObjPositionFromFoot();
    }

    private void findTheFootPoint() {
        if (this.actionState == (byte) 1) {
            this.chkPointX = ACUtilities.getRelativePointX(this.footX, this.footCollisionPointOffsetX[this.priorityChkId], this.footCollisionPointOffsetY, this.footDegree);
            this.chkPointY = ACUtilities.getRelativePointY(this.footY, this.footCollisionPointOffsetX[this.priorityChkId], this.footCollisionPointOffsetY, this.footDegree);
            this.chkPointId = this.priorityChkId;
            this.chkPointDegree = this.footDegree;
            calChkOffset(this.chkPointX, this.chkPointY, this.chkPointId, this.chkPointDegree);
            return;
        }
        int groundDirection = getDirectionByDegree(this.footDegree);
        int calDegree;
        int maxOffset;
        int i;
        int plumbDegree;
        int plumbVel;
        int velocityDegree;
        int degreeDiff;
        switch (groundDirection) {
            case 0:
            case 2:
                calDegree = this.footDegree;
                int maxBlockPixY = -1000;
                maxOffset = -1;
                i = 0;
                while (i < this.footCollisionPointOffsetX.length) {
                    this.footCollisionPointResaultX[i] = ACUtilities.getRelativePointX(this.footX, this.footCollisionPointOffsetX[i], this.footCollisionPointOffsetY, calDegree);
                    this.footCollisionPointResaultY[i] = getWorldY(this.footCollisionPointResaultX[i], ACUtilities.getRelativePointY(this.footY, this.footCollisionPointOffsetX[i], this.footCollisionPointOffsetY + this.user.getPressToGround(), calDegree), groundDirection);
                    if (this.footCollisionPointResaultY[i] != -1000 && (maxBlockPixY == -1000 || ((groundDirection == 0 && this.footCollisionPointResaultY[i] < maxBlockPixY) || ((groundDirection == 2 && this.footCollisionPointResaultY[i] > maxBlockPixY) || i == this.priorityChkId)))) {
                        maxOffset = i;
                        maxBlockPixY = this.footCollisionPointResaultY[i];
                        if (i == this.priorityChkId) {
                            if (maxOffset == -1) {
                                this.chkPointX = this.footCollisionPointResaultX[maxOffset];
                                this.chkPointY = maxBlockPixY;
                                this.chkPointId = maxOffset;
                                this.chkPointDegree = this.footDegree;
                                calChkOffset(this.chkPointX, this.chkPointY, this.chkPointId, this.chkPointDegree);
                                this.footDegree = getDegreeFromWorld(this.footDegree, this.chkPointX, this.chkPointY, this.acObj.posZ);
                                this.actionState = (byte) 0;
                                plumbDegree = (this.footDegree + 90) % MDPhone.SCREEN_WIDTH;
                                plumbVel = (((this.moveDistanceX * MyAPI.dCos(plumbDegree)) + (this.moveDistanceY * MyAPI.dSin(plumbDegree))) / 100) + this.user.getPressToGround();
                                velocityDegree = CrlFP32.actTanDegree(this.acObj.velY, this.acObj.velX);
                                degreeDiff = getDegreeDiff(this.footDegree, velocityDegree);
                                if (plumbVel < 0 && degreeDiff > this.user.getMinDegreeToLeaveGround()) {
                                    this.user.doWhileLeaveGround();
                                    this.chkPointX = ACUtilities.getRelativePointX(this.footX, this.footCollisionPointOffsetX[this.priorityChkId], this.footCollisionPointOffsetY, calDegree);
                                    this.chkPointY = ACUtilities.getRelativePointY(this.footY, this.footCollisionPointOffsetX[this.priorityChkId], this.footCollisionPointOffsetY, calDegree);
                                    this.chkPointId = this.priorityChkId;
                                    this.chkPointDegree = this.footDegree;
                                    calChkOffset(this.chkPointX, this.chkPointY, this.chkPointId, this.chkPointDegree);
                                    this.actionState = (byte) 1;
                                    return;
                                }
                                return;
                            }
                            this.user.doWhileLeaveGround();
                            this.chkPointX = ACUtilities.getRelativePointX(this.footX, this.footCollisionPointOffsetX[this.priorityChkId], this.footCollisionPointOffsetY, calDegree);
                            this.chkPointY = ACUtilities.getRelativePointY(this.footY, this.footCollisionPointOffsetX[this.priorityChkId], this.footCollisionPointOffsetY, calDegree);
                            this.chkPointId = this.priorityChkId;
                            this.chkPointDegree = this.footDegree;
                            calChkOffset(this.chkPointX, this.chkPointY, this.chkPointId, this.chkPointDegree);
                            this.actionState = (byte) 1;
                            return;
                        }
                    }
                    i++;
                }
                if (maxOffset == -1) {
                    this.user.doWhileLeaveGround();
                    this.chkPointX = ACUtilities.getRelativePointX(this.footX, this.footCollisionPointOffsetX[this.priorityChkId], this.footCollisionPointOffsetY, calDegree);
                    this.chkPointY = ACUtilities.getRelativePointY(this.footY, this.footCollisionPointOffsetX[this.priorityChkId], this.footCollisionPointOffsetY, calDegree);
                    this.chkPointId = this.priorityChkId;
                    this.chkPointDegree = this.footDegree;
                    calChkOffset(this.chkPointX, this.chkPointY, this.chkPointId, this.chkPointDegree);
                    this.actionState = (byte) 1;
                    return;
                }
                this.chkPointX = this.footCollisionPointResaultX[maxOffset];
                this.chkPointY = maxBlockPixY;
                this.chkPointId = maxOffset;
                this.chkPointDegree = this.footDegree;
                calChkOffset(this.chkPointX, this.chkPointY, this.chkPointId, this.chkPointDegree);
                this.footDegree = getDegreeFromWorld(this.footDegree, this.chkPointX, this.chkPointY, this.acObj.posZ);
                this.actionState = (byte) 0;
                plumbDegree = (this.footDegree + 90) % MDPhone.SCREEN_WIDTH;
                plumbVel = (((this.moveDistanceX * MyAPI.dCos(plumbDegree)) + (this.moveDistanceY * MyAPI.dSin(plumbDegree))) / 100) + this.user.getPressToGround();
                velocityDegree = CrlFP32.actTanDegree(this.acObj.velY, this.acObj.velX);
                degreeDiff = getDegreeDiff(this.footDegree, velocityDegree);
                if (plumbVel < 0) {
                    return;
                }
                return;
            case 1:
            case 3:
                int maxBlockPixX = -1000;
                calDegree = this.footDegree;
                maxOffset = -1;
                if (this.checkArrayForShowX == null) {
                    this.checkArrayForShowX = new int[this.footCollisionPointOffsetX.length];
                    this.checkArrayForShowY = new int[this.footCollisionPointOffsetX.length];
                }
                i = 0;
                while (i < this.footCollisionPointOffsetX.length) {
                    this.footCollisionPointResaultY[i] = ACUtilities.getRelativePointY(this.footY, this.footCollisionPointOffsetX[i], this.footCollisionPointOffsetY, calDegree);
                    this.checkArrayForShowX[i] = ACUtilities.getRelativePointX(this.footX, this.footCollisionPointOffsetX[i], this.footCollisionPointOffsetY + this.user.getPressToGround(), calDegree);
                    this.checkArrayForShowY[i] = this.footCollisionPointResaultY[i];
                    this.footCollisionPointResaultX[i] = getWorldX(ACUtilities.getRelativePointX(this.footX, this.footCollisionPointOffsetX[i], this.footCollisionPointOffsetY + this.user.getPressToGround(), calDegree), this.footCollisionPointResaultY[i], groundDirection);
                    if (this.footCollisionPointResaultX[i] != -1000 && (maxBlockPixX == -1000 || ((groundDirection == 1 && this.footCollisionPointResaultX[i] > maxBlockPixX) || ((groundDirection == 3 && this.footCollisionPointResaultX[i] < maxBlockPixX) || i == this.priorityChkId)))) {
                        maxOffset = i;
                        maxBlockPixX = this.footCollisionPointResaultX[i];
                        if (i == this.priorityChkId) {
                            if (maxOffset == -1) {
                                this.chkPointX = maxBlockPixX;
                                this.chkPointY = this.footCollisionPointResaultY[maxOffset];
                                this.chkPointId = maxOffset;
                                this.chkPointDegree = this.footDegree;
                                this.footDegree = getDegreeFromWorld(this.footDegree, this.chkPointX, this.chkPointY, this.acObj.posZ);
                                calChkOffset(this.chkPointX, this.chkPointY, this.chkPointId, this.chkPointDegree);
                                this.actionState = (byte) 0;
                                plumbDegree = (this.footDegree + 90) % MDPhone.SCREEN_WIDTH;
                                plumbVel = (((this.moveDistanceX * MyAPI.dCos(plumbDegree)) + (this.moveDistanceY * MyAPI.dSin(plumbDegree))) / 100) + this.user.getPressToGround();
                                velocityDegree = CrlFP32.actTanDegree(this.moveDistanceY, this.moveDistanceX);
                                degreeDiff = getDegreeDiff(this.footDegree, velocityDegree);
                                if (plumbVel < 0 && degreeDiff > this.user.getMinDegreeToLeaveGround()) {
                                    this.user.doWhileLeaveGround();
                                    this.chkPointX = ACUtilities.getRelativePointX(this.footX, this.footCollisionPointOffsetX[this.priorityChkId], this.footCollisionPointOffsetY, calDegree);
                                    this.chkPointY = ACUtilities.getRelativePointY(this.footY, this.footCollisionPointOffsetX[this.priorityChkId], this.footCollisionPointOffsetY, calDegree);
                                    this.chkPointId = this.priorityChkId;
                                    this.chkPointDegree = this.footDegree;
                                    calChkOffset(this.chkPointX, this.chkPointY, this.chkPointId, this.chkPointDegree);
                                    this.actionState = (byte) 1;
                                    return;
                                }
                                return;
                            }
                            this.user.doWhileLeaveGround();
                            this.chkPointX = ACUtilities.getRelativePointX(this.footX, this.footCollisionPointOffsetX[this.priorityChkId], this.footCollisionPointOffsetY, calDegree);
                            this.chkPointY = ACUtilities.getRelativePointY(this.footY, this.footCollisionPointOffsetX[this.priorityChkId], this.footCollisionPointOffsetY, calDegree);
                            this.chkPointId = this.priorityChkId;
                            this.chkPointDegree = this.footDegree;
                            calChkOffset(this.chkPointX, this.chkPointY, this.chkPointId, this.chkPointDegree);
                            this.actionState = (byte) 1;
                            return;
                        }
                    }
                    i++;
                }
                if (maxOffset == -1) {
                    this.user.doWhileLeaveGround();
                    this.chkPointX = ACUtilities.getRelativePointX(this.footX, this.footCollisionPointOffsetX[this.priorityChkId], this.footCollisionPointOffsetY, calDegree);
                    this.chkPointY = ACUtilities.getRelativePointY(this.footY, this.footCollisionPointOffsetX[this.priorityChkId], this.footCollisionPointOffsetY, calDegree);
                    this.chkPointId = this.priorityChkId;
                    this.chkPointDegree = this.footDegree;
                    calChkOffset(this.chkPointX, this.chkPointY, this.chkPointId, this.chkPointDegree);
                    this.actionState = (byte) 1;
                    return;
                }
                this.chkPointX = maxBlockPixX;
                this.chkPointY = this.footCollisionPointResaultY[maxOffset];
                this.chkPointId = maxOffset;
                this.chkPointDegree = this.footDegree;
                this.footDegree = getDegreeFromWorld(this.footDegree, this.chkPointX, this.chkPointY, this.acObj.posZ);
                calChkOffset(this.chkPointX, this.chkPointY, this.chkPointId, this.chkPointDegree);
                this.actionState = (byte) 0;
                plumbDegree = (this.footDegree + 90) % MDPhone.SCREEN_WIDTH;
                plumbVel = (((this.moveDistanceX * MyAPI.dCos(plumbDegree)) + (this.moveDistanceY * MyAPI.dSin(plumbDegree))) / 100) + this.user.getPressToGround();
                velocityDegree = CrlFP32.actTanDegree(this.moveDistanceY, this.moveDistanceX);
                degreeDiff = getDegreeDiff(this.footDegree, velocityDegree);
                if (plumbVel < 0) {
                    return;
                }
                return;
            default:
                return;
        }
    }

    private void moveToNextPosition() {
        int newY;
        int newX;
        int currentBlockX = ACUtilities.getQuaParam(this.chkPointX, this.worldInstance.getTileWidth());
        int currentBlockY = ACUtilities.getQuaParam(this.chkPointY, this.worldInstance.getTileHeight());
        this.worldInstance.getCollisionBlock(this.getBlock, this.chkPointX, this.chkPointY, this.acObj.posZ);
        int preDegree = this.user.getBodyDegree();
        int startPointX = this.chkPointX;
        int startPointY = this.chkPointY;
        this.moveDistanceX = (this.totalDistance * MyAPI.dCos(this.user.getBodyDegree())) / 100;
        this.moveDistanceY = (this.totalDistance * MyAPI.dSin(this.user.getBodyDegree())) / 100;
        int direction = getDirectionByDegree(this.user.getBodyDegree());
        int preFootDegree;
        if (direction == 0 || direction == 2) {
            if (direction == 2) {
                boolean moveReverse = true;
            } else {
                Object obj = null;
            }
            int preCheckX = this.chkPointX;
            if (this.moveDistanceX == 0) {
                this.moveDistanceY = 0;
            } else if (this.moveDistanceX > 0) {
                if (this.chkPointX + this.moveDistanceX >= getBlockLeftSide(currentBlockX + 1, currentBlockY)) {
                    this.chkPointX = getBlockLeftSide(currentBlockX + 1, currentBlockY);
                } else {
                    this.chkPointX += this.moveDistanceX;
                }
            } else {
                if (this.chkPointX + this.moveDistanceX <= getBlockRightSide(currentBlockX - 1, currentBlockY)) {
                    this.chkPointX = getBlockRightSide(currentBlockX - 1, currentBlockY);
                } else {
                    this.chkPointX += this.moveDistanceX;
                }
            }
            calObjPositionFromFoot();
            doSideCheckInGround(direction);
            calChkPointFromPos();
            preFootDegree = this.footDegree;
            newY = getWorldY(this.chkPointX, this.chkPointY + ((direction == 0 ? 1 : -1) * (Math.abs(this.chkPointX - startPointX) + this.user.getPressToGround())), direction);
            if (newY != -1000) {
                this.chkPointY = newY;
                this.footDegree = getDegreeFromWorld(this.footDegree, this.chkPointX, this.chkPointY, this.acObj.posZ);
            }
            calObjPositionFromFoot();
            if (direction == getDirectionByDegree(this.footDegree)) {
                doSideCheckInGround(direction);
                calChkPointFromPos();
            }
            this.footDegree = preFootDegree;
            newY = getWorldY(this.chkPointX, this.chkPointY + ((direction == 0 ? 1 : -1) * (Math.abs(this.chkPointX - startPointX) + this.user.getPressToGround())), direction);
            if (newY != -1000) {
                this.chkPointY = newY;
            }
        } else {
            if (this.moveDistanceY == 0) {
                this.moveDistanceX = 0;
            } else if (this.moveDistanceY > 0) {
                if (this.chkPointY + this.moveDistanceY >= getBlockUpSide(currentBlockX, currentBlockY + 1)) {
                    this.chkPointY = getBlockUpSide(currentBlockX, currentBlockY + 1);
                } else {
                    this.chkPointY += this.moveDistanceY;
                }
            } else {
                if (this.chkPointY + this.moveDistanceY <= getBlockDownSide(currentBlockX, currentBlockY - 1)) {
                    this.chkPointY = getBlockDownSide(currentBlockX, currentBlockY - 1);
                } else {
                    this.chkPointY += this.moveDistanceY;
                }
            }
            preFootDegree = this.footDegree;
            newX = getWorldX(this.chkPointX + ((direction == 3 ? 1 : -1) * (Math.abs(this.chkPointY - startPointY) + this.user.getPressToGround())), this.chkPointY, direction);
            if (newX != -1000) {
                this.chkPointX = newX;
                this.footDegree = getDegreeFromWorld(this.footDegree, this.chkPointX, this.chkPointY, this.acObj.posZ);
            }
            calObjPositionFromFoot();
            if (direction == getDirectionByDegree(this.footDegree)) {
                if (this.moveDistanceY > 0) {
                    if (direction == 1) {
                        rightSideCollisionChk(this.footX, this.footY, direction, this.collisionData);
                        newY = this.collisionData.newPosY;
                    } else {
                        leftSideCollisionChk(this.footX, this.footY, direction, this.collisionData);
                        newY = this.collisionData.newPosY;
                    }
                    if (newY != -1000) {
                        this.footY = newY;
                        calChkPointFromPos();
                        this.user.doWhileTouchWorld(direction == 1 ? 1 : 3, getDegreeFromWorld((this.footDegree + (direction == 1 ? 270 : 90)) % MDPhone.SCREEN_WIDTH, this.collisionData.collisionX, this.collisionData.collisionY, this.acObj.posZ));
                        this.moveDistanceY = 0;
                        this.acObj.velY = 0;
                    }
                } else if (this.moveDistanceY < 0) {
                    if (direction == 1) {
                        leftSideCollisionChk(this.footX, this.footY, direction, this.collisionData);
                        newY = this.collisionData.newPosY;
                    } else {
                        rightSideCollisionChk(this.footX, this.footY, direction, this.collisionData);
                        newY = this.collisionData.newPosY;
                    }
                    if (newY != -1000) {
                        this.footY = newY;
                        calChkPointFromPos();
                        this.user.doWhileTouchWorld(direction == 1 ? 3 : 1, getDegreeFromWorld((this.footDegree + (direction == 1 ? 90 : 270)) % MDPhone.SCREEN_WIDTH, this.collisionData.collisionX, this.collisionData.collisionY, this.acObj.posZ));
                        this.moveDistanceY = 0;
                        this.acObj.velY = 0;
                    }
                }
            }
            this.footDegree = preFootDegree;
            newX = getWorldX(this.chkPointX + ((direction == 3 ? 1 : -1) * (Math.abs(this.chkPointY - startPointY) + this.user.getPressToGround())), this.chkPointY, direction);
            if (newX != -1000) {
                this.chkPointX = newX;
            }
        }
        calObjPositionFromFoot();
        switch (getDirectionByDegree(this.footDegree)) {
            case 0:
                newY = getWorldY(this.footX, this.footY + this.worldInstance.getTileHeight(), 0);
                if (newY != -1000) {
                    this.footY = newY;
                    break;
                }
                break;
            case 1:
                newX = getWorldX(this.footX - this.worldInstance.getTileHeight(), this.footY, 1);
                if (newX != -1000) {
                    this.footX = newX;
                    break;
                }
                break;
            case 2:
                newY = getWorldY(this.footX, this.footY - this.worldInstance.getTileHeight(), 2);
                if (newY != -1000) {
                    this.footY = newY;
                    break;
                }
                break;
            case 3:
                newX = getWorldX(this.footX + this.worldInstance.getTileHeight(), this.footY, 3);
                if (newX != -1000) {
                    this.footX = newX;
                    break;
                }
                break;
        }
        int preMoveDistanceX = this.moveDistanceX;
        int preMoveDistanceY = this.moveDistanceY;
        this.moveDistanceX -= this.chkPointX - startPointX;
        this.moveDistanceY -= this.chkPointY - startPointY;
        if (this.totalDistance * (((this.moveDistanceX * MyAPI.dCos(this.user.getBodyDegree())) + (this.moveDistanceY * MyAPI.dSin(this.user.getBodyDegree()))) / 100) <= 0) {
            this.moveDistanceX = 0;
            this.moveDistanceY = 0;
        }
        this.totalDistance = ((this.moveDistanceX * MyAPI.dCos(this.user.getBodyDegree())) + (this.moveDistanceY * MyAPI.dSin(this.user.getBodyDegree()))) / 100;
    }

    public int getDirectionByDegree(int degree) {
        while (degree < 0) {
            degree += MDPhone.SCREEN_WIDTH;
        }
        degree %= MDPhone.SCREEN_WIDTH;
        if (degree > 315 || degree < 45) {
            return 0;
        }
        if (degree >= 225 && degree <= 315) {
            return 3;
        }
        if (degree <= StringIndex.FONT_COLON_RED || degree >= 225) {
            return 1;
        }
        return 2;
    }

    private int getWorldY(int x, int y, int direction) {
        return this.worldInstance.getWorldY(x, y, this.acObj.posZ, direction);
    }

    private int getWorldX(int x, int y, int direction) {
        return this.worldInstance.getWorldX(x, y, this.acObj.posZ, direction);
    }

    private void rightSideCollisionChk(int x, int y, int direction, ACCollisionData collisionData) {
        collisionData.reset();
        if (this.limit == null || !this.limit.noSideCollision()) {
            int collisionPointId;
            int maxDiff;
            int i;
            int objX;
            int objY;
            int diff;
            switch (direction) {
                case 0:
                case 2:
                    int maxBlockPixX = -1000;
                    collisionPointId = -1;
                    maxDiff = -1;
                    for (i = 0; i < this.bodyCollisionPointOffsetY.length; i++) {
                        objX = ACUtilities.getRelativePointX(x, this.bodyCollisionPointOffsetX, this.bodyCollisionPointOffsetY[i], this.user.getBodyDegree());
                        objY = ACUtilities.getRelativePointY(y, this.bodyCollisionPointOffsetX, this.bodyCollisionPointOffsetY[i], this.user.getBodyDegree());
                        int blockPixX = getWorldX(objX, objY, (direction + 3) % 4);
                        if (blockPixX != -1000) {
                            diff = Math.abs(blockPixX - objX);
                            if (maxBlockPixX == -1000 || diff > maxDiff) {
                                maxBlockPixX = blockPixX;
                                collisionPointId = i;
                                maxDiff = diff;
                                collisionData.collisionX = maxBlockPixX;
                                collisionData.collisionY = objY;
                                collisionData.newPosY = y;
                            }
                        }
                    }
                    if (maxBlockPixX != -1000) {
                        collisionData.newPosX = ACUtilities.getRelativePointX(maxBlockPixX, -this.bodyCollisionPointOffsetX, -this.bodyCollisionPointOffsetY[collisionPointId], this.user.getBodyDegree());
                        this.worldInstance.getCollisionBlock(this.getBlock, collisionData.collisionX, collisionData.collisionY, this.acObj.posZ);
                        collisionData.reBlock = this.getBlock;
                        break;
                    }
                    break;
            }
            int maxBlockPixY = -1000;
            collisionPointId = -1;
            maxDiff = -1;
            for (i = 0; i < this.bodyCollisionPointOffsetY.length; i++) {
                objX = ACUtilities.getRelativePointX(x, this.bodyCollisionPointOffsetX, this.bodyCollisionPointOffsetY[i], this.user.getBodyDegree());
                objY = ACUtilities.getRelativePointY(y, this.bodyCollisionPointOffsetX, this.bodyCollisionPointOffsetY[i], this.user.getBodyDegree());
                int blockPixY = getWorldY(objX, objY, (direction + 3) % 4);
                if (blockPixY != -1000) {
                    diff = Math.abs(blockPixY - objY);
                    if (maxBlockPixY == -1000 || diff > maxDiff) {
                        maxBlockPixY = blockPixY;
                        collisionPointId = i;
                        maxDiff = diff;
                        collisionData.collisionX = maxBlockPixY;
                        collisionData.collisionY = objX;
                        collisionData.newPosX = x;
                    }
                }
            }
            if (maxBlockPixY != -1000) {
                collisionData.newPosY = ACUtilities.getRelativePointY(maxBlockPixY, -this.bodyCollisionPointOffsetX, -this.bodyCollisionPointOffsetY[collisionPointId], this.user.getBodyDegree());
                this.worldInstance.getCollisionBlock(this.getBlock, collisionData.collisionX, collisionData.collisionY, this.acObj.posZ);
                collisionData.reBlock = this.getBlock;
            }
        }
    }

    private void leftSideCollisionChk(int x, int y, int direction, ACCollisionData collisionData) {
        collisionData.reset();
        if (this.limit == null || !this.limit.noSideCollision()) {
            int collisionPointId;
            int maxDiff;
            int i;
            int objX;
            int objY;
            int diff;
            switch (direction) {
                case 0:
                case 2:
                    int maxBlockPixX = -1000;
                    collisionPointId = -1;
                    maxDiff = -1;
                    for (i = 0; i < this.bodyCollisionPointOffsetY.length; i++) {
                        objX = ACUtilities.getRelativePointX(x, -this.bodyCollisionPointOffsetX, this.bodyCollisionPointOffsetY[i], this.user.getBodyDegree());
                        objY = ACUtilities.getRelativePointY(y, -this.bodyCollisionPointOffsetX, this.bodyCollisionPointOffsetY[i], this.user.getBodyDegree());
                        int blockPixX = getWorldX(objX, objY, (direction + 1) % 4);
                        if (blockPixX != -1000) {
                            diff = Math.abs(blockPixX - objX);
                            if (maxBlockPixX == -1000 || diff > maxDiff) {
                                maxBlockPixX = blockPixX;
                                collisionPointId = i;
                                maxDiff = diff;
                                collisionData.collisionX = maxBlockPixX;
                                collisionData.collisionY = objY;
                                collisionData.newPosY = y;
                            }
                        }
                    }
                    if (maxBlockPixX != -1000) {
                        collisionData.newPosX = ACUtilities.getRelativePointX(maxBlockPixX, this.bodyCollisionPointOffsetX, -this.bodyCollisionPointOffsetY[collisionPointId], this.user.getBodyDegree());
                        this.worldInstance.getCollisionBlock(this.getBlock, collisionData.collisionX, collisionData.collisionY, this.acObj.posZ);
                        collisionData.reBlock = this.getBlock;
                        break;
                    }
                    break;
            }
            int maxBlockPixY = -1000;
            collisionPointId = -1;
            maxDiff = -1;
            for (i = 0; i < this.bodyCollisionPointOffsetY.length; i++) {
                objX = ACUtilities.getRelativePointX(x, -this.bodyCollisionPointOffsetX, this.bodyCollisionPointOffsetY[i], this.user.getBodyDegree());
                objY = ACUtilities.getRelativePointY(y, -this.bodyCollisionPointOffsetX, this.bodyCollisionPointOffsetY[i], this.user.getBodyDegree());
                int blockPixY = getWorldY(objX, objY, (direction + 1) % 4);
                if (blockPixY != -1000) {
                    diff = Math.abs(blockPixY - objY);
                    if (maxBlockPixY == -1000 || diff > maxDiff) {
                        maxBlockPixY = blockPixY;
                        collisionPointId = i;
                        maxDiff -= diff;
                        collisionData.collisionX = objX;
                        collisionData.collisionY = maxBlockPixY;
                        collisionData.newPosX = x;
                    }
                }
            }
            if (maxBlockPixY != -1000) {
                collisionData.newPosX = ACUtilities.getRelativePointY(maxBlockPixY, this.bodyCollisionPointOffsetX, -this.bodyCollisionPointOffsetY[collisionPointId], this.user.getBodyDegree());
                this.worldInstance.getCollisionBlock(this.getBlock, collisionData.collisionX, collisionData.collisionY, this.acObj.posZ);
                collisionData.reBlock = this.getBlock;
            }
        }
    }

    private void upSideCollisionChk(int x, int y, int direction, ACCollisionData collisionData) {
        collisionData.reset();
        if (this.limit == null || !this.limit.noTopCollision()) {
            int maxDiff;
            int collisionPointId;
            int i;
            int objX;
            int objY;
            int diff;
            switch (direction) {
                case 0:
                case 2:
                    int maxBlockPixY = -1000;
                    maxDiff = -1;
                    collisionPointId = -1;
                    for (i = 0; i < this.headCollisionPointOffsetX.length; i++) {
                        objX = ACUtilities.getRelativePointX(x, this.headCollisionPointOffsetX[i], this.headCollisionPointOffsetY, this.footDegree);
                        objY = ACUtilities.getRelativePointY(y, this.headCollisionPointOffsetX[i], this.headCollisionPointOffsetY, this.footDegree);
                        int blockPixY = getWorldY(objX, objY, (direction + 2) % 4);
                        if (blockPixY != -1000) {
                            diff = Math.abs(objY - blockPixY);
                            if (maxBlockPixY == -1000 || diff > maxDiff) {
                                maxBlockPixY = blockPixY;
                                collisionPointId = i;
                                maxDiff = diff;
                            }
                        }
                    }
                    if (maxBlockPixY != -1000) {
                        objX = ACUtilities.getRelativePointX(x, this.headCollisionPointOffsetX[collisionPointId], this.headCollisionPointOffsetY, this.footDegree);
                        objY = ACUtilities.getRelativePointY(y, this.headCollisionPointOffsetX[collisionPointId], this.headCollisionPointOffsetY, this.footDegree);
                        this.worldInstance.getCollisionBlock(this.getBlock, objX, maxBlockPixY, this.acObj.posZ);
                        collisionData.collisionX = objX;
                        collisionData.collisionY = maxBlockPixY;
                        collisionData.newPosX = x;
                        collisionData.newPosY = ACUtilities.getRelativePointY(maxBlockPixY, -this.headCollisionPointOffsetX[collisionPointId], -this.headCollisionPointOffsetY, this.footDegree);
                        collisionData.reBlock = this.getBlock;
                        return;
                    }
                    return;
                default:
                    int maxBlockPixX = -1000;
                    maxDiff = -1;
                    collisionPointId = -1;
                    for (i = 0; i < this.headCollisionPointOffsetX.length; i++) {
                        objX = ACUtilities.getRelativePointX(x, this.headCollisionPointOffsetX[i], this.headCollisionPointOffsetY, this.footDegree);
                        int blockPixX = getWorldX(objX, ACUtilities.getRelativePointY(y, this.headCollisionPointOffsetX[i], this.headCollisionPointOffsetY, this.footDegree), (direction + 2) % 4);
                        if (blockPixX != -1000) {
                            diff = Math.abs(objX - blockPixX);
                            if (maxBlockPixX == -1000 || diff > maxDiff) {
                                maxBlockPixX = blockPixX;
                                collisionPointId = i;
                                maxDiff = diff;
                            }
                        }
                    }
                    if (maxBlockPixX != -1000) {
                        objX = ACUtilities.getRelativePointX(x, this.headCollisionPointOffsetX[collisionPointId], this.headCollisionPointOffsetY, this.footDegree);
                        objY = ACUtilities.getRelativePointY(y, this.headCollisionPointOffsetX[collisionPointId], this.headCollisionPointOffsetY, this.footDegree);
                        this.worldInstance.getCollisionBlock(this.getBlock, maxBlockPixX, objY, this.acObj.posZ);
                        collisionData.collisionX = maxBlockPixX;
                        collisionData.collisionY = objY;
                        collisionData.newPosY = y;
                        collisionData.newPosX = ACUtilities.getRelativePointX(maxBlockPixX, -this.headCollisionPointOffsetX[collisionPointId], -this.headCollisionPointOffsetY, this.footDegree);
                        collisionData.reBlock = this.getBlock;
                        return;
                    }
                    return;
            }
        }
    }

    private void downSideCollisionChk(int x, int y, int direction, ACCollisionData collisionData) {
        collisionData.reset();
        if (this.limit == null || !this.limit.noDownCollision()) {
            int collisionPointId = -1;
            int maxDiff = -1;
            int i;
            int objX;
            int objY;
            int diff;
            switch (direction) {
                case 0:
                case 2:
                    int maxBlockPixY = -1000;
                    i = 0;
                    while (i < this.footCollisionPointOffsetX.length) {
                        objX = ACUtilities.getRelativePointX(x, this.footCollisionPointOffsetX[i], this.footCollisionPointOffsetY, this.footDegree);
                        objY = ACUtilities.getRelativePointY(y, this.footCollisionPointOffsetX[i], this.footCollisionPointOffsetY, this.footDegree);
                        int blockPixY = getWorldY(objX, objY, (direction + 0) % 4);
                        if (blockPixY != -1000) {
                            diff = Math.abs(objY - blockPixY);
                            if (maxBlockPixY == -1000 || diff > maxDiff || i == this.priorityChkId) {
                                maxBlockPixY = blockPixY;
                                collisionPointId = i;
                                maxDiff = diff;
                                if (i == this.priorityChkId) {
                                    if (maxBlockPixY != -1000) {
                                        objX = ACUtilities.getRelativePointX(x, this.footCollisionPointOffsetX[collisionPointId], this.footCollisionPointOffsetY, this.footDegree);
                                        objY = ACUtilities.getRelativePointY(y, this.footCollisionPointOffsetX[collisionPointId], this.footCollisionPointOffsetY, this.footDegree);
                                        this.worldInstance.getCollisionBlock(this.getBlock, objX, maxBlockPixY, this.acObj.posZ);
                                        collisionData.collisionX = objX;
                                        collisionData.collisionY = maxBlockPixY;
                                        collisionData.newPosX = x;
                                        collisionData.newPosY = ACUtilities.getRelativePointY(maxBlockPixY, -this.footCollisionPointOffsetX[collisionPointId], -this.footCollisionPointOffsetY, this.footDegree);
                                        collisionData.reBlock = this.getBlock;
                                        collisionData.chkPointID = collisionPointId;
                                        return;
                                    }
                                    return;
                                }
                            }
                        }
                        i++;
                    }
                    if (maxBlockPixY != -1000) {
                        objX = ACUtilities.getRelativePointX(x, this.footCollisionPointOffsetX[collisionPointId], this.footCollisionPointOffsetY, this.footDegree);
                        objY = ACUtilities.getRelativePointY(y, this.footCollisionPointOffsetX[collisionPointId], this.footCollisionPointOffsetY, this.footDegree);
                        this.worldInstance.getCollisionBlock(this.getBlock, objX, maxBlockPixY, this.acObj.posZ);
                        collisionData.collisionX = objX;
                        collisionData.collisionY = maxBlockPixY;
                        collisionData.newPosX = x;
                        collisionData.newPosY = ACUtilities.getRelativePointY(maxBlockPixY, -this.footCollisionPointOffsetX[collisionPointId], -this.footCollisionPointOffsetY, this.footDegree);
                        collisionData.reBlock = this.getBlock;
                        collisionData.chkPointID = collisionPointId;
                        return;
                    }
                    return;
                default:
                    int maxBlockPixX = -1000;
                    i = 0;
                    while (i < this.footCollisionPointOffsetX.length) {
                        objX = ACUtilities.getRelativePointX(x, this.footCollisionPointOffsetX[i], this.footCollisionPointOffsetY, this.footDegree);
                        int blockPixX = getWorldX(objX, ACUtilities.getRelativePointY(y, this.footCollisionPointOffsetX[i], this.footCollisionPointOffsetY, this.footDegree), (direction + 0) % 4);
                        if (blockPixX != -1000) {
                            diff = Math.abs(objX - blockPixX);
                            if (maxBlockPixX == -1000 || diff > maxDiff || i == this.priorityChkId) {
                                maxBlockPixX = blockPixX;
                                collisionPointId = i;
                                maxDiff = diff;
                                if (i == this.priorityChkId) {
                                    if (maxBlockPixX != -1000) {
                                        objX = ACUtilities.getRelativePointX(x, this.footCollisionPointOffsetX[collisionPointId], this.footCollisionPointOffsetY, this.footDegree);
                                        objY = ACUtilities.getRelativePointY(y, this.footCollisionPointOffsetX[collisionPointId], this.footCollisionPointOffsetY, this.footDegree);
                                        this.worldInstance.getCollisionBlock(this.getBlock, maxBlockPixX, objY, this.acObj.posZ);
                                        collisionData.collisionX = maxBlockPixX;
                                        collisionData.collisionY = objY;
                                        collisionData.newPosY = y;
                                        collisionData.newPosX = ACUtilities.getRelativePointX(maxBlockPixX, -this.footCollisionPointOffsetX[collisionPointId], -this.footCollisionPointOffsetY, this.footDegree);
                                        collisionData.reBlock = this.getBlock;
                                        collisionData.chkPointID = collisionPointId;
                                        return;
                                    }
                                    return;
                                }
                            }
                        }
                        i++;
                    }
                    if (maxBlockPixX != -1000) {
                        objX = ACUtilities.getRelativePointX(x, this.footCollisionPointOffsetX[collisionPointId], this.footCollisionPointOffsetY, this.footDegree);
                        objY = ACUtilities.getRelativePointY(y, this.footCollisionPointOffsetX[collisionPointId], this.footCollisionPointOffsetY, this.footDegree);
                        this.worldInstance.getCollisionBlock(this.getBlock, maxBlockPixX, objY, this.acObj.posZ);
                        collisionData.collisionX = maxBlockPixX;
                        collisionData.collisionY = objY;
                        collisionData.newPosY = y;
                        collisionData.newPosX = ACUtilities.getRelativePointX(maxBlockPixX, -this.footCollisionPointOffsetX[collisionPointId], -this.footCollisionPointOffsetY, this.footDegree);
                        collisionData.reBlock = this.getBlock;
                        collisionData.chkPointID = collisionPointId;
                        return;
                    }
                    return;
            }
        }
    }

    private int getBlockLeftSide(int blockX, int blockY) {
        return (blockX + 0) * this.worldInstance.getTileWidth();
    }

    private int getBlockRightSide(int blockX, int blockY) {
        return ((blockX + 1) * this.worldInstance.getTileWidth()) - 1;
    }

    private int getBlockUpSide(int blockX, int blockY) {
        return (blockY + 0) * this.worldInstance.getTileHeight();
    }

    private int getBlockDownSide(int blockX, int blockY) {
        return ((blockY + 1) * this.worldInstance.getTileHeight()) - 1;
    }

    private void calObjPositionFromFoot() {
        this.footX = this.chkPointX - this.chkOffsetX;
        this.footY = this.chkPointY - this.chkOffsetY;
    }

    private void calChkPointFromPos() {
        this.chkPointX = this.footX + this.chkOffsetX;
        this.chkPointY = this.footY + this.chkOffsetY;
    }

    private void calChkOffset(int chkPointX, int chkPointY, int chkPointId, int degree) {
        int footX = ACUtilities.getRelativePointX(chkPointX, -this.footCollisionPointOffsetX[chkPointId], -this.footCollisionPointOffsetY, degree);
        int footY = ACUtilities.getRelativePointY(chkPointY, -this.footCollisionPointOffsetX[chkPointId], -this.footCollisionPointOffsetY, degree);
        this.chkOffsetX = chkPointX - footX;
        this.chkOffsetY = chkPointY - footY;
    }

    public byte getActionState() {
        return this.actionState;
    }

    private int getDegreeFromWorld(int currentDegree, int x, int y, int z) {
        while (currentDegree < 0) {
            currentDegree += MDPhone.SCREEN_WIDTH;
        }
        currentDegree %= MDPhone.SCREEN_WIDTH;
        if (this.degreeGetter == null) {
            return currentDegree;
        }
        this.degreeGetter.getDegreeFromWorldByPosition(this.degreeRe, currentDegree, x, y, z);
        return this.degreeRe.degree;
    }

    public int getDegreeDiff(int degree1, int degree2) {
        int re = Math.abs(degree1 - degree2);
        if (re > RollPlatformSpeedC.DEGREE_VELOCITY) {
            re = MDPhone.SCREEN_WIDTH - re;
        }
        if (re > 90) {
            return RollPlatformSpeedC.DEGREE_VELOCITY - re;
        }
        return re;
    }

    private void doSideCheckInGround(int direction) {
        int newX;
        int i;
        int degree;
        ACWorldCalUser aCWorldCalUser;
        if (this.moveDistanceX > 0) {
            if (direction == 0) {
                rightSideCollisionChk(this.footX, this.footY, direction, this.collisionData);
                newX = this.collisionData.newPosX;
            } else {
                leftSideCollisionChk(this.footX, this.footY, direction, this.collisionData);
                newX = this.collisionData.newPosX;
            }
            if (newX != -1000) {
                this.footX = newX;
                calChkPointFromPos();
                int i2 = this.footDegree;
                if (direction == 0) {
                    i = 270;
                } else {
                    i = 90;
                }
                degree = getDegreeFromWorld((i2 + i) % MDPhone.SCREEN_WIDTH, this.collisionData.collisionX, this.collisionData.collisionY, this.acObj.posZ);
                aCWorldCalUser = this.user;
                if (direction == 0) {
                    i = 1;
                } else {
                    i = 3;
                }
                aCWorldCalUser.doWhileTouchWorld(i, degree);
                this.moveDistanceX = 0;
                this.acObj.velX = 0;
            }
        } else if (this.moveDistanceX < 0) {
            if (direction == 0) {
                leftSideCollisionChk(this.footX, this.footY, direction, this.collisionData);
                newX = this.collisionData.newPosX;
            } else {
                rightSideCollisionChk(this.footX, this.footY, direction, this.collisionData);
                newX = this.collisionData.newPosX;
            }
            if (newX != -1000) {
                this.footX = newX;
                calChkPointFromPos();
                degree = getDegreeFromWorld((this.footDegree + (direction == 0 ? 90 : 270)) % MDPhone.SCREEN_WIDTH, this.collisionData.collisionX, this.collisionData.collisionY, this.acObj.posZ);
                aCWorldCalUser = this.user;
                if (direction == 0) {
                    i = 3;
                } else {
                    i = 1;
                }
                aCWorldCalUser.doWhileTouchWorld(i, degree);
                this.moveDistanceX = 0;
                this.acObj.velX = 0;
            }
        }
    }

    public void stopMoveX() {
        super.stopMoveX();
        if (this.actionState == (byte) 0) {
            int tmpMoveDistanceX = (this.totalDistance * MyAPI.dCos(this.user.getBodyDegree())) / 100;
            this.totalDistance = ACUtilities.getTotalFromDegree(0, (this.totalDistance * MyAPI.dSin(this.user.getBodyDegree())) / 100, this.user.getBodyDegree());
        }
    }

    public void stopMoveY() {
        super.stopMoveY();
        if (this.actionState == (byte) 0) {
            int dSin = (this.totalDistance * MyAPI.dSin(this.user.getBodyDegree())) / 100;
            this.totalDistance = ACUtilities.getTotalFromDegree((this.totalDistance * MyAPI.dCos(this.user.getBodyDegree())) / 100, 0, this.user.getBodyDegree());
        }
    }

    private boolean canBeSideStop(int direction) {
        boolean re = false;
        if (direction == 3 && ((getDirectionByDegree(this.footDegree) == 0 && (this.moveDistanceX <= 0 || this.acObj.velX <= 0)) || (getDirectionByDegree(this.footDegree) == 2 && (this.moveDistanceX >= 0 || this.acObj.velX >= 0)))) {
            re = true;
        }
        if (direction != 1) {
            return re;
        }
        if (getDirectionByDegree(this.footDegree) != 0 || (this.moveDistanceX < 0 && this.acObj.velX < 0)) {
            if (getDirectionByDegree(this.footDegree) != 2) {
                return re;
            }
            if (this.moveDistanceX > 0 && this.acObj.velX > 0) {
                return re;
            }
        }
        return true;
    }
}
