package com.sega.engine.action;

public interface ACFocusable {
    int getFocusX();

    int getFocusY();
}
