package com.sega.engine.action;

import com.sega.engine.lib.MyAPI;

public class ACMoveCalculator {
    protected ACObject acObj;
    private int chkPointX;
    private int chkPointY;
    protected int moveDistanceX;
    protected int moveDistanceY;
    protected ACMoveCalUser user;
    protected ACWorld worldInstance;

    public ACMoveCalculator(ACObject acObj, ACMoveCalUser user) {
        this.acObj = acObj;
        this.user = user;
        this.worldInstance = acObj.worldInstance;
    }

    public void actionLogic(int moveDistanceX, int moveDistanceY) {
        this.moveDistanceX = moveDistanceX;
        this.moveDistanceY = moveDistanceY;
        checkInMap();
    }

    private void checkInMap() {
        if (this.moveDistanceX == 0 && this.moveDistanceY == 0) {
            this.user.didAfterEveryMove(0, 0);
        }
        while (true) {
            if (this.moveDistanceX != 0 || this.moveDistanceY != 0) {
                this.chkPointX = this.acObj.posX;
                this.chkPointY = this.acObj.posY;
                int startPointX = this.chkPointX;
                int startPointY = this.chkPointY;
                checkInSky();
                this.acObj.posX = this.chkPointX;
                this.acObj.posY = this.chkPointY;
                this.user.didAfterEveryMove(this.chkPointX - startPointX, this.chkPointY - startPointY);
            } else {
                return;
            }
        }
    }

    private void checkInSky() {
        boolean xFirst;
        if (Math.abs(this.moveDistanceX) > Math.abs(this.moveDistanceY)) {
            xFirst = true;
        } else {
            xFirst = false;
        }
        int startPointX = this.chkPointX;
        int startPointY = this.chkPointY;
        int i;
        int i2;
        if (xFirst) {
            if (Math.abs(this.moveDistanceX) > this.worldInstance.getTileWidth()) {
                i = this.chkPointX;
                if (this.moveDistanceX > 0) {
                    i2 = 1;
                } else {
                    i2 = -1;
                }
                this.chkPointX = i + (i2 * this.worldInstance.getTileWidth());
                this.chkPointY += (this.moveDistanceY * this.worldInstance.getTileWidth()) / Math.abs(this.moveDistanceX);
            } else {
                this.chkPointX += this.moveDistanceX;
                this.chkPointY += this.moveDistanceY;
            }
        } else if (Math.abs(this.moveDistanceY) > this.worldInstance.getTileHeight()) {
            i = this.chkPointY;
            if (this.moveDistanceY > 0) {
                i2 = 1;
            } else {
                i2 = -1;
            }
            this.chkPointY = i + (i2 * this.worldInstance.getTileHeight());
            this.chkPointX += (this.moveDistanceX * this.worldInstance.getTileHeight()) / Math.abs(this.moveDistanceY);
        } else {
            this.chkPointX += this.moveDistanceX;
            this.chkPointY += this.moveDistanceY;
        }
        int preMoveDistanceX = this.moveDistanceX;
        int preMoveDistanceY = this.moveDistanceY;
        this.moveDistanceX -= this.chkPointX - startPointX;
        this.moveDistanceY -= this.chkPointY - startPointY;
        if (this.moveDistanceX * preMoveDistanceX <= 0) {
            this.moveDistanceX = 0;
        }
        if (this.moveDistanceY * preMoveDistanceY <= 0) {
            this.moveDistanceY = 0;
        }
    }

    public void stopMove(int degree) {
        int moveDistanceY2 = (((-this.moveDistanceX) * MyAPI.dSin(degree)) + (this.moveDistanceY * MyAPI.dCos(degree))) / 1;
        if (((this.moveDistanceX * MyAPI.dCos(degree)) + (this.moveDistanceY * MyAPI.dSin(degree))) / 1 > 0) {
            this.moveDistanceX = ((-moveDistanceY2) * MyAPI.dSin(degree)) / 10000;
            this.moveDistanceY = (MyAPI.dCos(degree) * moveDistanceY2) / 10000;
        }
    }

    public void stopMove() {
        stopMoveX();
        stopMoveY();
    }

    public void stopMoveX() {
        this.moveDistanceX = 0;
    }

    public void stopMoveY() {
        this.moveDistanceY = 0;
    }
}
