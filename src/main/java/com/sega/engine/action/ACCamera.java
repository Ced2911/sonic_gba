package com.sega.engine.action;

public class ACCamera {
    private static ACCamera instance;
    private ACFocusable focusObj;
    public int showHeight;
    public int showWidth;
    private ACWorld world;
    public int f0x;
    public int f1y;

    public static ACCamera getInstance() {
        if (instance == null) {
            instance = new ACCamera();
        }
        return instance;
    }

    private ACCamera() {
    }

    public void init(ACWorld world, ACFocusable focusObj, int showWidth, int showHeight) {
        setWorld(world);
        setFocusObj(focusObj);
        setShowSize(showWidth, showHeight);
    }

    public void setWorld(ACWorld world) {
        this.world = world;
    }

    public void setFocusObj(ACFocusable focusObj) {
        this.focusObj = focusObj;
    }

    public void setShowSize(int width, int height) {
        this.showWidth = width;
        this.showHeight = height;
    }

    public void logic() {
        if (this.focusObj != null && this.world != null) {
            this.f0x = this.focusObj.getFocusX() - (this.showWidth >> 1);
            this.f1y = this.focusObj.getFocusY() - (this.showHeight >> 1);
            if (this.f0x < 0) {
                this.f0x = 0;
            }
            if (this.f0x > this.world.getWorldWidth() - this.showWidth) {
                this.f0x = this.world.getWorldWidth() - this.showWidth;
            }
            if (this.f1y < 0) {
                this.f1y = 0;
            }
            if (this.f1y > this.world.getWorldHeight() - this.showHeight) {
                this.f1y = this.world.getWorldHeight() - this.showHeight;
            }
        }
    }
}
