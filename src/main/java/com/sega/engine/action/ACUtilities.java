package com.sega.engine.action;

import com.sega.engine.lib.MyAPI;

public class ACUtilities {
    public static int getQuaParam(int x, int divide) {
        if (x > 0) {
            return x / divide;
        }
        return (x - (divide - 1)) / divide;
    }

    public static int getRelativePointX(int originalX, int offsetX, int offsetY, int degree) {
        return (((MyAPI.dCos(degree) * offsetX) / 100) + originalX) - ((MyAPI.dSin(degree) * offsetY) / 100);
    }

    public static int getRelativePointY(int originalY, int offsetX, int offsetY, int degree) {
        return (((MyAPI.dSin(degree) * offsetX) / 100) + originalY) + ((MyAPI.dCos(degree) * offsetY) / 100);
    }

    public static int getTotalFromDegree(int x, int y, int degree) {
        return ((MyAPI.dCos(degree) * x) + (MyAPI.dSin(degree) * y)) / 100;
    }

    public static int getNewXFromDegree(int x, int y, int degree) {
        return (MyAPI.dCos(degree) * getTotalFromDegree(x, y, degree)) / 100;
    }

    public static int getNewXFromDegree(int total, int degree) {
        return (MyAPI.dCos(degree) * total) / 100;
    }

    public static int getNewYFromDegree(int x, int y, int degree) {
        return (MyAPI.dSin(degree) * getTotalFromDegree(x, y, degree)) / 100;
    }

    public static int getNewYFromDegree(int total, int degree) {
        return (MyAPI.dSin(degree) * total) / 100;
    }
}
