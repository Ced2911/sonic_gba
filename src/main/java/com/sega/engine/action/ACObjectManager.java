package com.sega.engine.action;

import com.sega.engine.lib.Iterator;
import java.lang.reflect.Array;
import java.util.Vector;

public class ACObjectManager {
    private static int currentVecX;
    private static int currentVecY;
    private static int destroyHeightRange;
    private static int destroyWidthRange;
    private static int heightRange;
    private static int roomHeight;
    private static Vector[][] roomVec;
    private static int roomWidth;
    private static int vecHeight;
    private static int vecWidth;
    private static int widthRange;

    public static class ObjectIterator implements Iterator {
        private Vector currentVec = new Vector();
        private boolean firstGet;
        private int objIndex = 0;
        private Vector objectVec = new Vector();
        private int vecIndex = 0;

        private Vector getVector() {
            return this.objectVec;
        }

        private void init() {
            this.objIndex = 0;
            this.vecIndex = 0;
            findNextAvailiable();
            this.firstGet = false;
        }

        private void init(Vector objVec) {
            this.objectVec = objVec;
            init();
        }

        public boolean hasNext() {
            int vecIndex2 = this.vecIndex;
            int objIndex2 = this.objIndex;
            Vector currentVec2 = null;
            if (this.firstGet) {
                objIndex2++;
            }
            while (vecIndex2 < this.objectVec.size()) {
                currentVec2 = (Vector) this.objectVec.elementAt(vecIndex2);
                if (objIndex2 < currentVec2.size()) {
                    break;
                }
                objIndex2 = 0;
                vecIndex2++;
            }
            if (vecIndex2 < this.objectVec.size() || (currentVec2 != null && objIndex2 < currentVec2.size())) {
                return true;
            }
            return false;
        }

        public Object next() {
            if (this.firstGet) {
                this.objIndex++;
            } else {
                this.firstGet = true;
            }
            findNextAvailiable();
            if (this.vecIndex < this.objectVec.size() || this.objIndex < this.currentVec.size()) {
                return this.currentVec.elementAt(this.objIndex);
            }
            return null;
        }

        private void findNextAvailiable() {
            while (this.vecIndex < this.objectVec.size()) {
                this.currentVec = (Vector) this.objectVec.elementAt(this.vecIndex);
                if (this.objIndex >= this.currentVec.size()) {
                    this.objIndex = 0;
                    this.vecIndex++;
                } else {
                    return;
                }
            }
        }

        public void remove() {
            if (this.vecIndex < this.objectVec.size() || this.objIndex < this.currentVec.size()) {
                this.currentVec.removeElementAt(this.objIndex);
                this.firstGet = false;
            }
        }
    }

    public static void init(ACWorld world, int mRoomWidth, int mRoomHeight, int mWidthRange, int mHeightRange) {
        roomWidth = mRoomWidth;
        roomHeight = mRoomHeight;
        vecWidth = ((world.getWorldWidth() + mRoomWidth) - 1) / mRoomWidth;
        vecHeight = ((world.getWorldHeight() + mRoomHeight) - 1) / mRoomHeight;
        roomVec = (Vector[][]) Array.newInstance(Vector.class, new int[]{vecWidth, vecHeight});
        for (int w = 0; w < vecWidth; w++) {
            for (int h = 0; h < vecHeight; h++) {
                roomVec[w][h] = new Vector();
            }
        }
        widthRange = mWidthRange;
        heightRange = mHeightRange;
        destroyWidthRange = mWidthRange + 1;
        destroyHeightRange = mHeightRange + 1;
    }

    public static void addObject(ACObject obj) {
        if (obj != null) {
            int vecX = obj.posX / roomWidth;
            int vecY = obj.posY / roomHeight;
            if (vecX >= 0 && vecX < vecWidth && vecY >= 0 && vecY < vecHeight) {
                roomVec[vecX][vecY].addElement(obj);
            }
        }
    }

    public static void objectsLogic() {
        checkObjectsWhenCameraMoved();
    }

    public static void getObjectsNearCamera(ObjectIterator iterator) {
        iterator.getVector().removeAllElements();
        ACCamera camera = ACCamera.getInstance();
        int centerX = (camera.f0x + (camera.showWidth >> 1)) / roomWidth;
        int centerY = (camera.f1y + (camera.showHeight >> 1)) / roomHeight;
        if (centerX >= 0 && centerX < vecWidth && centerY >= 0 && centerY < vecHeight) {
            int startX = centerX - widthRange;
            int startY = centerY - heightRange;
            int endX = centerX + widthRange;
            int endY = centerY + heightRange;
            if (startX < 0) {
                startX = 0;
            }
            if (startY < 0) {
                startY = 0;
            }
            if (endX >= vecWidth) {
                endX = vecWidth - 1;
            }
            if (endY >= vecHeight) {
                endY = vecHeight - 1;
            }
            if (roomVec != null) {
                for (int x = startX; x <= endX; x++) {
                    for (int y = startY; y <= endY; y++) {
                        iterator.getVector().addElement(roomVec[x][y]);
                    }
                }
                iterator.init();
            }
        }
    }

    public static void getObjectsNear(ACObject obj, ObjectIterator iterator) {
        iterator.getVector().removeAllElements();
        int centerX = obj.posX / roomWidth;
        int centerY = obj.posY / roomHeight;
        if (centerX >= 0 && centerX < vecWidth && centerY >= 0 && centerY < vecHeight) {
            int startX = centerX - widthRange;
            int startY = centerY - heightRange;
            int endX = centerX + widthRange;
            int endY = centerY + heightRange;
            if (startX < 0) {
                startX = 0;
            }
            if (startY < 0) {
                startY = 0;
            }
            if (endX >= vecWidth) {
                endX = vecWidth - 1;
            }
            if (endY >= vecHeight) {
                endY = vecHeight - 1;
            }
            if (roomVec != null) {
                for (int x = startX; x <= endX; x++) {
                    for (int y = startY; y <= endY; y++) {
                        iterator.getVector().addElement(roomVec[x][y]);
                    }
                }
                iterator.init();
            }
        }
    }

    private static void checkObjectsWhenCameraMoved() {
        ACCamera camera = ACCamera.getInstance();
        int centerVecX = (camera.f0x + (camera.showWidth >> 1)) / roomWidth;
        int centerVecY = ((camera.showHeight >> 1) + camera.f1y) / roomHeight;
        int xo;
        int i;
        if (currentVecX == -1 || currentVecY == -1) {
            xo = centerVecX - widthRange;
            while (xo <= widthRange + centerVecX) {
                if (xo >= 0 && xo < vecWidth) {
                    int yo = centerVecY - heightRange;
                    while (yo <= heightRange + centerVecY) {
                        if (yo >= 0 && yo < vecHeight) {
                            for (i = 0; i < roomVec[xo][yo].size(); i++) {
                                ACObject aCObject = (ACObject) roomVec[xo][yo].elementAt(i);
                            }
                        }
                        yo++;
                    }
                }
                xo++;
            }
        } else if (!(currentVecX == centerVecX && currentVecY == centerVecY)) {
            ACObject obj;
            int objBlockX;
            int objBlockY;
            int xOffset = centerVecX - currentVecX;
            int yOffset = centerVecY - currentVecY;
            if (xOffset != 0) {
                i = (xOffset > 0 ? -destroyWidthRange : destroyWidthRange) + centerVecX;
                if (i >= 0 && i < vecWidth) {
                    int yo2 = -destroyHeightRange;
                    while (yo2 <= destroyHeightRange) {
                        if (centerVecY + yo2 >= 0 && centerVecY + yo2 < vecHeight) {
                            xo = 0;
                            while (xo < roomVec[i][centerVecY + yo2].size()) {
                                obj = (ACObject) roomVec[i][centerVecY + yo2].elementAt(xo);
                                objBlockX = obj.posX / roomWidth;
                                objBlockY = obj.posY / roomHeight;
                                if (objBlockX >= 0 && objBlockX < vecWidth && objBlockY >= 0 && objBlockY < vecHeight && !(objBlockX == i && objBlockY == centerVecY + yo2)) {
                                    roomVec[i][centerVecY + yo2].removeElementAt(xo);
                                    xo--;
                                    roomVec[objBlockX][objBlockY].addElement(obj);
                                }
                                xo++;
                            }
                        }
                        yo2++;
                    }
                }
                xo = centerVecX + (xOffset > 0 ? widthRange : -widthRange);
                if (xo >= 0 && xo < vecWidth) {
                    xOffset = centerVecY - heightRange;
                    while (xOffset <= heightRange + centerVecY) {
                        if (xOffset >= 0 && xOffset < vecHeight) {
                            i = 0;
                            while (i < roomVec[xo][xOffset].size()) {
                                obj = (ACObject) roomVec[xo][xOffset].elementAt(i);
                                objBlockX = obj.posX / roomWidth;
                                objBlockY = obj.posY / roomHeight;
                                if (objBlockX >= 0 && objBlockX < vecWidth && objBlockY >= 0 && objBlockY < vecHeight && !(objBlockX == xo && objBlockY == xOffset)) {
                                    roomVec[xo][xOffset].removeElementAt(i);
                                    i--;
                                    roomVec[objBlockX][objBlockY].addElement(obj);
                                }
                                i++;
                            }
                        }
                        xOffset++;
                    }
                }
            }
            if (yOffset != 0) {
                i = (yOffset > 0 ? -destroyHeightRange : destroyHeightRange) + centerVecY;
                if (i >= 0 && i < vecHeight) {
                    xOffset = -destroyWidthRange;
                    while (xOffset <= destroyWidthRange) {
                        if (centerVecX + xOffset >= 0 && centerVecX + xOffset < vecWidth) {
                            xo = 0;
                            while (xo < roomVec[centerVecX + xOffset][i].size()) {
                                obj = (ACObject) roomVec[centerVecX + xOffset][i].elementAt(xo);
                                objBlockX = obj.posX / roomWidth;
                                objBlockY = obj.posY / roomHeight;
                                if (objBlockX >= 0 && objBlockX < vecWidth && objBlockY >= 0 && objBlockY < vecHeight && !(objBlockX == centerVecX + xOffset && objBlockY == i)) {
                                    roomVec[centerVecX + xOffset][i].removeElementAt(xo);
                                    xo--;
                                    roomVec[objBlockX][objBlockY].addElement(obj);
                                }
                                xo++;
                            }
                        }
                        xOffset++;
                    }
                }
                xo = centerVecY + (yOffset > 0 ? heightRange : -heightRange);
                if (xo >= 0 && xo < vecHeight) {
                    xOffset = centerVecX - widthRange;
                    while (xOffset <= widthRange + centerVecX) {
                        if (xOffset >= 0 && xOffset < vecWidth) {
                            i = 0;
                            while (i < roomVec[xOffset][xo].size()) {
                                obj = (ACObject) roomVec[xOffset][xo].elementAt(i);
                                objBlockX = obj.posX / roomWidth;
                                objBlockY = obj.posY / roomHeight;
                                if (objBlockX >= 0 && objBlockX < vecWidth && objBlockY >= 0 && objBlockY < vecHeight && !(objBlockX == xOffset && objBlockY == xo)) {
                                    roomVec[xOffset][xo].removeElementAt(i);
                                    i--;
                                    roomVec[objBlockX][objBlockY].addElement(obj);
                                }
                                i++;
                            }
                        }
                        xOffset++;
                    }
                }
            }
        }
        currentVecX = centerVecX;
        currentVecY = centerVecY;
    }
}
