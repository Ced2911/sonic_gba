package com.sega.mobile.define;

public interface MDPhone {
    public static final int KEY_CODE_BACK = 4;
    public static final int KEY_CODE_MENU = 82;
    public static final int KEY_CODE_NUM_0 = 7;
    public static final int KEY_CODE_NUM_1 = 8;
    public static final int KEY_CODE_NUM_2 = 9;
    public static final int KEY_CODE_NUM_3 = 10;
    public static final int KEY_CODE_NUM_4 = 11;
    public static final int KEY_CODE_NUM_5 = 12;
    public static final int KEY_CODE_NUM_6 = 13;
    public static final int KEY_CODE_NUM_7 = 14;
    public static final int KEY_CODE_NUM_8 = 15;
    public static final int KEY_CODE_NUM_9 = 16;
    public static final int KEY_CODE_NUM_POUND = 47;
    public static final int KEY_CODE_NUM_STAR = 29;
    public static final int KEY_CODE_O = 4;
    public static final int KEY_CODE_PAD_DOWN = 20;
    public static final int KEY_CODE_PAD_FIRE = 23;
    public static final int KEY_CODE_PAD_LEFT = 21;
    public static final int KEY_CODE_PAD_RIGHT = 22;
    public static final int KEY_CODE_PAD_UP = 19;
    public static final int KEY_CODE_SOFT_1 = 54;
    public static final int KEY_CODE_SOFT_2 = 52;
    public static final int KEY_CODE_X = 23;
    public static final int LARGE_FONT_HEIGHT = 30;
    public static final int MAX_PROFETCHED_SE = 15;
    public static final int MEDIUM_FONT_HEIGHT = 26;
    public static final int SCREEN_HEIGHT = 640;
    public static final int SCREEN_WIDTH = 360;
    public static final boolean SHIELD_NUMBER_KEY = true;
    public static final int SMALL_FONT_HEIGHT = 24;
    public static final boolean SUPPORT_JOYSTICK = false;
    public static final boolean SUPPORT_TOUCH = true;
}
