package com.sega.mobile.framework.android;

import android.content.Context;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import com.sega.mobile.framework.MFMain;
import com.sega.mobile.framework.device.MFDevice;

public class Canvas extends SurfaceView implements Callback {
    public int PID_BUFFER_SIZE = 10;
    private boolean initialFlag = false;
    protected Graphics mGraphics = new Graphics();
    private SurfaceHolder mHolder;
    int[] pidBuffer = new int[this.PID_BUFFER_SIZE];
    private boolean useMultiTouch = false;

    public Canvas(Context context) {
        super(context);
        for (int i = 0; i < this.PID_BUFFER_SIZE; i++) {
            this.pidBuffer[i] = -1;
        }
        this.mHolder = getHolder();
        this.mHolder.addCallback(this);
        setFocusable(true);
    }

    public void setFilterBitmap(boolean b) {
        this.mGraphics.setFilterBitmap(b);
    }

    public void setAntiAlias(boolean b) {
        this.mGraphics.setAntiAlias(b);
    }

    public void setUseMultitouch(boolean b) {
        this.useMultiTouch = b;
    }

    public void surfaceCreated(SurfaceHolder holder) {
        MFDevice.notifyStart(getWidth(), getHeight());
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
    }

    public boolean keyDown(int keyCode, KeyEvent msg) {
        keyPressed(keyCode);
        return true;
    }

    public boolean keyUp(int keyCode, KeyEvent msg) {
        keyReleased(keyCode);
        return true;
    }

    private void increaseBuffer() {
        this.PID_BUFFER_SIZE += 10;
        int[] tempBuffer = new int[this.PID_BUFFER_SIZE];
        for (int i = 0; i < this.PID_BUFFER_SIZE - 10; i++) {
            tempBuffer[i] = this.pidBuffer[i];
        }
        this.pidBuffer = tempBuffer;
    }

    public boolean touchEvent(MotionEvent event) {
        if (!this.useMultiTouch) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    pointerPressed(0, (int) event.getX(), (int) event.getY());
                    break;
                case MotionEvent.ACTION_UP:
                    pointerReleased(0, (int) event.getX(), (int) event.getY());
                    break;
                case MotionEvent.ACTION_MOVE:
                    pointerDragged(0, (int) event.getX(), (int) event.getY());
                    break;
                default:
                    break;
            }
        }
        int count = event.getPointerCount();
        if (count >= this.PID_BUFFER_SIZE) {
            count = this.PID_BUFFER_SIZE - 1;
        }
        int action = event.getAction();
        int index = action >> 8;
        int id = event.getPointerId(index);
        if (id >= this.PID_BUFFER_SIZE) {
            increaseBuffer();
        }
        int i;
        switch (action & 255) {
            case 0:
            case 5:
                this.pidBuffer[id] = id;
                pointerPressed(this.pidBuffer[id], (int) event.getX(index), (int) event.getY(index));
                break;
            case 1:
            case 6:
                if (this.pidBuffer[id] == -1) {
                    for (i = 0; i < this.PID_BUFFER_SIZE; i++) {
                        if (this.pidBuffer[i] != -1) {
                            pointerReleased(this.pidBuffer[i], (int) event.getX(index), (int) event.getY(index));
                            this.pidBuffer[i] = -1;
                            break;
                        }
                    }
                    break;
                }
                pointerReleased(this.pidBuffer[id], (int) event.getX(index), (int) event.getY(index));
                this.pidBuffer[id] = -1;
                break;
            case 2:
                for (i = 0; i < count; i++) {
                    int id2 = event.getPointerId(i);
                    if (this.pidBuffer[id2] != -1) {
                        pointerDragged(this.pidBuffer[id2], (int) event.getX(i), (int) event.getY(i));
                    } else {
                        int j = 0;
                        while (j < this.PID_BUFFER_SIZE) {
                            if (this.pidBuffer[j] != -1) {
                                this.pidBuffer[id2] = this.pidBuffer[j];
                                this.pidBuffer[j] = -1;
                                pointerDragged(this.pidBuffer[id2], (int) event.getX(i), (int) event.getY(i));
                            } else {
                                j++;
                            }
                        }
                    }
                }
                break;
        }
        return true;
    }

    public boolean onTrackballEvent(MotionEvent event) {
        System.out.println("todo: onTrackballEvent");
        return true;
        /*
        if (MFDevice.enableTrackBall) {
            switch (event.getAction()) {
                case 0:
                    keyPressed(23);
                    break;
                case 1:
                    keyReleased(23);
                    break;
                case 2:
                    if (event.getX() <= 0.0f) {
                        if (event.getX() >= 0.0f) {
                            if (event.getY() <= 0.0f) {
                                if (event.getY() < 0.0f) {
                                    trackballMoved(19);
                                    break;
                                }
                            }
                            trackballMoved(20);
                            break;
                        }
                        trackballMoved(21);
                        break;
                    }
                    trackballMoved(22);
                    break;
                    break;
            }
        }
        return true;
        */
    }

    public void onWindowFocusChanged(boolean hasWindowFocus) {
        if (!this.initialFlag) {
            this.initialFlag = true;
        }
        if (hasWindowFocus) {
            setFocusable(true);
            for (int i = 0; i < this.PID_BUFFER_SIZE; i++) {
                this.pidBuffer[i] = -1;
            }
            showNotify();
            return;
        }
        hideNotify();
    }

    public boolean initialized() {
        return this.initialFlag;
    }

    public void hideNotify() {
    }

    public void showNotify() {
    }

    public void keyPressed(int keyCode) {
    }

    public void keyReleased(int keyCode) {
    }

    public void trackballMoved(int keyCode) {
    }

    public void pointerPressed(int id, int x, int y) {
    }

    public void pointerReleased(int id, int x, int y) {
    }

    public void pointerDragged(int id, int x, int y) {
    }

    public void paint(Graphics g) {
    }

    public void repaint() {
        try {
            this.mGraphics.setCanvas(this.mHolder.lockCanvas());
            synchronized (this.mHolder) {
                if (this.mGraphics.getCanvas() != null) {
                    paint(this.mGraphics);
                }
            }
            if (this.mGraphics.getCanvas() != null) {
                this.mHolder.unlockCanvasAndPost(this.mGraphics.getCanvas());
            }
        } catch (Throwable th) {
            if (this.mGraphics.getCanvas() != null) {
                this.mHolder.unlockCanvasAndPost(this.mGraphics.getCanvas());
            }
        }
    }

    public void serviceRepaints() {
    }

    public void setFullScreenMode(boolean b) {
        if (b) {
            MFMain.getInstance().requestWindowFeature(1);
        }
    }
}
