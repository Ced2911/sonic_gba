package com.sega.mobile.framework.port;

public class MFStringArray {
    public String[] array;
    public int len;

    public MFStringArray(int length) {
        this.array = new String[length];
        this.len = length;
    }
}
