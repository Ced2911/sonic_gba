package com.sega.mobile.framework;

import SonicGBA.MapManager;
import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnKeyListener;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.MotionEvent;
import com.sega.mobile.framework.android.Canvas;
import com.sega.mobile.framework.device.MFDevice;
import com.sega.mobile.framework.device.MFGamePad;
import com.sega.mobile.framework.device.MFGraphics;

public abstract class MFMain extends Activity {
    private static MFMain instance;
    private static Canvas mCanvas;
    private static boolean mCreated = false;
    private Handler mHandler = new Handler();

    class C00031 implements OnKeyListener {
        C00031() {
        }

        public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
            if (keyCode == 84 || keyCode == 82) {
                return true;
            }
            return false;
        }
    }

    class C00042 implements OnClickListener {
        C00042() {
        }

        public void onClick(DialogInterface dialog, int which) {
            MFMain.this.dialogExit();
            MFMain.this.notifyDestroyed();
        }
    }

    class C00053 implements OnClickListener {
        C00053() {
        }

        public void onClick(DialogInterface dialog, int which) {
        }
    }

    public abstract MFGameState getEntryGameState();

    public static final MFMain getInstance() {
        return instance;
    }

    public String getAppProperty(String key) {
        return "Android";
    }

    public Handler getHandler() {
        return this.mHandler;
    }

    public void drawDeviceRotated(MFGraphics g, int width, int height) {
    }

    public boolean logicDeviceSuspend() {
        if (MFGamePad.isKeyPress(2113)) {
            return true;
        }
        return false;
    }

    public void drawDeviceSuspend(MFGraphics g) {
        g.enableExceedBoundary();
        g.clearScreen(0);
        g.setFont(-1);
        g.setColor(MapManager.END_COLOR);
        g.drawString("Press Enter", MFDevice.getScreenWidth() >> 1, MFDevice.getScreenHeight() >> 1, 3);
        g.drawString("Confirm", 0, MFDevice.getScreenHeight(), 36);
        g.disableExceedBoundary();
    }

    public void dialogExit() {
    }

    public void notifyDestroyed() {
        finish();
        MFDevice.openUrl();
        System.exit(0);
    }

    public void platformRequest(String url) {
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url)));
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!mCreated) {
            mCreated = true;
            requestWindowFeature(1);
            getWindow().setFlags(1024, 1024);
            setVolumeControlStream(3);
            if (instance == null) {
                instance = this;
                mCanvas = (Canvas) MFDevice.getSystemDisplayable();
                getInstance().setContentView(mCanvas);
            }
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 24) {
            MFDevice.notifyVolumeChange(true);
            if (MFDevice.getEnableVolumeKey()) {
                return false;
            }
            return true;
        } else if (keyCode == 25) {
            MFDevice.notifyVolumeChange(false);
            if (MFDevice.getEnableVolumeKey()) {
                return false;
            }
            return true;
        } else if (keyCode == 4) {
            if (!MFDevice.getEnableCustomBack()) {
                showExitConfirm();
            } else if (event.getAction() == 0) {
                mCanvas.keyPressed(keyCode);
            }
            return true;
        } else {
            mCanvas.keyDown(keyCode, event);
            if (keyCode == 84 || keyCode == 82) {
                return true;
            }
            return false;
        }
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        mCanvas.keyUp(keyCode, event);
        if (keyCode == 84 || keyCode == 82) {
            return true;
        }
        return false;
    }

    public boolean onTouchEvent(MotionEvent event) {
        mCanvas.touchEvent(event);
        return true;
    }

    public void showExitConfirm() {
        setExitConfirmStr("Confirm", "Are you sure you want to quit?", "OK", "Cancel");
    }

    public void setExitConfirmStr(String title, String message, String positive, String negative) {
        new Builder(getInstance()).setTitle(title).setMessage(message).setOnKeyListener(new C00031()).setPositiveButton(positive, new C00042()).setNegativeButton(negative, new C00053()).show();
    }

    public void onDestroy() {
        System.exit(0);
        super.onDestroy();
    }
}
