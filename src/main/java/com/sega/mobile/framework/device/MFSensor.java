package com.sega.mobile.framework.device;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import com.sega.mobile.framework.MFMain;
import java.util.List;

public class MFSensor {
    private static SensorManager mManager;
    private static float f8x;
    private static float f9y;
    private static float f10z;

    private static SensorEventListener listener = new SensorEventListener() {

        public void onSensorChanged(SensorEvent event) {
            if (event.sensor.getType() == 1) {
                MFSensor.f8x = event.values[0];
                MFSensor.f9y = event.values[1];
                MFSensor.f10z = event.values[2];
            }
        }

        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }
    };

    public static float getAccX() {
        return f8x;
    }

    public static float getAccY() {
        return f9y;
    }

    public static float getAccZ() {
        return f10z;
    }

    public static void init() {
        mManager = (SensorManager) MFMain.getInstance().getSystemService("sensor");
        List<Sensor> sensors = mManager.getSensorList(1);
        if (sensors.size() > 0) {
            mManager.registerListener(listener, (Sensor) sensors.get(0), 0);
        }
    }
}
