package com.sega.mobile.framework.device;

import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import com.sega.mobile.framework.MFMain;
import com.sega.mobile.framework.utility.MFUtility;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public final class MFPlayer implements OnCompletionListener {
    public static final int CLOSED = 4;
    public static final int PREFETCHED = 2;
    public static final int REALIZED = 1;
    public static final int STARTED = 3;
    private static final String[] STATE_NAME = new String[]{"UNREALIZED", "REALIZED", "PREFETCHED", "STARTED", "CLOSED"};
    public static final int UNREALIZED = 0;
    private long duration;
    private MediaPlayer mPlayer;
    private int mediaTimeForSet = 0;
    private boolean soundLoop;
    protected int soundPriority;
    private int soundState;
    private int soundType;
    protected String soundUrl;
    private int soundVolume;

    public static MFPlayer createMFPlayer(String url) {
        return new MFPlayer(url, 0);
    }

    public static MFPlayer createMFPlayer(InputStream stream, String type) throws IOException {
        if (type.indexOf("mid") != -1) {
            type = ".mid";
        } else if (type.indexOf("wav") != -1) {
            type = ".wav";
        } else if (type.indexOf("mpeg") != -1) {
            type = ".mp3";
        }
        String file = "tempsound" + type;
        FileOutputStream fos = MFMain.getInstance().openFileOutput(file, 0);
        byte[] buf = new byte[1024];
        while (true) {
            int len = stream.read(buf);
            if (len <= 0) {
                fos.flush();
                fos.close();
                return new MFPlayer(file, 1);
            }
            fos.write(buf, 0, len);
        }
    }

    private MFPlayer(String url, int type) {
        this.soundUrl = url;
        this.soundType = type;
        this.soundLoop = false;
        this.soundState = 0;
        this.soundVolume = 100;
        this.duration = 0;
    }

    public void setLoop(boolean loop) {
        this.soundLoop = loop;
    }

    public boolean isLoop() {
        return this.soundLoop;
    }

    public void setMediaTime(int msec) {
        if (this.mPlayer == null || this.soundState != 3) {
            this.mediaTimeForSet = msec;
        } else {
            this.mPlayer.seekTo(msec);
        }
    }

    public void setVolume(int volume) {
        this.soundVolume = MFUtility.getValueInRange(volume, 0, 100);
        if (this.mPlayer != null) {
            this.mPlayer.setVolume(((float) this.soundVolume) / 100.0f, ((float) this.soundVolume) / 100.0f);
        }
    }

    public int getVolume() {
        return this.soundVolume;
    }

    public int getState() {
        return this.soundState;
    }

    public void realize() {
        switch (this.soundState) {
            case 0:
            case 4:
                try {
                    if (this.soundType == 0) {
                        String substring;
                        AssetManager assets = MFMain.getInstance().getAssets();
                        if (this.soundUrl.startsWith("/")) {
                            substring = this.soundUrl.substring(1);
                        } else {
                            substring = this.soundUrl;
                        }
                        AssetFileDescriptor afd = assets.openFd(substring);
                        this.mPlayer = new MediaPlayer();
                        this.mPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
                        afd.close();
                    } else {
                        FileInputStream fis = MFMain.getInstance().openFileInput(this.soundUrl);
                        this.mPlayer = new MediaPlayer();
                        this.mPlayer.setDataSource(fis.getFD());
                        fis.close();
                    }
                    this.mPlayer.setAudioStreamType(3);
                    this.mPlayer.prepare();
                    this.duration = (long) this.mPlayer.getDuration();
                    this.mPlayer.setOnCompletionListener(this);
                    this.soundState = 1;
                    return;
                } catch (Exception e) {
                    e.printStackTrace();
                    return;
                }
            case 1:
            case 2:
            case 3:
                mediaError(this.soundUrl, "realize", STATE_NAME[this.soundState]);
                return;
            default:
                return;
        }
    }

    public void prefetch() {
        switch (this.soundState) {
            case 0:
            case 2:
            case 3:
            case 4:
                mediaError(this.soundUrl, "prefetch", STATE_NAME[this.soundState]);
                return;
            case 1:
                try {
                    this.soundState = 2;
                    return;
                } catch (Exception e) {
                    e.printStackTrace();
                    return;
                }
            default:
                return;
        }
    }

    public void start() {
        switch (this.soundState) {
            case 0:
            case 1:
            case 3:
            case 4:
                mediaError(this.soundUrl, "start", STATE_NAME[this.soundState]);
                return;
            case 2:
                try {
                    boolean z;
                    this.mPlayer.seekTo(this.mediaTimeForSet);
                    this.mediaTimeForSet = 0;
                    MediaPlayer mediaPlayer = this.mPlayer;
                    if (this.soundLoop) {
                        z = true;
                    } else {
                        z = false;
                    }
                    mediaPlayer.setLooping(z);
                    this.mPlayer.start();
                    this.soundState = 3;
                    return;
                } catch (Exception e) {
                    e.printStackTrace();
                    return;
                }
            default:
                return;
        }
    }

    public void stop() {
        switch (this.soundState) {
            case 0:
            case 1:
            case 4:
                mediaError(this.soundUrl, "stop", STATE_NAME[this.soundState]);
                return;
            case 3:
                try {
                    this.mPlayer.stop();
                    this.soundState = 2;
                    return;
                } catch (Exception e) {
                    e.printStackTrace();
                    return;
                }
            default:
                return;
        }
    }

    public void deallocate() {
        switch (this.soundState) {
            case 0:
            case 1:
            case 4:
                mediaError(this.soundUrl, "deallocate", STATE_NAME[this.soundState]);
                return;
            case 2:
                break;
            case 3:
                stop();
                break;
            default:
                return;
        }
        try {
            this.mPlayer.release();
            this.soundState = 1;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void close() {
        switch (this.soundState) {
            case 0:
            case 1:
                break;
            case 2:
                break;
            case 3:
                stop();
                break;
            default:
                return;
        }
        deallocate();
        if (this.mPlayer != null) {
            this.mPlayer.setOnCompletionListener(null);
            this.mPlayer.release();
        }
        this.mPlayer = null;
        this.soundState = 4;
    }

    public long getMediaTime() {
        if (this.mPlayer == null) {
            return 0;
        }
        return (long) this.mPlayer.getCurrentPosition();
    }

    public long getDuration() {
        return this.duration;
    }

    protected void tick() {
    }

    private static void mediaError(String url, String function, String state) {
    }

    public void onCompletion(MediaPlayer mp) {
        if (!this.soundLoop) {
            this.soundState = 2;
        }
    }
}
