package com.sega.mobile.framework.device;

import com.sega.mobile.define.MDPhone;

public class MFGamePad {
    private static int KEY = 0;
    public static final int KEY_A = 0x840;
    public static final int KEY_ANYKEY = -1;
    public static final int KEY_B = 0x20000;
    public static final int KEY_BACK = 0x80000;
    public static final int KEY_C = 0x10000;
    public static final int KEY_D = 0x40000;
    public static final int KEY_DOWN = 0x4008;
    public static final int KEY_DOWN_LEFT = 0x2000;
    public static final int KEY_DOWN_RIGHT = 0x8000;
    public static final int KEY_JOYSTICK_L1 = 0x4000000;
    public static final int KEY_JOYSTICK_MENU = 0x800000;
    public static final int KEY_JOYSTICK_O = 0x80000;
    public static final int KEY_JOYSTICK_R1 = 0x8000000;
    public static final int KEY_JOYSTICK_RECT = 0x200000;
    public static final int KEY_JOYSTICK_SELECT = 0x1000000;
    public static final int KEY_JOYSTICK_START = 0x2000000;
    public static final int KEY_JOYSTICK_TRIANGLE = 0x400000;
    public static final int KEY_JOYSTICK_X = 0x100000;
    public static final int KEY_LEFT = 0x410;
    public static final int KEY_NULL = 0;
    public static final int KEY_NUM_0 = 131072;
    public static final int KEY_NUM_1 = 128;
    public static final int KEY_NUM_2 = 256;
    public static final int KEY_NUM_3 = 512;
    public static final int KEY_NUM_4 = 1024;
    public static final int KEY_NUM_5 = 2048;
    public static final int KEY_NUM_6 = 4096;
    public static final int KEY_NUM_7 = 8192;
    public static final int KEY_NUM_8 = 16384;
    public static final int KEY_NUM_9 = 32768;
    public static final int KEY_NUM_POUND = 262144;
    public static final int KEY_NUM_STAR = 65536;
    public static final int KEY_PAD_CONFIRM = 64;
    public static final int KEY_PAD_DOWN = 0x8;
    public static final int KEY_PAD_LEFT = 0x10;
    public static final int KEY_PAD_RIGHT = 0x20;
    public static final int KEY_PAD_UP = 0x4;
    private static int KEY_R = 0;
    public static final int KEY_RIGHT = 0x1020;
    public static final int KEY_S1 = 0x1;
    public static final int KEY_S2 = 2;
    public static final int KEY_UP = 0x104;
    public static final int KEY_UP_LEFT = 0x80;
    public static final int KEY_UP_RIGHT = 0x200;
    private static int KeyOld = 0;
    private static int KeyPress = 0;
    private static int KeyRelease = 0;
    private static int KeyRepeat = 0;
    private static int deviceKeyValue = 0;
    private static boolean lastTrackballAvailiable = true;
    private static boolean trackballAvailiable = true;
    private static int trackballKey;

    public static final void resetKeys() {
        KEY = 0;
        KEY_R = 0;
        KeyPress = 0;
        KeyRepeat = 0;
        KeyRelease = 0;
        KeyOld = 0;
        deviceKeyValue = 0;
    }

    public static final boolean isKeyPress(int key) {
        return (KeyPress & key) != 0;
    }

    public static final boolean isKeyRepeat(int key) {
        return (KeyRepeat & key) != 0;
    }

    public static final boolean isKeyRelease(int key) {
        return (KeyRelease & key) != 0;
    }

    public static final int getKeyPress() {
        return KeyPress;
    }

    public static final int getKeyRepeat() {
        return KeyRepeat;
    }

    public static final int getKeyRelease() {
        return KeyRelease;
    }

    protected static final void keyTick() {
        KeyRepeat = KEY;
        KeyPress = (KeyOld ^ -1) & KeyRepeat;
        KEY &= KEY_R ^ -1;
        KeyOld = KeyRepeat;
        KeyRepeat = KEY;
        KeyRelease = KeyOld & (KeyRepeat ^ -1);
        KeyOld = KeyRepeat;
        KEY_R = 0;
        if (lastTrackballAvailiable) {
            if (trackballKey != 0) {
                releaseVisualKey(decodeKey(trackballKey));
                trackballKey = 0;
            }
            trackballAvailiable = true;
        }
        lastTrackballAvailiable = trackballAvailiable;
    }

    protected static final void keyPressed(int keyCode) {
        pressVisualKey(decodeKey(keyCode));
    }

    protected static final void keyReleased(int keyCode) {
        releaseVisualKey(decodeKey(keyCode));
    }

    protected static final void trackballMoved(int keyCode) {
        if (trackballAvailiable) {
            trackballAvailiable = false;
            pressVisualKey(decodeKey(keyCode));
            trackballKey = keyCode;
        }
    }

    public static final void pressVisualKey(int key) {
        KEY |= key;
    }

    public static final void releaseVisualKey(int key) {
        KEY_R |= key;
    }

    private static final int decodeKey(int keyCode) {
        switch (keyCode) {
            case 4:
                return KEY_BACK;
            case 19:
                return 4;
            case 20:
                return 8;
            case 21:
                return 16;
            case 22:
                return 32;
            case 52:
                return 2;
            case 82:
                return KEY_BACK;
            default:
                return 0;
        }
    }

    public static int DEBUG_GET_DEVICE_KEY_VALUE() {
        return 0;
    }
}
