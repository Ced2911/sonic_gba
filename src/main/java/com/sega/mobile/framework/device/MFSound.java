package com.sega.mobile.framework.device;

import android.media.AudioManager;
import com.sega.mobile.framework.MFMain;
import com.sega.mobile.framework.utility.MFUtility;
import java.util.Vector;

public final class MFSound {
    public static final int LEVEL_MAX = 15;
    public static final int VOLUME_MAX = 100;
    private static MFPlayer bgm;
    private static boolean bgmFlag;
    private static boolean bgmPlaying;
    private static boolean bgmStarted;
    private static boolean deviceInterrupted;
    private static MFPlayer lastBgm;
    private static AudioManager mManager;
    private static MFPlayer nextBgm;
    private static Vector<MFPlayer> prefetchedSeVector;
    private static boolean resumeFlag;
    private static boolean seFlag;
    private static Vector<MFPlayer> seVector;
    static Vector<MFPlayer> soundVector;
    private static int soundVolume;
    private static boolean suspendFlag;

    private static MFPlayer createMFPlayer(String url) {
        MFPlayer ret = MFPlayer.createMFPlayer(url);
        soundVector.addElement(ret);
        return ret;
    }

    private static boolean updateSound(MFPlayer player) {
        switch (player.getState()) {
            case 0:
            case 4:
                player.realize();
                break;
            case 1:
                player.prefetch();
                break;
            case 2:
                player.start();
                return true;
        }
        return false;
    }

    protected static void init() {
        bgmFlag = true;
        deviceInterrupted = false;
        resumeFlag = false;
        suspendFlag = false;
        soundVector = new Vector();
        seFlag = true;
        seVector = new Vector();
        prefetchedSeVector = new Vector();
        soundVolume = VOLUME_MAX;
        mManager = (AudioManager) MFMain.getInstance().getSystemService("audio");
    }

    protected static void tick() {
        int i;
        MFPlayer tmpSe = null;
        for (i = 0; i < soundVector.size(); i++) {
            ((MFPlayer) soundVector.elementAt(i)).tick();
        }
        if (bgmFlag) {
            if (suspendFlag) {
                if (!deviceInterrupted) {
                    deviceInterrupted = true;
                    stopBgm();
                }
                suspendFlag = false;
            }
            if (resumeFlag) {
                if (deviceInterrupted) {
                    deviceInterrupted = false;
                    resumeBgm();
                }
                resumeFlag = false;
            }
            if (nextBgm != null) {
                if (!(bgm == null || nextBgm.soundUrl.equals(bgm.soundUrl))) {
                    bgm.stop();
                    bgm.deallocate();
                    bgm.close();
                }
                bgm = nextBgm;
                nextBgm = null;
                bgmStarted = false;
            }
            if (bgm != null && (bgm.isLoop() || !bgmStarted)) {
                bgmStarted = updateSound(bgm);
            }
            if (bgm != null && !bgm.isLoop() && bgmStarted && bgm.getState() == 2) {
                bgmPlaying = false;
                bgmStarted = false;
                bgm.deallocate();
                bgm.close();
                bgm = null;
            }
        }
        i = 0;
        while (i < prefetchedSeVector.size()) {
            tmpSe = (MFPlayer) prefetchedSeVector.elementAt(i);
            if (tmpSe.getState() == 2) {
                tmpSe.deallocate();
                tmpSe.close();
                prefetchedSeVector.removeElementAt(i);
            } else {
                i++;
            }
        }
        if (seFlag) {
            while (seVector.size() > 0) {
                tmpSe = (MFPlayer) seVector.elementAt(0);
                if (updateSound(tmpSe)) {
                    seVector.removeElementAt(0);
                    prefetchedSeVector.addElement(tmpSe);
                }
            }
            return;
        }
        seVector.removeAllElements();
    }

    protected static void deviceInterrupt() {
        suspendFlag = true;
    }

    protected static void deviceResume() {
        resumeFlag = true;
    }

    public static void setVolume(int volume) {
        soundVolume = MFUtility.getValueInRange(volume, 0, VOLUME_MAX);
        mManager.setStreamVolume(3, (soundVolume * mManager.getStreamMaxVolume(3)) / VOLUME_MAX, 0);
    }

    public static void setLevel(int level) {
        mManager.setStreamVolume(3, level, 0);
    }

    public static int getVolume() {
        soundVolume = MFUtility.getValueInRange((mManager.getStreamVolume(3) * VOLUME_MAX) / mManager.getStreamMaxVolume(3), 0, VOLUME_MAX);
        return soundVolume;
    }

    public static int getLevel() {
        return mManager.getStreamVolume(3);
    }

    public static void setBgmFlag(boolean enable) {
        bgmFlag = enable;
        if (!bgmFlag) {
            stopBgm();
        }
    }

    public static void setSeFlag(boolean enable) {
        seFlag = enable;
        if (!seFlag) {
            seVector.removeAllElements();
        }
    }

    public static boolean getBgmFlag() {
        return bgmFlag;
    }

    public static boolean getSeFlag() {
        return seFlag;
    }

    public static void preloadSound(String url) {
    }

    public static void playBgm(String url, boolean loop) {
        nextBgm = null;
        for (int i = 0; i < soundVector.size(); i++) {
            MFPlayer tmpSe = (MFPlayer) soundVector.elementAt(i);
            if (tmpSe.soundUrl.equals(url)) {
                nextBgm = tmpSe;
                break;
            }
        }
        if (nextBgm == null) {
            nextBgm = createMFPlayer(url);
        }
        nextBgm.setLoop(loop);
        bgmPlaying = true;
        lastBgm = nextBgm;
    }

    public static void stopBgm() {
        if (bgm != null) {
            bgm.stop();
            bgm.deallocate();
            bgm.close();
        }
        bgmPlaying = false;
        bgmStarted = false;
        bgm = null;
        nextBgm = null;
    }

    public static long getBgmMediaTime() {
        if (bgm != null) {
            return bgm.getMediaTime();
        }
        return 0;
    }

    public static final void resumeBgm() {
        if (lastBgm != null && lastBgm.isLoop()) {
            bgmPlaying = true;
            nextBgm = lastBgm;
        }
    }

    public static final boolean isBgmPlaying() {
        return bgmPlaying;
    }

    public static void playSe(String url) {
        playSe(url, 0);
    }

    public static final void playSe(String url, int priority) {
        MFPlayer se = MFPlayer.createMFPlayer(url);
        se.soundPriority = priority;
        seVector.addElement(se);
    }

    public static void releaseAllSound() {
        for (int i = 0; i < soundVector.size(); i++) {
            ((MFPlayer) soundVector.elementAt(i)).close();
        }
        seVector.removeAllElements();
        nextBgm = null;
        bgm = null;
    }

    public static void setBgm(MFPlayer player, boolean loop) {
        nextBgm = player;
        nextBgm.setLoop(loop);
        bgmPlaying = true;
        lastBgm = nextBgm;
    }

    public static MFPlayer getCurrentBgm() {
        if (nextBgm != null) {
            return nextBgm;
        }
        return bgm;
    }
}
