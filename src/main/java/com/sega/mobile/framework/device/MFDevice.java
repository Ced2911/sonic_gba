package com.sega.mobile.framework.device;

import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Vibrator;
import android.telephony.TelephonyManager;
import com.sega.mobile.define.MDPhone;
import com.sega.mobile.framework.MFGameState;
import com.sega.mobile.framework.MFMain;
import com.sega.mobile.framework.android.Canvas;
import com.sega.mobile.framework.android.Graphics;
import com.sega.mobile.framework.android.Image;
import com.sega.mobile.framework.ui.MFTouchKey;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

public final class MFDevice {
    public static final int APN_CMNET = 2;
    public static final int APN_CMWAP = 1;
    public static final int APN_NONE = 0;
    private static final int MAX_LAYER = 1;
    private static final byte[] NULL_RECORD = new byte[4];
    private static final int PER_VIBRATION_TIME = 500;
    private static final String RECORD_NAME = "rms";
    public static final int SIM_CMCC = 1;
    public static final int SIM_NONE = 0;
    public static final int SIM_TELECOM = 3;
    public static final int SIM_UNICOM = 2;
    private static final String VERSION_INFO = "104_RELEASE";
    private static int apnType = -1;
    static int bufferHeight;
    private static Image bufferImage;
    static int bufferWidth;
    static int canvasHeight;
    static int canvasWidth;
    static boolean clearBuffer = true;
    private static Vector<MFComponent> componentVector;
    private static MFGameState currentState;
    private static long currentSystemTime;
    private static int deviceKeyValue = 0;
    private static Rect drawRect;
    private static boolean enableCustomBack = false;
    public static boolean enableTrackBall = true;
    private static boolean enableVolumeKey = true;
    private static boolean exitFlag;
    private static MFGraphics graphics;
    static int horizontalOffset;
    private static boolean inSuspendFlag;
    private static boolean inVibrationFlag;
    private static MFTouchKey interruptConfirm;
    private static boolean interruptPauseFlag;
    private static long lastSystemTime;
    private static boolean logicTrace;
    protected static MyGameCanvas mainCanvas = new MyGameCanvas(MFMain.getInstance());
    public static Thread mainThread;
    private static boolean methodCallTrace;
    private static MFGameState nextState;
    private static MFGraphics[] postLayerGraphics = new MFGraphics[1];
    private static Image[] postLayerImage = new Image[1];
    private static MFGraphics[] preLayerGraphics = new MFGraphics[1];
    private static Image[] preLayerImage = new Image[1];
    public static int preScaleShift = 0;
    public static boolean preScaleZoomInFlag = false;
    public static boolean preScaleZoomOutFlag = false;
    private static Hashtable<String, byte[]> records;
    private static boolean responseInterrupt;
    static int screenHeight;
    static int screenWidth;
    static boolean shieldInput = true;
    private static int simType = -1;
    static int verticvalOffset;
    private static boolean vibraionFlag;
    private static long vibrateStartTime;
    private static int vibrateTime;
    private static Vibrator vibrator;
    private static String webPageUrl;

    protected static Runnable mainRunnable = new Runnable() {

        public final void run() {
            MFDevice.interruptPauseFlag = false;
            MFDevice.responseInterrupt = true;
            MFDevice.exitFlag = false;
            MFDevice.lastSystemTime = System.currentTimeMillis();
            MFDevice.initRecords();
            MFGraphics.init();
            MFSound.init();
            MFSensor.init();
            MFGamePad.resetKeys();
            MFDevice.vibrator = (Vibrator) MFMain.getInstance().getSystemService("vibrator");
            MFDevice.componentVector = new Vector();
            MFDevice.vibraionFlag = true;
            MFDevice.inVibrationFlag = false;
            while (!MFDevice.exitFlag) {
                tick();
                /*
                */
                do {
                    try {
                        Thread.sleep(10);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    MFDevice.currentSystemTime = System.currentTimeMillis();
                } while (MFDevice.currentState != null && MFDevice.currentSystemTime - MFDevice.lastSystemTime > 0 && MFDevice.currentSystemTime - MFDevice.lastSystemTime < MFDevice.currentState.getFrameTime());
                /*
                */
                //MFDevice.lastSystemTime = System.currentTimeMillis();
            }
            MFDevice.currentState.onExit();
            MFMain.getInstance().notifyDestroyed();
        }

        private void tick() {
            if (MFDevice.mainCanvas.initialized()) {
                int i;
                synchronized (MFDevice.mainRunnable) {
                    MFGamePad.keyTick();
                    for (i = 0; i < MFDevice.componentVector.size(); i++) {
                        ((MFComponent) MFDevice.componentVector.elementAt(i)).tick();
                    }
                }
                MFSound.tick();
                if (MFDevice.vibraionFlag) {
                    if (MFDevice.vibrateTime > 0 && System.currentTimeMillis() - MFDevice.vibrateStartTime > ((long) MFDevice.vibrateTime)) {
                        MFDevice.vibrateTime = 0;
                        MFDevice.inVibrationFlag = false;
                    }
                    if (MFDevice.inVibrationFlag) {
                        MFDevice.vibrationImpl(500);
                    }
                }
                if (MFDevice.nextState != null) {
                    if (MFDevice.currentState != null) {
                        MFDevice.currentState.onExit();
                    }
                    MFDevice.currentState = MFDevice.nextState;
                    MFDevice.currentState.onEnter();
                    MFDevice.nextState = null;
                }
                MFDevice.graphics.reset();
                if (MFDevice.clearBuffer) {
                    MFDevice.clearScreen();
                }
                if (MFDevice.interruptPauseFlag) {
                    if (!MFDevice.inSuspendFlag && MFMain.getInstance().logicDeviceSuspend()) {
                        MFDevice.notifyResume();
                    }
                    MFMain.getInstance().drawDeviceSuspend(MFDevice.graphics);
                } else {
                    MFDevice.currentState.onTick();
                    if (!MFDevice.exitFlag) {
                        for (i = 1; i > 0; i--) {
                            if (MFDevice.preLayerGraphics[i - 1] != null) {
                                MFDevice.currentState.onRender(MFDevice.preLayerGraphics[i - 1], -i);
                            }
                        }
                        MFDevice.currentState.onRender(MFDevice.graphics);
                        for (i = 1; i <= 1; i++) {
                            if (MFDevice.postLayerGraphics[i - 1] != null) {
                                MFDevice.currentState.onRender(MFDevice.postLayerGraphics[i - 1], i);
                            }
                        }
                    }
                }
                MFDevice.mainCanvas.repaint();
            }
        }
    };

    static class MyGameCanvas extends Canvas {
        protected MyGameCanvas(Context context) {
            super(context);
        }

        public Graphics getGraphics() {
            return this.mGraphics;
        }

        public final void hideNotify() {
            MFDevice.inSuspendFlag = true;
            MFDevice.notifyPause();
        }

        public final void keyPressed(int keyCode) {
            synchronized (MFDevice.mainRunnable) {
                MFGamePad.keyPressed(keyCode);
            }
        }

        public final void keyReleased(int keyCode) {
            synchronized (MFDevice.mainRunnable) {
                MFGamePad.keyReleased(keyCode);
            }
        }

        public final void paint(Graphics g) {
            synchronized (MFDevice.mainRunnable) {
                int i;
                for (i = 1; i > 0; i--) {
                    if (MFDevice.preLayerImage[i - 1] != null) {
                        g.drawImage(MFDevice.preLayerImage[i - 1], 0, 0, 0);
                    }
                }
                if (MFDevice.bufferImage != null) {
                    g.drawScreen(MFDevice.bufferImage, null, new Rect(0, 0, MFDevice.screenWidth, MFDevice.screenHeight));
                }
                for (i = 1; i <= 1; i++) {
                    if (MFDevice.postLayerImage[i - 1] != null) {
                        g.drawImage(MFDevice.postLayerImage[i - 1], 0, 0, 0);
                    }
                }
            }
        }

        public final void pointerDragged(int id, int x, int y) {
            if (MFDevice.preScaleZoomOutFlag) {
                x <<= MFDevice.preScaleShift;
                y <<= MFDevice.preScaleShift;
            } else if (MFDevice.preScaleZoomInFlag) {
                x >>= MFDevice.preScaleShift;
                y >>= MFDevice.preScaleShift;
            }
            x = ((x - MFDevice.drawRect.left) * MFDevice.canvasWidth) / MFDevice.drawRect.width();
            y = ((y - MFDevice.drawRect.top) * MFDevice.canvasHeight) / MFDevice.drawRect.height();
            if (MFDevice.componentVector != null) {
                for (int i = 0; i < MFDevice.componentVector.size(); i++) {
                    ((MFComponent) MFDevice.componentVector.elementAt(i)).pointerDragged(id, x, y);
                }
            }
        }

        public final void pointerPressed(int id, int x, int y) {
            if (MFDevice.preScaleZoomOutFlag) {
                x <<= MFDevice.preScaleShift;
                y <<= MFDevice.preScaleShift;
            } else if (MFDevice.preScaleZoomInFlag) {
                x >>= MFDevice.preScaleShift;
                y >>= MFDevice.preScaleShift;
            }
            x = ((x - MFDevice.drawRect.left) * MFDevice.canvasWidth) / MFDevice.drawRect.width();
            y = ((y - MFDevice.drawRect.top) * MFDevice.canvasHeight) / MFDevice.drawRect.height();
            if (MFDevice.componentVector != null) {
                for (int i = 0; i < MFDevice.componentVector.size(); i++) {
                    ((MFComponent) MFDevice.componentVector.elementAt(i)).pointerPressed(id, x, y);
                }
            }
        }

        public final void pointerReleased(int id, int x, int y) {
            if (MFDevice.preScaleZoomOutFlag) {
                x <<= MFDevice.preScaleShift;
                y <<= MFDevice.preScaleShift;
            } else if (MFDevice.preScaleZoomInFlag) {
                x >>= MFDevice.preScaleShift;
                y >>= MFDevice.preScaleShift;
            }
            x = ((x - MFDevice.drawRect.left) * MFDevice.canvasWidth) / MFDevice.drawRect.width();
            y = ((y - MFDevice.drawRect.top) * MFDevice.canvasHeight) / MFDevice.drawRect.height();
            if (MFDevice.componentVector != null) {
                for (int i = 0; i < MFDevice.componentVector.size(); i++) {
                    ((MFComponent) MFDevice.componentVector.elementAt(i)).pointerReleased(id, x, y);
                }
            }
        }

        public final void showNotify() {
            MFDevice.inSuspendFlag = false;
        }

        public final void trackballMoved(int keyCode) {
            synchronized (MFDevice.mainRunnable) {
                MFGamePad.trackballMoved(keyCode);
            }
        }
    }

    public static final void addComponent(MFComponent component) {
        if (componentVector != null && !componentVector.contains(component)) {
            component.reset();
            componentVector.addElement(component);
        }
    }

    public static void changeState(MFGameState gameState) {
        nextState = gameState;
    }

    public static final void clearScreen() {
        int i;
        for (i = 1; i > 0; i--) {
            if (preLayerImage[i - 1] != null) {
                preLayerImage[i - 1].earseColor(0);
            }
        }
        if (bufferImage != null) {
            bufferImage.earseColor(0);
        }
        for (i = 1; i <= 1; i++) {
            if (postLayerImage[i - 1] != null) {
                postLayerImage[i - 1].earseColor(0);
            }
        }
    }

    public static long currentTimeMillis() {
        return System.currentTimeMillis();
    }

    public static final void DEBUG_ENABLE_LOGIC_TRACE(boolean enable) {
    }

    public static final void DEBUG_ENABLE_METHOD_TRACE(boolean enable) {
    }

    public static int DEBUG_GET_DEVICE_KEY_VALUE() {
        return 0;
    }

    public static final void DEBUG_PAINT_LOGIC_TRACE(String traceLog) {
    }

    public static final void DEBUG_PAINT_MESSAGE(String message) {
    }

    public static final void DEBUG_PAINT_METHOD_TRACE(String methodInfo) {
    }

    public static final void DEBUG_SHOW_ERROR(Throwable t) {
    }

    public static final void deleteRecord(String recordName) {
        records.remove(recordName);
        updateRecords();
    }

    public static void disableExceedBoundary() {
        graphics.disableExceedBoundary();
    }

    public static void enableExceedBoundary() {
        graphics.enableExceedBoundary();
    }

    public static int getApnType() {
        apnType = 0;
        Cursor cr = MFMain.getInstance().getContentResolver().query(Uri.parse("content://telephony/carriers/preferapn"), null, null, null, null);
        String pxy = "";
        while (cr != null && cr.moveToNext()) {
            pxy = cr.getString(cr.getColumnIndex("proxy"));
        }
        if (!(pxy == null || pxy.equals(""))) {
            apnType = 1;
        }
        return apnType;
    }

    public static final int getDeviceHeight() {
        return screenHeight;
    }

    public static final int getDeviceWidth() {
        return screenWidth;
    }

    public static boolean getEnableCustomBack() {
        return enableCustomBack;
    }

    public static boolean getEnableTrackBall() {
        return enableTrackBall;
    }

    public static boolean getEnableVolumeKey() {
        return enableVolumeKey;
    }

    public static MFGraphics getGraphics() {
        return graphics;
    }

    public static Thread getMainThread() {
        return mainThread;
    }

    public static int getNativeCanvasBottom() {
        if (preScaleZoomOutFlag) {
            return (bufferHeight - verticvalOffset) << preScaleShift;
        }
        if (preScaleZoomInFlag) {
            return (bufferHeight - verticvalOffset) >> preScaleShift;
        }
        return bufferHeight + verticvalOffset;
    }

    public static int getNativeCanvasHeight() {
        if (preScaleZoomOutFlag) {
            return bufferHeight << preScaleShift;
        }
        if (preScaleZoomInFlag) {
            return bufferHeight >> preScaleShift;
        }
        return bufferHeight;
    }

    public static int getNativeCanvasLeft() {
        if (preScaleZoomOutFlag) {
            return -(horizontalOffset << preScaleShift);
        }
        if (preScaleZoomInFlag) {
            return -(horizontalOffset >> preScaleShift);
        }
        return -horizontalOffset;
    }

    public static int getNativeCanvasRight() {
        if (preScaleZoomOutFlag) {
            return (bufferWidth - horizontalOffset) << preScaleShift;
        }
        if (preScaleZoomInFlag) {
            return (bufferWidth - horizontalOffset) >> preScaleShift;
        }
        return bufferWidth + horizontalOffset;
    }

    public static int getNativeCanvasTop() {
        if (preScaleZoomOutFlag) {
            return -(verticvalOffset << preScaleShift);
        }
        if (preScaleZoomInFlag) {
            return -(verticvalOffset >> preScaleShift);
        }
        return -verticvalOffset;
    }

    public static int getNativeCanvasWidth() {
        if (preScaleZoomOutFlag) {
            return bufferWidth << preScaleShift;
        }
        if (preScaleZoomInFlag) {
            return bufferWidth >> preScaleShift;
        }
        return bufferWidth;
    }

    public static String getPackageVersion() {
        String version = "";
        try {
            return MFMain.getInstance().getPackageManager().getPackageInfo(MFMain.getInstance().getPackageName(), 0).versionName;
        } catch (NameNotFoundException e) {
            e.printStackTrace();
            return version;
        }
    }

    public static final MFInputStream getResourceAsMFStream(String url) {
        try {
            String substring;
            AssetManager assets = MFMain.getInstance().getAssets();
            if (url.startsWith("/")) {
                substring = url.substring(1);
            } else {
                substring = url;
            }
            return new MFInputStream(assets.open(substring));
        } catch (Exception e) {
            System.out.println("Error on loading: " + url);
            return null;
        }
    }

    public static final InputStream getResourceAsStream(String url) {
        InputStream ret = null;
        try {
            String substring;
            AssetManager assets = MFMain.getInstance().getAssets();
            if (url.startsWith("/")) {
                substring = url.substring(1);
            } else {
                substring = url;
            }
            ret = assets.open(substring);
        } catch (Exception e) {
            System.out.println("Error on loading: " + url);
        }
        return ret;
    }

    public static final int getScreenHeight() {
        if (preScaleZoomOutFlag) {
            return canvasHeight << preScaleShift;
        }
        if (preScaleZoomInFlag) {
            return canvasHeight >> preScaleShift;
        }
        return canvasHeight;
    }

    public static final int getScreenWidth() {
        if (preScaleZoomOutFlag) {
            return canvasWidth << preScaleShift;
        }
        if (preScaleZoomInFlag) {
            return canvasWidth >> preScaleShift;
        }
        return canvasWidth;
    }

    public static int getSimType() {
        if (simType < 0) {
            simType = 0;
            String operator = ((TelephonyManager) MFMain.getInstance().getSystemService("phone")).getSimOperator();
            if (operator.equals("46000") || operator.equals("46002")) {
                simType = 1;
            } else if (operator.equals("46001")) {
                simType = 2;
            } else if (operator.equals("46003")) {
                simType = 3;
            }
        }
        return simType;
    }

    public static final Object getSystemDisplayable() {
        return mainCanvas;
    }

    public static final String getVersion() {
        return VERSION_INFO;
    }

    private static final void initRecords() {
        Exception e;
        DataInputStream dis = null;
        ByteArrayInputStream bis = null;
        if (records == null) {
            records = new Hashtable();
            try {
                ByteArrayInputStream bis2 = new ByteArrayInputStream(openRecordStore(RECORD_NAME));
                try {
                    DataInputStream dis2 = new DataInputStream(bis2);
                    try {
                        int recordStoreNumber = dis2.readInt();
                        for (int i = 0; i < recordStoreNumber; i++) {
                            String name = dis2.readUTF();
                            int offset = 0;
                            int dataSize = dis2.readInt();
                            byte[] data = new byte[dataSize];
                            do {
                                offset = dis2.read(data, offset, dataSize - offset);
                            } while (offset < dataSize);
                            records.put(name, data);
                        }
                        bis = bis2;
                        dis = dis2;
                    } catch (Exception e2) {
                        e = e2;
                        bis = bis2;
                        dis = dis2;
                    }
                } catch (Exception e22) {
                    e = e22;
                    bis = bis2;
                    e.printStackTrace();
                    dis.close();
                    bis.close();
                }
            } catch (Exception e222) {
                e = e222;
                e.printStackTrace();
                //dis.close();
                //bis.close();
            }
            try {
                dis.close();
            } catch (Exception e3) {
            }
            try {
                bis.close();
            } catch (Exception e4) {
            }
        }
    }

    public static final byte[] loadRecord(String recordName) {
        return (byte[]) records.get(recordName);
    }

    public static final void notifyExit() {
        exitFlag = true;
    }

    public static final void notifyPause() {
        synchronized (mainRunnable) {
            if (responseInterrupt && !interruptPauseFlag) {
                stopVibrate();
                MFGamePad.resetKeys();
                MFSound.deviceInterrupt();
                if (currentState != null) {
                    currentState.onPause();
                }
                interruptPauseFlag = true;
                if (componentVector != null) {
                    for (int i = 0; i < componentVector.size(); i++) {
                        ((MFComponent) componentVector.elementAt(i)).reset();
                    }
                    interruptConfirm = new MFTouchKey(0, canvasHeight - 50, 100, 50, 2112);
                    addComponent(interruptConfirm);
                }
            }
        }
    }

    public static final void notifyResume() {
        synchronized (mainRunnable) {
            if (responseInterrupt && interruptPauseFlag) {
                MFGamePad.resetKeys();
                MFSound.deviceResume();
                if (currentState != null) {
                    currentState.onResume();
                }
                interruptPauseFlag = false;
                if (componentVector != null) {
                    for (int i = 0; i < componentVector.size(); i++) {
                        ((MFComponent) componentVector.elementAt(i)).reset();
                    }
                    removeComponent(interruptConfirm);
                }
            }
        }
    }

    public static final void notifyStart(int width, int height) {
        if (mainThread == null) {
            if (MFMain.getInstance().getRequestedOrientation() == 1) {
                canvasHeight = MDPhone.SCREEN_HEIGHT;
                canvasWidth = MDPhone.SCREEN_WIDTH;
                if (width > height) {
                    screenWidth = height;
                    screenHeight = width;
                } else {
                    screenWidth = width;
                    screenHeight = height;
                }
            } else {
                canvasHeight = MDPhone.SCREEN_WIDTH;
                canvasWidth = MDPhone.SCREEN_HEIGHT;
                if (width < height) {
                    screenWidth = height;
                    screenHeight = width;
                } else {
                    screenWidth = width;
                    screenHeight = height;
                }
            }
            System.out.println("screenwidth:" + screenWidth + ",screenheight:" + screenHeight);
            changeState(MFMain.getInstance().getEntryGameState());
            setFullscreenMode(false);
            startThread();
        }
    }

    public static final void notifyVolumeChange(boolean isUp) {
        if (isUp) {
            currentState.onVolumeUp();
        } else {
            currentState.onVolumeDown();
        }
    }

    private static byte[] openRecordStore(String str) {
        FileNotFoundException fileNotFoundException;
        DataOutputStream dos;
        IOException iOException;
        byte[] ret = null;
        try {
            DataInputStream dis = new DataInputStream(MFMain.getInstance().openFileInput(new StringBuilder(String.valueOf(str)).append(".rms").toString()));
            try {
                int t = dis.readInt();
                if (t == 0) {
                    ret = NULL_RECORD;
                } else {
                    ret = new byte[t];
                    for (int i = 0; i < ret.length; i++) {
                        ret[i] = dis.readByte();
                    }
                }
                dis.close();
                DataInputStream dataInputStream = dis;
            } catch (FileNotFoundException e) {
                fileNotFoundException = e;
                //dataInputStream = dis;
                try {
                    System.out.println("Create new save file.");
                    ret = NULL_RECORD;
                    dos = new DataOutputStream(MFMain.getInstance().openFileOutput(new StringBuilder(String.valueOf(str)).append(".rms").toString(), 0));
                    dos.write(NULL_RECORD, 0, NULL_RECORD.length);
                    dos.flush();
                    dos.close();
                } catch (IOException e2) {
                    IOException ex = e2;
                    System.out.println("RMS ERROR : Can't create rms file.");
                }
                return ret;
            } catch (IOException e22) {
                iOException = e22;
                //dataInputStream = dis;
                System.out.println("RMS ERROR : Can't read rms file.");
                return ret;
            }
        } catch (FileNotFoundException e3) {
            fileNotFoundException = e3;
            System.out.println("Create new save file.");
            ret = NULL_RECORD;
            /*
            dos = new DataOutputStream(MFMain.getInstance().openFileOutput(new StringBuilder(String.valueOf(str)).append(".rms").toString(), 0));
            dos.write(NULL_RECORD, 0, NULL_RECORD.length);
            dos.flush();
            dos.close();
            */

            System.out.println("FileNotFoundException e3.");
            return ret;
        }
        /*catch (IOException e222) {
            iOException = e222;
            System.out.println("RMS ERROR : Can't read rms file.");
            return ret;
        }
        */
        return ret;
    }

    public static final void openUrl() {
        if (webPageUrl != null) {
            try {
                MFMain.getInstance().platformRequest(webPageUrl);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static final void openUrl(String url, boolean needClose) {
        if (needClose) {
            webPageUrl = url;
            notifyExit();
            return;
        }
        try {
            MFMain.getInstance().platformRequest(url);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static final void removeAllComponents() {
        if (componentVector != null) {
            componentVector.removeAllElements();
        }
    }

    public static final void removeComponent(MFComponent component) {
        if (componentVector != null) {
            componentVector.removeElement(component);
        }
    }

    public static final void saveRecord(String recordName, byte[] record) {
        records.put(recordName, record);
        updateRecords();
    }

    public static void setAntiAlias(boolean b) {
        mainCanvas.setAntiAlias(b);
    }

    public static void setCanvasSize(int w, int h) {
        canvasWidth = w;
        canvasHeight = h;
    }

    public static void setClearBuffer(boolean b) {
        clearBuffer = b;
    }

    public static void setEnableCustomBack(boolean b) {
        enableCustomBack = b;
    }

    public static void setEnableTrackBall(boolean b) {
        enableTrackBall = b;
    }

    public static void setEnableVolumeKey(boolean b) {
        enableVolumeKey = b;
    }

    public static void enableLayer(int layer) {
        if (layer <= 0 || layer > 1) {
            if (layer < 0 && layer >= -1 && preLayerImage[(-layer) - 1] == null) {
                preLayerImage[(-layer) - 1] = Image.createImage(screenWidth, screenHeight);
                preLayerGraphics[(-layer) - 1] = MFGraphics.createMFGraphics(preLayerImage[(-layer) - 1].getGraphics(), screenWidth, screenHeight);
            }
        } else if (postLayerImage[layer - 1] == null) {
            postLayerImage[layer - 1] = Image.createImage(screenWidth, screenHeight);
            postLayerGraphics[layer - 1] = MFGraphics.createMFGraphics(postLayerImage[layer - 1].getGraphics(), screenWidth, screenHeight);
        }
    }

    public static void disableLayer(int layer) {
        if (layer > 0 && layer <= 1) {
            postLayerImage[layer - 1] = null;
            postLayerGraphics[layer - 1] = null;
        } else if (layer < 0 && layer >= -1) {
            preLayerImage[(-layer) - 1] = null;
            preLayerGraphics[(-layer) - 1] = null;
        }
    }

    public static void setFilterBitmap(boolean b) {
        mainCanvas.setFilterBitmap(b);
    }

    public static void setFullscreenMode(boolean b) {
        if (b) {
            drawRect = new Rect(0, 0, screenWidth, screenHeight);
        } else if (((float) screenWidth) / ((float) canvasWidth) > ((float) screenHeight) / ((float) canvasHeight)) {
            int tmpHeight;
            if (preScaleZoomOutFlag && canvasHeight > screenHeight) {
                preScaleShift = 0;
                tmpHeight = canvasHeight;
                while (tmpHeight > screenHeight && tmpHeight - screenHeight > screenHeight - (tmpHeight / 2)) {
                    tmpHeight /= 2;
                    canvasWidth /= 2;
                    canvasHeight /= 2;
                    preScaleShift++;
                }
                if (preScaleShift == 0) {
                    preScaleZoomOutFlag = false;
                }
                preScaleZoomInFlag = false;
            }
            if (preScaleZoomInFlag && canvasHeight < screenHeight) {
                preScaleShift = 0;
                tmpHeight = canvasHeight;
                while (tmpHeight < screenHeight && screenHeight - tmpHeight > (tmpHeight * 2) - screenHeight) {
                    tmpHeight *= 2;
                    canvasWidth *= 2;
                    canvasHeight *= 2;
                    preScaleShift++;
                }
                if (preScaleShift == 0) {
                    preScaleZoomInFlag = false;
                }
                preScaleZoomOutFlag = false;
            }
            int h = screenHeight;
            int w = (canvasWidth * h) / canvasHeight;
            int x = (screenWidth - w) / 2;
            drawRect = new Rect(x, 0, x + w, 0 + h);
            if (preScaleZoomOutFlag) {
                bufferImage = Image.createImage(((canvasHeight * screenWidth) / screenHeight) << preScaleShift, canvasHeight << preScaleShift);
            } else if (preScaleZoomInFlag) {
                bufferImage = Image.createImage(((canvasHeight * screenWidth) / screenHeight) >> preScaleShift, canvasHeight >> preScaleShift);
            } else {
                bufferImage = Image.createImage((canvasHeight * screenWidth) / screenHeight, canvasHeight);
            }
            graphics = MFGraphics.createMFGraphics(bufferImage.getGraphics(), (canvasHeight * screenWidth) / screenHeight, canvasHeight);
        } else {
            int tmpWidth;
            if (preScaleZoomOutFlag && canvasWidth > screenWidth) {
                preScaleShift = 0;
                tmpWidth = canvasWidth;
                while (tmpWidth > screenWidth && ((float) tmpWidth) - ((float) screenWidth) > ((float) (screenWidth - (tmpWidth / 2)))) {
                    tmpWidth /= 2;
                    canvasWidth /= 2;
                    canvasHeight /= 2;
                    preScaleShift++;
                }
                if (preScaleShift == 0) {
                    preScaleZoomOutFlag = false;
                }
                preScaleZoomInFlag = false;
            }
            if (preScaleZoomInFlag && canvasWidth < screenWidth) {
                preScaleShift = 0;
                tmpWidth = canvasWidth;
                while (tmpWidth < screenWidth && screenWidth - tmpWidth < (tmpWidth * 2) - screenWidth) {
                    tmpWidth *= 2;
                    canvasWidth *= 2;
                    canvasHeight *= 2;
                    preScaleShift++;
                }
                if (preScaleShift == 0) {
                    preScaleZoomInFlag = false;
                }
                preScaleZoomOutFlag = false;
            }
            int w = screenWidth;
            int h = (canvasHeight * w) / canvasWidth;
            int y = (screenHeight - h) / 2;
            drawRect = new Rect(0, y, 0 + w, y + h);
            if (preScaleZoomOutFlag) {
                bufferImage = Image.createImage(canvasWidth << preScaleShift, ((canvasWidth * screenHeight) / screenWidth) << preScaleShift);
            } else if (preScaleZoomInFlag) {
                bufferImage = Image.createImage(canvasWidth >> preScaleShift, ((canvasWidth * screenHeight) / screenWidth) >> preScaleShift);
            } else {
                bufferImage = Image.createImage(canvasWidth, (canvasWidth * screenHeight) / screenWidth);
            }
            graphics = MFGraphics.createMFGraphics(bufferImage.getGraphics(), canvasWidth, (canvasWidth * screenHeight) / screenWidth);
        }
        bufferWidth = bufferImage.getWidth() >> preScaleShift;
        bufferHeight = bufferImage.getHeight() >> preScaleShift;
        horizontalOffset = (drawRect.left * bufferWidth) / screenWidth;
        verticvalOffset = (drawRect.top * bufferHeight) / screenHeight;
        graphics.f7g.getCanvas().translate((float) horizontalOffset, (float) verticvalOffset);
    }

    public static void setPreScale(boolean zoomIn, boolean zoomOut) {
        preScaleZoomInFlag = zoomIn;
        preScaleZoomOutFlag = zoomOut;
    }

    private static void setRecord(String str, byte[] data, int len) {
        try {
            DataOutputStream dos = new DataOutputStream(MFMain.getInstance().openFileOutput(new StringBuilder(String.valueOf(str)).append(".rms").toString(), 0));
            dos.writeInt(len);
            for (int i = 0; i < len; i++) {
                dos.writeByte(data[i]);
            }
            dos.flush();
            dos.close();
        } catch (Exception e) {
            System.out.println("RMS ERROR : Can't save rms file.");
        }
    }

    public static final void setResponseInterruptFlag(boolean flag) {
        responseInterrupt = flag;
    }

    public static final void setShieldInput(boolean b) {
        shieldInput = b;
    }

    public static void setUseMultitouch(boolean b) {
        mainCanvas.setUseMultitouch(b);
    }

    public static final void setVibrationFlag(boolean enable) {
        vibraionFlag = enable;
        if (!vibraionFlag) {
            stopVibrate();
        }
    }

    public static final void startThread() {
        if (mainThread == null) {
            mainThread = new Thread(mainRunnable);
            mainThread.start();
        }
    }

    public static final void startVibrate() {
        if (vibraionFlag) {
            inVibrationFlag = true;
            ((Vibrator) MFMain.getInstance().getSystemService("vibrator")).vibrate(10000);
        }
    }

    public static final void stopVibrate() {
        if (inVibrationFlag) {
            inVibrationFlag = false;
            ((Vibrator) MFMain.getInstance().getSystemService("vibrator")).cancel();
        }
    }

    private static final void updateRecords() {
        Exception e;
        DataOutputStream out = null;
        ByteArrayOutputStream bos = null;
        try {
            ByteArrayOutputStream bos2 = new ByteArrayOutputStream();
            try {
                DataOutputStream out2 = new DataOutputStream(bos2);
                try {
                    byte[] tmp;
                    out2.writeInt(records.size());
                    Enumeration<String> e2 = records.keys();
                    for (int i = 0; i < records.size(); i++) {
                        String name = (String) e2.nextElement();
                        tmp = (byte[]) records.get(name);
                        out2.writeUTF(name);
                        out2.writeInt(tmp.length);
                        out2.write(tmp);
                    }
                    out2.flush();
                    tmp = bos2.toByteArray();
                    setRecord(RECORD_NAME, tmp, tmp.length);
                    bos = bos2;
                    out = out2;
                } catch (Exception e3) {
                    e = e3;
                    bos = bos2;
                    out = out2;
                }
            } catch (Exception e32) {
                e = e32;
                bos = bos2;
                e.printStackTrace();
                out.close();
                bos.close();
            }
        } catch (Exception e322) {
            e = e322;
            e.printStackTrace();
            //out.close();
            //bos.close();
        }
        try {
            out.close();
        } catch (Exception e4) {
        }
        try {
            bos.close();
        } catch (Exception e5) {
        }
    }

    public static final void vibrateByTime(int time) {
        if (vibraionFlag) {
            vibrationImpl(time);
        }
    }

    private static final void vibrationImpl(int time) {
        vibrator.vibrate((long) time);
    }
}
