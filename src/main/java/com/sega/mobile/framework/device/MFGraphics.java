package com.sega.mobile.framework.device;

import SonicGBA.MapManager;
import com.sega.mobile.framework.android.Font;
import com.sega.mobile.framework.android.Graphics;
import com.sega.mobile.framework.utility.MFUtility;

public class MFGraphics {
    public static final int BOTTOM = 32;
    public static final int FONT_LARGE = -3;
    public static final int FONT_MEDIUM = -2;
    public static final int FONT_SMALL = -1;
    public static final int HCENTER = 1;
    public static final int LEFT = 4;
    public static final int RIGHT = 8;
    public static final int TOP = 16;
    public static final int TRANS_MIRROR = 2;
    public static final int TRANS_MIRROR_ROT180 = 1;
    public static final int TRANS_MIRROR_ROT270 = 4;
    public static final int TRANS_MIRROR_ROT90 = 7;
    public static final int TRANS_NONE = 0;
    public static final int TRANS_ROT180 = 3;
    public static final int TRANS_ROT270 = 6;
    public static final int TRANS_ROT90 = 5;
    public static final int VCENTER = 2;
    private static Font currentFont;
    private static int drawLineD;
    private static int drawLineDX;
    private static int drawLineDY;
    private static int drawLineIncrE;
    private static int drawLineIncrNE;
    private static boolean drawLineMFlag;
    private static int drawLineStepX;
    private static int drawLineStepY;
    private static int drawLineX;
    private static int drawLineY;
    private static Font font_large;
    private static Font font_medium;
    private static Font font_small;
    private static int font_type;
    private static int xOff = 0;
    private static int yOff = 0;
    private int alphaValue;
    private int blueValue;
    private int clipHeight;
    private int clipWidth;
    private int clipX;
    private int clipY;
    private int[] drawEffectRGB;
    private boolean effectFlag;
    private boolean enableExceed = false;
    int[] fillRectRGB = new int[100];
    protected Graphics f7g;
    private int grayValue;
    private int greenValue;
    private int[] pixelInt;
    private int redValue;
    private int transX;
    private int transY;

    private MFGraphics() {
    }

    protected static final void init() {
        if (font_small == null) {
            font_small = Font.getFont(0, 0, 8);
            font_medium = Font.getFont(0, 0, 0);
            font_large = Font.getFont(0, 0, 16);
            font_type = 8;
            currentFont = font_small;
        }
    }

    public void saveCanvas() {
        this.f7g.save();
    }

    public void restoreCanvas() {
        this.f7g.restore();
    }

    public void clipCanvas(int left, int top, int right, int bottom) {
        if (MFDevice.preScaleZoomOutFlag) {
            left >>= MFDevice.preScaleShift;
            top >>= MFDevice.preScaleShift;
            right >>= MFDevice.preScaleShift;
            bottom >>= MFDevice.preScaleShift;
        } else if (MFDevice.preScaleZoomInFlag) {
            left <<= MFDevice.preScaleShift;
            top <<= MFDevice.preScaleShift;
            right <<= MFDevice.preScaleShift;
            bottom <<= MFDevice.preScaleShift;
        }
        this.f7g.clipRect(left, top, right, bottom);
    }

    public void rotateCanvas(float degrees) {
        this.f7g.rotate(degrees);
    }

    public void rotateCanvas(float degrees, int px, int py) {
        if (MFDevice.preScaleZoomOutFlag) {
            px >>= MFDevice.preScaleShift;
            py >>= MFDevice.preScaleShift;
        } else if (MFDevice.preScaleZoomInFlag) {
            px <<= MFDevice.preScaleShift;
            py <<= MFDevice.preScaleShift;
        }
        this.f7g.rotate(degrees, (float) px, (float) py);
    }

    public void scaleCanvas(float sx, float sy) {
        this.f7g.scale(sx, sy);
    }

    public void scaleCanvas(float sx, float sy, int px, int py) {
        if (MFDevice.preScaleZoomOutFlag) {
            px >>= MFDevice.preScaleShift;
            py >>= MFDevice.preScaleShift;
        } else if (MFDevice.preScaleZoomInFlag) {
            px <<= MFDevice.preScaleShift;
            py <<= MFDevice.preScaleShift;
        }
        this.f7g.scale(sx, sy, (float) px, (float) py);
    }

    public void translateCanvas(int dx, int dy) {
        if (MFDevice.preScaleZoomOutFlag) {
            dx >>= MFDevice.preScaleShift;
            dy >>= MFDevice.preScaleShift;
        } else if (MFDevice.preScaleZoomInFlag) {
            dx <<= MFDevice.preScaleShift;
            dy <<= MFDevice.preScaleShift;
        }
        this.f7g.translate((float) dx, (float) dy);
    }

    public static final MFGraphics createMFGraphics(Object graphics, int width, int height) {
        if (MFDevice.preScaleZoomOutFlag) {
            width >>= MFDevice.preScaleShift;
            height >>= MFDevice.preScaleShift;
        } else if (MFDevice.preScaleZoomInFlag) {
            width <<= MFDevice.preScaleShift;
            height <<= MFDevice.preScaleShift;
        }
        System.out.println(new StringBuilder(String.valueOf(width)).append(" ").append(height).toString());
        MFGraphics ret = new MFGraphics();
        ret.setGraphics(graphics, width, height);
        ret.disableEffect();
        return ret;
    }

    public final Object getSystemGraphics() {
        return this.f7g;
    }

    public final void setGraphics(Object graphics) {
        this.f7g = (Graphics) graphics;
        this.f7g.setFont(Font.getFont(0, 0, 8));
    }

    public final void setGraphics(Object graphics, int width, int height) {
        if (MFDevice.preScaleZoomOutFlag) {
            width >>= MFDevice.preScaleShift;
            height >>= MFDevice.preScaleShift;
        } else if (MFDevice.preScaleZoomInFlag) {
            width <<= MFDevice.preScaleShift;
            height <<= MFDevice.preScaleShift;
        }
        reset();
        setGraphics(graphics);
    }

    public final void reset() {
        int screenWidth = MFDevice.getScreenWidth();
        int screenHeight = MFDevice.getScreenHeight();
        if (MFDevice.preScaleZoomOutFlag) {
            screenWidth >>= MFDevice.preScaleShift;
            screenHeight >>= MFDevice.preScaleShift;
        } else if (MFDevice.preScaleZoomInFlag) {
            screenWidth <<= MFDevice.preScaleShift;
            screenHeight <<= MFDevice.preScaleShift;
        }
        this.transX = 0;
        this.transY = 0;
        this.clipX = 0;
        this.clipY = 0;
        this.clipWidth = screenWidth;
        this.clipHeight = screenHeight;
    }

    public final void translate(int x, int y) {
        if (MFDevice.preScaleZoomOutFlag) {
            x >>= MFDevice.preScaleShift;
            y >>= MFDevice.preScaleShift;
        } else if (MFDevice.preScaleZoomInFlag) {
            x <<= MFDevice.preScaleShift;
            y <<= MFDevice.preScaleShift;
        }
        this.transX += x;
        this.transY += y;
        this.clipX += x;
        this.clipY += y;
    }

    public final void setTranslate(int x, int y) {
        if (MFDevice.preScaleZoomOutFlag) {
            x >>= MFDevice.preScaleShift;
            y >>= MFDevice.preScaleShift;
        } else if (MFDevice.preScaleZoomInFlag) {
            x <<= MFDevice.preScaleShift;
            y <<= MFDevice.preScaleShift;
        }
        translate(x - this.transX, y - this.transY);
    }

    public final int getTranslateX() {
        if (MFDevice.preScaleZoomOutFlag) {
            return this.transX << MFDevice.preScaleShift;
        }
        if (MFDevice.preScaleZoomInFlag) {
            return this.transX >> MFDevice.preScaleShift;
        }
        return this.transX;
    }

    public final int getTranslateY() {
        if (MFDevice.preScaleZoomOutFlag) {
            return this.transY << MFDevice.preScaleShift;
        }
        if (MFDevice.preScaleZoomInFlag) {
            return this.transY >> MFDevice.preScaleShift;
        }
        return this.transY;
    }

    public final void setClip(int x, int y, int width, int height) {
        int screenWidth = MFDevice.getScreenWidth();
        int screenHeight = MFDevice.getScreenHeight();
        if (MFDevice.preScaleZoomOutFlag) {
            x >>= MFDevice.preScaleShift;
            y >>= MFDevice.preScaleShift;
            width >>= MFDevice.preScaleShift;
            height >>= MFDevice.preScaleShift;
            screenWidth >>= MFDevice.preScaleShift;
            screenHeight >>= MFDevice.preScaleShift;
        } else if (MFDevice.preScaleZoomInFlag) {
            x <<= MFDevice.preScaleShift;
            y <<= MFDevice.preScaleShift;
            width <<= MFDevice.preScaleShift;
            height <<= MFDevice.preScaleShift;
            screenWidth <<= MFDevice.preScaleShift;
            screenHeight <<= MFDevice.preScaleShift;
        }
        this.clipX = this.transX + x;
        this.clipY = this.transY + y;
        this.clipWidth = width;
        this.clipHeight = height;
        if (this.enableExceed) {
            this.f7g.setClip(this.clipX, this.clipY, this.clipWidth, this.clipHeight);
            return;
        }
        int tx;
        int ty;
        int cx = this.clipX < 0 ? 0 : this.clipX;
        int cy = this.clipY < 0 ? 0 : this.clipY;
        if (this.clipX + width > screenWidth) {
            tx = screenWidth;
        } else {
            tx = this.clipX + width;
        }
        if (this.clipY + height > screenHeight) {
            ty = screenHeight;
        } else {
            ty = this.clipY + height;
        }
        this.f7g.setClip(cx, cy, tx - cx, ty - cy);
    }

    public final int getClipX() {
        if (MFDevice.preScaleZoomOutFlag) {
            return (this.clipX - this.transX) << MFDevice.preScaleShift;
        }
        if (MFDevice.preScaleZoomInFlag) {
            return (this.clipX - this.transX) >> MFDevice.preScaleShift;
        }
        return this.clipX - this.transX;
    }

    public final int getClipY() {
        if (MFDevice.preScaleZoomOutFlag) {
            return (this.clipY - this.transY) << MFDevice.preScaleShift;
        }
        if (MFDevice.preScaleZoomInFlag) {
            return (this.clipY - this.transY) >> MFDevice.preScaleShift;
        }
        return this.clipY - this.transY;
    }

    public final int getClipWidth() {
        if (MFDevice.preScaleZoomOutFlag) {
            return this.clipWidth << MFDevice.preScaleShift;
        }
        if (MFDevice.preScaleZoomInFlag) {
            return this.clipWidth >> MFDevice.preScaleShift;
        }
        return this.clipWidth;
    }

    public final int getClipHeight() {
        if (MFDevice.preScaleZoomOutFlag) {
            return this.clipHeight << MFDevice.preScaleShift;
        }
        if (MFDevice.preScaleZoomInFlag) {
            return this.clipHeight >> MFDevice.preScaleShift;
        }
        return this.clipHeight;
    }

    public final void drawImage(MFImage image, int x, int y, int anchor) {
        int imageWidth = image.getWidth();
        int imageHeight = image.getHeight();
        if (MFDevice.preScaleZoomOutFlag) {
            x >>= MFDevice.preScaleShift;
            y >>= MFDevice.preScaleShift;
            imageWidth >>= MFDevice.preScaleShift;
            imageHeight >>= MFDevice.preScaleShift;
        } else if (MFDevice.preScaleZoomInFlag) {
            x <<= MFDevice.preScaleShift;
            y <<= MFDevice.preScaleShift;
            imageWidth <<= MFDevice.preScaleShift;
            imageHeight <<= MFDevice.preScaleShift;
        }
        caculateAnchorOffset(imageWidth, imageHeight, anchor);
        drawImageImpl(image, xOff + x, yOff + y);
    }

    public final void drawImage(MFImage image, int x, int y) {
        drawImage(image, x, y, 20);
    }

    public final void drawImage(MFImage image, int x, int y, int flipMode, int anchor) {
        drawRegion(image, 0, 0, image.getWidth(), image.getHeight(), flipMode, x, y, anchor);
    }

    public final void drawImage(MFImage image, int x, int y, int regionX, int regionY, int regionW, int regionH) {
        drawRegion(image, regionX, regionY, regionW, regionH, 0, x, y, 20);
    }

    public final void drawRegion(MFImage image, int regionX, int regionY, int regionW, int regionH, int flipMode, int x, int y, int anchor) {
        if (MFDevice.preScaleZoomOutFlag) {
            x >>= MFDevice.preScaleShift;
            y >>= MFDevice.preScaleShift;
            regionX >>= MFDevice.preScaleShift;
            regionY >>= MFDevice.preScaleShift;
            regionW >>= MFDevice.preScaleShift;
            regionH >>= MFDevice.preScaleShift;
        } else if (MFDevice.preScaleZoomInFlag) {
            x <<= MFDevice.preScaleShift;
            y <<= MFDevice.preScaleShift;
            regionX <<= MFDevice.preScaleShift;
            regionY <<= MFDevice.preScaleShift;
            regionW <<= MFDevice.preScaleShift;
            regionH <<= MFDevice.preScaleShift;
        }
        if (flipMode <= 3) {
            caculateAnchorOffset(regionW, regionH, anchor);
        } else {
            caculateAnchorOffset(regionH, regionW, anchor);
        }
        drawRegionImpl(image, regionX, regionY, regionW, regionH, flipMode, x + xOff, y + yOff);
    }

    private final void caculateAnchorOffset(int width, int height, int anchor) {
        xOff = 0;
        yOff = 0;
        if ((anchor & 1) != 0) {
            xOff -= width >> 1;
        } else if ((anchor & 8) != 0) {
            xOff -= width;
        }
        if ((anchor & 2) != 0) {
            yOff -= height >> 1;
        } else if ((anchor & 32) != 0) {
            yOff -= height;
        }
    }

    public void drawRGB(int[] rgbData, int offset, int scanlength, int x, int y, int width, int height, boolean processAlpha) {
        int i;
        if (this.effectFlag) {
            if (this.alphaValue != 0) {
                if (!(this.redValue == 0 && this.greenValue == 0 && this.blueValue == 0)) {
                    for (i = 0; i < rgbData.length; i++) {
                        rgbData[i] = caculateColorValue(rgbData[i]);
                    }
                }
                if (this.grayValue != 0) {
                    for (i = 0; i < rgbData.length; i++) {
                        rgbData[i] = caculateGray(rgbData[i]);
                    }
                }
                if (this.alphaValue != 255) {
                    for (i = 0; i < rgbData.length; i++) {
                        rgbData[i] = caculateAlpha(rgbData[i]);
                    }
                }
            } else {
                return;
            }
        }
        int[] drawRGB = rgbData;
        int j;
        if (MFDevice.preScaleZoomOutFlag) {
            x >>= MFDevice.preScaleShift;
            y >>= MFDevice.preScaleShift;
            width >>= MFDevice.preScaleShift;
            height >>= MFDevice.preScaleShift;
            drawRGB = new int[(width * height)];
            for (i = 0; i < width; i++) {
                for (j = 0; j < height; j++) {
                    drawRGB[(j * width) + i] = rgbData[(i << MFDevice.preScaleShift) + ((j << MFDevice.preScaleShift) * (width << MFDevice.preScaleShift))];
                }
            }
            offset = 0;
            scanlength = width;
        } else if (MFDevice.preScaleZoomInFlag) {
            x <<= MFDevice.preScaleShift;
            y <<= MFDevice.preScaleShift;
            width <<= MFDevice.preScaleShift;
            height <<= MFDevice.preScaleShift;
            drawRGB = new int[(width * height)];
            for (i = 0; i < width; i++) {
                for (j = 0; j < height; j++) {
                    drawRGB[(j * width) + i] = rgbData[(i >> MFDevice.preScaleShift) + ((j >> MFDevice.preScaleShift) * (width >> MFDevice.preScaleShift))];
                }
            }
            offset = 0;
            scanlength = width;
        }
        if (this.effectFlag) {
            drawRGBFlip(drawRGB, x + this.transX, y + this.transY, width, height, 0);
            return;
        }
        drawRGBImpl(drawRGB, offset, scanlength, x + this.transX, y + this.transY, width, height, processAlpha);
    }

    private void drawRGBImpl(int[] rgbData, int offset, int scanlength, int x, int y, int width, int height, boolean processAlpha) {
        this.f7g.drawRGB(rgbData, offset, scanlength, x, y, width, height, processAlpha);
    }

    public void setColor(int color) {
        this.f7g.setColor(color);
    }

    public int getColor() {
        return this.f7g.getColor();
    }

    public void setColor(int red, int green, int blue) {
        this.f7g.setColor(((red << 16) | (green << 8)) | blue);
    }

    public final void drawPixel(int x, int y) {
        if (MFDevice.preScaleZoomOutFlag) {
            x >>= MFDevice.preScaleShift;
            y >>= MFDevice.preScaleShift;
        } else if (MFDevice.preScaleZoomInFlag) {
            x <<= MFDevice.preScaleShift;
            y <<= MFDevice.preScaleShift;
        }
        drawPixelImpl(this.transX + x, this.transY + y);
    }

    private final void drawPixelImpl(int x, int y) {
        if (this.effectFlag) {
            this.pixelInt[0] = this.f7g.getColor() | -16777216;
            if (!(this.alphaValue == 0 || (this.redValue == 0 && this.greenValue == 0 && this.blueValue == 0))) {
                this.pixelInt[0] = caculateColorValue(this.pixelInt[0]);
            }
            if (!(this.alphaValue == 0 || this.grayValue == 0)) {
                this.pixelInt[0] = caculateGray(this.pixelInt[0]);
            }
            if (!(this.alphaValue == 255 || this.alphaValue == 0)) {
                this.pixelInt[0] = caculateAlpha(this.pixelInt[0]);
            }
            if (this.alphaValue != 0) {
                this.f7g.drawRGB(this.pixelInt, 0, 1, x, y, 1, 1, true);
                return;
            }
            return;
        }
        this.f7g.drawLine(x, y, x, y);
    }

    public final void drawLine(int x1, int y1, int x2, int y2) {
        if (MFDevice.preScaleZoomOutFlag) {
            x1 >>= MFDevice.preScaleShift;
            y1 >>= MFDevice.preScaleShift;
            x2 >>= MFDevice.preScaleShift;
            y2 >>= MFDevice.preScaleShift;
        } else if (MFDevice.preScaleZoomInFlag) {
            x1 <<= MFDevice.preScaleShift;
            y1 <<= MFDevice.preScaleShift;
            x2 <<= MFDevice.preScaleShift;
            y2 <<= MFDevice.preScaleShift;
        }
        if (this.effectFlag) {
            drawLinePerPixel(this.transX + x1, this.transY + y1, this.transX + x2, this.transY + y2);
        } else {
            this.f7g.drawLine(this.transX + x1, this.transY + y1, this.transX + x2, this.transY + y2);
        }
    }

    private final void drawLinePerPixel(int x1, int y1, int x2, int y2) {
        int i;
        drawLineStepX = x1 < x2 ? 1 : -1;
        if (y1 < y2) {
            i = 1;
        } else {
            i = -1;
        }
        drawLineStepY = i;
        drawLineDX = Math.abs(x2 - x1);
        drawLineDY = Math.abs(y2 - y1);
        drawLineMFlag = drawLineDX - drawLineDY > 0;
        if (drawLineMFlag) {
            drawLineD = drawLineDX - (drawLineDY << 1);
            drawLineIncrE = -(drawLineDY << 1);
            drawLineIncrNE = (drawLineDX - drawLineDY) << 1;
        } else {
            drawLineD = drawLineDY - (drawLineDX << 1);
            drawLineIncrE = -(drawLineDX << 1);
            drawLineIncrNE = (drawLineDY - drawLineDX) << 1;
        }
        drawLineX = x1;
        drawLineY = y1;
        drawPixelImpl(drawLineX, drawLineY);
        if (drawLineMFlag) {
            while (drawLineX != x2) {
                if (drawLineD > 0) {
                    drawLineD += drawLineIncrE;
                    drawLineX += drawLineStepX;
                } else {
                    drawLineD += drawLineIncrNE;
                    drawLineX += drawLineStepX;
                    drawLineY += drawLineStepY;
                }
                drawPixelImpl(drawLineX, drawLineY);
            }
            return;
        }
        while (drawLineY != y2) {
            if (drawLineD > 0) {
                drawLineD += drawLineIncrE;
                drawLineY += drawLineStepY;
            } else {
                drawLineD += drawLineIncrNE;
                drawLineX += drawLineStepX;
                drawLineY += drawLineStepY;
            }
            drawPixelImpl(drawLineX, drawLineY);
        }
    }

    public final void drawTriangle(int x1, int y1, int x2, int y2, int x3, int y3) {
        drawLine(x1, y1, x2, y2);
        drawLine(x1, y1, x3, y3);
        drawLine(x2, y2, x3, y3);
    }

    public final void fillTriangle(int x1, int y1, int x2, int y2, int x3, int y3) {
        if (MFDevice.preScaleZoomOutFlag) {
            x1 >>= MFDevice.preScaleShift;
            y1 >>= MFDevice.preScaleShift;
            x2 >>= MFDevice.preScaleShift;
            y2 >>= MFDevice.preScaleShift;
            x3 >>= MFDevice.preScaleShift;
            y3 >>= MFDevice.preScaleShift;
        } else if (MFDevice.preScaleZoomInFlag) {
            x1 <<= MFDevice.preScaleShift;
            y1 <<= MFDevice.preScaleShift;
            x2 <<= MFDevice.preScaleShift;
            y2 <<= MFDevice.preScaleShift;
            x3 <<= MFDevice.preScaleShift;
            y3 <<= MFDevice.preScaleShift;
        }
        this.f7g.fillTriangle(this.transX + x1, this.transY + y1, this.transX + x2, this.transY + y2, this.transX + x3, this.transY + y3);
    }

    public final void drawRect(int x, int y, int w, int h) {
        if (MFDevice.preScaleZoomOutFlag) {
            x >>= MFDevice.preScaleShift;
            y >>= MFDevice.preScaleShift;
            w >>= MFDevice.preScaleShift;
            h >>= MFDevice.preScaleShift;
        } else if (MFDevice.preScaleZoomInFlag) {
            x <<= MFDevice.preScaleShift;
            y <<= MFDevice.preScaleShift;
            w <<= MFDevice.preScaleShift;
            h <<= MFDevice.preScaleShift;
        }
        if (w >= 0 && h >= 0) {
            if (this.effectFlag) {
                fillRect(x, y, w, 1);
                fillRect(x, y, 1, h);
                fillRect(x + w, y, 1, h);
                fillRect(x, y + h, w, 1);
                return;
            }
            this.f7g.drawRect(this.transX + x, this.transY + y, w, h);
        }
    }

    public final void fillRect(int x, int y, int w, int h) {
        if (MFDevice.preScaleZoomOutFlag) {
            x >>= MFDevice.preScaleShift;
            y >>= MFDevice.preScaleShift;
            w >>= MFDevice.preScaleShift;
            h >>= MFDevice.preScaleShift;
        } else if (MFDevice.preScaleZoomInFlag) {
            x <<= MFDevice.preScaleShift;
            y <<= MFDevice.preScaleShift;
            w <<= MFDevice.preScaleShift;
            h <<= MFDevice.preScaleShift;
        }
        if (this.effectFlag) {
            this.fillRectRGB[0] = this.f7g.getColor() | -16777216;
            if (!(this.alphaValue == 0 || (this.redValue == 0 && this.greenValue == 0 && this.blueValue == 0))) {
                this.fillRectRGB[0] = caculateColorValue(this.fillRectRGB[0]);
            }
            if (!(this.alphaValue == 0 || this.grayValue == 0)) {
                this.fillRectRGB[0] = caculateGray(this.fillRectRGB[0]);
            }
            if (!(this.alphaValue == 255 || this.alphaValue == 0)) {
                this.fillRectRGB[0] = caculateAlpha(this.fillRectRGB[0]);
            }
            if (this.alphaValue != 0) {
                int i;
                for (i = 0; i < this.fillRectRGB.length; i++) {
                    this.fillRectRGB[i] = this.fillRectRGB[0];
                }
                int x1 = this.transX + x < this.clipX ? this.clipX : this.transX + x;
                int y1 = this.transY + y < this.clipY ? this.clipY : this.transY + y;
                this.f7g.setClip(x1, y1, ((this.transX + x) + w) - x1, ((this.transY + y) + h) - y1);
                for (i = 0; i < (w / 10) + 1; i++) {
                    for (int j = 0; j < (h / 10) + 1; j++) {
                        this.f7g.drawRGB(this.fillRectRGB, 0, 10, (this.transX + x) + (i * 10), (this.transY + y) + (j * 10), 10, 10, true);
                    }
                }
                this.f7g.setClip(this.clipX, this.clipY, this.clipWidth, this.clipHeight);
                return;
            }
            return;
        }
        this.f7g.fillRect(this.transX + x, this.transY + y, w, h);
    }

    public final void drawArc(int x, int y, int width, int height, int startAngle, int arcAngle) {
        if (MFDevice.preScaleZoomOutFlag) {
            x >>= MFDevice.preScaleShift;
            y >>= MFDevice.preScaleShift;
            width >>= MFDevice.preScaleShift;
            height >>= MFDevice.preScaleShift;
        } else if (MFDevice.preScaleZoomInFlag) {
            x <<= MFDevice.preScaleShift;
            y <<= MFDevice.preScaleShift;
            width <<= MFDevice.preScaleShift;
            height <<= MFDevice.preScaleShift;
        }
        this.f7g.drawArc(this.transX + x, this.transY + y, width, height, startAngle, arcAngle);
    }

    public final void fillArc(int x, int y, int width, int height, int startAngle, int arcAngle) {
        if (MFDevice.preScaleZoomOutFlag) {
            x >>= MFDevice.preScaleShift;
            y >>= MFDevice.preScaleShift;
            width >>= MFDevice.preScaleShift;
            height >>= MFDevice.preScaleShift;
        } else if (MFDevice.preScaleZoomInFlag) {
            x <<= MFDevice.preScaleShift;
            y <<= MFDevice.preScaleShift;
            width <<= MFDevice.preScaleShift;
            height <<= MFDevice.preScaleShift;
        }
        this.f7g.fillArc(this.transX + x, this.transY + y, width, height, startAngle, arcAngle);
    }

    public final void drawRoundRect(int x, int y, int width, int height, int arcWidth, int arcHeight) {
        if (MFDevice.preScaleZoomOutFlag) {
            x >>= MFDevice.preScaleShift;
            y >>= MFDevice.preScaleShift;
            width >>= MFDevice.preScaleShift;
            height >>= MFDevice.preScaleShift;
        } else if (MFDevice.preScaleZoomInFlag) {
            x <<= MFDevice.preScaleShift;
            y <<= MFDevice.preScaleShift;
            width <<= MFDevice.preScaleShift;
            height <<= MFDevice.preScaleShift;
        }
        this.f7g.drawRoundRect(this.transX + x, this.transY + y, width, height, arcWidth, arcWidth);
    }

    public final void fillRoundRect(int x, int y, int width, int height, int arcWidth, int arcHeight) {
        if (MFDevice.preScaleZoomOutFlag) {
            x >>= MFDevice.preScaleShift;
            y >>= MFDevice.preScaleShift;
            width >>= MFDevice.preScaleShift;
            height >>= MFDevice.preScaleShift;
        } else if (MFDevice.preScaleZoomInFlag) {
            x <<= MFDevice.preScaleShift;
            y <<= MFDevice.preScaleShift;
            width <<= MFDevice.preScaleShift;
            height <<= MFDevice.preScaleShift;
        }
        this.f7g.fillRoundRect(this.transX + x, this.transY + y, width, height, arcWidth, arcWidth);
    }

    private static final Font getFont(int type) {
        switch (type) {
            case FONT_LARGE /*-3*/:
                return font_large;
            case -2:
                return font_medium;
            case -1:
                return font_small;
            default:
                return Font.getFont(type);
        }
    }

    public final void setFont(int font) {
        if (MFDevice.preScaleZoomOutFlag) {
            font >>= MFDevice.preScaleShift;
        } else if (MFDevice.preScaleZoomInFlag) {
            font <<= MFDevice.preScaleShift;
        }
        if (font_type != font) {
            currentFont = getFont(font);
            if (currentFont != null) {
                font_type = font;
            }
        }
        this.f7g.setFont(currentFont);
    }

    public final int getFont() {
        if (MFDevice.preScaleZoomOutFlag) {
            return font_type << MFDevice.preScaleShift;
        }
        if (MFDevice.preScaleZoomInFlag) {
            return font_type >> MFDevice.preScaleShift;
        }
        return font_type;
    }

    public final void drawString(String str, int x, int y, int anchor) {
        int stringWidth = stringWidth(str);
        int charHeight = charHeight();
        if (MFDevice.preScaleZoomOutFlag) {
            stringWidth >>= MFDevice.preScaleShift;
            charHeight >>= MFDevice.preScaleShift;
            x >>= MFDevice.preScaleShift;
            y >>= MFDevice.preScaleShift;
        } else if (MFDevice.preScaleZoomInFlag) {
            stringWidth <<= MFDevice.preScaleShift;
            charHeight <<= MFDevice.preScaleShift;
            x <<= MFDevice.preScaleShift;
            y <<= MFDevice.preScaleShift;
        }
        caculateAnchorOffset(stringWidth, charHeight, anchor);
        this.f7g.drawString(str, (xOff + x) + this.transX, (yOff + y) + this.transY, 20);
    }

    public final void drawSubstring(String str, int offset, int len, int x, int y, int anchor) {
        drawString(str.substring(offset, len), x, y, anchor);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final int charHeight(int r2) {
        /*
        r0 = r2;
        switch(r2) {
            case -3: goto L_0x0011;
            case -2: goto L_0x000f;
            case -1: goto L_0x000d;
            default: goto L_0x0004;
        };
    L_0x0004:
        r1 = com.sega.mobile.framework.device.MFDevice.preScaleZoomOutFlag;
        if (r1 == 0) goto L_0x0014;
    L_0x0008:
        r1 = com.sega.mobile.framework.device.MFDevice.preScaleShift;
        r1 = r0 << r1;
    L_0x000c:
        return r1;
    L_0x000d:
        r0 = 24;
    L_0x000f:
        r0 = 26;
    L_0x0011:
        r0 = 30;
        goto L_0x0004;
    L_0x0014:
        r1 = com.sega.mobile.framework.device.MFDevice.preScaleZoomInFlag;
        if (r1 == 0) goto L_0x001d;
    L_0x0018:
        r1 = com.sega.mobile.framework.device.MFDevice.preScaleShift;
        r1 = r0 >> r1;
        goto L_0x000c;
    L_0x001d:
        r1 = r0;
        goto L_0x000c;
        */
//        throw new UnsupportedOperationException("Method not decompiled: com.sega.mobile.framework.device.MFGraphics.charHeight(int):int");

        //System.out.println("Method not decompiled: com.sega.mobile.framework.device.MFGraphics.charHeight(int):int");
        return 24;
    }

    public static final int stringWidth(int font, String str) {
        if (MFDevice.preScaleZoomOutFlag) {
            if (font_type == font) {
                return currentFont.stringWidth(str) << MFDevice.preScaleShift;
            }
            return getFont(font).stringWidth(str) << MFDevice.preScaleShift;
        } else if (MFDevice.preScaleZoomInFlag) {
            if (font_type == font) {
                return currentFont.stringWidth(str) >> MFDevice.preScaleShift;
            }
            return getFont(font).stringWidth(str) >> MFDevice.preScaleShift;
        } else if (font_type == font) {
            return currentFont.stringWidth(str);
        } else {
            return getFont(font).stringWidth(str);
        }
    }

    public final int charHeight() {
        return charHeight(font_type);
    }

    public final int stringWidth(String str) {
        return stringWidth(font_type, str);
    }

    public final void enableEffect() {
        this.pixelInt = new int[1];
        this.redValue = 0;
        this.greenValue = 0;
        this.blueValue = 0;
        this.alphaValue = 255;
        this.grayValue = 0;
        this.effectFlag = true;
    }

    public final void disableEffect() {
        this.effectFlag = false;
    }

    public final void enableExceedBoundary() {
        this.enableExceed = true;
        this.f7g.setClip(-MFDevice.horizontalOffset, -MFDevice.verticvalOffset, MFDevice.bufferWidth, MFDevice.bufferHeight);
    }

    public final boolean getExceedBoundaryFlag() {
        return this.enableExceed;
    }

    public final boolean getEffectFlag() {
        return this.effectFlag;
    }

    public final int getNativeCanvasLeft() {
        return -MFDevice.horizontalOffset;
    }

    public final int getNativeCanvasTop() {
        return -MFDevice.verticvalOffset;
    }

    public final int getNativeCanvasRight() {
        return MFDevice.bufferWidth - MFDevice.horizontalOffset;
    }

    public final int getNativeCanvasBottom() {
        return MFDevice.bufferHeight - MFDevice.verticvalOffset;
    }

    public final int getNativeCanvasWidth() {
        return MFDevice.bufferWidth;
    }

    public final int getNativeCanvasHeight() {
        return MFDevice.bufferHeight;
    }

    public final void disableExceedBoundary() {
        this.enableExceed = false;
        if (MFDevice.preScaleZoomOutFlag) {
            this.f7g.setClip(0, 0, (MFDevice.bufferWidth - (MFDevice.horizontalOffset << 1)) << MFDevice.preScaleShift, (MFDevice.bufferHeight - (MFDevice.verticvalOffset << 1)) << MFDevice.preScaleShift);
        } else if (MFDevice.preScaleZoomInFlag) {
            this.f7g.setClip(0, 0, (MFDevice.bufferWidth - (MFDevice.horizontalOffset << 1)) >> MFDevice.preScaleShift, (MFDevice.bufferHeight - (MFDevice.verticvalOffset << 1)) >> MFDevice.preScaleShift);
        }
        this.f7g.setClip(0, 0, MFDevice.bufferWidth - (MFDevice.horizontalOffset << 1), MFDevice.bufferHeight - (MFDevice.verticvalOffset << 1));
    }

    public final void clearScreen(int color) {
        this.f7g.setColor(color);
        enableExceedBoundary();
        this.f7g.fillRect(-MFDevice.horizontalOffset, -MFDevice.verticvalOffset, MFDevice.bufferWidth, MFDevice.bufferHeight);
        disableExceedBoundary();
    }

    public final void setHue(int rValue, int gValue, int bValue) {
        this.redValue = caculateValue(rValue);
        this.greenValue = caculateValue(gValue);
        this.blueValue = caculateValue(bValue);
    }

    public final void setAlpha(int alpha) {
        this.alphaValue = alpha;
        if (this.alphaValue == -1) {
            this.alphaValue = 255;
        }
        if (this.alphaValue < 0) {
            this.alphaValue = 0;
        }
        if (this.alphaValue > 255) {
            this.alphaValue = 255;
        }
        this.f7g.setAlpha(this.alphaValue);
    }

    public final int getAlpha() {
        return this.f7g.getAlpha();
    }

    public final void setGray(int gValue) {
        this.grayValue = gValue;
        if (this.grayValue < 0) {
            this.grayValue = 0;
        }
        if (this.grayValue > 255) {
            this.grayValue = 255;
        }
    }

    private final int caculateValue(int value) {
        if (value < 0) {
            if (value > -255) {
                return value;
            }
            return -255;
        } else if (value == 0) {
            return 0;
        } else {
            return value > 255 ? 255 : (65536 / (256 - value)) - 256;
        }
    }

    private final void drawImageImpl(MFImage image, int x, int y) {
        if (this.effectFlag) {
            int i;
            this.drawEffectRGB = null;
            this.drawEffectRGB = new int[(image.getWidth() * image.getHeight())];
            image.getRGB(this.drawEffectRGB, 0, image.getWidth(), 0, 0, image.getWidth(), image.getHeight());
            if (!(this.alphaValue == 0 || (this.redValue == 0 && this.greenValue == 0 && this.blueValue == 0))) {
                for (i = 0; i < this.drawEffectRGB.length; i++) {
                    this.drawEffectRGB[i] = caculateColorValue(this.drawEffectRGB[i]);
                }
            }
            if (!(this.alphaValue == 0 || this.grayValue == 0)) {
                for (i = 0; i < this.drawEffectRGB.length; i++) {
                    this.drawEffectRGB[i] = caculateGray(this.drawEffectRGB[i]);
                }
            }
            if (!(this.alphaValue == 255 || this.alphaValue == 0)) {
                for (i = 0; i < this.drawEffectRGB.length; i++) {
                    this.drawEffectRGB[i] = caculateAlpha(this.drawEffectRGB[i]);
                }
            }
            if (this.alphaValue != 0) {
                drawRGBFlip(this.drawEffectRGB, x + this.transX, y + this.transY, image.getWidth(), image.getHeight(), 0);
                return;
            }
            return;
        }
        this.f7g.drawImage(image.image, this.transX + x, this.transY + y, 20);
    }

    private final void drawRegionImpl(MFImage image, int regionX, int regionY, int regionW, int regionH, int flipMode, int x, int y) {
        if (!this.effectFlag) {
            this.f7g.drawRegion(image.image, regionX, regionY, regionW, regionH, flipMode, x + this.transX, y + this.transY, 20);
        } else if (this.alphaValue != 0) {
            int i;
            this.drawEffectRGB = new int[(regionW * regionH)];
            image.getRGB(this.drawEffectRGB, 0, regionW, regionX, regionY, regionW, regionH);
            if (!(this.redValue == 0 && this.greenValue == 0 && this.blueValue == 0)) {
                for (i = 0; i < this.drawEffectRGB.length; i++) {
                    this.drawEffectRGB[i] = caculateColorValue(this.drawEffectRGB[i]);
                }
            }
            if (this.grayValue != 0) {
                for (i = 0; i < this.drawEffectRGB.length; i++) {
                    this.drawEffectRGB[i] = caculateGray(this.drawEffectRGB[i]);
                }
            }
            if (this.alphaValue != 255) {
                for (i = 0; i < this.drawEffectRGB.length; i++) {
                    this.drawEffectRGB[i] = caculateAlpha(this.drawEffectRGB[i]);
                }
            }
            drawRGBFlip(this.drawEffectRGB, x + this.transX, y + this.transY, regionW, regionH, flipMode);
            this.drawEffectRGB = null;
        }
    }

    private final void drawRGBFlip(int[] argb, int x, int y, int width, int height, int flipMode) {
        int[] rgbData = new int[width];
        int i;
        switch (flipMode) {
            case 0:
                for (i = 0; i < height; i++) {
                    System.arraycopy(argb, i * width, rgbData, 0, width);
                    this.f7g.drawRGB(rgbData, 0, width, x, y + i, width, 1, true);
                }
                return;
            case 1:
                for (i = 0; i < height; i++) {
                    System.arraycopy(argb, i * width, rgbData, 0, width);
                    this.f7g.drawRGB(rgbData, 0, width, x, ((y + height) - 1) - i, width, 1, true);
                }
                return;
            case 2:
                for (i = 0; i < height; i++) {
                    System.arraycopy(argb, i * width, rgbData, 0, width);
                    MFUtility.revertArray(rgbData);
                    this.f7g.drawRGB(rgbData, 0, width, x, y + i, width, 1, true);
                }
                return;
            case 3:
                for (i = 0; i < height; i++) {
                    System.arraycopy(argb, i * width, rgbData, 0, width);
                    MFUtility.revertArray(rgbData);
                    this.f7g.drawRGB(rgbData, 0, width, x, ((y + height) - 1) - i, width, 1, true);
                }
                return;
            case 4:
                for (i = 0; i < height; i++) {
                    System.arraycopy(argb, i * width, rgbData, 0, width);
                    this.f7g.drawRGB(rgbData, 0, 1, x + i, y, 1, width, true);
                }
                return;
            case 5:
                for (i = 0; i < height; i++) {
                    System.arraycopy(argb, i * width, rgbData, 0, width);
                    this.f7g.drawRGB(rgbData, 0, 1, ((x + height) - 1) - i, y, 1, width, true);
                }
                return;
            case 6:
                for (i = 0; i < height; i++) {
                    System.arraycopy(argb, i * width, rgbData, 0, width);
                    MFUtility.revertArray(rgbData);
                    this.f7g.drawRGB(rgbData, 0, 1, x + i, y, 1, width, true);
                }
                return;
            case 7:
                for (i = 0; i < height; i++) {
                    System.arraycopy(argb, i * width, rgbData, 0, width);
                    MFUtility.revertArray(rgbData);
                    this.f7g.drawRGB(rgbData, 0, 1, ((x + height) - 1) - i, y, 1, width, true);
                }
                return;
            default:
                return;
        }
    }

    private final int caculateColorValue(int argb) {
        return (((-16777216 & argb) | (caculatePerColor((16711680 & argb) >>> 16, this.redValue) << 16)) | (caculatePerColor((65280 & argb) >>> 8, this.greenValue) << 8)) | caculatePerColor(argb & 255, this.blueValue);
    }

    private final int caculatePerColor(int color, int value) {
        if (value == 0) {
            return color;
        }
        color += ((color + 1) * value) >> 8;
        if (color > 255) {
            return 255;
        }
        if (color < 0) {
            return 0;
        }
        return color;
    }

    private final int caculateGray(int argb) {
        int color;
        int r = (16711680 & argb) >>> 16;
        int g = (65280 & argb) >>> 8;
        int b = argb & 255;
        if (r > g) {
            color = r;
        } else {
            color = g;
        }
        if (color <= b) {
            color = b;
        }
        color = (this.grayValue * color) >> 8;
        if (color > 255) {
            color = 255;
        } else if (color < 0) {
            color = 0;
        }
        return color | (((-16777216 & argb) | (color << 16)) | (color << 8));
    }

    private final int caculateAlpha(int argb) {
        return (MapManager.END_COLOR & argb) | (((((-16777216 & argb) >>> 24) * this.alphaValue) >> 8) << 24);
    }

    public void drawRegion(MFImage img, int x_src, int y_src, int width, int height, int rotate_x, int rotate_y, int degree, int scale_x, int scale_y, int x_dest, int y_dest, int anchor) {
        if (MFDevice.preScaleZoomOutFlag) {
            x_src >>= MFDevice.preScaleShift;
            y_src >>= MFDevice.preScaleShift;
            width >>= MFDevice.preScaleShift;
            height >>= MFDevice.preScaleShift;
            rotate_x >>= MFDevice.preScaleShift;
            rotate_y >>= MFDevice.preScaleShift;
            scale_x >>= MFDevice.preScaleShift;
            scale_y >>= MFDevice.preScaleShift;
            x_dest >>= MFDevice.preScaleShift;
            y_dest >>= MFDevice.preScaleShift;
        } else if (MFDevice.preScaleZoomInFlag) {
            x_src <<= MFDevice.preScaleShift;
            y_src <<= MFDevice.preScaleShift;
            width <<= MFDevice.preScaleShift;
            height <<= MFDevice.preScaleShift;
            rotate_x <<= MFDevice.preScaleShift;
            rotate_y <<= MFDevice.preScaleShift;
            scale_x <<= MFDevice.preScaleShift;
            scale_y <<= MFDevice.preScaleShift;
            x_dest <<= MFDevice.preScaleShift;
            y_dest <<= MFDevice.preScaleShift;
        }
        this.f7g.drawRegion(img.image, x_src, y_src, width, height, rotate_x, rotate_y, degree, scale_x, scale_y, x_dest, y_dest, anchor);
    }
}
