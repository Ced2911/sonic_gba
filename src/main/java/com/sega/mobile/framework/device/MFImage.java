package com.sega.mobile.framework.device;

import com.sega.mobile.framework.android.Image;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class MFImage {
    private int alphaColor = -1;
    private MFGraphics graphics;
    protected Image image;
    private boolean mutable = false;

    private MFImage() {
    }

    public static final MFImage createPaletteImage(String paletteFileName) {
        try {
            int data;
            InputStream is = MFDevice.getResourceAsStream(paletteFileName);
            byte[] imageNameBytes = new byte[is.read()];
            is.read(imageNameBytes);
            ByteArrayOutputStream bis = new ByteArrayOutputStream();
            while (true) {
                data = is.read();
                if (data == -1) {
                    break;
                }
                bis.write(data);
            }
            is.close();
            byte[] palData = bis.toByteArray();
            bis.close();
            is = MFDevice.getResourceAsStream(paletteFileName.substring(0, paletteFileName.lastIndexOf(47) + 1) + new String(imageNameBytes));
            bis = new ByteArrayOutputStream();
            while (true) {
                data = is.read();
                if (data == -1) {
                    break;
                }
                bis.write(data);
            }
            is.close();
            byte[] image = bis.toByteArray();
            bis.close();
            for (int i = 0; i < palData.length; i++) {
                image[i + 37] = palData[i];
            }
            return createImage(new ByteArrayInputStream(image));
        } catch (Throwable th) {
            Throwable e = th;
            return null;
        }
    }

    public static final MFImage createImage(String url) {
        return createImage(MFDevice.getResourceAsStream(url));
    }

    public static final MFImage createImage(InputStream is) {
        if (is == null) {
            return null;
        }
        MFImage ret = null;
        try {
            ret = createImage(Image.createImage(is));
        } catch (Exception e) {
        }
        try {
            is.close();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        return ret;
    }

    public static final MFImage createImage(MFImage image, int x, int y, int width, int height, int transform) {
        return createImage(Image.createImage(image.image, x, y, width, height, transform));
    }

    public static final MFImage createImage(Object sysImg) {
        MFImage ret = new MFImage();
        ret.image = (Image) sysImg;
        return ret;
    }

    public static final MFImage createImage(int width, int height) {
        MFImage ret = new MFImage();
        ret.image = Image.createImage(width, height);
        ret.graphics = MFGraphics.createMFGraphics(ret.image.getGraphics(), width, height);
        ret.mutable = true;
        return ret;
    }

    public static final MFImage createImage(byte[] array) {
        InputStream bais = new ByteArrayInputStream(array);
        MFImage ret = createImage(bais);
        try {
            bais.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ret;
    }

    public static final MFImage createImage(byte[] array, int offset, int length) {
        InputStream bais = new ByteArrayInputStream(array, offset, length);
        MFImage ret = createImage(bais);
        try {
            bais.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ret;
    }

    public final MFGraphics getGraphics() {
        return this.graphics;
    }

    public final int getWidth() {
        return this.image.getWidth();
    }

    public final int getHeight() {
        return this.image.getHeight();
    }

    public final void getRGB(int[] rgbData, int offset, int scanlength, int x, int y, int width, int height) {
        this.image.getRGB(rgbData, offset, scanlength, x, y, width, height);
    }

    public boolean isMutable() {
        return this.mutable;
    }

    public void setAlphaColor(int color) {
        this.alphaColor = 0xFF000000 | color;
    }

    public int getAlphaColor() {
        return this.alphaColor;
    }

    public void setAlpha(int a) {
        this.image.setAlpha(a);
    }

    public int getAlpha() {
        return this.image.getAlpha();
    }
}
