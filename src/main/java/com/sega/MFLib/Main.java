package com.sega.MFLib;

import MFLib.MainState;
import SonicGBA.GlobalResource;
import SonicGBA.SonicDef;
import Special.SSDef;
import State.State;
import android.app.Dialog;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.telephony.TelephonyManager;
import android.view.KeyEvent;
import com.sega.mobile.framework.MFGameState;
import com.sega.mobile.framework.MFMain;
import com.sega.mobile.framework.device.MFDevice;
import com.sega.mobile.framework.device.MFGraphics;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import month.MonthCertificationState;

public class Main extends MFMain {
    public static boolean BULLET_TIME;
    public static boolean MONTH_CHECK = false;
    public Context context;
    private String encryptionSubID = null;
    private String esubid = null;
    public Handler handler = new C00001();
    public boolean isResumeFromOtherActivity;
    private String mScore = "";
    private String mScoreStr = "";
    public boolean resumeFromOtherActivityFlag;
    public String segaMoreUrl = null;
    private TelephonyManager tMgr;

    class C00001 extends Handler {
        C00001() {
        }

        public void handleMessage(Message msg) {
            switch (msg.what) {
            }
            super.handleMessage(msg);
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 25) {
            if (GlobalResource.soundSwitchConfig == 0) {
                return true;
            }
            if (!State.IsInInterrupt) {
                State.setSoundVolumnDown();
            }
            return super.onKeyDown(keyCode, event);
        } else if (keyCode == 24) {
            if (GlobalResource.soundSwitchConfig == 0) {
                return true;
            }
            if (!State.IsInInterrupt) {
                State.setSoundVolumnUp();
            }
            return super.onKeyDown(keyCode, event);
        } else if (keyCode == 82) {
            return true;
        } else {
            return super.onKeyDown(keyCode, event);
        }
    }

    public void showExitConfirm() {
        setExitConfirmStr("ソニックアドバンス", "ゲームを終了しますか？", "はい", "キャンセル");
    }

    public MFGameState getEntryGameState() {
        MFDevice.setCanvasSize(Math.min(Math.max(SSDef.PLAYER_MOVE_HEIGHT, (MFDevice.getDeviceWidth() * 160) / MFDevice.getDeviceHeight()), 284), 160);
        MFDevice.setEnableCustomBack(true);
        if (MONTH_CHECK) {
            return new MonthCertificationState(this, new MainState(this));
        }
        return new MainState(this);
    }

    public boolean logicDeviceSuspend() {
        return true;
    }

    public void drawDeviceSuspend(MFGraphics g) {
    }

    public void onDestroy() {
        super.onDestroy();
        System.exit(0);
    }

    protected void onResume() {
        super.onResume();
        if (this.resumeFromOtherActivityFlag) {
            this.isResumeFromOtherActivity = true;
            this.resumeFromOtherActivityFlag = false;
        }
        MonthCertificationState.resume();
    }

    protected void onPause() {
        super.onPause();
        MonthCertificationState.pause();
    }

    public Dialog onCreateDialog(int id) {
        return MonthCertificationState.onCreateDialog(id);
    }

    public void onPrepareDialog(int id, Dialog dialog) {
        MonthCertificationState.onPrepareDialog(id, dialog);
    }

    private void setSegaMoreUrl(boolean isHaveSim) {
        this.segaMoreUrl = "http://puyosega-android.segamobile.jp/appli/sonic_advance/redirect_dena.jsp" + (isHaveSim ? "?esubid=" + this.encryptionSubID : "");
    }

    public void buildSegaMoreUrl() {
        this.tMgr = (TelephonyManager) getSystemService("phone");
        this.esubid = this.tMgr.getSubscriberId();
        if (this.esubid == null) {
            setSegaMoreUrl(false);
            return;
        }
        this.encryptionSubID = getEncryptionSubID();
        setSegaMoreUrl(true);
    }

    public void setScore(String strScore) {
        this.mScore = strScore;
    }

    public void setScoreStr(String strScore) {
        this.mScoreStr = strScore;
    }

    public void setScore(int score) {
        /*
        int min = score / 60000;
        int sec = (score % 60000) / 1000;
        int msec = ((score % 60000) % 1000) / 10;
        String secStr = sec < 10 ? "0" + sec : sec;
        String msecStr = msec < 10 ? "0" + msec : msec;
        this.mScore = (SonicDef.OVER_TIME - score);
        System.out.println("~~mScore:" + this.mScore);
        this.mScoreStr = min + ":" + secStr + ":" + msecStr;
        System.out.println("~~mScoreStr:" + this.mScoreStr);
        */
        Integer min = score / 60000;
        Integer sec = (score % 60000) / 1000;
        Integer msec = ((score % 60000) % 1000) / 10;
        String secStr = sec < 10 ? "0" + sec.toString() : sec.toString();
        String msecStr = msec < 10 ? "0" + msec.toString() : msec.toString();

        Integer mscore = (SonicDef.OVER_TIME - score);

        this.mScore = mscore.toString();
        System.out.println("~~mScore:" + this.mScore);
        this.mScoreStr = min + ":" + secStr + ":" + msecStr;
        System.out.println("~~mScoreStr:" + this.mScoreStr);
    }

    public String getEncryptionSubID() {
        String before_subid = this.esubid;
        return toHexString(encrypt(before_subid.getBytes(), "UMAS3_SMARTPHONE".getBytes()));
    }

    private byte[] encrypt(byte[] data, byte[] secret_key) {
        try {
            SecretKeySpec sKey = new SecretKeySpec(secret_key, "AES");
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(1, sKey, new IvParameterSpec("UMAS3_ANDROID_IV".getBytes()));
            return cipher.doFinal(data);
        } catch (Exception e) {
            return null;
        }
    }

    private String toHexString(byte[] _byte) {
        StringBuffer hexString = new StringBuffer();
        for (byte b : _byte) {
            String plainText = Integer.toHexString(b & 255);
            if (plainText.length() < 2) {
                plainText = "0" + plainText;
            }
            hexString.append(plainText);
        }
        return new String(hexString);
    }
}
