package month;

import android.app.Activity;
import android.app.Dialog;
import android.view.SurfaceView;
import com.sega.mobile.framework.MFGameState;
import com.sega.mobile.framework.device.MFDevice;
import com.sega.mobile.framework.device.MFGraphics;

public class MonthCertificationState implements MFGameState {
    public static MonthCertification certification = new MonthCertification();
    public static boolean inCertification;
    static Activity mActivity;
    static MFGameState mainState;
    public static boolean pauseFlag;

    class C00161 implements Runnable {
        C00161() {
        }

        public void run() {
            MonthCertificationState.certification.init(MonthCertificationState.mActivity);
        }
    }

    public MonthCertificationState(Activity act, MFGameState entryState) {
        mainState = entryState;
        mActivity = act;
    }

    public static Dialog onCreateDialog(int id) {
        return certification.onCreateDialog(id);
    }

    public static void onPrepareDialog(int id, Dialog dialog) {
        certification.onPrepareDialog(id, dialog);
    }

    public static boolean logic() {
        return certification.logic();
    }

    public static void startGame() {
        mActivity.setContentView((SurfaceView) MFDevice.getSystemDisplayable());
        MFDevice.changeState(mainState);
    }

    public static void pause() {
        if (certification != null && !inCertification) {
            certification.onPause();
        }
    }

    public static void resume() {
        if (certification != null && !inCertification) {
            certification.onResume();
        }
    }

    public void onEnter() {
        inCertification = true;
        mActivity.runOnUiThread(new C00161());
    }

    public void onPause() {
        if (certification != null && !inCertification) {
            certification.onPause();
        }
    }

    public void onResume() {
        if (certification != null && !inCertification) {
            certification.onResume();
        }
    }

    public void onTick() {
    }

    public void onRender(MFGraphics g) {
    }

    public void onExit() {
        inCertification = false;
    }

    public int getFrameTime() {
        return 0;
    }

    public void onVolumeUp() {
    }

    public void onVolumeDown() {
    }

    public void onRender(MFGraphics g, int i) {
    }
}
