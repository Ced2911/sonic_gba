package month;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.telephony.TelephonyManager;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Calendar;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class MonthCertification {
    private static final int DIALOG_CERTIFICATION_MESSAGE = 200;
    private static final int DIALOG_ERROR_MESSAGE = 400;
    private static final int DIALOG_ERROR_NOT_WEB_TO_MESSAGE = 600;
    private static final int DIALOG_ERROR_WEB_TO_MESSAGE = 500;
    private static final int DIALOG_FINISH_MESSAGE = 100;
    private static final int DIALOG_OK_MESSAGE = 300;
    private static final int HTML_APP_START = 2;
    private static final int HTML_ERROR = 1;
    private static final int HTML_HANDLER_START = 3;
    private static final int HTML_MONTHLY = 0;
    public final String ANDROID_URL = "http://puyosega-android.segamobile.jp";
    public String appid = "sonic_advance";
    private int day = 0;
    private String devid = null;
    private String error_code = "";
    private String esubid = null;
    private int hour = 0;
    private byte[] http_data = null;
    private Activity mActivity;
    private int minute = 0;
    private int month = 0;
    public boolean monthly_check_flag = false;
    public int monthly_mode = 0;
    private String[] msg = null;
    private ProgressDialog progressDialog = null;
    private int second = 0;
    private boolean suspend_monthly_check_flag = false;
    private boolean suspend_progress_flag = false;
    private String ver = "100";
    private WebSettings webSettings = null;
    private boolean web_error_flag = false;
    private boolean web_progress_flag = false;
    private WebView webview = null;
    private int year = 0;

    class C00071 implements Runnable {
        C00071() {
        }

        public void run() {
            MonthCertificationState.startGame();
        }
    }

    class C00082 implements Runnable {
        C00082() {
        }

        public void run() {
            MonthCertificationState.startGame();
        }
    }

    class C00093 implements Runnable {
        C00093() {
        }

        public void run() {
            MonthCertification.this.progressDialog = new ProgressDialog(MonthCertification.this.mActivity);
            if (MonthCertification.this.progressDialog != null) {
                MonthCertification.this.progressDialog.setProgressStyle(0);
                MonthCertification.this.progressDialog.setMessage("通信中…");
                MonthCertification.this.progressDialog.setIndeterminate(false);
                MonthCertification.this.progressDialog.setCancelable(false);
                MonthCertification.this.progressDialog.show();
            }
        }
    }

    class C00104 implements Runnable {
        C00104() {
        }

        public void run() {
            if (MonthCertification.this.progressDialog != null) {
                MonthCertification.this.progressDialog.dismiss();
                MonthCertification.this.progressDialog = null;
            }
        }
    }

    class C00115 implements Runnable {
        C00115() {
        }

        public void run() {
            MonthCertification.this.mActivity.showDialog(200);
        }
    }

    class C00126 implements OnClickListener {
        C00126() {
        }

        public void onClick(DialogInterface dialog, int whichButton) {
            MonthCertification.this.exit();
        }
    }

    class C00137 implements OnClickListener {
        C00137() {
        }

        public void onClick(DialogInterface dialog, int whichButton) {
        }
    }

    class C00148 implements OnClickListener {
        C00148() {
        }

        public void onClick(DialogInterface dialog, int whichButton) {
            MonthCertification.this.monthly_mode = 1;
        }
    }

    class C00159 implements OnClickListener {
        C00159() {
        }

        public void onClick(DialogInterface dialog, int whichButton) {
            MonthCertification.this.exit();
        }
    }

    class CustomWebViewClient extends WebViewClient {
        private Activity activity;

        public CustomWebViewClient(Activity activity) {
            this.activity = activity;
            MonthCertification.this.web_progress_flag = false;
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            super.shouldOverrideUrlLoading(view, url);
            view.loadUrl(url);
            return true;
        }

        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            if (!MonthCertification.this.web_error_flag) {
                super.onPageStarted(view, url, favicon);
                if (!MonthCertification.this.web_progress_flag) {
                    MonthCertification.this.web_progress_flag = true;
                    MonthCertification.this.progressDialog = new ProgressDialog(this.activity);
                    MonthCertification.this.progressDialog.setProgressStyle(0);
                    MonthCertification.this.progressDialog.setMessage("Connecting...");
                    MonthCertification.this.progressDialog.setIndeterminate(false);
                    MonthCertification.this.progressDialog.setCancelable(false);
                    MonthCertification.this.progressDialog.show();
                }
            }
        }

        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            if (MonthCertification.this.web_progress_flag) {
                MonthCertification.this.web_progress_flag = false;
                if (MonthCertification.this.progressDialog != null) {
                    MonthCertification.this.progressDialog.dismiss();
                    MonthCertification.this.progressDialog = null;
                }
            }
        }

        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            super.onReceivedError(view, errorCode, description, failingUrl);
            MonthCertification.this.error_code = new StringBuilder(String.valueOf(errorCode)).toString();
            error();
        }

        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslErrorHandler error) {
            MonthCertification.this.error_code = "SSL ERROR";
            error();
        }

        private void error() {
            if (MonthCertification.this.web_progress_flag) {
                MonthCertification.this.web_progress_flag = false;
                if (MonthCertification.this.progressDialog != null) {
                    MonthCertification.this.progressDialog.dismiss();
                    MonthCertification.this.progressDialog = null;
                }
            }
            MonthCertification.this.exe(1);
        }
    }

    class JavaScript {
        private Activity activity;
        private MonthCertification cert;

        public JavaScript(Activity activity, MonthCertification cert) {
            this.activity = activity;
            this.cert = cert;
        }

        public void exit() {
            this.activity.finish();
            System.exit(0);
        }

        public void start() {
            this.cert.exe(3);
        }
    }

    public void init(Activity activity) {
        this.mActivity = activity;
        TelephonyManager telManager = (TelephonyManager) this.mActivity.getSystemService("phone");
        this.esubid = telManager.getSubscriberId();
        this.devid = telManager.getDeviceId();
        this.error_code = "";
        if (this.esubid == null) {
            this.error_code = "SIM ERROR";
            exe(1);
        } else if (this.devid == null) {
            this.error_code = "DEV ERROR";
            exe(1);
        } else {
            byte[] sData = read_data("esubid");
            byte[] dData = read_data("devid");
            if (sData == null && dData == null) {
                exe(0);
                return;
            }
            String _subid = "";
            String _devid = "";
            boolean check = true;
            if (sData != null) {
                _subid = new String(sData);
            }
            if (dData != null) {
                _devid = new String(dData);
            }
            if (_subid.equals("") || !_subid.equals(this.esubid)) {
                check = false;
            }
            if (_devid.equals("") || !_devid.equals(this.devid)) {
                check = false;
            }
            if (!check) {
                exe(0);
            } else if (monthly_check()) {
                exe(0);
            } else {
                exe(2);
            }
        }
    }

    private void exe(int mode) {
        if (mode == 0 || mode == 1) {
            JavaScript js = new JavaScript(this.mActivity, this);
            this.webview = new WebView(this.mActivity);
            this.webSettings = this.webview.getSettings();
            this.webSettings.setJavaScriptEnabled(true);
            this.webSettings.setBuiltInZoomControls(true);
            this.webview.addJavascriptInterface(js, "android");
            this.webview.setWebViewClient(new CustomWebViewClient(this.mActivity));
            this.webview.loadDataWithBaseURL("file:///android_asset/", getHTML(mode), "text/html", "utf-8", null);
            this.mActivity.setContentView(this.webview);
            this.webview.requestFocus();
        } else if (mode == 3) {
            Calendar cal1 = Calendar.getInstance();
            this.year = cal1.get(1);
            this.month = cal1.get(2) + 1;
            this.day = cal1.get(5);
            this.hour = cal1.get(11);
            this.minute = cal1.get(12);
            this.second = cal1.get(13);
            write_data("bYear", new StringBuilder(String.valueOf(this.year)).toString().getBytes());
            write_data("bMonth", new StringBuilder(String.valueOf(this.month)).toString().getBytes());
            write_data("bDay", new StringBuilder(String.valueOf(this.day)).toString().getBytes());
            write_data("bHour", new StringBuilder(String.valueOf(this.hour)).toString().getBytes());
            write_data("bMinute", new StringBuilder(String.valueOf(this.minute)).toString().getBytes());
            write_data("bSecond", new StringBuilder(String.valueOf(this.second)).toString().getBytes());
            write_data("esubid", this.esubid.getBytes());
            write_data("devid", this.devid.getBytes());
            this.mActivity.runOnUiThread(new C00071());
        } else if (mode == 2) {
            this.mActivity.runOnUiThread(new C00082());
        }
    }

    private byte[] read_data(String p_fileName) {
        byte[] w = new byte[10240];
        InputStream in = null;
        ByteArrayOutputStream out = null;
        try {
            in = this.mActivity.openFileInput(p_fileName);
            ByteArrayOutputStream out2 = new ByteArrayOutputStream();
            while (true) {
                try {
                    int size = in.read(w);
                    if (size <= 0) {
                        byte[] ret = out2.toByteArray();
                        out2.close();
                        in.close();
                        out = out2;
                        return ret;
                    }
                    out2.write(w, 0, size);
                } catch (Exception e) {
                    out = out2;
                }
            }
        } catch (Exception e2) {
            Exception exception = e2;
            if (out != null) {
                try {
                    out.close();
                } catch (Exception e3) {
                }
            }
            if (in != null) {
                try {
                    in.close();
                } catch (Exception e4) {
                }
            }
            return null;
        }
    }

    private void write_data(String p_fileName, byte[] p_data) {
        OutputStream out = null;
        try {
            out = this.mActivity.openFileOutput(p_fileName, 0);
            out.write(p_data, 0, p_data.length);
            out.close();
        } catch (Exception e) {
            if (out != null) {
                try {
                    out.close();
                } catch (Exception e2) {
                }
            }
        }
    }

    private boolean monthly_check() {
        boolean certification = false;
        Calendar cal1 = Calendar.getInstance();
        this.year = cal1.get(1);
        this.month = cal1.get(2) + 1;
        this.day = cal1.get(5);
        this.hour = cal1.get(11);
        this.minute = cal1.get(12);
        this.second = cal1.get(13);
        byte[] bYear = read_data("bYear");
        byte[] bMonth = read_data("bMonth");
        byte[] bDay = read_data("bDay");
        byte[] bHour = read_data("bHour");
        byte[] bMinute = read_data("bMinute");
        byte[] bSecond = read_data("bSecond");
        if (bYear == null || bMonth == null || bDay == null || bHour == null || bMinute == null || bSecond == null) {
            return true;
        }
        String str = new String(bYear);
        str = new String(bMonth);
        str = new String(bDay);
        str = new String(bHour);
        str = new String(bMinute);
        str = new String(bSecond);
        Calendar now = Calendar.getInstance();
        now.set(this.year, this.month, this.day, this.hour, this.minute, this.second);
        Calendar old = Calendar.getInstance();
        old.set(atoi(str), atoi(str), atoi(str), atoi(str), atoi(str), atoi(str));
        if (!now.after(old)) {
            certification = true;
        }
        Integer ym = (this.year + this.month);
        if ((ym).equals(new StringBuilder(String.valueOf(str)).append(str).toString())) {
            return certification;
        }
        return true;
    }

    private String getHTML(int mode) {
        String html = "";
        if (mode == 0) {
            return "<html><head><meta http-equiv=\"refresh\" content=\"0;URL=http://puyosega-android.segamobile.jp/ninshou/news.jsp?esubid=" + getEncryptionSubID() + "&devid=" + this.devid + "&appid=" + this.appid + "&ver=" + this.ver + "\">" + "</head>" + "<body>" + "</body>" + "</html>";
        }
        if (mode != 1) {
            return html;
        }
        html = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\"><html lang=\"ja\">\t<head>\t\t<meta http-equiv=\"Content-Type\" content=\"text/html; charset=shift_jis\">\t\t<meta http-equiv=\"Content-Script-Type\" content=\"text/javascript\">\t\t<meta name=\"viewport\" content=\"initial-scale=1.0;\">\t\t<script type=\"text/javascript\">\t\t\tfunction android_exit() { android.exit(); }\t\t</script>\t\t<title>\t\t\tE-7\t\t</title>\t</head>\t\t<body bgcolor=\"#ffffff\" text=\"#000000\" topmargin=\"0\" leftmargin=\"0\" marginwidth=\"0\" marginheight=\"0\"><div style=\"width: 100%; background:#154f98\" align=\"right\"><img src=\"img/migiue.gif\" alt=\"ヘッダー\"><br></div><div style=\"width: 100%; background:#f7e908\" align=\"center\"><img src=\"img/error.gif\" alt=\"通信に失敗しました\"><br></div><table width=\"85%\" align=\"center\" bgcolor=\"#FFFFFF\" border=\"0\">    <tr>    <td bgcolor=\"#ffffff\"><br>[<!--エラー番号--> ERROR TYPE:" + this.error_code + "]<br>" + "<br>" + "お手数ですが、一度アプリを終了し、再度起動してください。<br>" + "<br>" + "<div align=\"center\">" + "<input type=\"image\" src=\"img/end.gif\" alt=\"終了\" onClick=\"android_exit()\"><br>" + "</div>" + "<br>" + "    </td>" + "  </tr>" + "</table>" + "<div style=\"width: 100%; background:#154f98\" align=\"left\">" + "<img src=\"img/hidarishita.gif\" alt=\"フッダー\"><br>" + "</div>" + "<div style=\"width: 100%; background:#2166bc\" align=\"center\">" + "<img src=\"img/sega_logo.gif\" alt=\"セガロゴ\"><br>" + "</div>" + "\t</body>" + "</html>";
        this.web_error_flag = true;
        return html;
    }

    private int atoi(String string) {
        try {
            return Integer.parseInt(string);
        } catch (Exception e) {
            return 0;
        }
    }

    public String getEncryptionSubID() {
        String before_subid = this.esubid;
        return toHexString(encrypt(before_subid.getBytes(), "UMAS3_SMARTPHONE".getBytes()));
    }

    private byte[] encrypt(byte[] data, byte[] secret_key) {
        try {
            SecretKeySpec sKey = new SecretKeySpec(secret_key, "AES");
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(1, sKey, new IvParameterSpec("UMAS3_ANDROID_IV".getBytes()));
            return cipher.doFinal(data);
        } catch (Exception e) {
            return null;
        }
    }

    private String toHexString(byte[] _byte) {
        StringBuffer hexString = new StringBuffer();
        for (byte b : _byte) {
            String plainText = Integer.toHexString(b & 255);
            if (plainText.length() < 2) {
                plainText = "0" + plainText;
            }
            hexString.append(plainText);
        }
        return new String(hexString);
    }

    public void onPause() {
        ProgressDialogEnd();
        this.monthly_mode = 0;
        this.suspend_monthly_check_flag = true;
        if (this.web_progress_flag) {
            this.web_progress_flag = false;
            if (this.progressDialog != null) {
                this.progressDialog.dismiss();
                this.progressDialog = null;
            }
            this.error_code = "SUSPEND ERROR";
            exe(1);
        }
    }

    public void ProgressDialogStart() {
        this.suspend_progress_flag = true;
        this.mActivity.runOnUiThread(new C00093());
    }

    public void ProgressDialogEnd() {
        if (this.suspend_progress_flag) {
            this.suspend_progress_flag = false;
            this.mActivity.runOnUiThread(new C00104());
        }
    }

    public void onResume() {
        if (monthly_check() && this.suspend_monthly_check_flag) {
            monthly_check_dialog();
        }
        this.suspend_monthly_check_flag = false;
    }

    public void monthly_check_dialog() {
        this.monthly_check_flag = true;
        this.suspend_monthly_check_flag = false;
        try {
            this.mActivity.runOnUiThread(new C00115());
        } catch (Exception e) {
        }
    }

    public Dialog onCreateDialog(int id) {
        Builder d;
        String text = null;
        if (id == 100) {
            d = new Builder(this.mActivity);
            d.setTitle("Confirm");
            d.setMessage("Are you sure you want to quit?");
            d.setPositiveButton("Yes", new C00126());
            d.setNegativeButton("No", new C00137());
            return d.create();
        } else if (id == 200) {
            d = new Builder(this.mActivity);
            d.setCancelable(false);
            d.setTitle("Monthly Confirmation");
            d.setMessage("Verifying registration. Continue?");
            d.setPositiveButton("Yes", new C00148());
            d.setNegativeButton("No", new C00159());
            return d.create();
        } else if (id == 300) {
            d = new Builder(this.mActivity);
            d.setCancelable(false);
            d.setTitle("Montly Confirmation");
            d.setMessage("Registration has been confirmed.");
            d.setPositiveButton("OK", new OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    MonthCertification.this.write_data("bYear", new StringBuilder(String.valueOf(MonthCertification.this.year)).toString().getBytes());
                    MonthCertification.this.write_data("bMonth", new StringBuilder(String.valueOf(MonthCertification.this.month)).toString().getBytes());
                    MonthCertification.this.write_data("bDay", new StringBuilder(String.valueOf(MonthCertification.this.day)).toString().getBytes());
                    MonthCertification.this.write_data("bHour", new StringBuilder(String.valueOf(MonthCertification.this.hour)).toString().getBytes());
                    MonthCertification.this.write_data("bMinute", new StringBuilder(String.valueOf(MonthCertification.this.minute)).toString().getBytes());
                    MonthCertification.this.write_data("bSecond", new StringBuilder(String.valueOf(MonthCertification.this.second)).toString().getBytes());
                    MonthCertification.this.monthly_check_flag = false;
                }
            });
            return d.create();
        } else if (id == 400) {
            d = new Builder(this.mActivity);
            d.setCancelable(false);
            d.setTitle("Communication Error");
            d.setMessage("Unable to connect. Please check your data connections.");
            d.setPositiveButton("OK", new OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    MonthCertification.this.monthly_check_dialog();
                }
            });
            return d.create();
        } else if (id == 500) {
            String[] temp = this.msg[1].split("/");
            text = "";
            for (String append : temp) {
                text = new StringBuilder(String.valueOf(text)).append(append).append("\n").toString();
            }
            d = new Builder(this.mActivity);
            d.setCancelable(false);
            d.setTitle("code : " + this.msg[0]);
            d.setMessage(text);
            d.setPositiveButton("Yes", new OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    MonthCertification.this.webTo(MonthCertification.this.msg[2]);
                }
            });
            d.setNegativeButton("No", new OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    MonthCertification.this.exit();
                }
            });
            return d.create();
        } else if (id != DIALOG_ERROR_NOT_WEB_TO_MESSAGE) {
            return null;
        } else {
            String[] a = this.msg[1].split("/");
            text = "";
            for (String append2 : a) {
                text = new StringBuilder(String.valueOf(text)).append(append2).append("\n").toString();
            }
            d = new Builder(this.mActivity);
            d.setCancelable(false);
            d.setTitle("code : " + this.msg[0]);
            d.setMessage(text);
            d.setPositiveButton("OK", new OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    MonthCertification.this.monthly_check_dialog();
                }
            });
            return d.create();
        }
    }

    public void onPrepareDialog(int id, Dialog dialog) {
        if (id == DIALOG_ERROR_NOT_WEB_TO_MESSAGE) {
            String[] a = this.msg[1].split("/");
            String text = "";
            for (String append : a) {
                text = new StringBuilder(String.valueOf(text)).append(append).append("\n").toString();
            }
            ((AlertDialog) dialog).setTitle("code : " + this.msg[0]);
            ((AlertDialog) dialog).setMessage(text);
        }
    }

    private void webTo(String url) {
        try {
            this.mActivity.startActivityForResult(new Intent("android.intent.action.VIEW", Uri.parse(url)), 0);
            try {
                Thread.sleep(100);
            } catch (Exception e) {
            }
            exit();
        } catch (Exception e2) {
        }
    }

    public void exit() {
        this.mActivity.finish();
        System.exit(0);
    }

    public boolean logic() {
        if (!this.monthly_check_flag) {
            return false;
        }
        if (this.monthly_mode == 1) {
            this.monthly_mode = 2;
            ProgressDialogStart();
        } else if (this.monthly_mode == 2) {
            try {
                Calendar c = Calendar.getInstance();
                String y = new StringBuilder(String.valueOf(c.get(1))).toString();
                String m = new StringBuilder(String.valueOf(c.get(2) + 1)).toString();
                String d = new StringBuilder(String.valueOf(c.get(5))).toString();
                if (m.length() != 2) {
                    m = "0" + m;
                }
                if (d.length() != 2) {
                    d = "0" + d;
                }
                this.http_data = http_connect("http://puyosega-android.segamobile.jp/certification?esubid=" + getEncryptionSubID() + "&date=" + y + m + d + "&gid=" + this.appid);
                if (this.http_data == null) {
                    this.monthly_mode = 4;
                }
                ProgressDialogEnd();
                this.monthly_mode = 3;
            } catch (Exception e) {
                ProgressDialogEnd();
                this.monthly_mode = 4;
            }
        } else if (this.monthly_mode == 3) {
            try {
                if (new String(this.http_data, "Shift_jis").split(",")[0].equals("0")) {
                    this.monthly_mode = 5;
                } else {
                    this.monthly_mode = 6;
                }
            } catch (Exception e2) {
                Exception exception = e2;
                this.monthly_mode = 6;
            }
        } else if (this.monthly_mode == 4) {
            network_error_dialog();
            this.monthly_mode = 7;
        } else if (this.monthly_mode == 5) {
            network_ok_dialog();
            this.monthly_mode = 7;
        } else if (this.monthly_mode == 6) {
            try {
                String temp = new String(this.http_data, "Shift_jis");
                String[] _http_data = temp.split(",");
                if (temp.indexOf("http://") != -1) {
                    network_error_web_to_dialog(_http_data);
                } else {
                    network_error_not_web_to_dialog(_http_data);
                }
            } catch (Exception e3) {
            }
            this.monthly_mode = 7;
        }
        return true;
    }

    public byte[] http_connect(String path) throws Exception {
        Exception e;
        byte[] w = new byte[1024000];
        HttpURLConnection c = null;
        InputStream in = null;
        ByteArrayOutputStream out = null;
        try {
            c = (HttpURLConnection) new URL(path).openConnection();
            c.setRequestMethod("GET");
            c.connect();
            in = c.getInputStream();
            ByteArrayOutputStream out2 = new ByteArrayOutputStream();
            while (true) {
                try {
                    int size = in.read(w);
                    if (size <= 0) {
                        out2.close();
                        in.close();
                        c.disconnect();
                        return out2.toByteArray();
                    }
                    out2.write(w, 0, size);
                } catch (Exception e2) {
                    e = e2;
                    out = out2;
                }
            }
        } catch (Exception e22) {
            e = e22;
            if (c != null) {
                try {
                    c.disconnect();
                } catch (Exception e3) {
                }
            }
            if (in != null) {
                try {
                    in.close();
                } catch (Exception e4) {
                }
            }
            if (out != null) {
                try {
                    out.close();
                } catch (Exception e5) {
                }
            }
            throw e;
        }
    }

    public void network_ok_dialog() {
        try {
            this.mActivity.runOnUiThread(new Runnable() {
                public void run() {
                    MonthCertification.this.mActivity.showDialog(300);
                }
            });
        } catch (Exception e) {
        }
    }

    public void network_error_dialog() {
        try {
            this.mActivity.runOnUiThread(new Runnable() {
                public void run() {
                    MonthCertification.this.mActivity.showDialog(400);
                }
            });
        } catch (Exception e) {
        }
    }

    public void network_error_web_to_dialog(String[] msg) {
        this.msg = msg;
        try {
            this.mActivity.runOnUiThread(new Runnable() {
                public void run() {
                    MonthCertification.this.mActivity.showDialog(500);
                }
            });
        } catch (Exception e) {
        }
    }

    public void network_error_not_web_to_dialog(String[] msg) {
        this.msg = msg;
        try {
            this.mActivity.runOnUiThread(new Runnable() {
                public void run() {
                    MonthCertification.this.mActivity.showDialog(MonthCertification.DIALOG_ERROR_NOT_WEB_TO_MESSAGE);
                }
            });
        } catch (Exception e) {
        }
    }
}
