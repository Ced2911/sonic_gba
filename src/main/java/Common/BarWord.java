package Common;

import com.sega.mobile.framework.device.MFGraphics;

public interface BarWord {
    void drawWord(MFGraphics mFGraphics, int i, int i2, int i3);

    int getWordLength(int i);
}
